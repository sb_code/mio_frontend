/* eslint-disable no-restricted-globals */
var cacheName = 'mio-pwa-v1';
console.log(cacheName);
var filesToCache = [
  '/index.html',
  '/images',
  '/css',
  '/fonts',
  '/js',
  '/splashscreens',
];
self.addEventListener('install', function (e) {
  console.log('[ServiceWorker] Install');
  e.waitUntil(
    caches.open(cacheName).then(function (cache) {
      console.log('[ServiceWorker] Caching app shell');
      return cache.addAll(filesToCache);
    })
  );
});
self.addEventListener('activate', event => {
  event.waitUntil(self.clients.claim());
});
self.addEventListener('fetch', event => {
  event.respondWith(
    caches.match(event.request).then(response => {
      return response || fetch(event.request);
    })
  );
});