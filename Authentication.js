import React, { Component } from 'react';
import {useState} from 'react';

import Login from '../Pages/Login/Login.js';
import routeList from "../../routeList.json";
import cred from "../../cred.json";
import axios from "axios";
import moment from 'moment';
import _ from 'lodash';

var path = cred.API_PATH;


export function checkAuthentication(ChildComponent) {

    const initialAuthenticate = false
    const initialAuthorised = false
    const[authenticate, setAuthenticate] = useState(initialAuthenticate)
    const[authorised, setAuthorised] = useState(initialAuthorised)

    return class Authentication extends Component {

        constructor(props) {

            super(props)

        }

        

        componentDidMount() {

            this.isAuthenticated();
            console.log("Inside HOC --- >");

        }

        // checking whather storage data is available or not -- 
        isAuthenticated = () => {

            let storage = localStorage.getItem("MIO_Local");
            storage = storage ? JSON.parse(storage) : null;
            if (storage && storage.data.userId && storage.data.companyId && storage.data.companyType && storage.data.userType && storage.data.companyId) {
                // console.log("Inside true condition---");
                // return this.isAuthorised(storage.data.companyType, storage.data.userType, storage.data.companyId);
                this.setAuthenticate(true);
                this.isAuthorised(storage.data.companyType, storage.data.userType, storage.data.companyId);
                

            } else {
                let redirectURL = this.props && this.props.location ? `${this.props.location.pathname}${this.props.location.search}` : '';
                if (this.props && this.props.location && redirectURL && this.props.location.pathname == '/conferencecall') {
                    storage = {}
                    storage['redirectURL'] = redirectURL;
                    localStorage.setItem("MIO_Local_redirect", JSON.stringify(storage));
                }
                this.redirectLogin();
                // return false; *

                this.setAuthenticate(false);

            }

        }

        // checking this user type have access to the used url or not --
        isAuthorised = async(companyType, userType, companyId) => {

            let userRoutList = routeList[`${userType}`];
            // console.log("userRoutList ==", userRoutList)
            let Path = this.props.location.pathname.split('/')[1];
            let exactPath = '/' + `${Path}`;
            console.log("Path == ", Path);
            console.log("exactPath == ", exactPath);

            let routScope = !!userRoutList && _.filter(userRoutList, { 'to': this.props.location.pathname })[0];
            let othRoutScope = !!userRoutList && _.filter(userRoutList, { 'to': exactPath })[0];

            if (routScope || othRoutScope) {
                let trueCompanyList = [];
                let truePath = '';
                if (routScope) {
                    truePath = routScope && routScope.to;
                    trueCompanyList = routScope && routScope.company;
                    console.log("truePath == ", truePath);
                } else if (othRoutScope) {
                    truePath = othRoutScope && othRoutScope.to;
                    trueCompanyList = othRoutScope && othRoutScope.company;
                    console.log("truePath == ", truePath);
                }

                if (trueCompanyList && trueCompanyList.includes(companyType)) {

                    if (companyType == cred.COMPANY_TYPE_FREE_TYPE) {

                        if (this.props.location.pathname != '/upgrade-account') {
                            // return this.freeTypeAuthorization(companyId);

                            let freeTypeAuthorizationStatus = await this.freeTypeAuthorization(companyId);
                            if(freeTypeAuthorizationStatus && freeTypeAuthorizationStatus.valid){
                                this.setAuthorised(true);
                            }

                        } else {
                            // return true; *
                            this.setAuthorised(true);
                        }

                    } else {

                        if (trueCompanyList.includes(companyType)) {

                            // this.redirectUpgrade();
                            // return true; *
                            this.setAuthorised(true);

                        } else {
                            this.redirectLogin();
                            // return false; *
                            this.setAuthorised(false);
                        }

                    }

                } else {
                    this.redirectLogin();
                    // return false; *
                    this.setAuthorised(false);

                }

            } else {
                this.redirectLogin();
                // return false;*
                this.setAuthorised(false);

            }

        }


        freeTypeAuthorization = (companyId) => {
            return new Promise((resolve, reject)=> {

                console.log("freeTypeAuthorization --- >");
                let dataToSend = {
                    "companyId": companyId
                };
                axios
                    .post(path + 'user/get-company-by-id', dataToSend)
                    .then(serverResponse => {
                        let res = serverResponse.data;
                        let details = res && res.details;
                        let walletBalence = details.walletBalence;
                        let trialPeriod = details.trialPeriod;
                        // let createdAt = details.createdAt;

                        // let difference = (Date.now().getTime()-details.createdAt.getTime())/ (1000 * 3600 * 24);
                        let difference = (moment(Date.now()).diff(moment(details.createdAt))) / (1000 * 3600 * 24);
                        // console.log("difference --> ", difference);

                        if (parseFloat(trialPeriod).toFixed('2') < parseFloat(difference).toFixed('2')) {
                            this.redirectUpgrade();
                            // return false; *
                            resolve({
                                "valid": false
                            });
                        } else {
                            // return true; *
                            resolve({
                                "valid": true
                            });
                        }

                    })
                    .catch(error => {
                        console.log(error);
                    })

            })

        }

        // For date formatting -- 
        formatDate = (string) => {

            let options = { year: 'numeric', month: 'numeric', day: 'numeric' };
            return new Date(string).toLocaleDateString([], options);

        }


        redirectLogin = () => {
            this.props.history.push("/");
        }

        redirectUpgrade = () => {
            this.props.history.push("/upgrade-account");
        }

        render() {

            return (

                <div>

                    {/* {this.isAuthenticated() ? */}
                    {this.authenticate && this.authorised ?
                        <ChildComponent {...this.props} />
                        :
                        null

                    }

                </div>

            )

        }

    };
}

export default checkAuthentication;