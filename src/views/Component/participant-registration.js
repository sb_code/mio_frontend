import React, { Component } from 'react';
import { Link } from 'react-router-dom';
// import Navbar from "../../Component/Navbar";
import ReactTooltip from 'react-tooltip'; // For tool tip
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button, Card, Badge } from "react-bootstrap"

var path = cred.API_PATH;

class ParticipantRegistration extends Component {

    constructor(props) {
        super(props)
        this.state = {

            loading: false,
            invitationId: '',
            name: '',
            companyName: '',
            mobile: '',
            countryCode: '',
            email: '',
            error: '',

            countryCodeList: '',
            countryCodeList: [],

            showSuccessAlert: false,
            showErrorAlert: false,
            successAlertMessage: '',
            errorAlertMessage: '',

        }
    }

    componentDidMount() {

        document.body.classList.remove("sign-bg");
        document.body.classList.add("grey-bg");

        let invitationId = this.props.match.params.id;
        this.setState({
            invitationId: invitationId
        }, () => {
            this.getInvitationDetails();
            this.getCountryCodeList();
        });

    }

    getInvitationDetails = () => {

        let dataToSend = {
            "invitationId": this.state.invitationId
        };
        // API --
        axios
            .post(path + 'conference/get-invitation-details', dataToSend)
            .then(serverResponse => {
                let res = serverResponse.data;
                if (!res.isError) {

                    console.log("invitation details =", res);

                }

            })
            .catch(error => {
                console.log(error);
            })

    }

    getCountryCodeList = () => {

        axios
            .post(path + 'services/get-country-codes')
            .then(serverResponse => {

                let res = serverResponse.data;
                if (!res.isError) {
                    this.setState({
                        countryCodeList: res.details
                    });
                }

            })
            .catch(error => {
                console.log(error);
            })

    }

    submitRegistrationForm = () => {

        let { name, companyName, countryCode, mobile, email } = this.state;

        name == '' ? this.setState({ error: "Please fill all the mandatory fields." }) : this.setState({ error: "" });
        companyName == '' ? this.setState({ error: "Please fill all the mandatory fields." }) : this.setState({ error: "" });
        countryCode == '' ? this.setState({ error: "Please fill all the mandatory fields." }) : this.setState({ error: "" });
        countryCode == '' ? this.setState({ error: "Please fill all the mandatory fields." }) : this.setState({ error: "" });
        mobile == '' ? this.setState({ error: "Please fill all the mandatory fields." }) : this.setState({ error: "" });
        email == '' ? this.setState({ error: "Please fill all the mandatory fields." }) : this.setState({ error: "" });

        if (name == '' || companyName == '' || countryCode == '' || countryCode == '' || mobile == '' || email == '') {

            // console.log("mandatory fiels are pending to be field");

        } else {
            this.setState({
                error: ''
            });

            let dataToSend = {
                "invitationId": this.state.invitationId,
                "name": this.state.name,
                "company": this.state.companyName,
                "countryCode": this.state.countryCode,
                "phoneNumber": this.state.mobile,
                "email": email
            };

            axios
                .post(path + 'conference/participant-registration-submit', dataToSend)
                .then(serverResponse => {

                    let res = serverResponse.data;
                    if (!res.isError) {
                        // console.log("Registration response", res);
                        this.setState({
                            showSuccessAlert: true,
                            successAlertMessage: 'Registration done, please check your email for conference details'
                        }, () => {
                            this.resetForm();
                        });

                    } else {
                        this.setState({
                            showErrorAlert: true,
                            errorAlertMessage: 'Something is wrong...'
                        }, () => {
                            this.resetForm();
                        });

                    }

                })
                .catch(error => {
                    console.log(error);
                })

        }

    }

    resetForm = () => {

        this.setState({
            name: '',
            companyName: '',
            mobile: '',
            countryCode: '',
            email: '',
            error: '',
        })

    }

    clearAlert = () => {
        this.setState({
            loading: false,
            showSuccessAlert: false,
            successAlertMessage: '',
            showErrorAlert: false,
            errorAlertMessage: '',
        });
    }

    render() {
        let { loading, countryCodeList, invitationId, name, companyName, mobile, countryCode, email, error } = this.state;

        return (
            <main>
                <section className="user-mngnt-wrap">
                    <div className="container-fluid">
                        <div className="row justify-content-center">

                            {/* <Navbar routTo="/admincontentmanagement" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} /> */}
                            <div className="col-lg-9 p-0 mob-full-conf">

                                <div className="mobile-menu-header">
                                    {/* <a className="back-arw" onClick={() => console.log('<-BACK')}><i className="fas fa-arrow-left"></i></a> */}
                                    <a href="JavaScript:Void(0);" className="menu-tgl" onClick={() => this.sideMenuToggle()}><img src="images/menu-tgl.png" alt="" /></a>
                                </div>

                                <div className="mng-full-list about-mng-form">

                                    <div className="row">
                                        <div className="col-lg-12">
                                            <form>
                                                <div className="row">
                                                    <div className="col-md-12">
                                                        <div className="form-group">
                                                            <label>Invitation Id:</label>
                                                            <input type="text" name="invitationId" className="form-control" placeholder="Enter invitation id"
                                                                value={invitationId}
                                                                readOnly={true}
                                                                onChange={(e) => { this.setState({ invitationId: e.target.value }) }}
                                                            />
                                                        </div>
                                                    </div>

                                                    <div className="col-md-12">
                                                        <div className="form-group">
                                                            <label>User Name:<span style={{ color: "red" }}>*</span></label>
                                                            <input type="text" name="name" className="form-control" placeholder="Enter full name"
                                                                value={name}
                                                                readOnly={false}
                                                                onChange={(e) => { this.setState({ name: e.target.value, error: '' }) }}
                                                            />
                                                        </div>
                                                    </div>

                                                    <div className="col-md-12">
                                                        <div className="form-group">
                                                            <label>Company Name:<span style={{ color: "red" }}>*</span></label>
                                                            <input type="text" name="companyName" className="form-control" placeholder="Enter company name"
                                                                value={companyName}
                                                                readOnly={false}
                                                                onChange={(e) => { this.setState({ companyName: e.target.value, error: '' }) }}
                                                            />
                                                        </div>
                                                    </div>

                                                    <div className="col-md-4">
                                                        <div className="form-group">
                                                            <label>Country code:<span style={{ color: "red" }}>*</span></label>
                                                            <select className="form-control"
                                                                name="countryCode"
                                                                value={countryCode}
                                                                onChange={(e) => { this.setState({ countryCode: e.target.value, error: '' }) }}
                                                            >
                                                                <option value="">Select country</option>
                                                                {countryCodeList && countryCodeList.map((data, index) => {
                                                                    return <option key={index} value={data.code}>{data.code + ' ' + data.name}</option>
                                                                })}
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div className="col-md-8">
                                                        <div className="form-group">
                                                            <label>Phone No:<span style={{ color: "red" }}>*</span></label>
                                                            <input type="text" name="mobile" className="form-control" placeholder="Enter phone no"
                                                                value={mobile}
                                                                readOnly={false}
                                                                onChange={(e) => { this.setState({ mobile: e.target.value, error: '' }) }}
                                                            />
                                                        </div>
                                                    </div>

                                                    <div className="col-md-12">
                                                        <div className="form-group">
                                                            <label>email address:<span style={{ color: "red" }}>*</span></label>
                                                            <input type="email" name="email address" className="form-control" placeholder="Enter email address"
                                                                value={email}
                                                                readOnly={false}
                                                                onChange={(e) => { this.setState({ email: e.target.value, error: '' }) }}
                                                            />
                                                        </div>
                                                    </div>

                                                    <span style={{ color: "red" }}>{error}</span>

                                                </div>
                                            </form>
                                        </div>

                                    </div>

                                    <br />
                                    <div className="col-lg-3 col-md-4">

                                        <a href="JavaScript:Void(0);"
                                            onClick={() => { this.submitRegistrationForm() }}
                                            className="btn pay-btn"
                                            // ref={ref => this.fooRef = ref}
                                            data-tip='Save'
                                        // onFocus={() => { ReactTooltip.show(this.fooRef) }}
                                        >
                                            <i className="fa fa-save" />
                                        </a>
                                        <ReactTooltip
                                            effect="float"
                                            place="top"
                                            data-border="true"
                                        />

                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>

                </section>

                <Modal show={this.state.showSuccessAlert} onHide={() => this.clearAlert()}>
                    <Modal.Header closeButton>
                        <Modal.Title>OK</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <h5 style={{ color: 'green' }}>{`${this.state.successAlertMessage}`}</h5>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="primary" onClick={() => this.clearAlert()}>
                            Ok
            </Button>
                    </Modal.Footer>
                </Modal>

                <Modal show={this.state.showErrorAlert} onHide={() => this.clearAlert()}>
                    <Modal.Header closeButton>
                        <Modal.Title>OK</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <h5 style={{ color: 'red' }}>{`${this.state.errorAlertMessage}`}</h5>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="primary" onClick={() => this.clearAlert()}>
                            Ok
            </Button>
                    </Modal.Footer>
                </Modal>


            </main >
        );
    }
}

export default ParticipantRegistration;