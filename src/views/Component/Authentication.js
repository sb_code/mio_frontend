import React, { Component, ReactFragment } from 'react';
import Login from '../Pages/Login/Login.js';
import routeList from "../../routeList.json";
import cred from "../../cred.json";
import axios from "axios";
import moment from 'moment';
import _ from 'lodash';

var path = cred.API_PATH;


export function checkAuthentication(ChildComponent) {
    return class Authentication extends Component {

        constructor(props) {

            super(props)

        }

        componentDidMount() {

            this.isAuthenticated();
            console.log("Inside HOC --- >");

        }

        // checking whather storage data is available or not -- 
        isAuthenticated = () => {

            let storage = localStorage.getItem("MIO_Local");
            storage = storage ? JSON.parse(storage) : null;
            if (storage && storage.data.userId && storage.data.companyId && storage.data.companyType && storage.data.userType) {
                // console.log("Inside true condition---");
                return this.isAuthorised(storage.data.companyType, storage.data.userType, storage.data.companyId);
                // return true;

            } else {
                let redirectURL = this.props && this.props.location ? `${this.props.location.pathname}${this.props.location.search}` : '';
                if (this.props && this.props.location && redirectURL && this.props.location.pathname == '/conferencecall' && this.props.location.search) {
                    storage = {}
                    storage['redirectURL'] = redirectURL;
                    localStorage.setItem("MIO_Local_redirect", JSON.stringify(storage));
                }
                this.redirectLogin();
                return false;

            }

        }

        // checking this user type have access to the used url or not --
        isAuthorised = (companyType, userType, companyId) => {

            let userRoutList = routeList[`${userType}`];
            // console.log("userRoutList ==", userRoutList)
            let Path = this.props.location.pathname.split('/')[1];
            let exactPath = '/' + `${Path}`;
            console.log("Path == ", Path);
            console.log("exactPath == ", exactPath);

            let routScope = !!userRoutList && _.filter(userRoutList, { 'to': this.props.location.pathname })[0];
            let othRoutScope = !!userRoutList && _.filter(userRoutList, { 'to': exactPath })[0];

            if (routScope || othRoutScope) {
                let trueCompanyList = [];
                let truePath = '';
                if (routScope) {
                    truePath = routScope && routScope.to;
                    trueCompanyList = routScope && routScope.company;
                    console.log("truePath == ", truePath);
                } else if (othRoutScope) {
                    truePath = othRoutScope && othRoutScope.to;
                    trueCompanyList = othRoutScope && othRoutScope.company;
                    console.log("truePath == ", truePath);
                }

                if (trueCompanyList && trueCompanyList.includes(companyType)) {

                    if (companyType == cred.COMPANY_TYPE_FREE_TYPE) {

                        if (this.props.location.pathname != '/upgrade-account') {
                            return this.freeTypeAuthorization(companyId);
                        } else {
                            return true;
                        }

                    } else if (companyType == cred.COMPANY_TYPE_STARTUP) {

                        if (this.props.location.pathname != '/paymentmanagement') {
                            return this.strtupTypeBalanceCheck(companyId);
                        } else {
                            return true;
                        }

                    } else {

                        if (trueCompanyList.includes(companyType)) {

                            // this.redirectUpgrade();
                            return true;

                        } else {
                            this.redirectLogin();
                            return false;
                        }

                    }

                } else {
                    this.redirectLogin();
                    return false;
                }

            } else {
                this.redirectLogin();
                return false;
            }

        }


        freeTypeAuthorization = (companyId) => {
            // console.log("freeTypeAuthorization --- >");
            let dataToSend = {
                "companyId": companyId
            };
            return axios
                .post(path + 'user/get-company-by-id', dataToSend)
                .then(serverResponse => {
                    let res = serverResponse.data;
                    let details = res && res.details;
                    let walletBalence = details.walletBalence;
                    let minBalance = details.minBalance;
                    let trialPeriod = details.trialPeriod;
                    // let createdAt = details.createdAt;

                    let difference = (moment(Date.now()).diff(moment(details.createdAt))) / (1000 * 3600 * 24);
                    // console.log(trialPeriod,"difference --> ", difference);

                    if (parseFloat(trialPeriod) < parseFloat(difference)) {
                        this.redirectUpgrade();
                        return false;
                    } else {
                        return true;
                    }

                })
                .catch(error => {
                    console.log(error);
                })

        }

        strtupTypeBalanceCheck = (companyId) => {

            let dataToSend = {
                "companyId": companyId
            };
            return axios
                .post(path + 'user/get-company-by-id', dataToSend)
                .then(serverResponse => {
                    let res = serverResponse.data;
                    let details = res && res.details;
                    let walletBalence = details.walletBalence;
                    let minBalance = details.minBalance;


                    if (!walletBalence || parseFloat(walletBalence).toFixed('2') <= parseFloat(minBalance).toFixed('2')) {
                        this.redirectPayment();
                        return false;
                    } else {
                        return true;
                    }

                })
                .catch(error => {
                    console.log(error);
                })

        }

        // For date formatting -- 
        formatDate = (string) => {

            let options = { year: 'numeric', month: 'numeric', day: 'numeric' };
            return new Date(string).toLocaleDateString([], options);

        }


        redirectLogin = () => {
            this.props.history.push("/");
        }

        redirectUpgrade = () => {
            this.props.history.push("/upgrade-account");
        }

        redirectPayment = () => {
            this.props.history.push("/paymentmanagement");
        }

        render() {

            return (

                <div>

                    {this.isAuthenticated() ?
                        <ChildComponent {...this.props} />
                        :
                        null

                    }

                </div>

            )

        }

    };
}

export default checkAuthentication;