import React, { Component } from "react";
import { Link } from "react-router-dom";
import cred from "../../cred.json";
import routeList from "../../routeList.json";
import ReactTooltip from 'react-tooltip'; // For tool tip 
import { withRouter } from "react-router";

class Navbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: "",
      userType: "",
      userName: "",
      companyId: "",
      companyName: "",
      subRoot: "",
    };
  }

  componentDidMount() {
    // console.log("componentDidMount ========== ");
    let storage = localStorage.getItem("MIO_Local");
    storage = storage ? JSON.parse(storage) : null;
    if (storage && storage.data.userType) {

      let fname = storage.data.fname ? `${storage.data.fname}` : '';
      let lname = storage.data.lname ? `${storage.data.lname}` : '';

      if (storage.data.userType == 'GUEST') {
        this.setState({
          userType: storage.data.userType,
          userName: fname+' '+lname,
          companyType: 'GUEST',
        });
      } else {
        this.setState({
          userId: storage.data.userId,
          userType: storage.data.userType,
          userName: fname+' '+lname,
          companyName: storage.data.companyName,
          companyId: storage.data.companyId,
          companyType: storage.data.companyType,
          userPicUrl: storage.data.fileUrl
        });
      }

      let Path = this.props.location.pathname.split('/')[1];
      let exactPath = '/' + `${Path}`;
      this.setState({
        subRoot: exactPath
      });

    }
  }



  updateUserProfile = () => {

    this.props.history.push({
      pathname: '/userprofileupdate',
      state: {
        userId: this.state.userId
      }
    });

  }

  updateCompanyProfile = () => {

    this.props.history.push({
      pathname: '/companyprofileupdate',
      state: {
        companyId: this.state.companyId
      }
    });

  }


  logout() {
    localStorage.setItem("MIO_Local", null);
    localStorage.setItem("MIO_Local_redirect", null);
    this.props.history.push("/");
  }

  render() {
    let { userType, companyType } = this.state;

    let propValues = this.props && this.props.storageData && this.props.storageData.data;
    let fname = propValues && propValues.fname ? propValues.fname: '';
    let lname = propValues && propValues.lname ? propValues.lname: '';
    let companyName = propValues && propValues.companyName;
    let userPicUrl = propValues && propValues.fileUrl;
    
    return userType ? (
      <div className={this.props.open ? "col-xl-2 col-lg-3 p-0 main-menu-lt-col open" : 'col-xl-2 col-lg-3 p-0 main-menu-lt-col'}>
        <div className="main-menu-lt">
          <a href="JavaScript:Void(0);"
            className="back-arw d-md-none" onClick={() => this.props.closeMenu()}><i className="fas fa-arrow-left"></i>
          </a>
          <div className="logobox">
            <img src="/images/logo-sign.png" alt="" />
          </div>
          <div className="user-top-area">
            <div className="userimg">
              {/* <img src="/images/user-img.png" alt="" /> */}
              {propValues ?
                <img src={userPicUrl ? userPicUrl : "/images/user-img.png"} alt="userImg" />
                :
                null}
              {!propValues ?
                <img src={this.state.userPicUrl ? this.state.userPicUrl : "/images/user-img.png"} alt="userImg" />
                :
                null}
            </div>
            <h5>
              {this.state.userType != 'GUEST' ?
                <span
                  // ref={ref => this.fooRef = ref}
                  data-tip='Edit your profile'
                // onFocus={() => { ReactTooltip.show(this.fooRef) }}
                >
                  {propValues && fname && lname?
                    `${fname}` + ' ' + `${lname}`
                    :
                    `${this.state.userName}`
                  }
                  {/* &nbsp */}
                  <a href="JavaScript:Void(0);" onClick={() => { this.updateUserProfile() }} style={{ color: "#777a7d" }}>
                    <i className="fas fa-edit" />
                  </a>

                </span>
                :
                <span>
                  {this.state.userName}
                </span>
              }
              <ReactTooltip
                effect="float"
                place="top"
                data-border="true"
              />

              <br /><br />
              {this.state.userType == 'ADMIN' || this.state.userType == 'COMPANY_ADMIN' ?
                <small
                  // ref={ref => this.fooRef = ref}
                  data-tip='Edit company profile'
                // onFocus={() => { ReactTooltip.show(this.fooRef) }}
                >
                  {propValues && companyName ?
                    `${companyName}`
                    :
                    `${this.state.companyName}`
                  }
                  <a href="JavaScript:Void(0);" onClick={() => { this.updateCompanyProfile() }} style={{ color: "#777a7d" }}>
                    <i className="fas fa-edit" />
                  </a>
                </small>
                :
                <small>{this.state.companyName}</small>
              }
              <ReactTooltip
                effect="float"
                place="top"
                data-border="true"
              />
            </h5>
            <small onClick={() => this.logout()} style={{ cursor: "pointer" }}>Logout</small>
          </div>

          <div className="lt-menu">
            <ul>
              {
                routeList[userType].map((rout, index) => {
                  return rout.company.includes(companyType) && rout.status ? <li key={index} className={rout.to === this.props.routTo || rout.submenu.includes(this.state.subRoot) ? "current-menu" : ""} onClick={() => this.props.mobMenuClick()}>
                    <Link to={rout.to}>{rout.caption}</Link>
                  </li>
                    : null
                })
              }
            </ul>

          </div>

          <div>
            <ul style={{ background: '#0092DD', border: 'double 5px white', margin: '5px' }}>
              <li style={{ padding: "2px", margin: "2px", color: 'white' }}><Link to="/aboutus" target="_blank" style={{ color: "white", textDecoration: "underline" }}>About Us</Link></li>
              <li style={{ padding: "2px", margin: "2px", color: 'white' }}><Link to="/contactus" target="_blank" style={{ color: "white", textDecoration: "underline" }}>Contact Us</Link></li>
              <li style={{ padding: "2px", margin: "2px", color: 'white' }}><Link to="/faqs" target="_blank" style={{ color: "white", textDecoration: "underline" }}>FAQs</Link></li>
              <li style={{ padding: "2px", margin: "2px", color: 'white' }}><Link to="/privacypolicy" target="_blank" style={{ color: "white", textDecoration: "underline" }}>Privacy Policy</Link></li>
              <li style={{ padding: "2px", margin: "2px", color: 'white' }}><Link to="/termsconditions" target="_blank" style={{ color: "white", textDecoration: "underline" }}>Terms and Conditions</Link> </li>
            </ul>
          </div>

        </div>
        <ReactTooltip
          effect="float"
          place="top"
          data-border="true"
        />
      </div>

    ) : null;
  }
}

export default withRouter(Navbar);
