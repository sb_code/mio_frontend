import React from "react";
import { Bar } from "react-chartjs-2";
import { MDBContainer } from "mdbreact";

class ChartsPage extends React.Component {
  state = {
    dataBar: {
      //   labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
      labels: this.props.labels,
      datasets: [
        {
          label: "time of utilization (Hours)",
          // data: ["12", "19", "3", "5", 0],
          data: this.props.data,
          backgroundColor: [
            "rgba(98,  182, 239,0.4)",
            "rgba(98,  182, 239,0.4)",
            "rgba(98,  182, 239,0.4)",
            "rgba(98,  182, 239,0.4)",
            "rgba(98,  182, 239,0.4)",
            "rgba(98,  182, 239,0.4)", 
            "rgba(98,  182, 239,0.4)",
            "rgba(98,  182, 239,0.4)",
            "rgba(98,  182, 239,0.4)",
            "rgba(98,  182, 239,0.4)",
            "rgba(98,  182, 239,0.4)",
            "rgba(98,  182, 239,0.4)",
          ],         
          // backgroundColor: [
          //   "rgba(255, 134,159,0.4)",
          //   "rgba(98,  182, 239,0.4)",
          //   "rgba(255, 218, 128,0.4)",
          //   "rgba(113, 205, 205,0.4)",
          //   "rgba(170, 128, 252,0.4)",
          //   "rgba(255, 177, 101,0.4)"
          // ],
          borderWidth: 2,
          borderColor: [
            "rgba(98,  182, 239, 1)",
            "rgba(98,  182, 239, 1)",
            "rgba(98,  182, 239, 1)",
            "rgba(98,  182, 239, 1)",
            "rgba(98,  182, 239, 1)",
            "rgba(98,  182, 239, 1)",  
            "rgba(98,  182, 239, 1)",
            "rgba(98,  182, 239, 1)",
            "rgba(98,  182, 239, 1)",
            "rgba(98,  182, 239, 1)",
            "rgba(98,  182, 239, 1)",
            "rgba(98,  182, 239, 1)",        
          ],
          // borderColor: [
          //   "rgba(255, 134, 159, 1)",
          //   "rgba(98,  182, 239, 1)",
          //   "rgba(255, 218, 128, 1)",
          //   "rgba(113, 205, 205, 1)",
          //   "rgba(170, 128, 252, 1)",
          //   "rgba(255, 177, 101, 1)"
          // ]
        }
      ]
    },
    barChartOptions: {
      responsive: true,
      maintainAspectRatio: false,
      scales: {
        xAxes: [
          {
            barPercentage: 1,
            gridLines: {
              display: true,
              color: "rgba(0, 0, 0, 0.1)"
            }
          }
        ],
        yAxes: [
          {
            gridLines: {
              display: true,
              color: "rgba(0, 0, 0, 0.1)"
            },
            ticks: {
              beginAtZero: true
            }
          }
        ]
      }
    }
  }


  render() {
    console.log("data--> ", this.props.data);
    
    return (

      <MDBContainer style={{ width: "934px", height: "550px" }}>
        {/* <h3 className="mt-5">Bar chart</h3> */}
        <Bar data={this.state.dataBar} options={this.state.barChartOptions} />
      </MDBContainer>
    );
  }
}

export default ChartsPage;