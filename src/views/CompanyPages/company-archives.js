import React, { Component } from "react";
import Navbar from "../Component/Navbar";
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button } from "react-bootstrap"
import _ from 'lodash';
import { CSVLink, CSVDownload } from "react-csv";
import ReactTooltip from 'react-tooltip'; // For tool tip 

import { checkAuthentication } from '../Component/Authentication';

var path = cred.API_PATH;

export class CompanyArchives extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userId: '',
            selectedCompanyId: '',
            selectedCompanyName: '',
            selectedCompanyType: '',

            openSideMenu: false,
            searchString: '',
            searchType: '',
            searching: false,

            conferenceList: [],
            page: 1,
            listRecords: '',

            loading: false,
            searchLoading: false,

            serviceName: '',
            costPerMin: '',
            multiplier: '',
            pricePerMin: '',

            editUser: false,

        }
        this.perPage = 5;

    }

    componentDidMount() {
        document.body.classList.remove("sign-bg");
        document.body.classList.add("grey-bg");

        let storage = localStorage.getItem("MIO_Local");
        storage = storage ? JSON.parse(storage) : null;

        if (storage && storage.data.userId) {
            console.log("Archive state == ", this.props.location.state);
            if (this.props.location.state) {
                this.setState({
                    userId: storage.data.userId,
                    userType: storage.data.userType,
                    companyId: this.props.location.state.companyId,
                    companyName: this.props.location.state.name
                }, () => {
                    this.getConferenceListForAdmin()
                    // console.log('selected conference --', this.state.selectedCompanyId);
                });
            } else {
                this.setState({
                    userId: storage.data.userId,
                    userType: storage.data.userType,
                    companyId: storage.data.companyId
                }, () => {
                    this.getConferenceListForAdmin()
                    // console.log('selected conference --', this.state.selectedCompanyId);
                });
            }
            // console.log(this.parseJwt(storage.token));
            let tokenData = this.parseJwt(storage.token);

        }
        else {
            this.props.history.push("/conferencecall");
        }

    }

    getConferenceListForAdmin = () => {
        this.setState({ loading: true });
        let dataToSend = {
            "companyId": this.state.companyId,
            "page": this.state.page,
            "perPage": this.perPage
        };

        axios
            .post(path + 'conference/list-conference-company', dataToSend)
            .then(serverResponse => {
                let res = serverResponse.data;
                if (!res.isError) {
                    let conferenceList = res.details && res.details[0] && res.details[0].results;
                    this.setState({
                        loading: false,
                        listRecords: res.details && res.details[0] && res.details[0].noofpage,
                        conferenceList: conferenceList
                    });
                } else {
                    this.setState({ loading: false });
                }
            })
            .catch(error => {
                console.log(error);
            })

    }


    searchConference = (event) => {
        event.preventDefault();
        this.setState({
            searching: true,
            searchLoading: true
        });

        let dataToSend = {
            "conferenceName": this.state.searchString,
            "companyId": this.state.companyId
        };
        // API ToDo - 
        axios
            .post(path + 'conference/search-conference', dataToSend)
            .then(serverResponse => {
                let res = serverResponse.data;
                if (!res.isError) {
                    let conferenceList = res.details;
                    this.setState({
                        searchLoading: false,
                        conferenceList: conferenceList
                    });
                } else {
                    alert("Something is wrong---");
                    this.setState({
                        searchLoading: false,
                        searching: false
                    }, () => {
                        this.getConferenceListForAdmin();
                    })
                }
            })
            .catch(error => {
                this.setState({
                    searchLoading: false,
                    searching: false
                });
                console.log(error);
            })

    }

    clearSearch = (event) => {
        event.preventDefault()

        this.setState({
            searchLoading: false,
            searching: false,
            searchString: ''
        }, () => {
            this.getConferenceListForAdmin();
        });
    }

    viewArchives = (index) => {
        // console.log("View archives ---- ");
        let { conferenceList } = this.state;

        let { _id, conferenceTitle } = conferenceList && conferenceList[index];

        this.props.history.push({
            pathname: '/conferencearchivelist/' + `${_id}`,
            state: {
                name: conferenceTitle
            }
        });

    }

    editUsers = () => {
        let dataToSend = {
            "serviceName": this.state.serviceName,
            "costPerMin": this.state.costPerMin,
            "multiplier": this.state.multiplier,
            "pricePerMin": this.state.pricePerMin
        };
        // API ToDo
        console.log("dataToSave -- ", dataToSend);

    }

    clearState = (event) => {
        this.setState({
            loading: false,
            searchLoading: false,
            searching: false,
            serviceName: '',
            costPerMin: '',
            multiplier: '',
            pricePerMin: '',

            editUser: false,
        });
    }

    pageChangePrev = () => {

        this.setState({
            page: (parseFloat(this.state.page) - 1)
        }, () => {
            this.getConferenceListForAdmin()
        });

    }

    pageChangeNext = () => {

        this.setState({
            page: (parseFloat(this.state.page) + 1)
        }, () => {
            this.getConferenceListForAdmin()
        });
    }

    backMenueToggle = () => {
        this.props.history.goBack();
    }

    sideMenuToggle() {
        let { openSideMenu } = this.state;
        openSideMenu = !openSideMenu;
        this.setState({ openSideMenu });
    }

    mobMenuClick() {
        this.sideMenuToggle();
        console.log('mobMenuClick');
    }

    parseJwt = (token) => {
        if (!token) { return; }
        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace('-', '+').replace('_', '/');
        return JSON.parse(window.atob(base64));
    }


    render() {
        let { searchString, loading, searchLoading, serviceName, costPerMin, multiplier, pricePerMin, searching, page, listRecords } = this.state;
        return (
            <main>

                <section className="user-mngnt-wrap">
                    <div className="container-fluid">
                        <div className="row">
                            <Navbar routTo="/companyarchives" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />

                            <div className="col-xl-10 col-lg-9 p-0 mob-full-conf">

                                <div className="mobile-menu-header">
                                    <a href="JavaScript:Void(0);" className="back-arw" onClick={() => this.backMenueToggle()}><i className="fas fa-arrow-left"></i></a>
                                </div>
                                {this.state.selectedCompanyName ?
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="inv-code">
                                                <span style={{ color: '#007bff' }}>{this.state.selectedCompanyName}</span>
                                            </div>
                                        </div>
                                    </div>
                                    : null}
                                <div className="mng-full-list">
                                    <div className="mng-full-srch">
                                        <div className="row">
                                            <div className="col-md-10">
                                                <div className="srch-wrap">
                                                    <form>
                                                        <input type="text" name="searchString" value={searchString}
                                                            className="form-control" placeholder="Search conference by name"
                                                            onChange={(event) => { this.setState({ searchString: event.target.value }) }}
                                                        />
                                                        {this.state.searching ?
                                                            <input type="button" onClick={(event) => this.clearSearch(event)}
                                                                data-tip='Clear'
                                                            />
                                                            :
                                                            <input type="submit" onClick={(event) => this.searchConference(event)}
                                                                data-tip='Search'
                                                            />
                                                        }
                                                        <ReactTooltip
                                                            effect="float"
                                                            place="top"
                                                            data-border="true"
                                                        />
                                                    </form>
                                                </div>
                                            </div>
                                            {searchLoading?
                                                <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                                                    <i className="fa fa-spinner fa-spin" ></i>
                                                </a>
                                            :null}
                                        </div>
                                    </div>


                                    <div className="mng-full-table">
                                        <div className="row">

                                            <div className="col-md-10">
                                                <div className="mng-full-table-hdr">
                                                    <div className="row">
                                                        <div className="col-md-4 border-rt">
                                                            <h6>Conference Name</h6>
                                                        </div>
                                                        <div className="col-md-4 border-rt">
                                                            <h6>Recording Preference</h6>
                                                        </div>
                                                        <div className="col-md-4">
                                                            <h6>No of Participants</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="col-md-2">
                                                <div className="mng-full-table-hdr">
                                                    <div className="row">
                                                        <div className="col-md-12">
                                                            <h6>Archives</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        {this.state.conferenceList && this.state.conferenceList.map((user, index) => {
                                            return <div className="row" key={index}>

                                                <div className="col-md-10">
                                                    <div className="mng-full-table-row">
                                                        <div className="row">
                                                            <div className="col-md-4 border-rt">
                                                                <h6>Conference Name</h6>
                                                                <p className="textEllips">
                                                                    <a href="JavaScript:Void(0);" onClick={() => this.viewArchives(index)}>
                                                                        {user.conferenceTitle}
                                                                    </a>
                                                                </p>
                                                            </div>
                                                            <div className="col-md-4 border-rt">
                                                                <h6>Recording Preference</h6>
                                                                <p className="textEllips">{user.recordingPreference}</p>
                                                            </div>
                                                            <div className="col-md-4">

                                                                <h6>No of Participants</h6>
                                                                <p className="textEllips">
                                                                    {user.participants && user.participants.length}
                                                                </p>

                                                            </div>

                                                            <div className="mobile-ad-edt-btns">
                                                                <ul>
                                                                    <li onClick={() => this.viewArchives(index)}>
                                                                        <a href="JavaScript:Void(0);"
                                                                            data-tip='View'
                                                                        >
                                                                            <i className="fas fa-play"></i>

                                                                        </a>
                                                                        <ReactTooltip
                                                                            effect="float"
                                                                            place="top"
                                                                            data-border="true"
                                                                        />
                                                                    </li>

                                                                </ul>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>

                                                <div className="col-md-2">
                                                    <div className="mng-full-table-row add-edt text-center">
                                                        <div className="row">
                                                            <div className="col-md-12">
                                                                <a href="JavaScript:Void(0);"
                                                                    data-tip='View'
                                                                    onClick={() => { this.viewArchives(index) }}
                                                                >
                                                                    <i className="fas fa-play"></i>

                                                                </a>
                                                                <ReactTooltip
                                                                    effect="float"
                                                                    place="top"
                                                                    data-border="true"
                                                                />
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        })}

                                    </div>
                                    <br /><br />
                                    {!searching ?
                                        <div className="form-group">
                                            {!loading && parseFloat(page) > 1 ?
                                                <span style={{ color: "#007bff", cursor: "pointer" }}
                                                    onClick={() => { this.pageChangePrev() }}
                                                >
                                                    <b>{"<< Prev"}</b>
                                                </span>
                                                :
                                                null}
                                            &nbsp;&nbsp;|&nbsp;&nbsp;
                                            {!loading && (parseFloat(page) < parseFloat(listRecords)) ?
                                                <span style={{ color: "#007bff", cursor: "pointer" }}
                                                    onClick={() => { this.pageChangeNext() }}
                                                >
                                                    <b>{"Next >>"}</b>
                                                </span>
                                                :
                                                null}
                                            {loading ?
                                                <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                                                    <i className="fa fa-spinner fa-spin" ></i>
                                                </a>
                                                :
                                                null}

                                        </div>
                                        :
                                        null}

                                </div>

                            </div>

                        </div>
                    </div>
                </section>

            </main>
        );
    }

}

export default checkAuthentication(CompanyArchives);