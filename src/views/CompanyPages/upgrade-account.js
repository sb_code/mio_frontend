import React, { Component } from 'react'
import Navbar from '../Component/Navbar'
import Axios from "axios";
import cred from "../../cred.json";
import { Modal, Button } from "react-bootstrap"
import _ from 'lodash';
import { CSVLink, CSVDownload } from "react-csv";
import ReactTooltip from 'react-tooltip'; // For tool tip 

import { checkAuthentication } from '../Component/Authentication';

var path = cred.API_PATH;

export class UpgradeAccount extends Component {
  constructor(props) {
    super(props)
    this.state = {
      openSideMenu: false,
      companyType: "",
      companyId: "",
      companyName: '',
      userId: '',

      companyDetails: {},
      requestId: '',

      freePackage: '',
      startupPackage: '',
      enterprisePackage: '',

      showSuccessAlert: false,
      successAlertMessage: '',
      showErrorAlert: false,
      errorAlertMessage: '',

    }
  }

  componentDidMount() {
    document.body.classList.remove("sign-bg");
    document.body.classList.add("grey-bg");

    let storage = localStorage.getItem("MIO_Local");
    storage = storage ? JSON.parse(storage) : null;
    if (storage && storage.data.userId && storage.data.companyId && storage.data.companyType) {
      this.setState({
        companyId: storage.data.companyId,
        companyType: storage.data.companyType,
        companyName: storage.data.companyName,
        userId: storage.data.userId
      }, () => {
        this.getCompanyDetails();
        this.getPackageDetails();
      });
    }
    // else {
    //   this.props.history.push("/");
    // }
  }

  getCompanyDetails = () => {
    let dataToSend = {
      "companyId": this.state.companyId
    };

    Axios
      .post(path + 'user/get-company-by-id', dataToSend)
      .then(serverResponse => {

        let res = serverResponse.data;
        if (!res.isError) {
          console.log("getting company details ==>", res);

          let companyDetails = res && res.details;
          let requestId = companyDetails && companyDetails.upgradeRequestId;
          this.setState({
            companyDetails: companyDetails,
            requestId: requestId
          });

        }

      })
      .catch(error => {
        console.log(error);
      })

  }

  getPackageDetails = () => {
    this.setState({ loading: true });
    Axios
      .post(path + 'content-management/get-pricing-plans')
      .then(serverResponse => {

        let res = serverResponse.data;
        if (!res.isError) {
          // console.log("all pricing package details ===", res);

          let freePackage = res.plans && res.plans[0] && res.plans[0].contents;
          let startupPackage = res.plans && res.plans[1] && res.plans[1].contents;
          let enterprisePackage = res.plans && res.plans[2] && res.plans[2].contents;

          let freePackageId = res.plans && res.plans[0] && res.plans[0]._id;
          let startupPackageId = res.plans && res.plans[1] && res.plans[1]._id;
          let enterprisePackageId = res.plans && res.plans[2] && res.plans[2]._id;

          this.setState({
            loading: false,
            freePackage,
            startupPackage,
            enterprisePackage
          });

        } else {
          this.setState({
            loading: false
          })
        }

      })
      .catch(error => {
        console.log(error);
      })

  }


  sideMenuToggle() {
    let { openSideMenu } = this.state;
    openSideMenu = !openSideMenu;
    this.setState({ openSideMenu });
  }

  mobMenuClick() {
    this.sideMenuToggle();
    console.log('mobMenuClick');
  }

  selectPackage(packgId) {
    let that = this;
    // TODO: save company type in db
    let filterArr = _.filter(cred.PACKAGES, { "id": packgId });
    if (filterArr && filterArr.length > 0) {
      let selectedPackage = filterArr[0];
      let data = {
        "companyId": this.state.companyId,
        "companyType": selectedPackage.codeName
      }

      this.setState({
        showSuccessAlert: true,
        successAlertMessage: 'A request for upgradation to - ' + selectedPackage.codeName + ' has been sent. Please wait for approval from admin.',
      });

      // Axios.post(
      //   path + "user/update-company-details", data,
      //   { headers: { "Content-Type": "application/json" } }
      // ).then(serverResponse => {
      //   const res = serverResponse.data;
      //   if (!res.isError) {
      //     let storage = localStorage.getItem("MIO_Local");
      //     storage = storage ? JSON.parse(storage) : null;
      //     storage.data.companyType = selectedPackage.codeName;
      //     localStorage.setItem("MIO_Local", JSON.stringify(storage));

      //     that.updateBaseService(selectedPackage.codeName);

      //     // if (packgId == 'P001') {
      //     // redirect to home page
      //     this.props.history.push("/");
      //     // } else {
      //     //   // redirect to payment page
      //     //   this.props.history.push(routeList['OTHER']['addPayment']['to']);
      //     // }
      //   } else {
      //     alert('something goes wrong');
      //   }
      // }).catch(error => {
      //   alert('something goes wrong');
      // });

    } else {
      alert('invalid package.')
    }

  }

  updateBaseService = (companyType) => {

    let storage = localStorage.getItem("MIO_Local");
    storage = storage ? JSON.parse(storage) : null;

    if (storage.data) {
      let dataToSend = {
        "companyId": storage.data.companyId,
        "companyType": companyType
      };
      // API - 
      Axios
        .post(path + 'services/add-base-services-to-company', dataToSend)
        .then(serverResponse => {
          let res = serverResponse.data;
          if (!res.isError) {

            console.log("updateBaseService---", res);

          }
        })
        .catch(error => {
          console.log(error);
        })

    }
  }

  sendUpgradeReq = (code) => {

    let filterArr = _.filter(cred.PACKAGES, { "id": code });

    let changeType = '';
    if(this.state.companyType == cred.COMPANY_TYPE_ENTERPRISE && code != "P003"){
      changeType = 'DOWNGRADE';
    }else{
      changeType = 'UPGRADE';
    }

    if (filterArr && filterArr.length > 0) {
      let selectedPackage = filterArr[0];
      // this.setState({
      //   loading: true
      // });
      let dataToSend = {
        "companyId": this.state.companyId,
        "companyName": this.state.companyName,
        "requestedBy": this.state.userId,
        "currentCompanyType": this.state.companyType,
        "changeToCompanyType": selectedPackage.codeName,
        "changeType": changeType
      }

      Axios
        .post(path + 'user/company-upgrade-request', dataToSend)
        .then(serverResponse => {

          let res = serverResponse.data;
          if (!res.isError) {
            console.log("request sent -> ", res);

            this.setState({
              showSuccessAlert: true,
              successAlertMessage: 'Request sent. Please wait for approval...'
            }, () => {
              this.getCompanyDetails();
            });

          } else {
            this.setState({
              showErrorAlert: true,
              errorAlertMessage: 'Sorry! Something is wrong...'
            });
          }

        })
        .catch(error => {
          console.log(error);
        })

    }

  }


  hideSuccessAlert = () => {

    this.setState({
      showSuccessAlert: false,
      successAlertMessage: ''
    });

  }

  hideErrorAlert = () => {

    this.setState({
      showErrorAlert: false,
      errorAlertMessage: '',
    });

  }


  render() {
    let { companyType, requestId, freePackage,
      startupPackage,
      enterprisePackage } = this.state;
    return <main>
      
      <section className="user-mngnt-wrap">
        <div className="container-fluid">
          <div className="row">
            <Navbar routTo="/upgrade-account" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />
            <div className="col-xl-10 col-lg-9 p-0">

              <div className="mobile-menu-header">
                <a href="JavaScript:void(0);" className="back-arw"><i className="fas fa-arrow-left"></i></a>
                <a href="JavaScript:Void(0);" className="menu-tgl"><img src="images/menu-tgl.png" alt="" /></a>
              </div>

              {/* <div className="custom-brdcrmb">
                <nav aria-label="breadcrumb">
                  <ol className="breadcrumb">
                    <li className="breadcrumb-item"><a href="JavaScript:Void(0);">Home</a></li>
                    <li className="breadcrumb-item active" aria-current="page">Upgrade Account</li>
                  </ol>
                </nav>
              </div> */}

              {requestId && requestId != '' ?
                <div className="upgrd-mng-wrap">
                  <div className="row">

                    <div className="col-xl-10">
                      <div className="pkg-box-wrap upgrd">
                        <div className="pkg-box">
                          <h2>A request already sent.</h2>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
                :
                <div className="upgrd-mng-wrap">
                  <div className="row">
                    {companyType == cred.COMPANY_TYPE_FREE_TYPE || companyType == cred.COMPANY_TYPE_ENTERPRISE ?
                      <div className="col-xl-4">
                        <div className="pkg-box-wrap upgrd">
                          <div className="pkg-box">
                            <h2>Starter</h2>
                            {/* <div className="price">$25.00</div>
                            <h4>Features:</h4> */}
                            <ol>
                            {startupPackage && startupPackage.length> 0 && startupPackage.map((value, index)=>{
                              return<li key= {index}>{value}</li>
                            })}
                            </ol>
                            
                          </div>
                          <a href="JavaScript:Void(0);" className="btn" onClick={() => { this.sendUpgradeReq("P002") }}>
                            {companyType == cred.COMPANY_TYPE_FREE_TYPE?
                              <i className="fas fa-angle-double-up"></i>
                              :
                              null}
                            {companyType == cred.COMPANY_TYPE_ENTERPRISE?
                              <i className="fas fa-angle-double-down"></i>
                            :
                            null}
                            {companyType == cred.COMPANY_TYPE_FREE_TYPE?
                              "Upgrade"
                              :companyType == cred.COMPANY_TYPE_ENTERPRISE?
                                "Downgrade"
                              :
                              null}

                          </a>
                        </div>
                      </div>
                      : null}
                    {companyType == cred.COMPANY_TYPE_FREE_TYPE || companyType == cred.COMPANY_TYPE_STARTUP ?
                      <div className="col-xl-4">
                        <div className="pkg-box-wrap upgrd">
                          <div className="pkg-box">
                            <h2>Enterprise</h2>
                            {/* <div className="price">$50.00</div>
                            <h4>Features:</h4> */}
                            <ol>
                              {enterprisePackage && enterprisePackage.length>0 && enterprisePackage.map((value, index)=>{
                                return<li key = {index}>{value}</li>
                              })}
                            </ol>
                          </div>
                          <a href="JavaScript:Void(0);" className="btn" onClick={() => { this.sendUpgradeReq("P003") }}>
                            <i className="fas fa-angle-double-up"></i>
                            Upgrade
                          </a>
                        </div>
                      </div>
                      : null}
                  </div>
                </div>
              }
            </div>
          </div>
        </div>
      </section>


      <Modal show={this.state.showSuccessAlert} onHide={() => this.hideSuccessAlert()}>
        <Modal.Header closeButton>
          <Modal.Title>OK</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h5 style={{ color: 'green' }}>{`${this.state.successAlertMessage}`}</h5>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="primary" onClick={() => this.hideSuccessAlert()}>
            Ok
          </Button>
        </Modal.Footer>
      </Modal>


      <Modal show={this.state.showErrorAlert} onHide={() => this.hideErrorAlert()}>
        <Modal.Header closeButton>
          <Modal.Title>OK</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h5 style={{ color: 'red' }}>{`${this.state.errorAlertMessage}`}</h5>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="primary" onClick={() => this.hideErrorAlert()}>
            Ok
            </Button>
        </Modal.Footer>
      </Modal>

    </main>
  }
}

export default checkAuthentication(UpgradeAccount);