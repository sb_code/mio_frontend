import React, { Component } from 'react'
import Navbar from '../Component/Navbar'
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button } from "react-bootstrap"
import _ from 'lodash';
import { CSVLink, CSVDownload } from "react-csv";
import ReactTooltip from 'react-tooltip'; // For tool tip 
import { CSVReader } from 'react-papaparse';
import { Snackbar } from "@material-ui/core";
import MySnackbarContentWrapper from "../Component/MaterialSnack";

import { checkAuthentication } from '../Component/Authentication';

var path = cred.API_PATH;


export class OpmAddressBook extends Component {
  constructor(props) {
    super(props)
    this.state = {

      openSideMenu: false,
      searchString: '',
      searching: false,

      loading: false,

      showAddAddress: false,
      showEditAddress: false,
      showDeleteAddress: false,
      selectedId: "",

      countryCodeList: [],
      contacts: [],
      pageNo: 1,

      userType: cred.USER_TYPE_GUEST,
      name: "",
      emailAddress: "",
      phoneNumber: "",
      countryCode: "",
      contactUserId: "",

      nameError: "",
      emailAddressError: "",
      phoneNumberError: "",
      countryCodeError: "",
      importDataError: "",
      data: [],
      fileName: '',
      showImportAddress: false,

      EmailError: [],

      showErrorAlert: false,
      errorAlertMessage: '',
      showSuccessAlert: false,
      successAlertMessage: ''


    }
    this.fileInput = React.createRef();
    this.perPage = 10;
  }

  componentDidMount() {
    document.body.classList.remove("sign-bg");
    document.body.classList.add("grey-bg");

    let storage = localStorage.getItem("MIO_Local");
    storage = storage ? JSON.parse(storage) : null;
    if (storage && storage.data.userId) {
      this.setState({
        userId: storage.data.userId
      }, () => {
        this.getAddressBook(storage.data.userId);
        this.getCountryCodeList();
      });

    }

  }

  getAddressBook(userId) {
    let that = this;
    let contacts = [];
    let dataToSend = {
      "userId": userId,
      "page": that.state.pageNo,
      "perPage": this.perPage
    };
    this.setState({loading: true});

    axios
      .post(path + "user/get_contact_list", dataToSend, {
        headers: { "Content-Type": "application/json" }
      })
      .then(serverResponse => {
        console.log("Response from here : ", serverResponse.data);
        const res = serverResponse.data;
        if (!res.isError) {
          contacts = res.details && res.details.results ? res.details.results : [];
          // console.log(res.details.results);
          that.setState({ 
            loading: false,
            contacts: contacts,
            userRecords: res.details && res.details.noofpage
          });
        } else {
          this.setState({
            loading: false
          })
        }
      }).catch(error => {
        this.setState({
          loading: false
        })
        console.log(error);
      });
  }

  getCountryCodeList = () => {

    axios
      .post(path + 'services/get-country-codes')
      .then(serverResponse => {

        let res = serverResponse.data;
        if (!res.isError) {
          this.setState({
            countryCodeList: res.details
          });
        }

      })
      .catch(error => {
        console.log(error);
      })

  }

  hideAddAddress() {
    this.setState({
      name: "",
      emailAddress: "",
      phoneNumber: "",
      countryCode: "",
      contactUserId: "",
      showAddAddress: false
    })
  }

  saveAddress = (event) => {

    let { contacts, userId, name, emailAddress, phoneNumber, countryCode, contactUserId, userType, loading } = this.state;
    let that = this;
    !name ? this.setState({ nameError: "Please enter a contact name." }) : this.setState({ nameError: "" });
    !emailAddress ? this.setState({ emailAddressError: "Please enter an email address." }) : this.setState({ emailAddressError: "" });
    !phoneNumber ? this.setState({ phoneNumberError: "Please enter a phone number." }) : this.setState({ phoneNumberError: "" });
    !countryCode ? this.setState({ countryCodeError: "Please enter a country code." }) : this.setState({ countryCodeError: "" });

    if (userId && name && emailAddress && phoneNumber && countryCode && userType) {

      this.setState({ loading: true });

      // if (/^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/.test(`${emailAddress}`)) {
      (/.+@.+\.[A-Za-z]+$/.test(`${emailAddress}`)) ? this.setState({ emailAddressError: "" }) : this.setState({ emailAddressError: "Email address is not valid" });
      (!isNaN(phoneNumber)) ? this.setState({ phoneNumberError: "" }) : this.setState({ phoneNumberError: "Phone Number is not valid" });
      (!isNaN(countryCode)) ? this.setState({ countryCodeError: "" }) : this.setState({ countryCodeError: "Country code is not valid" });
      if (/.+@.+\.[A-Za-z]+$/.test(`${emailAddress}`) && !isNaN(phoneNumber) && !isNaN(countryCode)) {

        // Check any user present with that email or not
        axios.post(
          path + "user/get-user-by-email",
          { "email": emailAddress },
          { headers: { "Content-Type": "application/json" } }
        ).then(serverRes => {
          const res = serverRes.data;
          if (!res.isError) {
            if (countryCode.slice(0, 1) == '+') {
              countryCode = countryCode.slice(1);
            }
            let newuserType = res.details && res.details.userType ? res.details.userType : cred.USER_TYPE_GUEST;
            let contactUserId = res.details && res.details.userId ? res.details.userId : "";
            let address = {
              userType: newuserType,
              name: name,
              emailAddress: emailAddress,
              phoneNumber: phoneNumber,
              countryCode: countryCode,
              contactUserId: contactUserId,
              "userId": userId
            }

            axios.post(
              path + "user/add_contact", address,
              { headers: { "Content-Type": "application/json" } }
            ).then(serverResponse => {
              console.log("Response from saveAddress : ", serverResponse.data);
              const res = serverResponse.data;
              if (!res.isError) {
                if (res.statuscode == 200) {
                  this.setState({ showSuccessAlert: true, successAlertMessage: 'Saved Successfully' }); // Showing successful alert
                  this.getAddressBook(userId);
                  this.hideAddAddress();
                  this.setState({ loading: false });
                } else if (res.statuscode != 200) {
                  this.setState({ showErrorAlert: true, errorAlertMessage: res.message }); // Showing successful alert
                  this.getAddressBook(userId);
                  this.hideAddAddress();
                  this.setState({ loading: false });
                }
                // TODO:: Show success message.
              } else {
                // TODO:: Show failure message.
                this.setState({ loading: false });
              }
            }).catch(error => {
              // TODO:: Show failure message.
              this.setState({ loading: false });
            });

          } else { // There is no user with that email.
            this.setState({ loading: false, emailAddressError: "server side error" });
          }
        }).catch(error => {
          // TODO:: Show failure message.
          this.setState({ loading: false });
        });

      }
      else {
        this.setState({ loading: false, });
      }

    }
  }

  openEditAddress = (index) => {

    let { contacts } = this.state;
    let contact = contacts[index];
    // console.log('index==============', index, contact);
    let { name, emailAddress, phoneNumber, countryCode, contactUserId, userType } = contact;

    // this.setState({ selectedId: index, showEditAddress: true, fname, lname, email, mobile });
    this.setState({ selectedId: index, showEditAddress: true, name, emailAddress, phoneNumber, countryCode, contactUserId, userType });

  }

  hideEditAddress() {

    this.setState({
      name: "",
      emailAddress: "",
      phoneNumber: "",
      countryCode: "",
      showEditAddress: false,
      selectedId: '',
      userType: '',
    })

  }

  editAddress() {
    let that = this;
    let { userId, selectedId, name, emailAddress, phoneNumber, countryCode, userType, loading } = this.state;
    let { contacts } = this.state;
    let contact = contacts[selectedId];
    // console.log("Contact---", contact);

    !name ? this.setState({ nameError: "Please enter a contact name." }) : this.setState({ nameError: "" });
    // !emailAddress ? this.setState({ emailAddressError: "Please enter an email address." }) : this.setState({ emailAddressError: "" });
    !phoneNumber ? this.setState({ phoneNumberError: "Please enter a phone number." }) : this.setState({ phoneNumberError: "" });
    !countryCode ? this.setState({ countryCodeError: "Please enter a country code." }) : this.setState({ countryCodeError: "" });
    if (countryCode && !isNaN(countryCode) && countryCode.slice(0, 1) == '+') {
      countryCode = countryCode.slice(1);
    }
    isNaN(phoneNumber) ? this.setState({ phoneNumberError: "Please enter valid phone number." }) : this.setState({ phoneNumberError: "" });
    isNaN(countryCode) ? this.setState({ countryCodeError: "Please enter valid country code." }) : this.setState({ countryCodeError: "" });
    if (userId && name && phoneNumber && countryCode && !loading && !isNaN(phoneNumber) && !isNaN(countryCode)) {
      this.setState({ loading: true });
      let editData = {
        "userId": userId,
        "addressBookId": contact._id,
        "name": name,
        "phoneNumber": phoneNumber,
        "emailAddress": emailAddress,
        "countryCode": countryCode,
        "userType": userType
      }

      // Calling API for updating user contact details-- 
      axios
        .post(path + "user/update_contact_details", editData,
          {
            headers: { "Content-Type": "application/json" }
          })
        .then(serverResponse => {
          console.log("Response from editAddress : ", serverResponse.data);
          const res = serverResponse.data;
          if (!res.isError) {
            that.setState({ showSuccessAlert: true, successAlertMessage: 'Successfully Updated' }); // Showing successful alert
            that.getAddressBook(userId);
            //Resetting all keys for edit and hiding the modal --
            that.hideEditAddress();
            // TODO:: Show success message.
            that.setState({ loading: false, showEditAddress: false });

          } else {
            // TODO:: Show failure message.
            this.setState({ loading: false });
          }
        })
        .catch(error => {
          this.setState({ loading: false });
        })

    }

  }

  deleteAddressConfirm(index) {
    this.setState({ selectedId: index, showDeleteAddress: true });
  }

  deleteAddress() {
    let that = this;
    let { selectedId, contacts, userId } = this.state;
    let selectedContact = contacts[selectedId]; // use array index as selectedId
    this.setState({ loading: true });

    let dataToSend = {
      "userId": userId,
      "addressBookId": selectedContact && selectedContact._id,
    };
    axios
      .post(path + "user/delete_contact_details", dataToSend, {
        headers: { "Content-Type": "application/json" }
      })
      .then(serverResponse => {
        console.log("Response from here : ", serverResponse.data);
        const res = serverResponse.data;
        if (!res.isError) {
          this.setState({
            loading: false
          }, () => {
            that.getAddressBook(userId);
            that.hideDeleteAddress();
          });

        } else {
          this.setState({ loading: false });
        }
      }).catch(error => {
        console.log(error);
      });
  }

  hideDeleteAddress() {
    this.setState({
      showDeleteAddress: false,
      selectedId: '',
    })
  }

  hideSuccessAlert() {
    this.setState({
      showSuccessAlert: false,
      successAlertMessage: '',
    })
  }

  hideErrorAlert() {
    this.setState({
      showErrorAlert: false,
      errorAlertMessage: '',
    })
  }

  searchAddress(event) {

    event.preventDefault();
    this.setState({ searching: true });
    let that = this;
    let { searchString, userId } = this.state;
    let contacts = [];
    let dataToSend = {
      "userId": userId,
      "searchText": searchString
    };

    axios
      .post(path + "user/search_contact_details", dataToSend, {
        headers: { "Content-Type": "application/json" }
      })
      .then(serverResponse => {
        console.log("Response from here : ", serverResponse.data);
        const res = serverResponse.data;
        if (!res.isError) {
          contacts = res.details.results;
          console.log(res.details.results);
          that.setState({ contacts });
        } else {

        }
      }).catch(error => {
        console.log(error);
      });
  }

  clearSearch(event) {
    event.preventDefault();
    let { userId } = this.state;
    this.getAddressBook(userId);
    this.setState({ searchString: '', searching: false });
  }

  sideMenuToggle() {
    let { openSideMenu } = this.state;
    openSideMenu = !openSideMenu;
    this.setState({ openSideMenu });
  }

  mobMenuClick() {
    this.sideMenuToggle();
    console.log('mobMenuClick');
  }

  /**
   * Utill methods --END
   */

  /**
   * IMPORT CSV --START
   */
  handleReadCSV = (data) => {
    if (data && data.errors && data.errors.length < 1) {
      let fileName = this.fileInput.current.files[0].name;
      let allContacts = [];
      let rawData = data.data;
      console.log('data==', data);
      rawData && Object.keys(rawData).map(index => {
        if (index > 0 && rawData[index].length == 4) {
          let rowArray = rawData[index];
          let newObject = {};
          rowArray && Object.keys(rowArray).map(i => {
            // if (rowArray[i]) {
            newObject[rawData[0][i]] = rowArray[i];
            // }
          });
          allContacts.push(newObject)
        }
      });
      console.log('allContacts==', allContacts);
      this.setState({ data: allContacts, fileName, showImportAddress: true });
    } else {
      this.setState({ importDataError: 'Please import a valid CSV.', showImportAddress: false });
    }
  }

  handleOnError = (err, file, inputElem, reason) => {
    console.log('err==', err, reason);
    this.setState({ importDataError: reason });
  }

  handleImportOffer = () => {

    this.setState({ importDataError: null });
    this.fileInput.current.click();
  }

  clearFileState = () => {
    this.setState({
      data: [],
      fileName: "",
      importDataError: "",
    });
  }

  saveContacts = () => {
    let { data, userId, EmailError } = this.state;
    let that = this;
    let isValid = true;
    if (data && data.length > 0) {
      let details = [];
      data.map((contact, index) => {
        if (contact.Country_Code && contact.Country_Code.slice(0, 1) == '+') {
          contact.Country_Code = contact.Country_Code.slice(1);
        }

        !contact.Email || !(/.+@.+\.[A-Za-z]+$/.test(`${contact.Email}`)) ? EmailError[index] = "Please enter valid email" : EmailError[index] = "";

        if (contact.Name && contact.Email && (/.+@.+\.[A-Za-z]+$/.test(`${contact.Email}`)) && contact.Country_Code && !isNaN(contact.Country_Code) && contact.Phone && !isNaN(contact.Phone)) {

          let detail = {};
          detail['name'] = contact.Name;
          detail['phoneNumber'] = contact.Phone;
          detail['emailAddress'] = contact.Email;
          detail['countryCode'] = contact.Country_Code;
          detail['userType'] = cred.USER_TYPE_GUEST;
          detail['contactUserId'] = '';
          details.push(detail);
        } else {
          isValid = false;
        }
      })
      this.setState({
        EmailError: EmailError
      });
      if (isValid) {
        // save in db
        let dataToSend = {
          "userId": userId,
          "details": details
        };
        this.setState({ loading: true });

        axios
          .post(path + "user/add-contact-multiple", dataToSend, {
            headers: { "Content-Type": "application/json" }
          })
          .then(serverResponse => {
            console.log("Response from here : ", serverResponse.data);
            const res = serverResponse.data;
            if (!res.isError) {
              this.setState({
                loading: false,
                showSuccessAlert: true,
                successAlertMessage: 'Successfully Updated'
              }, () => {
                that.getAddressBook(userId);
              });

            } else {
              // alert('Server error data not save.');
              this.setState({
                loading: false,
                showErrorAlert: true,
                errorAlertMessage: "Sorry! Something is wrong"
              });
            }
          }).catch(error => {
            console.log(error);
          });
        this.hideImportAddress();
      } else {
        // alert('Data not valid');
        this.setState({
          loading: false,
          showErrorAlert: true,
          errorAlertMessage: "Sorry! Something is wrong"
        });
      }
    } else {
      alert('No data found');
      this.hideImportAddress();
    }

  }


  hideImportAddress = () => {
    this.setState({ showImportAddress: false });
    this.clearFileState();
  }

  importDataChange = (index, value, name) => {
    let { data } = this.state;
    switch (name) {
      case 'Name11':

        break;

      case 'Email':
        let EmailError = [];
        EmailError[index] = "";
        data[index][name] = value;

        this.setState({
          data: data,
          EmailError: EmailError
        });
        break;

      default:
        data[index][name] = value;
        this.setState({ data });
        break;
    }
  }

  /**
   * IMPORT CSV --END
   */

  render() {

    let { contacts, loading, searching, pageNo, userRecords, countryCodeList, importDataError, fileName, data } = this.state;
    let headers = [];
    headers = [
      { label: "Name", key: 'name' },
      { label: "Country Code", key: 'countryCode' },
      { label: "Phone", key: 'phoneNumber' },
      { label: "Email", key: 'emailAddress' },
      { label: "User Type", key: 'userType' },
    ];

    return <main>
      <section className="user-mngnt-wrap">
        <div className="container-fluid">
          <div className="row">
            <Navbar routTo="/opmaddressbook" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />
            <div className="col-xl-10 col-lg-9 p-0 mob-full-conf">

              <div className="mobile-menu-header">
                {/* <a className="back-arw" onClick={() => console.log('<-BACK')}><i className="fas fa-arrow-left"></i></a> */}
                <a className="menu-tgl" onClick={() => this.sideMenuToggle()}><img src="images/menu-tgl.png" alt="" /></a>
              </div>

              <div className="mng-full-list">
                <div className="mng-full-srch">
                  <div className="row">
                    <div className="col-md-9">
                      <div className="srch-wrap">
                        <form>
                          <input value={this.state.searchString} onChange={(event) => this.setState({ searchString: event.target.value })} type="text" name="" className="form-control" placeholder="Search User" />
                          {
                            this.state.searching ?
                              <input type="button" onClick={(event) => this.clearSearch(event)} data-tip='Clear' />
                              :
                              <input type="submit" onClick={(event) => this.searchAddress(event)} data-tip='Search' />
                          }
                          <ReactTooltip
                            effect="float"
                            place="top"
                            data-border="true"
                          />
                        </form>
                      </div>
                    </div>
                    <div className="col-md-3">
                      <div className="add-user-btn">
                        <a href="JavaScript:Void(0);" className="btn" onClick={() => this.setState({ showAddAddress: true })}>Add Contact</a>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="mng-full-table">
                  <div className="row">
                    <div className="col-md-9">
                      <div className="mng-full-table-hdr">
                        <div className="row">
                          <div className="col-md-3 border-rt">
                            <h6>Name/Company</h6>
                          </div>
                          <div className="col-md-3 border-rt">
                            <h6>User Type</h6>
                          </div>
                          <div className="col-md-3 border-rt">
                            <h6>Email</h6>
                          </div>
                          <div className="col-md-3">
                            <h6>Phone</h6>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="col-md-3">
                      <div className="mng-full-table-hdr">
                        <div className="row">
                          <div className="col-md-6 border-rt">
                            <h6>Edit</h6>
                          </div>
                          <div className="col-md-6">
                            <h6>Delete</h6>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>

                  <div className="row">
                    <div className="col-md-9">

                      <div className="mng-full-table-row">
                        <div className="row">
                          <div className="col-md-4" style={{ textAlign: 'center' }}>
                            {fileName ?
                              <div style={{ textAlign: 'left', marginTop: '15px' }}>
                                <span style={{ color: "#64b563", fontSize: '20px' }} onClick={() => this.clearFileState()}>{fileName} <i className="fa fa-times-circle" aria-hidden="true" style={{ cursor: 'pointer', color: '#d04a4a' }}></i></span>
                              </div>
                              :
                              <>
                                <CSVReader
                                  onFileLoaded={this.handleReadCSV}
                                  inputRef={this.fileInput}
                                  style={{ display: 'none' }}
                                  onError={this.handleOnError}
                                />
                                <button className="btn btn-primary" style={{ margin: 5, width: '100%' }} onClick={this.handleImportOffer}>IMPORT CONTACT </button>
                                {importDataError && <span className="error-text">{importDataError}</span>}
                              </>
                            }
                          </div>
                          <div className="col-md-4" style={{ textAlign: 'center' }}>
                            {/* <a className="btn btn-primary" style={{ margin: 5, width: '100%' }} href="https://contactssampel.s3.us-east-2.amazonaws.com/Import_contacts_sample.csv">Download Sample</a> */}
                            <a className="btn btn-primary" style={{ margin: 5, width: '100%' }} target= "blank" href="/asset/Import_contacts_sample.csv">Download Sample</a>
                          </div>
                          <div className="col-md-4" style={{ textAlign: 'center' }}>
                            {/* <a href className="btn" onClick={() => this.setState({ showAddAddress: true })}>Add Contact</a> */}

                            {Object.keys(contacts).length == 0 ?
                              null
                              :
                              <CSVLink data={contacts} headers={headers} className="btn btn-primary" filename="MIO_Contacts.csv" style={{ margin: 5, width: '100%' }}>
                                EXPORT CONTACT
                              </CSVLink>
                            }
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>

                  {contacts && contacts.length > 0 && contacts.map((contact, index) => {
                    return <div className="row" key={index}>
                      <div className="col-md-9">
                        <div className="mng-full-table-row">
                          <div className="row">
                            <div className="col-md-3 border-rt">
                              <h6>Name/Company</h6>
                              <p className="textEllips">
                                <a href="JavaScript:Void(0);" onClick={() => this.openEditAddress(index)}>
                                  {contact.name}
                                </a>
                              </p>
                            </div>
                            <div className="col-md-3 border-rt">
                              <h6>User Type</h6>
                              <p className="textEllips">{contact.userType}</p>
                            </div>
                            <div className="col-md-3 border-rt"
                              // ref={ref => this.fooRef = ref}
                              data-tip={contact.emailAddress}
                            // onFocus={() => { ReactTooltip.show(this.fooRef) }}
                            >
                              <h6>Email</h6>
                              <p className="textEllips">
                                {contact.emailAddress}
                              </p>
                              <ReactTooltip
                                effect="float"
                                place="top"
                                data-border="true"
                              />
                            </div>
                            <div className="col-md-3">
                              <h6>Phone</h6>
                              <p className="textEllips">{`+${contact.countryCode} ${contact.phoneNumber}`}</p>
                            </div>

                            <div className="mobile-ad-edt-btns">
                              <ul>
                                <li onClick={() => { this.openEditAddress(index) }}>
                                  <a href="JavaScript:Void(0);"
                                    // ref={ref => this.fooRef = ref}
                                    data-tip='Edit/View'
                                  // onFocus={() => { ReactTooltip.show(this.fooRef) }}
                                  >
                                    <i className="far fa-edit"></i>
                                  </a>
                                  <ReactTooltip
                                    effect="float"
                                    place="top"
                                    data-border="true"
                                  />
                                </li>

                                <li onClick={() => this.deleteAddressConfirm(index)}>
                                  <a href="JavaScript:Void(0);"
                                    data-toggle="modal"
                                    data-target="#delModal"
                                    // ref={ref => this.fooRef = ref}
                                    data-tip='Delete'
                                  // onFocus={() => { ReactTooltip.show(this.fooRef) }}
                                  >
                                    <i className="far fa-trash-alt"></i>
                                  </a>
                                  <ReactTooltip
                                    effect="float"
                                    place="top"
                                    data-border="true"
                                  />
                                </li>

                              </ul>
                            </div>
                          </div>
                        </div>

                      </div>

                      <div className="col-md-3">
                        <div className="mng-full-table-row add-edt text-center">
                          <div className="row">
                            <div className="col-md-6">
                              <a href="JavaScript:Void(0);"
                                onClick={() => this.openEditAddress(index)}
                                // ref={ref => this.fooRef = ref}
                                data-tip='Edit/View'
                              // onFocus={() => { ReactTooltip.show(this.fooRef) }}
                              >
                                <i className="far fa-edit"></i>
                              </a>
                              <ReactTooltip
                                effect="float"
                                place="top"
                                data-border="true"
                              />
                            </div>
                            <div className="col-md-6">
                              <a href="JavaScript:Void(0);"
                                onClick={() => this.deleteAddressConfirm(index)}
                                // ref={ref => this.fooRef = ref}
                                data-tip='Delete'
                              // onFocus={() => { ReactTooltip.show(this.fooRef) }}
                              >
                                <i className="far fa-trash-alt"></i>
                              </a>
                            </div>
                            <ReactTooltip
                              effect="float"
                              place="top"
                              data-border="true"
                            />
                          </div>
                        </div>
                      </div>

                    </div>
                  })
                  }

                </div>

                <br /><br />
                {!searching ?
                  <div className="form-group">
                    {!loading && parseFloat(pageNo) > 1 ?
                      <span style={{ color: "#007bff", cursor: "pointer" }}
                        onClick={() => { this.pageChangePrev() }}
                      >
                        <b>{"<< Prev"}</b>
                      </span>
                      :
                      null}
                      &nbsp;&nbsp;|&nbsp;&nbsp;
                      {!loading && (parseFloat(pageNo) < parseFloat(userRecords)) ?
                      <span style={{ color: "#007bff", cursor: "pointer" }}
                        onClick={() => { this.pageChangeNext() }}
                      >
                        <b>{"Next >>"}</b>
                      </span>
                      :
                      null}
                    {loading ?
                      <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                        <i className="fa fa-spinner fa-spin" ></i>
                      </a>
                      :
                      null}

                  </div>
                  :
                  null}

              </div>
            </div>
          </div>
        </div>
      </section>

      <Modal show={this.state.showAddAddress} onHide={() => this.hideAddAddress()}>
        <Modal.Header closeButton>
          <Modal.Title>Add new Contact</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="row">

            <div className="col-md-12">
              <div className="form-group">
                <label>Name:</label>
                <input
                  type="text"
                  name="name"
                  value={this.state.name}
                  className="form-control"
                  placeholder="Enter Name"
                  onChange={event => this.setState({ name: event.target.value })}
                  onFocus={() => this.setState({ nameError: "" })}
                />
                <span style={{ color: 'red' }}>{this.state.nameError ? `* ${this.state.nameError}` : ''}</span>
              </div>
            </div>

            <div className="col-md-12">
              <div className="form-group">
                <label>Email Address:</label>
                <input
                  type="email"
                  name="usrEmail"
                  value={this.state.emailAddress}
                  className="form-control"
                  placeholder="Enter Email Address"
                  onChange={event => this.setState({ emailAddress: event.target.value })}
                  onFocus={() => this.setState({ emailAddressError: "" })}
                />
                <span style={{ color: 'red' }}>{this.state.emailAddressError ? `* ${this.state.emailAddressError}` : ''}</span>
              </div>
            </div>

            <div className="col-md-5">
              <div className="form-group">
                <label>Country Code:</label>
                <select
                  className="form-control"
                  name="countryCode"
                  value={this.state.countryCode}
                  onChange={event => this.setState({ countryCode: event.target.value })}
                  onFocus={() => this.setState({ countryCodeError: "" })}
                >
                  <option value="">Select country</option>
                  {countryCodeList && countryCodeList.map((data, index) => {
                    return <option key={index} value={data.code}>{data.code + ' ' + data.name}</option>
                  })}
                </select>
                <span style={{ color: 'red' }}>{this.state.countryCodeError ? `* ${this.state.countryCodeError}` : ''}</span>
              </div>
            </div>

            <div className="col-md-7">
              <div className="form-group">
                <label>Mobile Number:</label>
                <input
                  type="tel"
                  name="usrMobile"
                  value={this.state.phoneNumber}
                  className="form-control"
                  placeholder="Enter Mobile Number"
                  onChange={event => this.setState({ phoneNumber: event.target.value })}
                  onFocus={() => this.setState({ phoneNumberError: "" })}
                />
                <span style={{ color: 'red' }}>{this.state.phoneNumberError ? `* ${this.state.phoneNumberError}` : ''}</span>
              </div>
            </div>

          </div>
        </Modal.Body>
        <Modal.Footer>

          {!loading ?
            <Button variant="primary" onClick={(event) => this.saveAddress(event)}> Save </Button>
            :
            null
          }
          {loading ?
            <Button variant="primary" >
              <i className="fa fa-spinner fa-spin" ></i>
            </Button>
            :
            null
          }
        </Modal.Footer>
      </Modal>

      <Modal show={this.state.showEditAddress} onHide={() => this.hideEditAddress()}>
        <Modal.Header closeButton>
          <Modal.Title>Edit address</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="row">

            <div className="col-md-12">
              <div className="form-group">
                <label>Name:</label>
                <input
                  type="text"
                  name="usrFname"
                  value={this.state.name}
                  className="form-control"
                  placeholder="Enter Contact Name"
                  onChange={event => this.setState({ name: event.target.value })}
                  onFocus={() => this.setState({ nameError: "" })}
                />
                <span style={{ color: 'red' }}>{this.state.nameError ? `* ${this.state.nameError}` : ''}</span>
              </div>
            </div>

            <div className="col-md-12">
              <div className="form-group">
                <label>Email Address:</label>
                <input
                  type="email"
                  name="usrEmail"
                  value={this.state.emailAddress}
                  className="form-control"
                  placeholder="Enter Email Address"
                  onChange={event => this.setState({ emailAddress: event.target.value })}
                  disabled
                />
              </div>
            </div>

            <div className="col-md-5">
              <div className="form-group">
                <label>Country Code:</label>
                <select
                  className="form-control"
                  name="countryCode"
                  value={this.state.countryCode}
                  onChange={event => this.setState({ countryCode: event.target.value })}
                  onFocus={() => this.setState({ countryCodeError: "" })}
                >
                  <option value="">Select country</option>
                  {countryCodeList && countryCodeList.map((data, index) => {
                    return <option value={data.code}>{data.code + ' ' + data.name}</option>
                  })}
                </select>
                <span style={{ color: 'red' }}>{this.state.countryCodeError ? `* ${this.state.countryCodeError}` : ''}</span>
              </div>
            </div>

            <div className="col-md-7">
              <div className="form-group">
                <label>Mobile Number:</label>
                <input
                  type="tel"
                  name="usrMobile"
                  value={this.state.phoneNumber}
                  className="form-control"
                  placeholder="Enter Mobile Number"
                  onChange={event => this.setState({ phoneNumber: event.target.value })}
                  onFocus={() => this.setState({ phoneNumberError: "" })}
                />
                <span style={{ color: 'red' }}>{this.state.phoneNumberError ? `* ${this.state.phoneNumberError}` : ''}</span>
              </div>
            </div>

          </div>
        </Modal.Body>
        <Modal.Footer>

          {!this.state.loading ?
            <Button variant="primary" onClick={(event) => this.editAddress(event)}>
              Save
            </Button>
            :
            null}
          {this.state.loading ?
            <Button variant="primary" >
              <i className="fa fa-spinner fa-spin" ></i>
            </Button>
            :
            null}
        </Modal.Footer>
      </Modal>

      <Modal show={this.state.showDeleteAddress} onHide={() => this.hideDeleteAddress()}>
        <Modal.Header closeButton>
          <Modal.Title>Delete address confirm</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h3>Are you sure?</h3>
        </Modal.Body>
        <Modal.Footer>
          {!loading ?
            <Button variant="secondary" onClick={() => this.deleteAddress()}>
              Yes
            </Button>
            :
            null
          }
          {loading ?
            <Button variant="primary" >
              <i className="fa fa-spinner fa-spin" ></i>
            </Button>
            :
            null
          }
          <Button variant="primary" onClick={() => this.hideDeleteAddress()}>
            No
          </Button>

        </Modal.Footer>
      </Modal>

      <Modal show={this.state.showSuccessAlert} onHide={() => this.hideSuccessAlert()}>
        <Modal.Header closeButton>
          <Modal.Title>OK</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h5 style={{ color: 'green' }}>{`${this.state.successAlertMessage}`}</h5>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="primary" onClick={() => this.hideSuccessAlert()}>
            Ok
          </Button>
        </Modal.Footer>
      </Modal>

      <Modal show={this.state.showErrorAlert} onHide={() => this.hideErrorAlert()}>
        <Modal.Header closeButton>
          <Modal.Title>OK</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h5 style={{ color: 'red' }}>{`${this.state.errorAlertMessage}`}</h5>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="primary" onClick={() => this.hideErrorAlert()}>
            Ok
          </Button>
        </Modal.Footer>
      </Modal>

      <Modal size="lg" show={this.state.showImportAddress} onHide={() => this.hideImportAddress()}>
        <Modal.Header closeButton>
          <Modal.Title>Verify Contacts</Modal.Title>
        </Modal.Header>
        <Modal.Body>

          {this.state.data && this.state.data.length > 0 ?
            this.state.data.map((contact, index) => {
              return <div className='row' key={index}>
                {console.log(index)}
                <div className='col-md-3'>
                  <div className="form-group">
                    <label>Name</label>
                    <input
                      type='text'
                      className="form-control"
                      value={data[index]['Name']}
                      onChange={e => this.importDataChange(index, e.target.value, 'Name')}
                    />
                    {!data[index]['Name'] ? <span style={{ color: 'red' }}>*Enter a name</span> : null}
                  </div>
                </div>
                <div className='col-md-3'>
                  <div className="form-group">
                    <label>Email</label>
                    <input
                      type='text'
                      className="form-control"
                      value={data[index]['Email']}
                      onChange={e => this.importDataChange(index, e.target.value, 'Email')}
                    />
                    {!data[index]['Email'] ? <span style={{ color: 'red' }}>*Enter an Email</span> : null}
                    {data[index]['Email'] ? <span style={{ color: 'red' }}>{this.state.EmailError[index] ? `* ${this.state.EmailError[index]}` : ''}</span> : null}
                  </div>
                </div>
                <div className='col-md-3'>
                  <div className="form-group">
                    <label>Country_Code</label>
                    <select
                      className="form-control"
                      name="Country_Code"
                      value={data[index]['Country_Code']}
                      // onChange={event => this.setState({ countryCode: event.target.value })}
                      onChange={e => this.importDataChange(index, e.target.value, 'Country_Code')}
                    >
                      <option value="">Select country</option>
                      {countryCodeList && countryCodeList.map((data, index) => {
                        return <option value={data.code}>{data.code + ' ' + data.name}</option>
                      })}
                    </select>
                    {!data[index]['Country_Code'] || isNaN(data[index]['Country_Code']) ? <span style={{ color: 'red' }}>*Enter valid country code</span> : null}
                  </div>
                </div>
                <div className='col-md-3'>
                  <div className="form-group">
                    <label>Phone</label>
                    <input
                      type='text'
                      className="form-control"
                      value={data[index]['Phone']}
                      onChange={e => this.importDataChange(index, e.target.value, 'Phone')}
                    />
                    {!data[index]['Phone'] || isNaN(data[index]['Phone']) ? <span style={{ color: 'red' }}>*Enter a valid phone no.</span> : null}
                  </div>
                </div>
              </div>
            })
            :
            <div className='rew'>
              <div className='col-md-12' style={{ textAlign: 'center' }}>Data not selected</div>
            </div>
          }
        </Modal.Body>
        <Modal.Footer>
          {!loading ?
            <Button variant="primary" onClick={() => this.saveContacts()}>
              Save contacts
            </Button>
            :
            null
          }
          {loading ?
            <Button variant="primary" >
              <i className="fa fa-spinner fa-spin" ></i>
            </Button>
            :
            null}
        </Modal.Footer>
      </Modal>

    </main>
  }
}

export default checkAuthentication(OpmAddressBook);