import React, { Component } from 'react'
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import cred from "../../cred.json";
import routeList from "../../routeList.json";
import Axios from 'axios';
import _ from 'lodash';
let path = cred.API_PATH;


class Packages extends Component {
  constructor(props) {
    super(props);
    this.state = {
      companyId: '',
      companyName: '',
      freePackage: '',
      startupPackage: '',
      enterprisePackage: '',
    };
  }

  componentDidMount() {
    document.body.classList.remove("sign-bg");
    document.body.classList.add("grey-bg");

    let storage = localStorage.getItem("MIO_Local");
    storage = storage ? JSON.parse(storage) : null;
    if (storage && storage.data.userId && storage.data.companyId && !storage.data.companyType) {
      this.setState({
        companyId: storage.data.companyId,
        companyName: storage.data.companyName
      }, () => {
        this.getPackageDetails();
      });
    } else {
      this.props.history.push("/");
    }
  }

  getPackageDetails = () => {
    this.setState({ loading: true });
    Axios
      .post(path + 'content-management/get-pricing-plans')
      .then(serverResponse => {

        let res = serverResponse.data;
        if (!res.isError) {
          // console.log("all pricing package details ===", res);

          let freePackage = res.plans && res.plans[0] && res.plans[0].contents;
          let startupPackage = res.plans && res.plans[1] && res.plans[1].contents;
          let enterprisePackage = res.plans && res.plans[2] && res.plans[2].contents;

          let freePackageId = res.plans && res.plans[0] && res.plans[0]._id;
          let startupPackageId = res.plans && res.plans[1] && res.plans[1]._id;
          let enterprisePackageId = res.plans && res.plans[2] && res.plans[2]._id;

          this.setState({
            loading: false,
            freePackage,
            startupPackage,
            enterprisePackage
          });

        } else {
          this.setState({
            loading: false
          })
        }

      })
      .catch(error => {
        console.log(error);
      })

  }

  selectPackage(packgId) {
    let that = this;
    // TODO: save company type in db
    let filterArr = _.filter(cred.PACKAGES, { "id": packgId });
    if (filterArr && filterArr.length > 0) {
      let selectedPackage = filterArr[0];
      let data = {
        "companyId": this.state.companyId,
        "companyName": this.state.companyName,
        "companyType": selectedPackage.codeName
      }
      Axios.post(
        path + "user/update-company-details", data,
        { headers: { "Content-Type": "application/json" } }
      ).then(serverResponse => {
        const res = serverResponse.data;
        if (!res.isError) {
          let storage = localStorage.getItem("MIO_Local");
          storage = storage ? JSON.parse(storage) : null;
          storage.data.companyType = selectedPackage.codeName;
          localStorage.setItem("MIO_Local", JSON.stringify(storage));

          that.updateBaseService(selectedPackage.codeName);

          // if (packgId == 'P001') {
          // redirect to home page
          this.props.history.push("/");
          // } else {
          //   // redirect to payment page
          //   this.props.history.push(routeList['OTHER']['addPayment']['to']);
          // }
        } else {
          alert('something goes wrong');
        }
      }).catch(error => {
        alert('something goes wrong');
      });
    } else {
      alert('invalid package.')
    }

  }

  updateBaseService = (companyType) => {

    let storage = localStorage.getItem("MIO_Local");
    storage = storage ? JSON.parse(storage) : null;

    if (storage.data) {
      let dataToSend = {
        "companyId": storage.data.companyId,
        "companyType": companyType
      };
      // API - 
      Axios
        .post(path + 'services/add-base-services-to-company', dataToSend)
        .then(serverResponse => {
          let res = serverResponse.data;
          if (!res.isError) {

            console.log("updateBaseService---", res);

          }
        })
        .catch(error => {
          console.log(error);
        })

    }
  }

  render() {
    let settings = {
      className: "center",
      centerMode: true,
      infinite: true,
      centerPadding: "60px",
      slidesToShow: 3,
      speed: 500,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            initialSlide: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    };
    let { freePackage, startupPackage, enterprisePackage, } = this.state;
    return <main>
      <section className="lft-rt-padd common-padd pricing-wrap-outer">
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-12">
              <div className="all-pkgs">
                <div className="logobox"><img src="images/logo-sign.png" alt="" /></div>
                <h4>Sign Up with our <span className="startr">Starter</span> or <span className="entrprs">Enterprise</span> Package to enjoy all features.</h4>
                {/* <div className="owl-carousel owl-theme pkgs"> */}
                <Slider {...settings}>
                  {cred.PACKAGES && cred.PACKAGES.map((packg, key) => {
                    return <div className="item" key={key}>
                      <div className={"pkg-box-wrap " + packg.boxWrapClass}>
                        <div className="pkg-box" style={packg.style}>
                          <h2>{packg.name}<small>{packg.description}</small></h2>
                          {/* <div className="price">{packg.unitSign}{packg.price}</div>
                          <h4>Features:</h4> */}
                          {packg.codeName == "FREE_TYPE" ?
                            <ol>
                              {freePackage && freePackage.map((featur, ind) => {
                                return <li key={ind}>{featur}</li>
                              })}
                            </ol>
                            : packg.codeName == "STARTUP" ?
                              <ol>
                                {startupPackage && startupPackage.map((featur, ind) => {
                                  return <li key={ind}>{featur}</li>
                                })}
                              </ol>
                              : packg.codeName == "ENTERPRISE" ?
                                <ol>
                                  {enterprisePackage && enterprisePackage.map((featur, ind) => {
                                    return <li key={ind}>{featur}</li>
                                  })}
                                </ol>
                                : null}
                        </div>
                        <a href="javaScript:void(0)" className="btn" onClick={() => this.selectPackage(packg.id)}><i className="fas fa-sign-in-alt"></i> {packg.buttonText}</a>
                      </div>
                    </div>

                  })
                  }

                  {/* <div className="item">
                    <div className="pkg-box-wrap free">
                      <div className="pkg-box" style={{ backgroundImage: "url(images/price-box-bg.png)" }}>
                        <h2>Free<small>For 7 days</small></h2>
                        <div className="price">$00.00</div>
                        <h4>Features:</h4>
                        <ol>
                          <li>Lorem ipsum dolor sit amet</li>
                          <li>Lorem ipsum dolor sit amet</li>
                          <li>Lorem ipsum dolor sit amet</li>
                          <li>Lorem ipsum dolor sit amet</li>
                          <li>Lorem ipsum dolor sit amet</li>
                        </ol>
                      </div>
                      <a href className="btn" onClick={() => this.selectPackage()}><i className="fas fa-sign-in-alt"></i> Start Now</a>
                    </div>
                  </div>
                  <div className="item">
                    <div className="pkg-box-wrap starter">
                      <div className="pkg-box" style={{ backgroundImage: "url(images/price-box-bg.png)" }}>
                        <h2>Starter</h2>
                        <div className="price">$25.00</div>
                        <h4>Features:</h4>
                        <ol>
                          <li>Lorem ipsum dolor sit amet</li>
                          <li>Lorem ipsum dolor sit amet</li>
                          <li>Lorem ipsum dolor sit amet</li>
                          <li>Lorem ipsum dolor sit amet</li>
                          <li>Lorem ipsum dolor sit amet</li>
                        </ol>
                      </div>
                      <a href="#" className="btn"><i className="fas fa-sign-in-alt"></i> Start Now</a>
                    </div>
                  </div>
                  <div className="item">
                    <div className="pkg-box-wrap enterprise">
                      <div className="pkg-box" style={{ backgroundImage: "url(images/price-box-bg.png)" }}>
                        <h2>Enterprise</h2>
                        <div className="price">$50.00</div>
                        <h4>Features:</h4>
                        <ol>
                          <li>Lorem ipsum dolor sit amet</li>
                          <li>Lorem ipsum dolor sit amet</li>
                          <li>Lorem ipsum dolor sit amet</li>
                          <li>Lorem ipsum dolor sit amet</li>
                          <li>Lorem ipsum dolor sit amet</li>
                        </ol>
                      </div>
                      <a href="#" className="btn"><i className="fas fa-sign-in-alt"></i> Start Now</a>
                    </div>
                  </div>
                 */}
                </Slider>
                {/* </div> */}
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
  }
}

export default Packages;