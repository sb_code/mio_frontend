import React, { Component } from 'react'
import Navbar from '../Component/Navbar'
import PaystackButton from 'react-paystack';
import StripeCheckout from 'react-stripe-checkout';
import { PayPalButton } from "react-paypal-button-v2";
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button } from "react-bootstrap"
import Table from 'react-bootstrap/Table'
import _ from 'lodash';
import ReactTooltip from 'react-tooltip'; // For tool tip 
import { checkAuthentication } from '../Component/Authentication';
import BounceLoader from "react-spinners/BounceLoader";
import { decode } from '../../util/converter';
import Swal from 'sweetalert2';

var path = cred.API_PATH;

export class PaymentManagement extends Component {
	constructor(props) {
		super(props)
		this.state = {
			userId: '',
			companyType: '',
			companyId: '',

			openSideMenu: false,

			pageLoading: true,
			loading: false,
			downloading: {},

			invoices: [],
			transactions: [],
			selectedTransaction: {},
			viewTransaction: false,

			selectedMonth: '',
			selectedYear: '',
			viewUsageModal: false,
			selectedUsageDetails: '',

			showAddGateWay: false,
			gateway: '',
			gatewayError: false,
			currency: '',
			currencyError: false,
			gatewayStatus: false,

			walletBalence: '',
			minBalance: '',
			paymentGateway: '',
			defaultCurrency: '',
			stripeCustomerId: '',
			paystackCustomerId: '',
			// amount: 1000, //equals NGN10,
			amount: '', //equals NGN10,
			email: "subhendu.sett@pkweb.in",  // customer email
			selectedInvoiceId: '',

			paystackKey: decode(cred.PAYSTACK.publicKey), //PAYSTACK PUBLIC KEY
			stripeKey: decode(cred.STRIPE.publicKey), //Stripe PUBLIC KEY
			paypalKey: decode(cred.PAYPAL.publicKey), //Paypal PUBLIC KEY

			rechargeModal: false,
			selectedPaymentGateWay: '',
			rechargeAmount: '',
			changeAmount: 0,

		}
	}

	componentDidMount() {
		document.body.classList.remove("sign-bg");
		document.body.classList.add("grey-bg");

		// 
		this.checkCookie();

		let storage = localStorage.getItem("MIO_Local");
		storage = storage ? JSON.parse(storage) : null;
		if (storage && storage.data.userId) {
			this.setState({
				userId: storage.data.userId,
				companyType: storage.data.companyType,
				companyId: storage.data.companyId,
				companyName: storage.data.companyName,
				email: storage.data.email,
				pageLoading: true,
			}, async () => {
				// Fetch company details-- set paymentGateway, set defaultCurrency
				await this.getWalletDetails();
				// monthly invoice payment details or Recharge history / transaction details
				/**
				 * if company type is enterprise
				 * -- get invoice list for last 12 month
				 * else if company type is start up
				 * -- get last 12 transactions
				 */
				if (this.state.companyType == cred.COMPANY_TYPE_ENTERPRISE) {
					await this.getUserPaymentDetails();
				} else if (this.state.companyType == cred.COMPANY_TYPE_STARTUP) {
					await this.getUserTransactionDetails();
				}
				this.setState({ pageLoading: false })
			});
		}
	}

	//#region for check cookie
	checkCookie = async () => {
		let cookieEnabled = navigator.cookieEnabled;
		if (!cookieEnabled) {
			document.cookie = "mtest";
			cookieEnabled = document.cookie.indexOf("mtest") != -1;
		}
		console.log('cookieEnabled', cookieEnabled);
		if (!cookieEnabled) {
			Swal.fire("Please enabled cookie. Otherwise payment will not works");
		} else {
			Swal.fire("Please enabled cookie. Otherwise payment will not works");
		}
	}
	//#endregion for check cookie

	// getting payment details of user
	getUserPaymentDetails = () => {

		return new Promise((resolve, reject) => {

			let dataToSend = {
				"companyId": this.state.companyId,
				"year": new Date().getFullYear()
			}
			// API / ToDo
			axios
				.post(path + 'price-management/fetch-invoice-companyId', dataToSend)
				.then(serverResponse => {

					let res = serverResponse.data;
					if (!res.isError) {

						let result = res.details;
						this.setState({
							invoices: result,
							loading: false
						}, () => resolve());
					} else {
						resolve();
					}

				})
				.catch(error => {
					console.log(error);
					resolve();
				})
		})


	};

	getUserTransactionDetails = () => {
		return new Promise((resolve, reject) => {
			let { transactions } = this.state;
			let dataToSend = {
				"companyId": this.state.companyId,
				"page": 0,
				"perPage": 10
			}
			// API ToDo -- 
			axios
				.post(path + 'price-management/get-transaction-list-By-CompanyId', dataToSend)
				.then(serverResponse => {
					let res = serverResponse.data;
					if (!res.isError) {

						let details = res.details && res.details[0];
						// console.log("Transaction details -- ", res);
						this.setState({
							transactions: details ? details.results : transactions,
						}, () => {
							console.log("transactions===", this.state.transactions);
							resolve();
						});

					} else {
						resolve();
					}
				})
				.catch(error => {
					console.log(error);
					resolve();
				})
		})

	}

	getWalletDetails = () => {
		let that = this;
		return new Promise((resolve, reject) => {
			let dataToSend = {
				companyId: this.state.companyId
			};
			// API - -
			axios
				.post(path + 'user/get-company-by-id', dataToSend)
				.then(serverResponse => {
					let res = serverResponse.data;
					if (!res.isError) {
						let wallet = res.details && res.details.walletBalence;
						let minBalance = res.details && res.details.minBalance;
						let gateway = res.details && res.details.paymentGateway;
						let currency = res.details && res.details.currency;
						let paystackCustomerId = res.details && res.details.paystackCustomerId;
						let stripeCustomerId = res.details && res.details.stripeCustomerId;
						that.setState({
							walletBalence: wallet,
							minBalance: minBalance,
							paymentGateway: gateway,
							defaultCurrency: currency,
							stripeCustomerId,
							paystackCustomerId,
						}, () => resolve())

					} else {
						resolve();
					}
				})
				.catch(error => {
					console.log(error);
					resolve();
				})
		});


	}


	// For viewing invoice -- 
	viewInvoice = (index) => {

		let { invoices } = this.state;
		let invoice = invoices[index];
		let _id = invoice._id;

		// this.props.history && this.props.history.push(`/invoice/${1}`);
		this.props.history.push({
			pathname: '/invoice/' + `${_id}`,
			state: {
				date: invoice.date,
				company: this.state.companyName,
				month: invoice.month,
				year: invoice.year
			}
		});

	}

	closeUsageModal = () => {

		this.setState({
			selectedMonth: '',
			selectedYear: '',
			viewUsageModal: false,
			selectedUsageDetails: {}
		})

	}

	viewHistory = (index) => {

		let { transactions } = this.state;
		let selectedTransaction = transactions[index];
		this.setState({
			selectedTransaction: selectedTransaction,
			viewTransaction: true
		});

	}

	// For viewing the monthly used services details - -
	viewUtilization = (event) => {

		this.props.history.push({
			pathname: '/usedservices',
			state: {
				"name": this.state.companyName,
				"type": this.state.companyType
			}
		});

	}

	saveGatewayForm = (e) => {
		e.preventDefault();

		let { currency, gateway, gatewayError, currencyError } = this.state;
		!currency ? this.setState({ currencyError: true }) : this.setState({ currencyError: false })
		!gateway ? this.setState({ gatewayError: true }) : this.setState({ gatewayError: false })

		if (currency && gateway) {
			let dataToSend = {
				"companyId": this.state.companyId,
				"companyType": this.state.companyType,
				"paymentGateway": gateway,
				"currency": currency
			};
			// API --
			axios
				.post(path + 'user/update-company-details', dataToSend)
				.then(serverResponse => {
					let res = serverResponse.data;
					if (!res.isError) {

						this.setState({
							showAddGateWay: false,
							gateway: ''
						}, () => {
							this.getWalletDetails();
						});

					} else {
						alert("Something is wrong...");
					}
				})
				.catch(error => {
					console.log(error);
				})
		} else {
			// this.setState({

			// })
			alert("Something is wrong...");
		}

	}

	// Getting company Details -- 
	getCompanyDetails = (companyId) => {
		return new Promise((resolve, reject) => {

			let dataToSend = {
				"companyId": companyId
			};
			axios
				.post(path + 'user/get-company-by-id', dataToSend)
				.then(serverResponse => {

					let res = serverResponse.data;
					if (!res.isError) {

						let address = res.details && res.details.address;
						let gateway = res.details && res.details.paymentGateway;
						let currency = res.details && res.details.currency;
						let invoiceBillingInfo = res.details && res.details.invoiceBillingInfo;
						let vat = res.details && res.details.vat;

						let returnData = {};
						returnData.address = address;
						returnData.gateway = gateway;
						returnData.currency = currency;
						returnData.invoiceBillingInfo = invoiceBillingInfo;
						returnData.vat = vat;
						console.log("returnData 1 ==", returnData);
						resolve(returnData);
					} else {
						reject();
					}

				})
				.catch(error => {
					console.log(error);
				})

		})
	}

	// getting invoice details -- 
	getInvoiceeDetails = (returnData, invoiceId) => {
		return new Promise((resolve, reject) => {

			let dataToSend = {
				"invoiceId": invoiceId,
			};
			axios
				.post(path + 'price-management/fetch-invoice-By-Id', dataToSend)
				.then(serverResponse => {

					let res = serverResponse.data;
					if (!res.isError) {
						let invoiceDetails = res.details[0];
						let services = invoiceDetails.services;
						// Total amount calculation - 
						let total = 0;
						services && services.map(value => {
							total = value.totalAmount && (parseFloat(value.totalAmount) + parseFloat(total)).toFixed(2);
						})
						let issueDate = invoiceDetails && invoiceDetails.date;

						returnData.invoiceDetails = invoiceDetails;
						returnData.invoiceServices = services;
						returnData.totalAmount = total;
						returnData.issueDate = this.formatDate(issueDate);

						console.log("returnData 2 -- ", returnData);
						resolve(returnData);

					} else {
						reject();
					}
				})
		})
	}

	// For getting calculations fo summary table --
	calculationForSummary = (usageDetails) => {

		return new Promise((resolve, reject) => {

			let archiveList = usageDetails.archiveList;
			let webCallList = usageDetails.webCallList;
			let sipCallList = usageDetails.sipCallList;

			let archiveListArr = [];
			let webCallListArr = [];
			let sipCallListArr = [];

			let archiveCount = 0;
			let webCallCount = 0;
			let sipCallCount = 0;

			// For calculation of archive list --
			if (archiveList && archiveList.length > 0) {
				let sessionArr = [];
				for (let i = 0; i < archiveList.length; i++) {
					if (sessionArr.includes(archiveList[i].sessionId)) {
						// let commonSession = _.filter(archiveListArr, {"sessionId": archiveList[i].sessionId})[0];
						archiveListArr.map((data, index) => {
							if (data.sessionId == archiveList[i].sessionId) {
								data.duration = parseFloat(data.duration) + parseFloat(archiveList[i].archiveDetail.duration);
								data.cost = parseFloat(data.cost) + parseFloat(archiveList[i].archiveDetail.cost);
								data.count = parseInt(data.count) + 1;
							}
						});
					} else {
						sessionArr.push(archiveList[i].sessionId);
						archiveListArr.push({
							"sessionId": archiveList[i].sessionId,
							"duration": parseFloat(archiveList[i].archiveDetail.duration),
							"cost": parseFloat(archiveList[i].archiveDetail.cost),
							"count": 1
						});

					}


				}

			}


			// For calculating web call list -- 
			if (webCallList && webCallList.length > 0) {
				let sessionArr = [];
				for (let i = 0; i < webCallList.length; i++) {
					if (sessionArr.includes(webCallList[i].sessionId)) {
						// let commonSession = _.filter(archiveListArr, {"sessionId": archiveList[i].sessionId})[0];
						webCallListArr.map((data, index) => {
							if (data.sessionId == webCallList[i].sessionId) {
								data.duration = parseFloat(data.duration) + parseFloat(webCallList[i].webCallDetail.duration);
								data.cost = parseFloat(data.cost) + parseFloat(webCallList[i].webCallDetail.cost);
								data.count = parseInt(data.count) + 1;
							}
						});
					} else {
						sessionArr.push(webCallList[i].sessionId);
						webCallListArr.push({
							"sessionId": webCallList[i].sessionId,
							"duration": parseFloat(webCallList[i].webCallDetail.duration),
							"cost": parseFloat(webCallList[i].webCallDetail.cost),
							"count": 1
						});

					}


				}

			}


			// For calculating  sip call list -- 
			if (sipCallList && sipCallList.length > 0) {
				let sessionArr = [];
				for (let i = 0; i < sipCallList.length; i++) {
					if (sessionArr.includes(sipCallList[i].sessionId)) {
						// let commonSession = _.filter(archiveListArr, {"sessionId": archiveList[i].sessionId})[0];
						sipCallListArr.map((data, index) => {
							if (data.sessionId == sipCallList[i].sessionId) {
								data.duration = parseFloat(data.duration) + parseFloat(sipCallList[i].sipCallDetail.duration);
								data.Cost = parseFloat(data.Cost) + parseFloat(sipCallList[i].sipCallDetail.Cost);
								data.count = parseInt(data.count) + 1;
							}
						});
					} else {
						sessionArr.push(sipCallList[i].sessionId);
						sipCallListArr.push({
							"sessionId": sipCallList[i].sessionId,
							"duration": parseFloat(sipCallList[i].sipCallDetail.duration),
							"Cost": parseFloat(sipCallList[i].sipCallDetail.Cost),
							"count": 1
						});

					}


				}

			}

			resolve({
				"archiveListArr": archiveListArr,
				"webCallListArr": webCallListArr,
				"sipCallListArr": sipCallListArr
			});


		})

	}

	// For date formatting -- 
	formatDate = (string) => {

		// new Intl.DateTimeFormat('en-US', {year: 'numeric', month: '2-digit',day: '2-digit', hour: '2-digit', minute: '2-digit', second: '2-digit'}).format(value.date)

		let options = { year: 'numeric', month: 'long', day: 'numeric' };
		return new Date(string).toLocaleDateString([], options);

	}

	handleCurrencyChange = (e) => {

		if (e.target.value != '') {
			this.setState({
				currency: e.target.value,
				gatewayStatus: true
			});
		} else {
			this.setState({
				currency: e.target.value
			});
		}

	}

	clearState = (event) => {

		this.setState({
			selectedTransaction: {},
			viewTransaction: false,
			amount: '',
			showAddGateWay: false,
			currency: '',
			gateway: '',
			currencyError: '',
			gatewayError: '',
			gatewayStatus: false
		});

	}

	// For payment of any month from list -- 
	paymentFromList = (index) => {
		console.log(index);
	}

	openRechargeModal = (paymentGateway) => {

		this.setState({
			rechargeModal: true,
			selectedPaymentGateWay: paymentGateway
		});

	}

	hideRechargeModal = (event) => {

		this.setState({
			rechargeModal: false,
			selectedPaymentGateWay: '',
			amount: '',
			changeAmount: 0,
		})

	}

	changeRechargeAmount = (event) => {

		if (event.target.value && !isNaN(event.target.value)) {
			let changeAmount = parseFloat(event.target.value);
			let amount = changeAmount * 100;
			this.setState({
				amount,
				changeAmount,
			});
		} else {
			this.setState({
				amount: '',
				changeAmount: 0,
			});
		}

	}

	sideMenuToggle() {
		let { openSideMenu } = this.state;
		openSideMenu = !openSideMenu;
		this.setState({ openSideMenu });
	}

	mobMenuClick() {
		this.sideMenuToggle();
		console.log('mobMenuClick');
	}

	//#region for paystack payment
	paystackCallback = (response, referenceCode, amount) => {
		let that = this;
		console.log('response', response); // card charged successfully, get reference here
		/*
		SUCCESS RESPONSE
		{
			reference: "oumynPScNCp1ysY"
			trans: "481047572"
			status: "success"
			message: "Approved"
			transaction: "481047572"
			trxref: "oumynPScNCp1ysY"
		}
		*/

		axios.post(`${cred.API_PATH}payment/paystack-payment`,
			{
				description: that.state.companyType == cred.USER_TYPE_ENTERPRISE ? "Pay for invoice" : "Recharge",
				currency: that.state.defaultCurrency,
				amount: amount,
				userId: this.state.userId,
				companyId: this.state.companyId,
				companyType: this.state.companyType,
				referenceCode: referenceCode,
				data: response
			})
			.then(async (responseData) => {
				console.log('responseData == ', responseData);
				// if (!responseData.data.isError) {

				// }
				if (that.state.companyType == cred.COMPANY_TYPE_ENTERPRISE) {
					await that.getUserPaymentDetails();
				} else if (that.state.companyType == cred.COMPANY_TYPE_STARTUP) {
					await that.getUserTransactionDetails();
					await that.getWalletDetails();
					that.hideRechargeModal();
				}
			})
			.catch(err => {
				alert('Some thing goes wrong');
			});
	}

	paystackClose = () => {
		console.log("Payment closed");
	}

	getReference = () => {
		//you can put any unique reference implementation code here
		let text = "";
		let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-.=";
		for (let i = 0; i < 15; i++)
			text += possible.charAt(Math.floor(Math.random() * possible.length));
		return text;
	}

	//#region for Stripe payment
	onToken = (token, referenceCode, amount) => {
		let that = this;
		console.log('Stripe token == ', token, referenceCode);
		axios.post(`${cred.API_PATH}payment/stripe-payment`,
			{
				description: that.state.companyType == cred.USER_TYPE_ENTERPRISE ? "Pay for invoice" : "Recharge",
				source: token.id,
				currency: that.state.defaultCurrency,
				amount: amount,
				customerId: that.state.stripeCustomerId,
				userId: that.state.userId,
				companyId: that.state.companyId,
				companyType: that.state.companyType,
				referenceCode: referenceCode, //'INVID0001'
			})
			.then(serverResponse => {
				let res = serverResponse.data;
				if (!res.isError) {
					if (that.state.companyType == cred.COMPANY_TYPE_ENTERPRISE) {
						that.getUserPaymentDetails();
					} else if (that.state.companyType == cred.COMPANY_TYPE_STARTUP) {
						that.getUserTransactionDetails();
						that.getWalletDetails();
						that.hideRechargeModal();
					}
				} else {
					alert("Sorry payment can't be done.");
				}
			})
			.catch();
	}

	//#reagion for paypal payment
	openPaypalPaymentModal() {
		let that = this;
		that.setState({
			paypalModalOpen: true
		});
	}

	closePaypalPaymentModal() {
		let that = this;
		that.setState({ paypalModalOpen: false, amount: '', selectedInvoiceId: '' }, () => {
			that.hideRechargeModal();
		})
	}

	paypalCallbackSuccess = (response) => {
		let that = this;
		console.log('response====', response); // card charged successfully, get reference here
		/*
		SUCCESS RESPONSE
		{
			reference: "oumynPScNCp1ysY"
			trans: "481047572"
			status: "success"
			message: "Approved"
			transaction: "481047572"
			trxref: "oumynPScNCp1ysY"
		}
		*/

		axios.post(`${cred.API_PATH}payment/paypal-payment`,
			{
				description: that.state.companyType == cred.USER_TYPE_ENTERPRISE ? "Pay for invoice" : "Recharge",
				currency: that.state.defaultCurrency,
				amount: that.state.amount,
				userId: that.state.userId,
				companyId: that.state.companyId,
				companyType: that.state.companyType,
				referenceCode: that.state.selectedInvoiceId,//'INVID0001'
				data: response
			})
			.then(() => {
				that.closePaypalPaymentModal();
				if (that.state.companyType == cred.COMPANY_TYPE_ENTERPRISE) {
					that.getUserPaymentDetails();
				} else if (that.state.companyType == cred.COMPANY_TYPE_STARTUP) {
					that.getUserTransactionDetails();
					that.getWalletDetails();
				}
			})
			.catch(err => {
				console.log('in error', err);
			});
	}

	paypalCallbackError = (error) => {
		console.log('error - response', error);
		alert('Paypal pyment error:', error);
		/*
		FALURE RESPONSE
		{
			reference: "oumynPScNCp1ysY"
			trans: "481047572"
			status: "success"
			message: "Approved"
			transaction: "481047572"
			trxref: "oumynPScNCp1ysY"
		}
		*/

	}



	render() {
		let { companyType, companyName, walletBalence, minBalance, paymentGateway, defaultCurrency, invoices, transactions, selectedTransaction,
			selectedMonth, selectedYear, viewUsageModal, selectedUsageDetails, selectedPaymentGateWay, pageLoading } = this.state;

		const monthNames = ["January", "February", "March", "April", "May", "June",
			"July", "August", "September", "October", "November", "December"
		];

		return <main>
			{pageLoading ?
				<div className="sweet-loading">
					<BounceLoader
						css={`
						display: block;
						margin: 0 auto;
						border-color: #0092DD;
					`}
						size={150}
						color={"#0092DD"}
						loading={pageLoading}
					/>
				</div>
				:
				<section className="user-mngnt-wrap">
					<div className="container-fluid">
						<div className="row">
							<Navbar routTo="/paymentmanagement" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />
							<div className="col-xl-10 col-lg-9 p-0 mob-full-conf">

								<div className="mobile-menu-header">
									{/* <a className="back-arw" onClick={() => console.log('<-BACK')}><i className="fas fa-arrow-left"></i></a> */}
									<a href="JavaScript:Void(0);" className="menu-tgl" onClick={() => this.sideMenuToggle()}><img src="images/menu-tgl.png" alt="" /></a>
								</div>

								<div className="payment-mng-wrap">

									{companyType == cred.COMPANY_TYPE_STARTUP ?
										<div className="paymnt-mng-hdr">
											<div className="blnc">
												<div className="ic-box"><img src="images/wallet-ic-white.png" alt="" /></div>
												<h4>Balance: {walletBalence ? walletBalence : '0.00'} <strong>{defaultCurrency ? defaultCurrency : ''}</strong></h4>
											</div>
											<div className="rchrg-btn">
												{paymentGateway ?
													<Button varient="primary" onClick={() => this.openRechargeModal(paymentGateway)}>Recharge</Button>
													:
													null}
											</div>
										</div>
										:
										<div className="paymnt-mng-hdr">
											<div className="blnc">
												<div className="logobox"><img src="/images/logo-sign.png" alt="" /></div>
											</div>
											<div className="rchrg-btn">
												<p className="text-right" style={{ margin: "0" }}>
													Company Name: {companyName}
													<br />Company Type: {companyType}
												</p>
											</div>
										</div>
									}

									<div className="row">
										{companyType == cred.COMPANY_TYPE_STARTUP ?
											<div className="col-md-4">
												<div >
													<a href="JavaScript:Void(0);" className="btn" onClick={(event) => this.viewUtilization(event)}>View service utilization</a>
												</div>
											</div>
											:
											null}
									</div>

									{companyType == cred.COMPANY_TYPE_STARTUP ?
										<div className="mng-full-table">

											<div className="row">
												<div className="col-md-10">
													<div className="mng-full-table-hdr">
														<div className="row">
															<div className="col-md-6 border-rt">
																<h6>Date</h6>
															</div>
															<div className="col-md-6">
																<h6>Amount</h6>
															</div>

														</div>
													</div>
												</div>

												<div className="col-md-2">
													<div className="mng-full-table-hdr">
														<div className="row">
															<div className="col-md-12">
																<h6>View</h6>
															</div>

														</div>
													</div>
												</div>
											</div>

											{transactions && transactions.length > 0 && transactions.map((value, index) => {
												return <div className="row" key={index}>

													<div className="col-md-10">

														<div className="mng-full-table-row">
															<div className="row">
																<div className="col-md-6 border-rt">
																	<h6>Date</h6>
																	<p>
																		<a href="JavaScript:Void(0);" onClick={() => { this.viewHistory(index) }}>
																			{this.formatDate(value.createdAt)}
																		</a>
																	</p>
																</div>
																<div className="col-md-6">
																	<h6>Amount</h6>
																	<p className="text-uppercase">{parseFloat(value.amount) / 100}</p>
																</div>

															</div>
														</div>

													</div>

													<div className="col-md-2">
														<div className="mng-full-table-row mng-rt text-center">
															<div className="row">

																<div className="col-md-12">
																	<a href="JavaScript:Void(0);"
																		data-tip='View History'
																		onClick={() => { this.viewHistory(index) }}
																		className="btn pay-btn"
																	>
																		<i className="fas fa-eye" />
																	</a>
																	<ReactTooltip
																		globalEventOff="click"
																		effect="float"
																		place="top"
																		data-border="true"
																	/>
																</div>

															</div>
														</div>
													</div>

												</div>
											})}

										</div>
										: (companyType == cred.COMPANY_TYPE_ENTERPRISE) ?
											<div className="mng-full-table">

												<div className="row">
													<div className="col-md-8">
														<div className="mng-full-table-hdr">
															<div className="row">
																<div className="col-md-4 border-rt">
																	<h6>Date</h6>
																</div>
																<div className="col-md-4 border-rt">
																	<h6>Amount</h6>
																</div>
																<div className="col-md-4">
																	<h6>Status</h6>
																</div>
															</div>
														</div>
													</div>

													<div className="col-md-4">
														<div className="mng-full-table-hdr">
															<div className="row">

																<div className="col-md-6 border-rt">
																	<h6>View</h6>
																</div>
																<div className="col-md-6">
																	<h6>Action</h6>
																</div>

															</div>
														</div>
													</div>
												</div>

												{invoices && invoices.length > 0 && invoices.map((value, index) => {
													return (value.isPublished ?
														<div className="row" key={index}>
															<div className="col-md-8">

																<div className="mng-full-table-row">
																	<div className="row">
																		<div className="col-md-4 border-rt">
																			<h6>Date</h6>
																			<p>
																				<a href="JavaScript:Void(0);" onClick={() => { this.viewInvoice(index) }}>
																					{monthNames[value.month - 1] + '-' + value.year}
																				</a>
																			</p>
																		</div>
																		<div className="col-md-4 border-rt">
																			<h6>Amount</h6>
																			<p className="text-uppercase">{value.vatAddedTotal ? parseFloat(value.vatAddedTotal).toFixed(2) : 0.0}</p>
																		</div>
																		<div className="col-md-4 ">
																			<h6>Status</h6>
																			<p className="text-uppercase">{value.isPaid ? "Paid" : "Due"}</p>
																		</div>
																	</div>
																</div>

															</div>

															<div className="col-md-4">
																<div className="mng-full-table-row mng-rt text-center">
																	{value.isPaid ?
																		<div className="row">
																			<div className="col-md-6">
																				<a href="JavaScript:Void(0);"
																					data-tip='View Invoice'
																					onClick={() => { this.viewInvoice(index) }}
																					className="btn pay-btn"
																				>
																					<i className="fas fa-eye" />
																				</a>
																				<ReactTooltip
																					globalEventOff="click"
																					effect="float"
																					place="top"
																					data-border="true"
																				/>
																			</div>

																			<div className="col-md-6">

																				<a href={value.invoiceUrl}
																					target="blank"
																					className="btn dwnld-btn"
																					data-tip='Download Invoice'
																					style={{ background: "#0083C6", color: "#fff" }}
																				>
																					<i className="fas fa-download"></i>
																				</a>
																				<ReactTooltip
																					globalEventOff="click"
																					effect="float"
																					place="top"
																					data-border="true"
																				/>
																			</div>

																		</div>
																		:
																		<div className="row">
																			<div className="col-md-6">
																				<a href="JavaScript:Void(0);"
																					data-tip='View Invoice'
																					onClick={() => { this.viewInvoice(index) }}
																					className="btn pay-btn"
																				>
																					<i className="fas fa-eye" />
																				</a>
																				<ReactTooltip
																					globalEventOff="click"
																					effect="float"
																					place="top"
																					data-border="true"
																				/>
																			</div>

																			{(paymentGateway && paymentGateway == cred.PAYMENT_GATEWAY.PAYSTACK) ?
																				<div className="col-md-6">
																					<PaystackButton
																						text="PAY"
																						class="btn"
																						callback={(response) => this.paystackCallback(response, value._id, Number(value.vatAddedTotal ? parseFloat(value.vatAddedTotal).toFixed(2) : 0.0) * 100)}
																						close={this.paystackClose}
																						disabled={false}
																						embed={false}
																						reference={value._id}
																						email={this.state.email}
																						amount={Number(value.vatAddedTotal ? parseFloat(value.vatAddedTotal).toFixed(2) : 0.0) * 100}
																						paystackkey={this.state.paystackKey}
																						tag="button"
																					/>
																				</div>
																				: (paymentGateway && paymentGateway == cred.PAYMENT_GATEWAY.STRIPE) ?
																					<div className="col-md-6">
																						<StripeCheckout
																							token={(token) => this.onToken(token, value._id, Number(value.vatAddedTotal ? parseFloat(value.vatAddedTotal).toFixed(2) : 0.0) * 100)}
																							stripeKey={this.state.stripeKey}
																							currency={this.state.defaultCurrency}
																							amount={Number(value.vatAddedTotal ? parseFloat(value.vatAddedTotal).toFixed(2) : 0.0) * 100}
																							email={this.state.email}
																						>
																							<a href="JavaScript:void(0);" className="btn"><i className="far fa-credit-card"></i> PAY</a>
																						</StripeCheckout>
																					</div>
																					: (paymentGateway && paymentGateway == cred.PAYMENT_GATEWAY.PAYPAL) ?
																						<div className="col-md-6">
																							<a href="JavaScript:void(0);" className="btn" onClick={() => { this.setState({ amount: Number(value.vatAddedTotal ? parseFloat(value.vatAddedTotal).toFixed(2) : 0.0) * 100, selectedInvoiceId: value._id }, () => { this.openPaypalPaymentModal() }) }}><i className="far fa-credit-card"></i> PAY</a>
																						</div>
																						:
																						<div className="col-md-6">
																							<a href="JavaScript:Void(0);" className="btn pay-btn"><i className="far fa-credit-card"></i> Pay</a>
																						</div>
																			}

																		</div>
																	}
																</div>
															</div>

														</div>
														: null)

												})}


											</div>

											: null}

								</div>

							</div>

						</div>

					</div>

				</section>
			}

			<Modal show={this.state.paypalModalOpen} onHide={() => this.closePaypalPaymentModal()}>
				<Modal.Header closeButton>

				</Modal.Header>
				<Modal.Body>
					{/* {console.log("=====t===t=", this.state.amount, this.state.defaultCurrency)} */}
					<PayPalButton

						amount={this.state.amount / 100}
						currency={this.state.defaultCurrency}
						shippingPreference="NO_SHIPPING" // default is "GET_FROM_FILE"
						options={
							{ currency: this.state.defaultCurrency, clientId: this.state.paypalKey }
						}
						onSuccess={(details) => {
							// alert("Transaction completed by " + details.payer.name.given_name);

							// Call server to save the transaction
							this.paypalCallbackSuccess(details)
							// return fetch("/paypal-transaction-complete", {
							// 	method: "post",
							// 	body: JSON.stringify({
							// 		orderID: data.orderID
							// 	})
							// });
						}}
						onError={error => {
							this.paypalCallbackError(error)
						}}

						onButtonReady={data => {
							console.log('onButtonReady====', data);
						}}

						catchError={data => {
							console.log('catchError=====', data);
						}}
					/>
				</Modal.Body>
			</Modal>

			<Modal show={this.state.viewTransaction} onHide={(event) => { this.clearState(event) }}>
				<Modal.Header closeButton>
					<Modal.Title> View transaction details </Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<div className="row">

						{selectedTransaction.referenceCode && <div className="col-md-6">
							<div className="form-group">
								<label>Reference Code:</label>

								<input
									type="text"
									name="noOfSeats"
									value={selectedTransaction.referenceCode}
									className="form-control"
									readOnly={true}
								/>
							</div>
						</div>}

						<div className="col-md-6">
							<div className="form-group">
								<label>Amount:</label>
								<input
									type="text"
									name="noOfSeats"
									value={parseFloat(selectedTransaction.amount / 100).toFixed(2)}
									className="form-control"
									placeholder="Enter amount"
									readOnly={true}
								/>

							</div>
						</div>

						<div className="col-md-6">
							<div className="form-group">
								<label>Currency:</label>
								<input
									type="text"
									name="pricePerSeat"
									value={selectedTransaction.currency}
									className="form-control"
									placeholder="Enter Currency"
									readOnly={true}
								/>

							</div>
						</div>

						<div className="col-md-6">
							<div className="form-group">
								<label>Payment Gateway:</label>
								<input
									type="text"
									name="pricePerSeat"
									value={selectedTransaction.paymentGateway}
									className="form-control"
									placeholder="Enter Gateway"
									readOnly={true}
								/>

							</div>
						</div>

						{/* <div className="col-md-6">
                                <div className="form-group">
                                    <label>Billing Address:</label>
                                    <input
                                        type="text"
                                        name="pricePerSeat"
                                        value={selectedTransaction.paymentGatewayData.billing_details.address.name}
                                        className="form-control"
                                        placeholder="Enter Address"
                                        readOnly= {true}
                                    />
                                    
                                </div>
                            </div> */}

					</div>
				</Modal.Body>
				<Modal.Footer>
					<Button variant="primary" onClick={(event) => { this.clearState(event) }}>
						Cancel
                        </Button>
				</Modal.Footer>
			</Modal>

			<Modal show={this.state.viewUsageModal} size="lg" onHide={() => { this.closeUsageModal() }}>
				<Modal.Header closeButton>
					<Modal.Title>Usage Details</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<div className="row">

						<div className="col-md-12">
							<div className="form-group">
								<span>Invoice of : {monthNames[selectedMonth - 1]}, {selectedYear}</span>
							</div>
						</div>

						<div className="col-md-12">
							<div className="form-group">
								<span>Archive List : </span>
							</div>
						</div>

						<div className="col-md-12">
							<Table striped bordered hover size="sm">
								<thead>
									<tr>
										<th>Archive Name</th>
										<th>Duration</th>
										<th>Cost</th>
									</tr>
								</thead>
								<tbody>
									{selectedUsageDetails && selectedUsageDetails.archiveList && selectedUsageDetails.archiveList.map((value, index) => {
										return <tr key={index}>
											<td>{value.name}</td>
											<td>{value.duration}</td>
											<td>{value.price}</td>
										</tr>
									})}
								</tbody>
								<tfoot>
									<tr>
										<td>Total Archive: {selectedUsageDetails.totalArchive}</td>
										<td>Total Duration: {selectedUsageDetails.totalArchiveDuration}</td>
										<td>Total Cost: {selectedUsageDetails.totalArchiveCost}</td>
									</tr>
								</tfoot>

							</Table>
						</div>

						<div className="col-md-12">
							<div className="form-group">
								<span>Web Call : </span>
							</div>
						</div>

						<div className="col-md-12">
							<Table striped bordered hover size="sm">
								<thead>
									<tr>
										<th>Web Call Name</th>
										<th>Duration</th>
										<th>Cost</th>
									</tr>
								</thead>
								<tbody>
									{selectedUsageDetails && selectedUsageDetails.webCallList && selectedUsageDetails.webCallList.map((value, index) => {
										return <tr key={index}>
											<td>{value.name}</td>
											<td>{value.duration}</td>
											<td>{value.price}</td>
										</tr>
									})}
								</tbody>
								<tfoot>
									<tr>
										<td>Total Web Call: {selectedUsageDetails.totalWebCall}</td>
										<td>Total Duration: {selectedUsageDetails.totalWebCalDuration}</td>
										<td>Total Cost: {selectedUsageDetails.totalWebCalCost}</td>
									</tr>
								</tfoot>

							</Table>
						</div>

						<div className="col-md-12">
							<div className="form-group">
								<span>SIP Call : </span>
							</div>
						</div>

						<div className="col-md-12">
							<Table striped bordered hover size="sm">
								<thead>
									<tr>
										<th>SIP Name</th>
										<th>Duration</th>
										<th>Cost</th>
									</tr>
								</thead>
								<tbody>
									{selectedUsageDetails && selectedUsageDetails.sipCallList && selectedUsageDetails.sipCallList.map((value, index) => {
										return <tr key={index}>
											<td>{value.name}</td>
											<td>{value.duration}</td>
											<td>{value.price}</td>
										</tr>
									})}
								</tbody>
								<tfoot>
									<tr>
										<td>Total SIP Call: {selectedUsageDetails.totalSipCall}</td>
										<td>Total Duration: {selectedUsageDetails.totalSipCalDuration}</td>
										<td>Total Cost: {selectedUsageDetails.totalSipCalCost}</td>
									</tr>
								</tfoot>

							</Table>
						</div>

					</div>
				</Modal.Body>
				<Modal.Footer>
					<Button variant="primary" onClick={() => this.closeUsageModal()}>
						Cancel
                    </Button>
				</Modal.Footer>
			</Modal>

			<Modal show={this.state.showAddGateWay} onHide={(event) => { this.clearState(event) }}>
				<Modal.Header closeButton>
					<Modal.Title> Add payment gateway </Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<div className="row">

						<div className="col-md-6">
							<div className="form-group">
								<label>Select Currency:</label>
								<select name="gateway" value={this.state.currency}
									className="form-control"
									onChange={(e) => { this.handleCurrencyChange(e) }}
									onFocus={() => { this.setState({ currencyError: '' }) }}
								>
									<option value="">Select a currency</option>
									<option value={cred.CURRENCY.usd}>{cred.CURRENCY.usd}</option>
									<option value={cred.CURRENCY.gbp}>{cred.CURRENCY.gbp}</option>
									<option value={cred.CURRENCY.naira}>{cred.CURRENCY.naira}</option>
									<option value={cred.CURRENCY.euro}>{cred.CURRENCY.euro}</option>
								</select>
								<span style={{ color: "red" }}>{this.state.currencyError ? '*Select currency' : ''}</span>
							</div>
						</div>
						<div className="col-md-6">
							<div className="form-group">
								<label>Select Gateway:</label>
								<select name="gateway" value={this.state.gateway}
									className="form-control"
									onChange={(e) => { this.setState({ gateway: e.target.value }) }}
									onFocus={() => { this.setState({ gatewayError: '' }) }}
									disabled={!this.state.gatewayStatus}
								>
									<option value="">Select Any Gateway</option>
									{this.state.currency && this.state.currency == cred.CURRENCY.naira ?
										<>
											<option value={cred.PAYMENT_GATEWAY.PAYSTACK}>{cred.PAYMENT_GATEWAY.PAYSTACK}</option>
										</>
										:
										<>
											<option value={cred.PAYMENT_GATEWAY.STRIPE}>{cred.PAYMENT_GATEWAY.STRIPE}</option>
											<option value={cred.PAYMENT_GATEWAY.PAYPAL}>{cred.PAYMENT_GATEWAY.PAYPAL}</option>
										</>
									}
								</select>
								<span style={{ color: "red" }}>{this.state.gatewayError ? '*Select gateway' : ''}</span>
							</div>
						</div>

					</div>
				</Modal.Body>
				<Modal.Footer>
					<Button variant="primary" onClick={(event) => { this.saveGatewayForm(event) }}>
						Save
                    </Button>
				</Modal.Footer>
			</Modal>

			<Modal show={this.state.rechargeModal} onHide={(event) => { this.hideRechargeModal(event) }}>
				<Modal.Header closeButton>
					<Modal.Title> Give recharge amount </Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<div className="row">

						<div className="col-md-6">
							<div className="form-group">
								<label>Recharge amount:</label>
								<input
									type="text"
									className="form-control"
									value={this.state.changeAmount}
									onChange={(event) => { this.changeRechargeAmount(event) }}
								/>
								<span style={{ color: "red" }}>{this.state.amount == '' || parseFloat(this.state.amount) < 1000 ? '*Give an amount (>10)' : ''}</span>
							</div>
						</div>

					</div>
				</Modal.Body>
				<Modal.Footer>
					{/* <Button variant="primary" onClick={(event) => { this.saveGatewayForm(event) }}>
						Save
                    </Button> */}
					{this.state.amount && parseFloat(this.state.amount) >= 1000 ?
						<div>
							{(selectedPaymentGateWay && selectedPaymentGateWay == cred.PAYMENT_GATEWAY.PAYSTACK) ?
								<PaystackButton
									text="Recharge"
									class="btn"
									callback={(response) => this.paystackCallback(response, '', this.state.amount)}
									close={this.paystackClose}
									disabled={false}
									embed={false}
									reference={this.getReference()}
									email={this.state.email}
									amount={this.state.amount}
									paystackkey={this.state.paystackKey}
									tag="button"
								/>
								: (selectedPaymentGateWay && selectedPaymentGateWay == cred.PAYMENT_GATEWAY.STRIPE) ?
									<StripeCheckout
										token={(token) => this.onToken(token, '', this.state.amount)}
										stripeKey={this.state.stripeKey}
										currency={this.state.defaultCurrency}
										amount={this.state.amount}
										email={this.state.email}
									>
										<a href="JavaScript:void(0);" className="btn"><i className="far fa-credit-card"></i> Recharge</a>
									</StripeCheckout>
									: (selectedPaymentGateWay && selectedPaymentGateWay == cred.PAYMENT_GATEWAY.PAYPAL) ?
										<a href="JavaScript:void(0);" className="btn" onClick={() => this.openPaypalPaymentModal()}><i className="far fa-credit-card"></i> Recharge</a>
										:
										null}
						</div>
						:
						null}

				</Modal.Footer>
			</Modal>


		</main >

	}

}

export default checkAuthentication(PaymentManagement);