import React, { Component } from "react";
import { Link } from 'react-router-dom';
import Navbar from "../Component/Navbar";
import ReactTooltip from 'react-tooltip'; // For tool tip
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button } from "react-bootstrap"
import _ from 'lodash';
import { CSVLink, CSVDownload } from "react-csv";

import { checkAuthentication } from '../Component/Authentication';

var path = cred.API_PATH;

export class CompanyOperator extends Component {
  constructor(props) {
    super(props);
    this.state = {

      userId: '',
      openSideMenu: false,

      searching: false,
      searchString: '',

      loading: false,

      countryCodeList: [],
      users: [],
      userRecords: '',
      conferenceList: [],
      pageNo: '',

      showUser: false,
      viewUser: false,
      showConference: false,
      checkList: {},
      deleteItem: '',
      deletingId: '',

      fname: '',
      fnameError: '',
      lname: '',
      lnameError: '',
      companyId: '',
      companyIdError: '',
      userType: '',
      userTypeError: '',
      email: '',
      emailError: '',
      mobile: '',
      mobileError: '',
      countryCode: '',
      companyType: '',
      companyTypeError: '',
      service: '',
      serviceError: '',

      selectedUserId: '',
      selectedConference: {},
      selectedConferenceId: '',

      showSuccessAlert: false,
      successAlertMessage: '',
      showErrorAlert: false,
      errorAlertMessage: ''

    }
    this.perPage = 10;
  }

  componentDidMount() {
    document.body.classList.remove("sign-bg");
    document.body.classList.add("grey-bg");

    let storage = localStorage.getItem("MIO_Local");
    storage = storage ? JSON.parse(storage) : null;
    if (storage && storage.data.userId) {
      this.setState({
        userId: storage.data.userId,
        companyType: storage.data.companyType,
        companyId: storage.data.companyId
      }, () => {
          this.getUserList();
          this.getCountryCodeList();
        });
      // this.getUserList();
    }

  }

  sideMenuToggle() {
    let { openSideMenu } = this.state;
    openSideMenu = !openSideMenu;
    this.setState({ openSideMenu });
  }

  getUserList = () => {
    this.setState(prevState => ({ loading: true }));

    let dataToSend = {
      "companyId": this.state.companyId,
      "createdBy": this.state.userId,
      "userType": cred.USER_TYPE_COMPANY_OPERATOR,
      "page": this.state.pageNo,
      "perPage": this.perPage
    };

    // API to get users -- 
    axios
      .post(path + 'user-management/get-user-usertype-basis', dataToSend)
      .then(serverResponse => {
        let res = serverResponse.data;
        if (!res.isError) {
          // console.log("response from get-user-usertype-basis -- ", res);
          let result = res.details && res.details.response && res.details.response[0] && res.details.response[0].results ;
          let totalRows = res.details && res.details.response && res.details.response[0] && res.details.response[0].noofpage ;
          this.setState({
            users: result,
            userRecords: totalRows,
            loading: false
          });

        } else {
          this.setState(prevState => ({ loading: false }));
        }
      })
      .catch(error => {
        console.log(error);
      })

  }

  getCountryCodeList = ()=> {

    axios
      .post(path + 'services/get-country-codes')
      .then(serverResponse => {

        let res = serverResponse.data;
        if (!res.isError) {
          this.setState({
            countryCodeList: res.details
          });
        }

      })
      .catch(error => {
        console.log(error);
      })

  }

  addNewUser = () => {

    let { loading, fname, lname, companyId, email, mobile, countryCode, companyType, service } = this.state;

    !fname ? this.setState({ fnameError: "Please enter a fname." }) : this.setState({ fnameError: "" });
    !lname ? this.setState({ lnameError: "Please enter a lname." }) : this.setState({ lnameError: "" });
    (/.+@.+\.[A-Za-z]+$/.test(`${email}`)) ? this.setState({ emailError: "" }) : this.setState({ emailError: "Please enter an valid email address." });
    (mobile != '' && !isNaN(mobile)) ? this.setState({ mobileError: "" }) : this.setState({ mobileError: "Please enter a phone number." });
    (countryCode != '' && !isNaN(countryCode)) ? this.setState({ countryCodeError: "" }) : this.setState({ countryCodeError: "Please enter a country code." });
    !service ? this.setState({ serviceError: "Please select a service." }) : this.setState({ serviceError: "" });

    if (fname && lname && (/.+@.+\.[A-Za-z]+$/.test(`${email}`)) && !isNaN(mobile) && mobile != '' && !isNaN(countryCode) && countryCode != '' && companyId && service) {

      this.setState(prevState => ({ loading: true }));
      if (countryCode.slice(0, 1) == '+') {
        countryCode = countryCode.slice(1);
      }

      let dataToSend = {
        'fname': fname,
        'lname': lname,
        'email': email,
        'countryCode': countryCode,
        'mobile': mobile,
        'service': service,
        'availablity': 'Available',
        'isAssigned': false,
        'companyId': companyId,
        'userType': cred.USER_TYPE_COMPANY_OPERATOR,
        'companyType': companyType,
        'createdBy': this.state.userId
      };
      // console.log(dataToSend);

      //For saving new user -- 
      axios
        .post(path + 'user-management/add-new-user', dataToSend)
        .then(serverResponse => {
          let res = serverResponse.data;
          if (!res.isError && res.statuscode == 200) {
            // console.log(res);
            this.setState({
              loading: false,
              showUser: false,
              showSuccessAlert: true,
              successAlertMessage: 'Successfully added.',
            });

            this.getUserList();
            // this.clearState();

            // alert('Password is: ' + res.details.password);
          } else {

            // this.clearState();
            this.setState({
              loading: false,
              showUser: false,
              showErrorAlert: true,
              errorAlertMessage: res.message
            });
          }
        })
        .catch(error => {
          console.log(error);
          this.setState(prevState => ({ loading: false }));
        })

    } else {
      this.setState(prevState => ({ loading: false }));
    }
  }

  searchUsers = (event) => {
    event.preventDefault();
    if(!this.state.searchString || (this.state.searchString && this.state.searchString.length < 2) ){
      alert("Please Use valid search");
    }else{
      
      this.setState(prevState => ({ searching: true }));

      let dataToSend = {
        "userName": this.state.searchString,
        "companyId": this.state.companyId,
        "createdBy": this.state.userId,
        "userType": cred.USER_TYPE_COMPANY_OPERATOR,
        "page": '',
        "perPage": ''
      };

      console.log("dataToSend--", dataToSend);
      // API for searching -- 
      axios
        .post(path + 'user-management/search-user-name-basis', dataToSend)
        .then(serverResponse => {
          let res = serverResponse.data;
          if (!res.isError) {
            console.log(res);
            if (res.response) {
              this.setState(prevState => ({
                searching: true,
                users: res.response
              }));
            }
          } else {
            this.setState(prevState => ({ searching: false }));
          }
        })
        .catch(error => {
          console.log(error);
        })

    
    }

  }

  clearSearch(event) {
    event.preventDefault();
    // let { userId } = this.state;
    this.setState(prevState => ({
      searchString: '',
      searching: false
    }));
    this.getUserList();
  }

  userAssign = (index) => {

    let { loading, users } = this.state;
    let user = users[index];
    let { fname, lname, isAssigned, userId } = user;
    this.setState({loading: true});
    let dataToSend = {
      "companyId": this.state.companyId
    };
    // API -- 
    axios
      .post(path + 'conference/list-conference-company', dataToSend)
      .then(serverResponse => {
        let res = serverResponse.data;
        if (!res.isError) {
          let conferenceList = res.details && res.details[0] && res.details[0].results;
          this.setState({
            loading: false,
            conferenceList: conferenceList,
            showConference: true,
            isAssigned: isAssigned,
            selectedUserId: userId
          });
        }
      })
      .catch(error => {
        console.log(error);
      })


  }

  userUnassign = (index) => {

    let { loading, users } = this.state;
    let user = users[index];
    let { fname, lname, isAssigned, userId, assignedToConference } = user;

    let dataToSend = {
      "userId": userId,
      "conferenceId": assignedToConference,
      "companyId": this.state.companyId
    };
    // API -- 
    axios
      .post(path + 'operator-management/un-assign-operator-from-company', dataToSend)
      .then(serverResponse => {
        let res = serverResponse.data;
        if (!res.isError) {
          this.setState({
            showSuccessAlert: true,
            successAlertMessage: "Successfully un-assigned"
          }, () => {
            this.getUserList();
          });
        } else {
          this.setState({
            showErrorAlert: true,
            errorAlertMessage: "Sorry! Try again later."
          }, () => {
            this.getUserList();
          })
        }
      })
      .catch(error => {
        console.log(error);
      })

  }

  selectConference = (event, index) => {

    let { conferenceList, selectedConferenceId } = this.state;

    // let checkList= {};
    let selectedConference = conferenceList[index];

    conferenceList.map(list => {

      if (list._id === selectedConference._id) {
        if (event.target.value == "on") {
          // checkList[`${selectedConference._id}`] = true;
          list["isChecked"] = true;
        } else {
          // checkList[`${selectedConference._id}`] = false;
          list["isChecked"] = false;
        }
      } else {
        // checkList[`${selectedConference._id}`] = false;
        list["isChecked"] = false;
      }

    })

    this.setState({
      isChecked: !this.state.isChecked,
      selectedConference: selectedConference,
      selectedConferenceId: selectedConference._id,
      conferenceList: conferenceList,
      // checkList: checkList
    }, () => {
      console.log(this.state.selectedConference);
    });

  }

  assignOperator = () => {

    let { selectedConferenceId, selectedConference, selectedUserId } = this.state;

    let invitationCode = selectedConference.invitationCode;
    let url = invitationCode ? `${cred.SITE_URL}conferencecall?invitationcode=${invitationCode}` : '';

    let dataToSend = {
      "userId": selectedUserId,
      "conferenceId": selectedConferenceId,
      "url": url,
      "companyId": this.state.companyId
    };
    this.setState({loading: true});

    // console.log("dataToSend for assign -- ", dataToSend);
    // API -- 
    axios
      .post(path + 'operator-management/assign-operator-to-company', dataToSend)
      .then(serverResponse => {
        let res = serverResponse.data;
        if (!res.isError) {

          this.setState({
            loading: false,
            showSuccessAlert: true,
            successAlertMessage: "Successfully assigned"
          });

          this.getUserList();

        } else {
          this.setState({
            loading: false,
            showErrorAlert: true,
            errorAlertMessage: "Sorry! Try again later."
          });

        }

      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error);
      })

  }

  utilizationStat = (index) => {

    let { users } = this.state;
    let user = users[index];
    console.log("selected user1--", user);
    let { _id, fname, lname, userId, service, companyId } = user;

    this.props.history.push({
      pathname: '/companyoperatorstat/' + `${_id}`,
      state: {
        name: `${fname + ' ' + lname}`,
        service: service,
        companyId: companyId
      }
    });

  }

  hideSuccessAlert() {
    this.setState({
      showSuccessAlert: false,
      successAlertMessage: '',
    }, () => {
      this.clearState();
    })
  }

  hideErrorAlert() {
    this.setState({
      showErrorAlert: false,
      errorAlertMessage: '',
    }, () => {
      this.clearState();
    })
  }

  clearState = () => {
    this.setState({
      loading: false,
      showUser: false,
      showUser: false,
      viewUser: false,
      showConference: false,
      deleteUser: false,
      deleteItem: '',
      deletingId: '',
      fname: '',
      lname: '',
      selectedUserId: '',
      userType: '',
      email: '',
      mobile: '',
      countryCode: '',
      service: '',

      selectedConference: {},
      selectedConferenceId: '',
      selectedUserId: '',

    });
  }

  pageChangePrev = () => {

    this.setState({
      pageNo: (parseFloat(this.state.pageNo) - 1)
    }, () => {
      this.getUserList()
    });

  }

  pageChangeNext = () => {

    this.setState({
      pageNo: (parseFloat(this.state.pageNo) + 1)
    }, () => {
      this.getUserList()
    });

  }

  mobMenuClick = () => {
    console.log('mobMenuClick');
  }


  render() {

    let { users, loading, searching, fname, lname, pageNo, userRecords, email, mobile, countryCode, service,
       conferenceList, countryCodeList } = this.state;
    let headers = [];
    headers = [
      { label: "Name", key: 'name' },
      { label: "Country Code", key: 'countryCode' },
      { label: "Phone", key: 'phoneNumber' },
      { label: "Email", key: 'emailAddress' },
      { label: "User Type", key: 'userType' },
    ];

    return (
      <main>

        <section className="user-mngnt-wrap">
          <div className="container-fluid">
            <div className="row">
              <Navbar routTo="/companyoperator" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />
              <div className="col-xl-10 col-lg-9 p-0">
                <div className="mobile-menu-header">
                  {/* <a className="back-arw" onClick={() => console.log('<-BACK')}><i className="fas fa-arrow-left"></i></a> */}
                  <a href= "JavaScript:Void(0);" className="menu-tgl" onClick={() => this.sideMenuToggle()}><img src="images/menu-tgl.png" alt="" /></a>
                </div>

                <div className="mng-full-list orga-mng">
                  <div className="mng-full-srch">
                    <div className="row">
                      <div className="col-md-9">
                        <div className="srch-wrap">
                          <form>
                            <input
                              type="text"
                              name=""
                              className="form-control"
                              placeholder="Search User"
                              value={this.state.searchString} onChange={(event) => this.setState({ searchString: event.target.value })}
                            />
                            {
                              this.state.searching ?
                                <input type="button" onClick={(event) => this.clearSearch(event)}
                                  data-tip='Clear'
                                />
                                :
                                <input type="submit" onClick={(event) => this.searchUsers(event)}
                                  data-tip='Search'
                                />
                            }
                            <ReactTooltip
                              effect="float"
                              place="top"
                              data-border="true"
                            />
                          </form>
                        </div>
                      </div>
                      <div className="col-md-3">
                        <div className="add-user-btn">
                          <a href="JavaScript:Void(0);" className="btn"
                            onClick={() => { this.setState({ showUser: true }) }}
                            // ref={ref => this.fooRef = ref}
                            data-tip='Add New Operator'
                            // onFocus={() => { ReactTooltip.show(this.fooRef) }}
                          >
                            Add Operator
                          </a>
                          <ReactTooltip
                            effect="float"
                            place="top"
                            data-border="true"
                          />
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="mng-full-table">
                    <div className="row">
                      <div className="col-md-8">
                        <div className="mng-full-table-hdr">
                          <div className="row">
                            <div className="col-md-4 border-rt">
                              <h6>Name</h6>
                            </div>
                            <div className="col-md-4 border-rt">
                              <h6>Service</h6>
                            </div>
                            <div className="col-md-4">
                              <h6>Availability</h6>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div className="col-md-4">

                      </div>
                    </div>

                    {users && users.length > 0 && users.map((data, index) => {

                      return (data.userId != this.state.userId) ?

                        <div className="row" key={index}>

                          <div className="col-md-8">
                            <div className="mng-full-table-row">
                              <div className="row">
                                <div className="col-md-4 border-rt">
                                  <h6>Name</h6>
                                  <p>
                                    <a href="JavaScript:Void(0);" onClick={() => { this.utilizationStat(index) }}>
                                      {data.fname + ' ' + data.lname}
                                    </a>
                                  </p>
                                </div>
                                <div className="col-md-4 border-rt">
                                  <h6>Service</h6>
                                  <p>{data.service}</p>
                                </div>
                                <div className="col-md-4">
                                  <h6>Availability</h6>
                                  <p>{data.isAssigned ? "Unavailable" : "Available"}</p>
                                </div>

                                <div className="mobile-ad-edt-btns">
                                  <ul>
                                    <li>
                                      <a href='JavaScript:Void(0);'
                                        onClick={() => { this.utilizationStat(index) }}
                                      >
                                        <i className="fas fa-cog"></i>
                                      </a>
                                    </li>
                                  </ul>
                                </div>

                              </div>
                            </div>
                          </div>

                          <div className="col-md-2">

                            {data.isAssigned ?
                              <a href="JavaScript:Void(0);" className="btn w-100 h-auto-btn"
                                data-tip='Unassign'
                                onClick={() => { this.userUnassign(index) }}
                              >
                                Unassign
                                
                              </a>
                              :
                              <a href="JavaScript:Void(0);" className="btn border w-100 h-auto-btn"
                                data-tip='Assign'
                                onClick={() => { this.userAssign(index) }}
                              >
                                Assign
                                
                              </a>
                            }
                            <ReactTooltip
                              effect="float"
                              place="top"
                              data-border="true"
                            />
                          </div>

                          <div className="col-md-2">

                            <div className="mng-full-table-row add-edt text-center">
                              <a href='JavaScript:Void(0);'
                                data-tip='Utilization Status'
                                onClick={() => { this.utilizationStat(index) }}
                              >
                                <i className="fas fa-cog"></i>
                              </a>
                              <ReactTooltip
                                effect="float"
                                place="top"
                                data-border="true"
                              />
                            </div>

                          </div>

                        </div>

                        : null

                    })}

                  </div>

                  <br /><br />
                {!searching ?
                  <div className="form-group">
                    {!loading && parseFloat(pageNo) > 1 ?
                      <span style={{ color: "#007bff", cursor: "pointer" }}
                        onClick={() => { this.pageChangePrev() }}
                      >
                        <b>{"<< Prev"}</b>
                      </span>
                      :
                      null}
                      &nbsp;&nbsp;|&nbsp;&nbsp;
                      {!loading && (parseFloat(pageNo) < parseFloat(userRecords)) ?
                      <span style={{ color: "#007bff", cursor: "pointer" }}
                        onClick={() => { this.pageChangeNext() }}
                      >
                        <b>{"Next >>"}</b>
                      </span>
                      :
                      null}
                    {loading ?
                      <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                        <i className="fa fa-spinner fa-spin" ></i>
                      </a>
                      :
                      null}

                  </div>
                  :
                  null}

                </div>

              </div>
            </div>
          </div>
        </section>

        <Modal show={this.state.showUser} onHide={() => { this.clearState() }}>
          <Modal.Header closeButton>
            <Modal.Title>Add new Operator</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="row">

              <div className="col-md-6">
                <div className="form-group">
                  <label>First Name:</label>
                  <input
                    type="text"
                    name="fname"
                    value={fname}
                    className="form-control"
                    placeholder="Enter First Name"
                    onChange={event => this.setState({ fname: event.target.value })}
                    onFocus={() => this.setState({ fnameError: "" })}
                  />
                  <span style={{ color: 'red' }}>{this.state.fnameError ? `* ${this.state.fnameError}` : ''}</span>
                </div>
              </div>

              <div className="col-md-6">
                <div className="form-group">
                  <label>Last Name:</label>
                  <input
                    type="text"
                    name="lname"
                    value={lname}
                    className="form-control"
                    placeholder="Enter Last Name"
                    onChange={event => this.setState({ lname: event.target.value })}
                    onFocus={() => this.setState({ lnameError: "" })}
                  />
                  <span style={{ color: 'red' }}>{this.state.lnameError ? `* ${this.state.lnameError}` : ''}</span>
                </div>
              </div>

              <div className="col-md-12">
                <div className="form-group">
                  <label>Email Address:</label>
                  <input
                    type="email"
                    name="email"
                    value={email}
                    className="form-control"
                    placeholder="Enter Email Address"
                    onChange={event => this.setState({ email: event.target.value })}
                    onFocus={() => this.setState({ emailError: "" })}
                  />
                  <span style={{ color: 'red' }}>{this.state.emailError ? `* ${this.state.emailError}` : ''}</span>
                </div>
              </div>

              <div className="col-md-5">
                <div className="form-group">
                  <label>Country Code:</label>
                  <select
                    className="form-control"
                    name="countryCode"
                    value={countryCode}
                    onChange={event => this.setState({ countryCode: event.target.value })}
                    onFocus={() => this.setState({ countryCodeError: "" })}
                  >
                    <option value="">Select country</option>
                    {countryCodeList && countryCodeList.map((data, index) => {
                      return <option value={data.code}>{data.code + ' ' + data.name}</option>
                    })}
                  </select>
                  <span style={{ color: 'red' }}>{this.state.countryCodeError ? `* ${this.state.countryCodeError}` : ''}</span>
                </div>
              </div>

              <div className="col-md-7">
                <div className="form-group">
                  <label>Mobile Number:</label>
                  <input
                    type="tel"
                    name="mobile"
                    value={mobile}
                    className="form-control"
                    placeholder="Enter Mobile Number"
                    onChange={event => this.setState({ mobile: event.target.value })}
                    onFocus={() => this.setState({ mobileError: "" })}
                  />
                  <span style={{ color: 'red' }}>{this.state.mobileError ? `* ${this.state.mobileError}` : ''}</span>
                </div>
              </div>

              <div className="col-md-6">
                <div className="form-group">
                  <label>Service:</label>
                  <select className="form-control" name="service" value={service}
                    onChange={event => this.setState({ service: event.target.value })}
                    onClick={() => this.setState({ serviceError: "" })}
                  >
                    <option value="">Select Service</option>
                    {Object.keys(cred.OPERATOR_SERVICES).map(service => {
                      return <option value={service}>{cred.OPERATOR_SERVICES[service]}</option>
                    })}
                  </select>

                  <span style={{ color: 'red' }}>{this.state.serviceError ? `* ${this.state.serviceError}` : ''}</span>
                </div>
              </div>

            </div>
          </Modal.Body>
          <Modal.Footer>
            {loading ?
              <Button variant="primary" disabled> <i class="fas fa-spinner fa-spin"></i> </Button>
              :
              null
            }
            {!loading ?
              <Button variant="primary" onClick={this.addNewUser}> Save </Button>
              :
              null
            }
          </Modal.Footer>
        </Modal>

        <Modal show={this.state.showConference} size={"lg"} onHide={() => { this.clearState() }}>
          <Modal.Header closeButton>
            <Modal.Title>Assign Operator</Modal.Title>
          </Modal.Header>
          <Modal.Body style={{ minHeight: '400px' }}>

            <div className="mng-full-table modal-adrs-book">
              <div className="row">
                <div className="col-md-12">

                  <div className="row pt-2 pb-2">
                    <div className="col-md-8">
                      <h4>Assign operator to conference</h4>
                    </div>

                  </div>
                  {/* For adding member End */}

                  <div className="mng-full-table-hdr">
                    <div className="row">
                      <div className="col-md-2 border-rt">
                        <h6>Select</h6>
                      </div>
                      <div className="col-md-5 border-rt">
                        <h6>conference Name</h6>
                      </div>
                      <div className="col-md-5">
                        <h6>Participants</h6>
                      </div>

                    </div>
                  </div>

                </div>

              </div>
              {conferenceList.length > 0 && conferenceList.map((member, index) => {

                return <div className="row" key={index}>
                  {member.phoneNumber != '' ?
                    <div className="col-md-12">

                      <div className="mng-full-table-row">

                        <div className="row">
                          <div className="col-md-2 border-rt">
                            <h6>Select</h6>

                            {member.isChecked ?
                              <input type="checkbox" name="checkList" checked={true} onChange={(event) => this.selectConference(event, index)} />
                              :
                              <input type="checkbox" name="checkList" checked={false} onChange={(event) => this.selectConference(event, index)} />
                            }
                          </div>

                          <div className="col-md-5 border-rt">
                            <h6>Conference Name</h6>
                            <p><a href="#">{member.conferenceTitle}</a></p>
                          </div>
                          <div className="col-md-5">
                            <h6>Participants</h6>
                            <p>{member.participants && member.participants.length}</p>
                          </div>

                        </div>

                      </div>

                    </div>
                    : null}
                </div>

              })

              }

            </div>

          </Modal.Body>
          <Modal.Footer>
            {!loading?
              <Button variant="primary" onClick={this.assignOperator}> Assign </Button>
              :null}

            {loading?
              <Button variant="primary" ><i className="fa fa-spinner fa-spin" ></i></Button>
            :null}

          </Modal.Footer>
        </Modal>

        <Modal show={this.state.showSuccessAlert} onHide={() => this.hideSuccessAlert()}>
          <Modal.Header closeButton>
            <Modal.Title>OK</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h5 style={{ color: 'green' }}>{`${this.state.successAlertMessage}`}</h5>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="primary" onClick={() => this.hideSuccessAlert()}>
              Ok
            </Button>
          </Modal.Footer>
        </Modal>

        <Modal show={this.state.showErrorAlert} onHide={() => this.hideErrorAlert()}>
          <Modal.Header closeButton>
            <Modal.Title>OK</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h5 style={{ color: 'red' }}>{`${this.state.errorAlertMessage}`}</h5>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="primary" onClick={() => this.hideErrorAlert()}>
              Ok
            </Button>
          </Modal.Footer>
        </Modal>


      </main>
    );
  }
}

export default checkAuthentication(CompanyOperator);
