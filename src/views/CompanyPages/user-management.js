import React, { Component } from 'react'
import Navbar from '../Component/Navbar'
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button } from "react-bootstrap"
import _ from 'lodash';
import { CSVLink, CSVDownload } from "react-csv";
import ReactTooltip from 'react-tooltip'; // For tool tip 

import { checkAuthentication } from '../Component/Authentication';

var path = cred.API_PATH;


export class UserManagement extends Component {
  constructor(props) {
    super(props)
    this.state = {
      userId: '',

      openSideMenu: false,
      searchString: '',
      searching: false,

      loading: false,

      countryCodeList: [],
      users: [],
      userRecords: '',
      pageNo: 1,

      showUser: false,
      viewUser: false,
      editUser: false,
      deleteUser: false,
      deleteItem: '',
      deletingId: '',

      fname: '',
      fnameError: '',
      lname: '',
      lnameError: '',
      companyId: '',
      companyIdError: '',
      userType: '',
      userTypeError: '',
      email: '',
      emailError: '',
      mobile: '',
      mobileError: '',
      countryCode: '',
      companyType: '',
      companyTypeError: '',

      showSuccessAlert: false,
      successAlertMessage: '',
      showErrorAlert: false,
      errorAlertMessage: ''

    }
    this.perPage = 5;
  }

  componentDidMount() {
    document.body.classList.remove("sign-bg");
    document.body.classList.add("grey-bg");

    let storage = localStorage.getItem("MIO_Local");
    storage = storage ? JSON.parse(storage) : null;
    if (storage && storage.data.userId) {
      this.setState({
        userId: storage.data.userId,
        companyType: storage.data.companyType,
        companyId: storage.data.companyId
      }, () => {
        this.getUserList();
        this.getCountryCodeList();
      });

      console.log(this.parseJwt(storage.token));
      let tokenData = this.parseJwt(storage.token);

    }


  }

  getUserList = () => {

    this.setState(prevState => ({ loading: true }));

    let dataToSend = {
      "companyId": this.state.companyId,
      "createdBy": this.state.userId,
      "page": this.state.pageNo,
      "perPage": this.perPage
    };
    // API to get users -- 
    axios
      .post(path + 'user-management/get-user-company-basis', dataToSend)
      .then(serverResponse => {

        let res = serverResponse.data;
        if (!res.isError) {
          // console.log("user-management---", res);
          let result = res.details && res.details.response && res.details.response[0].results;
          let totalPage = res.details && res.details.response && res.details.response[0].noofpage;
          this.setState({
            users: result,
            userRecords: totalPage,
            loading: false
          });
        } else {
          this.setState(prevState => ({ loading: false }));
        }

      })
      .catch(error => {
        console.log(error);

      })

  }

  getCountryCodeList = () => {

    axios
      .post(path + 'services/get-country-codes')
      .then(serverResponse => {

        let res = serverResponse.data;
        if (!res.isError) {
          this.setState({
            countryCodeList: res.details
          });
        }

      })
      .catch(error => {
        console.log(error);
      })

  }

  addNewUser = () => {

    let { loading, fname, lname, companyId, email, mobile, countryCode, companyType } = this.state;

    !fname ? this.setState({ fnameError: "Please enter a fname." }) : this.setState({ fnameError: "" });
    !lname ? this.setState({ lnameError: "Please enter a lname." }) : this.setState({ lnameError: "" });
    (/.+@.+\.[A-Za-z]+$/.test(`${email}`)) ? this.setState({ emailError: "" }) : this.setState({ emailError: "Please enter an email address." });
    (mobile && !isNaN(mobile)) ? this.setState({ mobileError: "" }) : this.setState({ mobileError: "Please enter a phone number." });
    (countryCode && !isNaN(countryCode)) ? this.setState({ countryCodeError: "" }) : this.setState({ countryCodeError: "Please enter a country code." });

    if (fname && lname && (/.+@.+\.[A-Za-z]+$/.test(`${email}`)) && !isNaN(mobile) && !isNaN(countryCode) && companyId) {

      if (countryCode.slice(0, 1) == '+') {
        countryCode = countryCode.slice(1);
      }

      let dataToSend = {
        'fname': fname,
        'lname': lname,
        'email': email,
        'countryCode': countryCode,
        'mobile': mobile,
        'companyId': companyId,
        'userType': cred.USER_TYPE_COMPANY_USER,
        'companyType': companyType,
        'createdBy': this.state.userId
      };
      this.setState(prevState => ({ loading: true }));
      // For saving new user -- 
      axios
        .post(path + 'user-management/add-new-user', dataToSend)
        .then(serverResponse => {
          let res = serverResponse.data;
          if (!res.isError && res.statuscode == 200) {
            // console.log(res);
            this.setState({ 
              loading: false, 
              showUser: false,
              showSuccessAlert: true,
              successAlertMessage: "Successfully added. The Password is : "+`${res.details && res.details.password}`
            });
            this.getUserList();
            this.clearState();
            // alert('Password is: ' + res.details.password);
          } else {

            this.clearState();
            let message = '';
            if (res.details && res.details != '') {
              message = res.details;
            } else {
              message = res.message;
            }
            this.setState({
              showErrorAlert: true,
              errorAlertMessage: message
            });
          }
        })
        .catch(error => {
          console.log(error);
          this.setState(prevState => ({ loading: false }));
        })

    }

  }

  hideSuccessAlert() {
    this.setState({
      showSuccessAlert: false,
      successAlertMessage: '',
    })
  }

  hideErrorAlert() {
    this.setState({
      showErrorAlert: false,
      errorAlertMessage: '',
    })
  }

  searchUsers = (event) => {

    event.preventDefault();
    if (!this.state.searchString || (this.state.searchString && this.state.searchString.length < 2)) {
      alert("Please use valid search");
    } else {

      this.setState(prevState => ({ searching: true }));

      let dataToSend = {
        "userName": this.state.searchString,
        "companyId": this.state.companyId,
        "createdBy": this.state.userId,
        "page": '',
        "perPage": ''
      };

      // API For searching -- 
      axios
        .post(path + 'user-management/search-user-name-basis', dataToSend)
        .then(serverResponse => {
          let res = serverResponse.data;
          if (!res.isError) {
            console.log(res.response);
            if (res.response) {
              this.setState(prevState => ({
                searching: true,
                users: res.response
              }));
            }
          } else {
            this.setState(prevState => ({ searching: false }));
          }

        })
        .catch(error => {
          console.log(error);
        })

    }

  }

  clearSearch(event) {
    event.preventDefault();
    let { userId } = this.state;
    this.setState(prevState => ({
      searchString: '', searching: false
    }));
    this.getUserList();
  }

  openViewUsers = (index) => {
    // console.log("In openViewUsers-->");
    this.setState(prevState => ({
      viewUser: true,
      editUser: true
    }));

    this.gettingDataOnModal(index);
  }

  openEditUsers = (index) => {

    this.setState(prevState => ({
      viewUser: false,
      editUser: true
    }));

    this.gettingDataOnModal(index);
  }

  // Getting data for modal -- 
  gettingDataOnModal = (index) => {

    let { loading, users } = this.state;
    let user = users[index];

    let { fname, lname, companyId, userType, email, mobile, countryCode, companyType, userId } = user;
    this.setState({ selectedId: index, fname, lname, companyId: companyId, userType, email, mobile, countryCode, companyType });

  }

  editUsers = () => {

    this.setState(prevState => ({ loading: true }));
    let { loading, users, selectedId, fname, lname, companyId, email, mobile, countryCode, companyType, selectedUserId } = this.state;

    !fname ? this.setState({ fnameError: "Please enter a fname." }) : this.setState({ fnameError: "" });
    !lname ? this.setState({ lnameError: "Please enter a lname." }) : this.setState({ lnameError: "" });
    (/.+@.+\.[A-Za-z]+$/.test(`${email}`)) ? this.setState({ emailError: "" }) : this.setState({ emailError: "Please enter an email address." });
    (mobile && !isNaN(mobile)) ? this.setState({ mobileError: "" }) : this.setState({ mobileError: "Please enter a phone number." });
    (countryCode && !isNaN(countryCode)) ? this.setState({ countryCodeError: "" }) : this.setState({ countryCodeError: "Please enter a country code." });

    if (fname && lname && (/.+@.+\.[A-Za-z]+$/.test(`${email}`)) && !isNaN(mobile) && !isNaN(countryCode) && companyId) {

      let user = users[selectedId];
      let { userId } = user;

      let dataToSend = {
        'userId': userId,
        'fname': fname,
        'lname': lname,
        'email': email,
        'countryCode': countryCode,
        'mobile': mobile,
        'companyId': companyId,
        'userType': cred.USER_TYPE_COMPANY_USER,
        'companyType': companyType
      };
      // console.log("dataToSend--", dataToSend);

      // API For updating user details -- 
      axios
        .post(path + 'user-management/edit-user', dataToSend)
        .then(serverResponse => {
          let res = serverResponse.data;
          if (!res.isError) {

            this.clearState();

            this.getUserList();

          } else {
            this.clearState();
            this.setState({
              showErrorAlert: true,
              errorAlertMessage: res.message
            });
            // alert(res.message);
          }
        })
        .catch(error => {
          console.log(error);
        })

    }

  }

  deleteUserConfirm = (index) => {

    let { loading, users } = this.state;
    let user = users[index];

    let { fname, lname } = user;

    this.setState({
      deleteUser: true,
      deleteItem: fname + ' ' + lname,
      deletingId: index
    });

  }

  deleteUser = () => {
    let { users } = this.state;
    let user = users[this.state.deletingId];
    let { userId } = user;

    let dataToSend = {
      "userId": userId
    };
    console.log("Inside deleting user--", dataToSend);
    // API For deleting user -- 
    axios
      .post(path + 'user-management/delete-user-id-basis', dataToSend)
      .then(serverResponse => {
        let res = serverResponse.data;
        if (!res.isError) {
          console.log(res);
          this.setState({
            deleteUser: false,
            deleteItem: '',
            deletingId: '',
            showSuccessAlert: true,
            successAlertMessage: 'Successfully deleted',
          });
          this.getUserList();
        } else {
          this.setState({
            deleteUser: false,
            deleteItem: '',
            deletingId: '',
            showErrorAlert: true,
            errorAlertMessage: res.message
          })
        }

      })
      .catch(error => {
        console.log(error);
      })

  }

  // For clearing state -- 
  clearState = () => {
    this.setState({
      loading: false,
      showUser: false,
      showUser: false,
      viewUser: false,
      editUser: false,
      deleteUser: false,
      deleteItem: '',
      deletingId: '',
      fname: '',
      lname: '',
      // companyId: this.state.companyId,
      userType: '',
      email: '',
      mobile: '',
      countryCode: '',
      // companyType: this.state.companyType,
    });
  }

  pageChangePrev = () => {

    this.setState({
      pageNo: (parseFloat(this.state.pageNo) - 1)
    }, () => {
      this.getUserList()
    });

  }

  pageChangeNext = () => {

    this.setState({
      pageNo: (parseFloat(this.state.pageNo) + 1)
    }, () => {
      this.getUserList()
    });

  }

  sideMenuToggle() {
    let { openSideMenu } = this.state;
    openSideMenu = !openSideMenu;
    this.setState({ openSideMenu });
  }

  mobMenuClick() {
    this.sideMenuToggle();
    console.log('mobMenuClick');
  }

  parseJwt = (token) => {
    if (!token) { return; }
    const base64Url = token.split('.')[1];
    const base64 = base64Url.replace('-', '+').replace('_', '/');
    return JSON.parse(window.atob(base64));
  }



  render() {

    let { users, loading, searching, fname, lname, userRecords, pageNo, email, mobile, countryCode, countryCodeList } = this.state;
    let headers = [];
    headers = [
      { label: "Name", key: 'name' },
      { label: "Country Code", key: 'countryCode' },
      { label: "Phone", key: 'phoneNumber' },
      { label: "Email", key: 'emailAddress' },
      { label: "User Type", key: 'userType' },
    ];

    return <main>
      <section className="user-mngnt-wrap">
        <div className="container-fluid">
          <div className="row">
            <Navbar routTo="/usermanagement" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />
            <div className="col-xl-10 col-lg-9 p-0 mob-full-conf">

              <div className="mobile-menu-header">
                {/* <a className="back-arw" onClick={() => console.log('<-BACK')}><i className="fas fa-arrow-left"></i></a> */}
                <a href="JavaScript:Void(0);" className="menu-tgl" onClick={() => this.sideMenuToggle()}><img src="images/menu-tgl.png" alt="" /></a>
              </div>

              <div className="mng-full-list">

                <div className="mng-full-srch">
                  <div className="row">
                    <div className="col-md-9">
                      <div className="srch-wrap">
                        <form>
                          <input value={this.state.searchString} onChange={(event) => this.setState({ searchString: event.target.value })} type="text" name="" className="form-control" placeholder="Search User" />
                          {
                            this.state.searching ?
                              <input type="button" onClick={(event) => this.clearSearch(event)}
                                data-tip='Clear'
                              />
                              :
                              <input type="submit" onClick={(event) => this.searchUsers(event)}
                                data-tip='Search'
                              />
                          }
                          <ReactTooltip
                            effect="float"
                            place="top"
                            data-border="true"
                          />
                        </form>
                      </div>
                    </div>
                    <div className="col-md-3">
                      <div className="add-user-btn">
                        <a href="JavaScript:Void(0);"
                          className="btn"
                          onClick={() => this.setState({ showUser: true })}
                          data-tip='Add new user'
                        >
                          Add New User
                        </a>
                        <ReactTooltip
                          effect="float"
                          place="top"
                          data-border="true"
                        />
                      </div>
                    </div>
                  </div>
                </div>

                <div className="mng-full-table">

                  <div className="row">

                    <div className="col-md-8">
                      <div className="mng-full-table-hdr">
                        <div className="row">
                          <div className="col-md-3 border-rt">
                            <h6>Name/Company</h6>
                          </div>
                          <div className="col-md-3 border-rt">
                            <h6>User Type</h6>
                          </div>
                          <div className="col-md-3 border-rt">
                            <h6>Email Id</h6>
                          </div>
                          <div className="col-md-3">
                            <h6>Phone No</h6>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="col-md-4">
                      <div className="mng-full-table-hdr">
                        <div className="row">
                          <div className="col-md-4 border-rt">
                            <h6>View</h6>
                          </div>
                          <div className="col-md-4 border-rt">
                            <h6>Edit</h6>
                          </div>
                          <div className="col-md-4">
                            <h6>Delete</h6>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>

                  {users && users.length > 0 && users.map((data, index) => {

                    return (data.userId != this.state.userId) ?

                      <div className="row" key={index}>

                        <div className="col-md-8">
                          <div className="mng-full-table-row">
                            <div className="row">
                              <div className="col-md-3 border-rt">
                                <h6>Name/Company</h6>
                                <p className="textEllips">
                                  <a href="JavaScript:Void(0);" onClick={() => this.openViewUsers(index)}>
                                    {data.fname + ' ' + data.lname}
                                  </a>
                                </p>
                              </div>
                              <div className="col-md-3 border-rt">
                                <h6>User Type</h6>
                                <p className="textEllips">{data.userType}</p>
                              </div>
                              <div className="col-md-3 border-rt"
                                // ref={ref => this.fooRef = ref}
                                data-tip={data.email}
                              // onFocus={() => { ReactTooltip.show(this.fooRef) }}
                              >
                                <h6>Email Id</h6>
                                <p className="textEllips">
                                  {data.email}
                                </p>
                                <ReactTooltip
                                  effect="float"
                                  place="top"
                                  data-border="true"
                                />
                              </div>
                              <div className="col-md-3">
                                <h6>Phone No</h6>
                                <p className="textEllips">{data.countryCode + ' ' + data.mobile}</p>
                              </div>

                              <div className="mobile-ad-edt-btns">
                                <ul>
                                  <li onClick={() => this.openViewUsers(index)}>
                                    <a href="JavaScript:Void(0);"
                                      // ref={ref => this.fooRef = ref}
                                      data-tip='View'
                                    // onFocus={() => { ReactTooltip.show(this.fooRef) }}
                                    >
                                      <i className="fas fa-eye"></i>
                                    </a>
                                    <ReactTooltip
                                      effect="float"
                                      place="top"
                                      data-border="true"
                                    />
                                  </li>

                                  <li onClick={() => this.openEditUsers(index)}>
                                    <a href="JavaScript:Void(0);"
                                      // ref={ref => this.fooRef = ref}
                                      data-tip='Edit'
                                    // onFocus={() => { ReactTooltip.show(this.fooRef) }}
                                    >
                                      <i className="far fa-edit"></i>
                                    </a>
                                    <ReactTooltip
                                      effect="float"
                                      place="top"
                                      data-border="true"
                                    />
                                  </li>

                                  <li onClick={() => this.deleteUserConfirm(index)}>
                                    <a href="JavaScript:Void(0);"

                                      // ref={ref => this.fooRef = ref}
                                      data-tip='Delete'
                                    // onFocus={() => { ReactTooltip.show(this.fooRef) }}
                                    >
                                      <i className="far fa-trash-alt"></i>
                                    </a>
                                    <ReactTooltip
                                      effect="float"
                                      place="top"
                                      data-border="true"
                                    />
                                  </li>

                                </ul>
                              </div>

                            </div>
                          </div>

                        </div>

                        <div className="col-md-4">
                          <div className="mng-full-table-row add-edt text-center">
                            <div className="row">
                              <div className="col-md-4">
                                <a href="JavaScript:Void(0);"
                                  // ref={ref => this.fooRef1 = ref}
                                  data-tip='View'
                                  onClick={() => { this.openViewUsers(index) }}
                                // onFocus={() => { ReactTooltip.show(this.fooRef1) }}
                                >
                                  <i className="fas fa-eye"></i>
                                </a>
                                <ReactTooltip
                                  effect="float"
                                  place="top"
                                  data-border="true"
                                />
                              </div>

                              <div className="col-md-4">
                                <a href="JavaScript:Void(0);"
                                  // ref={ref => this.fooRef2 = ref}
                                  data-tip='Edit'
                                  onClick={() => { this.openEditUsers(index); }}
                                // onFocus={() => { ReactTooltip.show(this.fooRef2) }}
                                >
                                  <i className="far fa-edit"></i>
                                </a>
                                <ReactTooltip
                                  effect="float"
                                  place="top"
                                  data-border="true"
                                />
                              </div>

                              <div className="col-md-4">
                                <a href="JavaScript:Void(0);"
                                  // ref={ref => this.fooRef3 = ref}
                                  data-tip='Delete'
                                  onClick={() => { this.deleteUserConfirm(index) }}
                                // onFocus={() => { ReactTooltip.show(this.fooRef3) }}
                                >
                                  <i className="far fa-trash-alt"></i>
                                </a>
                                <ReactTooltip
                                  effect="float"
                                  place="top"
                                  data-border="true"
                                />
                              </div>

                            </div>
                          </div>
                        </div>

                      </div>

                      : null

                  })
                  }

                </div>

                <br /><br />
                {!searching ?
                  <div className="form-group">
                    {!loading && parseFloat(pageNo) > 1 ?
                      <span style={{ color: "#007bff", cursor: "pointer" }}
                        onClick={() => { this.pageChangePrev() }}
                      >
                        <b>{"<< Prev"}</b>
                      </span>
                      :
                      null}
                      &nbsp;&nbsp;|&nbsp;&nbsp;
                      {!loading && (parseFloat(pageNo) < parseFloat(userRecords)) ?
                      <span style={{ color: "#007bff", cursor: "pointer" }}
                        onClick={() => { this.pageChangeNext() }}
                      >
                        <b>{"Next >>"}</b>
                      </span>
                      :
                      null}
                    {loading ?
                      <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                        <i className="fa fa-spinner fa-spin" ></i>
                      </a>
                      :
                      null}

                  </div>
                  :
                  null}

              </div>

            </div>
          </div>
        </div>

      </section>

      <Modal show={this.state.showUser} onHide={() => { this.clearState() }}>
        <Modal.Header closeButton>
          <Modal.Title>Add new Contact</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="row">

            <div className="col-md-6">
              <div className="form-group">
                <label>First Name:</label>
                <input
                  type="text"
                  name="fname"
                  value={fname}
                  className="form-control"
                  placeholder="Enter First Name"
                  onChange={event => this.setState({ fname: event.target.value })}
                  onFocus={() => this.setState({ fnameError: "" })}
                />
                <span style={{ color: 'red' }}>{this.state.fnameError ? `* ${this.state.fnameError}` : ''}</span>
              </div>
            </div>

            <div className="col-md-6">
              <div className="form-group">
                <label>Last Name:</label>
                <input
                  type="text"
                  name="lname"
                  value={lname}
                  className="form-control"
                  placeholder="Enter Last Name"
                  onChange={event => this.setState({ lname: event.target.value })}
                  onFocus={() => this.setState({ lnameError: "" })}
                />
                <span style={{ color: 'red' }}>{this.state.lnameError ? `* ${this.state.lnameError}` : ''}</span>
              </div>
            </div>

            <div className="col-md-12">
              <div className="form-group">
                <label>Email Address:</label>
                <input
                  type="email"
                  name="email"
                  value={email}
                  className="form-control"
                  placeholder="Enter Email Address"
                  onChange={event => this.setState({ email: event.target.value })}
                  onFocus={() => this.setState({ emailError: "" })}
                />
                <span style={{ color: 'red' }}>{this.state.emailError ? `* ${this.state.emailError}` : ''}</span>
              </div>
            </div>

            <div className="col-md-5">
              <div className="form-group">
                <label>Country Code:</label>
                <select
                  className="form-control"
                  name="countryCode"
                  value={countryCode}
                  onChange={event => this.setState({ countryCode: event.target.value })}
                  onFocus={() => this.setState({ countryCodeError: "" })}
                >
                  <option value="">Select country</option>
                  {countryCodeList && countryCodeList.map((data, index) => {
                    return <option value={data.code}>{data.code + ' ' + data.name}</option>
                  })}
                </select>
                <span style={{ color: 'red' }}>{this.state.countryCodeError ? `* ${this.state.countryCodeError}` : ''}</span>
              </div>
            </div>

            <div className="col-md-7">
              <div className="form-group">
                <label>Mobile Number:</label>
                <input
                  type="tel"
                  name="mobile"
                  value={mobile}
                  className="form-control"
                  placeholder="Enter Mobile Number"
                  onChange={event => this.setState({ mobile: event.target.value })}
                  onFocus={() => this.setState({ mobileError: "" })}
                />
                <span style={{ color: 'red' }}>{this.state.mobileError ? `* ${this.state.mobileError}` : ''}</span>
              </div>
            </div>

          </div>
        </Modal.Body>
        <Modal.Footer>
          {!loading ?
            <Button variant="primary" onClick={this.addNewUser}> Save </Button>
            :
            null}
          {loading ?
            <Button variant="primary" >
              <i className="fa fa-spinner fa-spin" ></i>
            </Button>
            :
            null}
        </Modal.Footer>
      </Modal>

      <Modal show={this.state.editUser} onHide={() => { this.clearState() }}>
        <Modal.Header closeButton>
          <Modal.Title>{this.state.viewUser ? "View User" : "Edit User"}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="row">

            <div className="col-md-6">
              <div className="form-group">
                <label>First Name:</label>
                <input
                  type="text"
                  name="fname"
                  value={fname}
                  className="form-control"
                  placeholder="Enter First Name"
                  onChange={event => this.setState({ fname: event.target.value })}
                  onFocus={() => this.setState({ fnameError: "" })}
                  readOnly={this.state.viewUser}
                />
                <span style={{ color: 'red' }}>{this.state.fnameError ? `* ${this.state.fnameError}` : ''}</span>
              </div>
            </div>

            <div className="col-md-6">
              <div className="form-group">
                <label>Last Name:</label>
                <input
                  type="text"
                  name="lname"
                  value={lname}
                  className="form-control"
                  placeholder="Enter Last Name"
                  onChange={event => this.setState({ lname: event.target.value })}
                  onFocus={() => this.setState({ lnameError: "" })}
                  readOnly={this.state.viewUser}
                />
                <span style={{ color: 'red' }}>{this.state.lnameError ? `* ${this.state.lnameError}` : ''}</span>
              </div>
            </div>

            <div className="col-md-12">
              <div className="form-group">
                <label>Email Address:</label>
                <input
                  type="email"
                  name="email"
                  value={email}
                  className="form-control"
                  placeholder="Enter Email Address"
                  onChange={event => this.setState({ email: event.target.value })}
                  onFocus={() => this.setState({ emailError: "" })}
                  readOnly
                />
                <span style={{ color: 'red' }}>{this.state.emailError ? `* ${this.state.emailError}` : ''}</span>
              </div>
            </div>

            <div className="col-md-5">
              <div className="form-group">
                <label>Country Code:</label>
                <select
                  className="form-control"
                  name="countryCode"
                  value={countryCode}
                  onChange={event => this.setState({ countryCode: event.target.value })}
                  onFocus={() => this.setState({ countryCodeError: "" })}
                  readOnly={this.state.viewUser}
                >
                  <option value="">Select country</option>
                  {countryCodeList && countryCodeList.map((data, index) => {
                    return <option value={data.code}>
                      {data.name + ' ' + data.code}
                    </option>
                  })}
                </select>
                <span style={{ color: 'red' }}>{this.state.countryCodeError ? `* ${this.state.countryCodeError}` : ''}</span>
              </div>
            </div>

            <div className="col-md-7">
              <div className="form-group">
                <label>Mobile Number:</label>
                <input
                  type="tel"
                  name="mobile"
                  value={mobile}
                  className="form-control"
                  placeholder="Enter Mobile Number"
                  onChange={event => this.setState({ mobile: event.target.value })}
                  onFocus={() => this.setState({ mobileError: "" })}
                  readOnly={this.state.viewUser}
                />
                <span style={{ color: 'red' }}>{this.state.mobileError ? `* ${this.state.mobileError}` : ''}</span>
              </div>
            </div>

          </div>
        </Modal.Body>
        <Modal.Footer>
          {!this.state.viewUser ?
            <>
              {!loading ?
                <Button variant="primary" onClick={() => this.editUsers()}>
                  Save
                </Button>
                :
                null}
              {loading ?
                <Button variant="primary" >
                  <i className="fa fa-spinner fa-spin" ></i>
                </Button>
                : null}
            </>
            : null}
        </Modal.Footer>
      </Modal>

      <Modal show={this.state.deleteUser} onHide={() => { this.clearState() }}>
        <Modal.Header closeButton>
          <Modal.Title>Delete user confirm</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h3>Are you sure?</h3>
          <p>Deleting {this.state.deleteItem}</p>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => this.deleteUser()}>
            Yes
            </Button>
          <Button variant="primary" onClick={() => { this.setState({ deleteUser: false }); }}>
            No
            </Button>
        </Modal.Footer>
      </Modal>

      <Modal show={this.state.showSuccessAlert} onHide={() => this.hideSuccessAlert()}>
        <Modal.Header closeButton>
          <Modal.Title>OK</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h5 style={{ color: 'green' }}>{`${this.state.successAlertMessage}`}</h5>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="primary" onClick={() => this.hideSuccessAlert()}>
            Ok
            </Button>
        </Modal.Footer>
      </Modal>

      <Modal show={this.state.showErrorAlert} onHide={() => this.hideErrorAlert()}>
        <Modal.Header closeButton>
          <Modal.Title>OK</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h5 style={{ color: 'red' }}>{`${this.state.errorAlertMessage}`}</h5>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="primary" onClick={() => this.hideErrorAlert()}>
            Ok
            </Button>
        </Modal.Footer>
      </Modal>

    </main>
  }

}

export default checkAuthentication(UserManagement);