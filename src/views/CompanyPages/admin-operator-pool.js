import React, { Component } from 'react'
import Navbar from '../Component/Navbar'
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button } from "react-bootstrap"
import _ from 'lodash';
import { CSVLink, CSVDownload } from "react-csv";
import ReactTooltip from 'react-tooltip'; // For tool tip 

import { checkAuthentication } from '../Component/Authentication';

var path = cred.API_PATH;


export class AdminOperatorPool extends Component {
	constructor(props) {
		super(props);
		this.state = {
			userId: '',
			userType: '',
			companyType: '',
			companyId: '',

			searchString: '',
			searching: false,
			loading: false,

			users: [],
			assignedUsers: [],
			selectedUser: {},
			selectedUserId: '',

			fname: '',
			lname: '',
			email: '',
			isActive: false,

			selectedConference: {},
			selectedConferenceId: '',

			conferenceList: [],

			showAssign: false,

			showSuccessAlert: false,
			successAlertMessage: '',
			showErrorAlert: false,
			errorAlertMessage: '',
			confirmUnassign: false,
			unassignMessage: '',

			pageNo_1: 1,
			pageNo_2: 1,
			totalPage: '',
			totalAssigned: '',

		}
		this.perPage = 5;
	}

	componentDidMount() {
		document.body.classList.remove("sign-bg");
		document.body.classList.add("grey-bg");

		let storage = localStorage.getItem("MIO_Local");
		storage = storage ? JSON.parse(storage) : null;
		if (storage && storage.data.userId) {
			this.setState({
				userId: storage.data.userId,
				companyType: storage.data.companyType,
				companyId: storage.data.companyId
			}, () => {
				this.getUserList();
				this.getAssignedOperators();
			});

		}
	}

	getUserList = () => {
		this.setState({ loading: true })
		let dataToSend = {
			"userName": '',
			"page": this.state.pageNo_1,
			"perPage": this.perPage
		};
		// API to get users -- 
		axios
			.post(path + 'operator-management/fetch-all-admin-operator', dataToSend)
			.then(serverResponse => {

				let res = serverResponse.data;
				if (!res.isError) {
					let result = res.details && res.details[0] && res.details[0].results;
					let resultNo = res.details && res.details[0] && res.details[0].noofpage;

					this.setState({
						users: result,
						totalPage: resultNo,
						loading: false
					}, () => {
						console.log('ooo0000000', this.state.totalPage);
					});

				} else {
					this.setState({ loading: false })
				}

			})
			.catch(error => {
				console.log(error);
			})

	}

	getAssignedOperators = () => {
		this.setState({ loading: true });
		let dataToSend = {
			"companyId": this.state.companyId,
			"page": this.state.pageNo_2,
			"perPage": this.perPage
		};
		// API --
		axios
			.post(path + 'operator-management/fetch-assigned-operators-for-company', dataToSend)
			.then(serverResponse => {

				let res = serverResponse.data;
				// console.log("Already assigned operators =", res);
				if (!res.isError) {
					let result = res.details && res.details.results;
					let resultNo = res.details && res.details.noofpage;
					this.setState({
						assignedUsers: result,
						loading: false,
						totalAssigned: res.details && res.details.noofpage
					});

				} else {
					this.setState({
						loading: false
					})
				}

			})
			.catch(error => {
				console.log(error);
			})

	}

	getConferenceOperators = (conferenceList) => {
		return new Promise((resolve, reject) => {
			let operatorArray = [];

			if (conferenceList && conferenceList.length > 0) {

				conferenceList.map((data, index) => {
					data.operator && data.operator.map((value, index) => {
						operatorArray.push(value.userId);
					})
				})

				resolve({
					"isError": false,
					"response": operatorArray
				});

			} else {
				resolve({
					"isError": false,
					"response": operatorArray
				});
			}
		})

	}

	setTableData = async (userList, conferenceList) => {
		return new Promise((resolve, reject) => {
			if (userList && userList.length > 0) {
				let users = [];
				console.log("all operators ==", userList);
				userList.map((data, index) => {

					if (!data.isAssigned) {
						users.push(data);
					} else {
						if (conferenceList.includes(data.userId)) {
							users.push(data);
						}
					}

				})
				console.log("operator for this user ===", users);
				resolve({
					"isError": false,
					"response": users
				});
			} else {
				resolve({
					"isError": true
				});
			}
		})

	}

	searchUsers = (event) => {

		event.preventDefault();
		this.setState(prevState => ({ searching: true }));

		let dataToSend = {
			"userName": this.state.searchString
		};

		console.log("dataToSend--", dataToSend);
		// API for searching -- 
		axios
			.post(path + 'operator-management/fetch-all-admin-operator', dataToSend)
			.then(serverResponse => {
				let res = serverResponse.data;
				if (!res.isError) {
					let result = res.details;
					this.setState({
						users: result
					}, () => {
						console.log("admin-operator", this.state.users)
					});
				} else {
					this.setState(prevState => ({ searching: false }));
				}
			})
			.catch(error => {
				console.log(error);
			})

	}

	clearSearch = (event) => {

		event.preventDefault();
		this.setState({
			searchString: '',
			searching: false
		}, () => {
			this.getUserList();
		});

	}

	getConferenceList = () => {
		return new Promise((resolve, reject) => {
			let dataToSend = {
				"companyId": this.state.companyId
			};
			// API -- 
			axios
				.post(path + 'conference/list-conference-company', dataToSend)
				.then(serverResponse => {
					let res = serverResponse.data;
					if (!res.isError) {
						let conferenceList = res.details;
						this.setState({
							conferenceList: conferenceList
						}, () => {
							resolve({
								"isError": false,
								"response": conferenceList
							});
						});

					} else {
						resolve({
							"isError": true
						});
					}
				})
				.catch(error => {
					console.log(error);
					resolve({
						"isError": true
					});
				})

		})

	}

	openAssignModal = async (index) => {

		try {
			let { users } = this.state;
			let user = users[index];
			let { _id, userId } = user;

			let conferenceListStatus = await this.getConferenceList();
			if (conferenceListStatus && !conferenceListStatus.isError) {
				this.setState({
					conferenceList: conferenceListStatus.response,
					showAssign: true,
					selectedUser: user,
					selectedUserId: userId
				}, () => {
					console.log("conferenceList--", this.state.conferenceList);

				});
			} else {
				this.setState({
					showAssign: true,
					selectedUser: user,
					selectedUserId: userId
				});
			}

		}
		catch (error) {
			console.log(error);
		}

	}

	viewAdminOperator = (index) => {

		let { users } = this.state;
		let user = users[index];
		let { isActive, fname, lname, email } = user;

		this.setState({
			showAdminOperator: true,
			isActive: isActive,
			fname: fname,
			lname: lname,
			email: email
		});

	}

	selectConference = (event, index) => {

		let { conferenceList, selectedConferenceId } = this.state;
		let selectedConference = conferenceList[index];

		conferenceList.map(list => {

			if (list._id === selectedConference._id) {
				if (event.target.value == "on") {
					// checkList[`${selectedConference._id}`] = true;
					list["isChecked"] = true;
				} else {
					// checkList[`${selectedConference._id}`] = false;
					list["isChecked"] = false;
				}
			} else {
				// checkList[`${selectedConference._id}`] = false;
				list["isChecked"] = false;
			}

		})

		this.setState({
			isChecked: !this.state.isChecked,
			selectedConference: selectedConference,
			selectedConferenceId: selectedConference._id,
			conferenceList: conferenceList
		}, () => {
			console.log(this.state.selectedConference);
		});

	}

	assignOperator = () => {

		let { selectedConferenceId, selectedUserId } = this.state;
		this.setState({ loading: true });
		let dataToSend = {
			"userId": selectedUserId,
			"conferenceId": selectedConferenceId,
			"companyId": this.state.companyId
		};

		console.log("dataToSend for assign -- ", dataToSend);
		// API -- 
		axios
			.post(path + 'operator-management/assign-operator-to-company', dataToSend)
			.then(serverResponse => {
				let res = serverResponse.data;
				if (!res.isError) {

					this.setState({
						loading: false,
						showSuccessAlert: true,
						successAlertMessage: "Successfully assigned"
					}, () => {
						this.getUserList();
						this.getAssignedOperators();
					});

				} else {
					this.setState({
						loading: false,
						showErrorAlert: true,
						errorAlertMessage: "Sorry! Try again later."
					});

				}

			})
			.catch(error => {
				console.log(error);
			})

	}

	unAssignUser = async (index) => {

		let { assignedUsers } = this.state;
		let user = assignedUsers[index];
		let { userId, conferenceId } = user;

		if (userId && conferenceId) {

			this.setState({ loading: true });
			let dataToSend = {
				"userId": userId,
				"conferenceId": conferenceId,
				"companyId": this.state.companyId
			};
			// API -- 
			axios
				.post(path + 'operator-management/un-assign-operator-from-company', dataToSend)
				.then(serverResponse => {
					let res = serverResponse.data;
					if (!res.isError) {
						this.setState({
							showSuccessAlert: true,
							successAlertMessage: "Successfully un-assigned"
						}, () => {
							this.getUserList();
							this.getAssignedOperators();
						});
					} else {
						this.setState({
							showErrorAlert: true,
							errorAlertMessage: "Sorry! Try again later."
						});
					}
				})
				.catch(error => {
					console.log(error);
				})

		} else {
			console.log("error in getConferenceList", user);
			this.setState({
				showErrorAlert: true,
				errorAlertMessage: "Sorry! Something is wrong."
			});
		}

	}


	getConferenceForUnassign = (userId) => {

		return new Promise((resolve, reject) => {
			let hasValue = false;
			let indexVal = '';
			let assignedConferenceId = '';
			let assignedConferenceTitle = '';
			let { conferenceList } = this.state;

			conferenceList.map((value, index) => {
				let operatorArray = value.operator;

				let operatorStatus = _.filter(operatorArray, { 'userId': userId })[0];
				// operatorStatus ? indexVal = index : indexVal = '';

				if (operatorStatus) {

					hasValue = true;
					indexVal = index;
				}
			})

			if (hasValue) {
				assignedConferenceId = conferenceList[indexVal] && conferenceList[indexVal]._id;
				assignedConferenceTitle = conferenceList[indexVal] && conferenceList[indexVal].conferenceTitle;

				resolve({
					"isError": false,
					"response": assignedConferenceId,
					"conferenceTitle": assignedConferenceTitle
				});
			} else {
				resolve({
					"isError": true
				});
			}

		})

	}

	clearState = () => {

		this.setState({
			selectedUser: {},
			selectedUserId: '',

			searchString: '',
			searching: false,

			selectedConference: {},
			selectedConferenceId: '',

			conferenceList: [],

			showAssign: false,

			showAdminOperator: false,
			isActive: false,
			fname: '',
			lname: '',
			email: '',

			showSuccessAlert: false,
			successAlertMessage: '',
			showErrorAlert: false,
			errorAlertMessage: '',
			confirmUnassign: false,
			unassignMessage: ''
		});

	}

	pageChangePrev = () => {

		this.setState({
			pageNo_1: (parseFloat(this.state.pageNo_1) - 1)
		}, () => {
			this.getUserList();
			// this.getAssignedOperators();
		});

	}

	pageChangeNext = () => {

		this.setState({
			pageNo_1: (parseFloat(this.state.pageNo_1) + 1)
		}, () => {
			this.getUserList();
			// this.getAssignedOperators();
		});

	}

	pageChangePrevAssigned = () => {

		this.setState({
			pageNo_2: (parseFloat(this.state.pageNo_2) - 1)
		}, () => {
			this.getAssignedOperators();
		});

	}

	pageChangeNextAssigned = () => {

		this.setState({
			pageNo_2: (parseFloat(this.state.pageNo_2) + 1)
		}, () => {
			this.getAssignedOperators();
		});

	}


	sideMenuToggle() {
		let { openSideMenu } = this.state;
		openSideMenu = !openSideMenu;
		this.setState({ openSideMenu });
	}

	mobMenuClick = () => {
		console.log('mobMenuClick');
		this.sideMenuToggle();
	}


	render() {
		let { loading, searching, users, assignedUsers, conferenceList, fname, lname, email, isActive, pageNo_1, pageNo_2, totalPage, totalAssigned } = this.state;

		return (
			<main>

				<section className="user-mngnt-wrap">
					<div className="container-fluid">
						<div className="row">
							<Navbar routTo="/adminoperatorpool" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />
							<div className="col-xl-10 col-lg-9 p-0">
								<div className="mobile-menu-header">
									{/* <a className="back-arw" onClick={() => console.log('<-BACK')}><i className="fas fa-arrow-left"></i></a> */}
									<a href="JavaScript:Void(0);" className="menu-tgl" onClick={() => this.sideMenuToggle()}><img src="images/menu-tgl.png" alt="" /></a>
								</div>

								<div className="mng-full-list orga-mng">
									<div className="mng-full-srch">
										{/* <div className="row">
											<div className="col-md-10">
												<div className="srch-wrap">
													<form>
														<input
															type="text"
															name=""
															className="form-control"
															placeholder="Search user by name"
															value={this.state.searchString} onChange={(event) => this.setState({ searchString: event.target.value })}
														/>
														{
															this.state.searching ?
																<input type="button" onClick={(event) => this.clearSearch(event)}
																	data-tip='Clear'
																/>
																:
																<input type="submit" onClick={(event) => this.searchUsers(event)}
																	data-tip='Search'
																/>
														}
														<ReactTooltip
															effect="float"
															place="top"
															data-border="true"
														/>
													</form>
												</div>
											</div>

											<div className="col-md-2">

											</div>
										</div> */}
									</div>


									<div className="mng-full-srch">
										<span style={{ color: "#0083C6" }}><b>Available operators: </b></span>
									</div>

									<div className="mng-full-table">
										<div className="row">
											<div className="col-md-10">
												<div className="mng-full-table-hdr">
													<div className="row">
														<div className="col-md-4 border-rt">
															<h6>Name</h6>
														</div>
														<div className="col-md-4 border-rt">
															<h6>Service</h6>
														</div>
														<div className="col-md-4">
															<h6>Availability</h6>
														</div>
													</div>
												</div>
											</div>

											<div className="col-md-2">

											</div>

										</div>

										{users && users.length > 0 ? users.map((member, index) => {
											return <div className="row" key={index}>
												<div className="col-md-10">

													<div className="mng-full-table-row">
														<div className="row">
															<div className="col-md-4 border-rt">
																<h6>Name</h6>
																<p>
																	<a href="JavaScript:Void(0);" onClick={() => { this.viewAdminOperator(index) }}>
																		{member.fname + ' ' + member.lname}
																	</a>
																</p>
															</div>
															<div className="col-md-4 border-rt">
																<h6>Service</h6>
																<p>{member.service}</p>
															</div>
															<div className="col-md-4">
																<h6>Availability</h6>
																<p>{member.isAssigned ? "Unavailable" : "Available"}</p>
															</div>
														</div>
													</div>

							   					</div>

												<div className="col-md-2">
													{!member.isAssigned ?

														<a href="JavaScript:Void(0);" className="btn w-100 h-auto-btn"
															data-tip='Assign'
															onClick={() => { this.openAssignModal(index) }}
														>
															Assign
														</a>
														:
														<a href="JavaScript:Void(0);" className="btn border w-100 h-auto-btn"
															data-tip='Unassign'
															onClick={() => { this.unAssignUser(index) }}
														>
															Unassign
														</a>
													}
													<ReactTooltip
														effect="float"
														place="top"
														data-border="true"
													/>
												</div>

											</div>

										})
											: null}

									</div>

									<br /><br />
									{!searching ?
										<div className="form-group">
											{!loading && parseFloat(pageNo_1) > 1 ?
												<span style={{ color: "#007bff", cursor: "pointer" }}
													onClick={() => { this.pageChangePrev() }}
												>
													<b>{"<< Prev"}</b>
												</span>
												:
												null}
											&nbsp;&nbsp;|&nbsp;&nbsp;
											{!loading && (parseFloat(pageNo_1) < parseFloat(totalPage)) ?
												<span style={{ color: "#007bff", cursor: "pointer" }}
													onClick={() => { this.pageChangeNext() }}
												>
													<b>{"Next >>"}</b>
												</span>
												:
												null}
											{loading ?
												<a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
													<i className="fa fa-spinner fa-spin" ></i>
												</a>
												:
												null}

										</div>
										:
										null}

									<br /><br /><br />
									<div className="mng-full-srch">
										<span style={{ color: "#0083C6" }}><b>Already assigned operators: </b></span>
									</div>

									<div className="mng-full-table">
										<div className="row">
											<div className="col-md-10">
												<div className="mng-full-table-hdr">
													<div className="row">
														<div className="col-md-4 border-rt">
															<h6>Name</h6>
														</div>
														<div className="col-md-4 border-rt">
															<h6>Service</h6>
														</div>
														<div className="col-md-4">
															<h6>Availability</h6>
														</div>
													</div>
												</div>
											</div>

											<div className="col-md-2">

											</div>

										</div>

										{assignedUsers && assignedUsers.length > 0 ? assignedUsers.map((member, index) => {
											return <div className="row" key={index}>
												<div className="col-md-10">

													<div className="mng-full-table-row">
														<div className="row">
															<div className="col-md-4 border-rt">
																<h6>Name</h6>
																<p>
																	<a href="JavaScript:Void(0);" onClick={() => { this.viewAdminOperator(index) }}>
																		{member.fname + ' ' + member.lname}
																	</a>
																</p>
															</div>
															<div className="col-md-4 border-rt">
																<h6>Service</h6>
																<p>{member.service}</p>
															</div>
															<div className="col-md-4">
																<h6>Availability</h6>
																<p>{member.isAssigned ? "Unavailable" : "Available"}</p>
															</div>
														</div>
													</div>

												</div>

												<div className="col-md-2">
													{!member.isAssigned ?

														<a href="JavaScript:Void(0);" className="btn w-100 h-auto-btn"
															data-tip='Assign'
															onClick={() => { this.openAssignModal(index) }}
														>
															Assign
														</a>
														:
														<a href="JavaScript:Void(0);" className="btn border w-100 h-auto-btn"
															data-tip='Unassign'
															onClick={() => { this.unAssignUser(index) }}
														>
															Unassign
														</a>
													}
													<ReactTooltip
														effect="float"
														place="top"
														data-border="true"
													/>
												</div>

											</div>

										})
											: null}

									</div>

									<br /><br />
									{!searching ?
										<div className="form-group">
											{!loading && parseFloat(pageNo_2) > 1 ?
												<span style={{ color: "#007bff", cursor: "pointer" }}
													onClick={() => { this.pageChangePrevAssigned() }}
												>
													<b>{"<< Prev"}</b>
												</span>
												:
												null}
											&nbsp;&nbsp;|&nbsp;&nbsp;
											{!loading && (parseFloat(pageNo_2) < parseFloat(totalAssigned)) ?
												<span style={{ color: "#007bff", cursor: "pointer" }}
													onClick={() => { this.pageChangeNextAssigned() }}
												>
													<b>{"Next >>"}</b>
												</span>
												:
												null}
											{loading ?
												<a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
													<i className="fa fa-spinner fa-spin" ></i>
												</a>
												:
												null}

										</div>
										:
										null}


								</div>
							</div>
						</div>
					</div>

				</section>


				<Modal show={this.state.showAdminOperator} onHide={() => { this.clearState() }}>
					<Modal.Header closeButton>
						<Modal.Title>View operator</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<div className="row">

							<div className="col-md-6">
								<div className="form-group">
									<label>First Name:</label>
									<input
										type="text"
										name="fname"
										value={fname}
										className="form-control"
										placeholder="Enter First Name"
										readOnly={true}
									/>
									<span style={{ color: 'red' }}>{this.state.fnameError ? `* ${this.state.fnameError}` : ''}</span>
								</div>
							</div>

							<div className="col-md-6">
								<div className="form-group">
									<label>Last Name:</label>
									<input
										type="text"
										name="lname"
										value={lname}
										className="form-control"
										placeholder="Enter Last Name"
										readOnly={true}

									/>
									<span style={{ color: 'red' }}>{this.state.lnameError ? `* ${this.state.lnameError}` : ''}</span>
								</div>
							</div>

							<div className="col-md-12">
								<div className="form-group">
									<label>Email Address:</label>
									<input
										type="email"
										name="email"
										value={email}
										className="form-control"
										placeholder="Enter Email Address"
										readOnly={true}
									/>
									<span style={{ color: 'red' }}>{this.state.emailError ? `* ${this.state.emailError}` : ''}</span>
								</div>
							</div>

							<div className="col-md-8">
								<div className="form-group">
									<label>Status: {isActive ? "Active" : "In-Active"}</label>

								</div>
							</div>

						</div>
					</Modal.Body>
					<Modal.Footer>

						<Button variant="primary" onClick={() => this.clearState()}>
							Cancel
              			</Button>

					</Modal.Footer>
				</Modal>

				<Modal show={this.state.showAssign} size={"lg"} onHide={() => { this.clearState() }}>
					<Modal.Header closeButton>
						<Modal.Title>Assign Operator</Modal.Title>
					</Modal.Header>
					<Modal.Body style={{ minHeight: '400px' }}>

						<div className="mng-full-table modal-adrs-book">
							<div className="row">
								<div className="col-md-12">

									<div className="row pt-2 pb-2">
										<div className="col-md-8">
											<h4>Assign operator to conference</h4>
										</div>

									</div>
									{/* For adding member End */}

									<div className="mng-full-table-hdr">
										<div className="row">
											<div className="col-md-2 border-rt">
												<h6>Select</h6>
											</div>
											<div className="col-md-5 border-rt">
												<h6>conference Name</h6>
											</div>
											<div className="col-md-5">
												<h6>Participants</h6>
											</div>

										</div>
									</div>

								</div>

							</div>
							{conferenceList.length > 0 && conferenceList.map((member, index) => {

								return <div className="row" key={index}>
									{member.phoneNumber != '' ?
										<div className="col-md-12">

											<div className="mng-full-table-row">

												<div className="row">
													<div className="col-md-2 border-rt">
														<h6>Select</h6>

														{member.isChecked ?
															<input type="checkbox" name="checkList" checked={true} onChange={(event) => this.selectConference(event, index)} />
															:
															<input type="checkbox" name="checkList" checked={false} onChange={(event) => this.selectConference(event, index)} />
														}
													</div>

													<div className="col-md-5 border-rt">
														<h6>Conference Name</h6>
														<p><a href="#">{member.conferenceTitle}</a></p>
													</div>
													<div className="col-md-5">
														<h6>Participants</h6>
														<p>{member.participants && member.participants.length}</p>
													</div>

												</div>

											</div>

										</div>
										: null}
								</div>

							})

							}

						</div>

					</Modal.Body>
					<Modal.Footer>
						{!loading ?
							<Button variant="primary" onClick={this.assignOperator}> Assign </Button>
							:
							null}
						{loading ?
							<Button variant="primary"> <i className="fa fa-spinner fa-spin" ></i> </Button>
							:
							null}

					</Modal.Footer>
				</Modal>

				<Modal show={this.state.confirmUnassign} onHide={() => this.clearState()}>
					<Modal.Header closeButton>
						<Modal.Title>Confirm</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<h5 style={{ color: 'green' }}>{`${this.state.unassignMessage}`}</h5>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="primary" onClick={() => this.unassignFromConference()}>
							Yes
            			</Button>
						<Button variant="secondary" onClick={() => this.clearState()}>
							No
            			</Button>
					</Modal.Footer>
				</Modal>

				<Modal show={this.state.showSuccessAlert} onHide={() => this.clearState()}>
					<Modal.Header closeButton>
						<Modal.Title>OK</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<h5 style={{ color: 'green' }}>{`${this.state.successAlertMessage}`}</h5>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="primary" onClick={() => this.clearState()}>
							Ok
            			</Button>
					</Modal.Footer>
				</Modal>

				<Modal show={this.state.showErrorAlert} onHide={() => this.clearState()}>
					<Modal.Header closeButton>
						<Modal.Title>OK</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<h5 style={{ color: 'red' }}>{`${this.state.errorAlertMessage}`}</h5>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="primary" onClick={() => this.clearState()}>
							Ok
            			</Button>
					</Modal.Footer>
				</Modal>


			</main >
		);
	}

}

export default checkAuthentication(AdminOperatorPool);