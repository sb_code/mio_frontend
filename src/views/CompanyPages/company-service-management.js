import React, { Component } from "react";
import Navbar from "../Component/Navbar";
import ProgressBar from 'react-bootstrap/ProgressBar';
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button } from "react-bootstrap"
import _ from 'lodash';
import { CSVLink, CSVDownload } from "react-csv";
import ReactTooltip from 'react-tooltip'; // For tool tip 

import { checkAuthentication } from '../Component/Authentication';

var path = cred.API_PATH;

export class CompanyServices extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userId: '',
            userType: '',
            companyId: '',
            companyName: '',
            companyType: '',

            openSideMenu: false,
            searchString: '',
            searchType: '',
            searching: false,

            rowId: '',
            services: [],
            details: '',

            loading: false,

            serviceName: '',
            costPerMin: 0.0,
            multiplier: 0.0,
            pricePerMin: 0.0,
            selectedService: '',
            serviceCreatedBy: '',

            serviceNameError: '',
            costPerMinError: '',
            pricePerMinError: '',
            multiplierError: '',

            noOfSeats: '0',
            pricePerSeat: '0.00',
            trialPeriod: '0',
            currency: '',
            vat: '',
            paymentGateway: '',
            sipProvider: '',
            fileUploaded1: '',
            fileUploaded2: '',
            voiceUrl: '',
            brandUrl: '',

            companyTypeError: '',
            trialPeriodError: '',
            pricePerSeatError: '',
            noOfSeatsError: '',
            currencyError: '',

            editService: false,
            editServiceConfig: false,
            viewService: false,
            deleteService: false,
            deleteOthService: false,

            showErrorAlert: false,
            errorAlertMessage: '',
            showSuccessAlert: false,
            successAlertMessage: '',

        }
    }

    componentDidMount() {
        document.body.classList.remove("sign-bg");
        document.body.classList.add("grey-bg");

        let storage = localStorage.getItem("MIO_Local");
        storage = storage ? JSON.parse(storage) : null;
        if (storage && storage.data.userId) {
            this.setState({
                userId: storage.data.userId,
                userType: storage.data.userType,
                companyId: storage.data.companyId,
                companyName: storage.data.companyName,
                companyType: storage.data.companyType
            }, () => {
                this.getServiceList()
                console.log('user management--', this.state.userType);
            });

            // console.log(this.parseJwt(storage.token));
            let tokenData = this.parseJwt(storage.token);

        }

    }


    getServiceList = () => {
        this.setState({
            loading: true
        });
        let dataToSend = {
            "companyId": this.state.companyId
        }
        // API - 
        axios
            // .post(path + 'services/get-base-services') 
            .post(path + 'user/get-company-by-id', dataToSend)
            .then(serverResponse => {

                let res = serverResponse.data;
                if (!res.isError) {
                    console.log('getting response----', res.details);

                    let services = res.details && res.details.services;
                    this.setState({
                        services: services,
                        details: res.details,
                        loading: false
                    }, () => {
                        console.log('getting response----', this.state.details)
                    });
                }
            })
            .catch(error => {
                console.log(error);
            })

    }


    viewCompanyService = (index) => {

        let { services } = this.state;
        let serviceDetails = services[index];

        this.setState({
            viewService: true,
            serviceName: serviceDetails.serviceName,
            pricePerMin: serviceDetails.pricePerMinute
        });

    }


    serviceConfigEdit = () => {

        let { details } = this.state;

        let { _id, companyType, noOfSeats, pricePerSeat, trialPeriod, currency, paymentGateway, sipProvider, vat, customVoiceUrl, brandVoiceUrl } = details;

        this.setState({
            editServiceConfig: true,
            companyType: companyType,
            noOfSeats: noOfSeats,
            pricePerSeat: pricePerSeat,
            trialPeriod: trialPeriod,
            selectedService: _id,
            currency: currency,
            vat: vat,
            paymentGateway: paymentGateway,
            sipProvider: sipProvider,
            voiceUrl: customVoiceUrl,
            brandUrl: brandVoiceUrl
        });

    }


    onFileChange = (e) => {
        console.log("uploading file name is===", e.target.name);
        let fileName = e.target.name;
        let file;
        if (fileName == 'welcome') {
            file = this.refs.welcome.files[0];
        } else if (fileName == 'branding') {
            file = this.refs.branding.files[0];
        }

        if (file) {
            // console.log("uploaded local file = ", file);
            // For checking uploaded file extension -- 
            let blnValid = false;
            let sizeValid = false;
            //For checking  file size ---
            let sizeInByte = file.size;
            let sizeInKB = parseFloat(parseFloat(sizeInByte) / 1024).toFixed('2');
            if (sizeInKB <= 3100) {
                sizeValid = true;
            } else {
                sizeValid = false;
            }

            let sFileName = file.name;
            var _validFileExtensions = [".mp3", ".wav"];
            for (var j = 0; j < _validFileExtensions.length; j++) {
                var sCurExtension = _validFileExtensions[j];
                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                    blnValid = true;
                    break;
                }
            }

            if (!blnValid) {
                this.setState({
                    showErrorAlert: true,
                    errorAlertMessage: "File type should be .mp3"
                })
            } else if (!sizeValid) {
                this.setState({
                    showErrorAlert: true,
                    errorAlertMessage: "File size shouldn't be more than 3mb."
                });

            } else {
                this.setState({
                    loading: true
                });
                var reader = new FileReader();
                var url = reader.readAsDataURL(file);
                // console.log("uploading file details == ", url);
                // reader.onloadend = function (e) {
                //     this.setState({
                //         voiceUrl: [reader.result]
                //     })
                // }.bind(this);

                const data = new FormData();
                // data.append('file', this.refs.file.files[0]);
                data.append('file', file);
                // data.append('companyId', this.state.companyId);

                // console.log("uploading payload for file == "+ data);

                axios
                    .post(path + "services/upload-custom-voice",
                        data,
                        {
                            headers: { 'Content-Type': 'multipart/form-data' },
                            onUploadProgress: (progressEvent) => {
                                // console.log("raw upload loader ---------", progressEvent.loaded, progressEvent.total);

                                let uploadPercentage = parseInt(Math.round((progressEvent.loaded / progressEvent.total) * 100));
                                if (fileName == 'welcome') {
                                    this.setState({ fileUploaded1: uploadPercentage });
                                } else if (fileName == 'branding') {
                                    this.setState({ fileUploaded2: uploadPercentage });
                                }

                            }

                        }
                    )
                    .then(serverResponse => {

                        let res = serverResponse.data;
                        if (!res.isError) {
                            console.log("upload response ===", res);
                            let voiceUrl = '';
                            let brandUrl = '';

                            if (fileName == 'welcome') {
                                voiceUrl = res.detail && res.detail.url;

                                this.setState({
                                    voiceUrl: voiceUrl,
                                    showSuccessAlert: true,
                                    successAlertMessage: 'Successfully uploaded.',
                                    fileUploaded1: '',
                                    fileUploaded2: '',
                                    loading: false
                                }, () => {
                                    console.log("after file upload....", this.state.brandUrl);
                                })

                            } else if (fileName == 'branding') {
                                brandUrl = res.detail && res.detail.url;

                                this.setState({
                                    brandUrl: brandUrl,
                                    showSuccessAlert: true,
                                    successAlertMessage: 'Successfully uploaded.',
                                    fileUploaded1: '',
                                    fileUploaded2: '',
                                    loading: false
                                }, () => {
                                    console.log("after file upload....", this.state.brandUrl);
                                })

                            }


                        } else {
                            this.setState({
                                showErrorAlert: true,
                                errorAlertMessage: 'Sorry! something wrong...',
                                fileUploaded1: '',
                                fileUploaded2: '',
                                loading: false
                            })
                        }

                    })
                    .catch(error => {
                        console.log(error);
                    })

            }

        }

    }


    editOthService = (event) => {

        let { details, companyId, companyType, companyName, trialPeriod, pricePerSeat, noOfSeats, currency, paymentGateway, sipProvider, vat, voiceUrl, brandUrl } = this.state;

        !currency ? this.setState({ currencyError: "Please enter valid currency." }) : this.setState({ currencyError: "" });
        !paymentGateway ? this.setState({ gatewayError: "Please enter valid paymentGateway." }) : this.setState({ gatewayError: "" });
        // !sipProvider ? this.setState({ sipProviderError: "Please enter valid sipProvider." }) : this.setState({ sipProviderError: "" });
        !vat ? this.setState({ vatError: "Please enter valid vat." }) : this.setState({ vatError: "" });

        if (currency && paymentGateway && vat) {
            this.setState({
                loading: true
            });

            let dataToSend = {
                companyId: companyId,
                companyName: this.state.companyName,
                companyType: companyType,
                noOfSeats: noOfSeats,
                pricePerSeat: pricePerSeat,
                currency: currency,
                vat: vat,
                trialPeriod: trialPeriod,
                paymentGateway: paymentGateway,
                sipProvider: sipProvider,
                customVoiceUrl: voiceUrl,
                brandVoiceUrl: brandUrl
            };

            axios
                .post(path + 'user/update-company-details', dataToSend)
                .then(serverResponse => {

                    let res = serverResponse.data;
                    if (!res.isError) {
                        // console.log("editing service configue---", res);
                        this.setState({
                            showSuccessAlert: true,
                            successAlertMessage: "Successfully updated."
                        },()=>{
                            this.clearState();
                            this.getServiceList();
                        });
                        
                    } else {
                        // alert("Sorry! Something is wrong.");
                        this.setState({
                            showErrorAlert: true,
                            errorAlertMessage: "Something is wrong."
                        },()=>{
                            this.clearState();
                        });
                        
                    }

                })
                .catch(error => {
                    console.log(error);
                })

        }else{
            alert("Mandatory field is blank.");
        }

    }


    hideServiceConfigModal = () => {

        this.setState({

            loading: false,
            editServiceConfig: false,
            companyType: '',
            noOfSeats: '',
            pricePerSeat: '',
            trialPeriod: '',
            selectedService: '',
            currency: '',
            vat: '',
            paymentGateway: '',
            sipProvider: '',
            voiceUrl: '',
            fileUploaded1: '',
            fileUploaded2: '',

            companyTypeError: '',
            noOfSeatsError: '',
            pricePerSeatError: '',
            trialPeriodError: '',
            currencyError: '',
            gatewayError: '',
            sipProviderError: '',


        });

    }

    clearState = () => {
        this.setState({
            loading: false,

            serviceName: '',
            costPerMin: '',
            multiplier: '',
            pricePerMin: '',
            selectedService: '',

            editService: false,
            viewService: false,

            currency: '',
            vat: '',
            voiceUrl: '',
            brandUrl: '',
            fileUploaded1: '',
            fileUploaded2: '',

            editServiceConfig: false,

        });
    }

    hideUploadAlert() {
        this.setState({
            showErrorAlert: false,
            errorAlertMessage: '',
            showSuccessAlert: false,
            successAlertMessage: ''
        })
    }


    sideMenuToggle() {
        let { openSideMenu } = this.state;
        openSideMenu = !openSideMenu;
        this.setState({ openSideMenu });
    }


    mobMenuClick() {
        this.sideMenuToggle();
        console.log('mobMenuClick');
    }


    parseJwt = (token) => {
        if (!token) { return; }
        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace('-', '+').replace('_', '/');
        return JSON.parse(window.atob(base64));
    }


    render() {
        let { details, serviceName, costPerMin, multiplier, pricePerMin, services, services1, companyType, noOfSeats, pricePerSeat, trialPeriod, currency, vat, paymentGateway, sipProvider, voiceUrl, brandUrl } = this.state;
        return (
            <main>

                <section className="user-mngnt-wrap">
                    <div className="container-fluid">
                        <div className="row">
                            <Navbar routTo="/companyservicemanagement" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />

                            <div className="col-xl-10 col-lg-9 p-0">

                                <div className="mobile-menu-header">
                                    {/* <a href="#" className="back-arw"><i className="fas fa-arrow-left"></i></a> */}
                                    <a href="JavaScript:Void(0);" className="menu-tgl" onClick={() => this.sideMenuToggle()}>
                                        <img src="images/menu-tgl.png" alt="" />
                                    </a>
                                </div>

                                <div className="paymnt-mng-hdr" style={{ margin: "30px" }}>
                                    <div className="blnc">
                                        <div className="logobox"><img src="/images/logo-sign.png" alt="" /></div>
                                    </div>
                                    <div className="rchrg-btn">
                                        <p className="text-right" style={{ margin: "0" }}>
                                            Company Name: {this.state.companyName}
                                            <br />Company Type: {this.state.companyType}
                                        </p>
                                    </div>
                                </div>

                                <div className="mng-full-list" style={{ padding: "0px 50px" }}>
                                    <div className="mng-full-srch">
                                        <div className="row">

                                        </div>
                                    </div>

                                    <div className="mng-full-table">
                                        <div className="row">
                                            <div className="col-md-10">
                                                <div className="mng-full-table-hdr">
                                                    <div className="row">
                                                        <div className="col-md-6 border-rt">
                                                            <h6>Service Name</h6>
                                                        </div>
                                                        <div className="col-md-6">
                                                            <h6>Price Per Minute</h6>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                            <div className="col-md-2">
                                                <div className="mng-full-table-hdr">
                                                    <div className="row">
                                                        <div className="col-md-12">
                                                            <h6>View</h6>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        {services && services.length > 0 && services.map((value, index) => {

                                            return <div className="row" key={index}>
                                                <div className="col-md-10">

                                                    <div className="mng-full-table-row">
                                                        <div className="row">
                                                            <div className="col-md-6 border-rt">
                                                                <h6>Service Name</h6>
                                                                <p>
                                                                    <a href="JavaScript:Void(0);" onClick={() => { this.viewCompanyService(index) }}>
                                                                        {value.serviceName}
                                                                    </a>
                                                                </p>
                                                            </div>
                                                            <div className="col-md-6">
                                                                <h6>Price Per Minute</h6>
                                                                <p>{value.pricePerMinute}</p>
                                                            </div>

                                                            <div className="mobile-ad-edt-btns">
                                                                <ul>
                                                                    <li>
                                                                        <a href="JavaScript:Void(0);"
                                                                            onClick={() => { this.viewCompanyService(index) }}
                                                                        >
                                                                            <i className="fas fa-eye"></i>
                                                                        </a>
                                                                    </li>

                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div className="col-md-2">
                                                    <div className="mng-full-table-row add-edt text-center">
                                                        <div className="row">
                                                            <div className="col-md-12">
                                                                <a href="JavaScript:Void(0);"
                                                                    // ref={ref => this.fooRef = ref}
                                                                    data-tip='View Service'
                                                                    onClick={() => { this.viewCompanyService(index) }}
                                                                // onFocus={() => { ReactTooltip.show(this.fooRef) }}
                                                                >
                                                                    <i className="fas fa-eye"></i>
                                                                </a>
                                                                <ReactTooltip
                                                                    effect="float"
                                                                    place="top"
                                                                    data-border="true"
                                                                />
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        })}

                                    </div>

                                </div>

                                <div className="mng-full-list">
                                    <div className="mng-full-srch">
                                        <div className="row">

                                        </div>
                                    </div>

                                    <div className="mng-full-table">
                                        <div className="row">
                                            <div className="col-md-10">
                                                <div className="mng-full-table-hdr">
                                                    <div className="row">
                                                        <div className="col-md-3 border-rt">
                                                            <h6>Company Type</h6>
                                                        </div>
                                                        <div className="col-md-3 border-rt">
                                                            <h6>No of Seat</h6>
                                                        </div>
                                                        <div className="col-md-3 border-rt">
                                                            <h6>Price Per Seat</h6>
                                                        </div>
                                                        <div className="col-md-3">
                                                            <h6>Currency</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="col-md-2">
                                                <div className="mng-full-table-hdr">
                                                    <div className="row">
                                                        <div className="col-md-12">
                                                            <h6>Configuration</h6>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="row">
                                            <div className="col-md-10">

                                                <div className="mng-full-table-row">
                                                    <div className="row">
                                                        <div className="col-md-3 border-rt">
                                                            <h6>Company Type</h6>
                                                            <p>
                                                                <a href="JavaScript:Void(0);" onClick={() => { this.serviceConfigEdit() }}>
                                                                    {details.companyType}
                                                                </a>
                                                            </p>
                                                        </div>
                                                        <div className="col-md-3 border-rt">
                                                            <h6>No of Seat</h6>
                                                            <p>{details.noOfSeats}</p>
                                                        </div>
                                                        <div className="col-md-3 border-rt">
                                                            <h6>Price Per Seat</h6>
                                                            <p>{details.pricePerSeat}</p>
                                                        </div>
                                                        <div className="col-md-3">
                                                            <h6>Currency</h6>
                                                            <p>{details.currency}</p>
                                                        </div>
                                                        <div className="mobile-ad-edt-btns">
                                                            <ul>
                                                                <li>
                                                                    <a href="JavaScript:Void(0);"
                                                                        data-tip='Configuration'
                                                                        onClick={() => { this.serviceConfigEdit() }}
                                                                    >
                                                                        <i className="fas fa-cog"></i>
                                                                    </a>
                                                                    <ReactTooltip
                                                                        effect="float"
                                                                        place="top"
                                                                        data-border="true"
                                                                    />
                                                                </li>

                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <div className="col-md-2">
                                                <div className="mng-full-table-row add-edt text-center">
                                                    <div className="row">
                                                        <div className="col-md-12">
                                                            <a href="JavaScript:Void(0);"
                                                                // ref={ref => this.fooRef = ref}
                                                                data-tip='Configuration'
                                                                onClick={() => { this.serviceConfigEdit() }}
                                                            // onFocus={() => { ReactTooltip.show(this.fooRef) }}
                                                            >
                                                                <i className="fas fa-cog"></i>
                                                            </a>
                                                            <ReactTooltip
                                                                effect="float"
                                                                place="top"
                                                                data-border="true"
                                                            />
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>
                </section>

                <Modal show={this.state.viewService} onHide={() => { this.clearState() }}>
                    <Modal.Header closeButton>
                        <Modal.Title> View Service</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="row">

                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>Service Name:</label>

                                    <input
                                        name="serviceName"
                                        value={serviceName}
                                        className="form-control"
                                        readOnly={true}
                                    />

                                    {/* <span style={{ color: 'red' }}>{this.state.serviceNameError ? `* ${this.state.serviceNameError}` : ''}</span> */}
                                </div>
                            </div>

                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>Price Per Minute:</label>
                                    <input
                                        type="text"
                                        name="pricePerMin"
                                        value={pricePerMin}
                                        className="form-control"
                                        placeholder="Enter Price Per Minute"
                                        // onChange={(event) => { this.changePricePerMin(event) }}
                                        // onFocus={() => this.setState({ pricePerMinError: "" })}
                                        readOnly={true}
                                    />
                                    {/* <span style={{ color: 'red' }}>{this.state.pricePerMinError ? `* ${this.state.pricePerMinError}` : ''}</span> */}
                                </div>
                            </div>

                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="primary" onClick={this.clearState}>
                            Cancel
                        </Button>
                    </Modal.Footer>
                </Modal>

                <Modal show={this.state.editServiceConfig} onHide={() => { this.hideServiceConfigModal() }}>
                    <Modal.Header closeButton>
                        <Modal.Title> Edit Service Configuration</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="row">

                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>Company Type:</label>
                                    <input
                                        name="companyType"
                                        value={companyType}
                                        className="form-control"
                                        // onChange={event => this.setState({ companyType: event.target.value })}
                                        // onFocus={() => this.setState({ companyTypeError: "" })}
                                        readOnly={true}
                                    />
                                    <span style={{ color: 'red' }}>{this.state.companyTypeError ? `* ${this.state.companyTypeError}` : ''}</span>
                                </div>
                            </div>

                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>No of Seats:</label>
                                    <input
                                        type="text"
                                        name="noOfSeats"
                                        value={noOfSeats}
                                        className="form-control"
                                        placeholder="Enter no of seats"
                                        onChange={event => this.setState({ noOfSeats: event.target.value })}
                                        // onChange={(event) => { this.changecostPerMin(event) }}
                                        onFocus={() => this.setState({ noOfSeatsError: "" })}
                                        readOnly={true}
                                    />
                                    <span style={{ color: 'red' }}>{this.state.noOfSeatsError ? `* ${this.state.noOfSeatsError}` : ''}</span>
                                </div>
                            </div>

                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>Price Per Seat:</label>
                                    <input
                                        type="text"
                                        name="pricePerSeat"
                                        value={pricePerSeat}
                                        className="form-control"
                                        placeholder="Enter Price"
                                        onChange={event => this.setState({ pricePerSeat: event.target.value })}
                                        // onChange={(event) => { this.changeMultiplier(event) }}
                                        onFocus={() => this.setState({ pricePerSeatError: "" })}
                                        readOnly={true}
                                    />
                                    <span style={{ color: 'red' }}>{this.state.pricePerSeatError ? `* ${this.state.pricePerSeatError}` : ''}</span>
                                </div>
                            </div>
                            
                            {companyType && companyType == `${cred.COMPANY_TYPE_FREE_TYPE}` ?
                                <div className="col-md-6">
                                    <div className="form-group">
                                        <label>Trial Period(days):</label>
                                        <input
                                            type="text"
                                            name="trialPeriod"
                                            value={trialPeriod}
                                            className="form-control"
                                            placeholder="Enter Price"
                                            onChange={event => this.setState({ trialPeriod: event.target.value })}
                                            // onChange={(event) => { this.changeMultiplier(event) }}
                                            onFocus={() => this.setState({ trialPeriodError: "" })}
                                            readOnly={true}
                                        />
                                        <span style={{ color: 'red' }}>{this.state.trialPeriodError ? `* ${this.state.trialPeriodError}` : ''}</span>
                                    </div>
                                </div>
                                : null}

                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>Currency:</label>
                                    <input
                                        type="text"
                                        name="currency"
                                        value={currency}
                                        className="form-control"
                                        placeholder="Currency"
                                        readOnly={true}
                                    />
                                    {/* <select
                                        name="currency"
                                        value={currency}
                                        className="form-control"
                                        onChange={event => this.setState({ currency: event.target.value })}
                                        onFocus={() => this.setState({ currencyError: "" })}
                                        readOnly={true}
                                        >
                                        <option value="">Select Currency</option>
                                        <option value={cred.CURRENCY.usd}>{cred.CURRENCY.usd}</option>
                                        <option value={cred.CURRENCY.gbp}>{cred.CURRENCY.gbp}</option>
                                        <option value={cred.CURRENCY.naira}>{cred.CURRENCY.naira}</option>
                                        <option value={cred.CURRENCY.euro}>{cred.CURRENCY.euro}</option>
                                    </select> */}

                                    <span style={{ color: 'red' }}>{this.state.currencyError ? `* ${this.state.currencyError}` : ''}</span>
                                </div>
                            </div>

                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>Payment Gateway:</label>
                                    <select
                                        name="paymentGateway"
                                        value={paymentGateway}
                                        className="form-control"
                                        onChange={event => this.setState({ paymentGateway: event.target.value })}
                                        onFocus={() => this.setState({ gatewayError: "" })}
                                        readOnly = {true}
                                    >
                                        {this.state.currency && this.state.currency == cred.CURRENCY.naira ?
                                            <>
                                                <option value="">Select Any Gateway</option>
                                                <option value={cred.PAYMENT_GATEWAY.PAYSTACK}>{cred.PAYMENT_GATEWAY.PAYSTACK}</option>
                                            </>
                                            :
                                            <>
                                                <option value="">Select Any Gateway</option>
                                                <option value={cred.PAYMENT_GATEWAY.STRIPE}>{cred.PAYMENT_GATEWAY.STRIPE}</option>
                                                <option value={cred.PAYMENT_GATEWAY.PAYPAL}>{cred.PAYMENT_GATEWAY.PAYPAL}</option>
                                            </>
                                        }
                                        {/* <option value="">Select Any Gateway</option>
                                        <option value={cred.PAYMENT_GATEWAY.PAYSTACK}>{cred.PAYMENT_GATEWAY.PAYSTACK}</option>
                                        <option value={cred.PAYMENT_GATEWAY.STRIPE}>{cred.PAYMENT_GATEWAY.STRIPE}</option>
                                        <option value={cred.PAYMENT_GATEWAY.PAYPAL}>{cred.PAYMENT_GATEWAY.PAYPAL}</option> */}
                                    </select>
                                    <span style={{ color: 'red' }}>{this.state.gatewayError ? `* ${this.state.gatewayError}` : ''}</span>
                                </div>
                            </div>

                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>VAT(%):</label>
                                    <input
                                        type="text"
                                        name="vat"
                                        value={vat}
                                        className="form-control"
                                        placeholder="Enter VAT"
                                        onChange={event => this.setState({ vat: event.target.value })}
                                        onFocus={() => this.setState({ vatError: "" })}
                                        readOnly={true}
                                    />
                                    <span style={{ color: 'red' }}>{this.state.vatError ? `* ${this.state.vatError}` : ''}</span>
                                </div>
                            </div>

                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>Welcome Tune: {voiceUrl}</label>
                                </div>
                            </div>

                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>Upload welcome tune(MP3/WAV)</label>
                                    <input
                                        type="file" name="welcome"
                                        ref="welcome"
                                        className="form-control"
                                        onChange={(e) => { this.onFileChange(e) }} multiple={false}
                                    // style= {{background: "#0083C6", color: "#fff"}}
                                    />
                                    {this.state.fileUploaded1 ?
                                        <ProgressBar striped variant="success" now={this.state.fileUploaded1} label={`${this.state.fileUploaded1}%`} />
                                        :
                                        null}
                                </div>
                            </div>

                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>Branding Voice: {brandUrl}</label>
                                </div>
                            </div>

                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>Upload branding tune(MP3/WAV)</label>
                                    <input
                                        type="file" name="branding"
                                        ref="branding"
                                        className="form-control"
                                        onChange={(e) => { this.onFileChange(e) }} multiple={false}
                                    // style= {{background: "#0083C6", color: "#fff"}}
                                    />
                                    {this.state.fileUploaded2 ?
                                        <ProgressBar striped variant="success" now={this.state.fileUploaded2} label={`${this.state.fileUploaded2}%`} />
                                        :
                                        null}
                                </div>
                            </div>

                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        {!this.state.loading ?
                            <Button variant="primary" onClick={(event) => this.editOthService(event)}>
                                Save
                            </Button>
                            :
                            null}
                        {this.state.loading ?
                            <Button variant="primary" >
                                <i className="fa fa-spinner fa-spin" ></i>
                            </Button>
                            :
                            null}
                    </Modal.Footer>
                </Modal>

                <Modal show={this.state.showSuccessAlert} onHide={() => this.hideUploadAlert()}>
                    <Modal.Header closeButton>
                        <Modal.Title>OK</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <h5 style={{ color: 'green' }}>{`${this.state.successAlertMessage}`}</h5>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="primary" onClick={() => this.hideUploadAlert()}>
                            Ok
                        </Button>
                    </Modal.Footer>
                </Modal>

                <Modal show={this.state.showErrorAlert} onHide={() => this.hideUploadAlert()}>
                    <Modal.Header closeButton>
                        <Modal.Title>OK</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <h5 style={{ color: 'red' }}>{`${this.state.errorAlertMessage}`}</h5>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="primary" onClick={() => this.hideUploadAlert()}>
                            Ok
                        </Button>
                    </Modal.Footer>
                </Modal>


            </main>
        );
    }

}

export default checkAuthentication(CompanyServices);