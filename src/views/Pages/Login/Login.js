import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import { Snackbar } from "@material-ui/core";
import { Modal, Button } from "react-bootstrap"
import MySnackbarContentWrapper from "../../Component/MaterialSnack";
import cred from "../../../cred.json";
import routeList from "../../../routeList.json";

// import io from 'socket.io-client';
// const socket = io('https://api.teamsmiths.co.uk');

// var path = 'http://localhost:3132/v1/' + 'user/';
var path = cred.API_PATH + "user/";
var path_con = cred.API_PATH + "conference/";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      loading: false,

      open: false,
      notiMessage: "",
      notiType: "info",
      showBrowse: false,

      loginAlert: false,
      loginAlertMessage: ""

    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  openModal = (type, msg) => {
    this.setState({
      open: true,
      notiMessage: msg,
      notiType: type
    });
  };

  handleModalClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({
      open: false
    });
    // setOpen(false);
  };

  componentDidMount() {
    document.body.classList.remove("grey-bg");
    document.body.classList.add("sign-bg");
    let storage = localStorage.getItem("MIO_Local");

    storage = storage ? JSON.parse(storage) : null;
    console.log(typeof storage);

    if (storage && storage.data && storage.data.userType) {
      let userType = storage.data.userType;
      let companyType = storage.data.companyType;
      // if user not select company type ['FREE_TYPE','STARTUP','ENTERPRISE', 'GUEST']
      if (!companyType) {
        this.props.history.push(routeList['OTHER']['packages']['to']);
      } else {
        routeList[userType].map(rout => {
          return rout.default ? this.props.history.push(rout.to) : null
        })
      }
    } else {
      console.log("No local storage.");
      this.props.history.push("/");
    }

    // socket.on('news', function (data) {
    //   console.log(data);
    //   socket.emit('my other event', { my: 'data' });
    // });
  }

  handleChange(event) {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  }

  onKeyUpHandler(event) {
    // console.log('event == ', event.keyCode);
    let keyCode = event.keyCode;
    if (keyCode === 13) {
      this.handleSubmit();
    }
  }

  handleSubmit() {
    let that = this;
    let dataToSend = {
      email: that.state.email,
      password: that.state.password
    };
    this.setState({loading: true});
    axios
      .post(path + "sign_in", dataToSend, {
        headers: { "Content-Type": "application/json" }
      })
      .then(serverResponse => {
        console.log("Response from here : ", serverResponse.data);
        const res = serverResponse.data;
        if (!res.isError) {
          this.setState({loading: false});
          localStorage.setItem("MIO_Local", JSON.stringify(res["details"]));
          let userType = res["details"].data.userType;
          // console.log(userType);

          if (userType) {
            let companyType = res["details"].data.companyType;
            // if user not select company type ['FREE_TYPE','STARTUP','ENTERPRISE']
            if (!companyType) {
              this.props.history.push(routeList['OTHER']['packages']['to']);
            } else {
              // if redirect URL present
              let redirectstorage = localStorage.getItem("MIO_Local_redirect");
              redirectstorage = redirectstorage ? JSON.parse(redirectstorage) : null;
              if (redirectstorage && redirectstorage.redirectURL) {
                that.props.history.push(redirectstorage.redirectURL);
                localStorage.setItem("MIO_Local_redirect", null);
              } else {
                // if redirect URL not present
                routeList[userType].map(rout => {
                  return rout.default ? that.props.history.push(rout.to) : null
                })
              }

              // let activeStatus = res["details"].data.isActive;
              // if(!activeStatus){
              //   alert('User is not active');
              // }

            }

          } else {
            that.props.history.push("/");
          }

          /*switch (userType) {
            case cred.USER_TYPE_ADMIN:
              that.props.history.push("/adduser/new");
              break;
            case cred.USER_TYPE_ADMIN_OPERATOR:
              that.props.history.push("/nomessage");
              break;
            case cred.USER_TYPE_COMPANY_OPERATOR:
              that.props.history.push("/companyoperator");
              break;
            case cred.USER_TYPE_ENTERPRISE:
              that.props.history.push("/adduser/new");
              break;
            case cred.USER_TYPE_STARTER:
              that.props.history.push("/adduser/new");
              break;
            case cred.USER_TYPE_PARTICIPANT:
              that.props.history.push("/adduser/new");
              break;
            default:
              that.props.history.push("/");
              break;
          }*/
        } else {
          this.setState({loading: false});

          if (res.statuscode == 505) {
            this.setState({
              loginAlert: true,
              loginAlertMessage: "Sorry! This user is not active now.."
            }, () => {
              this.openModal("error", "User is not active");
            });

          } else if(res.statuscode == 506){
            this.setState({
              loginAlert: true,
              loginAlertMessage: "Sorry! Your company is not active right now"
            }, () => {
              this.openModal("error", "Your company is not active");
            });
          }else if(res.statuscode == 507){
            this.setState({
              loginAlert: true,
              loginAlertMessage: "Sorry! Your company is not active right now"
            }, () => {
              this.openModal("error", "Please wait for Admin's approval");
            });
          }else{
            this.openModal("error", "Wrong username or password");
          }
        }
      })
      .catch(error=>{
        this.setState({loading: false});
        console.log(error);
      })
  }

  clearAlert = () => {

    this.setState({
      loginAlert: false,
      loginAlertMessage: ""
    })

  }

  // For "Browse as Guest" -->
  handleBrowse = () => {
    this.setState({
      showBrowse: true,
      browseFName: "",
      browseLName: "",
      browseEmail: ""
    });
  }

  hideBrowse = () => {
    this.setState({ showBrowse: false });
  }

  saveBrowse = () => {

    let { browseFName, browseLName, browseEmail } = this.state;
    let that = this;
    // console.log(browseFName, browseLName, browseEmail);
    !browseFName ? this.setState({ browseFNameError: "Please enter your fast name." }) : this.setState({ browseFNameError: "" });
    !browseEmail ? this.setState({ browseEmailError: "Please enter an email address." }) : this.setState({ browseEmailError: "" });
    !browseLName ? this.setState({ browseLNameError: "Please enter your last name." }) : this.setState({ browseLNameError: "" });

    if (browseFName && browseEmail && browseLName) {

      this.setState({ loading: true });
      if (/.+@.+\.[A-Za-z]+$/.test(`${browseEmail}`)) {

        let guestData = {
          firstName: browseFName,
          lastName: browseLName,
          email: browseEmail
        }

        console.log(Object(guestData));

        axios
          .post(path_con + "/login-as-guest", guestData,
            { headers: { "Content-Type": "application/json" } }
          )
          .then(serverResponse => {

            const res = serverResponse.data;
            console.log(res);

            if (!res.isError) {
              this.setState({ loading: false });
              // To Do 
              let token = res.details.token;
              console.log(token);
              res["details"]["data"]["companyType"] = "GUEST";
              res["details"]["data"]["companyId"] = "GUEST";
              localStorage.setItem("MIO_Local", JSON.stringify(res["details"]));

              let userType = res.details.data.userType;
              // let userType = "ENTERPRISE";
              console.log(userType);
              if (userType) {
                // if redirect URL present
                let redirectstorage = localStorage.getItem("MIO_Local_redirect");
                redirectstorage = redirectstorage ? JSON.parse(redirectstorage) : null;
                if (redirectstorage && redirectstorage.redirectURL) {
                  that.props.history.push(redirectstorage.redirectURL);
                  localStorage.setItem("MIO_Local_redirect", null);
                } else {
                  // if redirect URL not present
                  routeList[userType].map(rout => {
                    return rout.default ? that.props.history.push(rout.to) : null
                  })
                }
              } else {
                this.props.history.push("/");
              }

              this.setState({
                browseFName: "",
                browseLName: "",
                browseEmail: ""
              })

            } else {
              this.setState({ loading: false });
            }

          })
          .catch(error => {
            this.setState({ loading: false });
            console.log("Error: " + error);
          })



      } else {
        this.setState({ browseEmailError: "Please enter Valid email address." })
      }

    }

  }


  render() {
    let {loading} = this.state;
    return (
      <main>
        <section className="sign-inner">
          <div className="container-fluid">
            <div className="row">
              <div className="col-lg-5 col-md-12">
                <div className="signup-wrap">
                  <div className="sign-logo-desktop">
                    <img src="images/logo-sign.png" alt="" />
                  </div>
                  <div className="sign-logo-mobile">
                    <img src="images/mobile-logo-spalsh.png" alt="" />
                  </div>
                  <div className="signup-box">
                    <h2>Sign In</h2>
                    <p>Sign in to your account and get started!</p>
                    <form>
                      <div className="form-group">
                        <input
                          type="text"
                          name="email"
                          className="form-control"
                          placeholder="Enter your email"
                          onChange={event => this.handleChange(event)}
                        />
                      </div>
                      <div className="form-group">
                        <input
                          type="password"
                          name="password"
                          className="form-control"
                          placeholder="Enter your password"
                          onChange={event => this.handleChange(event)}
                          onKeyUp={event => this.onKeyUpHandler(event)}
                        />
                      </div>
                      {!loading?
                      <div className="form-group">
                        <input
                          type="button"
                          name=""
                          className="btn"
                          value="Sign In"
                          onClick={() => {
                            this.handleSubmit();
                          }}
                        />
                      </div>
                      :
                      null}
                      {loading?
                        <div className="form-group">
                          <input
                            type="button"
                            name=""
                            className="btn"
                            value="Sign In"
                          />
                          <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                            <i className="fa fa-spinner fa-spin" ></i>
                          </a>
                        </div>
                      :null}
                      <div className="form-group">
                        <p className="forget-link">
                          <Link to="/forgotpass" style={{ color: "#0092DD" }}>Forgot Password?</Link>
                          {/* </a> */}
                        </p>
                      </div>
                      <div className="form-group">
                        <input
                          type="button"
                          name=""
                          className="btn guest-btn"
                          value="Join Meeting"
                          onClick={this.handleBrowse}

                        />
                      </div>
                      <div className="form-group">
                        <p className="btm-sign-link">
                          Don't have an account? {/* <a href="#"> */}
                          <Link to="/register">Sign Up Free</Link>
                          {/* </a> */}
                        </p>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <div className="col-lg-7 col-md-12 d-none d-lg-block"></div>
            </div>
          </div>
        </section>

        <Modal show={this.state.loginAlert} onHide={() => this.clearAlert()}>
          <Modal.Header closeButton>
            <Modal.Title>Alert</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h5 style={{ color: 'red' }}>{`${this.state.loginAlertMessage}`}</h5>
          </Modal.Body>
          <Modal.Footer>

            <Button variant="primary" onClick={() => this.clearAlert()}>
              Cancel
            </Button>
          </Modal.Footer>
        </Modal>

        <Modal show={this.state.showBrowse} onHide={() => this.hideBrowse()}>
          <Modal.Header closeButton>
            <Modal.Title>Join Meeting</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="row">

              <div className="col-md-6">
                <div className="form-group">
                  <label>First Name:</label>
                  <input
                    type="text"
                    name="name"
                    value={this.state.browseFName}
                    className="form-control"
                    placeholder="Enter First Name"
                    onChange={event => this.setState({ browseFName: event.target.value })}
                    onFocus={() => this.setState({ browseFNameError: "" })}
                  />
                  <span style={{ color: 'red' }}>{this.state.browseFNameError ? `* ${this.state.browseFNameError}` : ''}</span>
                </div>
              </div>

              <div className="col-md-6">
                <div className="form-group">
                  <label>Last Name:</label>
                  <input
                    type="text"
                    name="lastName"
                    value={this.state.browseLName}
                    className="form-control"
                    placeholder="Enter Last Name"
                    onChange={event => this.setState({ browseLName: event.target.value })}
                    onFocus={() => this.setState({ browseLNameError: "" })}
                  />
                  <span style={{ color: 'red' }}>{this.state.browseLNameError ? `* ${this.state.browseLNameError}` : ''}</span>
                </div>
              </div>

              <div className="col-md-12">
                <div className="form-group">
                  <label>Email Address:</label>
                  <input
                    type="email"
                    name="usrEmail"
                    value={this.state.browseEmail}
                    className="form-control"
                    placeholder="Enter Email Address"
                    onChange={event => this.setState({ browseEmail: event.target.value })}
                    onFocus={() => this.setState({ browseEmailError: "" })}
                  />
                  <span style={{ color: 'red' }}>{this.state.browseEmailError ? `* ${this.state.browseEmailError}` : ''}</span>
                </div>
              </div>

            </div>
          </Modal.Body>
          <Modal.Footer>

            <Button variant="primary" onClick={this.saveBrowse}> Save </Button>

          </Modal.Footer>
        </Modal>

        <Snackbar
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "center"
          }}
          open={this.state.open}
          autoHideDuration={4000}
          onClose={() => this.handleModalClose()}
        >
          <MySnackbarContentWrapper
            onClose={() => this.handleModalClose()}
            variant={this.state.notiType}
            message={this.state.notiMessage}
          />
        </Snackbar>

      </main>
    );
  }
}

export default Login;
