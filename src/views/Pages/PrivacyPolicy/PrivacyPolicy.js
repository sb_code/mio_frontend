import React, { Component } from 'react'
import axios from "axios";
import RichTextEditor from 'react-rte';

import cred from "../../../cred.json";

var path = cred.API_PATH;

class PrivacyPolicy extends Component {
  constructor(props) {
    super(props);
    this.state = {
      policies: '',
    };
  }

  componentDidMount() {
    document.body.classList.remove("sign-bg");
    document.body.classList.add("grey-bg");
    this.getData();
  }

  getData() {
    axios
      .post(path + 'content-management/get-privacy-policy')
      .then(serverResponse => {
        let res = serverResponse.data;
        if (!res.isError) {
          this.setState({
            policies: res.details && res.details[0],
          })
        }
      })
      .catch(error => {
        console.log(error);
      })
  }


  render() {
    let { policies } = this.state;
    return (<main>
      <section className="user-mngnt-wrap">
        <div className="container-fluid">
          <div className="row">

            <div className="col-xl-12 col-lg-9 p-0 mob-full-conf">

              <div className="mobile-menu-header">
                {/* <a className="back-arw" onClick={() => console.log('<-BACK')}><i className="fas fa-arrow-left"></i></a> */}
                <a href="JavaScript:Void(0);" className="menu-tgl" onClick={() => console.log('back')}><img src="images/menu-tgl.png" alt="" /></a>
              </div>

              <div className="mng-full-list">

                <div className="mng-full-table">

                  <div className="row">
                    <div className="col-md-12">
                      <div className="mng-full-table-hdr">
                        <div className="row">
                          <div className="col-md-12">
                            <h6>Privacy Policy</h6>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  {/* {this.state.policies && this.state.policies.length > 0 && this.state.policies.map((obj, index) => { */}
                  <div className="row" >
                    <div className="col-md-12">
                      {/* <div className="mng-full-table-row"> */}

                      {policies && policies.policy ?
                        <RichTextEditor
                          value={RichTextEditor.createValueFromString(policies.policy, 'html')}
                          readOnly={true}
                        />
                        : null}

                    </div>
                  </div>
                  {/* })} */}

                </div>

              </div>

            </div>

          </div>
        </div>

      </section>
    </main >)

  }
}

export default PrivacyPolicy;