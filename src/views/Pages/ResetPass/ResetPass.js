import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import { Snackbar } from "@material-ui/core";
import MySnackbarContentWrapper from "../../Component/MaterialSnack";
import cred from "../../../cred.json";

var path = cred.API_PATH;


class ResetPass extends Component {
  constructor(props) {
    super(props);

    this.state = {
      forgotemail: "",
      resetemail: "",
      password: "",
      otp: "",

      open: false,
      notiMessage: "",
      notiType: "info"
    };

    this.handleResetPassword = this.handleResetPassword.bind(this);
  }

  openModal = (type, msg) => {
    this.setState({
      open: true,
      notiMessage: msg,
      notiType: type
    });
  };

  handleModalClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({
      open: false
    });
    // setOpen(false);
  };

  componentDidMount() {
    document.body.classList.add('sign-bg')
    console.log(this.props.location.state.details);
    this.setState({
      resetemail: this.props.location.state.details
    });

  }
 

  handleChange(event) {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  }

 
  handleResetPassword() {
    let that = this;
    let dataToSend = {
      email: that.state.resetemail,
      otp: that.state.otp,
      new_password: that.state.password,
    };
    axios
      .post(path + "user/reset_password", dataToSend, {
        headers: { "Content-Type": "application/json" }
      })
      .then(serverResponse => {
        const res = serverResponse.data;
        if (!res.isError) {
          that.openModal("success", "Password changed successfully");         
          setTimeout(function(){that.props.history.push("/login");},5000) ;          
          
        } else {
          that.openModal("error", "Some error occured");
        }
      });
  }

  render() {
    return (
        <main>
          <section className="sign-inner">
            <div className="container-fluid">
              <div className="row">
                <div className="col-lg-5 col-md-12">
                  <div className="signup-wrap">
                    <div className="sign-logo-desktop">
                      <img src="images/logo-sign.png" alt="" />
                    </div>
                    <div className="sign-logo-mobile">
                      <img src="images/mobile-logo-spalsh.png" alt="" />
                    </div>
                    <div className="signup-box">
                      <h2>Reset Password</h2>
                      <p>Enter your email, OTP and new password</p>
                      <form>                       
                        <div className="form-group">
                          <input
                            type="text"
                            name="resetemail"
                            value= {this.state.resetemail}
                            className="form-control"
                            placeholder="Enter your email"
                            onChange={event => this.handleChange(event)}
                          />
                        </div>
                        <div className="form-group">
                          <input
                            type="text"
                            name="otp"
                            className="form-control"
                            placeholder="Enter your OTP"
                            onChange={event => this.handleChange(event)}
                            autoFocus 
                          />
                        </div>
                        <div className="form-group">
                          <input
                            type="password"
                            name="password"
                            className="form-control"
                            placeholder="Enter your new password"
                            onChange={event => this.handleChange(event)}
                          />
                        </div>
                        
                        <div className="form-group">
                          <p className="btm-sign-link">
                            Want to check email again? <a href="#"><Link to="/forgotpass">Back</Link></a>
                          </p>
                        </div>

                        <div className="form-group">
                          <input
                            type="button"
                            name=""
                            className="btn guest-btn"
                            value="Reset Password"
                            onClick={()=>this.handleResetPassword()}
                          />
                        </div>

                        <div className="form-group">
                          <p className="btm-sign-link">
                            Already have an account? <a href="#"><Link to="/login">Sign In</Link></a>
                          </p>
                        </div>

                      </form>
                    </div>
                  </div>
                </div>
                <div className="col-lg-7 col-md-12 d-none d-lg-block"></div>
              </div>
            </div>
          </section>
          <Snackbar
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "center"
          }}
          open={this.state.open}
          autoHideDuration={4000}
          onClose={() => this.handleModalClose()}
        >
          <MySnackbarContentWrapper
            onClose={() => this.handleModalClose()}
            variant={this.state.notiType}
            message={this.state.notiMessage}
          />
        </Snackbar>
        </main>
    );
  }
}

export default ResetPass;
