import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Navbar from "../../Component/Navbar";
import ReactTooltip from 'react-tooltip'; // For tool tip
import axios from "axios";
import cred from "../../../cred.json";
import { Modal, Button, Card, Badge } from "react-bootstrap"

var path = cred.API_PATH;


class ContactUs extends Component {

    constructor(props) {
        super(props);
        this.state = {
            details: {},
            contactDetails: [],
            name: '',
            mailId1: '',
            mailId2: '',
            contactNo1: '',
            contactNo2: '',

            willEdit: true,

        }
    }

    componentDidMount() {
        document.body.classList.remove("sign-bg");
        document.body.classList.add("grey-bg");

        this.getContactUsDetails();

    }

    getContactUsDetails = () => {

        let dataToSend = {
            "companyId": ""
        };

        axios
            .post(path + 'content-management/get-mio-admin-detail', dataToSend)
            .then(serverResponse => {

                let res = serverResponse.data;
                if (!res.isError) {
                    console.log("About us", res);
                    let details = res.details;
                    let contactDetails = details && details.contactUs;
                    this.setState({
                        details: details,
                        contactDetails: contactDetails
                    });
                }

            })
            .catch(error => {
                console.log(error);
            })

    }

    render() {

        let { willEdit, details, contactDetails, name, mailId1, mailId2, contactNo1, contactNo2 } = this.state;

        return (
            <main>
                <section className="user-mngnt-wrap">
                    <div className="container-fluid">
                        <div className="row justify-content-center">

                            {/* <Navbar routTo="/admincontentmanagement" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} /> */}
                            <div className="col-lg-9 p-0 mob-full-conf">

                                <div className="mobile-menu-header">
                                    {/* <a className="back-arw" onClick={() => console.log('<-BACK')}><i className="fas fa-arrow-left"></i></a> */}
                                    <a className="menu-tgl" onClick={() => this.sideMenuToggle()}><img src="images/menu-tgl.png" alt="" /></a>
                                </div>

                                <div className="mng-full-list about-mng-form">

                                    <div className="row">
                                        {contactDetails && contactDetails.length > 0 ? contactDetails.map((value, index) => {
                                            return <div className="col-md-6" key={index}>
                                                <Card border="info" style={{ margin: '10px', padding: '10px' }}>
                                                    <Card.Header>{value.branchName}</Card.Header>
                                                    <Card.Body>
                                                        <Card.Title></Card.Title>
                                                        <Card.Subtitle className="mb-2 text-muted">{value.location ? `${value.location}` : ''}</Card.Subtitle>
                                                        <Card.Text>
                                                            {value.contactNo1 ? 'Contact No: ' + `${value.contactNo1}` : ''}
                                                        </Card.Text>
                                                        <Card.Text>
                                                            {value.contactNo2 ? ' Alt contact no: ' + `${value.contactNo2}` : ''}
                                                        </Card.Text>
                                                        <Card.Text>
                                                            {value.mailId1 ? 'Mail Id: ' + `${value.mailId1}` : ''}
                                                        </Card.Text>
                                                        <Card.Text>
                                                            {value.mailId2 ? ' Alt mail id: ' + `${value.mailId2}` : ''}
                                                        </Card.Text>

                                                    </Card.Body>
                                                </Card>
                                            </div>
                                        })
                                            : null}

                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>

                </section>
            </main>
        );
    }
}

export default ContactUs;