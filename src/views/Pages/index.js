import Login from './Login';
// import Page404 from './Page404';
// import Page500 from './Page500';
import Register from './Register';
import ForgotPass from './ForgotPass';
import ResetPass from './ResetPass';

import AboutUs from './AboutUs';
import ContactUs from './ContactUs';
import FAQ from './FAQ';
import PrivacyPolicy from './PrivacyPolicy';
import TermsConditions from './TermsConditions';

export {
  Login, Register, ForgotPass, ResetPass, AboutUs, ContactUs, FAQ, PrivacyPolicy, TermsConditions
};