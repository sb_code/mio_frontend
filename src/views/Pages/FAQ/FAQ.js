import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Navbar from "../../Component/Navbar";
import ReactTooltip from 'react-tooltip'; // For tool tip
import axios from "axios";
import cred from "../../../cred.json";
import { Modal, Button } from "react-bootstrap"

var path = cred.API_PATH;

class FAQ extends Component {
    constructor(props) {
        super(props)
        this.state = {
            details: '',
            editingField: '',
            question: '',
            answer: '',


        }
    }

    componentDidMount() {

        document.body.classList.remove("sign-bg");
        document.body.classList.add("grey-bg");

        this.getFAQDetails();

    }

    getFAQDetails = () => {

        axios
            .post(path + 'content-management/get-faq')
            .then(serverResponse => {
                let res = serverResponse.data;
                if (!res.isError) {
                    this.setState({
                        details: res.details,
                    });
                    // console.log("All FAQ details == ", res);
                }
            })
            .catch(error => {
                console.log(error);
            })

    }

    sideMenuToggle = () => {
        let { openSideMenu } = this.state;
        openSideMenu = !openSideMenu;
        this.setState({ openSideMenu });
    }

    mobMenuClick() {
        this.sideMenuToggle();
        console.log('mobMenuClick');
    }

    render() {

        let { details, editingField, question, answer } = this.state;

        return (
            <main>
                <section className="user-mngnt-wrap">
                    <div className="container-fluid">
                        <div className="row justify-content-center">

                            <Navbar routTo="/admincontentmanagement" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />
                            <div className="col-lg-10 p-0 mob-full-conf">

                                <div className="mobile-menu-header">
                                    {/* <a className="back-arw" onClick={() => console.log('<-BACK')}><i className="fas fa-arrow-left"></i></a> */}
                                    <a className="menu-tgl" onClick={() => this.sideMenuToggle()}><img src="images/menu-tgl.png" alt="" /></a>
                                </div>

                                <div className="mng-full-list about-mng-form">
                                    

                                    <div className="row">
                                        <div className="col-lg-12 col-md-12">

                                            {details && details.map((pair, index) => {
                                                return <div className="row" key={index}>
                                                    <div className="col-md-12">
                                                        <div className="form-group faq-ans-lbl">
                                                            <label>{index + 1}: {pair.question}</label>
                                                            <textarea type="text" name="details2" className="form-control" rows="4" cols="50"
                                                                placeholder="Enter Answer" style={{ height: "85px" }}
                                                                onChange={(event) => this.changeAnswer(event, index)}
                                                                readOnly={editingField && editingField == `${index}` ? false : true}
                                                                value={pair.answer}
                                                            >
                                                                {/* {pair.answer} */}
                                                            </textarea>
                                                        </div>
                                                    </div>

                                                    

                                                </div>
                                            })}


                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </section>
            </main >

        );
    }
}

export default FAQ;