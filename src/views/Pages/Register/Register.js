import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import { Snackbar } from "@material-ui/core";

import { Toast } from "react-bootstrap";
import Swal from 'sweetalert2';

import MySnackbarContentWrapper from "../../Component/MaterialSnack";
import cred from "../../../cred.json";
import routeList from "../../../routeList.json";

var path = cred.API_PATH + "user/";

class Register extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,

      fname: "",
      lname: "",
      email: "",
      mobile: "",
      countryCode: "",
      password: "",

      open: false,
      notiMessage: "",
      notiType: "info",

      showToast: false,
      toastTitle: "",
      toastMessage: "",

      countryCodeList: [],

    };

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  openModal = (type, msg) => {
    this.setState({
      open: true,
      notiMessage: msg,
      notiType: type
    });
  };

  handleModalClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({
      open: false
    });
    // setOpen(false);
  };

  handleChange(event) {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  }

  componentDidMount() {
    document.body.classList.add("sign-bg");
    let storage = localStorage.getItem("MIO_Local");

    storage = storage ? JSON.parse(storage) : null;
    console.log(typeof storage);

    if (storage && storage.data && storage.data.userType) {
      let userType = storage.data.userType;

      routeList[userType].map(rout => {
        return rout.default ? this.props.history.push(rout.to) : null
      })

      this.getCountryCodeList();

      /*switch (userType) {
        case cred.USER_TYPE_ADMIN:
          this.props.history.push("/adduser/new");
          break;
        case cred.USER_TYPE_ADMIN_OPERATOR:
          this.props.history.push("/nomessage");
          break;
        case cred.USER_TYPE_COMPANY_OPERATOR:
          this.props.history.push("/companyoperator");
          break;
        case cred.USER_TYPE_ENTERPRISE:
          this.props.history.push("/adduser/new");
          break;
        case cred.USER_TYPE_STARTER:
          this.props.history.push("/adduser/new");
          break;
        case cred.USER_TYPE_PARTICIPANT:
          this.props.history.push("/adduser/new");
          break;
        default:
          break;
      }*/
    } else {
      console.log("No local storage.");
      this.getCountryCodeList();
    }
  }

  getCountryCodeList = () => {

    axios
      .post(cred.API_PATH + 'services/get-country-codes')
      .then(serverResponse => {

        let res = serverResponse.data;
        if (!res.isError) {
          this.setState({
            countryCodeList: res.details
          });
        }

      })
      .catch(error => {
        console.log(error);
      })

  }

  handleSubmit() {
    let that = this;
    that.setState({
      loading: true
    })
    let countryCode = that.state.countryCode;
    if (
      that.state.fname === "" ||
      that.state.lname === "" ||
      that.state.email === "" ||
      that.state.countryCode === "" ||
      that.state.mobile === "" ||
      that.state.password === ""
    ) {
      alert("Please fill all the details");
      that.setState({
        loading: false
      })
    } else {
      if (countryCode.slice(0, 1) === '+') {
        countryCode = countryCode.slice(1);
      }
      if (isNaN(that.state.countryCode)) {
        alert("Invalide country code.");
        that.setState({
          loading: false
        })
      } else {
        if (isNaN(that.state.mobile)) {
          alert("Invalide phone number.");
          that.setState({
            loading: false
          })
        } else {
          // let phoneNo = `${that.state.countryCode + that.state.mobile}`;

          let dataToSend = {
            fname: that.state.fname,
            lname: that.state.lname,
            email: that.state.email,
            mobile: that.state.mobile,
            countryCode: countryCode,
            password: that.state.password
          };

          axios
            .post(path + "sign_up", dataToSend, {
              headers: { "Content-Type": "application/json" }
            })
            .then(serverResponse => {
              console.log("Response from here server: ", serverResponse);
              const res = serverResponse.data;
              if (!res.isError) {
                dataToSend.companyId = res.details.companyId;
                dataToSend.userId = res.details.userId;
                axios
                  .post(cred.API_PATH + "payment/create-customer", dataToSend, {
                    headers: { "Content-Type": "application/json" }
                  });
                that.openModal("success", "Signed Up successfully");
                that.setState({
                  showToast: true,
                  toastTitle: "Sign Up",
                  toastMessage: "You have been signed up successfully"
                }, () => {
                  this.clearForm();
                });

                // that.props.history.push("/login");

              } else {
                that.openModal("error", res.message);
                Swal.fire("Error", res.message, "Error");
                that.setState({
                  isAuthenticated: false,
                  authMessage: res.details
                }, () => {
                  this.clearForm();
                });
              }
            });
        }
      }
    }
  }

  clearForm = () => {

    this.setState({
      fname: '',
      lname: '',
      email: '',
      mobile: '',
      countryCode: '',
      password: '',
      loading: false
    })

  }

  hideToast = () => {
    this.setState({
      showToast: false
    }, () => {
      this.props.history.push("/login");
    });
  }


  render() {

    let { countryCodeList, loading } = this.state;

    return (
      <main>
        <section className="sign-inner">
          <div className="container-fluid">
            <div className="row">
              <div className="col-lg-5 col-md-12">
                <div className="signup-wrap">
                  <div className="sign-logo-desktop">
                    <img src="images/logo-sign.png" alt="" />
                  </div>
                  <div className="sign-logo-mobile">
                    <img src="images/mobile-logo-spalsh.png" alt="" />
                  </div>
                  <div className="signup-box">
                    <h2>Sign Up Free</h2>

                    <p>Register your account to sign in!</p>
                    <form>
                      <div className="form-group">
                        <input
                          type="text"
                          name="fname"
                          className="form-control"
                          placeholder="Enter your first name"
                          onChange={event => this.handleChange(event)}
                        />
                      </div>
                      <div className="form-group">
                        <input
                          type="text"
                          name="lname"
                          className="form-control"
                          placeholder="Enter your last name"
                          onChange={event => this.handleChange(event)}
                        />
                      </div>
                      <div className="form-group">
                        <input
                          type="email"
                          name="email"
                          className="form-control"
                          placeholder="Enter your email address"
                          onChange={event => this.handleChange(event)}
                        />
                      </div>
                      <div className="row">
                        <div className="col-md-5">
                          <div className="form-group">
                            <select
                              className="form-control"
                              name="countryCode"
                              // value={countryCode}
                              onChange={event => this.handleChange(event)}
                            >
                              <option value="">Select country</option>
                              {countryCodeList && countryCodeList.map((data, index) => {
                                return <option value={data.code}>{data.code + ' ' + data.name}</option>
                              })}
                            </select>
                          </div>
                        </div>
                        <div className="col-md-7">
                          <div className="form-group">
                            <input
                              type="tel"
                              name="mobile"
                              className="form-control"
                              placeholder="Enter your mobile number"
                              onChange={event => this.handleChange(event)}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="form-group">
                        <input
                          type="password"
                          name="password"
                          className="form-control"
                          placeholder="Enter your password"
                          onChange={event => this.handleChange(event)}
                        />
                      </div>
                      <div className="form-group">
                        {!this.state.loading ?
                          <input
                            type="button"
                            name=""
                            className="btn"
                            value="Sign Up Free"
                            onClick={() => {
                              this.handleSubmit();
                            }}
                          />
                          :
                          <a href="JavaScript:void(0);"
                            name="Processing"
                            className="btn"
                            value="Processing"
                          >
                            Processing &nbsp;
                            <i class="fas fa-spinner fa-spin"></i>
                          </a>
                        }
                      </div>

                      <div className="form-group">
                        <Link to="/termsconditions"><span className="cont-link">Terms and Conditions</span></Link>&nbsp;&nbsp;|&nbsp;&nbsp;
                        <Link to="/privacypolicy"><span className="cont-link">Privacy policy</span></Link>
                      </div>
                      <br />

                      <div className="form-group">
                        <p className="btm-sign-link">
                          Already have an account?
                          <Link to="/login">Sign In</Link>
                        </p>
                      </div>

                      <div className="row">
                        <div className="col-md-12">
                          <Link to="/aboutus" target="_blank"><span className="cont-link">About Us</span></Link>&nbsp;&nbsp;|&nbsp;&nbsp;
                          <Link to="/contactus" target="_blank"><span className="cont-link">Contact Us</span></Link>&nbsp;&nbsp;|&nbsp;&nbsp;
                          <Link to="/faqs" target="_blank"><span className="cont-link">FAQs</span></Link>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
              <div className="col-lg-7 col-md-12 d-none d-lg-block"></div>
            </div>
          </div>
        </section>
        <Snackbar
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "center"
          }}
          open={this.state.open}
          autoHideDuration={4000}
          onClose={() => this.handleModalClose()}
        >
          <MySnackbarContentWrapper
            onClose={() => this.handleModalClose()}
            variant={this.state.notiType}
            message={this.state.notiMessage}
          />
        </Snackbar>

        {/* Toast Notification Start -- */}
        <Toast
          show={this.state.showToast}
          onClose={this.hideToast}
          delay={3000}
          className="toast-class"
          autohide
        >
          <Toast.Header>
            <strong className="mr-auto">{this.state.toastTitle}</strong>
            {/* <small>11 mins ago</small> */}
          </Toast.Header>
          <Toast.Body>{this.state.toastMessage}</Toast.Body>
        </Toast>
        {/* Toast Notification End -- */}

      </main>
    );
  }
}

export default Register;
