import React, { Component } from 'react'
import axios from "axios";
import RichTextEditor from 'react-rte';

import cred from "../../../cred.json";


var path = cred.API_PATH;


class TermsConditions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      termsAndConditions: ''
    };
  }

  componentDidMount() {
    document.body.classList.remove("sign-bg");
    document.body.classList.add("grey-bg");
    this.getData();
  }

  getData() {
    axios
      .post(path + 'content-management/get-tac', {})
      .then(serverResponse => {
        let res = serverResponse.data;
        if (!res.isError) {
          let result = res.details && res.details[0];
          console.log(result);
          this.setState({
            termsAndConditions: result
          }, () => {
            console.log(this.state.termsAndConditions);
          });
        } else {
          // this.setState(prevState => ({ searching: false }));
        }
      })
      .catch(error => {
        console.log(error);
      })
  }


  render() {
    let {termsAndConditions} = this.state;
    return (<main>
      <section className="user-mngnt-wrap">
        <div className="container-fluid">
          <div className="row">

            <div className="col-xl-12 col-lg-9 p-0 mob-full-conf">

              <div className="mobile-menu-header">
                {/* <a className="back-arw" onClick={() => console.log('<-BACK')}><i className="fas fa-arrow-left"></i></a> */}
                <a href= "JavaScript:Void(0);" className="menu-tgl" onClick={() => console.log('back')}><img src="images/menu-tgl.png" alt="" /></a>
              </div>

              <div className="mng-full-list">

                <div className="mng-full-table">

                  <div className="row">
                    <div className="col-md-12">
                      <div className="mng-full-table-hdr">
                        <div className="row">
                          <div className="col-md-12">
                            <h6>Terms And Conditions</h6>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="row" >
                      <div className="col-md-12">
                        {/* <div className="mng-full-table-row"> */}
                          
                          <RichTextEditor
                            value={RichTextEditor.createValueFromString(termsAndConditions.termsAndCondition, 'html')}
                            readOnly={true}
                          />

                        {/* </div> */}
                      </div>
                    </div>
                  
                </div>

              </div>

            </div>

          </div>
        </div>

      </section>
    </main >)

  }
}

export default TermsConditions;