import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import { Snackbar } from "@material-ui/core";
import MySnackbarContentWrapper from "../../Component/MaterialSnack";
import cred from "../../../cred.json";

var path = cred.API_PATH;

class ForgotPass extends Component {
  constructor(props) {
    super(props);

    this.state = {
      forgotemail: "",
      resetemail: "",
      password: "",
      otp: "",
     
      open: false,
      notiMessage: "",
      notiType: "info"
    };

    this.handleForgotPassword = this.handleForgotPassword.bind(this);
  }


  openModal = (type, msg) => {
    this.setState({
      open: true,
      notiMessage: msg,
      notiType: type
    });
  };

  handleModalClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({
      open: false
    });
    // setOpen(false);
  };


  componentDidMount() {
    document.body.classList.add("sign-bg");
  }

  handleChange(event) {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  }

  handleForgotPassword() {
    let that = this;

    !that.state.forgotemail ? this.setState({ emailError: "Please enter an email." }) : this.setState({ emailError: "" });
    if(/.+@.+\.[A-Za-z]+$/.test(`${that.state.forgotemail}`))
    {
      let dataToSend = {
        email: that.state.forgotemail
      };
      console.log(Object(dataToSend));
      
      axios
        .post(path + "user/forgot_password", dataToSend, {
          headers: { "Content-Type": "application/json" }
        })
        .then(serverResponse => {
          const res = serverResponse.data;
          console.log(res);
          
          if (!res.isError) {
            that.openModal("success", "Please check your email");
            setTimeout(function(){that.props.history.push("/resetpass", { details: `${that.state.forgotemail}` } );},5000) ;   
          } else {
            that.openModal("error", "Email not found");
          }
        });

    }else{
      this.setState({ emailError: "Please enter an valid email." });
    }

  }

  render() {
    return (
      <main>
        <section className="sign-inner">
          <div className="container-fluid">
            <div className="row">
              <div className="col-lg-5 col-md-12">
                <div className="signup-wrap">
                  <div className="sign-logo-desktop">
                    <img src="images/logo-sign.png" alt="" />
                  </div>
                  <div className="sign-logo-mobile">
                    <img src="images/mobile-logo-spalsh.png" alt="" />
                  </div>
                  <div className="signup-box">
                    <h2>Forgot Password</h2>
                    <p>Enter your email to get the OTP</p>
                    <form>
                      <div className="form-group">
                        <input
                          type="text"
                          name="forgotemail"
                          className="form-control"
                          placeholder="Enter your email"
                          onChange={event => this.handleChange(event)}
                          onFocus={() => this.setState({ emailError: "" })}
                        />
                        <span style={{ color: 'red' }}>{this.state.emailError ? `* ${this.state.emailError}` : ''}</span>
                      </div>

                      <div className="form-group">
                        <input
                          type="button"
                          name=""
                          className="btn"
                          value="Send email"
                          onClick={() => {
                            this.handleForgotPassword();
                          }}
                        />
                      </div>
                      <div className="form-group">
                        <p className="btm-sign-link">
                          Already have an account?{" "}
                          <a href="#">
                            <Link to="/login">Sign In</Link>
                          </a>
                        </p>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <div className="col-lg-7 col-md-12 d-none d-lg-block"></div>
            </div>
          </div>
        </section>
        <Snackbar
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "center"
          }}
          open={this.state.open}
          autoHideDuration={4000}
          onClose={() => this.handleModalClose()}
        >
          <MySnackbarContentWrapper
            onClose={() => this.handleModalClose()}
            variant={this.state.notiType}
            message={this.state.notiMessage}
          />
        </Snackbar>
      </main>
    );
  }
}

export default ForgotPass;
