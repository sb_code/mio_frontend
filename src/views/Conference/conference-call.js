/* Let CRA handle linting for sample app */
import React, { Component } from 'react';
import Spinner from 'react-spinner';
import classNames from 'classnames';

import { CSVLink, CSVDownload } from "react-csv";

import Navbar from '../Component/Navbar';
import { Modal, Button, Badge, Toast } from "react-bootstrap";
import ProgressBar from 'react-bootstrap/ProgressBar';
import axios from "axios";
import cred from "../../cred.json";

// https://github.com/opentok/accelerator-core-js
import AccCore from 'opentok-accelerator-core';
import _ from 'lodash';

import { checkAuthentication } from '../Component/Authentication';

import ReactTooltip from 'react-tooltip' // For tool tip 

import NexmoClient from 'nexmo-client';
import Timer from './Timer';

import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css

let path = cred.API_PATH;
let nexmo;
let otCore;
const otCoreOptions = {
  credentials: {
    apiKey: "46425072",
    sessionId: "2_MX40NjQyNTA3Mn5-MTU3NDc2NjQ3MTExOX53Ly9YajRiNUs4MmovcmFWRElsUmdyUDJ-fg",
    token: "T1==cGFydG5lcl9pZD00NjQyNTA3MiZzaWc9NWM1ZDlmNmViY2YzMTljMzdkYmFjZWUzMDU0YmZjNTU0ZTUyOTNjNDpzZXNzaW9uX2lkPTJfTVg0ME5qUXlOVEEzTW41LU1UVTNORGMyTmpRM01URXhPWDUzTHk5WWFqUmlOVXM0TW1vdmNtRldSRWxzVW1keVVESi1mZyZjcmVhdGVfdGltZT0xNTc0ODUwNjQzJm5vbmNlPTAuMjEyODI2OTQ0Mzg3NTc4NTQmcm9sZT1wdWJsaXNoZXImZXhwaXJlX3RpbWU9MTU3NDkzNzA0MyZpbml0aWFsX2xheW91dF9jbGFzc19saXN0PQ==",
  },
  // A container can either be a query selector or an HTML Element
  streamContainers(pubSub, type, data, stream) {
    return {
      publisher: {
        camera: '#cameraPublisherContainer',
        screen: '#screenPublisherContainer',
      },
      subscriber: {
        camera: '#cameraSubscriberContainer',
        screen: '#screenSubscriberContainer',
      },
    }[pubSub][type];
  },
  controlsContainer: '#controls',
  packages: ['textChat', 'screenSharing'],
  // packages: ['textChat', ', 'annotation'],
  communication: {
    callProperties: null, // Using default
    subscribeOnly: false,
  },
  textChat: {
    name: ['David', 'Paul', 'Emma', 'George', 'Amanda'][Math.random() * 5 | 0], // eslint-disable-line no-bitwise
    waitingMessage: '',
    container: '#chat',
    alwaysOpen: true,
  },
  screenSharing: {
    extensionID: 'plocfffmbcclpdifaikiikgplfnepkpo',
    // annotation: true,
    externalWindow: false,
    dev: true,
    screenProperties: {
      insertMode: 'append',
      width: '100%',
      height: '100%',
      showControls: true,
      style: {
        buttonDisplayMode: 'on',
      },
      videoSource: 'window',
      fitMode: 'contain' // Using default
    },
  }
  // annotation: {
  //   absoluteParent: {
  //     publisher: '.App-video-container',
  //     subscriber: '.App-video-container'
  //   }
  // },
};

/**
 * Build classes for container elements based on state
 * @param {Object} state
 */
const containerClasses = (state) => {
  const { active, meta, localAudioEnabled, localVideoEnabled, isPresenter, isChatOpen } = state;
  const sharingScreen = meta ? !!meta.publisher.screen : false;
  const viewingSharedScreen = meta ? meta.subscriber.screen : false;
  const activeCameraPublisher = meta ? meta.publisher.camera : 0;
  const activeCameraSubscribers = meta ? meta.subscriber.camera : 0;
  const activeCameraSubscribersGt2 = activeCameraSubscribers > 2;
  const activeCameraSubscribersOdd = activeCameraSubscribers % 2;
  const screenshareActive = viewingSharedScreen || sharingScreen;

  return {
    controlClass: classNames('App-control-container', { hidden: !active }),
    localAudioClass: classNames('ots-video-control circle audio', { hidden: !active, muted: !localAudioEnabled }),
    localVideoClass: classNames('ots-video-control circle video', { hidden: !active, muted: !localVideoEnabled }),
    localCallClass: classNames('ots-video-control circle end-call', { hidden: !active }),
    cameraPublisherClass: classNames('video-container', { hidden: !active || !activeCameraPublisher, small: !!activeCameraSubscribers || screenshareActive, left: screenshareActive }),
    screenPublisherClass: classNames('video-container', { hidden: !active || !sharingScreen || isPresenter || viewingSharedScreen }),
    cameraSubscriberClass: classNames('video-container', { hidden: !active || !activeCameraSubscribers },
      { 'active-gt2': activeCameraSubscribersGt2 && !screenshareActive },
      { 'active-odd': activeCameraSubscribersOdd && !screenshareActive },
      { small: screenshareActive }
    ),
    screenSubscriberClass: classNames('video-container', { hidden: !viewingSharedScreen || !active }),
    appChatOuterWrap: classNames('app-chat-outer-wrap', { displaynone: !active || !isChatOpen }),
  };
};

const connectingMask = () =>
  <div className="App-mask">
    {/* <Spinner /> */}
    <div className="message with-spinner">Select a conference</div>
  </div>;

const selectingMask = () =>
  <div className="App-mask">
    {/* <Spinner /> */}
    <div className="message with-spinner">Selecting...</div>
  </div>;

const startCallMask = start =>
  <div className="App-mask">
    <button className="message button clickable" onClick={start}>Click to Join Conference / Start Call </button>
  </div>;

const loadingMask = () =>
  <div className="App-mask">
    {/* <Spinner /> */}
    <div className="message with-spinner">Loading...</div>
  </div>;

const waitingMask = () =>
  <div className="App-mask">
    {/* <Spinner /> */}
    <div className="message with-spinner">Waiting for Host..</div>
  </div>;

export class ConferenceCall extends Component {

  //#region constructor and life cycle hook

  constructor(props) {
    super(props);
    this.state = {
      showParticipantsModal: false,
      openSideMenu: false,
      openSubMenuMob: true,
      showCreateConference: false,
      maxNoOfParticipants: '',
      conferenceTitle: '',
      userId: '',
      userName: '',
      userEmail: '',
      userType: '',
      userCompanyId: '',
      companyType: '',
      userCompanyName: '',
      selectedCompanyName: '',
      conferenceTitleError: '',
      maxNoOfParticipantsError: '',

      countryCodeList: '',

      conferenceList: [],
      selectedConference: {},

      sessionId: '',
      token: '',

      starting: false,
      selecting: false,
      connected: false,
      active: false,
      waiting: false,
      listLoading: false,
      recordLoading: false,
      invitationLoading: false,
      publishers: null,
      subscribers: null,
      meta: null,
      localAudioEnabled: true,
      localVideoEnabled: true,

      showAddressBook: false,
      contactList: [],

      inboundRadio: false,
      invitationCode: '',
      callInitiator: 'OWNER',
      validUpto: '',
      timeOutHrs: '',
      timeOutMins: '',
      durationHrs: '',
      durationMins: '',
      recordingPreference: '',
      // recordingCheck: true,
      fileShare: true,
      inboundCall: false,
      screenWithoutPermission: true,
      screenWithPermission: false,
      registerMandatory: false,
      conferenceMode: 'CONFERENCE',
      hostJoinBy: 'ONLINE',
      currentScreen: {},

      recordingTimer: 0,    // For recording call
      startRecordingAt: 0,
      recording: false,

      conferenceMember: '',
      selectedConferenceId: '',
      inboundConnectionId: '', // For nexmo call 
      showMember: false,
      sipCallModal: false,
      showDeleteModal: false,
      conferenceToDelete: '',
      isPresenter: false, // For screen sharing 

      speakers: [],

      sipNo: '',
      sipCountryCode: '',
      // sipStatusCount: {}, // For getting sip call status 

      files: [],
      imageURL: '',
      fileUploaded: 0,
      // uploading: false,
      // For inbound calls -- 
      inboundUser: [],

      registeredParticipants: [], // for registered participantsin LECTURE mode conference--
      showRegisteredList: false,

      actionObserver: {},

      currentStream: [],
      myConnectionDetails: {},
      hostConnectionDetails: {},

      // For show /hide toast --
      showToast: false,
      toastTitle: '',
      toastMessage: '',

      isFullScreen: false,
      isChatOpen: true,

      startedArchiveId: '',

      showInvitationModal: false,
      inviteMail: '',
      inviteCountryCode: '',
      invitePhone: '',
      isOfflineUser: true,
      invitationError: '',

      messagesForHost: [],
      messageToHost: '',
      showSuccessAlert: false,
      successAlertMessage: '',
      showErrorAlert: false,
      errorAlertMessage: '',

      showPresenterRequest: false, // for showing presenter request
      presenterRequest: null,
      nexmoJWT: '',

      isAllowToScreenShare: true,

      timer: 0, // For timer --
      callTimerModal: false,
      callTimerMessage: "",

    };
    // this.startCall = this.startCall.bind(this);
    this.endConferenceCall = this.endConferenceCall.bind(this);
    this.toggleLocalAudio = this.toggleLocalAudio.bind(this);
    this.toggleLocalVideo = this.toggleLocalVideo.bind(this);
    this.getSIPStatus = null;
    this.callDurationChecker = null;

    this.child = React.createRef();
  }

  componentDidMount() {
    document.body.classList.remove("sign-bg");
    document.body.classList.add("grey-bg");

    let invitationCode = this.props && this.props.location && this.props.location.search && (this.props.location.search).split('invitationcode=').length > 1 ? (this.props.location.search).split('invitationcode=')[1] : ''
    console.log('history ====== ', this.props);

    let selectedCompanyId = this.props.match.params.id;
    let selectedCompanyName = this.props.location.state && this.props.location.state.companyName;

    // Fetch user from local storage
    let that = this;
    let storage = localStorage.getItem("MIO_Local");
    storage = storage ? JSON.parse(storage) : null;
    if (storage && storage.data.userId) {

      // Checking whether it is MIO_ADMIN or not --
      if (selectedCompanyId && selectedCompanyName && storage.data.userType == cred.USER_TYPE_ADMIN) {

        this.setState({
          userId: storage.data.userId,
          userName: storage.data.fname + ' ' + storage.data.lname,
          userType: storage.data.userType,
          userEmail: storage.data.email,
          userCompanyId: selectedCompanyId,
          selectedCompanyName: selectedCompanyName,
          invitationCode,
        }, () => {
          console.log("user type ===", this.state.userType);
          that.fetchAddresses();
          that.getAdminConferences();
          that.getCountryCodeList();
          invitationCode && that.joinConferenceByCode();
        });

      } else {

        this.setState({
          userId: storage.data.userId,
          userName: storage.data.fname + ' ' + storage.data.lname,
          userType: storage.data.userType,
          userEmail: storage.data.email,
          userCompanyName: storage.data.companyName,
          userCompanyId: storage.data.companyId,
          invitationCode,
          companyType: storage.data.companyType,
        }, () => {
          that.fetchAddresses();
          that.getConferences();
          that.getCountryCodeList();
          invitationCode && that.joinConferenceByCode();
        });

      }

    }


    // get Nexmo Jwt for web-client
    this.getNexmoJwt();

    // else {
    //   this.props.history.push("/");
    // }
  }

  componentWillUnmount() {
    let that = this;
    clearInterval(that.getSIPStatus);
    clearInterval(that.callDurationChecker);

  }

  //#endregion constructor and life cycle hook

  //#region Conference module

  videoFullScreenModeOn() {
    this.setState({ isFullScreen: true, isChatOpen: false });
    let element = document.getElementById('fullconf');
    element && element.classList.remove("col-xl-8");
    element && element.classList.add("col-xl-12");
  }

  videoFullScreenModeOff() {
    this.setState({ isFullScreen: false, isChatOpen: true });
    let element = document.getElementById('fullconf');
    element && element.classList.remove("col-xl-12");
    element && element.classList.add("col-xl-8");
  }

  /**
  * Get conferences by userid.
  */
  getConferenceById = (id, contactNumber, countryCode) => {
    // return new Promise((resolve, reject) => {

    let dataToSend = {
      "conferenceId": id
    };

    axios
      .post(path + 'conference/fetch-conference-by-id', dataToSend)
      .then(serverResponse => {
        let res = serverResponse.data;
        if (!res.isError) {
          // console.log('New API result---- get conference by id--------- ', res);
          let participants = res.details && res.details[0].participants;
          let sessionId = res.details && res.details.sessionId;
          console.log('New API result---- get conference by id--------- / participants', participants);

          // ADMIN will not be conferenceMember ---/START
          let members = [];
          participants && participants.map((value, index) => {
            if (!value.isAdmin) {
              members.push(value);
            }
          })
          // ADMIN will not be conferenceMember ---/START

          if (contactNumber && countryCode) {
            let callingParticipant = _.filter(members, { 'contactNumber': contactNumber, 'countryCode': countryCode })[0];
            // console.log("SIP Call status --", callingParticipant)
            let state = callingParticipant && callingParticipant.state;
            let reason_code = callingParticipant && callingParticipant.reason_code;
            let reason_message = callingParticipant && callingParticipant.reason_message;
            let event = callingParticipant && callingParticipant.event;
            // console.log("SIP Call status -- ",reason_message, event );
            let sipData = {
              'contactNumber': contactNumber
            };

            console.log('fetch-conference-by-id --- / in setInterval -- ', sipData);
          }

          this.setState({
            conferenceMember: members
          })

        } else {
          console.log('New API result---- get conference by id---------// error', res);
          // reject();
        }
      })
      .catch(error => {
        console.log(error);
        // reject();
      })

    // });
  }

  // getting total conference list / USER-- 
  getConferences() {
    // console.log('here========================');
    return new Promise((resolve, reject) => {
      let { userId } = this.state;
      let that = this;
      let dataToSend = {
        "userId": userId,
      };
      this.setState({ listLoading: true });

      axios
        .post(path + "conference/list-conference", dataToSend, {
          headers: { "Content-Type": "application/json" }
        })
        .then(serverResponse => {
          console.log("Response from getConferences : ", serverResponse.data);
          const res = serverResponse.data;
          if (!res.isError) {
            let conferenceList = res.details;
            // console.log(res.details);

            if (this.state.selectedConferenceId) {
              let selectedConference = _.filter(conferenceList, { '_id': this.state.selectedConferenceId })[0];

              // ADMIN will not be in conferenceMember --/START
              let participants = [];
              selectedConference && selectedConference.participants && selectedConference.participants.map((value, index) => {
                if (!value.isAdmin) {
                  participants.push(value);
                }
              })
              // ADMIN will not be in conferenceMember --/END

              selectedConference && this.setState({
                conferenceMember: participants,
                selectedConference: selectedConference,
                listLoading: false
              });
              console.log('in get conference --- // selected conference members', selectedConference.participants);
            }

            that.setState({
              conferenceList: conferenceList,
              listLoading: false
            }, () => resolve());

          } else {
            this.setState({
              listLoading: false
            }, () => {
              reject();
            });

          }
        })
        .catch(error => {
          console.log(error);
          this.setState({
            listLoading: false
          }, () => {
            reject();
          });
        });
    });

  }

  // getting total conference list for ADMIN -- 
  getAdminConferences = () => {

    return new Promise((resolve, reject) => {

      let dataToSend = {
        "companyId": this.state.userCompanyId,
        "page": '',
        "perPage": ''
      };
      this.setState({ listLoading: true });
      axios
        .post(path + 'conference/list-conference-company', dataToSend)
        .then(serverResponse => {

          let res = serverResponse.data;
          if (!res.isError) {
            // console.log("All conference lists of this company is ==", res);
            let conferenceList = res.details && res.details[0] && res.details[0].results;

            if (this.state.selectedConferenceId) {
              let selectedConference = _.filter(conferenceList, { '_id': this.state.selectedConferenceId })[0];

              // ADMIN will not be in conferenceMember ---/START
              let participants = [];
              selectedConference && selectedConference.participants && selectedConference.participants.map((value, index) => {
                if (!value.isAdmin) {
                  participants.push(value);
                }
              })
              // ADMIN will not be in conferenceMember ---/END

              selectedConference && this.setState({
                conferenceMember: participants,
                listLoading: false
              });
              console.log('in get conference --- // selected conference members', selectedConference.participants);
            }
            // ADMIN will not be in conferenceMember ----/END

            this.setState({
              conferenceList: conferenceList,
              listLoading: false
            }, () => {
              console.log("all conference lists for admin === ", this.state.conferenceList);
              resolve();
            });

          } else {
            this.setState({
              listLoading: false
            }, () => {
              reject();
            });

          }

        })
        .catch(error => {
          console.log(error);
          this.setState({
            listLoading: false
          }, () => {
            reject();
          });
        })

    })

  }

  //gettig country code list -
  getCountryCodeList = () => {

    axios
      .post(path + 'services/get-country-codes')
      .then(serverResponse => {

        let res = serverResponse.data;
        if (!res.isError) {
          this.setState({
            countryCodeList: res.details
          });
        }

      })
      .catch(error => {
        console.log(error);
      })

  }

  /**
   * create a new conference
   */
  createConference = () => {

    let that = this;
    let { userId, conferenceTitle, maxNoOfParticipants, conferenceTitleError, maxNoOfParticipantsError, contactList, callInitiator, conferenceMode, timeOutHrs, timeOutMins, validUpto, recordingPreference, recordingCheck, fileShare, inboundCall, screenWithoutPermission, registerMandatory, hostJoinBy } = this.state;
    !(conferenceTitle) ? this.setState({ conferenceTitleError: "Please enter a title." }) : this.setState({ conferenceTitleError: "" });
    !(callInitiator) ? this.setState({ callInitiatorError: "Please select an initiator." }) : this.setState({ callInitiatorError: "" });
    !(conferenceMode) ? this.setState({ conferenceModeError: "Please select a mode." }) : this.setState({ conferenceModeError: "" });
    !(hostJoinBy) ? this.setState({ hostJoinByError: "Please select a type." }) : this.setState({ hostJoinByError: "" });
    !(validUpto) ? this.setState({ validUpto: "unlimited" }) : this.setState({ validUpto: validUpto });
    !(timeOutHrs) ? this.setState({ timeOutHrsError: "Please select timeout." }) : this.setState({ timeOutHrsError: "" });
    !(timeOutMins) ? this.setState({ timeOutMinsError: "Please select timeout." }) : this.setState({ timeOutMinsError: "" });
    // !(durationHrs) ? this.setState({ durationHrsError: "Please select duration." }) : this.setState({ durationHrsError: "" });
    // !(durationMins) ? this.setState({ durationMinsError: "Please select duration." }) : this.setState({ durationMinsError: "" });
    !(recordingPreference) ? this.setState({ recordingPreferenceError: "Please select recording preference." }) : this.setState({ recordingPreferenceError: "" });
    !(maxNoOfParticipants) ? this.setState({ maxNoOfParticipantsError: "Please select max limit of participants." }) : this.setState({ maxNoOfParticipantsError: "" });

    // maxNoOfParticipants && !isNaN(maxNoOfParticipants) && Number(maxNoOfParticipants) > 0 ? this.setState({ maxNoOfParticipantsError: "" }) : this.setState({ maxNoOfParticipantsError: "Enter valide number of paticipants" });

    if (conferenceTitle && maxNoOfParticipants && !conferenceTitleError && !maxNoOfParticipantsError) {

      this.setState({
        loading: true
      });

      let participants = [];
      participants.push(userId);
      contactList.map(contact => {
        if (contact.isAdded) {
          participants.push(contact.contactUserId);
        }
      })

      // let duration = {
      //   hours: !isNaN(Number(durationHrs)) ? Number(durationHrs) : 1,
      //   minutes: !isNaN(Number(durationMins)) ? Number(durationMins) : 0,
      //   seconds: 0
      // };

      let timeOut = {
        hours: !isNaN(Number(timeOutHrs)) ? Number(timeOutHrs) : 1,
        minutes: !isNaN(Number(timeOutMins)) ? Number(timeOutMins) : 0,
        seconds: 0
      };

      let dataToSend = {
        "conferenceTitle": conferenceTitle,
        "callInitiateBy": callInitiator,
        "companyId": this.state.userCompanyId,
        "hostUserId": userId,
        "broadcastId": '',
        "validUpTo": validUpto,
        "timeOut": timeOut,
        // "duration": duration,
        "callType": conferenceMode,
        "hostJoinBy": hostJoinBy,
        "recordingPreference": recordingPreference,
        "fileShare": fileShare,
        "acceptInboundCall": inboundCall,
        "allowParticipantToShareScreen": screenWithoutPermission,
        "isRegisterMandatory": registerMandatory,
        "maxNoOfParticipants": !isNaN(Number(maxNoOfParticipants)) ? Number(maxNoOfParticipants) : 1,
        "participants": participants
      };

      // console.log('dataToSend === for create conference ==', dataToSend);

      axios
        .post(path + "conference/create-conference", dataToSend, {
          headers: { "Content-Type": "application/json" }
        })
        .then(serverResponse => {
          console.log("Response from here : ", serverResponse.data);
          const res = serverResponse.data;
          if (!res.isError) {
            this.setState({
              loading: false
            });
            // Fatch all conference to refresh the list.
            if (this.state.userType == cred.USER_TYPE_ADMIN) {
              that.getAdminConferences();
            } else {
              that.getConferences();
            }
            that.hideCreateConferenceModal();
          } else {
            this.setState({
              loading: false
            });
            // TODO: Handle create conference error
            that.hideCreateConferenceModal();
          }
        })
        .catch(error => {
          this.setState({
            loading: false
          });
          that.hideCreateConferenceModal();
          // TODO: Handle create conference error
        });

    }

  }


  openCreateConferenceModal() {
    this.setState({
      showCreateConference: true,
      conferenceTitleError: '',
      maxNoOfParticipantsError: '',
      conferenceTitle: '',
    })
  }


  hideCreateConferenceModal() {
    this.setState({
      showCreateConference: false,
      conferenceTitleError: '',
      callInitiatorError: '',
      conferenceModeError: '',
      hostJoinByError: '',
      timeOutHrsError: '',
      timeOutMinsError: '',
      durationHrsError: '',
      durationMinsError: '',
      recordingPreferenceError: '',
      maxNoOfParticipantsError: '',
      screenWithoutPermission: true,
      conferenceTitle: '',
      maxNoOfParticipants: '',
      // callInitiator: '',
      conferenceMode: '',
      validUpto: '',
      timeOutHrs: '',
      timeOutMins: '',
      durationHrs: '',
      durationMins: '',
      recordingPreference: '',
      hostJoinBy: '',
      registerMandatory: false
    });
  }

  /**
   * select aparticular conference from list.
   */
  selectConference(conferenceId) {
    return new Promise((resolve, reject) => {
      if (!this.state.selecting) {
        let that = this;
        that.setState({ selecting: true });
        // for mobile
        that.subMenuMobToggle();
        // for mobile --end
        that.switchConference();

        let { conferenceList, userId } = that.state;
        let selectedConference = _.filter(conferenceList, { '_id': conferenceId })[0];
        // let userName = _.filter(selectedConference.participants, { 'userId': userId })[0].name;

        let userName = '';

        // check whether all participants are allow to screen share
        let isAllowToScreenShare = true;
        if (!selectedConference.allowParticipantToShareScreen && selectedConference.hostUserId != userId) {
          isAllowToScreenShare = false;
        }

        let participants = [];
        selectedConference && selectedConference.participants && selectedConference.participants.map((value, index) => {
          if (!value.isAdmin) {
            participants.push(value);
          }
        })

        that.setState({
          selectedConference: selectedConference,
          selectedConferenceId: conferenceId,
          conferenceMember: participants,
          isAllowToScreenShare
        }, () => {
          console.log("SELECTED CONFERENCE---", this.state.selectedConference);
          console.log(this.state.selectedConference.hostUserId);
          console.log(this.state.userId);
          userName = this._getUserNameById(userId);
        });
        // console.log('selectedConference-> participants ============== ', selectedConference.participants);
        // get sessionId, token, invitationLink from server by conferenceId.
        let sessionId = "";
        let token = "";
        let invitationLink = "";
        let callType = "";
        sessionId = selectedConference.sessionId;
        callType = selectedConference.callType;
        let dataToSend = {
          "conferenceId": selectedConference._id,
          "userId": userId,
          "sessionId": sessionId,
        };
        axios
          .post(path + "conference/join-conference", dataToSend, {
            headers: { "Content-Type": "application/json" }
          })
          .then(serverResponse => {
            console.log('here -----------3');

            const res = serverResponse.data;
            if (!res.isError) {
              token = res.details.token;

              let credentials = {
                "apiKey": cred.OPENTOK.apiKey,
                "sessionId": sessionId,
                "token": token
              }
              otCoreOptions.credentials = credentials;
              // If the conference type is lecture mode
              if (callType == cred.CALL_TYPE.Lecture) {
                let filtered1 = otCoreOptions.packages.filter((value, index, arr) => {
                  return value != 'textChat';
                });
                otCoreOptions.packages = filtered1;
                otCoreOptions.largeScale = true;
                if (selectedConference.hostUserId != userId) {
                  otCoreOptions.communication.subscribeOnly = true;
                  let filtered = otCoreOptions.packages.filter((value, index, arr) => {
                    return value != 'screenSharing';
                  });
                  otCoreOptions.packages = filtered;
                } else {
                  otCoreOptions.communication.subscribeOnly = false;
                }
              } else {
                // current user name in chat setting
                otCoreOptions.textChat.name = `${userName}`;
              }

              otCore = new AccCore(otCoreOptions);
              // console.log('here -----------4');
              console.log('otcore ===', otCore);
              // otCore.checkScreenSharingCapability(function(response) {
              //   console.log(response);

              //   // if (!response.supported || response.extensionRegistered === false) {
              //   //   // This browser does not support screen sharing
              //   // } else if (response.extensionInstalled === false) {
              //   //   // Prompt to install the extension
              //   // } else {
              //   //   // Screen sharing is available.
              //   // }
              // });


              otCore.connect().then((response) => {
                this.setState({ connected: true, selecting: false });
                console.log('here -----------5', response);

                let session = otCore.getSession();

                // let myConnection = session.connection;
                // let allConnections = session.connections;
                let streams = session.streams;
                console.log("here -----------5/streams", streams);
                streams.map(stream => {
                  console.log("here -----------5/stream", stream);
                })

                const events = [
                  'subscribeToCamera',
                  'unsubscribeFromCamera',
                  'subscribeToScreen',
                  'unsubscribeFromScreen',
                  'startScreenShare',
                  'endScreenShare',
                  'streamPropertyChanged',
                ];

                events.forEach(event => otCore.on(event, ({ publishers, subscribers, meta }) => {
                  if (publishers && subscribers && meta) {
                    this.setState({ publishers, subscribers, meta });
                  } else {
                    let otCoreState = otCore.state();
                    this.setState({ publishers: otCoreState.publishers, subscribers: otCoreState.subscribers, meta: otCoreState.meta });
                  }
                }));
                that.registerEventListener(); // Extra added

                // otCore.on('streamPropertyChanged', (event) => {
                //   var subscribers = otCore.getSubscribersForStream(event.stream);
                //   console.log('event ======= ', subscribers.length, event);

                //   // Getting userId changing their states (i.e: hasAudio, hasVideo etc)
                //   let changedUserId = event.stream.connection.data.slice(7);
                //   let action = event.changedProperty;
                //   let actionValue = event.newValue;
                //   let { actionObserver } = this.state;
                //   let thisUser = actionObserver[changedUserId];

                //   if (!thisUser) {
                //     thisUser = { 'hasAudio': false, 'hasVideo': false };
                //   }
                //   if (action === 'hasAudio') {
                //     thisUser['hasAudio'] = actionValue;
                //   }
                //   else if (action === 'hasVideo') {
                //     thisUser['hasVideo'] = actionValue;
                //   }
                //   actionObserver[changedUserId] = thisUser;
                //   this.setState({
                //     actionObserver
                //   }, () => {
                //     console.log(this.state.actionObserver);
                //   });


                //   for (var i = 0; i < subscribers.length; i++) {
                //     console.log('================', subscribers[i]);
                //   }

                // });

                resolve();
              }).catch(error => {
                that.setState({ selecting: false });
                console.log('OT error === ', error);
                if (error.name && error.name === "OT_NOT_CONNECTED") {
                  alert("You are not connected to the internet. Check your network connection.");
                }
              });

            } else {
              // TODO: Handel connect to a conference error
              that.setState({ selecting: false });
              reject();
            }
          }).catch(error => {
            // TODO: Handel connect to a conference error
            console.log('error ===== ', JSON.stringify(error));
            if (error.message === "Network Error") {
              alert("You are not connected to the internet. Check your network connection.");
            }
            that.setState({ selecting: false });
            reject();
          });
      } else {
        reject();
      }
    });
  }

  switchConference = () => {
    if (otCore) {
      otCore.disconnect();
      this.state.active && this.endConferenceCall();

      let element = document.getElementById('startScreenSharing');
      element && element.parentNode.removeChild(element);

      let element2 = document.getElementById('chat');
      element2 && element2.childNodes[0] && element2.removeChild(element2.childNodes[0]);

      let element3 = document.getElementById('dialog-form-chrome');
      element3 && element3.remove();

      let element4 = document.getElementById('dialog-form-ff');
      element4 && element4.remove();
    }

    this.setState({
      selectedConference: '',
      selectedConferenceId: '',
      conferenceMember: '',
      connected: false,
      active: false,
      speakers: [],
      isPresenter: false,
      isAllowToScreenShare: true,
      currentScreen: {},
      currentStream: []
    }, () => {
      if (this.state.userType == cred.USER_TYPE_ADMIN) {
        this.getAdminConferences();
      } else {
        this.getConferences();
      }
    });

  }

  joinConferenceByCode(e) {
    e && e.preventDefault();
    let that = this;
    let { invitationCode, userId, userType, selectedConferenceId } = this.state;

    if (selectedConferenceId != '') {
      that.endConferenceCall();
    }

    let dataToSend = {
      "userType": userType,
      "userId": userId,
      "invitationCode": invitationCode
    };
    this.setState({ joinLoading: true });

    axios
      .post(path + "conference/join-conference-with-code", dataToSend, {
        headers: { "Content-Type": "application/json" }
      })
      .then(serverResponse => {
        console.log("Response from here : ", serverResponse.data);
        const res = serverResponse.data;
        if (!res.isError) {
          // get user id / participants id
          console.log(res);
          if (res.statuscode == 204) {
            alert('Sorry! Invitation code is wrong...');
            that.setState({
              joinLoading: false,
              invitationCode: ''
            });
            if (that.state.userType == cred.USER_TYPE_ADMIN) {
              that.getAdminConferences();
            } else {
              that.getConferences();
            }
            // that.getConferences(); // for updating active and inactive conference's lists // may not be use here.
          } else {
            if (that.state.userType == cred.USER_TYPE_ADMIN) {
              that.getAdminConferences().then(() => {
                let conference = res.details;
                that.selectConference(conference[0]._id).then(data => {
                  console.log('in selectConference..............');
                  // that.startCall();
                  this.prepareForStart();
                });
              });
            } else {
              that.getConferences().then(() => {
                let conference = res.details;
                that.selectConference(conference[0]._id).then(data => {
                  console.log('in selectConference..............');
                  // that.startCall();
                  this.prepareForStart();
                });
              });
            }
            // that.getConferences().then(() => {
            //   let conference = res.details;
            //   that.selectConference(conference[0]._id).then(data => {
            //     console.log('in selectConference..............');
            //     // that.startCall();
            //     this.prepareForStart();
            //   });
            // });
            that.setState({
              joinLoading: false,
              invitationCode: ''
            });

          }

        } else {
          alert('Sorry! Invitation code is wrong...');
          that.setState({
            joinLoading: false,
            invitationCode: ''
          });
        }

      }).catch(error => {
        console.log(error);
      });

  }

  // startCallModified = async () => {
  //   let that = this;
  //   if (!that.state.active) {

  //     // API for start call --
  //     that.setState({ starting: true });
  //     // check whether this user is hostuser or host has started the call
  //     let data = await axios.post(path + "conference/is-conference-ready", { "conferenceId": this.state.selectedConferenceId, userId: this.state.userId });
  //     if (data.data && !data.data.isError) {
  //       otCore.startCall().then(({ publishers, subscribers, meta }) => {
  //         this.setState({ publishers, subscribers, meta, starting: false, active: true });
  //         this.getSessionDetails().then(() => {
  //           let dataToSend = {
  //             userId: this.state.userId,
  //             conferenceId: this.state.selectedConferenceId,
  //             connections: this.state.myConnectionDetails
  //           };

  //           axios
  //             .post(path + "conference/start-conference", dataToSend)
  //             .then(serverResponse => {
  //               console.log(serverResponse.data);
  //               let res = serverResponse.data;
  //               let status = serverResponse.data.details;
  //               // Checking whether host user has initiated the call or not --
  //               if (res.statuscode == 200) {
  //                 console.log("Start conference call /  response ---- ", res);
  //                 if (status.isStarted) {
  //                   // that.getSessionDetails(); // For getting session data
  //                   that.registerEventListener(); // Extra added
  //                   that.getConferences();
  //                   // Calling function to get SIP status if any in the call -- 
  //                   that.getSIPCallStatus(that.state.selectedConferenceId);

  //                   that.callDurationChecker = setInterval(() => this.countCallDuration(), 1000);

  //                   // For calling function of recording if automatically recording selected by default ---
  //                   if (this.state.userId == this.state.selectedConference.hostUserId && this.state.selectedConference.recordingPreference == "AUTOMATICALLY_INITIATED") {

  //                     that.startCallRecording();

  //                   }

  //                 } else {
  //                   that.setState({ starting: false });
  //                   that.endConferenceCall();
  //                   alert("Sorry! You can't join untill host starts the call...");
  //                 }
  //               } else {
  //                 that.setState({ starting: false });
  //                 that.endConferenceCall();
  //                 alert("Sorry! You can't join untill host starts the call...");
  //               }
  //             })
  //             .catch(error => {
  //               that.setState({ starting: false });
  //               alert('Some thing goes wrong please try again!');
  //               console.log(error);
  //             })
  //         })
  //       }).catch(error => {
  //         that.setState({ starting: false });
  //         console.log(error);
  //         if (error.code == 1500) {
  //           // Showing the reason of not startig call -- // error message
  //           alert(error.originalMessage ? error.originalMessage : error.message.split('.')[0]);
  //         } else if (error.code == 1010) {
  //           alert(error.originalMessage ? error.originalMessage : error.message);
  //         }
  //       });
  //     } else {
  //       that.setState({ starting: false });
  //       alert(data.data.message);
  //       // if conference already Started
  //       if (data.data.statuscode == 299) {
  //         that.endConferenceCall();
  //       }
  //     }
  //   }
  // }


  // Applyed only on selected conference.
  changeHostUser = async (userId) => {
    return new Promise(async (resolve, reject) => {
      let that = this;
      let { selectedConference } = this.state;
      try {
        let data = await axios.post(path + "conference/change-host-user", { "conferenceId": this.state.selectedConference._id, userId });
        // console.log(data);

        if (data.data && !data.data.isError && selectedConference && selectedConference._id == data.data.details._id) {
          selectedConference = data.data.details;

          // send signal to other web clients
          let signalData = {
            messageType: 'changehostuser',
            message: '',
            sender: 'Host',
            conferenceId: that.state.selectedConference._id
          };
          this.signalSpecificClient(null, signalData, 'usertouser');

          // Change local state.
          this.setState({ selectedConference }, () => resolve({ isError: false }));
        } else {
          // console.log('in else',selectedConference._id , data.data.details._id);

          resolve({ isError: true });
        }
      } catch (error) {
        // console.log('in catch',error);

        resolve({ isError: true });
      }
    });
  }

  changeHostUserRemote = async (data) => {
    console.log('come hare', data, this.state.selectedConference._id);

    let that = this;
    if (data && data.conferenceId && data.conferenceId == this.state.selectedConference._id) {
      let dataToSend = {
        "conferenceId": data.conferenceId
      };
      axios
        .post(path + 'conference/fetch-conference-by-id', dataToSend)
        .then(serverResponse => {
          let res = serverResponse.data;
          if (!res.isError) {
            let selectedConference = res.details && res.details[0];
            // update selected conference.
            that.setState({ selectedConference });
          }
        });
    }
  }

  onHostJoin = async (data) => {
    console.log('onHostJoin=====', data, this.state.selectedConference._id);
    let that = this;
    let { selectedConference, waiting, starting } = this.state;
    if (data && data.conferenceId && data.conferenceId == selectedConference._id && waiting && starting) {
      that.startConferenceCall();
    } else {
      console.log('NP====', data, data.conferenceId, selectedConference._id, waiting, starting);
    }
  }

  prepareForStart = async () => {
    let that = this;
    let { userId, selectedConference } = this.state;

    that.setState({ starting: true });

    if (!(selectedConference && selectedConference._id)) {
      alert('Please select a conference.');
      that.setState({ starting: false });
      return;
    }

    // check whether this user is hostuser or host has started the call
    let data = await axios.post(path + "conference/is-conference-ready", { "conferenceId": this.state.selectedConferenceId, userId: this.state.userId });
    if (data.data && data.data.isError) {
      if (data.data.statuscode == 405) {
        alert('Something goes wrong. Please try again.');
        that.setState({ starting: false });
      } else if (data.data.statuscode == 204) {
        alert('Conference not found.');
        that.setState({ starting: false });
      } else if (data.data.statuscode == 999) {
        alert('Insufficient balance please refill your account.');
        that.setState({ starting: false });
      } else if (data.data.statuscode == 299) {
        // 'Conference already started.'
        alert(data.data.message);
        that.setState({ starting: false });
        that.endConferenceCall();
      } else if (data.data.statuscode == 409) {
        // Max Number Of Participants limit touched.
        alert(data.data.message);
        that.setState({ starting: false });
      } else if (data.data.statuscode == 410) {
        // Wait for host.
        if (userId == selectedConference.createdBy || (selectedConference.companyAdmins && selectedConference.companyAdmins.includes(userId)) || (selectedConference.mioAdmins && selectedConference.mioAdmins.includes(userId))) {
          let changeStatus = await that.changeHostUser(userId);
          console.log('changeStatus', changeStatus);

          if (!changeStatus.isError) {
            that.startConferenceCall();
          } else {
            // Wait for host.
            alert(data.data.message);
            that.setState({ starting: false });
          }
        } else {
          confirmAlert({
            title: "You're Early 😊",
            message: 'Please wait for your host to start the meeting',
            buttons: [
              {
                label: 'Ok',
                // onClick: () => alert('Click Yes')
              }
            ]
          });
          that.setState({ waiting: true });
        }
      }
    } else {
      // Start conference.
      that.startConferenceCall();
    }
  }

  startConferenceCall = async () => {
    let that = this;
    if (!that.state.active) {
      otCore.startCall().then(({ publishers, subscribers, meta }) => {
        this.setState({ publishers, subscribers, meta, starting: false, waiting: false, active: true });
        this.getSessionDetails().then((otcoreSession) => {
          // let myConnection = otcoreSession.connection;
          // let allConnections = otcoreSession.connections;
          // console.log('allConnections === === ===', allConnections);

          let dataToSend = {
            userId: this.state.userId,
            userName: this.state.userName,
            userEmail: this.state.userEmail,
            userType: this.state.userType,
            conferenceId: this.state.selectedConferenceId,
            connections: this.state.myConnectionDetails
          };

          axios
            .post(path + "conference/start-conference", dataToSend)
            .then(serverResponse => {
              console.log(serverResponse.data);
              let res = serverResponse.data;
              let status = serverResponse.data.details;
              // Checking whether host user has initiated the call or not --
              if (res.statuscode == 200) {
                console.log("Start conference call /  response ---- ", res);
                if (status.isStarted) {
                  that.getSessionDetails(); // For getting session data
                  // that.registerEventListener(); // Extra added
                  if (this.state.userType == cred.USER_TYPE_ADMIN) {
                    that.getAdminConferences();
                  } else {
                    that.getConferences();
                  }
                  // Calling function to get SIP outbound status if any in the call -- 
                  that.getSIPCallStatus(that.state.selectedConferenceId);

                  // Start observing conference duration.
                  let calltimeout = Math.floor(that.state.selectedConference.timeOut.hours * 3600) + Math.floor(that.state.selectedConference.timeOut.minutes * 60) + Math.floor(that.state.selectedConference.timeOut.seconds);
                  if (calltimeout > 0) { that.callDurationChecker = setInterval(() => that.countCallDuration(), 1000); }

                  // For calling function of recording if automatically recording selected by default ---
                  if (this.state.userId == this.state.selectedConference.hostUserId && this.state.selectedConference.recordingPreference == "AUTOMATICALLY_INITIATED") {
                    that.startCallRecording();
                  }

                  // send other web client that host is joining
                  if (that.state.userId == this.state.selectedConference.hostUserId) {
                    try {
                      let data = {
                        messageType: 'hostjoin',
                        message: '',
                        sender: 'Host',
                        conferenceId: that.state.selectedConferenceId,
                      };
                      that.signalSpecificClient(null, data, 'usertouser');
                    } catch (error) {
                      console.log('error === === == ', error);
                    }
                  } else {
                    try {
                      let data = {
                        messageType: 'toast',
                        message: `${this.state.userName}` + ' has joined.',
                        sender: '',
                        conferenceId: that.state.selectedConferenceId,
                      };
                      that.signalSpecificClient(null, data, 'usertouser');
                    } catch (error) {
                      console.log('error === === == ', error);
                    }
                  }


                } else {
                  that.setState({ starting: false, waiting: false });
                  that.endConferenceCall();
                  alert("Sorry! You can't join untill host starts the call...");
                }
              } else {
                that.setState({ starting: false, waiting: false });
                that.endConferenceCall();
                alert("Sorry! You can't join untill host starts the call...");
              }
            })
            .catch(error => {
              that.setState({ starting: false, waiting: false });
              alert('Some thing goes wrong please try again!');
              console.log(error);
            })
        })
      }).catch(error => {
        that.setState({ starting: false, waiting: false });
        console.log(error);
        if (error.code == 1500) {
          // Showing the reason of not startig call -- // error message
          alert(error.originalMessage ? error.originalMessage : error.message.split('.')[0]);
        } else if (error.code == 1010) {
          alert(error.originalMessage ? error.originalMessage : error.message);
        }
      });

    }
  }

  saveStreamSubscription(allwebStreamArray, connectionId) {
    let { selectedConference, userId } = this.state;
    axios.post(path + "conference/manipulate-stream-for-webcall", { userId, streams: allwebStreamArray, sessionId: selectedConference.sessionId, connectionId }).then(data => {
      console.log('data.data', data.data);
      if (data.data && !data.data.isError) {
      } else {
      }
    });
  }
  // For getting session data --
  getSessionDetails = () => {
    return new Promise((resolve, reject) => {
      console.log('Inside getSessionDetails function ------------ ');
      let that = this;
      let session = otCore.getSession();
      console.log('Inside getSessionDetails function ------------ ', session);

      let myConnection = session.connection;
      let allConnections = session.connections;
      let streams = session.streams;

      console.log("subscribers..........", session.toString());
      console.log("my connection..........", myConnection.connectionId);

      allConnections.map(thisconnection => {
        console.log("all connection..........", thisconnection);
      });

      // Setting toast notifications start-
      console.log("all conference members of selected one----", this.state.conferenceMember);
      let newMemberId = myConnection.data.split(':')[1];

      let newMemberNameData = _.filter(this.state.conferenceMember, { 'userId': newMemberId })[0];
      let newMemberName = newMemberNameData && newMemberNameData.name;
      console.log("New member is joining having name of ----- ", newMemberName);

      let message = "";
      if (newMemberId == this.state.selectedConference.hostUserId) {
        message = "Host " + newMemberName + " of this conference is joining.";
      } else {
        message = newMemberName + " is joining.";
      }

      let activeArray = _.filter(this.state.currentStream, { 'userId': newMemberId });
      let isActive = activeArray && activeArray.length > 0;
      // checking .. whather the member is already active or not ..  
      if (newMemberName && !isActive) {
        this.displayToast(this.state.selectedConference.conferenceTitle, message);
      }
      // Setting toast notifications end--


      let streamArray = [];
      let allwebStreamArray = [];
      let myConnectionDetails = {};
      let hostConnectionDetails = {};
      // console.log(streamArray);
      // TODO : webcalcu: event.stream.streamId sub start by current user if started

      streams.map(stream => {
        console.log("all streams======", stream);

        let streamData = stream.connection.data.split(':');
        let streamElement = {};

        if (streamData && streamData[0] == 'userId') {
          allwebStreamArray.push({ streamId: stream.streamId });
          streamElement = {
            streamId: stream.streamId,
            userId: streamData[1],
            phnNo: '',
            hasAudio: stream.hasAudio,
            hasVideo: stream.hasVideo
          };
          if (streamData[1] == this.state.userId) {
            myConnectionDetails = {
              streamId: stream.streamId,
              connectionId: stream.connection.connectionId
            }
          }
          if (streamData[1] == this.state.selectedConference.hostUserId) {
            hostConnectionDetails = {
              streamId: stream.streamId,
              connectionId: stream.connection.connectionId
            }
          }

        } else {
          streamElement = {
            streamId: stream.streamId,
            userId: '',
            phnNo: streamData[1],
            hasAudio: stream.hasAudio,
            hasVideo: stream.hasVideo
          };
        }


        streamArray = streamArray.concat(streamElement);
        // console.log("streamArray-----", streamArray);

        if (stream.hasAudio) {
          this.findSpeakers(stream);
        }

        // For screen sharing -- 
        if (stream.hasVideo && stream.videoType === "screen") {
          if (Object.keys(this.state.currentScreen).length === 0) { // if screen sharing is going on other can't share their one
            // showing tost notification for screen sharing and control -- 
            this.screenSharingStarting(stream);
          }

        }

      })

      that.saveStreamSubscription(allwebStreamArray, myConnection.connectionId);

      this.setState({
        currentStream: streamArray,
        myConnectionDetails: myConnectionDetails,
        hostConnectionDetails: hostConnectionDetails
      }, () => {
        console.log("Current stream array----", this.state.currentStream);
        console.log("Current connection details Obj----", this.state.myConnectionDetails);
        console.log("Connection details of host user----", this.state.hostConnectionDetails);
        resolve(session);
      });

    });

  }


  registerEventListener = () => {
    let that = this;
    let session = otCore.getSession();

    // 1.>> "streamCreated" / Start --->
    session.on("streamCreated", function (event) {
      console.log('event/streamCreated/stream =======', event.stream);
      console.log('getSubscribersForStream1', otCore.getSubscribersForStream(event.stream));
      console.log('getPublisherForStream', otCore.getPublisherForStream(event.stream));

      // calling for getting sip call status start --- 

      let creatingMember = event.stream.connection.data.split(':');
      console.log("registerEventListener / new stream member --------", creatingMember);

      if (that.state.userType == cred.USER_TYPE_ADMIN) {
        that.getAdminConferences().then(() => {

          // that.getSessionDetails().then(() => {
          if (creatingMember[0] == "phoneNumber") {
            // To DO
            console.log(creatingMember[1]);
          } else {
            // TODO:
            let { currentStream } = that.state;
            let activeArray = _.filter(currentStream, { 'userId': creatingMember[1] });
            let isActive = activeArray && activeArray.length > 0;
            let userName = that._getUserNameById(creatingMember[1]);
            let message = '';

            console.log("openning inbound == event", creatingMember[0]);

            if (userName) {
              message = userName + ' Is joining.';
            } else if (creatingMember[3] && creatingMember[3].includes('447451271121')) {
              message = 'Dial in Active.';
            } else {
              message = 'Someone is joining.';
            }
            // !isActive && that.displayToast(creatingMember[1], "Join")
            if (!isActive) {
              that.displayToast(that.state.selectedConference.conferenceTitle, message);
            }

          }

          // });
        });
      } else {
        that.getConferences().then(() => {

          // that.getSessionDetails().then(() => {
          if (creatingMember[0] == "phoneNumber") {
            // To DO
            // console.log(creatingMember[1]);
          } else {
            // TODO:
            let { currentStream } = that.state;
            let activeArray = _.filter(currentStream, { 'userId': creatingMember[1] });
            let isActive = activeArray && activeArray.length > 0;
            let userName = that._getUserNameById(creatingMember[1]);
            let message = '';
            if (userName) {
              message = userName + ' Is joining.';
            } else if (creatingMember[3] && creatingMember[3].includes('447451271121')) {
              message = 'Dial in Active.';
            } else {
              message = 'Someone is joining.';
            }
            // !isActive && that.displayToast(creatingMember[1], "Join")
            if (!isActive) {
              that.displayToast(that.state.selectedConference.conferenceTitle, message);
            }

          }

          // });
        });
      }

      that.getSessionDetails();
      that.findSpeakers(event.stream);
    })
    // "streamCreated" / End --> 

    // 2.>> "streamDestroyed" / Start --->
    session.on("streamDestroyed", function (event) {

      console.log('event/streamDestroyed/stream =======', event.stream);
      console.log("Stream " + event.stream.name + " ended. " + event.reason);

      // resetting "speaking" in status of the member getting disconnected from call
      let destroyingMember = event.stream.connection.data.split(':')[1];
      // console.log(destroyingMember);

      let speaker = [];
      speaker[destroyingMember] = false;
      let newSpeaker = [];
      newSpeaker = that.state.speakers.concat(speaker);

      that.setState({
        speakers: newSpeaker
      }, () => {
        that.getSessionDetails().then(() => {
          that.getConferenceById(that.state.selectedConferenceId)
        })
      });


      if (event.stream.hasVideo && event.stream.videoType === "screen") {
        if (Object.keys(that.state.currentScreen).length != 0) { // if screen sharing is going on other can't share their one
          // showing tost notification for screen sharing and control -- 

          that.screenSharingEnding();

        }
      }

    });
    // "streamDestroyed" / End ---> 

    // 3.>> "signal:mute" / Start --->
    session.on("signal:mute", function (event) {
      if (event.from.connectionId !== session.connection.id) {
        // let data = event.data;
        that.toggleLocalAudio();

      }
    });
    // "signal:mute" / End --->

    // 4.>> "signal:host" / Start --->
    session.on("signal:host", function (event) {
      if (event.from.connectionId !== session.connection.id) {
        let data = event.data;
        if (data) {
          data = JSON.parse(data);
        }
        console.log('event.data ==', data);
        that.actionForHost(data);
      }
    });
    // "signal:host" / End --->

    // 4.>> "signal:usertouser" / Start --->
    session.on("signal:usertouser", function (event) {
      console.log('event.data ==');
      if (event.from.connectionId !== session.connection.id) {
        let data = event.data;
        if (data) {
          data = JSON.parse(data);
        }
        console.log('event.data ==', data);
        that.actionForUser(data);
      }
    });
    // "signal:usertouser" / End --->

    //6.>> ScreenSharing events/START --
    otCore.on('startScreenSharing', (publisher) => {

      //that.getSessionDetails();
      // For screen sharing start  // for mirror efffect-- 
      let connectionData = publisher.stream.connection.data;
      let sharedUser = connectionData.split(':')[1];
      if (sharedUser === that.state.userId) {
        that.setState({ isPresenter: true });
      } else {
        that.setState({ isPresenter: false });
      }

      console.log('startScreenSharing', publisher);
      let element = document.getElementById('startScreenSharing');
      element && element.classList.add("active");
      // document.body.classList.remove("sign-bg");

    });

    // For screen sharing END -- 
    otCore.on('endScreenSharing', (publisher) => {
      that.setState({ isPresenter: false });
      console.log('endScreenSharing', publisher);

      // Showing toast notification for end screen share and control -- 
      that.screenSharingEnding();

      let element = document.getElementById('startScreenSharing');
      element && element.classList.remove("active");

    });

    otCore.on('screenSharingError', (error) => {
      console.log('otCore.screenSharingError ==== ', error);
      if (error && error.code == 1500) {
        alert('Screen sharing not supported.');
      }

    });

    otCore.on('archiveError', (error) => {
      console.log('otCore.archiveError ==== ', error);

    });

    // For new connection entered into an ongoing conference - 
    session.on("connectionCreated", function (event) {
      console.log('session.connectionCreated ==== ', event);

      that.getSessionDetails().then(() => {
        that.getConferenceById(that.state.selectedConferenceId)
      });

    });

    // For ending call while host is leaving conference --
    session.on("connectionDestroyed", function (event) {
      // connectionCount--;
      // displayConnectionCount();

      console.log('Is host leaving the conference? -- ', event);
      let leavingUserId = event.connection && event.connection.data && event.connection.data.split(':')[1];
      // alert(leavingUserId+' - is leaving the room');
      if (that.state.selectedConference.hostUserId == leavingUserId) {
        if (that.state.selectedConference.hostUserId === that.state.userId) {
          that.endConferenceCall();
        } else {
          axios
            .post(path + "conference/is-host-join", { "conferenceId": that.state.selectedConferenceId })
            .then(data => {
              if (data.data && !data.data.isError && data.data.details) {
                console.log('hereeee1');
              } else {
                console.log('hereeee2');
                let userName = that._getUserNameById(leavingUserId);
                let message = 'Host - ' + userName + ' Is Leaving...';
                that.displayToast(that.state.selectedConference.conferenceTitle, message);
                that.endConferenceCall();
              }
            });
        }
      }
    });
  }


  unregisterEventListener = () => {
    console.log('unregisterEventListener------');
    let session = otCore ? otCore.getSession() : null;
    session && session.off("streamCreated", () => {
      console.log('unregister streamCreated.');
    });
    session && session.off("streamDestroyed", () => {
      console.log('unregister streamDestroyed.');
    });
    otCore && otCore.off("startScreenSharing", () => {
      console.log('unregister startScreenSharing.');
    });
    otCore && otCore.off("endScreenSharing", () => {
      console.log('unregister endScreenSharing.');
    });
  }

  speakerDetection = function (subscriber, userData, startTalking, stopTalking) {
    let that = this;
    var activity = null;
    subscriber.on('audioLevelUpdated', function (event) {
      var now = Date.now();
      if (event.audioLevel > 0.05) {
        if (!activity) {
          activity = { timestamp: now, talking: false };
        } else if (activity.talking) {
          activity.timestamp = now;
        } else if (now - activity.timestamp > 1000) {
          // detected audio activity for more than 1s
          // for the first time.
          activity.talking = true;
          if (typeof (startTalking) === 'function') {
            if (userData && userData.split(':').length === 2) {
              let speakers = that.state.speakers ? that.state.speakers : {};
              // let newSpeakers= [];
              speakers[userData.split(':')[1]] = true;

              that.setState({ speakers }, () => { console.log("Speaking...", that.state.speakers); });
            }
            startTalking();
          }
        }
      } else if (activity && now - activity.timestamp > 3000) {
        // detected low audio activity for more than 3s
        if (activity.talking) {
          if (typeof (stopTalking) === 'function') {
            if (userData && userData.split(':').length === 2) {
              let speakers = that.state.speakers ? that.state.speakers : {};
              // let newSpeakers= [];
              speakers[userData.split(':')[1]] = false;

              that.setState({ speakers }, () => { console.log("Speaking...", that.state.speakers); });
            }
            stopTalking();
          }
        }
        activity = null;
      }
    });

  };

  findSpeakers = (stream) => {

    let allSubscribers = otCore.getSubscribersForStream(stream);
    if (allSubscribers && allSubscribers.length > 0) {
      allSubscribers.map(subscriber => {
        this.speakerDetection(subscriber, stream.connection.data, function () {
          console.log('started talking');
        }, function () {
          console.log('stopped talking');
        });

        console.log("getting Subscribers-------", subscriber);

      });

    }
  }

  endConferenceCall() {
    let that = this;
    // console.log('ooooooooooooooooooooooooooooooooooooo');

    // that.getConnectionIdFromSession().then(() => {
    if (otCore) {
      let webUserConnectionIds = that.getConnectionIdFromSession();
      that.stopCallRecording() // stop call recording if started
      that.videoFullScreenModeOff(); // on call end full screen will compress
      that.unregisterEventListener(); // Extra added
      otCore.disconnect();
      otCore.endCall();

      let selectedConference = this.state.selectedConference;
      let userId = this.state.userId;
      that.setState({
        active: false,
        connected: false
      }, () => { });

      // console.log("web user's connection Ids -- ",webUserConnectionIds);
      // API for end call --
      let dataToSend = {
        userId: this.state.userId,
        conferenceId: this.state.selectedConferenceId,
        connectionId: webUserConnectionIds,
        sessionId: this.state.selectedConference.sessionId
      }

      console.log("DataToSend for end-conference -- ", dataToSend);

      axios
        .post(path + "conference/end-conference", dataToSend)
        .then(serverResponse => {
          that.setState({
            selectedConference: '',
            selectedConferenceId: '',
            conferenceMember: '',
            connected: false,
            active: false,
            speakers: [],
            isPresenter: false,
            isAllowToScreenShare: true,
            currentScreen: {},
            currentStream: [],
            timer: 0
          }, () => {
            clearInterval(that.callDurationChecker)
          });
          // that.getConferences();
          clearInterval(that.getSIPStatus); // stopping setInterval 
          console.log("after ending conference call ---> ", serverResponse.data);
          // window.location.reload();
          // window.location = window.location.href.split("?")[0];
          console.log('lllllllll ==== ', selectedConference.createdBy, userId);

          if (selectedConference.createdBy == userId || selectedConference.hostUserId == userId) {
            that.props.history.push({
              pathname: `/useses/${selectedConference._id}`,
              state: { selectedConference, isCostDisplay: this.state.companyType && this.state.companyType == cred.COMPANY_TYPE_ENTERPRISE || this.state.companyType == cred.COMPANY_TYPE_FREE_TYPE ? false : true }
            });
          } else {
            that.props.history.push('/thankyou');
          }
          window.location.reload();
        })
        .catch(error => {
          console.log(error);
        })
    }
    // })
  }

  getConnectionIdFromSession = () => {
    let session = otCore.getSession();
    // let myConnection = session.connection;
    let allConnections = session.connections;

    console.log("all connection..........", allConnections);

    let connectionArray = [];

    allConnections.map(thisconnection => {

      console.log("all connection..........", thisconnection);
      let hasData = thisconnection.data && thisconnection.data.split(':')[0];
      // if (hasData == 'userId' && thisconnection.connectionId) {
      console.log("thisconnection.connectionId----", thisconnection.connectionId);
      let connectionId = thisconnection.connectionId;
      connectionArray = [...connectionArray, connectionId];
      // }
    })
    return connectionArray;
  }

  getPublisherForStream() {
    console.log("In getPublisherForStream..............");
  }

  toggleLocalAudio() {
    otCore.toggleLocalAudio(!this.state.localAudioEnabled);
    this.setState({ localAudioEnabled: !this.state.localAudioEnabled });
  }

  toggleLocalVideo() {
    otCore.toggleLocalVideo(!this.state.localVideoEnabled);
    this.setState({ localVideoEnabled: !this.state.localVideoEnabled });
  }

  // For adding member inside a conference room / start -->
  openAddMemberModal(conferenceId) {
    console.log("add member in conference room----", conferenceId);
    // this.setState({selectedConferenceId: conferenceId, showMember: true});
    this.setState({ showMember: true });
  }

  hideAddMemberModal() {
    this.setState({ showMember: false });
  }

  // For addding members from address book -- 
  addOrRemoveToConference(id) {
    let { contactList } = this.state;
    contactList.map(contact => {
      if (contact._id === id) {
        contact['isAdded'] = !contact.isAdded;
      }
      return false;
    });
    // console.log(contactList);
  }

  // For adding member inside a conference room -->
  addMemberFromAddressBook() {

    let members = [];
    this.state.contactList.map(contact => {
      console.log("contact is added----", contact);
      if (contact.isAdded) {
        members.push({
          "userId": contact.contactUserId,
          "userType": contact.userType
        });
        // members["userType"].push(contact.userType);
      }
    })

    let dataToSend = {
      "conferenceId": this.state.selectedConferenceId,
      "participants": members,
      "conferenceTitle": this.state.selectedConference.conferenceTitle
    };

    let that = this;
    // API
    axios
      .post(path + "conference/add-participant", dataToSend,
        { headers: { "Content-Type": "application/json" } })
      .then(serverResponse => {
        // console.log("Response of Add member----", serverResponse);
        const res = serverResponse.data;
        if (!res.isError) {
          // console.log(res);
          if (that.state.userType == cred.USER_TYPE_ADMIN) {
            that.getAdminConferences().then(() => {
              that.selectConference(this.state.selectedConferenceId);
            });
          } else {
            that.getConferences().then(() => {
              that.selectConference(this.state.selectedConferenceId);
            });
          }
        } else {

        }

      })
      .catch(error => {
        console.log(error);
      })

    this.setState({ showMember: false });

  }

  // For Deleting conference -- 
  openDeleteModal = (selectedConferenceId) => {
    // console.log(selectedConferenceId);

    this.setState({
      showDeleteModal: true,
      conferenceToDelete: selectedConferenceId
    });
  }


  hideDeleteModal = () => {
    this.setState({ showDeleteModal: false });
  }

  deleteConference = () => {

    let dataToSend = {
      conferenceId: this.state.conferenceToDelete
    };
    this.setState({ loading: true });
    // API -- 
    axios
      .post(path + "conference/delete-conference", dataToSend)
      .then(serverResponse => {

        console.log(serverResponse.data);
        let res = serverResponse.data;
        if (!res.isError) {
          this.setState({
            loading: false,
            showDeleteModal: false,
            conferenceToDelete: ''
          });

          this.switchConference();
          if (this.state.userType == cred.USER_TYPE_ADMIN) {
            this.getAdminConferences();
          } else {
            this.getConferences();
          }
        }

      })
      .catch(error => {
        console.log(error);

      })

  }

  /**********************************/
  // For Util functions -- 
  _getUserNameById = (userId) => {
    let userName = '';
    if (userId) {
      if (this.state.selectedConference && this.state.selectedConference.participants) {
        try {
          let userNameData = this.state.selectedConference.participants && _.filter(this.state.selectedConference.participants, { 'userId': userId })[0];
          userName = userNameData && userNameData.name? userNameData.name : '';
        } catch (err) {

        }
      }
    }

    return userName;

  }

  _getStreamUserDetails = (data) => {

    let userDetails = {
      name: '',
      value: ''
    };

    if (data && data.length > 0) {
      let dataPart = data.split(':');

      userDetails = {
        name: dataPart[0],
        value: dataPart[1]
      };
    }
    return userDetails;

  }

  /****************************/
  toggleRemortAudio = (userId) => {

    let session = otCore.getSession();
    // let myConnection = session.connection;
    let allConnections = session.connections;
    allConnections.map(thisconnection => {
      let data = this._getStreamUserDetails(thisconnection.data);
      if (data && data.value == userId) {
        // this.signalSpecificClient(thisconnection, true, 'mute').then(() => {
        //   this.getSessionDetails();

        // });

        this.getSessionDetails().then(() => {
          this.signalSpecificClient(thisconnection, true, 'mute');
        });

      }

    });

  }

  // For checking call timeOut --
  countCallDuration = () => {

    let callDuration = 0;
    if (this.state.selectedConference.timeOut) {

      callDuration = Math.floor(this.state.selectedConference.timeOut.hours * 3600) + Math.floor(this.state.selectedConference.timeOut.minutes * 60) + Math.floor(this.state.selectedConference.timeOut.seconds);

    }
    if (this.state.timer < callDuration) {
      let { timer } = this.state;
      this.setState({
        timer: timer + 1
      }, () => {
        // console.log("timer count is ==", this.state.timer);
      });
    } else {
      if (this.state.userId == this.state.selectedConference.hostUserId) {
        this.setState({
          callTimerModal: true,
          callTimerMessage: "Your conference time duration has been exceeded. Do you want to continue?"
        })
      } else {
        this.continueAvoidingTimer();
      }
    }

  }

  // For continueing call after exceeding callDuration -
  continueAvoidingTimer = () => {

    clearInterval(this.callDurationChecker);
    this.setState({
      timer: 0,
      callTimerModal: false,
      callTimerMessage: ''
    });

  }

  // For stoping call by getting timer alert --
  stopCallByTimer = () => {

    clearInterval(this.callDurationChecker);
    this.setState({
      timer: 0,
      callTimerModal: false,
      callTimerMessage: ''
    }, () => {
      this.endConferenceCall();
    });

  }

  //#endregion conference module

  //#region Address book module

  fetchAddresses() {
    console.log('fetchAddresses============');

    let that = this;
    let { userId } = this.state;
    let dataToSend = {
      "userId": userId,
    };
    axios
      .post(path + "user/get_contact_list", dataToSend, {
        headers: { "Content-Type": "application/json" }
      })
      .then(serverResponse => {
        console.log("Response from here : ", serverResponse.data);
        const res = serverResponse.data;
        if (!res.isError) {
          let contactList = res.details.results;
          console.log(res.details.results);
          that.setState({ contactList });
        } else {

        }
      }).catch(error => {
        console.log(error);
      });
  }

  openAddressBookModal() {
    this.setState({ showAddressBook: true })
  }

  hideAddressBookModal() {
    this.setState({ showAddressBook: false })
  }

  //#endregion Address book module

  //#region SIP Outbound module

  // In group SIP call hide and show checkBox as per call status -- 
  sipCallInGroup(id) {

    // let { conferenceMember } = this.state;
    // conferenceMember.map(contact => {
    //   if (contact._id === id) {
    //     contact['isCalled'] = !contact.isCalled;
    //   }
    //   return false;
    // });

    let { contactList } = this.state;
    contactList.map(contact => {
      if (contact._id === id) {
        contact['isCalled'] = !contact.isCalled;
      }
      return false;
    });

    // console.log(conferenceMember);
  }

  // For SIP call in group from address book -- 
  groupSipCallFromAddressBook() {
    let that = this;
    let group = [];
    let dataToSend = {};

    // this.state.conferenceMember.map(member => {
    //   if (member.isCalled) {
    //     group.push(
    //       {
    //         "countryCode": `${member.countryCode}`,
    //         "toMobile": `${member.contactNumber}`
    //       }
    //     )
    //     // member['isInCall'] = true; 

    //   }
    // })
    // console.log(this.state.conferenceMember);

    this.state.contactList.map(member => {
      if (member.isCalled) {
        group.push(
          {
            "countryCode": `${member.countryCode}`,
            "toMobile": `${member.phoneNumber}`
          }
        )
        // member['isInCall'] = true;
      }
    })

    // console.log("group sip--", group);

    dataToSend = {
      "sessionId": this.state.selectedConference.sessionId,
      "fromMobile": "9874433192",
      "mioUsers": group
    };
    // console.log(dataToSend);
    this.setState({ showMember: false });

    // API -- 
    axios
      .post(path + "sip-dial/sip-dial-out-multiple", dataToSend)
      .then(serverResponse => {

        // hiding modal --
        // this.setState({ showMember: false });
        this.hideSIPCallModal();

        let res = serverResponse.data.details;

        console.log("Multiple SIP call response----", res);

        var groupStringNotCall = '';
        var groupStringCalling = '';

        res.map((data, index) => {

          console.log(data.message);
          if (!data.isError) {
            if (data.statuscode != 200) {

              console.log(`${data.toUser}` + "  Can't be reached.");
              // resetting selected members --
              // this.state.conferenceMember.map(member => {
              //   if (('' + member.countryCode + member.contactNumber) == data.toUser) {
              //     member.isCalled = !member.isCalled;
              //     member.isInCall = false;

              //   }
              // })

              this.state.contactList.map(member => {
                if ((member.phoneNumber) == data.toUser) {
                  member.isCalled = !member.isCalled;
                  member.isInCall = false;
                }
              })

              groupStringNotCall += data.toUser + ', ';

            } else if (data.statuscode == 200) {

              console.log(`${data.toUser}` + "  is connecting...");
              // resetting selected members --
              // this.state.conferenceMember.map(member => {
              //   if (('' + member.countryCode + member.contactNumber) == data.toUser) {
              //     member.isCalled = !member.isCalled;
              //     member.isInCall = true;
              //   }
              // })

              this.state.contactList.map(member => {
                if ((member.phoneNumber) == data.toUser) {
                  member.isCalled = !member.isCalled;
                  member.isInCall = true;
                }
              })

              groupStringCalling += data.toUser + ', ';

              this.setState({
                sipCallModal: false
              }, () => {
                if (that.state.userType == cred.USER_TYPE_ADMIN) {
                  that.getAdminConferences();
                } else {
                  that.getConferences();
                }
              });

              // that.getConferences().then(() => {

              //   let newParticipants = res.details[0];
              //   console.log("SIP Dial out response // newParticipants -- ", newParticipants);

              //   that.setState({
              //     conferenceMember: newParticipants.participants
              //   });

              // })

            }

          }

        })

        let message = '';
        if (groupStringNotCall != '' && groupStringCalling != '') {
          message = "* " + groupStringNotCall.slice(0, -1) + " Can't be reached." + " and " + "* " + groupStringCalling.slice(0, -1) + " Joining soon.";
        } else if (groupStringNotCall != '' && groupStringCalling == '') {
          message = "* " + groupStringNotCall.slice(0, -1) + " Can't be reached.";
        } else if (groupStringNotCall == '' && groupStringCalling != '') {
          message = "* " + groupStringCalling.slice(0, -1) + " Joining soon.";
        }

        this.displayToast("Dial Out in Group", message);

      })
      .catch(error => {
        console.log(error);

      })

  }

  // For group SIP call modal control --
  openSIPCallModal(conferenceId) {
    this.setState({ sipCallModal: true });
  }

  hideSIPCallModal() {
    this.setState({ sipCallModal: false });
  }

  // Removing member from conference room // start-->
  endSIPCall(countryCode, contactNumber) {
    let that = this;
    // To end the SIP Call of this contactNumber
    console.log("SIP");
    let dataToSend = {
      conferenceId: this.state.selectedConferenceId,
      countryCode: `${countryCode}`,
      toMobile: `${contactNumber}`
    }

    // API 
    axios
      .post(path + "conference/sip-dial-end", dataToSend,
        { header: { "Content-Type": "application/json" } })
      .then(serverResponse => {
        const res = serverResponse.data;
        if (!res.isError) {
          // For notification -- 
          this.displayToast("Dial Out", `${countryCode}${contactNumber} is leaving the room.`);

          console.log("Ending SIP Call ------------------ ", res);
          // that.registerEventListener();

          // For updating conference member after ending call -
          this.getConferenceById(this.state.selectedConferenceId);

          // Hiding from group Sip Call List --
          // this.state.conferenceMember.map(member => {
          //   if (('' + member.countryCode + member.phoneNumber) == `${countryCode}${contactNumber}`) {
          //     // member.isCalled = !member.isCalled;
          //     member.isInCall = false;
          //   }
          // })

          this.state.contactList.map(member => {
            if (('' + member.countryCode + member.phoneNumber) == `${countryCode}${contactNumber}`) {
              // member.isCalled = !member.isCalled;
              member.isInCall = false;
            }
          })

        }
      })
      .catch(error => {
        console.log(error);
      })
  }

  // Removing member from conference room // end-->
  getSIPCallStatus = (selectedConferenceId, contactNumber, countryCode) => {

    this.getSIPStatus = setInterval(() => {

      // console.log('In getSIPCallStatus----------');

      // ******** for outbound START **********//
      let isSIPCall = false;
      this.state.currentStream.map((value, index) => {
        if (value.phnNo != '') {
          isSIPCall = true;
        }
      })

      if (isSIPCall) {

        let dataToSend = {
          "conferenceId": selectedConferenceId
        };

        axios
          .post(path + 'conference/fetch-conference-by-id', dataToSend)
          .then(serverResponse => {
            let res = serverResponse.data;
            if (!res.isError) {
              // console.log('New API result---- get conference by id--------- ', res);
              let participants = res.details && res.details[0].participants;
              let sessionId = res.details && res.details.sessionId;
              // console.log('New API result---- get conference by id--------- / participants', participants);

              let members = [];
              participants && participants.map((value, index) => {
                if (!value.isAdmin) {
                  members.push(value);
                }
              })

              if (contactNumber && countryCode) {
                let callingParticipant = contactNumber && countryCode && _.filter(participants, { 'contactNumber': contactNumber, 'countryCode': countryCode })[0];
                // console.log("SIP Call status --", callingParticipant)
                let state = callingParticipant && callingParticipant.state;
                let reason_code = callingParticipant && callingParticipant.reason_code;
                let reason_message = callingParticipant && callingParticipant.reason_message;
                let event = callingParticipant && callingParticipant.event;
                // console.log("SIP Call status -- ",reason_message, event );
              }

              this.setState({
                conferenceMember: members
              }, () => {
                // console.log('New API result---- get conference====', this.state.conferenceMember);
              })

            } else {
              console.log('New API result---- get conference by id---------// error', res);
              // reject();
            }
          })
          .catch(error => {
            console.log(error);
            // reject();
          })

      }
      // ******** for outbound END **********//

      // // ******* for inbound --START ********//
      // // Calling function to get inbound call details --- 
      // if (this.state.inboundRadio) {
      //   this.getInboundCallDetails();
      // }
      // // ******* for inbound --END *********//

    }, 2000);

  }

  dialSipCall = () => {
    let that = this;
    // console.log(this.state.sipNo);
    console.log(this.state.selectedConference.sessionId);
    let phoneNo = `${this.state.sipNo}`;
    let countryCode = `${this.state.sipCountryCode}`;
    if (countryCode.slice(0, 1) == '+') {
      countryCode = countryCode.slice(1);
    }

    let dataToSend = {
      "sessionId": this.state.selectedConference.sessionId,
      // "sessionId": "2_MX40NjQyNTA3Mn5-MTU3NDc2NjQ3MTExOX53Ly9YajRiNUs4MmovcmFWRElsUmdyUDJ-fg",
      "toMobile": `${phoneNo}`,
      "countryCode": `${countryCode}`,
      "fromMobile": "9874433192"
    }
    console.log(dataToSend.toMobile);

    // APi-- 
    axios
      .post(path + "conference/sip-dial-out", dataToSend,
        { headers: { "Content-Type": "application/json" } })

      .then(serverResponse => {
        const res = serverResponse.data;
        // console.log('res=========================res', res);

        if (!res.isError) {
          // this.getConferences();
          if (that.state.userType == cred.USER_TYPE_ADMIN) {
            that.getAdminConferences().then(() => {
              let newParticipants = res.details[0];
              // Updating the conferenceMembers in state after sip call getting connected 
              that.setState({
                conferenceMember: newParticipants.participants
              });

              // For notification --
              that.displayToast("Dial Out", `+${countryCode}${phoneNo} Is joining soon.`);
              that.setState({ sipNo: '', sipCountryCode: '' });

            });
          } else {
            that.getConferences().then(() => {
              let newParticipants = res.details[0];
              // Updating the conferenceMembers in state after sip call getting connected 
              that.setState({
                conferenceMember: newParticipants.participants
              });

              // For notification --
              that.displayToast("Dial Out", `+${countryCode}${phoneNo} Is joining soon.`);
              that.setState({ sipNo: '', sipCountryCode: '' });

            });
          }
        }
      })
      .catch(error => {
        console.log(error);

      })


  }

  // For SIP Call from conference -- 
  sipCallFromConference(countryCode, contactNumber, from) {
    let that = this;
    let toMobile = `${countryCode}${contactNumber}`
    let dataToSend = {
      "sessionId": this.state.selectedConference.sessionId,
      // "sessionId": "2_MX40NjQyNTA3Mn5-MTU3NDc2NjQ3MTExOX53Ly9YajRiNUs4MmovcmFWRElsUmdyUDJ-fg",
      "toMobile": `${contactNumber}`,
      "countryCode": `${countryCode}`,
      "fromMobile": "9874433192"
    }

    if (from == 'modal') {
      this.setState({ showParticipantsModal: false })
    }

    // APi-- 
    axios
      .post(path + "conference/sip-dial-out", dataToSend,
        { headers: { "Content-Type": "application/json" } })

      .then(serverResponse => {
        const res = serverResponse.data;
        if (!res.isError) {
          // for notification of joining --
          that.displayToast("Dial Out", `${toMobile} Is joining soon.`);

          console.log("SIP Dial out response -- ", res);
          let newParticipants = res.details[0];
          console.log("SIP Dial out response // newParticipants -- ", newParticipants);
          // Updating the conferenceMembers in state after sip call getting connected 

          let participants = [];
          newParticipants && newParticipants.participants && newParticipants.participants.map((value, index) => {
            if (!value.isAdmin) {
              participants.push(value);
            }
          })

          that.setState({
            conferenceMember: participants
          });


          // For finding SIP call status ...
          // that.getSIPCallStatus(that.state.selectedConferenceId, contactNumber, countryCode);
          // For finding SIP call status ..

          // that.registerEventListener();

          // Hiding from group Sip Call List --
          this.state.contactList.map(member => {
            if (('' + member.countryCode + member.phoneNumber) == toMobile) {
              // member.isCalled = !member.isCalled;
              member.isInCall = true;
            }
          })

        }
      })
      .catch(error => {
        console.log(error);

      })


  }

  // For removing SIP user ---
  removeSIPUser = (contactNumber, _id) => {
    let that = this;
    let dataToSend = {
      'conferenceId': this.state.selectedConference._id,
      'participantId': _id
    };

    axios
      .post(path + "conference/remove-participant", dataToSend)
      .then(serverResponse => {
        let res = serverResponse.data;
        if (!res.isError) {

          if (that.state.userType == cred.USER_TYPE_ADMIN) {
            that.getAdminConferences();
          } else {
            that.getConferences();
          }
          // this.getConferences().then(() => {

          //   console.log("after delete/after updation========");

          // })

        }

      })
      .catch(error => {
        console.log(error);
      })

  }

  removeSIPUserFromModal = (contactNumber, _id) => {
    let that = this;
    let dataToSend = {
      'conferenceId': this.state.selectedConference._id,
      'participantId': _id
    };

    axios
      .post(path + "conference/remove-participant", dataToSend)
      .then(serverResponse => {
        let res = serverResponse.data;
        if (!res.isError) {
          if (that.state.userType == cred.USER_TYPE_ADMIN) {
            that.getAdminConferences().then(() => {
              console.log("after delete/after updation========");
              this.setState({
                showParticipantsModal: false
              });
            });
          } else {
            that.getConferences().then(() => {
              console.log("after delete/after updation========");
              this.setState({
                showParticipantsModal: false
              });
            });
          }
        }
      })
      .catch(error => {
        console.log(error);
      })

  }

  //#endregion SIP outbound module

  //#region Screen sharing module

  screenSharingStarting = (stream) => {

    console.log('Inside screenSharing Start ============ ');
    let screenUserId = stream.connection.data.split(':')[1];
    let screenUserData = _.filter(this.state.conferenceMember, { 'userId': screenUserId })[0];
    let screenUser = screenUserData && screenUserData.name;

    let screenSharingUsers = {
      screenUserId: screenUserId
    };

    this.setState({ currentScreen: screenSharingUsers });

    if (this.state.userId != screenSharingUsers.screenUserId) {

      let element = document.getElementById('startScreenSharing');
      element && element.classList.add("hidden");

    }

    let message = screenUser + ' is sharing screen';

    // Setting notification -- 
    this.displayToast(this.state.selectedConference.conferenceTitle, message);

  }


  screenSharingEnding = () => {

    console.log('Inside screenSharing End ============ ');
    this.setState({ currentScreen: {} });
    let message = 'Screen Sharing is being stopped.'
    // Setting notification --
    this.displayToast(this.state.selectedConference.conferenceTitle, message);
    if (this.state.userId != this.state.currentScreen.screenUserId) {
      let element = document.getElementById('startScreenSharing');
      element && element.classList.remove("hidden");
    }

  }

  // For sending request --
  sendRequestToShareScreen = () => {

    let conferenceType = "all";
    let messageType = "askforpresenter";
    let message = " Wants to be a presenter";

    this.sendMessageToHost({ conferenceType, messageType, message }).then(isSuccess => {
      isSuccess && this.displayToast('Hi,', 'Your request has sent to host.');
    });
  }

  // For accepting request 
  acceptScreenShareReq = () => {
    let { presenterRequest } = this.state;
    console.log('--------presenterRequest', presenterRequest)
    this.sendMessageToUser({
      messageType: 'presenteraccept',
      message: 'Accepted',
      receiverUserId: presenterRequest.sender.userId
    });
    this.hidePresenterRequest();
  }

  // For rejecting request -- 
  rejectScreenShareReq = () => {
    let { presenterRequest } = this.state;
    console.log('--------presenterRequest', presenterRequest)
    this.sendMessageToUser({
      messageType: 'presenterreject',
      message: 'Your request has rejected',
      receiverUserId: presenterRequest.sender.userId
    });
    this.hidePresenterRequest();
  }

  hidePresenterRequest() {
    this.setState({
      showPresenterRequest: false,
      presenterRequest: null
    });
  }

  openScreenSharePopup() {
    document.getElementById("startScreenSharing").click();
  }

  //#endregion Screen sharing module

  //#region SIP Inbound module

  // For inbound call chanel open functionality --
  openInboundCall(e) {
    let that = this;
    this.setState({ inboundRadio: !this.state.inboundRadio },
      () => { console.log(this.state.inboundRadio); });

    // For starting Nexmo Call// API -- 
    let dataToSend = {
      sessionId: that.state.selectedConference.sessionId
    };
    // API -- 
    axios
      .post(path + "conference/dialout-for-dialin", dataToSend)
      .then(serverResponse => {

        let resNexmo = serverResponse.data.details;
        console.log(resNexmo);

        that.setState({
          inboundConnectionId: resNexmo.connectionId
        });
        that.inboundCallListener();
        // console.log("Nexmo call starting // response-----",resNexmo);
      })
      .catch(error => {
        console.log(error);
      })
    //For starting NEXMO call // End -- 

  }


  // For inbound call chanel close functionality --
  closeInboundCall(e) {
    let that = this;
    this.setState({ inboundRadio: !this.state.inboundRadio },
      () => { console.log(this.state.inboundRadio); });

    // For ending Nexmo Call// API -- 
    if (that.state.userId == that.state.selectedConference.hostUserId) {
      // only host user can stop the nexmo call while ending the call;

      // console.log("Nexmo connectionId // call end time----", that.state.inboundConnectionId);
      let dataToSendNexmo = {
        sessionId: that.state.selectedConference.sessionId,
        connectionId: that.state.inboundConnectionId
      };

      axios
        .post(path + "conference/dialout-hang-up", dataToSendNexmo)
        .then(serverResponse => {

          let resNexmo = serverResponse.data;
          console.log(resNexmo);

          that.setState({
            inboundConnectionId: '' // resetting the nexmo connection id in state
          });
          // console.log("Nexmo response // end call --- ",resNexmo);

        })
        .catch(error => {
          console.log(error);
        })

    }
    // For Nexmo call end // end -- 

  }

  // Get token from nexmo to access web client
  getNexmoJwt() {
    let that = this;
    axios.post(path + "conference/nexmo-get-jwt", {}).then(data => {
      // console.log('hereeee1', data);
      if (data.data && !data.data.isError && data.data.details) {
        that.setState({ nexmoJWT: data.data.details });
      } else {

      }
    }).catch(error => { console.log(error) });
  }

  // Event listener for inbound callers
  inboundCallListener() {
    let that = this;
    let selectedConference = this.state.selectedConference;
    if (selectedConference) {
      nexmo = new NexmoClient({
        nexmo_api_url: "https://api-eu-1.nexmo.com",
        url: "wss://ws-eu-1.nexmo.com",
        ips_url: "https://api-eu-1.nexmo.com/v1/image",
        debug: true,
        log_reporter: { enabled: true },
      })
        .login(that.state.nexmoJWT)
        .then(async application => {
          console.log('NEXMO Logged in to app==>', application);

          // application.on("call:status:changed", (nxmCall) => {
          //   console.log('NEXMO call:status:changed ==> nxmCall==>', nxmCall);
          //   if (nxmCall.status === nxmCall.CALL_STATUS.STARTED) {
          //     console.log('NEXMO call:status:changed ==> the call has started');
          //   }
          // });
          let nexmoConversation = await that.getNexmoConversationBySessionId();

          application.getConversation(nexmoConversation.id).then(conversation => {
            console.log('conversation ===? ==', conversation);
            // nexmoConversation = conversation;

            // when a dtmf come from an inbound member
            conversation.on("audio:dtmf", (from, event) => {
              console.log('NEXMO DTMF========');
              console.log(
                event.digit, // the dtmf digit(s) received
                event.from, //id of the user who sent the dtmf
                event.timestamp,//timestamp of the event
                event.cid,// conversation id the event was sent to
                event.body, // additional context about the dtmf
              );
              let inboundUser = that.state.inboundUser;
              let fromDetails = event.body.channel.from;
              // && selectedConference.callType == 'LECTURE'
              if (fromDetails.number != "0000000000" && fromDetails.type == "phone") {
                inboundUser && inboundUser.some((user, index) => {
                  if (user.number == fromDetails.number && user.uuid == event.body.channel.id && event.digit == '0') {
                    inboundUser[index].dtmf = event.digit;
                    return true;
                  }
                });
                that.setState({
                  inboundUser: inboundUser
                });
              }

            });

            // when mute an inbound member 
            conversation.on("audio:mute:on", (member, event) => {
              console.log('NEXMO MUTE========');
              console.log(member);
              let inboundUser = that.state.inboundUser;
              let fromDetails = member.channel.from;
              if (fromDetails.number != "0000000000" && fromDetails.type == "phone") {
                inboundUser && inboundUser.some((user, index) => {
                  if (user.number == fromDetails.number && user.uuid == member.channel.id) {
                    inboundUser[index].muteStatus = true;
                    return true;
                  }
                });
                that.setState({
                  inboundUser: inboundUser
                });
              }

            });

            // when unmute an inbound member 
            conversation.on("audio:mute:off", (member, event) => {
              console.log('NEXMO UNMUTE========');
              console.log(member);
              let inboundUser = that.state.inboundUser;
              let fromDetails = member.channel.from;
              if (fromDetails.number != "0000000000" && fromDetails.type == "phone") {
                inboundUser && inboundUser.some((user, index) => {
                  if (user.number == fromDetails.number && user.uuid == member.channel.id) {
                    inboundUser[index].muteStatus = false;
                    return true;
                  }
                });
                that.setState({
                  inboundUser: inboundUser
                });
              }

            });

            // when new inbound member join.
            conversation.on("member:joined", async (member, event) => {
              console.log("NEXMO member:joined ==> ");
              console.log(event)
              console.log('NEXMO ----------------------------');
              console.log(member);
              // get this newly joined mamber name if conference register is mandetory.
              let getSipInbound = () => new Promise((resolve, reject) => {
                axios
                  .post(path + 'sip-inbound/get-sipinbound-by-uuid', { uuid: member.channel.id })
                  .then(serverResponse => {
                    let res = serverResponse.data;
                    if (!res.isError) {
                      let inboundCall = res.details;
                      resolve(inboundCall);
                    } else {
                      resolve(null);
                    }
                  });
              });

              let inboundUser = that.state.inboundUser;
              let fromDetails = member.channel.from;
              if (fromDetails.number != "0000000000" && fromDetails.type == "phone") {
                let inboundCall = null;
                if (selectedConference.isRegisterMandatory) {
                  inboundCall = await getSipInbound();
                }
                let user = {};
                user.uuid = member.channel.id;
                user.number = fromDetails && fromDetails.number;
                user.name = inboundCall ? inboundCall.registerName : '';
                user.muteStatus = selectedConference.callType == 'LECTURE' ? true : false; // this will true if selected conference is lecture mode.
                inboundUser.push(user);
                that.setState({
                  inboundUser: inboundUser
                });
              }
            });

            // When an inbound member left.
            conversation.on("member:left", (member, event) => {
              console.log("NEXMO member:left ==> ");
              console.log(member);
              console.log('NEXMO ----------------------------');
              console.log(event);
              let inboundUser = that.state.inboundUser;
              let fromDetails = member.channel.from;
              if (fromDetails.number != "0000000000" && fromDetails.type == "phone") {
                inboundUser && inboundUser.map((user, index) => {
                  if (user.number == fromDetails.number && user.uuid == member.channel.id) {
                    inboundUser.splice(index, 1);
                  }
                });
                that.setState({
                  inboundUser: inboundUser
                });
              }
            });

            // conversation.getEvents({ page_size: 20, order: 'desc' }).then((events_page) => {
            //   console.log('innnNNNNNNNN', conversation.me);
            //   console.log((events_page));
            // });
            // conversation.on("sip:status", (member) => {
            //   console.log("NEXMO sip:status ==> ");
            //   console.log(member);
            // });

            // conversation.on("sip:hangup", (member) => {
            //   console.log("NEXMO sip:hangup ==> ");
            //   console.log(member);
            // });

            // conversation.on("leg:status:update", (member) => {
            //   console.log("NEXMO leg:status:update ==> ");
            //   console.log(member);
            // });

            // conversation.on("member:call:status", (member) => {
            //   console.log("NEXMO member:call:status ==> ");
            //   console.log(member.callStatus);
            // });


          });
        })
        .catch(err => {
          console.log('NEXMO error ', err)
          that.getNexmoJwt();
        });
    }
    // Calling function to get inbound call details --- 
    // this.getInboundCallDetails();
    // console.log("Inbound users // START CALL ---", inboundCallDetails);



  }

  // To get inbound call details --- 
  getNexmoConversationBySessionId = () => {

    return new Promise((resolve, reject) => {

      let dataToSend = {
        "sessionId": this.state.selectedConference.sessionId
        // "sessionId": "1_MX40NjQyNTA3Mn5-MTU4NDM2MTgxODQ5Mn5YWlphWHc5eTdXUnQ0clVoNW1tL0dLT1Z-fg"
      };

      // API -- 
      axios.post(path + 'sip-inbound/nexmo-call-details', dataToSend).then(serverResponse => {
        let res = serverResponse.data;
        if (!res.isError) {
          resolve(res.details);
        } else {
          resolve(null)
        }
      }).catch(error => {
        console.log(error);
        resolve(null)
      })
    })

  }


  // For mute unmute toggle of inbound users --
  toggleRemortAudioForInbound = (uuid) => {
    let that = this;
    let { inboundUser } = this.state;
    let selectedUser = inboundUser.filter((user) => user.uuid == uuid);
    let action = '';
    if (selectedUser.length > 0) {
      action = selectedUser[0].muteStatus ? 'unmute' : 'mute';
    }
    let dataToSend = {
      "uuid": uuid,
      "action": action
    };
    // API -- 
    axios
      .post(path + 'sip-inbound/modify-nexmo-inprogress-call', dataToSend)
      .then(serverResponse => {
        let res = serverResponse.data;
        if (!res.isError) {
          console.log("mute unmute for inbound callers === ", res);
          inboundUser && inboundUser.some((user, index) => {
            if (user.uuid == uuid) {
              inboundUser[index].muteStatus = dataToSend.action == 'mute' ? true : false;
              return true;
            }
          });
          that.setState({
            inboundUser: inboundUser
          });
        }
      })
      .catch(error => {
        console.log(error);
      })
  }

  // For hangup inbound users --
  endInboundUserCall = (uuid) => {
    let dataToSend = {
      "uuid": uuid,
      "action": 'hangup'
    };
    // API -- 
    axios
      .post(path + 'sip-inbound/modify-nexmo-inprogress-call', dataToSend)
      .then(serverResponse => {
        let res = serverResponse.data;
        if (!res.isError) {
          console.log("End call successfully.");
        }
      })
      .catch(error => {
        console.log(error);
      })
  }

  // For accept raise hand request of an inbound user --
  acceptRaiseHandReq = (uuid) => {
    let that = this;
    let { inboundUser, selectedConference } = this.state;

    // For web clients
    let data = {
      messageType: 'acceptRaiseHandReq',
      message: '',
      sender: 'Host',
    };
    this.signalSpecificClient(null, data, 'usertouser');

    // For inbound caller
    let dataToSend = {
      "uuid": uuid,
      "action": 'accept',
      "sessionId": selectedConference.sessionId
    };
    // API -- 
    uuid && axios
      .post(path + 'sip-inbound/raise-hand-req-action', dataToSend)
      .then(serverResponse => {
        let res = serverResponse.data;
        if (!res.isError) {
          inboundUser && inboundUser.some((user, index) => {
            if (user.uuid == uuid) {
              inboundUser[index].raiseHandReq = 'accepted';
              return true;
            }
          });
          that.setState({
            inboundUser: inboundUser
          });
        }
      })
      .catch(error => {
        console.log(error);
      })
  }

  // For close raise hand request of an inbound user --
  closeRaiseHandReq = (uuid) => {
    let that = this;
    let { inboundUser, selectedConference } = this.state;

    // For web clients
    let data = {
      messageType: 'closeRaiseHandReq',
      message: '',
      sender: 'Host',
    };
    this.signalSpecificClient(null, data, 'usertouser');

    // For inbound caller
    let dataToSend = {
      "uuid": uuid,
      "action": 'close',
      "sessionId": selectedConference.sessionId
    };
    // API -- 
    uuid && axios
      .post(path + 'sip-inbound/raise-hand-req-action', dataToSend)
      .then(serverResponse => {
        let res = serverResponse.data;
        if (!res.isError) {
          inboundUser && inboundUser.some((user, index) => {
            if (user.uuid == uuid) {
              inboundUser[index].raiseHandReq = '';
              inboundUser[index].dtmf = '';
              return true;
            }
          });
          that.setState({
            inboundUser: inboundUser
          });
        }
      })
      .catch(error => {
        console.log(error);
      })
  }

  // subscribeInboundSip if host want
  subscribeInboundSip = () => {
    console.log('hereeeee;;;;etest2e;e;e;e;');
    let session = otCore.getSession();
    let streams = session.streams;
    let inboundStrime = null;
    // `{"sip":true, "role":"client", "name":"'${conferenceNumber}'"}`
    streams && streams.map(stream => {
      console.log("all streams======1", stream);
      if (stream.connection && stream.connection.data) {
        let data = null;
        try {
          data = JSON.parse(stream.connection.data);
        } catch (error) {
          data = null;
        }
        if (data && data.sip && data.role == 'client') {
          inboundStrime = stream
        }
      }
    });
    otCore.subscribe(inboundStrime)
  }

  // unsubscribeInboundSip if host want
  unsubscribeInboundSip = () => {
    console.log('hereeeee;;;;e;;e;e;e;e;');

    let subscribers = this.state.subscribers.sip;
    let subscriber = null;
    subscribers && Object.keys(subscribers).map((key, index) => {
      let thisSub = subscribers[key];
      if (thisSub && thisSub.stream && thisSub.stream.connection && thisSub.stream.connection.data) {
        let data = null;
        try {
          data = JSON.parse(thisSub.stream.connection.data);
        } catch (error) {
          data = null;
        }
        if (data && data.sip && data.role == 'client') {
          subscriber = subscribers[key];
        }
      }
    })
    subscriber && otCore.unsubscribe(subscriber);
  }

  //#endregion

  //#region Chat module
  /*********************************/
  onFileChange = (e) => {
    let file = this.refs.file.files[0];
    if (file) {
      // For checking uploaded file extension -- 
      let blnValid = false;
      let sFileName = file.name;
      var _validFileExtensions = [".jpg", ".jpeg", ".png"];
      for (var j = 0; j < _validFileExtensions.length; j++) {
        var sCurExtension = _validFileExtensions[j];
        if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
          blnValid = true;
          break;
        }
      }
      // console.log(blnValid);

      if (blnValid) { // If the extension matches will be desplayed as it is -- 

        var reader = new FileReader();
        var url = reader.readAsDataURL(file);
        reader.onloadend = function (e) {
          this.setState({
            imageURL: [reader.result]
          })
        }.bind(this);

      } else {
        // If the uploaded file is not of type mentioned above, will be displayed another -- 
        this.setState({
          imageURL: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQYQzzO-D0kZN8PYN2Bt-xUToYTxfK7wZGdSQ5s8nqRuG9xK_kO&s"
        });
      }
    }
  }

  // For file uploading -- 
  handleUploadImage = (ev) => {
    ev.preventDefault();
    // this.setState({uploading: true});
    const photo = new FormData();
    photo.append('file', this.refs.file.files[0]);
    // photo.append('filename', this.uploadInput.files[0].name);

    axios
      .post(path + "conference/upload-participant-file",
        photo,
        {
          headers: { 'Content-Type': 'multipart/form-data' },
          onUploadProgress: (progressEvent) => {
            console.log("raw upload loader ---------", progressEvent.loaded, progressEvent.total);

            let uploadPercentage = parseInt(Math.round((progressEvent.loaded / progressEvent.total) * 100));
            this.setState({ fileUploaded: uploadPercentage }, () => { console.log("File uploaded ....................", this.state.fileUploaded); });

          }
        })
      .then((response) => {

        let res = response.data;
        if (!res.isError) {
          let imgUrl = res.detail.url;
          let inputF = document.getElementById("messageBox");
          let inputSubmit = document.getElementById("sendMessage");
          inputF.value = `<a href="${imgUrl}" target= '_blank'>${this.refs.file.files[0].name}...Download</a>`;
          inputSubmit.click();

          this.setState({ imageURL: '', fileUploaded: 0 });
          console.log(imgUrl);

          console.log("File uploaded in response.....................", this.state.fileUploaded);

        }

      })
      .catch(error => {
        console.log(error);

      })

  }

  chatDisplayToggle = () => {
    // let { isChatOpen, isFullScreen } = this.state;
    // this.setState({isChatOpen:!isChatOpen});

    // best-practices
    this.setState(prevState => ({ isChatOpen: !prevState.isChatOpen }))
  }

  //#endregion

  //#region Signal module
  // Sending a signal to a specific client in a session
  /**
   * connection, data, type
   */
  signalSpecificClient = (connection, data, type = 'textMessage') => {
    return otCore.signal(type, data, connection);
  }


  // for Lecture mode Message
  // Do not call this function in if the user is host.
  // { conferenceType, messageType, message }
  sendMessageToHost = ({ conferenceType, messageType, message }) => new Promise((resolve, reject) => {
    if (conferenceType && messageType && message && this.state.userId && this.state.selectedConference && otCore) {

      // get host connection froom tokbox session
      let session = otCore.getSession();
      if (session) {
        let myConnection = session.connection;
        let allConnections = session.connections;

        let sender = {
          name: '',
          userId: '',
          connection: null,
        }

        sender.name = this._getUserNameById(this.state.userId);
        sender.userId = this.state.userId;
        sender.connection = myConnection;

        let data = {
          conferenceType,
          messageType,
          message,
          sender,
        };
        let status = false;
        allConnections.map(thisconnection => {
          let streamData = this._getStreamUserDetails(thisconnection.data);
          if (streamData && streamData.value == this.state.selectedConference.hostUserId) {
            this.signalSpecificClient(thisconnection, data, 'host');
            this.setState({ messageToHost: '' });
            status = true;
          }
        });

        resolve(status);
      } else {
        alert('Can not send to host');
        resolve(false);
      }
    } else {
      alert('Can not send to host');
      resolve(false);
    }
  });

  actionForHost(data) {

    if (data && data.conferenceType == cred.CALL_TYPE.Lecture && data.messageType == 'faq') {
      // Show notification
      this.displayToast(data.sender.name, data.message);

      // push in messagesForHost
      let messagesForHost = this.state.messagesForHost ? this.state.messagesForHost : [];
      let initial = data && data.sender && data.sender.name ? data.sender.name.charAt(0) : '-'
      messagesForHost.push({ message: data.message, sender: data.sender.name, initial });
      this.setState({ messagesForHost });
    } else if (data && data.messageType == 'askforpresenter') {

      this.setState({
        showPresenterRequest: true,
        presenterRequest: data
      });

    } else {
      alert(typeof data);
    }

  }

  // { metadata = null, messageType, message, receiverUserId }
  sendMessageToUser = ({ metadata = null, messageType, message, receiverUserId }) => new Promise((resolve, reject) => {
    if (receiverUserId && messageType && message && this.state.userId && this.state.selectedConference && otCore) {
      // get host connection froom tokbox session
      let session = otCore.getSession();
      if (session) {
        let myConnection = session.connection;
        let allConnections = session.connections;

        let sender = {
          name: '',
          userId: '',
          connection: null,
        }

        sender.name = this._getUserNameById(this.state.userId);
        sender.userId = this.state.userId;
        sender.connection = myConnection;

        let data = {
          metadata,
          messageType,
          message,
          sender,
        };
        let status = false;
        allConnections.map(thisconnection => {
          let streamData = this._getStreamUserDetails(thisconnection.data);
          if (streamData && streamData.value == receiverUserId) {
            this.signalSpecificClient(thisconnection, data, 'usertouser');
            status = true;
          }
        });
        resolve(status);
      } else {
        alert('Can not send1');
        resolve(false);
      }
    } else {
      alert('Can not send1');
      resolve(false);
    }
  });

  actionForUser(data) {
    if (data && data.messageType == 'presenteraccept') {
      // open screen shareing modal
      this.openScreenSharePopup();
    } else if (data && data.messageType == 'presenterreject') {
      this.displayToast('Hi,', 'Your request for screen share has been rejected.');
    } else if (data && data.messageType == 'acceptRaiseHandReq') {
      this.unsubscribeInboundSip();
    } else if (data && data.messageType == 'closeRaiseHandReq') {
      this.subscribeInboundSip();
    } else if (data && data.messageType == 'changehostuser') {
      this.changeHostUserRemote(data);
    } else if (data && data.messageType == 'hostjoin') {
      this.onHostJoin(data);
    } else if (data && data.messageType == "toast") {
      if (data.messageSubType && data.messageSubType == 'started') {
        this.setState({ recording: true });
        this.displayToast('Hi', data.message)
      } else if (data.messageSubType && data.messageSubType == 'stopped') {
        this.setState({ recording: false });
        this.displayToast('Hi', data.message)
      }
      this.displayToast('Hi', data.message)
    } else {
      alert(typeof data);
    }

  }

  //#endregion Signal module

  //#region Call Recording module

  gotoArchiveList = () => {

    if (this.state.selectedConferenceId != '') {
      this.endConferenceCall();
    }

    if (this.state.userType == cred.USER_TYPE_ADMIN) {
      this.props.history.push({
        pathname: '/companyarchives',
        state: { companyId: this.state.userCompanyId, name: this.state.selectedCompanyName }
      });
    } else {
      this.props.history.push({
        pathname: '/companyarchives'
      });
    }
  }

  startCallRecording = () => {
    let that = this;
    let { selectedConference } = this.state;
    if (selectedConference && selectedConference.sessionId) {
      let dataToSend = {
        "sessionId": selectedConference.sessionId,
        "conferenceId": selectedConference._id,
        "conferenceTitle": selectedConference.conferenceTitle,
        // "companyId": this.state.userCompanyId
      };

      this.setState({ recordLoading: true });

      axios
        .post(path + 'conference/start-archive', dataToSend).then(serverResponse => {
          let res = serverResponse.data;
          console.log('start-archive == ', res);
          if (!res.isError) {
            that.setState({
              recordLoading: false,
              startedArchiveId: res.details.id
            }, () => {
              that.startTimer();
            });

            try {
              let data = {
                messageType: 'toast',
                messageSubType: 'started',
                message: 'This call is being recorded',
                sender: '',
                conferenceId: that.state.selectedConferenceId,
              };
              that.signalSpecificClient(null, data, 'usertouser');
            } catch (error) {
              console.log('error === === == ', error);
            }

          } else {
            that.setState({
              startedArchiveId: '',
              recordLoading: false
            });
            alert('Sorry recording can not start.');
          }
        }).catch(err => {
          alert('Sorry recording can not start.');
          this.setState({ recordLoading: false });
        });
    } else {
      alert('No conference selected.');
    }
  }

  stopCallRecording = () => {
    let that = this;
    let { selectedConference, startedArchiveId } = this.state;
    if (selectedConference && startedArchiveId) {
      let dataToSend = {
        "archiveId": startedArchiveId,
        "sessionId": selectedConference.sessionId,
        "conferenceId": selectedConference._id,
      };
      this.setState({ recordLoading: true });
      axios
        .post(path + 'conference/stop-archive', dataToSend)
        .then(serverResponse => {
          let res = serverResponse.data;
          // console.log('stop-archive == ', res);
          if (!res.isError) {
            that.stopTimer();

            that.setState({
              recordLoading: false,
              startedArchiveId: ''
            });

            try {
              let data = {
                messageType: 'toast',
                messageSubType: 'stopped',
                message: 'Recording stopped',
                sender: '',
                conferenceId: that.state.selectedConferenceId,
              };
              that.signalSpecificClient(null, data, 'usertouser');
            } catch (error) {
              console.log('error === === == ', error);
            }

          } else {
            that.setState({
              recordLoading: false
            })
            alert('can not stop');
          }
        }).catch(err => {
          that.setState({
            recordLoading: false,
            startedArchiveId: ''
          });
          alert('can not stop');
          console.log("Error for stop recording == ", err);
        });
    }
  }

  // For timer of call recording / START timer --
  startTimer = () => {
    this.child && this.child.current && this.child.current.startTimer();
  }

  // For timer of call recording / STOP timer --
  stopTimer = () => {
    this.child && this.child.current && this.child.current.stopTimer();
  }

  //#endregion Call Recording module

  //#region Invitation module
  sendInvitationCode() {
    let { selectedConference } = this.state;
    if (selectedConference && selectedConference.invitationCode) {
      this.setState({ showInvitationModal: true });
    }
  }

  hideInvitationModal() {
    this.setState({
      showInvitationModal: false,
      inviteMail: '',
      inviteCountryCode: '',
      invitePhone: '',
      invitationError: '',
      isOfflineUser: true
    });
  }

  invitationSend() {
    let { inviteMail, inviteCountryCode, invitePhone, isOfflineUser, selectedConference } = this.state;
    if (inviteMail || (inviteCountryCode && invitePhone)) {
      let url = `${cred.SITE_URL}conferencecall?invitationcode=${selectedConference.invitationCode}`;

      // send invitation
      let dataToSend = {
        "mailId": inviteMail ? inviteMail : '',
        "phoneNumber": invitePhone ? invitePhone : '',
        "countryCode": inviteCountryCode ? inviteCountryCode : '',
        "conferenceTitle": selectedConference.conferenceTitle,
        "conferenceId": selectedConference._id,
        "senderUserId": this.state.userId,
        "senderName": this.state.userName,
        "senderCompanyName": this.state.userCompanyName ? this.state.userCompanyName : this.state.selectedCompanyName,
        "senderCompanyId": this.state.userCompanyId,
        "isRegisterMandatory": this.state.selectedConference.isRegisterMandatory,
        "isOfflineUser": isOfflineUser,
        "invitationUrl": url,
        "dtmf": selectedConference.dtmf,
      };
      this.setState({ invitationLoading: true });
      axios
        .post(path + 'conference/send-conference-invitation', dataToSend)
        .then(serverResponse => {
          let res = serverResponse.data;
          if (!res.isError) {

            this.setState({
              invitationLoading: false,
              showSuccessAlert: true,
              successAlertMessage: "Invitation has been sent."
            }, () => {
              this.hideInvitationModal();
            })

          } else {

            this.setState({
              invitationLoading: false,
              showErrorAlert: true,
              errorAlertMessage: "Sorry! Invitation Can't be sent."
            }, () => {
              this.hideInvitationModal();
            })

          }
        })
    } else {
      this.setState({ invitationError: 'Please put Email or Country Code and Mobile Number' })
    }
  }

  // For showing already registered participants list 
  viewRegistered = () => {

    let dataToSend = {
      "conferenceId": this.state.selectedConference._id
    };
    // API  -- 
    axios
      .post(path + 'conference/get-register-participants', dataToSend)
      .then(serverResponse => {

        let res = serverResponse.data;
        // console.log("already registered participants == ", res);
        if (!res.isError) {

          let registeredParticipants = res.details;
          this.setState({
            registeredParticipants: registeredParticipants,
            showRegisteredList: true
          });

        } else {
          alert("Sorry! something went wrong.");
        }

      })
      .catch(error => {
        console.log(error);
      })

  }

  // For hiding registered list --
  hideRegisteredList = () => {
    this.setState({
      registeredParticipants: [],
      showRegisteredList: false
    })
  }

  //#endregion Invitatio module

  //#region for mobile version: Menu handler
  /******************************************/
  sideMenuToggle() {
    let { openSideMenu } = this.state;
    openSideMenu = !openSideMenu;
    this.setState({ openSideMenu });
  }

  mobMenuClick() {
    console.log('mobMenuClick');

    this.sideMenuToggle();
    this.subMenuMobToggle();
  }

  subMenuMobToggle() {
    let { openSubMenuMob } = this.state;
    openSubMenuMob = !openSubMenuMob;
    this.setState({ openSubMenuMob });
  }

  backMenueToggle = () => {
    this.props.history.goBack();
  }
  //#endregion mobile version: Menu handler

  //#region for mobile version: participant list
  /***********************************************/
  participantsModalOpen() {
    this.setState({ showParticipantsModal: true });
  }

  hideParticipantsModal() {
    this.setState({ showParticipantsModal: false });
  }

  //#endregion mobile version: participant list

  //#region Toast module
  /************************************/
  /**
   * @param {string} toastTitle
   * @param {string} toastMessage
   */
  displayToast = (toastTitle, toastMessage) => {
    this.setState({ showToast: true, toastTitle, toastMessage });
  }


  hideToast = () => {
    this.setState({
      showToast: false,
      toastTitle: "",
      toastMessage: ""
    });
  }

  //#endregion Toast module

  //#region Alert module
  hideSuccessAlert = () => {

    this.setState({
      showSuccessAlert: false,
      successAlertMessage: ''
    })

  }

  hideErrorAlert = () => {

    this.setState({
      showErrorAlert: false,
      errorAlertMessage: ''
    })

  }
  //#endregion Alert module

  //#region Render
  /**************************************/
  render() {
    const {
      loading,
      listLoading,
      joinLoading,
      isAllowToScreenShare,
      selecting,
      connected,
      starting,
      waiting,
      active,
      contactList,
      conferenceList,
      selectedConference,
      conferenceMember,
      selectedConferenceId,
      isFullScreen,
      isChatOpen,
      userId,
      companyType,
      messagesForHost,
      othParticipantsInModal
    } = this.state;

    const {
      localAudioClass,
      localVideoClass,
      localCallClass,
      controlClass,
      cameraPublisherClass,
      screenPublisherClass,
      cameraSubscriberClass,
      screenSubscriberClass,
      appChatOuterWrap,
    } = containerClasses(this.state);

    let totalParticipants = conferenceMember ? conferenceMember.length : 0;
    let visibleCircleNo = 2;

    let element = document.getElementById('startScreenSharing');
    element && !isAllowToScreenShare && (element.style.display = "none");

    let headers = [];
    headers = [
      { label: "Name", key: 'name' },
      { label: "Company Name", key: 'company' },
      { label: "Phone", key: 'phone' },
      { label: "Email", key: 'email' }
    ];

    const participantArray_1 = ["50", "100", "150", "200"];
    const participantArray_2 = ["100", "200", "500", "1000"];

    return <main>

      <section className="user-mngnt-wrap">
        <div className="container-fluid">
          <div className="row">
            {!isFullScreen ? <Navbar routTo="/conferencecall" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} /> : null}
            {!isFullScreen ? <div className="col-xl-2 col-lg-3 p-0 main-menu-lt-col-2">
              <div className="main-menu-lt">
                <div className="inv-code-top">
                  <h5>Invitation Code</h5>
                  <div className="inv-code-form">
                    <form>
                      <input type="text" name="" className="form-control" placeholder="Paste Code"
                        value={this.state.invitationCode}
                        onChange={(event) => this.setState({ invitationCode: event.target.value })}
                      />
                      {!joinLoading ?
                        <button className="btn"
                          ref={ref => this.fooRef = ref}
                          data-tip='Join to Conference'
                          onClick={(e) => this.joinConferenceByCode(e)}
                        >
                          <ReactTooltip
                            effect="float"
                            place="top"
                            data-border="true"
                          />
                        Join Conference
                      </button>
                        : null}
                      {joinLoading ?
                        <button className="btn"
                          ref={ref => this.fooRef = ref}
                          data-tip='Joining'
                        >
                          <ReactTooltip
                            effect="float"
                            place="top"
                            data-border="true"
                          />
                        Joining
                      </button>
                        : null}

                    </form>
                  </div>
                </div>

                <div className="inv-code-top">
                  <div className="inv-code-form">
                    {this.state.userType != 'GUEST' ?
                      <button className="btn"
                        ref={ref => this.fooRef = ref}
                        data-tip='Create Conference'
                        onClick={() => this.openCreateConferenceModal()}
                      // onFocus={() => { ReactTooltip.show(this.fooRef) }}  
                      >
                        <ReactTooltip
                          effect="float"
                          place="top"
                          data-border="true"

                        />
                        Create Conference
                      </button>
                      : null}

                  </div>
                </div>

                <div className="inv-code-top">
                  <div className="inv-code-form">
                    {this.state.userType != 'GUEST' && this.state.userType != 'COMPANY_USER' ?
                      <button className="btn"
                        ref={ref => this.fooRef = ref}
                        data-tip='Archive Lists'
                        onClick={() => this.gotoArchiveList()}
                      // onFocus={() => { ReactTooltip.show(this.fooRef) }}  
                      >
                        <ReactTooltip
                          effect="float"
                          place="top"
                          data-border="true"
                        />
                        Recordings
                      </button>
                      : null}

                  </div>
                </div>
                <h5>Ongoing Conferences</h5>
                {listLoading ?
                  <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                    <i className="fa fa-spinner fa-spin" ></i>
                  </a>
                  : null}
                <div className="lt-menu">
                  <ul>
                    {
                      conferenceList && conferenceList.length > 0 ? conferenceList.map((conference, index) => {

                        return conference.isActive ?
                          <li key={index} className={selectedConference && selectedConference._id === conference._id ? "current-menu" : ""} onClick={() => this.selectConference(conference._id)}>
                            <a>{conference.conferenceTitle}</a>

                          </li>
                          : null
                      })

                        : null
                    }
                  </ul>
                </div>

                <h5>Other Conferences</h5>
                {listLoading ?
                  <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                    <i className="fa fa-spinner fa-spin" ></i>
                  </a>
                  : null}

                <div className="lt-menu">
                  {this.state.userType != cred.USER_TYPE_ADMIN ?
                    <ul>
                      {
                        conferenceList && conferenceList.length > 0 ? conferenceList.map((conference, index) => {
                          return !conference.isActive && (conference.hostUserId == this.state.userId || conference.createdBy == this.state.userId) ?
                            <li key={index} className={selectedConference && selectedConference._id === conference._id ? "current-menu" : ""} onClick={() => this.selectConference(conference._id)}>
                              {conference.callType && conference.callType === 'LECTURE' ?
                                <a>{conference.conferenceTitle + '  (L)'}</a>
                                : <a>{conference.conferenceTitle}</a>
                              }
                            </li>
                            : null
                        })
                          :
                          null
                      }
                    </ul>
                    :
                    null}
                  {/* ADMIN */}
                  {this.state.userType == cred.USER_TYPE_ADMIN ?
                    <ul>
                      {
                        conferenceList && conferenceList.length > 0 ? conferenceList.map((conference, index) => {
                          return !conference.isActive ?
                            <li key={index} className={selectedConference && selectedConference._id === conference._id ? "current-menu" : ""} onClick={() => this.selectConference(conference._id)}>
                              {conference.callType && conference.callType === 'LECTURE' ?
                                <a>{conference.conferenceTitle + '  (L)'}</a>
                                : <a>{conference.conferenceTitle}</a>
                              }
                            </li>
                            : null
                        })
                          :
                          null
                      }
                    </ul>
                    :
                    null}
                </div>

              </div>
            </div>
              : null}

            {/* For mobile view/ Start */}
            <div id="fullconf" className={`col-lg-6 p-0 mob-full-conf ${isFullScreen && isChatOpen ? 'full-src-opn-chat col-xl-12' : isFullScreen && !isChatOpen ? 'col-xl-12' : 'col-xl-8'}`}>

              <div className="mobile-menu-header">
                <a href="JavaScript:Void(0);" className="back-arw" onClick={() => this.subMenuMobToggle()}><i className="fas fa-arrow-left"></i></a>
                {/* <a href="JavaScript:Void(0);" className="menu-tgl" onClick={() => this.sideMenuToggle()}><img src="images/menu-tgl.png" alt="" /></a> */}


                <div className="main-menu-lt conf-list-mobile" style={this.state.openSubMenuMob ? { display: 'block' } : { display: 'none' }}>
                  <a href="JavaScript:Void(0);" className="back-arw pb-2 pl-3 d-block" onClick={() => this.backMenueToggle()}><i className="fas fa-arrow-left"></i></a>
                  <div className="inv-code-top">
                    <h5>Invitation Code</h5>
                    <div className="inv-code-form">
                      <form>
                        <input type="text" name="" className="form-control" placeholder="Paste Code"
                          value={this.state.invitationCode}
                          onChange={(event) => this.setState({ invitationCode: event.target.value })}
                        />

                        <button className="btn"
                          ref={ref => this.fooRef = ref}
                          data-tip='Join to Conference'
                          onClick={(e) => this.joinConferenceByCode(e)}
                          onFocus={() => { ReactTooltip.show(this.fooRef) }} >
                          <ReactTooltip
                            effect="float"
                            place="top"
                            data-border="true"
                          />
                          Join to Conference
                        </button>

                      </form>
                    </div>
                  </div>
                  <div className="inv-code-top">
                    <div className="inv-code-form">

                      <button className="btn"
                        ref={ref => this.fooRef = ref}
                        data-tip='Create Conference'
                        onClick={() => this.openCreateConferenceModal()}
                        onFocus={() => { ReactTooltip.show(this.fooRef) }}  >
                        <ReactTooltip
                          effect="float"
                          place="top"
                          data-border="true"
                        />
                        Create Conference
                        </button>

                    </div>
                  </div>
                  <h5>Ongoing Conferences</h5>

                  <div className="lt-menu">
                    <ul>
                      {
                        conferenceList && conferenceList.length > 0 ? conferenceList.map((conference, index) => {
                          return conference.isActive ?
                            <li key={index} className={selectedConference && selectedConference._id === conference._id ? "current-menu" : ""} onClick={() => this.selectConference(conference._id)}>
                              <a>{conference.conferenceTitle}</a>
                            </li>
                            : null
                        })
                          :
                          null
                      }
                    </ul>
                  </div>

                  <h5>Others Conferences</h5>

                  <div className="lt-menu">
                    {this.state.userType != cred.USER_TYPE_ADMIN ?
                      <ul>
                        {
                          conferenceList && conferenceList.length > 0 ? conferenceList.map((conference, index) => {
                            return !conference.isActive && conference.hostUserId == this.state.userId ?
                              <li key={index} className={selectedConference && selectedConference._id === conference._id ? "current-menu" : ""} onClick={() => this.selectConference(conference._id)}>
                                {conference.callType && conference.callType === 'LECTURE' ?
                                  <a>{conference.conferenceTitle + '  ' + '(L)'}</a>
                                  : <a>{conference.conferenceTitle}</a>
                                }
                              </li>
                              : null
                          })
                            :
                            null
                        }
                      </ul>
                      :
                      null}
                    {this.state.userType == cred.USER_TYPE_ADMIN ?
                      <ul>
                        {
                          conferenceList && conferenceList.length > 0 ? conferenceList.map((conference, index) => {
                            return !conference.isActive ?
                              <li key={index} className={selectedConference && selectedConference._id === conference._id ? "current-menu" : ""} onClick={() => this.selectConference(conference._id)}>
                                {conference.callType && conference.callType === 'LECTURE' ?
                                  <a>{conference.conferenceTitle + '  ' + '(L)'}</a>
                                  : <a>{conference.conferenceTitle}</a>
                                }
                              </li>
                              : null
                          })
                            :
                            null
                        }
                      </ul>
                      :
                      null}
                  </div>

                </div>

              </div>

              <div className="confrnc-outer">
                {this.state.userType == 'ADMIN' && this.state.selectedConferenceId && connected && active && !starting ?
                  <div className="row">
                    <div className="col-md-12">
                      <div className="inv-code">
                        {this.state.userId == this.state.selectedConference.hostUserId ?
                          <Badge variant="success" style={{ cursor: "pointer" }} onClick={(e) => { this.changeHostUser(this.state.selectedConference.createdBy) }}>Give Host access</Badge>
                          : <Badge variant="danger" style={{ cursor: "pointer" }} onClick={(e) => { this.changeHostUser(this.state.userId) }}>Take Host Access</Badge>
                        }
                      </div>
                    </div>
                  </div>
                  : null
                }

                {selectedConferenceId && this.state.userId == this.state.selectedConference.hostUserId
                  || (this.state.selectedConference.mioAdmins && this.state.selectedConference.mioAdmins.includes(this.state.userId)) ? // testing whather any conference is selected or not
                  // || this.state.selectedConference.mioAdmins.includes(this.state.userId)
                  <div className="row">
                    <div className="col-md-12">
                      <div className="inv-code">
                        <span>Invitation code: </span>
                        <a href="JavaScript:void(0);" style={{ color: '#007bff' }} onClick={() => this.sendInvitationCode()}>
                          {selectedConference && selectedConference.invitationCode}
                        </a>
                        {this.state.selectedConference.hostDtmf ?
                          <>
                            &nbsp;&nbsp;|&nbsp;&nbsp;
                            <span>Host Code: </span>
                            <a href="JavaScript:void(0);" style={{ color: '#007bff' }}>
                              {selectedConference && selectedConference.hostDtmf}
                            </a>
                          </>
                          : null}
                        {selectedConference && selectedConference.callType == "LECTURE" && selectedConference.isRegisterMandatory ?
                          <>
                            &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                            <Badge variant="info" style={{ cursor: "pointer" }} onClick={() => { this.viewRegistered() }} data-tip='View registered list'>Registered Users</Badge>
                            {/* <a href="JavaScript:Void(0);" style={{ color: '#007bff' }} onClick={() => { this.viewRegistered() }} data-tip='View registered list'>
                              <i class="fa fa-eye" aria-hidden="true" />
                            </a> */}
                          </>
                          : null}
                        <ReactTooltip
                          effect="float"
                          place="top"
                          data-border="true"
                        />
                      </div>

                    </div>
                    {this.state.selectedConference.acceptInboundCall ?
                      <>
                        <div className="col-md-6">
                          <div className="inv-code">
                            <span>Phone :</span> <b>+448443260152</b><span> &nbsp;&nbsp;&nbsp; Participant Code :</span><b>{selectedConference && selectedConference.dtmf}</b>
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="inv-code">
                            <span>Inbound Call :</span> <span>&nbsp;&nbsp;&nbsp;</span>
                            {!this.state.inboundRadio ?
                              <Badge variant="success" style={{ cursor: "pointer" }} onClick={(e) => { this.openInboundCall(e) }}>Open</Badge>
                              : <Badge variant="danger" style={{ cursor: "pointer" }} onClick={(e) => { this.closeInboundCall(e) }}>Close</Badge>}
                          </div>
                        </div>
                      </>
                      :
                      null}
                  </div>
                  : null}

                <div className="confrnc-box">

                  <div className="App-main">
                    <div className="App-video-container">
                      {!connected && !selecting && connectingMask()}
                      {!connected && selecting && selectingMask()}
                      {connected && !active && !starting && startCallMask(this.prepareForStart)}
                      {!waiting && starting && loadingMask()}
                      {waiting && starting && waitingMask()}
                      <div id="cameraPublisherContainer" className={cameraPublisherClass} />
                      <div id="screenPublisherContainer" className={screenPublisherClass} />
                      <div id="cameraSubscriberContainer" className={cameraSubscriberClass} />
                      <div id="screenSubscriberContainer" className={screenSubscriberClass} />
                    </div>
                    <div id="controls" className={controlClass}>
                      <div className={localAudioClass} onClick={this.toggleLocalAudio} />
                      <div className={localVideoClass} onClick={this.toggleLocalVideo} />
                      <div className={localCallClass} onClick={this.endConferenceCall} />
                      {isFullScreen ?
                        (<div className="ots-video-control circle full-scr-close" onClick={() => this.videoFullScreenModeOff()}> <span><i className="fas fa-compress"></i> </span></div>)
                        :
                        (<div className="ots-video-control circle full-scr-open" onClick={() => this.videoFullScreenModeOn()}><span> <i className="fas fa-expand"></i></span> </div>)
                      }

                      {isChatOpen ?
                        (<div className="ots-video-control circle chat-close" onClick={() => this.chatDisplayToggle()}> <span><i className="fas fa-comment"></i> </span></div>)
                        :
                        (<div className="ots-video-control circle chat-open" onClick={() => this.chatDisplayToggle()}><span> <i className="far fa-comment"></i></span> </div>)
                      }

                    </div>

                    <div className={appChatOuterWrap}>
                      {selectedConference.callType == cred.CALL_TYPE.Lecture ?
                        <React.Fragment>

                          {/* <a href="JavaScript:Void(0);" className="back-arw d-lg-none" onClick={() => this.chatDisplayToggle()}><i className="fas fa-arrow-left"></i></a> */}
                          <div id="chat" className="App-chat-container">
                            <section>
                              <div className="ots-text-chat-container">
                                <div className="ots-text-chat">
                                  <div className="ots-messages-header ots-hidden" id="chatHeader">
                                    <span>Chat with</span>
                                  </div>
                                  <div id="otsChatWrap">
                                    {selectedConference.hostUserId == userId ?
                                      <div className="ots-messages-holder has-alert" id="messagesHolder">
                                        <div className="ots-messages-alert" id="messagesWaiting"></div>
                                        {messagesForHost && messagesForHost.map(messageObj => {
                                          return <div className="ots-message-item ots-message-sent">
                                            <div className="ots-user-name-initial"> {messageObj.initial}</div>
                                            <div className="ots-item-timestamp"> {messageObj.sender}, <span></span></div>
                                            <div className="ots-item-text">
                                              <span> {messageObj.message}</span>
                                            </div>
                                          </div>
                                        })}


                                      </div>
                                      :
                                      <div className="ots-send-message-box">
                                        <input type="text" maxlength="160" className="ots-message-input" placeholder="Enter your message here" id="messageBox" value={this.state.messageToHost} onChange={(e) => this.setState({ messageToHost: e.target.value })} />
                                        <button className="ots-icon-check" id="sendMessage" onClick={() => this.sendMessageToHost({ conferenceType: cred.CALL_TYPE.Lecture, messageType: 'faq', message: this.state.messageToHost })}></button>
                                        <div className="ots-character-count">
                                          {/* <span><span id="characterCount">0</span>/160 characters</span> */}
                                        </div>
                                      </div>
                                    }

                                  </div>
                                </div>
                              </div>
                            </section>

                          </div>
                          {/* <form>
                            <div className="chat-attach">
                               <i className="far fa-images"></i> 
                               <input name="file" type="file" multiple="" /> 
                            </div>
                          </form> */}


                        </React.Fragment>
                        :
                        <React.Fragment>
                          <a href="JavaScript:Void(0);" className="back-arw d-lg-none" onClick={() => this.chatDisplayToggle()}><i className="fas fa-arrow-left"></i></a>
                          <div id="chat" className="App-chat-container" />

                          {/* File Upload start */}
                          <form>
                            <div className="chat-attach">
                              <i className="far fa-images"></i>
                              {/* <input ref={(ref) => { this.uploadInput = ref ;}} type="file" /> */}
                              <input ref="file" name="file" type="file"
                                onChange={this.onFileChange} multiple={true}
                              // ref={ref => this.fooRef = ref}
                              // data-tip='Upload File'
                              // onFocus={() => { ReactTooltip.show(this.fooRef) }}
                              />
                              {/* <ReactTooltip
                        effect="float"
                        place="top"
                        data-border="true"
                      /> */}
                            </div>
                            {this.state.imageURL ?
                              <div className="chat-upload">
                                <button onClick={this.handleUploadImage}><i className="fas fa-upload"></i></button>
                                {/* {this.state.uploading? <i className="fa fa-spinner fa-spin spinner"></i> : <button onClick={this.handleUploadImage}><i className="fas fa-upload"></i></button>} */}
                                <div className="imgbox" onClick={this.handleUploadImage}><img src={this.state.imageURL ? this.state.imageURL : "https://www.goldensherpa.com/assets/img/default-banner.jpg"} alt="img" /></div>
                                <ProgressBar striped variant="success" now={this.state.fileUploaded} label={`${this.state.fileUploaded}%`} />
                              </div>
                              : null}
                          </form>
                          {/* File Upload end */}

                        </React.Fragment>

                      }

                    </div>
                  </div>

                  {selectedConferenceId !== '' && this.state.userType != 'GUEST' ?
                    <>
                      {/* For start and stop recording button-- */}
                      {this.state.active && this.state.userId == this.state.selectedConference.hostUserId && this.state.selectedConference.recordingPreference != "NOT_ALLOWED" ?
                        <>
                          {!this.state.startedArchiveId && !this.state.recordLoading ?
                            <div className="archive-btn"
                              z-index="10"
                            >
                              <a className="btn"
                                href="JavaScript:Void(0);"
                                onClick={() => this.startCallRecording()}
                                data-tip="Start recording"
                              >
                                <i className="fa fa-bullseye" aria-hidden="true"></i>
                                <ReactTooltip
                                  effect="float"
                                  place="top"
                                  data-border="true" />
                              </a>
                            </div>
                            :
                            null}
                          {this.state.startedArchiveId && !this.state.recordLoading ?
                            <div className="archive-btn"
                              z-index="10"
                            >
                              <a className="btn"
                                href="JavaScript:Void(0);"
                                onClick={() => this.stopCallRecording()}
                                data-tip="Stop recording"
                              >
                                <i class="fas fa-pause"></i>
                                <ReactTooltip
                                  effect="float"
                                  place="top"
                                  data-border="true" />
                              </a>
                            </div>
                            :
                            null}
                          {this.state.recordLoading ?
                            <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                              <i className="fa fa-spinner fa-spin" ></i>
                            </a>
                            : null}
                          {!this.state.recordLoading && this.state.startedArchiveId ? <Timer ref={this.child} /> : null}
                        </>
                        :
                        null}

                      {this.state.active && this.state.recording ?
                        <>
                          <div className="archive-btn"
                            z-index="10"
                          >
                            <a className="btn"
                              href="JavaScript:Void(0);"
                              data-tip="Recording"
                            >
                              <i className="fa fa-bullseye" aria-hidden="true"></i>
                              <ReactTooltip
                                effect="float"
                                place="top"
                                data-border="true" />
                            </a>
                          </div>
                        </>
                        : null}

                      <div className="add-user-btn">
                        {this.state.active && this.state.userId == this.state.selectedConference.hostUserId ?
                          <a
                            className="btn"
                            href="JavaScript:Void(0);"
                            data-tip="Phone dial out"
                            onClick={() => this.openSIPCallModal(selectedConferenceId)}
                          >

                            <i className="fas fa-phone"></i>
                            <ReactTooltip
                              effect="float"
                              place="top"
                              data-border="true" />
                          </a>
                          : null}
                        {!this.state.active && this.state.userId == this.state.selectedConference.hostUserId ?
                          <a
                            className="btn"
                            href="JavaScript:Void(0);"
                            // ref={ref => this.fooRef = ref}
                            data-tip="Add Member"
                            onClick={() => this.openAddMemberModal(selectedConferenceId)}
                          // onFocus={() => { ReactTooltip.show(this.fooRef) }}  
                          >
                            <i className="fas fa-user-plus"></i>
                            <ReactTooltip
                              effect="float"
                              place="top"
                              data-border="true" />
                          </a>
                          : null}
                        {!this.state.active && this.state.userId == this.state.selectedConference.hostUserId ?
                          <a
                            href="JavaScript:Void(0);"
                            className="btn red"
                            // ref={ref => this.fooRef = ref}
                            data-tip="Delete Conference"
                            // onFocus={() => { ReactTooltip.show(this.fooRef) }}
                            onClick={() => this.openDeleteModal(selectedConferenceId)} >
                            <i className="fas fa-trash-alt"></i>
                            <ReactTooltip
                              effect="float"
                              place="top"
                              data-border="true" />
                          </a>
                          : null}
                        {this.state.active && this.state.userId != this.state.selectedConference.hostUserId && !this.state.selectedConference.allowParticipantToShareScreen ?

                          <a className="btn"
                            href="JavaScript:Void(0);"
                            // ref={ref => this.archiveEnRef = ref}
                            data-tip="Send request to share screen"
                            onClick={() => this.sendRequestToShareScreen()}
                          // onFocus={() => { ReactTooltip.show(this.archiveEnRef) }} 
                          >
                            <i className="fas fa-archive"></i>
                            <ReactTooltip
                              effect="float"
                              place="top"
                              data-border="true" />
                          </a>

                          :
                          null}
                      </div>
                    </>

                    : null}

                  {
                    active && conferenceMember.length > 0 ?

                      <div className="all-users-list" >
                        {this.state.selectedConference.callType != 'LECTURE' ?
                          <React.Fragment>
                            {this.state.userId == this.state.selectedConference.hostUserId ?
                              <ul>
                                {/* <li>
                                      <div className="thumb-main">
                                        <div className="userimg" style={{ cursor: "pointer" }}
                                          onClick={() => { this.participantsModalOpen() }}
                                        >
                                          {'+' + parseInt(parseInt(conferenceMember.length) - parseInt(visibleCircleNo))}
                                        </div>
                                      </div>
                                    </li> */}
                                <li>
                                  <div className="thumb-main">
                                    <div className="userimg" style={{ cursor: "pointer" }}
                                      onClick={() => { this.participantsModalOpen() }}
                                    >
                                      {parseInt(this.state.currentStream.length) + parseInt(this.state.inboundUser.length) - 1}
                                    </div>
                                  </div>
                                </li>
                              </ul>
                              : this.state.userId != this.state.selectedConference.hostUserId ?
                                <ul>
                                  {/* <li>
                                        <div className="thumb-main">
                                          <div className="userimg" style={{ cursor: "pointer" }}>
                                            {'+' + parseInt(parseInt(conferenceMember.length) - parseInt(visibleCircleNo))}
                                          </div>
                                        </div>
                                      </li> */}
                                  <li>
                                    <div className="thumb-main">
                                      <div className="userimg" style={{ cursor: "pointer" }}>
                                        {parseInt(this.state.currentStream.length) + parseInt(this.state.inboundUser.length) - 1}
                                      </div>
                                    </div>
                                  </li>
                                </ul>
                                :
                                null}
                          </React.Fragment>
                          : this.state.selectedConference.callType == 'LECTURE' ?
                            <React.Fragment>
                              {this.state.userId == this.state.selectedConference.hostUserId ?
                                <ul>
                                  <li>
                                    <div className="thumb-main">
                                      <div className="userimg" style={{ cursor: "pointer" }}
                                        onClick={() => { this.participantsModalOpen() }}
                                      >
                                        {parseInt(this.state.conferenceMember.length) + parseInt(this.state.inboundUser.length)}
                                      </div>
                                    </div>
                                  </li>
                                </ul>
                                : null}
                            </React.Fragment>
                            : null}
                        <>
                          {/* {Number(conferenceMember.length) <= visibleCircleNo ? conferenceMember.map((member, index) => {
                          // index < visibleCircleNo ?

                          var memberId = member.userId;
                          var contactNumber = '' + member.countryCode + member.contactNumber; // from db

                          // whether he belong from sip, check from db.
                          let isSipMember = member.userType === 'DIALEDUSER';

                          // from OT active stream.
                          let currentStream = this.state.currentStream;

                          // whethre this SIP member is in ongoing stream.
                          let sipCallArray = isSipMember ? _.filter(currentStream, { 'phnNo': contactNumber }) : [];
                          let isInSipStream = sipCallArray.length > 0 && sipCallArray;

                          let activeArray = _.filter(currentStream, { 'userId': memberId });
                          let isActive = activeArray.length > 0 && activeArray;

                          let muteStatus = isActive && activeArray[0].hasAudio ? true : false;

                          // var visibleCircleNo = 2; // how many circle will be shown in conference screen / after then it'll be shown as '+N'

                          return (memberId !== this.state.userId && this.state.userId == this.state.selectedConference.hostUserId) ?

                            <ul key={index}>

                              <li>

                                <div className="thumb-main">
                                  <div className="userimg">

                                    {memberId && isActive ? <span className="active-call-cirlce"></span> : <span></span>}
                                    {memberId && this.state.speakers && this.state.speakers[memberId] ? <span className="speaking"><i className="fas fa-microphone"></i></span> : <span><i></i></span>}
                                    {memberId && Object.keys(this.state.currentScreen).length > 0 && memberId == this.state.currentScreen.screenUserId ?
                                      <span className="ic-share"><i className="fas fa-desktop"></i></span>
                                      : null}

                                    {(!memberId && isInSipStream && member.state == 'ACTIVE' && member.event == "callUpdated") ? <span className="active-call-cirlce" ></span>
                                      : (!memberId && isInSipStream && member.state != 'NA' && member.state != 'ACTIVE' && member.event == "callCreated" || member.event == "callUpdated") ? <span className="active-call-cirlce2" ></span>
                                        : (!memberId && !isInSipStream) ? <span></span>
                                          : <span></span>
                                    }

                                    {(!memberId && this.state.speakers && this.state.speakers[contactNumber]) ? <span className="speaking"><i className="fas fa-microphone"></i></span> : <span><i></i></span>}

                                    {member && member.name ? member.name.slice(0, 2) : <span><i className="far fa-user"></i></span>}


                                  </div>

                                  <div className="clients-name"><span>{member.name ? member.name : "Participant"}</span></div>

                                  {this.state.userId == this.state.selectedConference.hostUserId ?
                                    <div className="users-info">

                                      {memberId ?
                                        <ul>
                                          {isActive ? <li><span style={{ color: "green" }}>Online</span></li> : null}
                                          <li><span>{this.state.speakers && this.state.speakers[memberId] ? "Speaking" : null}</span></li>
                                          <li><span>{member.contactNumber ? contactNumber : member.userType}</span></li>


                                          {isActive ?
                                            <li>
                                              <span style={{ color: "green", cursor: "pointer" }} onClick={() => { this.toggleRemortAudio(memberId) }}>{muteStatus ? "Mute" : "Unmute"}</span>
                                            </li>
                                            : null}

                                        </ul>
                                        : <ul>

                                          {isInSipStream ? <li><span style={{ color: "green" }}>Online</span></li> : null}
                                          <li><span>{this.state.speakers && this.state.speakers[contactNumber] ? "Speaking" : null}</span></li>
                                          <li><span>{member.contactNumber ? contactNumber : member.userType}</span></li>
                                          <li><span>{member.state && member.state != 'NA' && member.state != 'ACTIVE' && member.event == "callUpdated" ?
                                            member.state
                                            : member.state && member.state != 'NA' && member.state != 'ACTIVE' && member.event == "callCreated" ?
                                              "Connecting.."
                                              : member.state && member.state != 'NA' && member.state != 'ACTIVE' && member.event == "callDestroyed" ?
                                                null
                                                : null}
                                          </span>
                                          </li>

                                          <li>
                                            {isInSipStream ?
                                              <button onClick={() => this.endSIPCall(member.countryCode, member.contactNumber)}>End Call</button>
                                              :
                                              <button onClick={() => this.sipCallFromConference(member.countryCode, member.contactNumber)}>Start Call</button>
                                            }
                                          </li>
                                          <li>
                                            {!isInSipStream ?
                                              <button onClick={() => this.removeSIPUser(contactNumber, member._id)}>Remove</button>
                                              : null}
                                          </li>

                                        </ul>
                                      }

                                    </div>
                                    : null}

                                </div>

                              </li>

                            </ul>

                            // For the users are not host -- // only the online users will be visible
                            : (memberId !== this.state.userId && this.state.userId != this.state.selectedConference.hostUserId && isActive || isInSipStream) ?
                              // <ul> will be repleated....
                              <ul key={index}>

                                <li>

                                  <div className="thumb-main">
                                    <div className="userimg">

                                      {memberId && isActive ? <span className="active-call-cirlce"></span> : <span></span>}
                                      {memberId && this.state.speakers && this.state.speakers[memberId] ? <span className="speaking"><i className="fas fa-microphone"></i></span> : <span><i></i></span>}
                                      {memberId && Object.keys(this.state.currentScreen).length > 0 && memberId == this.state.currentScreen.screenUserId ?
                                        <span className="ic-share"><i className="fas fa-desktop"></i></span>
                                        : null}

                                      {(!memberId && isInSipStream && member.state == 'ACTIVE' && member.event == "callUpdated") ? <span className="active-call-cirlce" ></span>
                                        : (!memberId && isInSipStream && member.state != 'NA' && member.state != 'ACTIVE' && member.event == "callCreated" || member.event == "callUpdated") ? <span className="active-call-cirlce2" ></span>
                                          : (!memberId && !isInSipStream) ? <span></span>
                                            : <span></span>
                                      }
                                      {!memberId && this.state.speakers && this.state.speakers[contactNumber] ? <span className="speaking"><i className="fas fa-microphone"></i></span> : <span><i></i></span>}

                                      {member && member.name ?
                                        member.name.slice(0, 2)
                                        : <span><i className="far fa-user"></i></span>}


                                    </div>

                                    <div className="clients-name"><span>{member.name ? member.name : "Participant"}</span></div>

                                  </div>

                                </li>

                              </ul>
                              : null
                          // :
                          // null
                        })
                          : Number(conferenceMember.length) > visibleCircleNo ?
                            <React.Fragment>
                              {conferenceMember.slice(0, visibleCircleNo).map((member, index) => {
                                var memberId = member.userId;
                                var contactNumber = '' + member.countryCode + member.contactNumber; // from db

                                // whether he belong from sip, check from db.
                                let isSipMember = member.userType === 'DIALEDUSER';

                                // from OT active stream.
                                let currentStream = this.state.currentStream;

                                // whethre this SIP member is in ongoing stream.
                                let sipCallArray = isSipMember ? _.filter(currentStream, { 'phnNo': contactNumber }) : [];
                                let isInSipStream = sipCallArray.length > 0 && sipCallArray;

                                let activeArray = _.filter(currentStream, { 'userId': memberId });
                                let isActive = activeArray.length > 0 && activeArray;

                                let muteStatus = (isActive && activeArray[0].hasAudio);

                                // var visibleCircleNo = 2; // how many circle will be shown in conference screen / after then it'll be shown as '+N'

                                return (memberId !== this.state.userId && this.state.userId == this.state.selectedConference.hostUserId) ?
                                  <React.Fragment>
                                    <ul key={index}>

                                      <li>

                                        <div className="thumb-main">
                                          <div className="userimg">

                                            {memberId && isActive ? <span className="active-call-cirlce"></span> : <span></span>}
                                            {memberId && this.state.speakers && this.state.speakers[memberId] ? <span className="speaking"><i className="fas fa-microphone"></i></span> : <span><i></i></span>}
                                            {memberId && Object.keys(this.state.currentScreen).length > 0 && memberId == this.state.currentScreen.screenUserId ?
                                              <span className="ic-share"><i className="fas fa-desktop"></i></span>
                                              : null}
                                            
                                            {(!memberId && isInSipStream && member.state == 'ACTIVE' && member.event == "callUpdated") ? <span className="active-call-cirlce" ></span>
                                              : (!memberId && isInSipStream && member.state != 'NA' && member.state != 'ACTIVE' && member.event == "callCreated" || member.event == "callUpdated") ? <span className="active-call-cirlce2" ></span>
                                                : (!memberId && !isInSipStream) ? <span></span>
                                                  : <span></span>
                                            }

                                            {(!memberId && this.state.speakers && this.state.speakers[contactNumber]) ? <span className="speaking"><i className="fas fa-microphone"></i></span> : <span><i></i></span>}
                                           


                                            {member && member.name ? member.name.slice(0, 2) : <span><i className="far fa-user"></i></span>}


                                          </div>

                                          <div className="clients-name"><span>{member.name ? member.name : "Participant"}</span></div>

                                          {this.state.userId == this.state.selectedConference.hostUserId ?
                                            <div className="users-info">

                                              {memberId ?
                                                <ul>
                                                  {isActive ? <li><span style={{ color: "green" }}>Online</span></li> : null}
                                                  <li><span>{this.state.speakers && this.state.speakers[memberId] ? "Speaking" : null}</span></li>
                                                  <li><span>{member.contactNumber ? contactNumber : member.userType}</span></li>

                                                  {isActive ?
                                                    <li>
                                                      <span style={{ color: "green", cursor: "pointer" }} onClick={() => { this.toggleRemortAudio(memberId) }}>{muteStatus ? "Mute" : "Unmute"}</span>
                                                    </li>
                                                    : null}

                                                </ul>
                                                : <ul>

                                                  {isInSipStream ? <li><span style={{ color: "green" }}>Online</span></li> : null}
                                                  <li><span>{this.state.speakers && this.state.speakers[contactNumber] ? "Speaking" : null}</span></li>
                                                  <li><span>{member.contactNumber ? contactNumber : member.userType}</span></li>
                                                  <li><span>{member.state && member.state != 'NA' && member.state != 'ACTIVE' && member.event == "callUpdated" ?
                                                    member.state
                                                    : member.state && member.state != 'NA' && member.state != 'ACTIVE' && member.event == "callCreated" ?
                                                      "Connecting.."
                                                      : member.state && member.state != 'NA' && member.state != 'ACTIVE' && member.event == "callDestroyed" ?
                                                        null
                                                        : null}
                                                  </span>
                                                  </li>

                                                  <li>
                                                    {isInSipStream ?
                                                      <button onClick={() => this.endSIPCall(member.countryCode, member.contactNumber)}>End Call</button>
                                                      :
                                                      <button onClick={() => this.sipCallFromConference(member.countryCode, member.contactNumber)}>Start Call</button>
                                                    }
                                                  </li>
                                                  <li>
                                                    {!isInSipStream ?
                                                      <button onClick={() => this.removeSIPUser(contactNumber, member._id)}>Remove</button>
                                                      : null}
                                                  </li>

                                                </ul>
                                              }

                                            </div>
                                            : null}

                                        </div>

                                      </li>

                                    </ul>

                                  </React.Fragment>
                                  // For the users are not host -- // only the online users will be visible
                                  : (memberId !== this.state.userId && this.state.userId != this.state.selectedConference.hostUserId && isActive || isInSipStream) ?
                                    // <ul> will be repleated....
                                    <React.Fragment>
                                      <ul key={index}>

                                        <li>

                                          <div className="thumb-main">
                                            <div className="userimg">

                                              {memberId && isActive ? <span className="active-call-cirlce"></span> : <span></span>}
                                              {memberId && this.state.speakers && this.state.speakers[memberId] ? <span className="speaking"><i className="fas fa-microphone"></i></span> : <span><i></i></span>}
                                              {memberId && Object.keys(this.state.currentScreen).length > 0 && memberId == this.state.currentScreen.screenUserId ?
                                                <span className="ic-share"><i className="fas fa-desktop"></i></span>
                                                : null}
                                              

                                              {(!memberId && isInSipStream && member.state == 'ACTIVE' && member.event == "callUpdated") ? <span className="active-call-cirlce" ></span>
                                                : (!memberId && isInSipStream && member.state != 'NA' && member.state != 'ACTIVE' && member.event == "callCreated" || member.event == "callUpdated") ? <span className="active-call-cirlce2" ></span>
                                                  : (!memberId && !isInSipStream) ? <span></span>
                                                    : <span></span>
                                              }
                                              {!memberId && this.state.speakers && this.state.speakers[contactNumber] ? <span className="speaking"><i className="fas fa-microphone"></i></span> : <span><i></i></span>}
                                              

                                              {member && member.name ?
                                                member.name.slice(0, 2)
                                                : <span><i className="far fa-user"></i></span>}

                                            </div>

                                            <div className="clients-name"><span>{member.name ? member.name : "Participant"}</span></div>

                                          </div>

                                        </li>

                                      </ul>

                                    </React.Fragment>
                                    : null


                              })}

                            </React.Fragment>
                            :
                            null} */}
                        </>
                      </div>
                      : null
                  }

                  {/* hide untill conference start */}
                  {this.state.selectedConferenceId && this.state.active ?
                    <span onClick={() => this.participantsModalOpen()} className="mob-user-list-ic"><i className="fas fa-ellipsis-h"></i></span>
                    : null}

                </div>

              </div>

            </div>
            {/* For mobile view/ Start */}

          </div>
        </div>

      </section>

      {/* For Mobile version--  Participants list*/}
      <Modal size={"lg"} show={this.state.showParticipantsModal} onHide={() => this.hideParticipantsModal()}>
        <Modal.Header closeButton>
          <Modal.Title>All Participants</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="mob-partcpt-list">
            {
              active && conferenceMember.length > 0 && <div className="all-users-list" >
                {conferenceMember.map((member, index) => {
                  var memberId = member.userId;
                  var contactNumber = '' + member.countryCode + member.contactNumber; // from db

                  // whether he belong from sip, check from db.
                  let isSipMember = member.userType === 'DIALEDUSER';

                  // from OT active stream.
                  let currentStream = this.state.currentStream;

                  // whethre this SIP member is in ongoing stream.
                  let sipCallArray = isSipMember ? _.filter(currentStream, { 'phnNo': contactNumber }) : [];
                  let isInSipStream = sipCallArray && sipCallArray.length > 0;

                  let activeArray = _.filter(currentStream, { 'userId': memberId });
                  let isActive = activeArray && activeArray.length > 0;

                  let muteStatus = (isActive && activeArray[0].hasAudio);

                  // console.log("isSipMember,isInSipStream ==== ", isSipMember, isInSipStream);
                  return memberId !== this.state.userId ? <ul key={index}>
                    <li>

                      <div className="thumb-main">
                        <div className="userimg">

                          {memberId && isActive ? <span className="active-call-cirlce"></span> : <span></span>}
                          {memberId && this.state.speakers && this.state.speakers[memberId] ? <span className="speaking"><i className="fas fa-microphone"></i></span> : <span><i></i></span>}
                          {memberId && Object.keys(this.state.currentScreen).length > 0 && memberId == this.state.currentScreen.screenUserId ?
                            <span className="ic-share"><i className="fas fa-desktop"></i></span>
                            : <span><i></i></span>}
                          {/* For SIP User */}
                          {!memberId && isInSipStream ? <span className="active-call-cirlce"></span> : null}
                          {!memberId && !isInSipStream ? <span></span> : null}
                          {!memberId && this.state.speakers && this.state.speakers[contactNumber] ? <span className="speaking"><i className="fas fa-microphone"></i></span> : <span><i></i></span>}
                          {/* For SIP User */}

                          {/* <i className="fas fa-microphone-slash"></i> */}

                          {member.name ?
                            member.name.slice(0, 2)
                            : <i className="far fa-user"></i>}

                          {/* {this.state.actionObserver[memberId] ?
                            !this.state.actionObserver[memberId].hasAudio ?
                              <span className="vol-up-hvr" ><button className="fas fa-microphone" ><i></i></button></span>
                              : <span className="vol-off-hvr" ><button className="fas fa-microphone-slash"> <i></i></button></span>
                            : null} */}
                        </div>

                        <div className="clients-name"><span>{member.name ? member.name : "Participant"}{/*<small>Presenter</small>*/}</span></div>

                        {this.state.userId == this.state.selectedConference.hostUserId ?
                          <div className="users-info">

                            {memberId ?
                              <ul>
                                {isActive ? <li><span style={{ color: "green" }}>Online</span></li> : null}
                                <li><span>{this.state.speakers && this.state.speakers[memberId] ? "Speaking" : null}</span></li>
                                <li><span>{member.contactNumber ? contactNumber : member.userType}</span></li>

                                {/* {
                                      !(this.state.actionObserver[memberId] && this.state.actionObserver[memberId].hasAudio === true) ?
                                        <li className="vol-up-hvr" onClick={() => this.muteMember(member.name, member.userId)}><button className="fas fa-microphone-slash" ><i></i></button></li>
                                        : <li className="vol-off-hvr" onClick={() => this.unmuteMember(member.name, member.userId)}><button className="fas fa-microphone"><i></i> </button></li>
                                    } */}

                                {isActive ?
                                  <li>
                                    <span style={{ color: "green", cursor: "pointer" }} onClick={() => { this.toggleRemortAudio(memberId) }}>{muteStatus ? "Mute" : "Unmute"}</span>
                                  </li>
                                  : null}

                              </ul>
                              : <ul>

                                {isInSipStream ? <li><span style={{ color: "green" }}>Online</span></li> : null}
                                <li><span>{this.state.speakers && this.state.speakers[contactNumber] ? "Speaking" : null}</span></li>
                                <li><span>{member.contactNumber ? contactNumber : member.userType}</span></li>
                                <li><span>{member.state && member.state != 'NA' && member.state != 'ACTIVE' && member.event == "callUpdated" ?
                                  member.state
                                  : member.state && member.state != 'NA' && member.state != 'ACTIVE' && member.event == "callCreated" ?
                                    "Connecting.."
                                    : member.state && member.state != 'NA' && member.state != 'ACTIVE' && member.event == "callDestroyed" ?
                                      null
                                      : null}
                                </span>
                                </li>
                                <li>
                                  {isInSipStream ?
                                    <button onClick={() => this.endSIPCall(member.countryCode, member.contactNumber)}>End Call</button>
                                    :
                                    <button onClick={() => this.sipCallFromConference(member.countryCode, member.contactNumber, 'modal')}>Start Call</button>
                                  }

                                </li>
                                <li>
                                  {!isInSipStream ?
                                    <button onClick={() => this.removeSIPUserFromModal(member.contactNumber, member._id)}>Remove</button>
                                    : null}
                                </li>
                              </ul>
                            }

                          </div>
                          : null}

                      </div>

                    </li>
                  </ul>
                    : null

                })
                }

              </div>
            }

            {/* For showing list of inbound users */}
            {this.state.inboundUser && <div className="all-users-list" >
              {this.state.inboundUser.map((user, index) => {

                return <ul key={index}>
                  <li>
                    <div className="thumb-main">
                      <div className="userimg">
                        <i className="fas fa-phone"></i>
                        {user.dtmf ?
                          user.raiseHandReq == 'accepted' ?
                            <i className="fas fa-people-arrows" style={{ cursor: "pointer" }} onClick={() => this.closeRaiseHandReq(user.uuid)}></i>
                            : <i className="far fa-hand-paper" style={{ cursor: "pointer" }} onClick={() => this.acceptRaiseHandReq(user.uuid)}></i>
                          : <span></span>
                        }
                      </div>
                      <div className="clients-name"><span><span>{user.name ? user.name : 'Inbound User'}</span></span></div>
                      <div className="users-info">
                        <ul>
                          <li><span>{user.number}</span></li>
                          <li>
                            <span style={{ color: "green", cursor: "pointer" }} onClick={() => { this.toggleRemortAudioForInbound(user.uuid) }}>{!user.muteStatus ? "Mute" : "Unmute"}</span>
                          </li>
                          <li>
                            <button onClick={() => this.endInboundUserCall(user.uuid)}>End Call</button>
                          </li>
                        </ul>
                      </div>

                    </div>
                  </li>
                </ul>

              })}
            </div>
            }

          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="primary" onClick={() => this.hideParticipantsModal()}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>


      <Modal size="lg" show={this.state.showCreateConference} onHide={() => this.hideCreateConferenceModal()}>
        <Modal.Header closeButton>
          <Modal.Title>Create New Conference</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="row">

            <div className="col-md-12">
              <div className="form-group">
                <label>Title:</label>
                <input
                  type="text"
                  name="usrFname"
                  value={this.state.conferenceTitle}
                  className="form-control"
                  placeholder="Title"
                  onChange={event => this.setState({ conferenceTitle: event.target.value })}
                />
                <span style={{ color: "red" }}>{this.state.conferenceTitleError ? `* ${this.state.conferenceTitleError}` : ''}</span>
              </div>
            </div>

            <div className="col-md-6">
              <div className="form-group">
                <label>Mode:</label>
                <select className="form-control" value={this.state.conferenceMode} onChange={(event) => this.setState({ conferenceMode: event.target.value })} >
                  <option value="">Select conference mode</option>
                  <option value="CONFERENCE">Normal</option>
                  <option value="LECTURE">Lecture</option>
                </select>
                <span style={{ color: "red" }}>{this.state.conferenceModeError ? `* ${this.state.conferenceModeError}` : ''}</span>
              </div>
            </div>

            <div className="col-md-6">
              <div className="form-group">
                <label>Host join:</label>
                <select className="form-control" value={this.state.hostJoinBy}
                  onChange={(event) =>
                    // this.setState({ hostJoinBy: event.target.value });
                    event.target.value == "OFFLINE_OR_ONLINE" ?
                      this.setState({ hostJoinBy: event.target.value, inboundCall: true })
                      :
                      this.setState({ hostJoinBy: event.target.value, inboundCall: false })
                  }
                >
                  <option value=""> Select Host join </option>
                  <option value="ONLINE">Online</option>
                  <option value="OFFLINE_OR_ONLINE">Offline Or Online</option>
                </select>
                <span style={{ color: "red" }}>{this.state.hostJoinByError ? `* ${this.state.hostJoinByError}` : ''}</span>
              </div>
            </div>

            <div className="col-md-6">
              <div className="form-group">
                <label>Recording preference:</label>
                <select className="form-control" value={this.state.recordingPreference} onChange={(event) => this.setState({ recordingPreference: event.target.value })} >
                  <option value="">Select an option</option>
                  <option value="AUTOMATICALLY_INITIATED">Automatically initiated</option>
                  <option value="AVAILABLE_TO_ORGANISER">Available to organiser</option>
                  <option value="NOT_ALLOWED">Not Allowed</option>
                </select>
                <span style={{ color: "red" }}>{this.state.recordingPreferenceError ? `* ${this.state.recordingPreferenceError}` : ''}</span>
              </div>
            </div>

            <div className="col-md-6">
              <div className="form-group">
                <label>No of participants:</label>
                <select className="form-control" value={this.state.maxNoOfParticipants} onChange={(event) => this.setState({ maxNoOfParticipants: event.target.value })} >
                  <option value="1">Select max. limit</option>
                  {this.state.conferenceMode == "CONFERENCE" ?
                    (participantArray_1.map((value, index) => {
                      return <option value={value}>{value}</option>
                    }))
                    : this.state.conferenceMode == "LECTURE" ?
                      (participantArray_2.map((value, index) => {
                        return <option value={value}>{value}</option>
                      }))
                      : null}
                </select>
                <span style={{ color: "red" }}>{this.state.maxNoOfParticipantsError ? `* ${this.state.maxNoOfParticipantsError}` : ''}</span>
              </div>
            </div>

            <div className="col-md-6">
              <div className="form-group">
                <label>Time Out(Hours):</label>
                <select className="form-control" value={this.state.timeOutHrs} onChange={(event) => this.setState({ timeOutHrs: event.target.value })} >
                  <option value="0">Select hours</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>
                  <option value="10">10</option>
                  <option value="11">11</option>
                  <option value="12">12</option>
                  <option value="-1">No time out</option>
                </select>
                <span style={{ color: "red" }}>{this.state.timeOutHrsError ? `* ${this.state.timeOutHrsError}` : ''}</span>
              </div>
            </div>

            <div className="col-md-6">
              <div className="form-group">
                <label>Time out(Minutes):</label>
                <select className="form-control" value={this.state.timeOutMins} onChange={(event) => this.setState({ timeOutMins: event.target.value })} >
                  <option value="00">Select mins</option>
                  <option value="10">10</option>
                  <option value="20">20</option>
                  <option value="30">30</option>
                  <option value="40">40</option>
                  <option value="50">50</option>
                  <option value="-1">No time out</option>
                </select>
                <span style={{ color: "red" }}>{this.state.timeOutMinsError ? `* ${this.state.timeOutMinsError}` : ''}</span>
              </div>
            </div>

            <div className="col-md-6">
              <div className="form-group">
                <label>Valid Upto:</label>
                {/* <select className="form-control" value={this.state.durationHrs} onChange={(event) => this.setState({ durationHrs: event.target.value })} >
                  <option value="0">Select hours</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>
                  <option value="10">10</option>
                  <option value="11">11</option>
                  <option value="12">12</option>
                  <option value="-1">Unlimited</option>
                </select> */}
                <input
                  type="date"
                  className="form-control"
                  name="validUpto" value={this.state.validUpto}
                  onChange={(event) => { this.setState({ validUpto: event.target.value }) }}
                />
                {/* <span style={{ color: "red" }}>{this.state.durationHrsError ? `* ${this.state.durationHrsError}` : ''}</span> */}
              </div>
            </div>

            <div className="col-md-6"></div>

            <div className="col-md-3">
              <div className="form-group">
                <label>File Share:</label>
              </div>
            </div>

            <div className="col-md-3">
              <div className="form-group">
                <input
                  type="checkbox"
                  checked={this.state.fileShare}
                  onChange={(e) => this.setState({ fileShare: !this.state.fileShare })}
                />
              </div>
            </div>

            <div className="col-md-3">
              <div className="form-group">
                <label>Accept Inbound Call:</label>
              </div>
            </div>

            <div className="col-md-3">
              <div className="form-group">
                <input
                  type="checkbox"
                  checked={this.state.inboundCall}
                  onChange={(e) => this.setState({ inboundCall: !this.state.inboundCall })}
                  disabled={this.state.hostJoinBy == "OFFLINE_OR_ONLINE" ? true : false}
                />
              </div>
            </div>

            {this.state.conferenceMode == 'LECTURE' ?
              <>
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Is register mandatory?:</label>
                  </div>
                </div>

                <div className="col-md-1">
                  <div className="form-group">
                    <label>Yes:</label>
                  </div>
                </div>
                <div className="col-md-2">
                  <div className="form-group">
                    <input
                      type="checkbox"
                      checked={this.state.registerMandatory}
                      onChange={(e) => this.setState({ registerMandatory: !this.state.registerMandatory })}
                    // disabled={this.state.conferenceMode == 'LECTURE' ? true : false}
                    />
                  </div>
                </div>

                <div className="col-md-1">
                  <div className="form-group">
                    <label>No:</label>
                  </div>
                </div>
                <div className="col-md-2">
                  <div className="form-group">
                    <input
                      type="checkbox"
                      checked={!this.state.registerMandatory}
                      onChange={(e) => this.setState({ registerMandatory: !this.state.registerMandatory })}
                    // disabled={this.state.conferenceMode == 'LECTURE' ? true : false}
                    />
                  </div>
                </div>
              </>
              :
              null}


            <div className="col-md-12">
              <div className="form-group">
                <label>Allow participants to share screen:</label>
              </div>
            </div>

            <div className="col-md-3">
              <div className="form-group">
                <label>Without organizer's Permission:</label>
              </div>
            </div>
            <div className="col-md-3">
              <div className="form-group">
                <input
                  type="checkbox"
                  checked={this.state.screenWithoutPermission}
                  onChange={(e) => this.setState({ screenWithoutPermission: !this.state.screenWithoutPermission })}
                />
              </div>
            </div>

            <div className="col-md-3">
              <div className="form-group">
                <label>With organizer's Permission:</label>
              </div>
            </div>
            <div className="col-md-3">
              <div className="form-group">
                <input
                  type="checkbox"
                  checked={!this.state.screenWithoutPermission}
                  onChange={(e) => this.setState({ screenWithoutPermission: !this.state.screenWithoutPermission })}
                />
              </div>
            </div>

            <div className="col-md-12">
              <div className="form-group" style={{ padding: "5px" }}>
                {contactList && contactList.length > 0 && contactList.map((address, index) => {
                  // return address.isAdded ? <tr style= {{padding: "5px"}}><td style={{color: "#0083C6", padding: "5px"}} key={index}>{address.name}</td></tr> : null
                  return address.isAdded ?
                    <Badge pill variant="success" style={{ fontSize: "12px", margin: "3px" }} key={index}>{address.name}</Badge>
                    : null
                })
                }
              </div>
            </div>

            <table style={{ marginLeft: '20px' }}>
              <tbody>
                <tr style={{ marginTop: '10px' }}>
                  <td>
                    <Button variant="primary" onClick={() => this.openAddressBookModal()}>
                      Add/Remove Participant
                    </Button>
                    {/* <h6 variant="primary" onClick={() => this.openAddressBookModal()}> Add/Remove Participant </h6> */}
                  </td>
                </tr>
              </tbody>
            </table>

          </div>
        </Modal.Body>
        <Modal.Footer>
          {this.state.loading ?
            <Button variant="secondary">
              <i className="fas fa-spinner fa-spin"></i>
            </Button>
            :
            null}
          {!this.state.loading ?
            <Button variant="primary" onClick={() => this.createConference()}>
              Save
            </Button>
            :
            null}
        </Modal.Footer>
      </Modal>

      {/* Address book modal --START */}
      <Modal size={"lg"} show={this.state.showAddressBook} onHide={() => this.hideAddressBookModal()}>
        <Modal.Header closeButton>
          <Modal.Title>Address book</Modal.Title>
        </Modal.Header>
        <Modal.Body style={{ minHeight: '400px' }}>
          <div className="mng-full-table modal-adrs-book">
            <div className="row">
              <div className="col-md-12">
                <div className="mng-full-table-hdr">
                  <div className="row">
                    <div className="col-md-2 border-rt">
                      <h6>Add/Remove</h6>
                    </div>
                    <div className="col-md-4 border-rt">
                      <h6>Contact Name</h6>
                    </div>
                    <div className="col-md-3 border-rt">
                      <h6>User Type</h6>
                    </div>
                    <div className="col-md-3">
                      <h6>Phone Number</h6>
                    </div>
                  </div>
                </div>
              </div>

            </div>
            {contactList && contactList.length > 0 && contactList.map((address, index) => {
              return <div className="row" key={index}>
                <div className="col-md-12">
                  <div className="mng-full-table-row">
                    <div className="row">
                      <div className="col-md-2 border-rt">
                        <h6>Add</h6>
                        <input type="checkbox" value={address.isAdded} defaultChecked={address.isAdded} onChange={(event) => this.addOrRemoveToConference(address._id)} />
                      </div>
                      <div className="col-md-4 border-rt">
                        <h6>Contact Name</h6>
                        <p><a href="#">{address.name}</a></p>
                      </div>
                      <div className="col-md-3 border-rt">
                        <h6>User Type</h6>
                        <p>{address.userType}</p>
                      </div>
                      <div className="col-md-3">
                        <h6>Phone Number</h6>
                        <p>{address.phoneNumber}</p>
                      </div>
                      {/* <div className="mobile-ad-edt-btns">
                        <ul>
                          <li><a href="#"><i className="fas fa-eye"></i></a></li>
                          <li><a href="JavaScript:Void(0);" data-toggle="modal" data-target="#delModal"><i className="far fa-trash-alt"></i></a></li>
                        </ul>
                      </div> */}
                    </div>
                  </div>

                </div>
              </div>
            })
            }
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="primary" onClick={() => this.hideAddressBookModal()}> Save And Close </Button>
        </Modal.Footer>
      </Modal>
      {/* Address book modal --END */}

      {/*Add Member modal --START */}
      <Modal size={"lg"} show={this.state.showMember} onHide={() => this.hideAddMemberModal()}>
        <Modal.Header closeButton>
          <Modal.Title>Address book</Modal.Title>
        </Modal.Header>
        <Modal.Body style={{ minHeight: '400px' }}>
          <div className="mng-full-table modal-adrs-book">
            <div className="row">
              <div className="col-md-12">

                {/* For adding member Start */}
                <div className="row pt-2 pb-2">
                  <div className="col-md-8">
                    <h4>Add new member to conference</h4>
                  </div>

                  <div className="col-md-4">
                    <button className="btn w-100" onClick={() => this.addMemberFromAddressBook()}>Save and close</button>
                  </div>
                </div>
                {/* For adding member End */}

                <div className="mng-full-table-hdr">
                  <div className="row">
                    <div className="col-md-2 border-rt">
                      <h6>Add/Remove</h6>
                    </div>
                    <div className="col-md-4 border-rt">
                      <h6>Contact Name</h6>
                    </div>
                    <div className="col-md-3 border-rt">
                      <h6>User Type</h6>
                    </div>
                    <div className="col-md-3">
                      <h6>Phone Number</h6>
                    </div>
                  </div>
                </div>

              </div>

            </div>
            {contactList && contactList.length > 0 && contactList.map((address, index) => {
              return <div className="row" key={index}>
                <div className="col-md-12">

                  <div className="mng-full-table-row">
                    <div className="row">
                      <div className="col-md-2 border-rt">
                        <h6>Add/Remove</h6>
                        <input type="checkbox" value={address.isAdded} defaultChecked={address.isAdded} onChange={(event) => this.addOrRemoveToConference(address._id)} />
                      </div>

                      <div className="col-md-4 border-rt">
                        <h6>Contact Name</h6>
                        <p><a href="#">{address.name}</a></p>
                      </div>
                      <div className="col-md-3 border-rt">
                        <h6>User Type</h6>
                        <p>{address.userType}</p>
                      </div>
                      <div className="col-md-3">
                        <h6>Phone Number</h6>
                        <p>{address.phoneNumber}</p>
                      </div>

                    </div>
                  </div>

                </div>
              </div>
            })
            }

          </div>

        </Modal.Body>
        {/* <Modal.Footer>
          <Button variant="primary" onClick={() => this.addMemberFromAddressBook()}> Save And Close </Button>
        </Modal.Footer> */}
      </Modal>
      {/* Add member modal --END */}

      {/* SIP Group Call Modal start */}
      <Modal size={"lg"} show={this.state.sipCallModal} onHide={() => this.hideSIPCallModal()}>
        <Modal.Header closeButton>
          <Modal.Title>Call Book</Modal.Title>
        </Modal.Header>
        <Modal.Body style={{ minHeight: '400px' }}>

          <div className="mng-full-table modal-adrs-book">

            <div className="row">

              <div className="col-md-12">

                {/* For SIP call Start */}
                {selectedConferenceId !== '' ?
                  <div className="row srch-row">
                    <div className="col-md-3">
                      <input type="text" value={this.state.sipCountryCode} className="form-control" placeholder="Country code" name="sipNo" onChange={(e) => this.setState({ sipCountryCode: e.target.value })} />
                    </div>
                    <div className="col-md-6">
                      <input type="text" value={this.state.sipNo} className="form-control" placeholder="Phone No." name="sipNo" onChange={(e) => this.setState({ sipNo: e.target.value })} />
                    </div>
                    <div className="col-md-3">
                      <button className="btn w-100" onClick={this.dialSipCall}><i className="fas fa-phone"></i></button>
                    </div>
                  </div>
                  : null}
                {/* For SIP call End */}

              </div>

            </div>

          </div>

          <div className="mng-full-table modal-adrs-book">
            <div className="row">
              <div className="col-md-12">

                <div className="row pt-2 pb-2">
                  <div className="col-md-8">
                    <h4>Call members of conference</h4>
                  </div>

                  <div className="col-md-4">
                    <button className="btn w-100" onClick={() => this.groupSipCallFromAddressBook()}>Call</button>
                  </div>
                </div>
                {/* For adding member End */}

                <div className="mng-full-table-hdr">
                  <div className="row">
                    <div className="col-md-2 border-rt">
                      <h6>Select</h6>
                    </div>
                    <div className="col-md-4 border-rt">
                      <h6>Contact Name</h6>
                    </div>
                    <div className="col-md-3 border-rt">
                      <h6>User Type</h6>
                    </div>
                    <div className="col-md-3">
                      <h6>Phone Number</h6>
                    </div>
                  </div>
                </div>

              </div>

            </div>
            {contactList.length > 0 && contactList.map((member, index) => {

              return <div className="row" key={index}>
                {member.phoneNumber != '' ?
                  <div className="col-md-12">

                    <div className="mng-full-table-row">

                      <div className="row">
                        <div className="col-md-2 border-rt">
                          <h6>Select</h6>
                          {!member.isInCall ?
                            <input type="checkbox" value={member.isCalled} defaultChecked={member.isCalled} onChange={(event) => this.sipCallInGroup(member._id)} />
                            : null}
                        </div>

                        <div className="col-md-4 border-rt">
                          <h6>Contact Name</h6>
                          <p><a href="#">{member.name}</a></p>
                        </div>
                        <div className="col-md-3 border-rt">
                          <h6>User Type</h6>
                          <p>{member.userType}</p>
                        </div>
                        <div className="col-md-3">
                          <h6>Phone Number</h6>
                          {/* <p>{'' + member.countryCode + member.contactNumber}</p> */}
                          <p>{member.countryCode + member.phoneNumber}</p>
                        </div>

                      </div>

                    </div>

                  </div>
                  : null}
              </div>

            })

            }


          </div>

        </Modal.Body>
        {/* <Modal.Footer>
          <Button variant="primary" onClick={() => this.addMemberFromAddressBook()}> Save And Close </Button>
        </Modal.Footer> */}
      </Modal>
      {/* SIP Group Call Modal start */}

      {/*Delete conference modal --START */}
      <Modal size={"lg"} show={this.state.showDeleteModal} onHide={() => this.hideDeleteModal()}>
        <Modal.Header closeButton>
          <Modal.Title>Delete Conference</Modal.Title>
        </Modal.Header>
        <Modal.Body style={{ minHeight: '50px' }}>
          <h4>Are you sure to delete conference?</h4>
        </Modal.Body>

        <Modal.Footer>
          {!loading ?
            <Button variant="primary" onClick={() => this.deleteConference()}> Ok </Button>
            : null}
          {loading ?
            <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
              <i className="fa fa-spinner fa-spin" ></i>
            </a>
            : null}
          <Button variant="primary" onClick={() => this.hideDeleteModal()}> Cancel </Button>
        </Modal.Footer>
      </Modal>
      {/* Delete conference modal --END */}

      {/*Send Invitation Link modal --START */}
      <Modal show={this.state.showInvitationModal} onHide={() => this.hideInvitationModal()}>
        <Modal.Header closeButton>
          <Modal.Title>Send Invitation Link</Modal.Title>
        </Modal.Header>
        <Modal.Body style={{ minHeight: '50px' }}>
          <div className="row">

            <div className="col-md-12">
              <div className="form-group">
                <label>Email:</label>
                <input
                  type="text"
                  name="email"
                  value={this.state.inviteMail}
                  className="form-control"
                  placeholder="Email"
                  onChange={event => this.setState({ inviteMail: event.target.value })}
                />
              </div>
            </div>

            <br />
            <div className="col-md-12">
              <div className="form-group">
                <label>OR:</label>
              </div>
            </div>
            <br />

            <div className="col-md-5">
              <div className="form-group">
                <label>Country Code:</label>
                <select
                  name="inviteCountryCode"
                  value={this.state.inviteCountryCode}
                  className="form-control"
                  onChange={event => this.setState({ inviteCountryCode: event.target.value })}
                // onFocus={() => this.setState({ countryCodeError: "" })}
                >
                  <option value=''>Select Code</option>
                  {this.state.countryCodeList && this.state.countryCodeList.map((data, index) => {
                    return <option value={data.code}>{data.code + ' ' + data.name}</option>
                  })}

                </select>
              </div>
            </div>

            <div className="col-md-7">
              <div className="form-group">
                <label>Mobile No:</label>
                <input
                  type="text"
                  name="invitePhone"
                  value={this.state.invitePhone}
                  className="form-control"
                  placeholder="Number"
                  onChange={event => this.setState({ invitePhone: event.target.value })}
                />
              </div>
            </div>

            <div className="col-md-3">
              <div className="form-group">
                <label>Offline User:</label>
              </div>
            </div>
            <div className="col-md-3">
              <div className="form-group">
                <input
                  type="checkbox"
                  checked={this.state.isOfflineUser}
                  onChange={(e) => this.setState({ isOfflineUser: !this.state.isOfflineUser })}
                />
              </div>
            </div>

            <div className="col-md-3">
              <div className="form-group">
                <label>Online User:</label>
              </div>
            </div>
            <div className="col-md-3">
              <div className="form-group">
                <input
                  type="checkbox"
                  checked={!this.state.isOfflineUser}
                  onChange={(e) => this.setState({ isOfflineUser: !this.state.isOfflineUser })}
                />
              </div>
            </div>

          </div>
          <span style={{ color: "red" }}>{this.state.invitationError ? `* ${this.state.invitationError}` : ''}</span>

        </Modal.Body>

        <Modal.Footer>
          {!this.state.invitationLoading ?
            <Button variant="primary" onClick={() => this.invitationSend()}> Send </Button>
            : null}
          {this.state.invitationLoading ?
            <Button variant="primary"><i className="fa fa-spinner fa-spin" ></i></Button>
            : null}
        </Modal.Footer>
      </Modal>
      {/* Send Invitation Link modal --END */}


      {/* For success alert --START */}
      <Modal show={this.state.showSuccessAlert} onHide={() => this.hideSuccessAlert()}>
        <Modal.Header closeButton>
          <Modal.Title>OK</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h5 style={{ color: 'green' }}>{`${this.state.successAlertMessage}`}</h5>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="primary" onClick={() => this.hideSuccessAlert()}>
            Ok
            </Button>
        </Modal.Footer>
      </Modal>
      {/* For success alert --END */}


      {/* For error alert  -- START */}
      <Modal show={this.state.showErrorAlert} onHide={() => this.hideErrorAlert()}>
        <Modal.Header closeButton>
          <Modal.Title>OK</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h5 style={{ color: 'red' }}>{`${this.state.errorAlertMessage}`}</h5>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="primary" onClick={() => this.hideErrorAlert()}>
            Ok
            </Button>
        </Modal.Footer>
      </Modal>
      {/* For error alert  -- END */}


      {/* For screen share request notification/ START  - */}
      <Modal show={this.state.showPresenterRequest} onHide={() => this.rejectScreenShareReq()}>
        <Modal.Header closeButton>
          <Modal.Title>Request for Presenter</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h5 style={{ color: 'red' }}>{this.state.presenterRequest ? `${this.state.presenterRequest.sender.name}${this.state.presenterRequest.message}` : ''}</h5>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="primary" onClick={() => this.acceptScreenShareReq()}>
            Accept
          </Button>
          <Button variant="danger" onClick={() => this.rejectScreenShareReq()}>
            Reject
          </Button>
        </Modal.Footer>
      </Modal>
      {/* For screen share request notification/ END  - */}


      {/* For showing alert after crossing conference time duration/ START -  */}
      <Modal show={this.state.callTimerModal} onHide={() => { }}>
        <Modal.Header closeButton>
          <Modal.Title>Request for Presenter</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h5 style={{ color: 'red' }}>{this.state.callTimerMessage ? `${this.state.callTimerMessage}` : ''}</h5>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="primary" onClick={() => this.continueAvoidingTimer()}>
            Continue
          </Button>
          <Button variant="danger" onClick={() => this.stopCallByTimer()}>
            End
          </Button>
        </Modal.Footer>
      </Modal>
      {/* For showing alert after crossing conference time duration/ END -  */}

      {/* For showing registered participants for a LECTURE mode call/ START - */}
      <Modal size={"lg"} show={this.state.showRegisteredList} onHide={() => this.hideRegisteredList()}>
        <Modal.Header closeButton>
          <Modal.Title>Registered participant's list</Modal.Title>
        </Modal.Header>
        <Modal.Body style={{ minHeight: '400px' }}>

          <div className="mng-full-table modal-adrs-book">
            <div className="row">
              <div className="col-md-12">

                <div className="mng-full-table-hdr">
                  <div className="row">
                    <div className="col-md-3 border-rt">
                      <h6>Name</h6>
                    </div>
                    <div className="col-md-3 border-rt">
                      <h6>Company Name</h6>
                    </div>
                    <div className="col-md-4 border-rt">
                      <h6>Email</h6>
                    </div>
                    <div className="col-md-2">
                      <h6>Phone No.</h6>
                    </div>
                  </div>
                </div>

              </div>

            </div>



            {this.state.registeredParticipants && this.state.registeredParticipants.length > 0 ?
              <>
                {this.state.registeredParticipants.map((member, index) => {

                  return <div className="row" key={index}>
                    {member.phoneNumber != '' ?
                      <div className="col-md-12">

                        <div className="mng-full-table-row">

                          <div className="row">
                            <div className="col-md-3 border-rt">
                              <h6>Name</h6>
                              <p><a href="JavaScript:Void(0);">{member.name && member.name}</a></p>
                            </div>

                            <div className="col-md-3 border-rt">
                              <h6>Company Name</h6>
                              <p><a href="JavaScript:Void(0);">{member.company}</a></p>
                            </div>
                            <div className="col-md-4 border-rt">
                              <h6>Email</h6>
                              <p>{member.email}</p>
                            </div>
                            <div className="col-md-2">
                              <h6>Phone No.</h6>
                              {/* <p>{'' + member.countryCode + member.contactNumber}</p> */}
                              <p className="textEllips">{member.phone}</p>
                            </div>

                          </div>

                        </div>

                      </div>
                      : null}
                  </div>

                })}
                <div className="row">
                  <div className="col-md-12">
                    <div className="mng-full-table-row">
                      <div className="row">
                        <div className="col-md-12" style={{ textAlign: 'center' }}>
                          {this.state.registeredParticipants.length > 0 ?
                            <CSVLink data={this.state.registeredParticipants} headers={headers} className="btn btn-primary" filename="Participants_list.csv" style={{ width: '50%' }}>
                              EXPORT LIST
                      </CSVLink>
                            : null}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </>
              :
              null

            }

          </div>

        </Modal.Body>

      </Modal>
      {/* For showing registered participants for a LECTURE mode call/ END - */}


      {/* Toast Notification Start -- */}
      <Toast
        show={this.state.showToast}
        onClose={() => { this.hideToast() }}
        delay={3500}
        className="toast-class"
        autohide
      >
        <Toast.Header>
          <strong className="mr-auto">{this.state.toastTitle}</strong>
          {/* <small>11 mins ago</small> */}
        </Toast.Header>
        <Toast.Body>{this.state.toastMessage}</Toast.Body>
      </Toast>
      {/* Toast Notification End -- */}

    </main >
  }
  //#endregion render
}

export default checkAuthentication(ConferenceCall);
