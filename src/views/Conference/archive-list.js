import React, { Component } from 'react';
import Navbar from '../Component/Navbar';
import ReactTooltip from 'react-tooltip';
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button } from "react-bootstrap"
import _ from 'lodash';

import { checkAuthentication } from '../Component/Authentication';

var path = cred.API_PATH;


export class ArchiveList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openSideMenu: false,
      userId: '',
      userType: '',

      selectedCompanyId: '',
      selectedConferenceId: '',
      selectedCompanyName: '',
      selectedCompanyType: '',
      selectedConferenceName: '',

      searchString: '',
      searchType: '',
      searching: false,

      archiveList: [],
      listRecords: '',

      loading: false,
      page: 1,


      showVideoModal: false,
      selectedArchiveName: '',
      selectedUrl: '',

      shareModal: false,
      selectedArchiveId: '',
      shareMail: '',
      shareCountryCode: '',
      sharePhone: '',
      shareError: '',

      showSuccessAlert: false,
      successAlertMessage: '',
      showErrorAlert: false,
      errorAlertMessage: '',

    };
    this.perPage = 10;

  }

  componentDidMount() {
    document.body.classList.remove("sign-bg");
    document.body.classList.add("grey-bg");

    if (this.props.location.state && this.props.location.state.name && this.props.location.state.type && this.props.location.state.conference) {

      let storage = localStorage.getItem("MIO_Local");
      storage = storage ? JSON.parse(storage) : null;
      let selectedCompanyId = this.props.match.params.companyId;
      let selectedConferenceId = this.props.match.params.conferenceId;

      let selectedCompanyName = this.props.location.state.name;
      let selectedCompanyType = this.props.location.state.type;
      let selectedConferenceName = this.props.location.state.conference;


      if (storage && storage.data.userId) {
        this.setState({
          userId: storage.data.userId,
          userType: storage.data.userType,
          userName: storage.data.fname + ' ' + storage.data.lname,
          companyId: storage.data.companyId,
          selectedConferenceId: selectedConferenceId,
          selectedCompanyId: selectedCompanyId,
          selectedCompanyName: selectedCompanyName,
          selectedCompanyType: selectedCompanyType,
          selectedConferenceName: selectedConferenceName
        }, () => {
          this.getArchivesListForAdmin()
          // console.log('selected conference --', this.state.selectedCompanyId);
        });

        // console.log(this.parseJwt(storage.token));
        let tokenData = this.parseJwt(storage.token);

      }

    } else {
      this.props.history.push("/adminconference");
    }

  }

  getArchivesListForAdmin = () => {
    this.setState({ loading: true });
    let dataToSend = {
      "conferenceId": this.state.selectedConferenceId,
      "page": this.state.page,
      "perPage": this.perPage
    };

    axios
      .post(path + 'conference/get-archive-list', dataToSend)
      .then(serverResponse => {
        let res = serverResponse.data;
        if (!res.isError) {
          // console.log("response for archive list--", res);
          let archiveList = res.details && res.details[0] && res.details[0].results;
          this.setState({
            loading: false,
            archiveList: archiveList,
            listRecords: res.details && res.details[0] && res.details[0].noofpage
          });
        } else {
          this.setState({ loading: false });
        }

      })
      .catch(error => {
        console.log(error);
      })

  }

  playArchive = (index) => {

    let { archiveList } = this.state;
    let archive = archiveList && archiveList[index];
    let url = archive.url;
    console.log("playArchive--", archive);

    this.setState({
      showVideoModal: true,
      selectedArchiveName: archive.name,
      selectedUrl: url
    });

  }

  closeVideoModal = (event) => {
    this.setState({
      showVideoModal: false,
      selectedArchiveName: '',
      selectedUrl: ''
    })
  }

  downloadArchive = (index) => {

    let { archiveList } = this.state;
    let archive = archiveList && archiveList[index];
    let url = archive.url;
    console.log("downloadArchive--", archive);

  }

  pageChangePrev = () => {

    this.setState({
      page: (parseFloat(this.state.page) - 1)
    }, () => {
      this.getArchivesListForAdmin()
    });

  }

  pageChangeNext = () => {

    this.setState({
      page: (parseFloat(this.state.page) + 1)
    }, () => {
      this.getArchivesListForAdmin()
    });

  }

  openShareModal = (index) => {

    this.setState({
      selectedArchiveId: index,
      shareModal: true
    });

  }

  hideShareArchiveModal = () => {

    this.setState({
      loading: false,
      shareModal: false,
      selectedArchiveId: '',
      shareMail: '',
      shareCountryCode: '',
      sharePhone: '',
      shareError: ''
    })

  }

  shareArchive = () => {

    let { archiveList, selectedArchiveId, selectedConferenceId, userName, selectedConferenceName, shareMail, shareCountryCode, sharePhone, shareError } = this.state;

    let archive = archiveList && archiveList[selectedArchiveId];
    let { archiveId, name, accessCode, url } = archive;

    if (shareMail && shareCountryCode && sharePhone) {
      this.setState({
        loading: true
      });

      let dataToSend = {
        conferenceId: selectedConferenceId,
        archiveId: archiveId,
        conferenceName: selectedConferenceName,
        archiveName: name,
        archiveLink: url,
        accessCode: accessCode,
        senderName: userName,
        receiverEmail: shareMail,
        receiverMobile: sharePhone,
        countryCode: shareCountryCode
      };
      console.log("sharing archive with==", dataToSend);

      // API --
      axios
        .post(path + 'conference/share-archive', dataToSend)
        .then(serverResponse => {

          let res = serverResponse.data;
          if (!res.isError) {
            this.setState({
              showSuccessAlert: true,
              successAlertMessage: "Successfully shared."
            }, () => {
              this.hideShareArchiveModal();
            });

          } else {
            this.setState({
              showErrorAlert: true,
              errorAlertMessage: "Sorry! can't be shared."
            }, () => {
              this.hideShareArchiveModal();
            });

          }

        })
        .catch(error => {
          console.log(error);
        })


    } else {
      this.setState({
        shareError: "Please give mail id and phone number"
      });
    }

  }

  deleteArchive= (index)=> {

    let {archiveList} = this.state;
    let selectedArchive = archiveList[index];

    let fileName = selectedArchive.url.split('/archives/'); 
    // console.log("selected archive file name is ==", fileName);

    let dataToSend = {
      "archiveId": selectedArchive.archiveId,
      "fileName": fileName && fileName.length > 0 ? fileName[1]: ''
    }
    // API --
    axios
    .post(path+'conference/delete-archive', dataToSend)
    .then(serverResponse=>{
      let res = serverResponse.data;
      if(!res.isError){

        this.setState({
          showSuccessAlert: true,
          successAlertMessage: "Successfully deleted.",
          loading: false
        },()=>{
          this.getArchivesListForAdmin();
        });

      }else{

        this.setState({
          showErrorAlert: true,
          errorAlertMessage: "Sorry! can't be deleted.",
          loading: false
        });

      }
    })
    .catch(error=>{
      console.log(error);
      this.setState({loading: false});
    })



  }


  hideSuccessAlert = () => {

    this.setState({
      showSuccessAlert: false,
      successAlertMessage: ''
    });

  }

  hideErrorAlert = () => {

    this.setState({
      showErrorAlert: false,
      errorAlertMessage: ''
    });

  }

  sideMenuToggle() {
    let { openSideMenu } = this.state;
    openSideMenu = !openSideMenu;
    this.setState({ openSideMenu });
  }

  mobMenuClick() {
    this.sideMenuToggle();
  }

  parseJwt = (token) => {
    if (!token) { return; }
    const base64Url = token.split('.')[1];
    const base64 = base64Url.replace('-', '+').replace('_', '/');
    return JSON.parse(window.atob(base64));
  }

  render() {
    let { loading, listRecords, page, archiveList, selectedArchiveName, showVideoModal } = this.state;

    return <main>

      <section className="user-mngnt-wrap">
        <div className="container-fluid">
          <div className="row">
            <Navbar routTo="/archive" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />
            <div className="col-xl-10 col-lg-9 p-0">

              <div className="mobile-menu-header">
                {/* <a href="JavaScript:Void(0);" className="back-arw"><i className="fas fa-arrow-left"></i></a> */}
                <a href="JavaScript:Void(0);" className="menu-tgl" onClick={() => this.sideMenuToggle()}><img src="images/menu-tgl.png" alt="" /></a>
              </div>

              <div className="paymnt-mng-hdr" style={{ margin: "30px" }}>
                <div className="blnc">
                  <div className="logobox"><img src="/images/logo-sign.png" alt="" /></div>
                </div>
                <div className="rchrg-btn">
                  <p className="text-right" style={{ margin: "0" }}>
                    Company Name: {this.state.selectedCompanyName}
                    <br />Company Type: {this.state.selectedCompanyType}
                    <br />Conference: {this.state.selectedConferenceName}
                  </p>
                </div>
              </div>

              <div className="payment-mng-wrap">

                {archiveList && archiveList.length > 0 ? archiveList.map((list, index) => {

                  return <div className="recrdng-list" key={index}>
                    <div className="recrdng-list-lt">
                      <div className="icbox"><img src="images/play-circle-bordr.png" alt="" /></div>
                      <h5>{list.name}</h5>
                    </div>
                    <div className="recrdng-list-rt">
                      <ul>
                        <li>
                          <a href="JavaScript:Void(0);"
                            onClick={() => { this.playArchive(index) }}
                            data-tip='Play'
                          >
                            <i className="fas fa-play" />
                          </a>
                          <ReactTooltip
                            effect="float"
                            place="top"
                            data-border="true"
                          />
                        </li>
                        <li>
                          <a href={list.url} className="dark-btn" target="_blank"
                            onClick={() => { this.downloadArchive(index) }}
                            data-tip='Download'
                          >
                            <i className="fas fa-download" />
                          </a>
                          <ReactTooltip
                            effect="float"
                            place="top"
                            data-border="true"
                          />
                        </li>
                        <li>
                          <a href="JavaScript:Void(0);"
                            onClick={() => { this.openShareModal(index) }}
                            data-tip='Share'
                          >
                            <i className="fa fa-share-alt" />
                          </a>
                          <ReactTooltip
                            effect="float"
                            place="top"
                            data-border="true"
                          />
                        </li>
                        <li>
                          <a href="JavaScript:Void(0);"
                            onClick={() => { this.deleteArchive(index) }}
                            data-tip='Delete'
                          >
                            <i className="fas fa-trash" />
                          </a>
                          <ReactTooltip
                            effect="float"
                            place="top"
                            data-border="true"
                          />
                        </li>
                      </ul>
                    </div>
                  </div>

                })
                  : null}

                {archiveList && archiveList.length == 0 ?
                  <div className="recrdng-list" >
                    <div className="recrdng-list-lt">

                      <h5>No records found...</h5>

                    </div>
                    <div className="recrdng-list-rt">

                    </div>
                  </div>
                  :
                  null}

              </div>

              <div className="form-group">
                {!loading && parseFloat(page) > 1 ?
                  <span style={{ color: "#007bff", cursor: "pointer" }}
                    onClick={() => { this.pageChangePrev() }}
                  >
                    <b>{"<< Prev"}</b>
                  </span>
                  :
                  null}
                    &nbsp;&nbsp;|&nbsp;&nbsp;
                    {!loading && (parseFloat(page) < parseFloat(listRecords)) ?
                  <span style={{ color: "#007bff", cursor: "pointer" }}
                    onClick={() => { this.pageChangeNext() }}
                  >
                    <b>{"Next >>"}</b>
                  </span>
                  :
                  null}
                {loading ?
                  <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                    <i className="fa fa-spinner fa-spin" ></i>
                  </a>
                  :
                  null}

              </div>


            </div>
          </div>
        </div>
      </section>

      <Modal show={this.state.showVideoModal} size="lg" onHide={(event) => { this.closeVideoModal(event) }}>
        <Modal.Header closeButton>
          <Modal.Title> {selectedArchiveName} </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="row">

            {/* <video autoPlay="" loop="" muted=""
              src={this.state.selectedUrl}
            /> */}

            <video preload="auto" style={{ width: "100%" }}
              controls controlsList="nofullscreen nodownload"
            >
              <source src={this.state.selectedUrl} />
            </video>

            {/* <source type="video/mp4" src= {this.state.selectedUrl} />
            </video> */}

          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="primary" onClick={(event) => { this.closeVideoModal(event) }}>
            Cancel
          </Button>
        </Modal.Footer>
      </Modal>

      <Modal show={this.state.shareModal} onHide={() => this.hideShareArchiveModal()}>
        <Modal.Header closeButton>
          <Modal.Title>Share archive with- </Modal.Title>
        </Modal.Header>
        <Modal.Body style={{ minHeight: '50px' }}>
          <div className="row">

            <div className="col-md-12">
              <div className="form-group">
                <label>Email:</label>
                <input
                  type="text"
                  name="email"
                  value={this.state.shareMail}
                  className="form-control"
                  placeholder="Email"
                  onChange={event => this.setState({ shareMail: event.target.value, shareError: '' })}
                />
              </div>
            </div>

            {/* <br />
            <div className="col-md-12">
              <div className="form-group">
                <label>OR:</label>
              </div>
            </div>
            <br /> */}

            <div className="col-md-4">
              <div className="form-group">
                <label>Country Code:</label>
                <input
                  type="text"
                  name="inviteCountryCode"
                  value={this.state.shareCountryCode}
                  className="form-control"
                  placeholder="Country Code"
                  onChange={event => this.setState({ shareCountryCode: event.target.value, shareError: '' })}
                />
              </div>
            </div>

            <div className="col-md-8">
              <div className="form-group">
                <label>Mobile No:</label>
                <input
                  type="text"
                  name="invitePhone"
                  value={this.state.sharePhone}
                  className="form-control"
                  placeholder="Number"
                  onChange={event => this.setState({ sharePhone: event.target.value, shareError: '' })}
                />
              </div>
            </div>

          </div>
          <span style={{ color: "red" }}>{this.state.shareError ? `* ${this.state.shareError}` : ''}</span>

        </Modal.Body>

        <Modal.Footer>
          {loading ?
            <Button variant="primary"> <i className="fa fa-spinner fa-spin" ></i> </Button>
            :
            null}
          {!loading ?
            <Button variant="primary" onClick={() => this.shareArchive()}> Send </Button>
            :
            null}

        </Modal.Footer>
      </Modal>

      {/* For success alert --START */}
      <Modal show={this.state.showSuccessAlert} onHide={() => this.hideSuccessAlert()}>
        <Modal.Header closeButton>
          <Modal.Title>OK</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h5 style={{ color: 'green' }}>{`${this.state.successAlertMessage}`}</h5>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="primary" onClick={() => this.hideSuccessAlert()}>
            Ok
            </Button>
        </Modal.Footer>
      </Modal>
      {/* For success alert --END */}

      {/* For error alert  -- START */}
      <Modal show={this.state.showErrorAlert} onHide={() => this.hideErrorAlert()}>
        <Modal.Header closeButton>
          <Modal.Title>OK</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h5 style={{ color: 'red' }}>{`${this.state.errorAlertMessage}`}</h5>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="primary" onClick={() => this.hideErrorAlert()}>
            Ok
            </Button>
        </Modal.Footer>
      </Modal>
      {/* For error alert  -- END */}


    </main>
  }
}

export default checkAuthentication(ArchiveList);