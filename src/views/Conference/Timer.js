import React, { Component } from 'react';

class Timer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      hours: '00',
      minutes: '00',
      seconds: '00'
    };

    this.counter = 0;
  }

  componentWillUnmount(){
    
    clearInterval(this.intervalHandle);
    this.setState({
      hours: "00",
      minutes: "00",
      seconds: "00",
    },()=>{
      this.counter = 0;
    });

  }

  startTimer = () => {
    // alert('getAlert from Child');
    this.intervalHandle = setInterval(() => this.tick(), 1000);
  }

  stopTimer = () => {
    clearInterval(this.intervalHandle);
    // this.setState({
    //   hours: "00",
    //   minutes: "00",
    //   seconds: "00",
    // },()=>{
    //   this.counter = 0;
    // });
    
  }

  tick() {
    let hrs = Math.floor(this.counter / 3600);
    let min = Math.floor((this.counter - (hrs * 3600)) / 60);
    let sec = Math.floor(this.counter - ((hrs * 3600) + (min * 60)));
    this.setState({
      hours: hrs < 10 ? "0" + hrs : hrs,
      minutes: min < 10 ? "0" + min : min,
      seconds: sec < 10 ? "0" + sec : sec
    });

    this.counter++;
  }

  render() {
    let { hours, minutes, seconds } = this.state;
    return <span style={{ color: 'red' }}>{`${hours}:${minutes}:${seconds}`}</span>;
  }
}

export default Timer;