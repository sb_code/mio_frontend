import React, { Component } from 'react';
import Navbar from '../Component/Navbar'
import routeList from "../../routeList.json";
import ProgressBar from 'react-bootstrap/ProgressBar';
import { Modal, Button } from "react-bootstrap";
import cred from "../../cred.json";
import axios from "axios";
import moment from 'moment';
import ReactTooltip from 'react-tooltip'; // For tool tip 
import _ from 'lodash';

import { checkAuthentication } from '../Component/Authentication';

var path = cred.API_PATH;


export class UserProfile extends Component {

    constructor(props) {
        super(props)

        this.state = {
            loading: false,
            openSideMenu: false,

            userId: "",
            userType: "",
            userName: "",

            fname: "",
            lname: "",
            email: "",
            countryCode: "",
            mobile: "",
            imgUrl: "",

            companyId: "",
            companyName: "",

            details: {},
            newStorageData: {},

            willEdit: false,

            fileUploaded: '',

            showSuccessAlert: false,
            successAlertMessage: '',
            showErrorAlert: false,
            errorAlertMessage: '',

        };
    }

    componentDidMount() {
        document.body.classList.remove("sign-bg");
        document.body.classList.add("grey-bg");

        let storage = localStorage.getItem("MIO_Local");
        storage = storage ? JSON.parse(storage) : null;

        if (storage && storage.data) {
            this.setState({
                companyName: storage.data.companyName,
                userType: storage.data.userType,
                userId: storage.data.userId
            }, () => {
                this.getUserDetails(this.state.userId);
            });
        }

    }


    getUserDetails = (userId) => {

        let dataToSend = {
            "userId": userId
        };
        this.setState({ loading: true });
        axios
            .post(path + 'user/get_user_by_id', dataToSend)
            .then(serverResponse => {
                let res = serverResponse.data;
                if (!res.isError) {
                    // console.log("response = ", res);
                    this.setState({
                        loading: false,
                        fname: res.details.fname,
                        lname: res.details.lname,
                        email: res.details.email,
                        mobile: res.details.mobile,
                        countryCode: res.details.countryCode,
                        userName: res.details.fname + ' ' + res.details.lname,
                        imgUrl: res.details.fileUrl
                    });

                } else {
                    this.setState({
                        loading: false
                    })
                }
            })
            .catch(error => {
                console.log(error);
            })

    }


    onFileChange = (e) => {

        let file = this.refs.file.files[0];
        if (file) {
            // For checking uploaded file extension -- 
            // console.log("file upload input = ", file);
            let blnValid = false;
            let sFileName = file.name;
            var _validFileExtensions = [".jpg", ".jpeg", ".png"];
            for (var j = 0; j < _validFileExtensions.length; j++) {
                var sCurExtension = _validFileExtensions[j];
                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                    blnValid = true;
                    break;
                }
            }

            if (!blnValid) {
                this.setState({
                    showErrorAlert: true,
                    errorAlertMessage: "File type doesn't match..."
                })
            } else {

                var reader = new FileReader();
                var url = reader.readAsDataURL(file);
                console.log("file url == ", url);
                reader.onloadend = function (e) {
                    this.setState({
                        imgUrl: [reader.result]
                    })
                }.bind(this);

                const photo = new FormData();
                photo.append('file', this.refs.file.files[0]);
                photo.append('userId', this.state.userId);
                this.setState({ loading: true });
                axios
                    .post(path + "user-management/upload-profile-picture",
                        photo,
                        {
                            headers: { 'Content-Type': 'multipart/form-data' },
                            onUploadProgress: (progressEvent) => {
                                // console.log("raw upload loader ---------", progressEvent.loaded, progressEvent.total);

                                let uploadPercentage = parseInt(Math.round((progressEvent.loaded / progressEvent.total) * 100));
                                this.setState({
                                    fileUploaded: uploadPercentage
                                });

                            }

                        }
                    )
                    .then(serverResponse => {

                        let res = serverResponse.data;
                        if (!res.isError) {
                            this.getUserDetails(this.state.userId)
                            this.setState({
                                loading: false,
                                showSuccessAlert: true,
                                successAlertMessage: 'Successfully uploaded.',
                                fileUploaded: ''
                            }, () => {
                                this.getUpdatedStorageData(this.state.userId);
                            });

                        } else {
                            this.setState({
                                loading: false,
                                showErrorAlert: true,
                                errorAlertMessage: 'Sorry! something wrong...',
                                fileUploaded: ''
                            })
                        }

                    })
                    .catch(error => {
                        console.log(error);
                    })

            }

        }

    }


    getUpdatedStorageData = async (userId) => {

        let dataToSend = {
            "userId": userId
        };
        this.setState({ loading: true });
        // API -- 
        axios
            .post(path + 'user/get-updated-storage-data', dataToSend)
            .then(async serverResponse => {
                let res = serverResponse.data;
                if (!res.isError) {

                    let setNewStorageData = await this.setUpdatedStorage(res);
                    // let fetchNewStorageData = await this.fetchNewDataFromStorage();
                    this.forceUpdate();
                }
            })
            .catch(error => {
                console.log(error);
            })

    }

    updateForm = (event) => {
        event.preventDefault();

        let { fname, lname, email, countryCode, mobile } = this.state;
        !fname ? this.setState({ fnameError: "Please enter first name." }) : this.setState({ fnameError: "" });
        !lname ? this.setState({ lnameError: "Please enter  last name." }) : this.setState({ lnameError: "" });
        (email && (/.+@.+\.[A-Za-z]+$/.test(`${email}`))) ? this.setState({ emailError: "" }) : this.setState({ emailError: "Please enter an valid email address." });
        (mobile && !isNaN(mobile)) ? this.setState({ mobileError: "" }) : this.setState({ mobileError: "Please enter a valid phone number." });
        (countryCode && !isNaN(countryCode)) ? this.setState({ countryCodeError: "" }) : this.setState({ countryCodeError: "Please enter a valid country code." });

        if (fname && lname && email && (/.+@.+\.[A-Za-z]+$/.test(`${email}`)) && mobile && !isNaN(mobile) && countryCode && !isNaN(countryCode)) {

            let dataToSend = {
                "userId": this.state.userId,
                "fname": fname,
                "lname": lname,
                "email": email,
                "countryCode": countryCode,
                "mobile": mobile
            };
            this.setState({ loading: true });
            axios
                .post(path + 'user/update_user_details', dataToSend)
                .then(serverResponse => {

                    let res = serverResponse.data;
                    // console.log("update-user-profile == ", res);
                    if (!res.isError) {

                        this.setState({
                            loading: false,
                            showSuccessAlert: true,
                            successAlertMessage: 'Successfully updated',
                            willEdit: false
                        }, () => {
                            this.getUpdatedStorageData(this.state.userId);
                        });

                    } else {
                        this.setState({
                            loading: false,
                            showErrorAlert: true,
                            errorAlertMessage: 'Sorry! something is wrong...',
                            willEdit: false
                        })
                    }

                })

        }

    }

    setUpdatedStorage = (res) => {
        return new Promise((resolve, reject) => {
            // localStorage.setItem("MIO_Local", JSON.stringify(res["details"]),()=>{
            this.setState({
                loading: false,
                newStorageData: res["details"]
            }, () => {
                localStorage.setItem("MIO_Local", JSON.stringify(res["details"]));
                resolve();
            })

            // });

        })

    }

    fetchNewDataFromStorage = () => {
        return new Promise((resolve, reject) => {
            let storage = localStorage.getItem("MIO_Local");
            storage = storage ? JSON.parse(storage) : null;

            if (storage && storage.data) {
                this.setState({
                    loading: false,
                    companyName: storage.data.companyName,
                    userType: storage.data.userType,
                    userId: storage.data.userId
                }, () => {
                    this.getUserDetails(this.state.userId);
                    resolve();
                });
            } else {
                this.setState({
                    loading: false
                }, () => {
                    reject();
                });
            }
        })

    }

    clearState = () => {

        this.setState({
            showSuccessAlert: false,
            successAlertMessage: '',
            showErrorAlert: false,
            errorAlertMessage: ''
        });

    }


    sideMenuToggle = () => {
        let { openSideMenu } = this.state;
        openSideMenu = !openSideMenu;
        this.setState({ openSideMenu });
    }

    mobMenuClick() {
        this.sideMenuToggle();
        console.log('mobMenuClick');
    }


    render() {
        let { loading, newStorageData, userName, fname, lname, email, countryCode, mobile, imgUrl, willEdit, openSideMenu } = this.state;

        return (
            <main>
                <section className="user-mngnt-wrap">
                    <div className="container-fluid">
                        <div className="row">

                            <Navbar routTo="/userprofileupdate" storageData={newStorageData} open={openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />
                            <div className="col-xl-10 col-lg-9 p-0 mob-full-conf">

                                <div className="mobile-menu-header">
                                    {/* <a className="back-arw" onClick={() => console.log('<-BACK')}><i className="fas fa-arrow-left"></i></a> */}
                                    <a href="JavaScript:Void(0);" className="menu-tgl" onClick={() => this.sideMenuToggle()}><img src="images/menu-tgl.png" alt="" /></a>
                                </div>

                                <div className="mng-full-list about-mng-form">
                                    <div className="row">
                                        <div className="col-lg-9 col-md-8">
                                            {!this.state.willEdit && !loading ?
                                                <div class="col-lg-3 col-md-4">

                                                    <a href="JavaScript:Void(0);"
                                                        onClick={() => { this.setState({ willEdit: !this.state.willEdit }); ReactTooltip.hide(); }}
                                                        className="btn pay-btn"
                                                        data-tip='Edit'
                                                    >
                                                        <i className="far fa-edit" />
                                                    </a>
                                                    <ReactTooltip
                                                        effect="float"
                                                        place="top"
                                                        data-border="true"
                                                    />

                                                </div>
                                                : null
                                            }
                                            {loading ?
                                                <div class="col-lg-3 col-md-4">

                                                    <a href="JavaScript:Void(0);"
                                                        className="btn pay-btn"
                                                    >
                                                        <i className="fa fa-spinner fa-spin" ></i>
                                                    </a>

                                                </div>
                                                : null}
                                            <br />
                                            <form>
                                                <div className="row">
                                                    <div className="col-md-12">
                                                        <div className="form-group">
                                                            <label>First Name:</label>
                                                            <input type="text" name="name" className="form-control" placeholder="Enter First Name"
                                                                value={fname} onChange={(event) => { this.setState({ fname: event.target.value }) }}
                                                                onFocus={() => { this.setState({ fnameError: '' }) }}
                                                                readOnly={!willEdit}
                                                            />
                                                            <span style={{ color: 'red' }}>{this.state.fnameError ? `* ${this.state.fnameError}` : ''}</span>
                                                        </div>
                                                    </div>

                                                    <div className="col-md-12">
                                                        <div className="form-group">
                                                            <label>Last Name:</label>
                                                            <input type="text" name="name" className="form-control" placeholder="Enter First Name"
                                                                value={lname} onChange={(event) => { this.setState({ lname: event.target.value }) }}
                                                                onFocus={() => { this.setState({ lnameError: '' }) }}
                                                                readOnly={!willEdit}
                                                            />
                                                            <span style={{ color: 'red' }}>{this.state.lnameError ? `* ${this.state.lnameError}` : ''}</span>
                                                        </div>
                                                    </div>

                                                    <div className="col-md-5">
                                                        <div className="form-group">
                                                            <label>Country Code:</label>
                                                            <select
                                                                className="form-control"
                                                                name="countryCode"
                                                                value={this.state.countryCode}
                                                                onChange={event => this.setState({ countryCode: event.target.value })}
                                                                onFocus={() => { this.setState({ countryCodeError: '' }) }}
                                                                disabled={!willEdit}
                                                            >
                                                                <option value="">Select country</option>
                                                                {cred.COUNTRY_CODE && cred.COUNTRY_CODE.map((data, index) => {
                                                                    return <option value={data.code}>{data.name + '  +' + data.code}</option>
                                                                })}
                                                            </select>
                                                            <span style={{ color: 'red' }}>{this.state.countryCodeError ? `* ${this.state.countryCodeError}` : ''}</span>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-7">
                                                        <div className="form-group">
                                                            <label>Contact Number:</label>
                                                            <input type="tel" name="mobile" className="form-control" placeholder="Enter Company Mobile Number"
                                                                value={mobile} onChange={(event) => { this.setState({ mobile: event.target.value }) }}
                                                                onFocus={() => { this.setState({ mobileError: '' }) }}
                                                                readOnly={!willEdit}
                                                            />
                                                            <span style={{ color: 'red' }}>{this.state.mobileError ? `* ${this.state.mobileError}` : ''}</span>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-12">
                                                        <div className="form-group">
                                                            <label>Mail Id:</label>
                                                            <input type="text" name="address" className="form-control" placeholder="Enter Company Email Address"
                                                                value={email} onChange={(event) => { this.setState({ email: event.target.value }) }}
                                                                onFocus={() => { this.setState({ emailError: '' }) }}
                                                                readOnly={!willEdit}
                                                            />
                                                            <span style={{ color: 'red' }}>{this.state.emailError ? `* ${this.state.emailError}` : ''}</span>
                                                        </div>
                                                    </div>
                                                    {willEdit ?
                                                        <div className="col-md-12">
                                                            <div className="form-group">
                                                                {!loading ?
                                                                    <button className="btn w-100"
                                                                        data-tip='Update'
                                                                        onClick={(event) => { this.updateForm(event); ReactTooltip.hide(); }}
                                                                    >
                                                                        <i className="fas fa-sync" />
                                                                    Update
                                                                    <ReactTooltip
                                                                            effect="float"
                                                                            place="top"
                                                                            data-border="true"
                                                                        />
                                                                    </button>
                                                                    : null}
                                                                {loading ?
                                                                    <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                                                                        <i className="fa fa-spinner fa-spin" ></i>
                                                                    </a>
                                                                    : null}

                                                            </div>
                                                        </div>
                                                        :
                                                        null}

                                                </div>
                                            </form>
                                        </div>


                                        <div className="col-lg-3 col-md-4">
                                            <div className="img-upload-rt">
                                                <div className="form-group">
                                                    <label>Upload Image:</label>
                                                    <div className="upload-box">
                                                        {/* <img src="https://cdn.shoplightspeed.com/shops/622945/files/10501368/800x1024x1/temby-australia-black-and-silver-curved-soprano-sa.jpg" /> */}

                                                        <img src={imgUrl} />

                                                    </div>
                                                </div>
                                                <input
                                                    type="file" name="Upload"
                                                    ref="file"
                                                    className="form-control"
                                                    onChange={(e) => { this.onFileChange(e) }} multiple={true}
                                                    style={{ background: "#0083C6", color: "#fff" }}
                                                />
                                                <ProgressBar striped variant="success" now={this.state.fileUploaded} label={`${this.state.fileUploaded}%`} />
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </section>

                <Modal show={this.state.showSuccessAlert} onHide={() => this.clearState()}>
                    <Modal.Header closeButton>
                        <Modal.Title>OK</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <h5 style={{ color: 'green' }}>{`${this.state.successAlertMessage}`}</h5>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="primary" onClick={() => this.clearState()}>
                            Ok
                        </Button>
                    </Modal.Footer>
                </Modal>

                <Modal show={this.state.showErrorAlert} onHide={() => this.clearState()}>
                    <Modal.Header closeButton>
                        <Modal.Title>OK</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <h5 style={{ color: 'red' }}>{`${this.state.errorAlertMessage}`}</h5>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="primary" onClick={() => this.clearState()}>
                            Ok
                        </Button>
                    </Modal.Footer>
                </Modal>


            </main>
        );
    }
}

export default checkAuthentication(UserProfile);