import React, { Component } from 'react'
import Navbar from '../Component/Navbar'
import PaystackButton from 'react-paystack';
import StripeCheckout from 'react-stripe-checkout';
import { PayPalButton } from "react-paypal-button-v2";

import { Route, Switch, Redirect } from 'react-router-dom';
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button } from "react-bootstrap"
import _ from 'lodash';
import { CSVLink, CSVDownload } from "react-csv";
import ReactTooltip from 'react-tooltip'; // For tool tip 
import { reportCSS } from '../../util/reportCSS';
import { getInvoiceReportHtml } from '../../util/getInvoiceReportHtml';

import { checkAuthentication } from '../Component/Authentication';

var path = cred.API_PATH;

const monthList = [{ "key": 1, "name": 'January' }, { "key": 2, "name": 'February' }, { "key": 3, "name": 'March' }, { "key": 4, "name": 'April' }, { "key": 5, "name": 'May' },
{ "key": 6, "name": 'June' }, { "key": 7, "name": 'July' }, { "key": 8, "name": 'August' }, { "key": 9, "name": 'September' }, { "key": 10, "name": 'October' },
{ "key": 11, "name": 'November' }, { "key": 12, "name": 'December' }];

export class UsedServices extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userId: '',
            companyType: '',
            companyId: '',
            companyName: '',

            openSideMenu: false,

            loading: false,

            transactionLists: '',
            archive: '',
            sipInBoundCall: '',
            sipcall: '',
            webCall: '',

            searchYear: (new Date()).getFullYear(),
            searchMonth: (new Date()).getMonth() + 1,
            searching: false,

            thisYear: (new Date()).getFullYear(),
            minOffset: 0,
            maxOffset: 60,
            thisMonth: (new Date()).getMonth() + 1,

        }
    }

    componentDidMount() {
        document.body.classList.remove("sign-bg");
        document.body.classList.add("grey-bg");

        let storage = localStorage.getItem("MIO_Local");
        storage = storage ? JSON.parse(storage) : null;

        if (storage && storage.data.userId) {
            this.setState({
                userId: storage.data.userId,
                companyType: storage.data.companyType,
                companyId: storage.data.companyId,
                companyName: storage.data.companyName
            }, () => {
                this.getusedServiceDetails();
            });

        }

    }


    getusedServiceDetails = () => {

        let today = new Date()
        let dataToSend = {
            "month": today.getMonth() + 1,
            "year": today.getFullYear(),
            "companyId": this.state.companyId
        };
        this.setState({ loading: true });

        axios
            .post(path + 'price-management/fetch-overall-cost-for-particular-company', dataToSend)
            .then(serverResponse => {
                let res = serverResponse.data;
                if (!res.isError) {
                    let details = res.details;
                    // console.log("transaction details ==", details);
                    this.setState({
                        transactionLists: details,
                        archive: details && details.archive,
                        sipInBoundCall: details && details.sipInBoundCall,
                        sipcall: details && details.sipcall,
                        webCall: details && details.webCall,
                        loading: false
                    });
                } else {
                    this.setState({
                        loading: false
                    });
                }
            })
            .catch(error => {
                console.log(error);
            })

    }


    searchTransaction = (event) => {

        event.preventDefault();
        let { searchYear, searchMonth, companyId } = this.state;
        this.setState({ searching: true, loading: true });
        let dataToSend = {
            "year": searchYear,
            "month": searchMonth,
            // "companyId": "5e5fc83922feea7a2f232c77"
            "companyId": companyId
        };

        axios
            .post(path + 'price-management/fetch-overall-cost-for-particular-company', dataToSend)
            .then(serverResponse => {
                let res = serverResponse.data;
                if (!res.isError) {
                    let details = res.details;
                    // console.log("transaction details ==", details);
                    this.setState({
                        transactionLists: details,
                        archive: details && details.archive,
                        sipInBoundCall: details && details.sipInBoundCall,
                        sipcall: details && details.sipcall,
                        webCall: details && details.webCall,
                        loading: false
                    });
                } else {
                    this.setState({
                        loading: false
                    });
                }
            })
            .catch(error => {
                console.log(error);
            })

    }


    clearSearch = (event) => {
        event.preventDefault();
        this.setState({
            searching: false,
            loading: false,
            searchYear: (new Date()).getFullYear(),
            searchMonth: (new Date()).getMonth() + 1,
            transactionLists: '',
            archive: '',
            sipInBoundCall: '',
            sipcall: '',
            webCall: '',
        }, () => {
            this.getusedServiceDetails();
        });

    }

    // For date formatting -- 
    formatDate = (string) => {

        // new Intl.DateTimeFormat('en-US', {year: 'numeric', month: '2-digit',day: '2-digit', hour: '2-digit', minute: '2-digit', second: '2-digit'}).format(value.date)

        let options = { year: 'numeric', month: 'long', day: 'numeric' };
        return new Date(string).toLocaleDateString([], options);

    }

    backMenueToggle = () => {
        this.props.history.goBack();
    }

    sideMenuToggle() {
        let { openSideMenu } = this.state;
        openSideMenu = !openSideMenu;
        this.setState({ openSideMenu });
    }


    mobMenuClick() {
        this.sideMenuToggle();
        console.log('mobMenuClick');
    }



    render() {
        let { loading, searching, searchMonth, searchYear,
            transactionLists,
            archive,
            sipInBoundCall,
            sipcall,
            webCall,
            thisYear,
            minOffset,
            maxOffset } = this.state;
        const monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
        ];
        let options = [];
        for (let i = minOffset; i <= maxOffset; i++) {
            let year = thisYear - i;
            options.push(<option value={year} key={i}>{year}</option>);
        }

        return <main>

            <section className="user-mngnt-wrap">
                <div className="container-fluid">
                    <div className="row">
                        <Navbar routTo="/paymentmanagement" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />
                        <div className="col-xl-10 col-lg-9 p-0 mob-full-conf">

                            <div className="mobile-menu-header">
                                <a href="JavaScript:Void(0);" className="back-arw" onClick={() => this.backMenueToggle()}><i className="fas fa-arrow-left"></i></a>
                                {/* <a className="menu-tgl" onClick={() => this.sideMenuToggle()}><img src="images/menu-tgl.png" alt="" /></a> */}
                            </div>

                            <div className="mng-full-list">
                                <div className="mng-full-srch">

                                    <div className="row">
                                        <div className="col-md-6">
                                            <label>Month</label>
                                            <select className="form-control"
                                                name="searchType"
                                                onChange={(event) => { this.setState({ searchMonth: event.target.value }) }}
                                            >
                                                <option value="">Select Month</option>
                                                {monthList && monthList.map((data, index) => {
                                                    return <option value={data.key}
                                                        selected={data.key == searchMonth ? true : false}
                                                    >
                                                        {data.name}
                                                    </option>
                                                })}
                                            </select>
                                        </div>

                                        <div className="col col-md-6">
                                            <label>Year</label>
                                            <div className="srch-wrap">
                                                <form>
                                                    <select class="form-control"
                                                        name="searchString"
                                                        vlaue={thisYear}
                                                        onChange={(event) => { this.setState({ searchYear: event.target.value }) }}
                                                    >

                                                        {options}
                                                    </select>

                                                    {this.state.searching ?
                                                        <input type="button" onClick={(event) => this.clearSearch(event)}
                                                            data-tip='Clear'
                                                        />
                                                        :
                                                        <input type="submit" onClick={(event) => this.searchTransaction(event)}
                                                            data-tip='Search'
                                                        />
                                                    }

                                                    <ReactTooltip
                                                        effect="float"
                                                        place="top"
                                                        data-border="true"
                                                    />
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {transactionLists ?
                                    <div className="mng-full-table company-mangmnt">

                                        <div className="row">

                                            <div className="col-md-12">
                                                <div className="mng-full-table-hdr">
                                                    <div className="row">
                                                        <div className="col-md-3 border-rt">
                                                            <h6>Name</h6>
                                                        </div>
                                                        <div className="col-md-3 border-rt">
                                                            <h6>No of calls</h6>
                                                        </div>
                                                        <div className="col-md-3 border-rt">
                                                            <h6>Duration</h6>
                                                        </div>
                                                        <div className="col-md-3">
                                                            <h6>Price</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        {sipInBoundCall ?
                                            <div className="row">
                                                <div className="col-md-12">

                                                    <div className="mng-full-table-row">
                                                        <div className="row">
                                                            <div className="col-md-3 border-rt">
                                                                <h6>Name</h6>
                                                                <p className="textEllips">
                                                                    SIP Inbound Call
                                                                </p>
                                                            </div>
                                                            <div className="col-md-3 border-rt">
                                                                <h6>No of calls</h6>
                                                                <p>
                                                                    {sipInBoundCall.totalSipInBound}
                                                                </p>
                                                            </div>
                                                            <div className="col-md-3 border-rt">
                                                                <h6>Duration</h6>
                                                                <p>{parseFloat(sipInBoundCall.sipInBoundOverAllDuration).toFixed('2')}</p>
                                                            </div>
                                                            <div className="col-md-3">
                                                                <h6>Price</h6>
                                                                <p>{parseFloat(sipInBoundCall.sipInBoundOverAllPrice).toFixed('2')}</p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            :
                                            null}

                                        {sipcall ?
                                            <div className="row">
                                                <div className="col-md-12">

                                                    <div className="mng-full-table-row">
                                                        <div className="row">
                                                            <div className="col-md-3 border-rt">
                                                                <h6>Name</h6>
                                                                <p className="textEllips">
                                                                    <a href="JavaScript:Void(0);" >
                                                                        SIP Outbound Call
                                                                    </a>
                                                                </p>
                                                            </div>
                                                            <div className="col-md-3 border-rt">
                                                                <h6>No of Calls</h6>
                                                                <p>
                                                                    {sipcall.totalSipCall}
                                                                </p>
                                                            </div>
                                                            <div className="col-md-3 border-rt">
                                                                <h6>Duration</h6>
                                                                <p>{parseFloat(sipcall.sipCallOverAllDuration).toFixed('2')}</p>
                                                            </div>
                                                            <div className="col-md-3">
                                                                <h6>Price</h6>
                                                                <p>{parseFloat(sipcall.sipCallOverAllPrice).toFixed('2')}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            :
                                            null}

                                        {webCall ?
                                            <div className="row">
                                                <div className="col-md-12">

                                                    <div className="mng-full-table-row">
                                                        <div className="row">
                                                            <div className="col-md-3 border-rt">
                                                                <h6>Name</h6>
                                                                <p className="textEllips">
                                                                    <a href="JavaScript:Void(0);" >
                                                                        Web Calls
                                                                        </a>
                                                                </p>
                                                            </div>
                                                            <div className="col-md-3 border-rt">
                                                                <h6>No of Calls</h6>
                                                                <p>
                                                                    {webCall.totalWebCall}
                                                                </p>
                                                            </div>
                                                            <div className="col-md-3 border-rt">
                                                                <h6>Duration</h6>
                                                                <p>{parseFloat(webCall.webCallOverAllDuration).toFixed('2')}</p>
                                                            </div>
                                                            <div className="col-md-3">
                                                                <h6>Price</h6>
                                                                <p>{parseFloat(webCall.webCallOverAllPrice).toFixed('2')}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            :
                                            null}

                                        {archive ?
                                            <div className="row">
                                                <div className="col-md-12">

                                                    <div className="mng-full-table-row">
                                                        <div className="row">
                                                            <div className="col-md-3 border-rt">
                                                                <h6>Name</h6>
                                                                <p className="textEllips">
                                                                    <a href="JavaScript:Void(0);" >
                                                                        Archives
                                                                    </a>
                                                                </p>
                                                            </div>
                                                            <div className="col-md-3 border-rt">
                                                                <h6>No of Calls</h6>
                                                                <p>
                                                                    {archive.totalArchive}
                                                                </p>
                                                            </div>
                                                            <div className="col-md-3 border-rt">
                                                                <h6>Duration</h6>
                                                                <p>{parseFloat(archive.archiveOverAllDuration).toFixed('2')}</p>
                                                            </div>
                                                            <div className="col-md-3">
                                                                <h6>Price</h6>
                                                                <p>{parseFloat(archive.archiveOverAllPrice).toFixed('2')}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            :
                                            null}

                                    </div>
                                    :
                                    <div className="mng-full-table">
                                        <div className="row">
                                            <div className="col-md-12">
                                                <div className="mng-full-table-hdr admn-usr-hdr">
                                                    <div className="row">
                                                        <div className="col-md-12">
                                                            <h6>No Details Found.</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                }

                                {loading ?
                                    <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                                        <i className="fa fa-spinner fa-spin" ></i>
                                    </a>
                                    :
                                    null}

                            </div>

                        </div>

                    </div>

                </div>

            </section>

        </main >

    }

}

export default checkAuthentication(UsedServices);