import React, { Component } from "react";
import Navbar from "../Component/Navbar";

export class Thankyou extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openSideMenu: false,
    }
  }
  componentDidMount() {
    document.body.classList.remove("sign-bg");
    document.body.classList.add("grey-bg");
  }

  sideMenuToggle() {
    let { openSideMenu } = this.state;
    openSideMenu = !openSideMenu;
    this.setState({ openSideMenu });
  }

  mobMenuClick() {
    this.sideMenuToggle();
    console.log('mobMenuClick');
  }

  render() {

    let { } = this.state;

    return (
      <main>

        <section className="user-mngnt-wrap">
          <div className="container-fluid">
            <div className="row">
              <Navbar routTo="/useses" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />
              <div className="col-xl-10 col-lg-9 p-0 mob-full-conf">
                <div className="mobile-menu-header">
                  {/* <a className="back-arw" onClick={() => console.log('<-BACK')}><i className="fas fa-arrow-left"></i></a> */}
                  <a className="menu-tgl" onClick={() => this.sideMenuToggle()}><img src="images/menu-tgl.png" alt="" /></a>
                </div>
                <div className="mng-full-list center-screen">
                  <div className="mng-full-table">
                    <div className="row">
                      <div className="col-md-12">
                        <p className=" thankyou-box">
                          Thank you for joining the conference.
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
    );
  }
}

export default Thankyou;
