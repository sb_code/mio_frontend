import React, { Component } from "react";
import Navbar from "../Component/Navbar";
import axios from "axios";
import cred from "../../cred.json";

let path = cred.API_PATH;

export class DurationDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      openSideMenu: false,
      userId: '',
      selectedConference: '',
      durationDetails: null,
      isCostDisplay: false,

      totalDuration: '',
      totalPrice: '',
      totalCost: ''

    }
  }
  componentDidMount() {
    document.body.classList.remove("sign-bg");
    document.body.classList.add("grey-bg");

    let storage = localStorage.getItem("MIO_Local");
    storage = storage ? JSON.parse(storage) : null;

    if (storage && storage.data.userId && this.props.location.state && this.props.location.state.selectedConference && (this.props.location.state.selectedConference.createdBy == storage.data.userId || this.props.location.state.selectedConference.hostUserId == storage.data.userId)) {
      this.setState({
        userId: storage.data.userId,
        selectedConference: this.props.location.state.selectedConference,
        isCostDisplay: this.props.location.state.isCostDisplay 
        // isCostDisplay: true
      }, () => this.getUsesDetails())
    } else {
      this.props.history.push('/');
    }
  }

  getUsesDetails() {
    let that = this;
    let { selectedConference } = this.state;
    let dataToSend = {
      conferenceId: selectedConference._id
    };
    this.setState({ loading: true });

    axios
      .post(path + "price-management/fetch-cost-for-duration-of-conference", dataToSend)
      .then(serverResponse => {
        console.log(serverResponse);
        if (!serverResponse.data.isError) {

          let res = serverResponse.data && serverResponse.data.details;
          let totalDuration = parseFloat(res.archiveDetails && res.archiveDetails.totalArchiveDuration) +
            parseFloat(res.sipCallDetails && res.sipCallDetails.totalSipCalDuration) +
            parseFloat(res.sipInboundcallDetails && res.sipInboundcallDetails.totalSipInboundDuration) +
            parseFloat(res.webCallDetails && res.webCallDetails.totalWebCallDuration);


          let totalPrice = parseFloat(res.archiveDetails && res.archiveDetails.totalArchivePrice) +
            parseFloat(res.sipCallDetails && res.sipCallDetails.totalSipCalPrice) +
            parseFloat(res.sipInboundcallDetails && res.sipInboundcallDetails.totalSipInboundCallPrice) +
            parseFloat(res.webCallDetails && res.webCallDetails.totalWebCalPrice);

          let totalCost = parseFloat(res.archiveDetails && res.archiveDetails.totalArchiveCost) +
            parseFloat(res.sipCallDetails && res.sipCallDetails.totalSipCalCost) +
            parseFloat(res.sipInboundcallDetails && res.sipInboundcallDetails.totalSipInboundCallCost) +
            parseFloat(res.webCallDetails && res.webCallDetails.totalWebCalCost);


          that.setState({
            loading: false,
            durationDetails: serverResponse.data.details,
            totalDuration: totalDuration,
            totalPrice: totalPrice,
            totalCost: totalCost
          });
        } else {
          this.setState({ loading: false })
        }
      })
      .catch(error => {
        console.log(error);
      })
  }

  sideMenuToggle() {
    let { openSideMenu } = this.state;
    openSideMenu = !openSideMenu;
    this.setState({ openSideMenu });
  }

  mobMenuClick() {
    this.sideMenuToggle();
    console.log('mobMenuClick');
  }

  backMenueToggle = () => {
    this.props.history.goBack();
  }

  render() {

    let { durationDetails, isCostDisplay, loading } = this.state;

    return (
      <main>

        <section className="user-mngnt-wrap">
          <div className="container-fluid">
            <div className="row">
              <Navbar routTo="/useses" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />
              <div className="col-xl-10 col-lg-9 p-0 mob-full-conf">
                <div className="mobile-menu-header">
                  <a href="JavaScript:Void(0);" className="back-arw" onClick={() => this.backMenueToggle()}><i className="fas fa-arrow-left"></i></a>
                  {/* <a className="menu-tgl" onClick={() => this.sideMenuToggle()}><img src="images/menu-tgl.png" alt="" /></a> */}
                </div>
                <div className="mng-full-list">
                  <div className="mng-full-table">

                    <div className="row">
                      <div className="col-md-12">
                        <div className="mng-full-table-hdr">
                          <div className="row">
                            <div className={isCostDisplay ? "col-md-3 border-rt" : "col-md-4 border-rt"}>
                              <h6>Service</h6>
                            </div>
                            <div className={isCostDisplay ? "col-md-3 border-rt" : "col-md-4 border-rt"}>
                              <h6>Count</h6>
                            </div>
                            <div className={isCostDisplay ? "col-md-3 border-rt" : "col-md-4"}>
                              <h6>Duration</h6>
                            </div>
                            {isCostDisplay ?
                              <div className="col-md-3">
                                <h6>Charges</h6>
                              </div>
                              : null
                            }
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-md-12">
                        <div className="mng-full-table-row">
                          <div className="row">
                            <div className={isCostDisplay ? "col-md-3 border-rt" : "col-md-4 border-rt"}>
                              <h6>Head</h6>
                              <p className="textEllips">
                                <a href="JavaScript:Void(0);" >
                                  Web call
                                </a>
                              </p>
                            </div>
                            <div className={isCostDisplay ? "col-md-3 border-rt" : "col-md-4 border-rt"}>
                              <h6>Count</h6>
                              <p className="textEllips">{durationDetails && durationDetails.webCallDetails && durationDetails.webCallDetails.totalWebCall ? (durationDetails.webCallDetails.totalWebCall) : '0'}</p>
                            </div>
                            <div className={isCostDisplay ? "col-md-3 border-rt" : "col-md-4"}>

                              <h6>Duration</h6>
                              <p className="textEllips">
                                {durationDetails && durationDetails.webCallDetails && durationDetails.webCallDetails.totalWebCallDuration ? parseFloat(durationDetails.webCallDetails.totalWebCallDuration).toFixed(2) : '0.00'}
                              </p>
                            </div>
                            {isCostDisplay ?
                              <div className="col-md-3">
                                <h6>Charges</h6>
                                <p className="textEllips">{durationDetails && durationDetails.webCallDetails && durationDetails.webCallDetails.totalWebCalPrice ? parseFloat(durationDetails.webCallDetails.totalWebCalPrice).toFixed(2) : '0.00'}</p>
                              </div>
                              :
                              null
                            }
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-md-12">
                        <div className="mng-full-table-row">
                          <div className="row">
                            <div className={isCostDisplay ? "col-md-3 border-rt" : "col-md-4 border-rt"}>
                              <h6>Head</h6>
                              <p className="textEllips">
                                <a href="JavaScript:Void(0);" >
                                  Dial in
                                </a>
                              </p>
                            </div>
                            <div className={isCostDisplay ? "col-md-3 border-rt" : "col-md-4 border-rt"}>
                              <h6>Count</h6>
                              <p className="textEllips">{durationDetails && durationDetails.sipInboundcallDetails && durationDetails.sipInboundcallDetails.totalSipInboundCall ? (durationDetails.sipInboundcallDetails.totalSipInboundCall) : '0'}</p>
                            </div>
                            <div className={isCostDisplay ? "col-md-3 border-rt" : "col-md-4"}>

                              <h6>Duration</h6>
                              <p className="textEllips">
                                {durationDetails && durationDetails.sipInboundcallDetails && durationDetails.sipInboundcallDetails.totalSipInboundDuration ? parseFloat(durationDetails.sipInboundcallDetails.totalSipInboundDuration).toFixed('2') : '0.00'}
                              </p>
                            </div>
                            {isCostDisplay ?
                              <div className="col-md-3">
                                <h6>Charges</h6>
                                <p className="textEllips">{durationDetails && durationDetails.sipInboundcallDetails && durationDetails.sipInboundcallDetails.totalSipInboundCallPrice ? parseFloat(durationDetails.sipInboundcallDetails.totalSipInboundCallPrice).toFixed('2') : '0.00'}</p>
                              </div>
                              :
                              null
                            }
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-md-12">
                        <div className="mng-full-table-row">
                          <div className="row">
                            <div className={isCostDisplay ? "col-md-3 border-rt" : "col-md-4 border-rt"}>
                              <h6>Head</h6>
                              <p className="textEllips">
                                <a href="JavaScript:Void(0);" >
                                  Dial out
                                </a>
                              </p>
                            </div>
                            <div className={isCostDisplay ? "col-md-3 border-rt" : "col-md-4 border-rt"}>
                              <h6>Count</h6>
                              <p className="textEllips">{durationDetails && durationDetails.sipCallDetails && durationDetails.sipCallDetails.totalSipCall ? (durationDetails.sipCallDetails.totalSipCall) : '0'}</p>
                            </div>
                            <div className={isCostDisplay ? "col-md-3 border-rt" : "col-md-4"}>
                              <h6>Duration</h6>
                              <p className="textEllips">
                                {durationDetails && durationDetails.sipCallDetails && durationDetails.sipCallDetails.totalSipCalDuration ? parseFloat(durationDetails.sipCallDetails.totalSipCalDuration).toFixed('2') : '0.00'}
                              </p>
                            </div>
                            {isCostDisplay ?
                              <div className="col-md-3">
                                <h6>Charges</h6>
                                <p className="textEllips">{durationDetails && durationDetails.sipCallDetails && durationDetails.sipCallDetails.totalSipCalPrice ? parseFloat(durationDetails.sipCallDetails.totalSipCalPrice).toFixed('2') : '0.00'}</p>
                              </div>
                              :
                              null
                            }
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-md-12">
                        <div className="mng-full-table-row">
                          <div className="row">
                            <div className={isCostDisplay ? "col-md-3 border-rt" : "col-md-4 border-rt"}>
                              <h6>Head</h6>
                              <p className="textEllips">
                                <a href="JavaScript:Void(0);" >
                                  Archive
                                </a>
                              </p>
                            </div>
                            <div className={isCostDisplay ? "col-md-3 border-rt" : "col-md-4 border-rt"}>
                              <h6>Count</h6>
                              <p className="textEllips">{durationDetails && durationDetails.archiveDetails && durationDetails.archiveDetails.totalArchive ? (durationDetails.archiveDetails.totalArchive) : '0'}</p>
                            </div>
                            <div className={isCostDisplay ? "col-md-3 border-rt" : "col-md-4"}>
                              <h6>Duration</h6>
                              <p className="textEllips">
                                {durationDetails && durationDetails.archiveDetails && durationDetails.archiveDetails.totalArchiveDuration ? parseFloat(durationDetails.archiveDetails.totalArchiveDuration).toFixed('2') : '0.00'}
                              </p>
                            </div>
                            {isCostDisplay ?
                              <div className="col-md-3">
                                <h6>Charges</h6>
                                <p className="textEllips">{durationDetails && durationDetails.archiveDetails && durationDetails.archiveDetails.totalArchivePrice ? parseFloat(durationDetails.archiveDetails.totalArchivePrice).toFixed('2') : '0.00'}</p>
                              </div>
                              : null
                            }

                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-md-12">
                        <div className="mng-full-table-row">
                          <div className="row">
                            <div className={isCostDisplay ? "col-md-3 border-rt" : "col-md-4 border-rt"}>
                              <h6>Head</h6>
                              <p className="textEllips">
                                <a href="JavaScript:Void(0);" >
                                  Total
                                </a>
                              </p>
                            </div>
                            <div className={isCostDisplay ? "col-md-3 border-rt" : "col-md-4 border-rt"}>
                              <h6>Count</h6>
                              <p className="textEllips">-</p>
                            </div>
                            <div className={isCostDisplay ? "col-md-3 border-rt" : "col-md-4"}>
                              <h6>Duration</h6>
                              <p className="textEllips">
                                {this.state.totalDuration ? parseFloat(this.state.totalDuration).toFixed('2') : '0.00'}
                              </p>
                            </div>
                            {isCostDisplay ?
                              <div className="col-md-3">
                                <h6>Charges</h6>
                                <p className="textEllips">
                                  {this.state.totalPrice ? parseFloat(this.state.totalPrice).toFixed('2') : '0.00'}
                                </p>
                              </div>
                              : null
                            }

                          </div>
                        </div>
                      </div>
                    </div>

                  </div>

                  <br /><br />
                  <div className="form-group">

                    {loading ?
                      <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                        <i className="fa fa-spinner fa-spin" ></i>
                      </a>
                      :
                      null}

                  </div>

                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
    );
  }
}

export default DurationDetails;
