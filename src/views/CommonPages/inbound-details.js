import React, { Component } from "react";
import axios from "axios";
import cred from "../../cred.json";

// import { checkAuthentication } from '../Component/Authentication';

var path = cred.API_PATH;

export class InboundDetails extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: false,

			userId: '',

			openSideMenu: false,
			searchString: '',
			searchType: '',
			searching: false,

			inboundLists: [],

			editUser: false,
			confirm: false,

			selectedCompanyId: '',

			pageNo: 1,
			perPage: 10,
			totalRows: '',


		}
	}

	componentDidMount() {
		document.body.classList.remove("sign-bg");
		document.body.classList.add("grey-bg");
		// let storage = localStorage.getItem("MIO_Local");

		let selectedCompanyId = this.props.match.params.id;

		this.setState({
			selectedCompanyId: selectedCompanyId
		}, () => {
			this.getInboundList();
		});

	}

	getInboundList = () => {
		let { selectedCompanyId, pageNo, perPage } = this.state;
		this.setState({ loading: true });
		let dataToSend = {
			companyId: selectedCompanyId,
			"page": pageNo,
			"perPage": perPage
		};

		axios
			.post(path + 'user/get-inbound-numbers-by-companyid', dataToSend)
			.then(serverResponse => {
				let res = serverResponse.data;
				console.log(res);
				if (!res.isError && res.details && res.details.length > 0 && res.details[0].results && res.details[0].results.length > 0) {
					this.setState({
						inboundLists: res.details[0].results,
						totalRows: res.details[0].total,
						loading: false
					}, () => {
						console.log("inbound details == ", this.state.inboundLists)
					});
				} else {
					this.setState({
						loading: false
					});
				}
			})
			.catch(error => {
				console.log(error);
				this.setState({
					loading: false
				});
			})
	}

	// For gong previous page--
	pageChangePrev = () => {

		this.setState({
			pageNo: (parseFloat(this.state.pageNo) - 1)
		}, () => {
			this.getInboundList()
		});

	}

	// For going next page --
	pageChangeNext = () => {

		this.setState({
			pageNo: (parseFloat(this.state.pageNo) + 1)
		}, () => {
			this.getInboundList()
		});

	}


	sideMenuToggle() {
		let { openSideMenu } = this.state;
		openSideMenu = !openSideMenu;
		this.setState({ openSideMenu });
	}

	mobMenuClick() {
		this.sideMenuToggle();
		console.log('mobMenuClick');
	}


	render() {

		let { inboundLists, selectedCompanyId, pageNo, perPage, totalRows, loading } = this.state;

		return (
			<main>

				<section className="user-mngnt-wrap">
					<div className="container-fluid">
						<div className="row">
							{/* <Navbar routTo="/newregistered" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} /> */}
							<div className="col-xl-12 col-lg-9 p-0">
								<div className="mobile-menu-header">
									{/* 
                                <a href="JavaScript:Void(0);" className="menu-tgl" onClick={() => this.sideMenuToggle()}>
                                    <img src="/images/menu-tgl.png" alt="" />
                                </a> */}
								</div>
								<div className="mng-full-list">
									<div className="mng-full-srch">

										<div className="row">


										</div>

									</div>
									{inboundLists && inboundLists.length > 0 ?
										<div className="mng-full-table company-mangmnt">

											<div className="row">

												<div className="col-md-12">
													<div className="mng-full-table-hdr">
														<div className="row">
															<div className="col-md-6 border-rt">
																<h6>Country</h6>
															</div>
															<div className="col-md-6 border-rt">
																<h6>Phone No.</h6>
															</div>
															{/* <div className="col-md-4">
																	<h6>SIP Provider</h6>
															</div> */}
														</div>
													</div>
												</div>

											</div>

											{inboundLists && inboundLists.length > 0 ? inboundLists.map((data, index) => {

												return (data && data && data.isActive ?
													<div className="row" key={index}>
														<div className="col-md-12">

															<div className="mng-full-table-row">
																<div className="row">
																	<div className="col-md-6 border-rt">
																		<h6>Country</h6>
																		<p>{data.country}</p>
																	</div>
																	<div className="col-md-6 border-rt">
																		<h6>Phone No.</h6>
																		<p>{`+ ${data.countryCode} ${data.phoneNumber}`}</p>
																	</div>
																	{/* <div className="col-md-4">
																			<h6>SIP Provider</h6>
																			<p>{data.inboundNumbers.sipProvider}</p>
																	</div> */}
																</div>
															</div>

														</div>
													</div>
													: null)
											})
												: null}

										</div>
										:
										<div className="mng-full-table">
											<div className="row">
												<div className="col-md-12">
													<div className="mng-full-table-hdr admn-usr-hdr">
														<div className="row">
															<div className="col-md-12">
																<h6>No Inbound Numbers Found.</h6>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									}
									<br /><br />
									<div className="form-group">
										{!loading && parseFloat(pageNo) > 1 ?
											<span style={{ color: "#007bff", cursor: "pointer" }}
												onClick={() => { this.pageChangePrev() }}
											>
												<b>{"<< Prev"}</b>
											</span>
											:
											null}
                                            &nbsp;&nbsp;|&nbsp;&nbsp;
                                            {!loading && ((parseFloat(pageNo) * parseFloat(perPage)) < parseFloat(totalRows)) ?
											<span style={{ color: "#007bff", cursor: "pointer" }}
												onClick={() => { this.pageChangeNext() }}
											>
												<b>{"Next >>"}</b>
											</span>
											:
											null}
										{loading ?
											<a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
												<i className="fa fa-spinner fa-spin" ></i>
											</a>
											:
											null}

									</div>

								</div>

							</div>

						</div>
					</div>
				</section>

			</main>
		);
	}
}

export default InboundDetails;
