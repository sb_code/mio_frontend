import React, { Component } from 'react'
import Navbar from '../Component/Navbar'
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button } from "react-bootstrap"
import _ from 'lodash';
import { CSVLink, CSVDownload } from "react-csv";
import ReactTooltip from 'react-tooltip'; // For tool tip 
var path = cred.API_PATH;


class ContentManagement extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userId: '',
            userType: '',

            openSideMenu: false,
            searchString: '',
            searching: false,

            loading: false,

            pageList: [],

            selectedPageName: '',
            selectedpageNo: '',

        }
    }

    componentDidMount() {
        document.body.classList.remove("sign-bg");
        document.body.classList.add("grey-bg");

        let storage = localStorage.getItem("MIO_Local");
        storage = storage ? JSON.parse(storage) : null;

        // Page name -- 
        let pages = [{ name: "About Us", key: "aboutus" }, { name: "Contact Us", key: "contactus" }, 
                    { name: "FAQ's", key: "faq" }, { name: "Terms & Conditions", key: "termsconditions" }, 
                    { name: "Privacy Policy", key: "privacypolicy" }, {name: "Package Policy", key: "package"}];

        if (storage && storage.data.userId) {
            this.setState({
                userId: storage.data.userId,
                userType: storage.data.userType,
                companyId: storage.data.companyId,
                pageList: pages
            });

            // console.log(this.parseJwt(storage.token));
            let tokenData = this.parseJwt(storage.token);

        }


    }


    // For userList of a particular company - 
    openNewPage = (index) => {

        let { pageList } = this.state;
        let selectedpageNo = index;
        let page = pageList[index];
        let selectedPageKey = page.key;
        let selectedPageName = page.name;
        // this.props.history && this.props.history.push(`/admincompanyuser/${selectedCompanyId}`);

        this.props.history.push({
            pathname: '/admincontentmanagement/' + `${selectedPageKey}`,
            state: { name: selectedPageName }
        });

    }


    sideMenuToggle = () => {
        let { openSideMenu } = this.state;
        openSideMenu = !openSideMenu;
        this.setState({ openSideMenu });
    }

    mobMenuClick() {
        this.sideMenuToggle();
        console.log('mobMenuClick');
    }

    parseJwt = (token) => {
        if (!token) { return; }
        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace('-', '+').replace('_', '/');
        return JSON.parse(window.atob(base64));
    }


    render() {

        let { pageList } = this.state;
        return <main>
            <section className="user-mngnt-wrap">
                <div className="container-fluid">
                    <div className="row">

                        <Navbar routTo="/admincontentmanagement" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />
                        <div className="col-xl-10 col-lg-9 p-0 mob-full-conf">

                            <div className="mobile-menu-header">
                                {/* <a className="back-arw" onClick={() => console.log('<-BACK')}><i className="fas fa-arrow-left"></i></a> */}
                                <a className="menu-tgl" onClick={() => this.sideMenuToggle()}><img src="images/menu-tgl.png" alt="" /></a>
                            </div>

                            <div className="mng-full-list">

                                <div className="mng-full-srch">
                                    <div className="row">


                                    </div>
                                </div>

                                <div className="mng-full-table">
                                    <div className="row">
                                        <div className="col-md-10">
                                            <div className="mng-full-table-hdr">
                                                <div className="row">
                                                    <div className="col-md-12">
                                                        <h6>Page Name</h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-md-2">
                                            <div className="mng-full-table-hdr">
                                                <div className="row">
                                                    <div className="col-md-12">
                                                        <h6>Edit</h6>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    {pageList && pageList.length > 0 && pageList.map((page, index) => {

                                        return <div className="row" key={index}>
                                            <div className="col-md-10">
                                                <div className="mng-full-table-row">
                                                    <div className="row">
                                                        <div className="col-md-12">
                                                            <h6>Page Name</h6>
                                                            <p>
                                                                <a href="JavaScript:Void(0);" onClick={() => { this.openNewPage(index) }}>
                                                                    {page.name}
                                                                </a>
                                                            </p>
                                                        </div>
                                                        <div className="mobile-ad-edt-btns">
                                                            <ul>
                                                                <li><a href="JavaScript:Void(0);"
                                                                    onClick={() => { this.openNewPage(index) }}
                                                                >
                                                                    <i className="fas fa-eye"></i>
                                                                </a></li>

                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <div className="col-md-2">
                                                <div className="mng-full-table-row add-edt text-center">
                                                    <div className="row">
                                                        <div className="col-md-12">
                                                            <a href="JavaScript:Void(0);"
                                                                // ref={ref => this.fooRef = ref}
                                                                data-tip='View Page'
                                                                // onFocus={() => { ReactTooltip.show(this.fooRef) }}
                                                                onClick={() => { this.openNewPage(index); }}
                                                            >
                                                                <i className="fas fa-eye"></i>
                                                            </a>
                                                            <ReactTooltip
                                                                effect="float"
                                                                place="top"
                                                                data-border="true"
                                                            />
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    })}

                                </div>

                            </div>
                        </div>

                    </div>
                </div>

            </section>
        </main >

    }

}

export default ContentManagement;



