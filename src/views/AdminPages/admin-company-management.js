import React, { Component } from "react";
import Navbar from "../Component/Navbar";
import ProgressBar from 'react-bootstrap/ProgressBar';
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button, Row, Col } from "react-bootstrap"
import ToggleButton from 'react-bootstrap/ToggleButton'
import _ from 'lodash';
import { CSVLink, CSVDownload } from "react-csv";
import ReactTooltip from 'react-tooltip'; // For tool tip 

import { checkAuthentication } from '../Component/Authentication';

var path = cred.API_PATH;

export class AdminCompanyManagement extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      searchLoading: false,

      userId: '',

      openSideMenu: false,
      searchString: '',
      searchType: '',
      searching: false,

      companyLists: [],
      companyListsRecords: '',

      pageNo: 1,

      editUser: false,
      confirm: false,

      companyName: '',
      selectedCompanyType: '',

      noOfSeats: '0',
      pricePerSeat: '0.00',
      trialPeriod: '0',
      currency: '',
      isEditError: false,

      selectedCompanyId: '',
      selectedCompany: {},
      companyServices: [],
      updatedCompany: {},
      invoiceBillingInfo: '',
      vat: 0.0,
      voiceUrl: '',
      brandUrl: '',
      fileUploaded1: '',
      fileUploaded2: '',

      showSuccessAlert: false,
      successAlertMessage: '',
      showErrorAlert: false,
      errorAlertMessage: ''

    }
    this.perPage = 10;
  }

  componentDidMount() {
    document.body.classList.remove("sign-bg");
    document.body.classList.add("grey-bg");

    let storage = localStorage.getItem("MIO_Local");
    storage = storage ? JSON.parse(storage) : null;
    if (storage && storage.data.userId) {
      this.setState({
        userId: storage.data.userId,
        userType: storage.data.userType,
        companyId: storage.data.companyId
      }, () => {
        this.getCompanyListForAdmin()
        console.log('user management--', this.state.userType);
      });

      // console.log(this.parseJwt(storage.token));
      let tokenData = this.parseJwt(storage.token);

    }

  }

  getCompanyListForAdmin = () => {
    this.setState({ loading: true });
    let dataToSend = {
      "page": this.state.pageNo,
      "perPage": this.perPage
    };
    // API --
    axios
      .post(path + 'user/get-companies', dataToSend)
      .then(serverResponse => {
        let res = serverResponse.data;
        if (!res.isError) {
          this.setState({
            companyLists: res.details && res.details[0] && res.details[0].results,
            companyListsRecords: res.details && res.details[0] && res.details[0].noofpage,
            loading: false
          });
        } else {
          this.setState({
            loading: false
          })
        }
      })
      .catch(error => {
        console.log(error);
      })
  }

  searchCompany = (event) => {
    event.preventDefault();

    if (this.state.searchType == '' && this.state.searchString == '') {
      alert('Invalid search');
    } else {

      this.setState({
        searching: true,
        loading: true,
        searchLoading: true
      });

      let DataToSend = {
        companyType: this.state.searchType,
        companyName: this.state.searchString
      };
      // console.log("dataToSend - - ", DataToSend);
      // API -- 
      axios
        .post(path + 'user/search-companies', DataToSend)
        .then(serverResponse => {
          let res = serverResponse.data;
          if (!res.isError) {
            console.log("serverResponse for search -- ", res);
            if (res.details) {
              this.setState({
                searchLoading: false,
                companyLists: res.details,
                loading: false
              });
            } else {
              this.setState({
                searchLoading: false,
                loading: false
              })
            }
          } else {
            this.setState({
              searchLoading: false,
              loading: false
            })
            alert('Sorry! something is wrong.');
          }
        })
        .catch(error => {
          this.setState({
            searchLoading: false,
            loading: false
          })
          console.log(error);
        })

    }
  }

  clearSearch = (event) => {
    event.preventDefault();
    this.setState({
      searchString: '',
      searchType: '',
      searching: false,
      searchLoading: false
    }, () => {
      this.getCompanyListForAdmin()
    });
  }

  openEditUsers = (index) => {

    let selectedCompany = this.state.companyLists && this.state.companyLists[index];
    let selectedCompanyId = selectedCompany._id;
    let companyServices = selectedCompany && selectedCompany.services;

    this.setState({
      selectedCompany: selectedCompany,
      selectedCompanyId: selectedCompanyId,
      companyServices: companyServices,
      companyName: selectedCompany.companyName,
      selectedCompanyType: selectedCompany.companyType,
      noOfSeats: selectedCompany.noOfSeats,
      trialPeriod: selectedCompany.trialPeriod,
      pricePerSeat: selectedCompany.pricePerSeat,
      currency: selectedCompany.currency,
      editUser: true,
      invoiceBillingInfo: selectedCompany.invoiceBillingInfo,
      invoiceBillingAddress: selectedCompany.invoiceBillingAddress,
      vat: selectedCompany.vat,
      voiceUrl: selectedCompany.customVoiceUrl,
      brandUrl: selectedCompany.brandVoiceUrl,
      initialInvoiceNumber: selectedCompany.initialInvoiceNumber,
      currentInvoiceNumber: selectedCompany.currentInvoiceNumber
    });

  }

  onFileChange = (e) => {

    console.log("uploading file name is===", e.target.name);
    let fileName = e.target.name;
    let file;
    if (fileName == 'welcome') {
      file = this.refs.welcome.files[0];
    } else if (fileName == 'branding') {
      file = this.refs.branding.files[0];
    }

    if (file) {
      // console.log("uploaded local file = ", file);
      // For checking uploaded file extension -- 
      let blnValid = false;
      let sizeValid = false;
      //For checking  file size ---
      let sizeInByte = file.size;
      let sizeInKB = parseFloat(parseFloat(sizeInByte) / 1024).toFixed('2');
      if (sizeInKB <= 3100) {
        sizeValid = true;
      } else {
        sizeValid = false;
      }

      let sFileName = file.name;
      var _validFileExtensions = [".mp3", ".wav"];
      for (var j = 0; j < _validFileExtensions.length; j++) {
        var sCurExtension = _validFileExtensions[j];
        if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
          blnValid = true;
          break;
        }
      }

      if (!blnValid) {
        this.setState({
          showErrorAlert: true,
          errorAlertMessage: "File type should be .mp3"
        })
      } else if (!sizeValid) {
        this.setState({
          showErrorAlert: true,
          errorAlertMessage: "File size shouldn't be more than 3mb."
        });

      } else {
        this.setState({
          loading: true
        });
        var reader = new FileReader();
        var url = reader.readAsDataURL(file);
        // console.log("uploading file details == ", url);
        // reader.onloadend = function (e) {
        //     this.setState({
        //         voiceUrl: [reader.result]
        //     })
        // }.bind(this);

        const data = new FormData();
        // data.append('file', this.refs.file.files[0]);
        data.append('file', file);
        // data.append('companyId', this.state.companyId);

        // console.log("uploading payload for file == "+ data);

        axios
          .post(path + "services/upload-custom-voice",
            data,
            {
              headers: { 'Content-Type': 'multipart/form-data' },
              onUploadProgress: (progressEvent) => {
                // console.log("raw upload loader ---------", progressEvent.loaded, progressEvent.total);

                let uploadPercentage = parseInt(Math.round((progressEvent.loaded / progressEvent.total) * 100));
                if (fileName == 'welcome') {
                  this.setState({ fileUploaded1: uploadPercentage });
                } else if (fileName == 'branding') {
                  this.setState({ fileUploaded2: uploadPercentage });
                }

              }

            }
          )
          .then(serverResponse => {

            let res = serverResponse.data;
            if (!res.isError) {
              console.log("upload response ===", res);
              let voiceUrl = '';
              let brandUrl = '';

              if (fileName == 'welcome') {
                voiceUrl = res.detail && res.detail.url;

                this.setState({
                  voiceUrl: voiceUrl,
                  showSuccessAlert: true,
                  successAlertMessage: 'Successfully uploaded.',
                  fileUploaded1: '',
                  fileUploaded2: '',
                  loading: false
                }, () => {
                  console.log("after file upload....", this.state.brandUrl);
                })
              } else if (fileName == 'branding') {
                brandUrl = res.detail && res.detail.url;

                this.setState({
                  brandUrl: brandUrl,
                  showSuccessAlert: true,
                  successAlertMessage: 'Successfully uploaded.',
                  fileUploaded1: '',
                  fileUploaded2: '',
                  loading: false
                }, () => {
                  console.log("after file upload....", this.state.brandUrl);
                })
              }


            } else {
              this.setState({
                showErrorAlert: true,
                errorAlertMessage: 'Sorry! something wrong...',
                fileUploaded1: '',
                fileUploaded2: '',
                loading: false
              })
            }

          })
          .catch(error => {
            console.log(error);
          })

      }

    }
  }

  updateServices = (event, index) => {
    let fieldName = event.target.name;
    let fieldValue = event.target.value;
    // console.log("fieldName--", fieldName);

    let { companyServices } = this.state;
    let thisCompany = companyServices[index];
    thisCompany[`${fieldName}`] = fieldValue;

    let thisCostPerMinute = thisCompany.costPerMinute;

    let calculatedVal = '0.0';

    if (fieldName == "pricePerMinute") {

      calculatedVal = parseFloat(parseFloat(fieldValue) / parseFloat(thisCostPerMinute)).toFixed('2');
      thisCompany["multiplier"] = !isNaN(calculatedVal) ? calculatedVal : '0.0';

    } else if (fieldName == "multiplier") {

      calculatedVal = parseFloat(parseFloat(fieldValue) * parseFloat(thisCostPerMinute)).toFixed('2');
      thisCompany["pricePerMinute"] = !isNaN(calculatedVal) ? calculatedVal : '0.0';

    }

    companyServices[index] = thisCompany;

    if (thisCompany[`${fieldName}`] == '' || isNaN(thisCompany[`${fieldName}`])) {
      companyServices[index][`${fieldName}` + "Error"] = "Use Valid " + `${fieldName}`;
      this.setState({
        isEditError: true
      });
    } else {
      companyServices[index][`${fieldName}` + "Error"] = '';
      this.setState({
        isEditError: false
      });
    }

    this.setState({
      companyServices: companyServices
    });

  }

  editCompany = (event) => {

    let { isEditError, companyName, noOfSeats, pricePerSeat, currency, trialPeriod, voiceUrl, brandUrl, initialInvoiceNumber, currentInvoiceNumber } = this.state;
    companyName == '' ? this.setState({ companyNameError: "Use valid Company Name" }) : this.setState({ companyNameError: "" });
    noOfSeats == '' || isNaN(noOfSeats) ? this.setState({ noOfSeatsError: "Use valid no of seats" }) : this.setState({ noOfSeatsError: "" });
    pricePerSeat == '' || isNaN(pricePerSeat) ? this.setState({ pricePerSeatError: "Use valid price per seat" }) : this.setState({ pricePerSeatError: "" });
    currency == "" ? this.setState({ currencyError: "Use valid currency" }) : this.setState({ currencyError: "" });
    !currentInvoiceNumber && initialInvoiceNumber == "" ? this.setState({ initialInvoiceNumberError: "Use valid invoice no." }) : this.setState({ initialInvoiceNumberError: "" });

    if (!isEditError && noOfSeats != '' && !isNaN(noOfSeats) && pricePerSeat != '' && !isNaN(pricePerSeat) && currency != '' && companyName != '') {
      this.setState({
        loading: true
      });
      let DataToSend = {
        companyId: this.state.selectedCompanyId,
        companyName: this.state.companyName,
        companyType: this.state.selectedCompany.companyType,
        noOfSeats: this.state.noOfSeats,
        pricePerSeat: this.state.pricePerSeat,
        currency: this.state.currency,
        trialPeriod: trialPeriod,
        services: this.state.companyServices,
        invoiceBillingInfo: this.state.invoiceBillingInfo,
        invoiceBillingAddress: this.state.invoiceBillingAddress,
        vat: this.state.vat,
        customVoiceUrl: voiceUrl,
        brandVoiceUrl: brandUrl,
        initialInvoiceNumber: initialInvoiceNumber
      };
      console.log("DataToSend--", DataToSend);

      // API -- 
      axios
        .post(path + 'user/update-company-details', DataToSend)
        .then(serverResponse => {
          let res = serverResponse.data;
          if (!res.isError) {
            this.setState({
              showSuccessAlert: true,
              successAlertMessage: "Successfully Updated"
            }, () => {
              this.getCompanyListForAdmin();
              this.clearState();
            });

          } else {
            this.setState({
              showErrorAlert: true,
              errorAlertMessage: "Sorry! Try again."
            }, () => {
              this.clearState();
            });

          }
        })

    }

  }

  pageChangePrev = () => {

    this.setState({
      pageNo: (parseFloat(this.state.pageNo) - 1)
    }, () => {
      this.getCompanyListForAdmin()
    });

  }

  pageChangeNext = () => {

    this.setState({
      pageNo: (parseFloat(this.state.pageNo) + 1)
    }, () => {
      this.getCompanyListForAdmin()
    });

  }

  changeStatusConfirm = (index) => {

    let selectedCompany = this.state.companyLists && this.state.companyLists[index];
    let selectedCompanyName = selectedCompany.companyName;

    this.setState({
      selectedCompany: selectedCompany,
      companyName: selectedCompanyName,
      confirm: true
    });

  }

  changeStatus = (event) => {

    let status = '';
    let currentStatus = this.state.selectedCompany && this.state.selectedCompany.status;
    currentStatus && currentStatus == 'ACTIVE' ? status = 'IN_ACTIVE' : status = 'ACTIVE';

    this.setState({
      loading: true
    });

    let dataToSend = {
      companyId: this.state.selectedCompany._id,
      status: status
    };
    // API -- 
    axios
      .post(path + 'user/change-company-status', dataToSend)
      .then(serverResponse => {
        let res = serverResponse.data;
        if (!res.isError) {
          this.setState({
            loading: false,
            selectedCompany: '',
            companyName: '',
            confirm: false,
            showSuccessAlert: true,
            successAlertMessage: "Successfully Updated"
          }, () => {
            // this.getCompanyListForAdmin();
          })
        } else {
          this.setState({
            loading: false,
            selectedCompany: '',
            companyName: '',
            confirm: false,
            showErrorAlert: true,
            errorAlertMessage: "Sorry! Try again."
          });
        }
      })
      .catch(error => {
        console.log(error);
      })

  }

  clearState = () => {
    this.setState({
      loading: false,
      editUser: false,
      confirm: false,
      showSuccessAlert: false,
      showErrorAlert: false,
      successAlertMessage: '',
      errorAlertMessage: '',
      selectedCompany: {},
      selectedCompanyId: '',
      companyServices: [],
      companyName: '',
      noOfSeats: '',
      pricePerSeat: '',
      currency: '',
      searchString: '',
      searchType: '',
      searching: false,
      invoiceBillingInfo: '',
      voiceUrl: '',
      brandUrl: '',
      fileUploaded: '',
      vat: 0.0,
    })

  }

  onAlertHide() {
    this.setState({
      showSuccessAlert: false,
      showErrorAlert: false,
      successAlertMessage: '',
      errorAlertMessage: '',
    });
  }

  sideMenuToggle() {
    let { openSideMenu } = this.state;
    openSideMenu = !openSideMenu;
    this.setState({ openSideMenu });
  }

  mobMenuClick() {
    this.sideMenuToggle();
    console.log('mobMenuClick');
  }

  parseJwt = (token) => {
    if (!token) { return; }
    const base64Url = token.split('.')[1];
    const base64 = base64Url.replace('-', '+').replace('_', '/');
    return JSON.parse(window.atob(base64));
  }


  render() {

    let { loading, searchLoading, searchType, searchString, searching, pageNo, companyListsRecords, companyName,
      noOfSeats,
      pricePerSeat,
      trialPeriod,
      companyServices,
      companyLists,
      selectedCompanyType,
      invoiceBillingInfo,
      invoiceBillingAddress,
      vat,
      voiceUrl,
      brandUrl,
      initialInvoiceNumber,
      currentInvoiceNumber
    } = this.state;

    return (
      <main>

        <section className="user-mngnt-wrap">
          <div className="container-fluid">
            <div className="row">
              <Navbar routTo="/admincompanymanagement" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />
              <div className="col-xl-10 col-lg-9 p-0">
                <div className="mobile-menu-header">

                  <a href="JavaScript:Void(0);" className="menu-tgl" onClick={() => this.sideMenuToggle()}>
                    <img src="/images/menu-tgl.png" alt="" />
                  </a>
                </div>
                <div className="mng-full-list">
                  <div className="mng-full-srch">

                    <div className="row">
                      <div className="col-margin col-md-4">
                        <select className="form-control"
                          name="searchType"
                          onChange={(event) => { this.setState({ searchType: event.target.value }) }}
                        >
                          <option value='' selected={searchType == '' ? true : false}>Select Company Type</option>
                          <option value={`${cred.COMPANY_TYPE_FREE_TYPE}`} selected={searchType == cred.COMPANY_TYPE_FREE_TYPE ? true : false}>
                            {cred.COMPANY_TYPE_FREE_TYPE}
                          </option>
                          <option value={`${cred.COMPANY_TYPE_STARTUP}`} selected={searchType == cred.COMPANY_TYPE_STARTUP ? true : false}>
                            {cred.COMPANY_TYPE_STARTUP}
                          </option>
                          <option value={`${cred.COMPANY_TYPE_ENTERPRISE}`} selected={searchType == cred.COMPANY_TYPE_ENTERPRISE ? true : false}>
                            {cred.COMPANY_TYPE_ENTERPRISE}
                          </option>
                        </select>
                      </div>
                      <div className="col col-md-8">
                        <div className="srch-wrap">
                          <form>
                            <input
                              type="text"
                              name="searchString"
                              value={this.state.searchString}
                              onChange={(event) => { this.setState({ searchString: event.target.value }) }}
                              className="form-control"
                              placeholder="Search by company name"
                            />
                            {this.state.searching ?
                              <input type="button" onClick={(event) => this.clearSearch(event)}
                                data-tip='Clear'
                              />
                              :
                              <input type="submit" onClick={(event) => this.searchCompany(event)}
                                data-tip='Search'
                              />
                            }
                            <ReactTooltip
                              effect="float"
                              place="top"
                              data-border="true"
                            />
                          </form>
                        </div>
                      </div>
                    </div>
                    {searchLoading ?
                      <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                        <i className="fa fa-spinner fa-spin" ></i>
                      </a>
                      : null}
                  </div>
                  {companyLists && companyLists.length > 0 ?
                    <div className="mng-full-table company-mangmnt">

                      <div className="row">

                        <div className="col-md-8">
                          <div className="mng-full-table-hdr">
                            <div className="row">
                              <div className="col-md-4 border-rt">
                                <h6>Company</h6>
                              </div>
                              <div className="col-md-4 border-rt">
                                <h6>Type</h6>
                              </div>
                              <div className="col-md-4">
                                <h6>Currency</h6>
                              </div>
                              {/* <div className="col-md-3">
                              <h6>Introduction</h6>
                            </div> */}
                            </div>
                          </div>
                        </div>
                        <div className="col-md-4">
                          <div className="mng-full-table-hdr">
                            <div className="row">
                              <div className="col-md-6 border-rt">
                                <h6>Edit</h6>
                              </div>
                              <div className="col-md-6">
                                <h6>Status</h6>
                              </div>
                            </div>
                          </div>
                        </div>

                      </div>

                      {companyLists && companyLists.length > 0 ? companyLists.map((data, index) => {

                        return <div className="row" key={index}>
                          <div className="col-md-8">

                            <div className="mng-full-table-row">
                              <div className="row">
                                <div className="col-md-4 border-rt">
                                  <h6>Company</h6>
                                  <p className="textEllips">
                                    <a href="JavaScript:Void(0);" onClick={() => this.openEditUsers(index)}>
                                      {data.companyName}
                                    </a>
                                  </p>

                                </div>
                                <div className="col-md-4 border-rt">
                                  <h6>Type</h6>
                                  <p>{data.companyType}</p>
                                </div>
                                <div className="col-md-4">
                                  <h6>Currency</h6>
                                  <p>{data.currency}</p>
                                </div>

                                <div className="mobile-ad-edt-btns">
                                  <ul>
                                    <li onClick={() => this.openEditUsers(index)}>
                                      <a href="JavaScript:Void(0);"
                                        // ref={ref => this.fooRef = ref}
                                        data-tip='Edit'
                                      // onFocus={() => { ReactTooltip.show(this.fooRef) }}
                                      >
                                        <i className="far fa-edit"></i>

                                      </a>
                                      <ReactTooltip
                                        effect="float"
                                        place="top"
                                        data-border="true"
                                      />
                                    </li>

                                    <li onClick={() => this.changeStatusConfirm(index)}>
                                      <a href="JavaScript:Void(0);"
                                        // ref={ref => this.fooRef = ref}
                                        data-tip={`${data.status}`}
                                      // onFocus={() => { ReactTooltip.show(this.fooRef) }}
                                      >

                                        {data.status == "ACTIVE" ?
                                          <span><i className="fas fa-toggle-on"></i></span>
                                          :
                                          null}
                                        {data.status == "IN_ACTIVE" ?
                                          <span><i className="fas fa-toggle-off"></i></span>
                                          :
                                          null}

                                      </a>
                                      <ReactTooltip
                                        effect="float"
                                        place="top"
                                        data-border="true"
                                      />

                                    </li>

                                  </ul>
                                </div>

                              </div>
                            </div>
                          </div>

                          <div className="col-md-4">
                            <div className="mng-full-table-row add-edt text-center">
                              <div className="row">

                                <div className="col-md-6">
                                  <a href="JavaScript:Void(0);"
                                    // ref={ref => this.fooRef2 = ref}
                                    data-tip='Edit'
                                    onClick={() => { this.openEditUsers(index); }}
                                  // onFocus={() => { ReactTooltip.show(this.fooRef2) }}
                                  >
                                    <i className="far fa-edit"></i>

                                  </a>
                                  <ReactTooltip
                                    effect="float"
                                    place="top"
                                    data-border="true"
                                  />
                                </div>

                                <div className="col-md-6">
                                  <a href="JavaScript:Void(0);"
                                    // ref={ref => this.fooRef3 = ref}
                                    data-tip={`${data.status}`}
                                    onClick={() => { this.changeStatusConfirm(index) }}
                                  // onFocus={() => { ReactTooltip.show(this.fooRef3) }}
                                  >

                                    {data.status == "ACTIVE" ?
                                      <span><i className="fas fa-toggle-on" /></span>
                                      :
                                      null}
                                    {data.status == "IN_ACTIVE" ?
                                      <span><i className="fas fa-toggle-off" /></span>
                                      :
                                      null}

                                  </a>

                                  <ReactTooltip
                                    effect="float"
                                    place="top"
                                    data-border="true"
                                  />
                                </div>

                              </div>
                            </div>
                          </div>

                        </div>

                      })
                        : null}

                    </div>
                    :
                    <div className="mng-full-table">
                      <div className="row">
                        <div className="col-md-12">
                          <div className="mng-full-table-hdr admn-usr-hdr">
                            <div className="row">
                              <div className="col-md-12">
                                <h6>No Company Found.</h6>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  }

                  <br /><br />
                  {!searching ?
                    <div className="form-group">
                      {!loading && parseFloat(pageNo) > 1 ?
                        <span style={{ color: "#007bff", cursor: "pointer" }}
                          onClick={() => { this.pageChangePrev() }}
                        >
                          <b>{"<< Prev"}</b>
                        </span>
                        :
                        null}
                        &nbsp;&nbsp;|&nbsp;&nbsp;
                        {!loading && (parseFloat(pageNo) < parseFloat(companyListsRecords)) ?
                        <span style={{ color: "#007bff", cursor: "pointer" }}
                          onClick={() => { this.pageChangeNext() }}
                        >
                          <b>{"Next >>"}</b>
                        </span>
                        :
                        null}
                      {loading ?
                        <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                          <i className="fa fa-spinner fa-spin" ></i>
                        </a>
                        :
                        null}

                    </div>
                    :
                    null}

                </div>

              </div>
            </div>
          </div>
        </section>

        <Modal show={this.state.editUser} size="lg" onHide={() => { this.clearState() }}>
          <Modal.Header closeButton>
            <Modal.Title> Edit Company</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {/* {companyLists && companyLists.map((company, index)=>{ */}
            <div className="row">

              <div className="col-md-12">
                <div className="form-group">
                  <label>Company Name:</label>
                  <input
                    type="text"
                    name="companyName"
                    value={companyName}
                    className="form-control"
                    placeholder="Enter Company Name"
                    onChange={event => { this.setState({ companyName: event.target.value }) }}
                    onFocus={() => this.setState({ companyNameError: "" })}
                  />
                  <span style={{ color: 'red' }}>{this.state.companyNameError ? `* ${this.state.companyNameError}` : ''}</span>
                </div>
              </div>

              {companyServices && companyServices.length > 0 ? companyServices.map((service, index) => {
                return <React.Fragment key={index}>
                  <div className="col-md-12">
                    <div className="form-group">
                      <label><b>Cost settings for {service.serviceName}:</b></label>
                    </div>
                  </div>

                  <div className="col-md-4">
                    <div className="form-group">
                      <label>Cost Per Minute:</label>
                      <input
                        type="text"
                        name="costPerMinute"
                        value={service.costPerMinute}
                        className="form-control"
                        placeholder="Enter Cost Per Minute"
                        onChange={event => { this.updateServices(event, index) }}
                        onFocus={() => this.setState({ costPerMinError: "" })}
                      // readOnly={true}
                      />
                      {/* <span style={{ color: 'red' }}>{this.state.costPerMinError ? `* ${this.state.costPerMinError}` : ''}</span> */}
                    </div>
                  </div>

                  <div className="col-md-4">
                    <div className="form-group">
                      <label>Multiplier:</label>
                      <input
                        type="text"
                        name="multiplier"
                        value={service.multiplier}
                        className="form-control"
                        placeholder="Enter multiplier"
                        onChange={event => { this.updateServices(event, index) }}
                        onFocus={() => this.setState({ multiplierError: "" })}
                      />
                      <span style={{ color: 'red' }}>{service.multiplierError ? `* ${service.multiplierError}` : ''}</span>
                    </div>
                  </div>

                  <div className="col-md-4">
                    <div className="form-group">
                      <label>Price Per Minute:</label>
                      <input
                        type="text"
                        name="pricePerMinute"
                        value={service.pricePerMinute}
                        className="form-control"
                        placeholder="Enter Price Per Minute"
                        onChange={event => { this.updateServices(event, index) }}
                        onFocus={() => this.setState({ pricePerMinuteError: "" })}
                      />
                      <span style={{ color: 'red' }}>{service.pricePerMinuteError ? `* ${service.pricePerMinuteError}` : ''}</span>
                    </div>
                  </div>

                </React.Fragment>
              })
                : null}


              <div className="col-md-6">
                <div className="form-group">
                  <label>No of Seats:</label>
                  <input
                    type="text"
                    name="noOfSeats"
                    value={noOfSeats}
                    className="form-control"
                    placeholder="Enter no of Seat"
                    onChange={event => { this.setState({ noOfSeats: event.target.value }) }}
                    onFocus={() => this.setState({ noOfSeatsError: "" })}
                  />
                  <span style={{ color: 'red' }}>{this.state.noOfSeatsError ? `* ${this.state.noOfSeatsError}` : ''}</span>
                </div>
              </div>

              {selectedCompanyType && selectedCompanyType == "FREE_TYPE" ?

                <div className="col-md-6">
                  <div className="form-group">
                    <label>Trial Period(days):</label>
                    <input
                      type="text"
                      name="trialPeriod"
                      value={trialPeriod}
                      className="form-control"
                      placeholder="Enter Trial Period"
                      onChange={event => this.setState({ trialPeriod: event.target.value })}
                      onFocus={() => this.setState({ pricePerSeatError: "" })}
                    />
                    {/* <span style={{ color: 'red' }}>{this.state.pricePerSeatError ? `* ${this.state.pricePerSeatError}` : ''}</span> */}
                  </div>
                </div>

                : null}

              <div className="col-md-6">
                <div className="form-group">
                  <label>Price Per Seat:</label>
                  <input
                    type="text"
                    name="pricePerSeat"
                    value={pricePerSeat}
                    className="form-control"
                    placeholder="Enter Price Per Seat"
                    onChange={event => this.setState({ pricePerSeat: event.target.value })}
                    onFocus={() => this.setState({ pricePerSeatError: "" })}
                  />
                  <span style={{ color: 'red' }}>{this.state.pricePerSeatError ? `* ${this.state.pricePerSeatError}` : ''}</span>
                </div>
              </div>

              {/* <div className="col-md-6">
                <div className="form-group">
                  <label>Currency:</label>
                  <select className="form-control"
                    name="currency"
                    value={currency}
                    onChange={(event) => { this.setState({ currency: event.target.value }) }}
                    onClick={() => { this.setState({ currencyError: '' }) }}
                  >
                    <option value="">Select Currency</option>
                    <option value={cred.CURRENCY.usd}>{cred.CURRENCY.usd}</option>
                    <option value={cred.CURRENCY.gbp}>{cred.CURRENCY.gbp}</option>
                    <option value={cred.CURRENCY.naira}>{cred.CURRENCY.naira}</option>
                    <option value={cred.CURRENCY.euro}>{cred.CURRENCY.euro}</option>
                  </select>
                  <span style={{ color: 'red' }}>{this.state.currencyError ? `* ${this.state.currencyError}` : ''}</span>
                </div>
              </div> */}

              <div className="col-md-6">
                <div className="form-group">
                  <label>VAT:(%)</label>
                  <input
                    type="text"
                    name="vat"
                    value={vat}
                    className="form-control"
                    placeholder="Enter VAT"
                    onChange={event => this.setState({ vat: event.target.value })}
                    onFocus={() => this.setState({ vatError: "" })}
                  />
                  <span style={{ color: 'red' }}>{this.state.vatError ? `* ${this.state.vatError}` : ''}</span>
                </div>
              </div>

              {selectedCompanyType && selectedCompanyType == cred.COMPANY_TYPE_ENTERPRISE ?
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Initial Invoice No.</label>
                    <input
                      type="text"
                      name="initialInvoiceNumber"
                      value={initialInvoiceNumber}
                      className="form-control"
                      placeholder="Enter invoice no."
                      onChange={event => this.setState({ initialInvoiceNumber: event.target.value })}
                      onFocus={() => this.setState({ initialInvoiceNumberError: "" })}
                      readOnly={currentInvoiceNumber ? true : false}
                    />
                    <span style={{ color: 'red' }}>{this.state.initialInvoiceNumberError ? `* ${this.state.initialInvoiceNumberError}` : ''}</span>
                  </div>
                </div>

                : null}

              <div className="col-md-12">
                <div className="form-group">
                  <label>Welcome Tune: {voiceUrl}</label>
                </div>
              </div>

              <div className="col-md-6">
                <div className="form-group">
                  <label>Upload welcome tune(MP3/WAV)</label>
                  <input
                    type="file" name="welcome"
                    ref="welcome"
                    className="form-control"
                    onChange={(e) => { this.onFileChange(e) }} multiple={false}
                  // style= {{background: "#0083C6", color: "#fff"}}
                  />
                  {this.state.fileUploaded1 ?
                    <ProgressBar striped variant="success" now={this.state.fileUploaded1} label={`${this.state.fileUploaded1}%`} />
                    :
                    null}
                </div>
              </div>

              <div className="col-md-6">
                {voiceUrl && <audio controls style={{ width: '100%', border: 'solid #0092DD 1px', borderRadius: '30px', outline: 'none', marginTop: '20px', height: '45px' }}>
                  <source src={voiceUrl} type="audio/mpeg" />
                  {voiceUrl}
                </audio>
                }
              </div>

              {/* <div className="col-md-12">
                <div className="form-group">
                  <label>Branding Voice: {brandUrl}</label>
                </div>
              </div> */}

              {/* <div className="col-md-12">
                <div className="form-group">
                  <label>Upload branding tune(MP3/WAV)</label>
                  <input
                    type="file" name="branding"
                    ref="branding"
                    className="form-control"
                    onChange={(e) => { this.onFileChange(e) }} multiple={false}
                  // style= {{background: "#0083C6", color: "#fff"}}
                  />
                  {this.state.fileUploaded2 ?
                    <ProgressBar striped variant="success" now={this.state.fileUploaded2} label={`${this.state.fileUploaded2}%`} />
                    :
                    null}
                </div>
              </div> */}

              <div className="col-md-12">
                <div className="form-group">
                  <label>Invoice Billing Info</label>
                  <textarea
                    type="text"
                    rows="5"
                    name="info"
                    value={invoiceBillingInfo}
                    className="form-control"
                    placeholder="Enter Invoice Billing Info"
                    onChange={event => this.setState({ invoiceBillingInfo: event.target.value })}
                    style={{ height: '150px' }}
                  // onFocus={() => this.setState({ vatError: "" })}
                  />
                  {/* <span style={{ color: 'red' }}>{this.state.vatError ? `* ${this.state.vatError}` : ''}</span> */}
                </div>
              </div>

              <div className="col-md-12">
                <div className="form-group">
                  <label>Invoice Billing Address</label>
                  <textarea
                    type="text"
                    rows="5"
                    name="address"
                    value={invoiceBillingAddress}
                    className="form-control"
                    placeholder="Enter Invoice Billing address"
                    onChange={event => this.setState({ invoiceBillingAddress: event.target.value })}
                    style={{ height: '150px' }}
                  // onFocus={() => this.setState({ vatError: "" })}
                  />
                  {/* <span style={{ color: 'red' }}>{this.state.vatError ? `* ${this.state.vatError}` : ''}</span> */}
                </div>
              </div>


            </div>
            {/* })
            :<div></div>} */}
          </Modal.Body>
          <Modal.Footer>

            <div className="row">
              <div className="col-md-6">
                <div className="form-group" style={{ marginRight: "25px" }}>
                  {loading ?
                    <Button variant="primary"> <i className="fa fa-spinner fa-spin" ></i> </Button>
                    :
                    null}
                  {!loading ?
                    <Button variant="primary" onClick={(event) => this.editCompany(event)}> Save </Button>
                    :
                    null}
                </div>
              </div>
              <div className="col-md-6">

              </div>
            </div>

          </Modal.Footer>
        </Modal>

        <Modal show={this.state.confirm} onHide={() => { this.clearState() }}>
          <Modal.Header closeButton>
            <Modal.Title>Change status</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h3>Are you sure?</h3>
            <p>Changing status of {this.state.companyName}</p>
          </Modal.Body>
          <Modal.Footer>
            {!loading ?
              <Button variant="secondary" onClick={(event) => this.changeStatus(event)}>
                Yes
              </Button>
              :
              null}
            {loading ?
              <Button variant="secondary" >
                <i class="fas fa-spinner fa-spin"></i>
              </Button>
              :
              null}
            <Button variant="primary" onClick={() => { this.clearState() }}>
              No
            </Button>
          </Modal.Footer>
        </Modal>

        <Modal show={this.state.showSuccessAlert} onHide={() => this.onAlertHide()}>
          <Modal.Header closeButton>
            <Modal.Title>OK</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h5 style={{ color: 'green' }}>{`${this.state.successAlertMessage}`}</h5>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="primary" onClick={() => this.onAlertHide()}>
              Ok
            </Button>
          </Modal.Footer>
        </Modal>

        <Modal show={this.state.showErrorAlert} onHide={() => this.onAlertHide()}>
          <Modal.Header closeButton>
            <Modal.Title>OK</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h5 style={{ color: 'red' }}>{`${this.state.errorAlertMessage}`}</h5>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="primary" onClick={() => this.onAlertHide()}>
              Ok
            </Button>
          </Modal.Footer>
        </Modal>


      </main>
    );
  }
}

export default checkAuthentication(AdminCompanyManagement);
