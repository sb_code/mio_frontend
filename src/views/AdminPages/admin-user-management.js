import React, { Component } from 'react'
import Navbar from '../Component/Navbar'
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button } from "react-bootstrap"
import _ from 'lodash';
import { CSVLink, CSVDownload } from "react-csv";
import ReactTooltip from 'react-tooltip'; // For tool tip 

import { checkAuthentication } from '../Component/Authentication';

var path = cred.API_PATH;


export class AdminUserManagement extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userId: '',
            userType: '',

            openSideMenu: false,
            searchString: '',
            searching: false,

            loading: false,
            searchLoading: false,

            usersList: [],
            userListRecords: '',

            adminCompanyId: '',
            adminCompanyName: '',
            adminCompanyType: '',

            pageNo: 1,
            perPage: 10,
            totalRows: '',

        }
        this.perPage = 10;
    }

    componentDidMount() {
        document.body.classList.remove("sign-bg");
        document.body.classList.add("grey-bg");

        let storage = localStorage.getItem("MIO_Local");
        storage = storage ? JSON.parse(storage) : null;
        if (storage && storage.data.userId) {
            this.setState({
                userId: storage.data.userId,
                userType: storage.data.userType,
                companyId: storage.data.companyId,
                adminCompanyId: storage.data.companyId,
                adminCompanyName: storage.data.companyName,
                adminCompanyType: storage.data.companyType
            }, () => {
                this.getUserListForAdmin()
                console.log('user management--', this.state.userType);
            });

            // console.log(this.parseJwt(storage.token));
            let tokenData = this.parseJwt(storage.token);

        }


    }

    // For getting user list -- 
    getUserListForAdmin = () => {

        this.setState({ loading: true });
        let dataToSend = {
            "page": this.state.pageNo,
            "perPage": this.perPage
        };
        // API --
        axios
            .post(path + 'user/get-companies', dataToSend)
            .then(serverResponse => {
                let res = serverResponse.data;
                if (!res.isError) {
                    this.setState({
                        usersList: res.details && res.details[0] && res.details[0].results,
                        userListRecords: res.details && res.details[0] && res.details[0].noofpage,
                        loading: false
                    });
                } else {
                    this.setState({
                        loading: false
                    })
                    // console.log('something goes wrong.');
                }
            })
            .catch(error => {
                console.log(error);
            })

    }

    // For searching user - 
    searchUsers = (event) => {
        event.preventDefault();
        if (this.state.searchString == '' || (this.state.searchString && this.state.searchString.length < '2')) {
            alert('Invalid search');
        } else {
            let dataToSend = {
                companyType: '',
                companyName: this.state.searchString
            }
            // API ToDo - 
            this.setState({
                searching: true,
                loading: true,
                searchLoading: true
            })

            axios
                .post(path + 'user/search-companies', dataToSend)
                .then(serverResponse => {

                    let res = serverResponse.data;
                    if (!res.isError) {
                        this.setState({
                            usersList: res.details,
                            loading: false,
                            searchLoading: false
                        });
                    } else {
                        this.setState({
                            loading: false,
                            searchLoading: false
                        });
                        alert('Sorry! something is wrong.');
                    }

                })
                .catch(error => {
                    this.setState({
                        loading: false,
                        searchLoading: false
                    });
                    console.log(error);
                })

        }
    }

    // For clearing search -
    clearSearch = (event) => {
        event.preventDefault();
        this.setState({
            searchString: '',
            searching: false,
            searchLoading: false
        }, () => {
            this.getUserListForAdmin()
        })
    }

    // For userList of a particular company - 
    openViewUsers = (index, name) => {

        if (name && name == "admin") {

            this.props.history.push({
                pathname: '/admincompanyuser/' + `${index}`,
                state: { name: this.state.adminCompanyName, type: this.state.adminCompanyType }
            });

        } else {
            let selectedCompanyId = this.state.usersList && this.state.usersList[index] && this.state.usersList[index]._id;
            let selectedCompanyname = this.state.usersList && this.state.usersList[index] && this.state.usersList[index].companyName;
            let selectedCompanyType = this.state.usersList && this.state.usersList[index] && this.state.usersList[index].companyType;
            // linking to another url to get user's details of a company in list -- 

            // this.props.history && this.props.history.push(`/admincompanyuser/${selectedCompanyId}`);
            this.props.history.push({
                pathname: '/admincompanyuser/' + `${selectedCompanyId}`,
                state: { name: selectedCompanyname, type: selectedCompanyType }
            });
        }

    }

    pageChangePrev = () => {

        this.setState({
            pageNo: (parseFloat(this.state.pageNo) - 1)
        }, () => {
            this.getUserListForAdmin()
        });

    }

    pageChangeNext = () => {

        this.setState({
            pageNo: (parseFloat(this.state.pageNo) + 1)
        }, () => {
            this.getUserListForAdmin()
        });

    }

    sideMenuToggle = () => {
        let { openSideMenu } = this.state;
        openSideMenu = !openSideMenu;
        this.setState({ openSideMenu });
    }

    mobMenuClick() {
        this.sideMenuToggle();
        console.log('mobMenuClick');
    }

    parseJwt = (token) => {
        if (!token) { return; }
        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace('-', '+').replace('_', '/');
        return JSON.parse(window.atob(base64));
    }


    render() {
        let { loading, searchLoading, searching, pageNo, userListRecords } = this.state;
        return <main>
            <section className="user-mngnt-wrap">
                <div className="container-fluid">
                    <div className="row">

                        <Navbar routTo="/adminusermanagement" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />
                        <div className="col-xl-10 col-lg-9 p-0 mob-full-conf">

                            <div className="mobile-menu-header">
                                {/* <a className="back-arw" onClick={() => console.log('<-BACK')}><i className="fas fa-arrow-left"></i></a> */}
                                <a href="JavaScript:Void(0);" className="menu-tgl" onClick={() => this.sideMenuToggle()}><img src="images/menu-tgl.png" alt="" /></a>
                            </div>

                            <div className="mng-full-list">

                                <div className="mng-full-srch">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="srch-wrap">
                                                <form>
                                                    <input value={this.state.searchString} onChange={(event) => this.setState({ searchString: event.target.value })} type="text" name="" className="form-control" placeholder="Search Company" />
                                                    {this.state.searching ?
                                                        <input type="button" onClick={(event) => this.clearSearch(event)}
                                                            data-tip='Clear'
                                                        />
                                                        :
                                                        <input type="submit" onClick={(event) => this.searchUsers(event)}
                                                            data-tip='Search'
                                                        />
                                                    }
                                                    <ReactTooltip
                                                        effect="float"
                                                        place="top"
                                                        data-border="true"
                                                    />
                                                </form>
                                            </div>
                                        </div>
                                        {searchLoading ?
                                            <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                                                <i className="fa fa-spinner fa-spin" ></i>
                                            </a>
                                            : null}

                                    </div>
                                </div>

                                {this.state.usersList && this.state.usersList.length > 0 ?
                                    <div className="mng-full-table">
                                        <div className="row">

                                            <div className="col-md-10">
                                                <div className="mng-full-table-hdr">
                                                    <div className="row">
                                                        <div className="col-md-4 border-rt">
                                                            <h6>Name/Company</h6>
                                                        </div>
                                                        <div className="col-md-4 border-rt">
                                                            <h6>Type</h6>
                                                        </div>
                                                        <div className="col-md-4">
                                                            <h6>Currency</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="col-md-2">
                                                <div className="mng-full-table-hdr">
                                                    <div className="row">
                                                        <div className="col-md-12">
                                                            <h6>View</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div className="row">

                                            <div className="col-md-10">
                                                <div className="mng-full-table-row">
                                                    <div className="row">
                                                        <div className="col-md-4 border-rt">
                                                            <h6>Name/Company</h6>
                                                            <p className="textEllips">
                                                                <a href="JavaScript:Void(0);" onClick={() => this.openViewUsers(this.state.adminCompanyId, "admin")}>
                                                                    {this.state.adminCompanyName}
                                                                </a>
                                                            </p>
                                                        </div>
                                                        <div className="col-md-4 border-rt">
                                                            <h6>Type</h6>
                                                            <p className="textEllips">{this.state.adminCompanyType}</p>
                                                        </div>
                                                        <div className="col-md-4">

                                                            <h6>Currency</h6>
                                                            <p className="textEllips">
                                                                -
                                                                </p>

                                                        </div>

                                                        <div className="mobile-ad-edt-btns">
                                                            <ul>
                                                                <li onClick={() => this.openViewUsers(this.state.adminCompanyId, "admin")}>
                                                                    <a href="JavaScript:Void(0);"
                                                                        data-tip='View'
                                                                    >
                                                                        <i className="fas fa-eye"></i>
                                                                    </a>
                                                                    <ReactTooltip
                                                                        effect="float"
                                                                        place="top"
                                                                        data-border="true"
                                                                    />
                                                                </li>

                                                            </ul>
                                                        </div>

                                                    </div>
                                                </div>

                                            </div>

                                            <div className="col-md-2">
                                                <div className="mng-full-table-row add-edt text-center">
                                                    <div className="row">
                                                        <div className="col-md-12">
                                                            <a href="JavaScript:Void(0);"
                                                                data-tip='View'
                                                                onClick={() => { this.openViewUsers(this.state.adminCompanyId, "admin") }}
                                                            >
                                                                <i className="fas fa-eye"></i>

                                                            </a>
                                                            <ReactTooltip
                                                                effect="float"
                                                                place="top"
                                                                data-border="true"
                                                            />
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        {this.state.usersList && this.state.usersList.length > 0 && this.state.usersList.map((user, index) => {
                                            return <div className="row" key={index}>

                                                <div className="col-md-10">
                                                    <div className="mng-full-table-row">
                                                        <div className="row">
                                                            <div className="col-md-4 border-rt">
                                                                <h6>Name/Company</h6>
                                                                <p className="textEllips">
                                                                    <a href="JavaScript:Void(0);" onClick={() => this.openViewUsers(index)}>
                                                                        {user.companyName}
                                                                    </a>
                                                                </p>
                                                            </div>
                                                            <div className="col-md-4 border-rt">
                                                                <h6>Type</h6>
                                                                <p className="textEllips">{user.companyType}</p>
                                                            </div>
                                                            <div className="col-md-4">

                                                                <h6>Currency</h6>
                                                                <p className="textEllips">
                                                                    {user.currency}
                                                                </p>

                                                            </div>

                                                            <div className="mobile-ad-edt-btns">
                                                                <ul>
                                                                    <li onClick={() => this.openViewUsers(index)}>
                                                                        <a href="JavaScript:Void(0);"
                                                                            data-tip='View'
                                                                        >
                                                                            <i className="fas fa-eye"></i>
                                                                        </a>
                                                                        <ReactTooltip
                                                                            effect="float"
                                                                            place="top"
                                                                            data-border="true"
                                                                        />
                                                                    </li>

                                                                </ul>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>

                                                <div className="col-md-2">
                                                    <div className="mng-full-table-row add-edt text-center">
                                                        <div className="row">
                                                            <div className="col-md-12">
                                                                <a href="JavaScript:Void(0);"
                                                                    data-tip='View'
                                                                    onClick={() => { this.openViewUsers(index) }}
                                                                >
                                                                    <i className="fas fa-eye"></i>

                                                                </a>
                                                                <ReactTooltip
                                                                    effect="float"
                                                                    place="top"
                                                                    data-border="true"
                                                                />
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        })}

                                    </div>
                                    :
                                    <div className="mng-full-table">
                                        <div className="row">
                                            <div className="col-md-12">
                                                <div className="mng-full-table-hdr admn-usr-hdr">
                                                    <div className="row">
                                                        <div className="col-md-12">
                                                            <h6>No Company Found.</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                }

                                <br /><br />
                                {!searching ?
                                    <div className="form-group">
                                        {!loading && parseFloat(pageNo) > 1 ?
                                            <span style={{ color: "#007bff", cursor: "pointer" }}
                                                onClick={() => { this.pageChangePrev() }}
                                            >
                                                <b>{"<< Prev"}</b>
                                            </span>
                                            :
                                            null}
                                    &nbsp;&nbsp;|&nbsp;&nbsp;
                                    {!loading && (parseFloat(pageNo) < parseFloat(userListRecords)) ?
                                            <span style={{ color: "#007bff", cursor: "pointer" }}
                                                onClick={() => { this.pageChangeNext() }}
                                            >
                                                <b>{"Next >>"}</b>
                                            </span>
                                            :
                                            null}
                                        {loading ?
                                            <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                                                <i className="fa fa-spinner fa-spin" ></i>
                                            </a>
                                            :
                                            null}

                                    </div>
                                    :
                                    null}

                            </div>

                        </div>
                    </div>
                </div>

            </section>
        </main>

    }

}

export default checkAuthentication(AdminUserManagement);