import React, { Component } from 'react'
import Navbar from '../Component/Navbar';
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button } from "react-bootstrap"
import _ from 'lodash';
import { CSVLink, CSVDownload } from "react-csv";
import ReactTooltip from 'react-tooltip'; // For tool tip 
import { CSVReader } from 'react-papaparse';
import { Snackbar } from "@material-ui/core";
import MySnackbarContentWrapper from "../Component/MaterialSnack";

import { checkAuthentication } from '../Component/Authentication';

var path = cred.API_PATH;


// var path = cred.API_PATH + "admin/";

export class AdminAddressBook extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      searchLoading: false,
      userId: '',
      companyType: '',
      companyId: '',
      selectedCompanyId: '',
      selectedUserId: '',
      page: '1',

      userType: cred.USER_TYPE_GUEST,

      contacts: [],
      countryCodeList: [],
      contactRecords: '',
      searchString: '',
      openSideMenu: false,
      name: "",
      emailAddress: "",
      phoneNumber: "",
      countryCode: "",
      contactUserId: "",

      nameError: "",
      emailAddressError: "",
      phoneNumberError: "",
      countryCodeError: "",
      importDataError: "",
      data: [],
      fileName: '',
      showImportAddress: false,
      showAddAddress: false,
      showEditAddress: false,

      showSuccessAlert: false,
      showErrorAlert: false,
      successAlertMessage: '',
      errorAlertMessage: '',
      showDeleteAddress: false,

    }
    this.perPage = 5;
  }

  componentDidMount() {
    document.body.classList.remove("sign-bg");
    document.body.classList.add("grey-bg");
    // this.getUsers();
    if (this.props.location.state) {

      let selectedCompanyId = this.props.match.params.company;
      let selectedUserId = this.props.match.params.user;
      let selectedCompanyName = this.props.location.state.name;
      let selectedCompanyType = this.props.location.state.type;
      let selecteduserName = this.props.location.state.userName;

      // console.log("selectedCompanyId -- ", selectedCompanyId);

      let storage = localStorage.getItem("MIO_Local");
      storage = storage ? JSON.parse(storage) : null;
      if (storage && storage.data.userId) {
        this.setState({
          userId: storage.data.userId,
          companyType: storage.data.companyType,
          companyId: storage.data.companyId,
          selectedCompanyId: selectedCompanyId,
          selectedUserId: selectedUserId,
          selectedCompanyName: selectedCompanyName,
          selectedCompanyType: selectedCompanyType,
          selecteduserName: selecteduserName
        }, () => {
          this.getAddressBook();
          this.getCountryCodeList();
        });

        console.log(this.parseJwt(storage.token));
        let tokenData = this.parseJwt(storage.token);

      }
    } else {
      this.props.history.push("/adminusermanagement");
    }

  }

  getAddressBook = () => {
    this.setState({loading: true});
    let dataToSend = {
      "userId": this.state.selectedUserId,
      "page": this.state.page,
      "perPage": this.perPage
    };
    // API ToDo - 
    axios
      .post(path + "user/get_contact_list", dataToSend, {
        headers: { "Content-Type": "application/json" }
      })
      .then(serverResponse => {
        // console.log("Response from here : ", serverResponse.data);
        let res = serverResponse.data;
        if (!res.isError) {
          let contacts = res.details && res.details.results;

          this.setState({ 
            loading: false,
            contacts: contacts,
            contactRecords: res.details && res.details.noofpage
          });
        } else {
          this.setState({
            loading: false
          });
        }
      }).catch(error => {
        console.log(error);
      });

  }

  getCountryCodeList = () => {

    axios
      .post(path + 'services/get-country-codes')
      .then(serverResponse => {

        let res = serverResponse.data;
        if (!res.isError) {
          this.setState({
            countryCodeList: res.details
          });
        }

      })
      .catch(error => {
        console.log(error);
      })

  }

  searchAddress(event) {
    event.preventDefault();
    let that = this;

    that.setState({ searching: true, searchLoading: true, loading: true });
    let { searchString, selectedUserId } = this.state;
    let contacts = [];
    let dataToSend = {
      "userId": selectedUserId,
      "searchText": searchString
    };

    axios
      .post(path + "user/search_contact_details", dataToSend, {
        headers: { "Content-Type": "application/json" }
      })
      .then(serverResponse => {
        console.log("Response from here : ", serverResponse.data);
        const res = serverResponse.data;
        if (!res.isError) {

          contacts = res.details && res.details.results;
          that.setState({ 
            contacts: contacts,
            loading: false,
            searchLoading: false

          });
          
        } else {
          alert("Sorry! Something wrong.");
          this.setState({
            loading: false,
            searching: false,
            searchLoading: false,
            searchString: ''
          });
        }
      }).catch(error => {
        console.log(error);
      });

  }

  clearSearch = (event) => {
    event.preventDefault();
    this.setState({
      searchLoading: false,
      searching: false,
      searchString: ''
    }, () => { this.getAddressBook() });
  }

  saveAddress = (event) => {
    console.log("save address ...1");
    let { contacts, name, emailAddress, phoneNumber, countryCode, contactUserId, loading } = this.state;
    // let that = this;
    !name ? this.setState({ nameError: "Please enter a contact name." }) : this.setState({ nameError: "" });
    !emailAddress ? this.setState({ emailAddressError: "Please enter an email address." }) : this.setState({ emailAddressError: "" });
    !phoneNumber ? this.setState({ phoneNumberError: "Please enter a phone number." }) : this.setState({ phoneNumberError: "" });
    !countryCode ? this.setState({ countryCodeError: "Please enter a country code." }) : this.setState({ countryCodeError: "" });

    if (name && emailAddress && phoneNumber && countryCode && !loading) {
      console.log("save address ...2");
      this.setState({ loading: true });

      (/.+@.+\.[A-Za-z]+$/.test(`${emailAddress}`)) ? this.setState({ emailAddressError: "" }) : this.setState({ emailAddressError: "Email address is not valid" });
      (!isNaN(phoneNumber)) ? this.setState({ phoneNumberError: "" }) : this.setState({ phoneNumberError: "Phone Number is not valid" });
      (!isNaN(countryCode)) ? this.setState({ countryCodeError: "" }) : this.setState({ countryCodeError: "Country code is not valid" });
      if (/.+@.+\.[A-Za-z]+$/.test(`${emailAddress}`) && !isNaN(phoneNumber) && !isNaN(countryCode)) {
        console.log("save address ...3");
        // Check any user present with that email or not
        axios.post(
          path + "user/get-user-by-email",
          { "email": emailAddress },
          { headers: { "Content-Type": "application/json" } }
        ).then(serverRes => {
          const res = serverRes.data;
          if (!res.isError) {
            if (countryCode.slice(0, 1) == '+') {
              countryCode = countryCode.slice(1);
            }
            let newuserType = res.details && res.details.userType ? res.details.userType : cred.USER_TYPE_GUEST;
            let contactUserId = res.details && res.details.userId ? res.details.userId : "";
            let address = {
              "userType": newuserType,
              "name": name,
              "emailAddress": emailAddress,
              "phoneNumber": phoneNumber,
              "countryCode": countryCode,
              "contactUserId": contactUserId,
              "userId": this.state.selectedUserId
            }

            axios.post(
              path + "user/add_contact", address,
              { headers: { "Content-Type": "application/json" } }
            ).then(serverResponse => {
              // console.log("Response from saveAddress : ", serverResponse.data);
              const res = serverResponse.data;
              if (!res.isError) {
                if (res.statuscode == 200) {
                  this.setState({ 
                    showSuccessAlert: true, 
                    successAlertMessage: 'Saved Successfully',
                    loading: false 
                  },()=>{
                    this.hideAddAddress();
                    this.getAddressBook();
                  }); // Showing successful alert
                  
                } else if (res.statuscode != 200) {
                  this.setState({ 
                    showErrorAlert: true, 
                    errorAlertMessage: res.message,
                    loading: false
                  },()=>{
                    this.hideAddAddress();
                    this.getAddressBook();
                  }); // Showing successful alert
                  
                }
                // TODO:: Show success message.
              } else {
                // TODO:: Show failure message.
                this.setState({ loading: false });
              }
            }).catch(error => {
              // TODO:: Show failure message.
              this.setState({ loading: false });
            });

            // } else { // There is no user with that email.
            this.setState({ loading: false });
          }
        }).catch(error => {
          // TODO:: Show failure message.
          this.setState({ loading: false });
        });

      }
      else {
        this.setState({ loading: false, });
      }

    }

  }

  openEditAddress = (index) => {

    let { contacts } = this.state;
    let contact = contacts[index];
    // console.log('index==============', index, contact);
    let { name, emailAddress, phoneNumber, countryCode, contactUserId, userType } = contact;

    // this.setState({ selectedId: index, showEditAddress: true, fname, lname, email, mobile });
    this.setState({ selectedId: index, showEditAddress: true, name, emailAddress, phoneNumber, countryCode, contactUserId, userType });

  }

  editAddress = () => {

    let that = this;
    let { selectedUserId, selectedId, name, emailAddress, phoneNumber, countryCode, userType, loading } = this.state;
    let { contacts } = this.state;
    let contact = contacts[selectedId];
    console.log("Contact---", contact);

    !name ? this.setState({ nameError: "Please enter a contact name." }) : this.setState({ nameError: "" });
    // !emailAddress ? this.setState({ emailAddressError: "Please enter an email address." }) : this.setState({ emailAddressError: "" });
    !phoneNumber ? this.setState({ phoneNumberError: "Please enter a phone number." }) : this.setState({ phoneNumberError: "" });
    !countryCode ? this.setState({ countryCodeError: "Please enter a country code." }) : this.setState({ countryCodeError: "" });
    if (countryCode && !isNaN(countryCode) && countryCode.slice(0, 1) == '+') {
      countryCode = countryCode.slice(1);
    }
    isNaN(phoneNumber) ? this.setState({ phoneNumberError: "Please enter valid phone number." }) : this.setState({ phoneNumberError: "" });
    isNaN(countryCode) ? this.setState({ countryCodeError: "Please enter valid country code." }) : this.setState({ countryCodeError: "" });
    if (name && phoneNumber && countryCode && !loading && !isNaN(phoneNumber) && !isNaN(countryCode)) {
      this.setState({ loading: true });
      let editData = {
        "userId": selectedUserId,
        "addressBookId": contact._id,
        "name": name,
        "phoneNumber": phoneNumber,
        "emailAddress": emailAddress,
        "countryCode": countryCode,
        "userType": userType
      }

      // Calling API for updating user contact details-- 
      axios
        .post(path + "user/update_contact_details", editData,
          {
            headers: { "Content-Type": "application/json" }
          })
        .then(serverResponse => {
          console.log("Response from editAddress : ", serverResponse.data);
          const res = serverResponse.data;
          if (!res.isError) {
            that.setState({ showSuccessAlert: true, successAlertMessage: 'Successfully Updated' }); // Showing successful alert
            that.getAddressBook();
            //Resetting all keys for edit and hiding the modal --
            that.hideEditAddress();
            // TODO:: Show success message.
            that.setState({ loading: false, showEditAddress: false });

          } else {
            // TODO:: Show failure message.
            this.setState({ loading: false });
          }
        })
        .catch(error => {
          this.setState({ loading: false });
        })

    }

  }

  deleteAddressConfirm = (index) => {
    let { contacts } = this.state;
    let contact = contacts[index];
    let { name } = contact;

    this.setState({
      selectedId: index,
      name: name,
      showDeleteAddress: true
    });

  }

  deleteAddress = () => {

    let { selectedId, contacts, selectedUserId } = this.state;
    let selectedContact = contacts[selectedId]; // use array index as selectedId

    let dataToSend = {
      "userId": selectedUserId,
      "addressBookId": selectedContact._id,
    };
    axios
      .post(path + "user/delete_contact_details", dataToSend, {
        headers: { "Content-Type": "application/json" }
      })
      .then(serverResponse => {
        console.log("Response from here : ", serverResponse.data);
        const res = serverResponse.data;
        if (!res.isError) {
          this.getAddressBook();
          this.hideDeleteAddress();
          this.setState({
            showSuccessAlert: true,
            successAlertMessage: "Successfully deleted"
          });
        } else {
          // TODO: Handel connect to a conference error
          this.setState({
            showErrorAlert: true,
            errorAlertMessage: "Sorry! Something is wrong."
          });
        }
      }).catch(error => {
        // TODO: Handel connect to a conference error
        console.log(error);
      });
  }

  pageChangePrev = () => {

    this.setState({
      pageNo: (parseFloat(this.state.pageNo) - 1)
    }, () => {
      this.getAddressBook()
    });

  }

  pageChangeNext = () => {

    this.setState({
      pageNo: (parseFloat(this.state.pageNo) + 1)
    }, () => {
      this.getAddressBook()
    });

  }

  hideAddAddress = () => {
    this.setState({
      name: "",
      emailAddress: "",
      phoneNumber: "",
      countryCode: "",
      contactUserId: "",
      showAddAddress: false
    })
  }

  hideEditAddress() {
    this.setState({
      name: "",
      emailAddress: "",
      phoneNumber: "",
      countryCode: "",
      contactUserId: "",
      showEditAddress: false
    })
  }

  hideSuccessAlert = (event) => {
    this.setState({
      showSuccessAlert: false,
      successAlertMessage: ''
    });
  }

  hideErrorAlert = () => {
    this.setState({
      showErrorAlert: false,
      errorAlertMessage: ''
    });
  }

  hideDeleteAddress = () => {
    this.setState({
      selectedId: "",
      name: "",
      showDeleteAddress: false
    });
  }

  sideMenuToggle() {
    let { openSideMenu } = this.state;
    openSideMenu = !openSideMenu;
    this.setState({ openSideMenu });
  }

  mobMenuClick() {
    this.sideMenuToggle();
    console.log('mobMenuClick');
  }

  parseJwt = (token) => {
    if (!token) { return; }
    const base64Url = token.split('.')[1];
    const base64 = base64Url.replace('-', '+').replace('_', '/');
    return JSON.parse(window.atob(base64));
  }


  render() {
    let {loading, searchLoading, searching, contacts, countryCodeList, contactRecords, page, importDataError, fileName, data } = this.state;
    let headers = [];
    headers = [
      { label: "Name", key: 'name' },
      { label: "Country Code", key: 'countryCode' },
      { label: "Phone", key: 'phoneNumber' },
      { label: "Email", key: 'emailAddress' },
      { label: "User Type", key: 'userType' },
    ];

    return <main>
      <section className="user-mngnt-wrap">
        <div className="container-fluid">
          <div className="row">
            <Navbar routTo="/opmaddressbook" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />
            <div className="col-xl-10 col-lg-9 p-0 mob-full-conf">

              <div className="mobile-menu-header">
                <a href= "JavaScript:Void(0);" className="menu-tgl" onClick={() => this.sideMenuToggle()}><img src="images/menu-tgl.png" alt="" /></a>
              </div>

              <div className="paymnt-mng-hdr" style={{ margin: "30px" }}>
                <div className="blnc">
                  <div className="logobox"><img src="/images/logo-sign.png" alt="" /></div>
                </div>
                <div className="rchrg-btn">
                  <p className="text-right" style={{ margin: "0" }}>
                    Company Name: {this.state.selectedCompanyName}<br />
                    Company Type: {this.state.selectedCompanyType}<br />
                    User Name: {this.state.selecteduserName}<br />
                    {/* User Id: {this.state.selectedCompanyId} */}
                  </p>
                </div>
              </div>

              <div className="mng-full-list" style={{ padding: "0px 50px" }}>
                
                <div className="mng-full-srch">
                  <div className="row">
                    <div className="col-md-9">
                      <div className="srch-wrap">
                        <form>
                          <input value={this.state.searchString} onChange={(event) => this.setState({ searchString: event.target.value })} type="text" name="" className="form-control" placeholder="Search by name" />
                          {
                            this.state.searching ?
                              <input type="button" onClick={(event) => this.clearSearch(event)} />
                              :
                              <input type="submit" onClick={(event) => this.searchAddress(event)} />
                          }
                        </form>
                      </div>
                    </div>
                    <div className="col-md-3">
                      <div className="add-user-btn">
                        <a href= "JavaScript:Void(0);" className="btn" onClick={() => this.setState({ showAddAddress: true })}>Add Contact</a>
                      </div>
                    </div>
                  </div>
                  {searchLoading?
                    <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                      <i className="fa fa-spinner fa-spin" ></i>
                    </a>
                  :null}
                </div>

                <div className="mng-full-table">
                  <div className="row">
                    <div className="col-md-9">
                      <div className="mng-full-table-hdr">
                        <div className="row">
                          <div className="col-md-3 border-rt">
                            <h6>Name/Company</h6>
                          </div>
                          <div className="col-md-3 border-rt">
                            <h6>User Type</h6>
                          </div>
                          <div className="col-md-3 border-rt">
                            <h6>Email</h6>
                          </div>
                          <div className="col-md-3">
                            <h6>Phone</h6>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="col-md-3">
                      <div className="mng-full-table-hdr">
                        <div className="row">
                          <div className="col-md-6 border-rt">
                            <h6>Edit</h6>
                          </div>
                          <div className="col-md-6">
                            <h6>Delete</h6>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>

                  {contacts && contacts.length > 0 && contacts.map((contact, index) => {
                    return <div className="row" key={index}>
                      <div className="col-md-9">
                        <div className="mng-full-table-row">
                          <div className="row">
                            <div className="col-md-3 border-rt">
                              <h6>Name/Company</h6>
                              <p className="textEllips">
                                <a href="JavaScript:Void(0);" onClick={() => this.openEditAddress(index)}>
                                  {contact.name}
                                </a>
                              </p>
                            </div>
                            <div className="col-md-3 border-rt">
                              <h6>User Type</h6>
                              <p className="textEllips">{contact.userType}</p>
                            </div>
                            <div className="col-md-3 border-rt"
                              data-tip={contact.emailAddress}
                            >
                              <h6>Email</h6>
                              <p className="textEllips">
                                {contact.emailAddress}
                              </p>
                              <ReactTooltip
                                effect="float"
                                place="top"
                                data-border="true"
                              />
                            </div>
                            <div className="col-md-3">
                              <h6>Phone</h6>
                              <p className="textEllips">{`+${contact.countryCode} ${contact.phoneNumber}`}</p>
                            </div>

                            <div className="mobile-ad-edt-btns">
                              <ul>

                                <li onClick={() => this.openEditAddress(index)}>
                                  <a href="JavaScript:Void(0);"
                                    data-tip='Edit/View'
                                  >
                                    <i className="far fa-edit"></i>

                                  </a>
                                  <ReactTooltip
                                    effect="float"
                                    place="top"
                                    data-border="true"
                                  />
                                </li>

                                <li onClick={() => this.deleteAddressConfirm(index)}>
                                  <a href="JavaScript:Void(0);"
                                    data-toggle="modal"
                                    data-target="#delModal"
                                    data-tip='Delete'
                                  >
                                    <i className="far fa-trash-alt"></i>

                                  </a>
                                  <ReactTooltip
                                    effect="float"
                                    place="top"
                                    data-border="true"
                                  />
                                </li>

                              </ul>
                            </div>
                          </div>
                        </div>

                      </div>

                      <div className="col-md-3">
                        <div className="mng-full-table-row add-edt text-center">
                          <div className="row">
                            <div className="col-md-6">
                              <a href="JavaScript:Void(0);"
                                onClick={() => this.openEditAddress(index)}
                                data-tip='Edit/View'
                              >
                                <i className="far fa-edit"></i>
                              </a>
                              <ReactTooltip
                                effect="float"
                                place="top"
                                data-border="true"
                              />
                            </div>

                            <div className="col-md-6">
                              <a href="JavaScript:Void(0);"
                                onClick={() => this.deleteAddressConfirm(index)}
                                // ref={ref => this.fooRef = ref}
                                data-tip='Delete'
                              // onFocus={() => { ReactTooltip.show(this.fooRef) }}
                              >
                                <i className="far fa-trash-alt"></i>

                              </a>
                              <ReactTooltip
                                effect="float"
                                place="top"
                                data-border="true"
                              />
                            </div>

                          </div>
                        </div>
                      </div>

                    </div>
                  })
                  }

                </div>

                <br /><br />
                  {!searching ?
                    <div className="form-group">
                      {!loading && parseFloat(page) > 1 ?
                        <span style={{ color: "#007bff", cursor: "pointer" }}
                          onClick={() => { this.pageChangePrev() }}
                        >
                          <b>{"<< Prev"}</b>
                        </span>
                        :
                        null}
                        &nbsp;&nbsp;|&nbsp;&nbsp;
                        {!loading && (parseFloat(page) < parseFloat(contactRecords)) ?
                        <span style={{ color: "#007bff", cursor: "pointer" }}
                          onClick={() => { this.pageChangeNext() }}
                        >
                          <b>{"Next >>"}</b>
                        </span>
                        :
                        null}
                      {loading ?
                        <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                          <i className="fa fa-spinner fa-spin" ></i>
                        </a>
                        :
                        null}

                    </div>
                    :
                    null}

              </div>
            </div>
          </div>
        </div>
      </section>

      <Modal show={this.state.showAddAddress} onHide={() => this.hideAddAddress()}>
        <Modal.Header closeButton>
          <Modal.Title>Add new Contact</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="row">

            <div className="col-md-12">
              <div className="form-group">
                <label>Name:</label>
                <input
                  type="text"
                  name="name"
                  value={this.state.name}
                  className="form-control"
                  placeholder="Enter Name"
                  onChange={event => this.setState({ name: event.target.value })}
                  onFocus={() => this.setState({ nameError: "" })}
                />
                <span style={{ color: 'red' }}>{this.state.nameError ? `* ${this.state.nameError}` : ''}</span>
              </div>
            </div>

            <div className="col-md-12">
              <div className="form-group">
                <label>Email Address:</label>
                <input
                  type="email"
                  name="usrEmail"
                  value={this.state.emailAddress}
                  className="form-control"
                  placeholder="Enter Email Address"
                  onChange={event => this.setState({ emailAddress: event.target.value })}
                  onFocus={() => this.setState({ emailAddressError: "" })}
                />
                <span style={{ color: 'red' }}>{this.state.emailAddressError ? `* ${this.state.emailAddressError}` : ''}</span>
              </div>
            </div>

            <div className="col-md-5">
              <div className="form-group">
                <label>Country Code:</label>
                <select
                  className="form-control"
                  name="countryCode"
                  value={this.state.countryCode}
                  onChange={event => this.setState({ countryCode: event.target.value })}
                  onFocus={() => this.setState({ countryCodeError: "" })}
                >
                  <option value="">Select country</option>
                  {countryCodeList && countryCodeList.map((data, index) => {
                    return <option key={index} value={data.code}>{data.code + ' ' + data.name}</option>
                  })}
                </select>
                <span style={{ color: 'red' }}>{this.state.countryCodeError ? `* ${this.state.countryCodeError}` : ''}</span>
              </div>
            </div>

            <div className="col-md-7">
              <div className="form-group">
                <label>Mobile Number:</label>
                <input
                  type="tel"
                  name="usrMobile"
                  value={this.state.phoneNumber}
                  className="form-control"
                  placeholder="Enter Mobile Number"
                  onChange={event => this.setState({ phoneNumber: event.target.value })}
                  onFocus={() => this.setState({ phoneNumberError: "" })}
                />
                <span style={{ color: 'red' }}>{this.state.phoneNumberError ? `* ${this.state.phoneNumberError}` : ''}</span>
              </div>
            </div>

          </div>
        </Modal.Body>
        <Modal.Footer>
          {/* <Button variant="secondary" onClick={() => this.hideAddAddress()}>
            Close
          </Button> */}
          {loading ?
            <Button variant="primary"> <i className="fa fa-spinner fa-spin" ></i> </Button>
            :
            null}
          {!loading ?
            <Button variant="primary" onClick={(event) => this.saveAddress(event)}> Save </Button>
            :
            null}

        </Modal.Footer>
      </Modal>

      <Modal show={this.state.showEditAddress} onHide={() => this.hideEditAddress()}>
        <Modal.Header closeButton>
          <Modal.Title>Edit address</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="row">

            <div className="col-md-12">
              <div className="form-group">
                <label>Name:</label>
                <input
                  type="text"
                  name="usrFname"
                  value={this.state.name}
                  className="form-control"
                  placeholder="Enter Contact Name"
                  onChange={event => this.setState({ name: event.target.value })}
                  onFocus={() => this.setState({ nameError: "" })}
                />
                <span style={{ color: 'red' }}>{this.state.nameError ? `* ${this.state.nameError}` : ''}</span>
              </div>
            </div>

            <div className="col-md-12">
              <div className="form-group">
                <label>Email Address:</label>
                <input
                  type="email"
                  name="usrEmail"
                  value={this.state.emailAddress}
                  className="form-control"
                  placeholder="Enter Email Address"
                  onChange={event => this.setState({ emailAddress: event.target.value })}
                  disabled
                />
              </div>
            </div>

            <div className="col-md-5">
              <div className="form-group">
                <label>Country Code:</label>
                <select
                  className="form-control"
                  name="countryCode"
                  value={this.state.countryCode}
                  onChange={event => this.setState({ countryCode: event.target.value })}
                  onFocus={() => this.setState({ countryCodeError: "" })}
                >
                  <option value="">Select country</option>
                  {countryCodeList && countryCodeList.map((data, index) => {
                    return <option value={data.code}
                      selected= {this.state.countryCode == data.code? true: false}
                    >
                      {data.code + ' ' + data.name}
                    </option>
                  })}
                </select>
                <span style={{ color: 'red' }}>{this.state.countryCodeError ? `* ${this.state.countryCodeError}` : ''}</span>
              </div>
            </div>

            <div className="col-md-7">
              <div className="form-group">
                <label>Mobile Number:</label>
                <input
                  type="tel"
                  name="usrMobile"
                  value={this.state.phoneNumber}
                  className="form-control"
                  placeholder="Enter Mobile Number"
                  onChange={event => this.setState({ phoneNumber: event.target.value })}
                  onFocus={() => this.setState({ phoneNumberError: "" })}
                />
                <span style={{ color: 'red' }}>{this.state.phoneNumberError ? `* ${this.state.phoneNumberError}` : ''}</span>
              </div>
            </div>

          </div>
        </Modal.Body>
        <Modal.Footer>
          {!loading ?
            <Button variant="primary" onClick={() => this.editAddress()}>
              Save
            </Button>
            :
            null}
          {loading ?
            <Button variant="primary" >
              <i className="fa fa-spinner fa-spin" ></i>
            </Button>
            :
            null}

        </Modal.Footer>
      </Modal>

      <Modal show={this.state.showDeleteAddress} onHide={() => this.hideDeleteAddress()}>
        <Modal.Header closeButton>
          <Modal.Title>Delete address confirm</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h3>Are you sure to delete {this.state.name}</h3>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => this.deleteAddress()}>
            Yes
          </Button>
          <Button variant="primary" onClick={() => this.hideDeleteAddress()}>
            No
          </Button>
        </Modal.Footer>
      </Modal>

      <Modal show={this.state.showSuccessAlert} onHide={() => this.hideSuccessAlert()}>
        <Modal.Header closeButton>
          <Modal.Title>OK</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h5 style={{ color: 'green' }}>{`${this.state.successAlertMessage}`}</h5>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="primary" onClick={() => this.hideSuccessAlert()}>
            Ok
          </Button>
        </Modal.Footer>
      </Modal>

      <Modal show={this.state.showErrorAlert} onHide={() => this.hideErrorAlert()}>
        <Modal.Header closeButton>
          <Modal.Title>OK</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h5 style={{ color: 'red' }}>{`${this.state.errorAlertMessage}`}</h5>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="primary" onClick={() => this.hideErrorAlert()}>
            Ok
          </Button>
        </Modal.Footer>
      </Modal>

      <Modal size="lg" show={this.state.showImportAddress} onHide={() => this.hideImportAddress()}>
        <Modal.Header closeButton>
          <Modal.Title>Verify Contacts</Modal.Title>
        </Modal.Header>
        <Modal.Body>

          {this.state.data && this.state.data.length > 0 ?
            this.state.data.map((contact, index) => {
              return <div className='row' key={index}>
                {console.log(index)}
                <div className='col-md-3'>
                  <div className="form-group">
                    <label>Name</label>
                    <input
                      type='text'
                      className="form-control"
                      value={data[index]['Name']}
                      onChange={e => this.importDataChange(index, e.target.value, 'Name')}
                    />
                    {!data[index]['Name'] && <span className="error-text">Enter a name</span>}
                  </div>
                </div>
                <div className='col-md-3'>
                  <div className="form-group">
                    <label>Email</label>
                    <input
                      type='text'
                      className="form-control"
                      value={data[index]['Email']}
                      onChange={e => this.importDataChange(index, e.target.value, 'Email')}
                    />
                    {!data[index]['Email'] && <span className="error-text">Enter an Email</span>}
                  </div>
                </div>
                <div className='col-md-3'>
                  <div className="form-group">
                    <label>Country_Code</label>
                    <input
                      type='text'
                      className="form-control"
                      value={data[index]['Country_Code']}
                      onChange={e => this.importDataChange(index, e.target.value, 'Country_Code')}
                    />
                    {!data[index]['Country_Code'] && <span className="error-text">Enter country code</span>}
                  </div>
                </div>
                <div className='col-md-3'>
                  <div className="form-group">
                    <label>Phone</label>
                    <input
                      type='text'
                      className="form-control"
                      value={data[index]['Phone']}
                      onChange={e => this.importDataChange(index, e.target.value, 'Phone')}
                    />
                    {!data[index]['Phone'] && <span className="error-text">Enter a phone</span>}
                  </div>
                </div>
              </div>
            })
            :
            <div className='rew'>
              <div className='col-md-12' style={{ textAlign: 'center' }}>Data not selected</div>
            </div>
          }
        </Modal.Body>
        <Modal.Footer>
          {!loading ?
            <Button variant="primary" onClick={() => this.saveContacts()}>
              Save contacts
          </Button>
            :
            null}
          {loading ?
            <Button variant="primary" >
              <i className="fa fa-spinner fa-spin" ></i>
            </Button>
            :
            null}
        </Modal.Footer>
      </Modal>

    </main>
  }
}

export default checkAuthentication(AdminAddressBook);