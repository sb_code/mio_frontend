import React, { Component } from "react";
import Navbar from "../Component/Navbar";
import ProgressBar from 'react-bootstrap/ProgressBar';
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button, Row, Col } from "react-bootstrap"
import ToggleButton from 'react-bootstrap/ToggleButton'
import _ from 'lodash';
import { CSVLink, CSVDownload } from "react-csv";
import ReactTooltip from 'react-tooltip'; // For tool tip 
import moment from 'moment';

import { checkAuthentication } from '../Component/Authentication';

var path = cred.API_PATH;

export class NewRegistered extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      pageNo: 1,
      companyListsRecords: '',

      userId: '',

      openSideMenu: false,
      searchString: '',
      searchType: '',
      searching: false,

      countryCodeList: [],
      companyLists: [],

      editUser: false,
      confirm: false,

      companyName: '',
      selectedCompanyType: '',

      noOfSeats: '0',
      pricePerSeat: '0.00',
      trialPeriod: '0',
      currency: '',
      paymentGateway: '',
      gatewayError: false,
      isEditError: false,
      showUser: false,

      selectedCompanyId: '',
      selectedCompany: {},
      companyServices: [],
      updatedCompany: {},
      invoiceBillingInfo: '',
      vat: 0.0,
      voiceUrl: '',
      brandUrl: '',
      fileUploaded1: '',
      fileUploaded2: '',

      fname: '',
      lname: '',
      email: '',
      countryCode: '',
      mobile: '',
      companyType: '',
      password: '',

      showSuccessAlert: false,
      successAlertMessage: '',
      showErrorAlert: false,
      errorAlertMessage: ''

    }
    this.perPage = 10;
  }

  componentDidMount() {
    document.body.classList.remove("sign-bg");
    document.body.classList.add("grey-bg");

    let storage = localStorage.getItem("MIO_Local");
    storage = storage ? JSON.parse(storage) : null;
    if (storage && storage.data.userId) {
      this.setState({
        userId: storage.data.userId,
        userType: storage.data.userType,
        companyId: storage.data.companyId
      }, () => {
        this.getCompanyListForAdmin();
        this.getCountryCodeList();
      });

      let tokenData = this.parseJwt(storage.token);

    }

  }


  getCompanyListForAdmin = () => {
    this.setState({ loading: true });
    let dataToSend = {
      "isApproved": false,
      "page": this.state.pageNo,
      "perPage": this.perPage
    };

    axios
      .post(path + 'user/get-companies', dataToSend)
      .then(serverResponse => {
        let res = serverResponse.data;
        if (!res.isError) {
          this.setState({
            companyLists: res.details && res.details[0] && res.details[0].results,
            companyListsRecords: res.details && res.details[0] && res.details[0].noofpage,
            loading: false
          });
        } else {
          this.setState({
            loading: false
          })
        }
      })
      .catch(error => {
        console.log(error);
        this.setState({
          loading: false
        })
      })
  }

  pageChangePrev = () => {

    this.setState({
      pageNo: (parseFloat(this.state.pageNo) - 1)
    }, () => {
      this.getCompanyListForAdmin()
    });

  }

  pageChangeNext = () => {

    this.setState({
      pageNo: (parseFloat(this.state.pageNo) + 1)
    }, () => {
      this.getCompanyListForAdmin()
    });

  }


  getCountryCodeList = () => {

    axios
      .post(path + 'services/get-country-codes')
      .then(serverResponse => {

        let res = serverResponse.data;
        if (!res.isError) {
          this.setState({
            countryCodeList: res.details
          });
        }

      })
      .catch(error => {
        console.log(error);
      })

  }

  searchCompany = (event) => {
    event.preventDefault();

    if (this.state.searchType == '' && this.state.searchString == '') {
      alert('Invalid search');
    } else {

      this.setState({
        searching: true
      });

      let DataToSend = {
        companyType: this.state.searchType,
        companyName: this.state.searchString
      };
      // console.log("dataToSend - - ", DataToSend);
      // API -- 
      axios
        .post(path + 'user/search-companies', DataToSend)
        .then(serverResponse => {
          let res = serverResponse.data;
          if (!res.isError) {
            console.log("serverResponse for search -- ", res);
            if (res.details) {
              this.setState({
                companyLists: res.details
              });
            }
          } else {
            alert('Sorry! something is wrong.');
          }
        })
        .catch(error => {
          console.log(error);
        })

    }
  }

  clearSearch = (event) => {
    event.preventDefault();
    this.setState({
      searchString: '',
      searchType: '',
      searching: false,
    }, () => {
      this.getCompanyListForAdmin()
    });
  }

  approveCompany = (index) => {

    let selectedCompany = this.state.companyLists && this.state.companyLists[index];
    let selectedCompanyId = selectedCompany._id;
    let companyServices = selectedCompany && selectedCompany.services;

    this.setState({
      selectedCompany: selectedCompany,
      selectedCompanyId: selectedCompanyId,
      companyName: selectedCompany.companyName,
      selectedCompanyType: selectedCompany.companyType,
      currency: selectedCompany.currency,
      paymentGateway: selectedCompany.paymentGateway,
      editUser: true
    }, () => { console.log("companyServices----", this.state.companyServices) });

  }

  onFileChange = (e) => {

    console.log("uploading file name is===", e.target.name);
    let fileName = e.target.name;
    let file;
    if (fileName == 'welcome') {
      file = this.refs.welcome.files[0];
    } else if (fileName == 'branding') {
      file = this.refs.branding.files[0];
    }

    if (file) {
      // console.log("uploaded local file = ", file);
      // For checking uploaded file extension -- 
      let blnValid = false;
      let sizeValid = false;
      //For checking  file size ---
      let sizeInByte = file.size;
      let sizeInKB = parseFloat(parseFloat(sizeInByte) / 1024).toFixed('2');
      if (sizeInKB <= 3100) {
        sizeValid = true;
      } else {
        sizeValid = false;
      }

      let sFileName = file.name;
      var _validFileExtensions = [".mp3", ".wav"];
      for (var j = 0; j < _validFileExtensions.length; j++) {
        var sCurExtension = _validFileExtensions[j];
        if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
          blnValid = true;
          break;
        }
      }

      if (!blnValid) {
        this.setState({
          showErrorAlert: true,
          errorAlertMessage: "File type should be .mp3"
        })
      } else if (sizeValid) {
        this.setState({
          showErrorAlert: true,
          errorAlertMessage: "File size shouldn't be more than 3mb."
        });

      } else {
        this.setState({
          loading: true
        });
        var reader = new FileReader();
        var url = reader.readAsDataURL(file);
        // console.log("uploading file details == ", url);
        // reader.onloadend = function (e) {
        //     this.setState({
        //         voiceUrl: [reader.result]
        //     })
        // }.bind(this);

        const data = new FormData();
        // data.append('file', this.refs.file.files[0]);
        data.append('file', file);
        // data.append('companyId', this.state.companyId);

        // console.log("uploading payload for file == "+ data);

        axios
          .post(path + "services/upload-custom-voice",
            data,
            {
              headers: { 'Content-Type': 'multipart/form-data' },
              onUploadProgress: (progressEvent) => {
                // console.log("raw upload loader ---------", progressEvent.loaded, progressEvent.total);

                let uploadPercentage = parseInt(Math.round((progressEvent.loaded / progressEvent.total) * 100));
                if (fileName == 'welcome') {
                  this.setState({ fileUploaded1: uploadPercentage });
                } else if (fileName == 'branding') {
                  this.setState({ fileUploaded2: uploadPercentage });
                }

              }

            }
          )
          .then(serverResponse => {

            let res = serverResponse.data;
            if (!res.isError) {
              console.log("upload response ===", res);
              let voiceUrl = '';
              let brandUrl = '';

              if (fileName == 'welcome') {
                voiceUrl = res.detail && res.detail.url;

                this.setState({
                  voiceUrl: voiceUrl,
                  showSuccessAlert: true,
                  successAlertMessage: 'Successfully uploaded.',
                  fileUploaded1: '',
                  fileUploaded2: '',
                  loading: false
                }, () => {
                  console.log("after file upload....", this.state.brandUrl);
                })
              } else if (fileName == 'branding') {
                brandUrl = res.detail && res.detail.url;

                this.setState({
                  brandUrl: brandUrl,
                  showSuccessAlert: true,
                  successAlertMessage: 'Successfully uploaded.',
                  fileUploaded1: '',
                  fileUploaded2: '',
                  loading: false
                }, () => {
                  console.log("after file upload....", this.state.brandUrl);
                })
              }


            } else {
              this.setState({
                showErrorAlert: true,
                errorAlertMessage: 'Sorry! something wrong...',
                fileUploaded1: '',
                fileUploaded2: '',
                loading: false
              })
            }

          })
          .catch(error => {
            console.log(error);
          })

      }

    }
  }

  updateServices = (event, index) => {
    let fieldName = event.target.name;
    let fieldValue = event.target.value;
    // console.log("fieldName--", fieldName);

    let { companyServices } = this.state;
    let thisCompany = companyServices[index];
    thisCompany[`${fieldName}`] = fieldValue;

    let thisCostPerMinute = thisCompany.costPerMinute;

    let calculatedVal = '0.0';

    if (fieldName == "pricePerMinute") {

      calculatedVal = parseFloat(parseFloat(fieldValue) / parseFloat(thisCostPerMinute)).toFixed('2');
      thisCompany["multiplier"] = !isNaN(calculatedVal) ? calculatedVal : '0.0';

    } else if (fieldName == "multiplier") {

      calculatedVal = parseFloat(parseFloat(fieldValue) * parseFloat(thisCostPerMinute)).toFixed('2');
      thisCompany["pricePerMinute"] = !isNaN(calculatedVal) ? calculatedVal : '0.0';

    }

    companyServices[index] = thisCompany;

    if (thisCompany[`${fieldName}`] == '' || isNaN(thisCompany[`${fieldName}`])) {
      companyServices[index][`${fieldName}` + "Error"] = "Use Valid " + `${fieldName}`;
      this.setState({
        isEditError: true
      });
    } else {
      companyServices[index][`${fieldName}` + "Error"] = '';
      this.setState({
        isEditError: false
      });
    }

    this.setState({
      companyServices: companyServices
    });

  }

  addNewCompany = async () => {
    try {

      let { fname, lname, email, countryCode, mobile, companyType, password, fnameError, lnameError, emailError, countryCodeError, mobileError, passwordError } = this.state;
      !fname ? this.setState({ fnameError: "Please give first name" }) : this.setState({ fnameError: '' })
      !lname ? this.setState({ lnameError: "Please give last name" }) : this.setState({ lnameError: '' })
      !email ? this.setState({ emailError: "Please give email" }) : this.setState({ emailError: '' })
      !countryCode || isNaN(countryCode) ? this.setState({ countryCodeError: "Please give country code" }) : this.setState({ countryCodeError: '' })
      !mobile || isNaN(mobile) ? this.setState({ mobileError: "Please give phone no." }) : this.setState({ mobileError: '' })
      !password ? this.setState({ passwordError: "Please give password" }) : this.setState({ passwordError: '' })
      !companyType ? this.setState({ companyTypeError: "Please give company type" }) : this.setState({ companyTypeError: "" })

      if (fname && lname && email && countryCode && mobile && password && !isNaN(countryCode) && !isNaN(mobile)) {
        this.setState({ loading: true });
        let dataToSend = {
          fname: fname,
          lname: lname,
          email: email,
          mobile: mobile,
          countryCode: countryCode,
          password: password
        };

        let registrationStatus = await this.registration(dataToSend);
        let newCompanyId = registrationStatus && registrationStatus.companyId;
        let newUserId = registrationStatus && registrationStatus.userId;
        let updateStatus = await this.updation(newCompanyId, companyType);
        let upgradeStatus = await this.upgradeCompanyService(newCompanyId, companyType);
        let customerStatus = await this.createCustomer(fname, lname, email, newUserId, newCompanyId);

        if (registrationStatus && updateStatus && updateStatus.status && upgradeStatus && upgradeStatus.status && customerStatus) {

          this.setState({
            loading: false,
            showSuccessAlert: true,
            successAlertMessage: 'Successfully added.'
          }, () => {
            this.clearCompanyForm();
            this.getCompanyListForAdmin();
          });

        } else {

          this.setState({
            loading: false,
            showErrorAlert: true,
            errorAlertMessage: 'Sorry! Something is wrong.'
          }, () => {
            this.clearCompanyForm();
            this.getCompanyListForAdmin();
          });

        }

      }
    }
    catch (error) {
      console.log(error);
    }

  }

  registration = (dataToSend) => {

    return new Promise((resolve, reject) => {

      // API --
      axios
        .post(path + 'user/sign_up', dataToSend)
        .then(serverResponse => {

          let res = serverResponse.data;
          if (!res.isError) {
            resolve(res.details);
          } else {
            reject();
          }

        })
        .catch(error => {
          console.log(error);
        })


    })

  }

  updation = (companyId, companyType) => {

    return new Promise((resolve, reject) => {

      let dataToSend = {
        "companyId": companyId,
        "companyType": companyType
      };
      // API --
      axios
        .post(path + 'user/update-company-details', dataToSend)
        .then(serverResponse => {

          let res = serverResponse.data;
          if (!res.isError) {
            let data = {
              "status": true
            };
            resolve(data);
          } else {
            let data = {
              "status": false
            };
            resolve(data);
          }

        })
        .catch(error => {
          console.log(error);
        })

    })

  }

  upgradeCompanyService = (companyId, companyType) => {
    return new Promise((resolve, reject) => {

      let DataToSend = {
        "companyId": companyId,
        "companyType": companyType
      };

      axios
        .post(path + 'services/add-base-services-to-company', DataToSend)
        .then(serverResponse => {

          let res = serverResponse.data;
          if (!res.isError) {
            let data = {
              "status": true
            };
            resolve(data);
          } else {
            let data = {
              "status": false
            };
            resolve(data);
          }

        })
        .catch(error => {
          console.log(error);
        })

    })

  }

  createCustomer = (fname, lname, email, userId, companyId) => {

    return new Promise((resolve, reject) => {

      let dataToSend = {
        "fname": fname,
        "lname": lname,
        "email": email,
        "userId": userId,
        "companyId": companyId
      }

      axios
        .post(path + 'payment/create-customer', dataToSend)
        .then(serverResponse => {
          let res = serverResponse.data;
          if (!res.isError) {
            resolve(true);
          } else {
            resolve(false);
          }
        })
        .catch(error => {
          console.log(error);
          reject();
        })

    })

  }

  editCompany = (event) => {

    let { companyName, currency, paymentGateway } = this.state;
    companyName == '' ? this.setState({ companyNameError: "Use valid Company Name" }) : this.setState({ companyNameError: "" })
    // noOfSeats == '' || isNaN(noOfSeats) ? this.setState({ noOfSeatsError: "Use valid no of seats" }) : this.setState({ noOfSeatsError: "" })
    // pricePerSeat == '' || isNaN(pricePerSeat) ? this.setState({ pricePerSeatError: "Use valid price per seat" }) : this.setState({ pricePerSeatError: "" })
    currency == "" ? this.setState({ currencyError: true }) : this.setState({ currencyError: false })
    paymentGateway == "" ? this.setState({ gatewayError: true }) : this.setState({ gatewayError: false })

    if (currency != '' && companyName != '' && paymentGateway != '') {
      this.setState({
        loading: true
      });
      let DataToSend = {
        companyId: this.state.selectedCompanyId,
        companyName: companyName,
        companyType: this.state.selectedCompany.companyType,
        currency: currency,
        paymentGateway: paymentGateway,
        isApproved: true
      };
      console.log("DataToSend--", DataToSend);

      // API -- 
      axios
        .post(path + 'user/update-company-details', DataToSend)
        .then(serverResponse => {
          let res = serverResponse.data;
          if (!res.isError) {
            this.setState({
              showSuccessAlert: true,
              successAlertMessage: "Successfully Approved"
            }, () => {
              this.getCompanyListForAdmin();
              this.clearState();
            });

          } else {
            this.setState({
              showErrorAlert: true,
              errorAlertMessage: "Sorry! Try again."
            }, () => {
              this.clearState();
            });

          }
        })

    }

  }

  clearState = () => {
    this.setState({
      loading: false,
      editUser: false,
      confirm: false,
      showSuccessAlert: false,
      showErrorAlert: false,
      successAlertMessage: '',
      errorAlertMessage: '',
      selectedCompany: {},
      selectedCompanyId: '',
      companyName: '',
      currency: '',
      paymentGateway: '',
      searchString: '',
      searchType: '',
      searching: false
    })

  }

  clearCompanyForm = () => {
    this.setState({
      showUser: false,
      fname: '',
      lname: '',
      email: '',
      countryCode: '',
      mobile: '',
      password: ''
    });
  }

  onAlertHide() {
    this.setState({
      showSuccessAlert: false,
      showErrorAlert: false,
      successAlertMessage: '',
      errorAlertMessage: '',
    });
  }

  sideMenuToggle() {
    let { openSideMenu } = this.state;
    openSideMenu = !openSideMenu;
    this.setState({ openSideMenu });
  }

  mobMenuClick() {
    this.sideMenuToggle();
    console.log('mobMenuClick');
  }

  parseJwt = (token) => {
    if (!token) { return; }
    const base64Url = token.split('.')[1];
    const base64 = base64Url.replace('-', '+').replace('_', '/');
    return JSON.parse(window.atob(base64));
  }


  render() {

    let { users, loading, companyName,
      currency,
      companyLists,
      countryCodeList,
      fname, lname, email, countryCode, mobile, companyType, password,
      pageNo, searching, companyListsRecords,
    } = this.state;

    return (
      <main>

        <section className="user-mngnt-wrap">
          <div className="container-fluid">
            <div className="row">
              <Navbar routTo="/newregistered" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />
              <div className="col-xl-10 col-lg-9 p-0">
                <div className="mobile-menu-header">

                  <a href="JavaScript:Void(0);" className="menu-tgl" onClick={() => this.sideMenuToggle()}>
                    <img src="/images/menu-tgl.png" alt="" />
                  </a>
                </div>
                <div className="mng-full-list">
                  <div className="mng-full-srch">

                    <div className="row">

                      <div className="col-md-3">
                        <div className="add-user-btn">
                          <a href="JavaScript:Void(0);"
                            className="btn"
                            onClick={() => this.setState({ showUser: true })}
                            data-tip='Add new company'
                          >
                            Add New Company
                            </a>
                          <ReactTooltip
                            effect="float"
                            place="top"
                            data-border="true"
                          />
                        </div>
                      </div>

                    </div>

                  </div>
                  {companyLists && companyLists.length > 0 ?
                    <div className="mng-full-table company-mangmnt">

                      <div className="row">

                        <div className="col-md-10">
                          <div className="mng-full-table-hdr">
                            <div className="row">
                              <div className="col-md-4 border-rt">
                                <h6>Company</h6>
                              </div>
                              <div className="col-md-4 border-rt">
                                <h6>Type</h6>
                              </div>
                              <div className="col-md-4">
                                <h6>Created on</h6>
                              </div>
                              {/* <div className="col-md-3">
                              <h6>Introduction</h6>
                            </div> */}
                            </div>
                          </div>
                        </div>
                        <div className="col-md-2">

                        </div>

                      </div>

                      {companyLists && companyLists.length > 0 ? companyLists.map((data, index) => {

                        return <div className="row" key={index}>
                          <div className="col-md-10">

                            <div className="mng-full-table-row">
                              <div className="row">
                                <div className="col-md-4 border-rt">
                                  <h6>Company</h6>
                                  <p className="textEllips">
                                    <a href="JavaScript:Void(0);">
                                      {data.companyName}
                                    </a>
                                  </p>

                                </div>
                                <div className="col-md-4 border-rt">
                                  <h6>Type</h6>
                                  <p>{data.companyType}</p>
                                </div>
                                <div className="col-md-4">
                                  <h6>Created On</h6>
                                  <p>{moment(new Date(data.companyCreatedAt)).format("YYYY-MM-DD")}</p>
                                </div>

                                <div className="mobile-ad-edt-btns">


                                </div>

                              </div>
                            </div>
                          </div>

                          <div className="col-md-2">
                            {data.isApproved ?
                              <a
                                href="JavaScript:Void(0);"
                                className="btn border w-100 h-auto-btn"
                                data-tip='Assign'
                              // onClick={() => { this.approveCompany(index) }}
                              > Approved </a>
                              :
                              <a
                                href="JavaScript:Void(0);"
                                className="btn w-100 h-auto-btn"
                                data-tip='Assign'
                                onClick={() => { this.approveCompany(index) }}
                              > Approve </a>
                            }
                            <ReactTooltip
                              effect="float"
                              place="top"
                              data-border="true"
                            />
                          </div>

                        </div>

                      })
                        : null}

                    </div>
                    :
                    <div className="mng-full-table">
                      <div className="row">
                        <div className="col-md-12">
                          <div className="mng-full-table-hdr admn-usr-hdr">
                            <div className="row">
                              <div className="col-md-12">
                                <h6>No Company Found.</h6>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  }
                  <br /><br />
                  {!searching ?
                    <div className="form-group">
                      {!loading && parseFloat(pageNo) > 1 ?
                        <span style={{ color: "#007bff", cursor: "pointer" }}
                          onClick={() => { this.pageChangePrev() }}
                        >
                          <b>{"<< Prev"}</b>
                        </span>
                        :
                        null}
                        &nbsp;&nbsp;|&nbsp;&nbsp;
                        {!loading && (parseFloat(pageNo) < parseFloat(companyListsRecords)) ?
                        <span style={{ color: "#007bff", cursor: "pointer" }}
                          onClick={() => { this.pageChangeNext() }}
                        >
                          <b>{"Next >>"}</b>
                        </span>
                        :
                        null}
                      {loading ?
                        <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                          <i className="fa fa-spinner fa-spin" ></i>
                        </a>
                        :
                        null}

                    </div>
                    :
                    null}
                </div>

              </div>
            </div>
          </div>
        </section>

        <Modal show={this.state.showUser} onHide={() => { this.clearCompanyForm() }}>
          <Modal.Header closeButton>
            <Modal.Title>Add new Company</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="row">

              <div className="col-md-6">
                <div className="form-group">
                  <label>First Name:</label>
                  <input
                    type="text"
                    name="fname"
                    value={fname}
                    className="form-control"
                    placeholder="Enter First Name"
                    onChange={event => this.setState({ fname: event.target.value })}
                    onFocus={() => this.setState({ fnameError: "" })}
                  />
                  <span style={{ color: 'red' }}>{this.state.fnameError ? `* ${this.state.fnameError}` : ''}</span>
                </div>
              </div>

              <div className="col-md-6">
                <div className="form-group">
                  <label>Last Name:</label>
                  <input
                    type="text"
                    name="lname"
                    value={lname}
                    className="form-control"
                    placeholder="Enter Last Name"
                    onChange={event => this.setState({ lname: event.target.value })}
                    onFocus={() => this.setState({ lnameError: "" })}
                  />
                  <span style={{ color: 'red' }}>{this.state.lnameError ? `* ${this.state.lnameError}` : ''}</span>
                </div>
              </div>

              <div className="col-md-12">
                <div className="form-group">
                  <label>Email Address:</label>
                  <input
                    type="email"
                    name="email"
                    value={email}
                    className="form-control"
                    placeholder="Enter Email Address"
                    onChange={event => this.setState({ email: event.target.value })}
                    onFocus={() => this.setState({ emailError: "" })}
                  />
                  <span style={{ color: 'red' }}>{this.state.emailError ? `* ${this.state.emailError}` : ''}</span>
                </div>
              </div>

              <div className="col-md-5">
                <div className="form-group">
                  <label>Country Code:</label>
                  <select
                    className="form-control"
                    name="countryCode"
                    value={countryCode}
                    onChange={(event) => this.setState({ countryCode: event.target.value })}
                  >
                    <option value="">Select country</option>
                    {countryCodeList && countryCodeList.map((data, index) => {
                      return <option value={data.code}>{data.code + ' ' + data.name}</option>
                    })}
                  </select>
                  <span style={{ color: 'red' }}>{this.state.countryCodeError ? `* ${this.state.countryCodeError}` : ''}</span>
                </div>
              </div>

              <div className="col-md-7">
                <div className="form-group">
                  <label>Mobile Number:</label>
                  <input
                    type="tel"
                    name="mobile"
                    value={mobile}
                    className="form-control"
                    placeholder="Enter Mobile Number"
                    onChange={event => this.setState({ mobile: event.target.value })}
                    onFocus={() => this.setState({ mobileError: "" })}
                  />
                  <span style={{ color: 'red' }}>{this.state.mobileError ? `* ${this.state.mobileError}` : ''}</span>
                </div>
              </div>

              <div className="col-md-6">
                <div className="form-group">
                  <label>Company Type:</label>
                  <select
                    className="form-control"
                    name="companyType"
                    value={companyType}
                    onChange={(event) => { this.setState({ companyType: event.target.value }) }}
                  >
                    <option value="">Select One</option>
                    <option value="FREE_TYPE">FREE</option>
                    <option value="STARTUP">START UP</option>
                    <option value="ENTERPRISE">ENTERPRISE</option>
                  </select>
                  <span style={{ color: 'red' }}>{this.state.companyTypeError ? `* ${this.state.companyTypeError}` : ''}</span>
                </div>
              </div>

              <div className="col-md-6">
                <div className="form-group">
                  <label>Password:</label>
                  <input
                    type="password"
                    name="password"
                    value={password}
                    className="form-control"
                    placeholder="Enter Password"
                    onChange={event => this.setState({ password: event.target.value })}
                    onFocus={() => this.setState({ passwordError: "" })}
                  />
                  <span style={{ color: 'red' }}>{this.state.passwordError ? `* ${this.state.passwordError}` : ''}</span>
                </div>
              </div>

            </div>
          </Modal.Body>
          <Modal.Footer>
            {loading ?
              <Button variant="primary"> <i className="fa fa-spinner fa-spin" ></i> </Button>
              :
              null}
            {!loading ?
              <Button variant="primary" onClick={() => { this.addNewCompany() }}> Save </Button>
              :
              null}
          </Modal.Footer>
        </Modal>

        <Modal show={this.state.editUser} onHide={() => { this.clearState() }}>
          <Modal.Header closeButton>
            <Modal.Title> Approve Company</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {/* {companyLists && companyLists.map((company, index)=>{ */}
            <div className="row">

              <div className="col-md-12">
                <div className="form-group">
                  <label>Company Name:</label>
                  <input
                    type="text"
                    name="companyName"
                    value={companyName}
                    className="form-control"
                    placeholder="Enter Company Name"
                    onChange={event => { this.setState({ companyName: event.target.value }) }}
                    onFocus={() => this.setState({ companyNameError: "" })}
                    readOnly={true}
                  />
                  <span style={{ color: 'red' }}>{this.state.companyNameError ? `* ${this.state.companyNameError}` : ''}</span>
                </div>
              </div>

              <div className="col-md-6">
                <div className="form-group">
                  <label>Currency:<span style={{ color: "red" }}>*</span></label>
                  <select className="form-control"
                    name="currency"
                    value={currency}
                    onChange={(event) => { this.setState({ currency: event.target.value }) }}
                    onClick={() => { this.setState({ currencyError: false }) }}
                  >
                    <option value="">Select Currency</option>
                    <option value={cred.CURRENCY.usd}>{cred.CURRENCY.usd}</option>
                    <option value={cred.CURRENCY.gbp}>{cred.CURRENCY.gbp}</option>
                    <option value={cred.CURRENCY.naira}>{cred.CURRENCY.naira}</option>
                    <option value={cred.CURRENCY.euro}>{cred.CURRENCY.euro}</option>
                  </select>
                  <span style={{ color: 'red' }}>{this.state.currencyError ? '* Select valid currency' : ''}</span>
                </div>
              </div>

              <div className="col-md-6">
                <div className="form-group">
                  <label>Select Gateway:<span style={{ color: "red" }}>*</span></label>
                  <select name="gateway" value={this.state.paymentGateway}
                    className="form-control"
                    onChange={(e) => { this.setState({ paymentGateway: e.target.value }) }}
                    onFocus={() => { this.setState({ gatewayError: false }) }}
                    disabled={this.state.currency != '' ? false : true}
                  >
                    <option value="">Select Any Gateway</option>
                    {this.state.currency && this.state.currency == cred.CURRENCY.naira ?
                      <>
                        <option value={cred.PAYMENT_GATEWAY.PAYSTACK}>{cred.PAYMENT_GATEWAY.PAYSTACK}</option>
                      </>
                      :
                      <>
                        <option value={cred.PAYMENT_GATEWAY.STRIPE}>{cred.PAYMENT_GATEWAY.STRIPE}</option>
                        <option value={cred.PAYMENT_GATEWAY.PAYPAL}>{cred.PAYMENT_GATEWAY.PAYPAL}</option>
                      </>
                    }
                  </select>
                  <span style={{ color: "red" }}>{this.state.gatewayError ? '*Select gateway' : ''}</span>
                </div>
              </div>

            </div>
            {/* })
            :<div></div>} */}
          </Modal.Body>
          <Modal.Footer>

            <div className="row">
              <div className="col-md-6">
                <div className="form-group" style={{ marginRight: "25px" }}>
                  {loading ?
                    <Button variant="primary"> <i className="fa fa-spinner fa-spin" ></i> </Button>
                    :
                    null}
                  {!loading ?
                    <Button variant="primary" onClick={(event) => this.editCompany(event)}> Approve </Button>
                    :
                    null}
                </div>
              </div>
              <div className="col-md-6">

              </div>
            </div>

          </Modal.Footer>
        </Modal>

        <Modal show={this.state.confirm} onHide={() => { this.clearState() }}>
          <Modal.Header closeButton>
            <Modal.Title>Change status</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h3>Are you sure?</h3>
            <p>Changing status of {this.state.companyName}</p>
          </Modal.Body>
          <Modal.Footer>
            {!loading ?
              <Button variant="secondary" onClick={(event) => this.changeStatus(event)}>
                Yes
              </Button>
              :
              null}
            {loading ?
              <Button variant="secondary" >
                <i class="fas fa-spinner fa-spin"></i>
              </Button>
              :
              null}
            <Button variant="primary" onClick={() => { this.clearState() }}>
              No
            </Button>
          </Modal.Footer>
        </Modal>

        <Modal show={this.state.showSuccessAlert} onHide={() => this.onAlertHide()}>
          <Modal.Header closeButton>
            <Modal.Title>OK</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h5 style={{ color: 'green' }}>{`${this.state.successAlertMessage}`}</h5>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="primary" onClick={() => this.onAlertHide()}>
              Ok
            </Button>
          </Modal.Footer>
        </Modal>

        <Modal show={this.state.showErrorAlert} onHide={() => this.onAlertHide()}>
          <Modal.Header closeButton>
            <Modal.Title>OK</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h5 style={{ color: 'red' }}>{`${this.state.errorAlertMessage}`}</h5>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="primary" onClick={() => this.onAlertHide()}>
              Ok
            </Button>
          </Modal.Footer>
        </Modal>


      </main>
    );
  }
}

export default checkAuthentication(NewRegistered);
