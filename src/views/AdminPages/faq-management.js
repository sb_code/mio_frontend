import React, { Component } from 'react'
import Navbar from '../Component/Navbar'
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button } from "react-bootstrap"
import _ from 'lodash';
import { CSVLink, CSVDownload } from "react-csv";
import ReactTooltip from 'react-tooltip'; // For tool tip 

import { checkAuthentication } from '../Component/Authentication';

var path = cred.API_PATH;


export class FaqManagement extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userId: '',
            userType: '',
            companyId: '',
            selectedPageName: '',

            openSideMenu: false,

            loading: false,

            details: [],
            editingField: '',

            showModal: false,
            question: '',
            answer: '',

            showSuccessAlert: false,
            successAlertMessage: '',
            showErrorAlert: false,
            errorAlertMessage: ''

        }
    }

    componentDidMount() {
        document.body.classList.remove("sign-bg");
        document.body.classList.add("grey-bg");

        if (this.props.location.state && this.props.location.state.name) {

            let storage = localStorage.getItem("MIO_Local");
            storage = storage ? JSON.parse(storage) : null;
            let selectedPageName = this.props.location.state.name;

            if (storage && storage.data.userId) {
                this.setState({
                    userId: storage.data.userId,
                    userType: storage.data.userType,
                    companyId: storage.data.companyId,
                    selectedPageName: selectedPageName
                }, () => {
                    this.getFaqDetails();
                });

                // console.log(this.parseJwt(storage.token));
                let tokenData = this.parseJwt(storage.token);

            }
        } else {
            this.props.history.push("/admincontentmanagement");
        }

    }


    getFaqDetails = () => {

        axios
            .post(path + 'content-management/get-faq')
            .then(serverResponse => {
                let res = serverResponse.data;
                if (!res.isError) {
                    this.setState({
                        details: res.details,
                    });
                    console.log("All FAQ details == ", res);
                }
            })
            .catch(error => {
                console.log(error);
            })

    }


    addNewFAQ = () => {

        let { question, answer, userId, companyId } = this.state;

        question || question != '' ? this.setState({ questionError: '' }) : this.setState({ questionError: 'Enter a valid question.' });
        answer || answer != '' ? this.setState({ answerError: '' }) : this.setState({ answerError: 'Enter a valid answer.' });

        if (question && answer && question != '' && answer != '') {

            let pair = {
                "question": question,
                "answer": answer
            };
            let dataToSend = {
                "faq": [pair]
            };
            this.setState({loading: true});
            // API - -
            axios
                .post(path + 'content-management/insert-faq', dataToSend)
                .then(serverResponse => {
                    let res = serverResponse.data;
                    console.log("response after save qsn==", res);
                    if (!res.isError) {
                        this.setState({
                            loading: false,
                            question: '',
                            answer: '',
                            showModal: false,
                            showSuccessAlert: true,
                            successAlertMessage: 'Successfully saved.'
                        }, () => {
                            this.getFaqDetails();
                        });
                    } else {
                        this.setState({
                            loading: false,
                            question: '',
                            answer: '',
                            showModal: false,
                            showErrorAlert: true,
                            errorAlertMessage: 'Sorry! Something is wrong.'
                        });
                    }
                })
                .catch(error => {
                    this.setState({loading: false})
                    console.log(error);
                })

        }
    }

    editAns = (index) => {
        let { details } = this.state;
        let pair = details[index];

        let dataToSend = {
            "faqId": pair._id,
            "answer": pair.answer
        };
        this.setState({loading: true});
        // API -- 
        axios
            .post(path + 'content-management/update-faq-by-id', dataToSend)
            .then(serverResponse => {
                let res = serverResponse.data;
                if (!res.isError) {
                    this.setState({
                        loading: false,
                        editingField: '',
                        showSuccessAlert: true,
                        successAlertMessage: 'Successfully saved.'
                    }, () => {
                        this.getFaqDetails();
                    });
                } else {
                    this.setState({
                        loading: false,
                        editingField: '',
                        showErrorAlert: true,
                        errorAlertMessage: 'Sorry! Something is wrong.'
                    }, () => {
                        this.getFaqDetails();
                    });
                }
            })
            .catch(error => {
                this.setState({loading: false});
                console.log(error);
            })

    }

    changeAnswer = (event, index) => {

        let { details } = this.state;
        let pair = details[index];
        pair["answer"] = event.target.value;
        details[index] = pair;

        this.setState({
            details: details
        });

    }

    deleteAns = (index) => {

        let { details } = this.state;
        let pair = details[index];
        let dataToSend = {
            "faqId": pair._id,
        };
        this.setState({loading: true});
        // API for deleting -- 
        axios
            .post(path + 'content-management/delete-faq', dataToSend)
            .then(serverResponse => {
                let res = serverResponse.data;
                if (!res.isError) {
                    this.setState({
                        loading: false,
                        showSuccessAlert: true,
                        successAlertMessage: 'Successfully deleted.'
                    }, () => {
                        this.getFaqDetails();
                    })
                } else {
                    this.setState({
                        loading: false,
                        showErrorAlert: true,
                        errorAlertMessage: 'Sorry! Something is wrong.'
                    })
                }
            })
            .catch(error => {
                this.setState({loading: false});
                console.log(error);
            })

    }

    clearState = () => {

        this.setState({
            question: '',
            answer: '',
            showModal: false
        });

    }

    hideSuccessAlert() {
        this.setState({
            showSuccessAlert: false,
            successAlertMessage: '',
        })
    }

    hideErrorAlert() {
        this.setState({
            showErrorAlert: false,
            errorAlertMessage: ''
        })
    }

    sideMenuToggle = () => {
        let { openSideMenu } = this.state;
        openSideMenu = !openSideMenu;
        this.setState({ openSideMenu });
    }

    mobMenuClick() {
        this.sideMenuToggle();
        console.log('mobMenuClick');
    }

    backMenueToggle= ()=>{
        this.props.history.goBack();
    }

    parseJwt = (token) => {
        if (!token) { return; }
        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace('-', '+').replace('_', '/');
        return JSON.parse(window.atob(base64));
    }


    render() {

        let {loading, details, editingField, question, answer } = this.state;

        return <main>
            <section className="user-mngnt-wrap">
                <div className="container-fluid">
                    <div className="row">

                        <Navbar routTo="/admincontentmanagement" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />
                        <div className="col-xl-10 col-lg-10 p-0 mob-full-conf">

                            <div className="mobile-menu-header">
                                <a href= "JavaScript:Void(0);" className="back-arw" onClick={()=> this.backMenueToggle()}><i className="fas fa-arrow-left"></i></a>
                                {/* <a className="menu-tgl" onClick={() => this.sideMenuToggle()}><img src="images/menu-tgl.png" alt="" /></a> */}
                            </div>

                            <div className="custom-brdcrmb">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item"><a href="JavaScript:Void(0);">Content Management</a></li>
                                        <li className="breadcrumb-item active" aria-current="page">{this.state.selectedPageName}</li>
                                    </ol>
                                </nav>
                            </div>

                            <div className="mng-full-list about-mng-form" style={{ padding: "25px 30px" }}>
                                <div className="row">
                                    <div className="col-md-3">
                                        <div className="add-user-btn">
                                            <a  href= "JavaScript:Void(0);"
                                                className="btn"
                                                onClick={() => this.setState({ showModal: true })}
                                                ref={ref => this.fooRef = ref}
                                                data-tip='Add new FAQ'
                                                onFocus={() => { ReactTooltip.show(this.fooRef) }}
                                            >
                                                Add New FAQ
                                                </a>
                                            <ReactTooltip
                                                effect="float"
                                                place="top"
                                                data-border="true"
                                            />
                                        </div>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-lg-12 col-md-12">

                                        {details && details.map((pair, index) => {
                                            return <div className="row" key={index}>
                                                <div className="col-md-10">
                                                    <div className="form-group faq-ans-lbl">
                                                        <label>{index + 1}: {pair.question}</label>
                                                        <textarea type="text" name="details2" className="form-control" rows="4" cols="50"
                                                            placeholder="Enter Answer" style={{ height: "85px" }}
                                                            onChange={(event) => this.changeAnswer(event, index)}
                                                            readOnly={editingField && editingField == `${index}` ? false : true}
                                                            value={pair.answer}
                                                        >
                                                            {/* {pair.answer} */}
                                                        </textarea>
                                                    </div>
                                                </div>

                                                <div className="mobile-ad-edt-btns">
                                                    <ul>
                                                        {editingField == `${index}` && !loading ?
                                                            <li onClick={() => { this.editAns(index) }}>
                                                                <a href="JavaScript:Void(0);"
                                                                >
                                                                    <i className="fas fa-save"></i>
                                                                </a>
                                                                
                                                            </li>
                                                            :
                                                            null}
                                                        {editingField != `${index}` && !loading ?
                                                            <li onClick={() => { this.setState({ editingField: `${index}` }) }}>
                                                                <a href="JavaScript:Void(0);"
                                                                >
                                                                    <i className="fas fa-edit"></i>
                                                                </a>
                                                                
                                                            </li>
                                                            :
                                                            null}
                                                    </ul>
                                                    <ul>
                                                        
                                                        <li onClick={() => { this.deleteAns(index) }}>
                                                            {!loading?
                                                            <a href="JavaScript:Void(0);"
                                                            >
                                                                <i className="fas fa-trash"></i>
                                                            </a>
                                                            :null}
                                                            {loading?
                                                                <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                                                                    <i className="fa fa-spinner fa-spin" ></i>
                                                                </a>
                                                            :null}
                                                            
                                                        </li>
                                                            
                                                    </ul>

                                                </div>

                                                <div className="col-md-2">
                                                    <div className="mng-full-table-row add-edt text-center">
                                                        <div className="row">
                                                            {editingField == `${index}` && !loading ?
                                                                <div className="col-md-4">
                                                                    <a href="JavaScript:Void(0);"
                                                                        onClick={() => { this.editAns(index) }}
                                                                        data-tip='Save'
                                                                    >
                                                                        <i className="fas fa-save"></i>
                                                                    </a>
                                                                    <ReactTooltip
                                                                        effect="float"
                                                                        place="top"
                                                                        data-border="true"
                                                                    />
                                                                </div>
                                                                : null}

                                                            {editingField != `${index}` && !loading ?
                                                                <div className="col-md-4">
                                                                    <a href="JavaScript:Void(0);"
                                                                        onClick={() => { this.setState({ editingField: `${index}` })}}
                                                                        data-tip='Edit'
                                                                    >
                                                                        <i className="fas fa-edit"></i>
                                                                    </a>
                                                                    <ReactTooltip
                                                                        effect="float"
                                                                        place="top"
                                                                        data-border="true"
                                                                    />
                                                                </div>
                                                                : null}
                                                            {!loading?
                                                                <div className="col-md-4">
                                                                    <a href="JavaScript:Void(0);"
                                                                        ref={ref => this.fooRef = ref}
                                                                        data-tip='Delete'
                                                                        onFocus={() => { ReactTooltip.show(this.fooRef) }}
                                                                        onClick={() => { this.deleteAns(index); ReactTooltip.hide(this.fooRef) }}
                                                                    >
                                                                        <i className="fas fa-trash"></i>
                                                                    </a>
                                                                    <ReactTooltip
                                                                        effect="float"
                                                                        place="top"
                                                                        data-border="true"
                                                                    />
                                                                </div>
                                                            :null}
                                                            {loading?
                                                                <div className="col-md-4">
                                                                    <a href="JavaScript:Void(0);" >
                                                                        <i className="fa fa-spinner fa-spin" ></i>
                                                                    </a>
                                                                </div>
                                                            :null}

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        })}


                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </section>

            <Modal show={this.state.showModal} size="lg" onHide={() => { this.clearState() }}>
                <Modal.Header closeButton>
                    <Modal.Title>Add new FAQ</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="row">

                        <div className="col-md-12">
                            <div className="form-group">
                                <label>Question:</label>
                                <textarea type="text" name="question" className="form-control" rows="4" cols="50"
                                    placeholder="Enter a question" style={{ height: "50px" }}
                                    value={question}
                                    onChange={(event) => this.setState({ question: event.target.value })}
                                    onFocus={() => { this.setState({ questionError: '' }) }}
                                >
                                    {question}
                                </textarea>
                                <span style={{ color: 'red' }}>{this.state.questionError ? `* ${this.state.questionError}` : ''}</span>
                            </div>
                        </div>

                        <div className="col-md-12">
                            <div className="form-group">
                                <label>Answer:</label>
                                <textarea type="text" name="answer" className="form-control" rows="4" cols="50"
                                    placeholder="Enter an answer" style={{ height: "100px" }}
                                    value={answer}
                                    onChange={(event) => this.setState({ answer: event.target.value })}
                                    onFocus={() => { this.setState({ answerError: '' }) }}
                                >
                                    {answer}
                                </textarea>
                                <span style={{ color: 'red' }}>{this.state.answerError ? `* ${this.state.answerError}` : ''}</span>
                            </div>
                        </div>

                    </div>
                </Modal.Body>
                <Modal.Footer>
                    {!loading?
                        <Button variant="primary" onClick={this.addNewFAQ}> Save </Button>
                    :null}
                    {loading?
                        <Button variant="primary"> <i className="fa fa-spinner fa-spin" ></i> </Button>
                    :null}

                </Modal.Footer>
            </Modal>

            <Modal show={this.state.showSuccessAlert} onHide={() => this.hideSuccessAlert()}>
                <Modal.Header closeButton>
                    <Modal.Title>OK</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h5 style={{ color: 'green' }}>{`${this.state.successAlertMessage}`}</h5>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={() => this.hideSuccessAlert()}>
                        Ok
            </Button>
                </Modal.Footer>
            </Modal>

            <Modal show={this.state.showErrorAlert} onHide={() => this.hideErrorAlert()}>
                <Modal.Header closeButton>
                    <Modal.Title>OK</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h5 style={{ color: 'red' }}>{`${this.state.errorAlertMessage}`}</h5>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={() => this.hideErrorAlert()}>
                        Ok
            </Button>
                </Modal.Footer>
            </Modal>

        </main >

    }

}

export default checkAuthentication(FaqManagement);



