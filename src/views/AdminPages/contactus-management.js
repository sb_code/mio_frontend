import React, { Component } from 'react'
import Navbar from '../Component/Navbar'
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button, Card, Badge } from "react-bootstrap"
import _ from 'lodash';
import { CSVLink, CSVDownload } from "react-csv";
import ReactTooltip from 'react-tooltip'; // For tool tip 

import { checkAuthentication } from '../Component/Authentication';

var path = cred.API_PATH;


export class ContactusManagement extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userId: '',
            userType: '',
            companyId: '',
            selectedPageName: '',

            openSideMenu: false,
            searchString: '',
            searching: false,

            showSuccessAlert: false,
            successAlertMessage: '',
            showErrorAlert: false,
            errorAlertMessage: '',

            loading: false,

            contactList: [],
            details: {},
            branchName: '',
            address: '',
            name: '',
            mailId1: '',
            mailId2: '',
            contactNo1: '',
            contactNo2: '',
            mailId1Error: '',
            mailId1Error: '',
            contactNo1Error: '',
            contactNo2Error: '',

            selectedContactId: '',
            deletedItem: '',
            willEdit: true,
            hasError: false,
            showContactForm: false,
            editContactForm: false,
            showConfirmBox: false,

        }
    }

    componentDidMount() {
        document.body.classList.remove("sign-bg");
        document.body.classList.add("grey-bg");

        if (this.props.location.state && this.props.location.state.name) {

            let storage = localStorage.getItem("MIO_Local");
            storage = storage ? JSON.parse(storage) : null;
            let selectedPageName = this.props.location.state.name;

            if (storage && storage.data.userId) {
                this.setState({
                    userId: storage.data.userId,
                    userType: storage.data.userType,
                    companyId: storage.data.companyId,
                    selectedPageName: selectedPageName
                }, () => {
                    this.getAboutmeDetails();
                });

                // console.log(this.parseJwt(storage.token));
                let tokenData = this.parseJwt(storage.token);

            }
        } else {
            this.props.history.push("/admincontentmanagement");
        }

    }


    getAboutmeDetails = () => {

        let dataToSend = {};
        // API --- 
        axios
            .post(path + 'content-management/get-mio-admin-detail', dataToSend)
            .then(serverResponse => {
                let res = serverResponse.data;
                if (!res.isError) {
                    let details = res.details;
                    let contactDetails = details && details.contactUs;
                    this.setState({
                        details: details,
                        contactList: contactDetails,

                    })
                }
            })
            .catch(error => {
                console.log(error);
            })

    }


    saveContact = (event) => {

        event.preventDefault();
        let hasError = false;
        let { mailId1, mailId2, contactNo1, contactNo2, address, branchName } = this.state;

        !branchName ? this.setState({ branchNameError: "Please use a branch name" }) : this.setState({ branchNameError: "" });
        mailId1 && (/.+@.+\.[A-Za-z]+$/.test(`${mailId1}`)) ? this.setState({ mailId1Error: "" }) : this.setState({ mailId1Error: "Please use valid mail id" });

        if (mailId2) {
            if (/.+@.+\.[A-Za-z]+$/.test(`${mailId2}`)) {
                this.setState({ mailId2Error: "" });
            } else {
                this.setState({ mailId2Error: "Please use valid mail id" });
                hasError = true;
            }
        } else {
            hasError = false;
        }

        // contactNo1 && contactNo1.match(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/) ? this.setState({ contactNo1Error: "" }) : this.setState({ contactNo1Error: "Please use valid mobile no" });

        // if (contactNo2) {
        //     if (contactNo2.match(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/)) {
        //         this.setState({ contactNo2Error: "" })
        //     } else {
        //         this.setState({ contactNo2Error: "Please use valid mobile no" });
        //         hasError = true;
        //     }
        // } else {
        //     hasError = false;
        // }

        if (branchName && mailId1 && (/.+@.+\.[A-Za-z]+$/.test(`${mailId1}`)) && contactNo1 && !hasError) {

            let dataToSend = {
                "companyId": this.state.companyId,
                "branchName": branchName,
                "address": address,
                "mailId1": mailId1,
                "mailId2": mailId2,
                "contactNo1": contactNo1,
                "contactNo2": contactNo2,
            };
            this.setState({loading: true})
            // console.log("dataToSend ====", dataToSend);
            axios
                .post(path + 'content-management/insert-contact-for-mio-admin', dataToSend)
                .then(serverResponse => {

                    let res = serverResponse.data;
                    if (!res.isError) {
                        console.log("response === ", res);
                        this.setState({
                            loading: false,
                            showSuccessAlert: true,
                            successAlertMessage: 'Successfully Saved.'
                        }, () => {
                            this.hideContactForm();
                            this.getAboutmeDetails();
                        });

                    } else {
                        console.log("response === ", res);
                        this.setState({
                            loading: false,
                            showErrorAlert: true,
                            errorAlertMessage: 'Sorry! something is wrong.',
                        });
                    }

                })


        }

    }


    editContact = (index) => {

        let { contactList } = this.state;
        let contact = contactList[index];
        let { _id, branchName, contactNo1, contactNo2, mailId1, mailId2, location } = contact;

        this.setState({
            selectedContactId: _id,
            branchName: branchName,
            contactNo1: contactNo1,
            contactNo2: contactNo2,
            mailId1: mailId1,
            mailId2: mailId2,
            address: location,
            editContactForm: true
        });

    }

    updateContact = (event) => {
        event.preventDefault();

        let { selectedContactId, branchName, mailId1, mailId2, contactNo1, contactNo2, address, } = this.state;
        // mailId1 != '' && !(/.+@.+\.[A-Za-z]+$/.test(`${mailId1}`)) ? this.setState({ mailId1Error: "Please use valid mail id", hasError: true }) : this.setState({ mailId1Error: '', hasError: false });
        // mailId2 != '' && !(/.+@.+\.[A-Za-z]+$/.test(`${mailId2}`)) ? this.setState({ mailId1Error: "Please use valid mail id", hasError: true }) : this.setState({ mailId2Error: '', hasError: false });
        // contactNo1 != '' && (isNaN(contactNo1)) ? this.setState({ contactNo1Error: "Please use valid mobile no", hasError: true }) : this.setState({ contactNo1Error: "", hasError: false });
        // contactNo2 != '' && (isNaN(contactNo2)) ? this.setState({ contactNo2Error: "Please use valid mobile no", hasError: true }) : this.setState({ contactNo2Error: "", hasError: false});

        // if (!hasError) {
        let dataToSend = {
            "companyId": this.state.companyId,
            "contactId": selectedContactId,
            "branchName": branchName,
            "mailId1": mailId1,
            "mailId2": mailId2,
            "contactNo1": contactNo1,
            "contactNo2": contactNo2,
            "address": address
        };
        this.setState({loading: true})
        // API -- 
        axios
            .post(path + 'content-management/update-contactUs-for-mio-admin', dataToSend)
            .then(serverResponse => {
                let res = serverResponse.data;
                if (!res.isError) {

                    this.setState({
                        loading: false,
                        showSuccessAlert: true,
                        successAlertMessage: 'Successfully updated.',
                    }, () => {
                        this.hideContactForm();
                        this.getAboutmeDetails();
                    })

                } else {
                    this.setState({
                        loading: false,
                        showErrorAlert: true,
                        errorAlertMessage: 'Sorry! something is wrong.',
                    }, () => {
                        this.getAboutmeDetails();
                    })
                }
            })

        // }
    }


    deleteContact = (index) => {

        let { contactList } = this.state;
        let contact = contactList[index];
        let { _id, branchName, contactNo1, contactNo2, mailId1, mailId2, location } = contact;

        this.setState({
            selectedContactId: _id,
            showConfirmBox: true,
            deletedItem: branchName,
        });

    }

    deleteContactConfirmed = () => {
        this.setState({loading: true});
        let dataToSend = {
            'companyId': this.state.companyId,
            'contactId': this.state.selectedContactId
        };
        axios
            .post(path + 'content-management/delete-contact-of-mio-admin', dataToSend)
            .then(serverResponse => {
                let res = serverResponse.data;
                if (!res.isError) {

                    this.setState({
                        loading: false,
                        showSuccessAlert: true,
                        successAlertMessage: 'Successfully deleted.',
                    }, () => {
                        this.hideContactForm();
                        this.getAboutmeDetails();
                    });

                } else {
                    this.setState({
                        loading: false,
                        showErrorAlert: true,
                        errorAlertMessage: 'Sorry! something is wrong.',
                    }, () => {
                        this.hideContactForm();
                    });
                }
            })


    }

    hideSuccessAlert() {
        this.setState({
            showSuccessAlert: false,
            successAlertMessage: '',
        })
    }

    hideErrorAlert() {
        this.setState({
            showErrorAlert: false,
            errorAlertMessage: '',
        })
    }

    hideContactForm = () => {

        this.setState({
            showContactForm: false,
            editContactForm: false,
            showConfirmBox: false,
            selectedContactId: '',
            deletedItem: '',
            address: '',
            branchName: '',
            mailId1: '',
            mailId2: '',
            contactNo1: '',
            contactNo2: '',
            branchNameError: '',
            mailId1Error: '',
            mailId2Error: '',
            contactNo1Error: '',
            contactNo2Error: '',
            hasError: false

        });

    }

    sideMenuToggle = () => {
        let { openSideMenu } = this.state;
        openSideMenu = !openSideMenu;
        this.setState({ openSideMenu });
    }

    mobMenuClick() {
        this.sideMenuToggle();
        console.log('mobMenuClick');
    }

    backMenueToggle= ()=>{
        this.props.history.goBack();
    }

    parseJwt = (token) => {
        if (!token) { return; }
        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace('-', '+').replace('_', '/');
        return JSON.parse(window.atob(base64));
    }


    render() {

        let {loading, contactList, willEdit, details, name, address, branchName, mailId1, mailId2, contactNo1, contactNo2, branchNameError, mailId1Error, mailId2Error, contactNo1Error, contactNo2Error } = this.state;

        return <main>
            <section className="user-mngnt-wrap">
                <div className="container-fluid">
                    <div className="row">

                        <Navbar routTo="/admincontentmanagement" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />
                        <div className="col-xl-10 col-lg-9 p-0 mob-full-conf">

                            <div className="mobile-menu-header">
                                <a href= "JavaScript:coid(0);" className="back-arw" onClick={()=> this.backMenueToggle()}><i className="fas fa-arrow-left"></i></a>
                                {/* <a className="menu-tgl" onClick={() => this.sideMenuToggle()}><img src="images/menu-tgl.png" alt="" /></a> */}
                            </div>

                            <div className="custom-brdcrmb">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item"><a href="#">Content Management</a></li>
                                        <li className="breadcrumb-item active" aria-current="page">{this.state.selectedPageName}</li>
                                    </ol>
                                </nav>
                            </div>

                            <>
                                {/*<div className="mng-full-list about-mng-form">
                                 <div className="row">
                                    <div className="col-lg-9 col-md-8">
                                        <form>
                                            <div className="row">
                                                <div className="col-md-12">
                                                    <div className="form-group">
                                                        <span>Company Name: <b><strong>{name}</strong></b></span>

                                                    </div>
                                                </div>
                                                <div className="col-md-12">
                                                    <div className="form-group">
                                                        <label>Contact No:</label>
                                                        <input type="tel" name="contactNo1" className="form-control" placeholder="Enter contact no"
                                                            value={contactNo1} onChange={(event) => { this.setState({ contactNo1: event.target.value }) }}
                                                            readOnly={willEdit} onFocus={() => this.setState({ contactNo1Error: "" })}
                                                        />
                                                        <span style={{ color: 'red' }}>{contactNo1Error ? `* ${contactNo1Error}` : ''}</span>
                                                    </div>
                                                </div>
                                                <div className="col-md-12">
                                                    <div className="form-group">
                                                        <label>Alt. Contact No.:</label>
                                                        <input type="tel" name="contactNo2" className="form-control" placeholder="Enter alt contact no"
                                                            value={contactNo2} onChange={(event) => { this.setState({ contactNo2: event.target.value }) }}
                                                            readOnly={willEdit} onFocus={() => this.setState({ contactNo2Error: "" })}
                                                        />
                                                        <span style={{ color: 'red' }}>{contactNo2Error ? `* ${contactNo2Error}` : ''}</span>
                                                    </div>
                                                </div>
                                                <div className="col-md-12">
                                                    <div className="form-group">
                                                        <label>Email Id:</label>
                                                        <input type="email" name="mailId1" className="form-control" placeholder="Enter email id"
                                                            value={mailId1} onChange={(event) => { this.setState({ mailId1: event.target.value }) }}
                                                            readOnly={willEdit} onFocus={() => this.setState({ mailId1Error: "" })}
                                                        />
                                                        <span style={{ color: 'red' }}>{mailId1Error ? `* ${mailId1Error}` : ''}</span>
                                                    </div>
                                                </div>
                                                <div className="col-md-12">
                                                    <div className="form-group">
                                                        <label>Alt Email Id:</label>
                                                        <input type="email" name="mailId2" className="form-control" placeholder="Enter alt email id"
                                                            value={mailId2} onChange={(event) => { this.setState({ mailId2: event.target.value }) }}
                                                            readOnly={willEdit} onFocus={() => this.setState({ mailId2Error: "" })}
                                                        />
                                                        <span style={{ color: 'red' }}>{mailId2Error ? `* ${mailId2Error}` : ''}</span>
                                                    </div>
                                                </div>
                                                <div className="col-md-12">
                                                    <div className="form-group">
                                                        <button className="btn w-100"
                                                            onClick={(event) => { this.updateForm(event) }}
                                                            ref={ref => this.fooRef = ref}
                                                            data-tip='Update'
                                                            onFocus={() => { ReactTooltip.show(this.fooRef) }}
                                                        >
                                                            <i className="fas fa-sync" />
                                                            Update
                                                        </button>
                                                        <ReactTooltip
                                                            effect="float"
                                                            place="top"
                                                            data-border="true"
                                                        />
                                                    </div>
                                                </div>

                                            </div>
                                        </form>
                                    </div>

                                    {willEdit ?
                                        <div class="col-lg-3 col-md-4">

                                            <a href="JavaScript:Void(0);"
                                                onClick={() => { this.setState({ willEdit: !this.state.willEdit }) }}
                                                className="btn pay-btn"
                                                ref={ref => this.fooRef = ref}
                                                data-tip='Edit'
                                                onFocus={() => { ReactTooltip.show(this.fooRef) }}
                                            >
                                                <i className="far fa-edit" />
                                            </a>
                                            <ReactTooltip
                                                effect="float"
                                                place="top"
                                                data-border="true"
                                            />
                                        </div>
                                        : null}

                                </div> 
                            </div> */}
                            </>


                            <div className="mng-full-list about-mng-form">
                                <div className="row">
                                    <div className="col-md-3">
                                        <div className="add-user-btn">
                                            <a className="btn"
                                                data-tip='Add Contact'
                                                onClick={() => this.setState({ showContactForm: true })}
                                            >
                                                Add Contact
                                            </a>
                                            <ReactTooltip
                                                effect="float"
                                                place="top"
                                                data-border="true"
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    {contactList && contactList.length > 0 ? contactList.map((value, index) => {
                                        return <div className="col-md-4" key={index}>
                                            <Card border="info" style={{ margin: '10px', padding: '10px' }}>
                                                <Card.Header>{value.branchName}</Card.Header>
                                                <Card.Body>
                                                    <Card.Title></Card.Title>
                                                    <Card.Subtitle className="mb-2 text-muted">{value.location ? `${value.location}` : ''}</Card.Subtitle>
                                                    <Card.Text>
                                                        {value.contactNo1}{value.contactNo2 ? ' , ' + `${value.contactNo2}` : ''}
                                                    </Card.Text>
                                                    <Card.Text>
                                                        {value.mailId1}{value.mailId2 ? ' , ' + `${value.mailId2}` : ''}
                                                    </Card.Text>
                                                    <Card.Link href="JavaScript:Void(0);" onClick={() => { this.editContact(index) }}>
                                                        <Badge variant="primary" style={{ fontSize: "90%", lineHeight: "1.5" }}>Edit</Badge>
                                                    </Card.Link>
                                                    <Card.Link href="JavaScript:Void(0);" onClick={() => { this.deleteContact(index) }}>
                                                        <Badge variant="danger" style={{ fontSize: "90%", lineHeight: "1.5" }}>Delete</Badge>
                                                    </Card.Link>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                    })
                                        : null}

                                </div>

                            </div>


                        </div>

                    </div>
                </div>

            </section>

            <Modal show={this.state.showSuccessAlert} onHide={() => this.hideSuccessAlert()}>
                <Modal.Header closeButton>
                    <Modal.Title>OK</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h5 style={{ color: 'green' }}>{`${this.state.successAlertMessage}`}</h5>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={() => this.hideSuccessAlert()}>
                        Ok
            </Button>
                </Modal.Footer>
            </Modal>

            <Modal show={this.state.showErrorAlert} onHide={() => this.hideErrorAlert()}>
                <Modal.Header closeButton>
                    <Modal.Title>OK</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h5 style={{ color: 'red' }}>{`${this.state.errorAlertMessage}`}</h5>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={() => this.hideErrorAlert()}>
                        Ok
            </Button>
                </Modal.Footer>
            </Modal>

            <Modal show={this.state.showContactForm} onHide={() => this.hideContactForm()}>
                <Modal.Header closeButton>
                    <Modal.Title>Add new Contact</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="row">

                        <div className="col-md-12">
                            <div className="form-group">
                                <label>Branch Name:<span style={{ color: 'red' }}>*</span></label>
                                <input
                                    type="text"
                                    name="name"
                                    value={this.state.branchName}
                                    className="form-control"
                                    placeholder="Enter Name"
                                    onChange={event => this.setState({ branchName: event.target.value })}
                                    onFocus={() => this.setState({ branchNameError: "" })}
                                />
                                <span style={{ color: 'red' }}>{this.state.branchNameError ? `* ${this.state.branchNameError}` : ''}</span>
                            </div>
                        </div>

                        <div className="col-md-12">
                            <div className="form-group">
                                <label>Email Address1:<span style={{ color: 'red' }}>*</span></label>
                                <input
                                    type="email"
                                    name="mailId1"
                                    value={this.state.mailId1}
                                    className="form-control"
                                    placeholder="Enter Email Address"
                                    onChange={event => this.setState({ mailId1: event.target.value })}
                                    onFocus={() => this.setState({ mailId1Error: "" })}
                                />
                                <span style={{ color: 'red' }}>{this.state.mailId1Error ? `* ${this.state.mailId1Error}` : ''}</span>
                            </div>
                        </div>

                        <div className="col-md-12">
                            <div className="form-group">
                                <label>Email Address2:</label>
                                <input
                                    type="email"
                                    name="mailId2"
                                    value={this.state.mailId2}
                                    className="form-control"
                                    placeholder="Enter Email Address"
                                    onChange={event => this.setState({ mailId2: event.target.value })}
                                    onFocus={() => this.setState({ mailId2Error: "" })}
                                />
                                <span style={{ color: 'red' }}>{this.state.mailId2Error ? `* ${this.state.mailId2Error}` : ''}</span>
                            </div>
                        </div>

                        <div className="col-md-6">
                            <div className="form-group">
                                <label>Contact No. 1:<span style={{ color: 'red' }}>*</span></label>
                                <input
                                    type="tel"
                                    name="contactNo1"
                                    value={this.state.contactNo1}
                                    className="form-control"
                                    placeholder="Enter Contact Number"
                                    onChange={event => this.setState({ contactNo1: event.target.value })}
                                    onFocus={() => this.setState({ contactNo1Error: "" })}
                                />
                                <span style={{ color: 'red' }}>{this.state.contactNo1Error ? `* ${this.state.contactNo1Error}` : ''}</span>
                            </div>
                        </div>

                        <div className="col-md-6">
                            <div className="form-group">
                                <label>Contact No. 2:</label>
                                <input
                                    type="tel"
                                    name="contactNo2"
                                    value={this.state.contactNo2}
                                    className="form-control"
                                    placeholder="Enter Contact Number"
                                    onChange={event => this.setState({ contactNo2: event.target.value })}
                                    onFocus={() => this.setState({ contactNo2Error: "" })}
                                />
                                <span style={{ color: 'red' }}>{this.state.contactNo2Error ? `* ${this.state.contactNo2Error}` : ''}</span>
                            </div>
                        </div>

                        <div className="col-md-12">
                            <div className="form-group">
                                <label>Address:</label>
                                <textarea
                                    col="20" row="5"
                                    style={{ height: "150px" }}
                                    type="text"
                                    name="address"
                                    value={this.state.address}
                                    className="form-control"
                                    placeholder="Enter Mobile Number"
                                    onChange={event => this.setState({ address: event.target.value })}
                                >
                                    {this.state.address}
                                </textarea>
                                {/* <span style={{ color: 'red' }}>{this.state.addressError ? `* ${this.state.addressError}` : ''}</span> */}
                            </div>
                        </div>

                    </div>
                </Modal.Body>
                <Modal.Footer>
                    {!loading?
                        <Button variant="primary" onClick={(event) => this.saveContact(event)}> Save </Button>
                        :null}
                    {loading?
                        <Button variant="primary"> <i className="fa fa-spinner fa-spin" ></i> </Button>
                    :null}

                </Modal.Footer>
            </Modal>

            <Modal show={this.state.editContactForm} onHide={() => this.hideContactForm()}>
                <Modal.Header closeButton>
                    <Modal.Title>Edit Contact</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="row">

                        <div className="col-md-12">
                            <div className="form-group">
                                <label>Branch Name:</label>
                                <input
                                    type="text"
                                    name="name"
                                    value={this.state.branchName}
                                    className="form-control"
                                    placeholder="Enter Name"
                                    onChange={event => this.setState({ branchName: event.target.value })}
                                    onFocus={() => this.setState({ branchNameError: "" })}
                                    readOnly={true}
                                />
                                <span style={{ color: 'red' }}>{this.state.branchNameError ? `* ${this.state.branchNameError}` : ''}</span>
                            </div>
                        </div>

                        <div className="col-md-12">
                            <div className="form-group">
                                <label>Email Address1:</label>
                                <input
                                    type="email"
                                    name="mailId1"
                                    value={this.state.mailId1}
                                    className="form-control"
                                    placeholder="Enter Email Address"
                                    onChange={event => this.setState({ mailId1: event.target.value })}
                                    onFocus={() => this.setState({ mailId1Error: "" })}
                                />
                                <span style={{ color: 'red' }}>{this.state.mailId1Error ? `* ${this.state.mailId1Error}` : ''}</span>
                            </div>
                        </div>

                        <div className="col-md-12">
                            <div className="form-group">
                                <label>Email Address2:</label>
                                <input
                                    type="email"
                                    name="mailId2"
                                    value={this.state.mailId2}
                                    className="form-control"
                                    placeholder="Enter Email Address"
                                    onChange={event => this.setState({ mailId2: event.target.value })}
                                // onFocus={() => this.setState({ mailId2Error: "" })}
                                />
                                {/* <span style={{ color: 'red' }}>{this.state.mailId2Error ? `* ${this.state.mailId2Error}` : ''}</span> */}
                            </div>
                        </div>

                        <div className="col-md-6">
                            <div className="form-group">
                                <label>Contact No. 1:</label>
                                <input
                                    type="tel"
                                    name="contactNo1"
                                    value={this.state.contactNo1}
                                    className="form-control"
                                    placeholder="Enter Contact Number"
                                    onChange={event => this.setState({ contactNo1: event.target.value })}
                                    onFocus={() => this.setState({ contactNo1Error: "" })}
                                />
                                <span style={{ color: 'red' }}>{this.state.contactNo1Error ? `* ${this.state.contactNo1Error}` : ''}</span>
                            </div>
                        </div>

                        <div className="col-md-6">
                            <div className="form-group">
                                <label>Contact No. 2:</label>
                                <input
                                    type="tel"
                                    name="contactNo2"
                                    value={this.state.contactNo2}
                                    className="form-control"
                                    placeholder="Enter Contact Number"
                                    onChange={event => this.setState({ contactNo2: event.target.value })}
                                // onFocus={() => this.setState({ contactNo2Error: "" })}
                                />
                                {/* <span style={{ color: 'red' }}>{this.state.contactNo2Error ? `* ${this.state.contactNo2Error}` : ''}</span> */}
                            </div>
                        </div>

                        <div className="col-md-12">
                            <div className="form-group">
                                <label>Address:</label>
                                <textarea
                                    col="20" row="5"
                                    style={{ height: "150px" }}
                                    type="text"
                                    name="address"
                                    value={this.state.address}
                                    className="form-control"
                                    placeholder="Enter Mobile Number"
                                    onChange={event => this.setState({ address: event.target.value })}
                                >
                                    {this.state.address}
                                </textarea>
                                {/* <span style={{ color: 'red' }}>{this.state.addressError ? `* ${this.state.addressError}` : ''}</span> */}
                            </div>
                        </div>

                    </div>
                </Modal.Body>
                <Modal.Footer>

                    {!loading?
                        <Button variant="primary" onClick={(event) => this.updateContact(event)}> Update </Button>
                    :null}
                    {loading?
                        <Button variant="primary"> <i className="fa fa-spinner fa-spin" ></i> </Button>
                    :null}

                </Modal.Footer>
            </Modal>

            <Modal show={this.state.showConfirmBox} onHide={() => this.hideContactForm()}>
                <Modal.Header closeButton>
                    <Modal.Title>Are you sure?</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h5 style={{ color: 'green' }}> Deleting contact for - {`${this.state.deletedItem}`}</h5>
                </Modal.Body>
                <Modal.Footer>
                    {!loading?
                        <Button variant="primary" onClick={() => this.deleteContactConfirmed()}>
                            Yes
                        </Button>
                    :null}
                    {loading?
                        <Button variant="primary"> <i className="fa fa-spinner fa-spin" ></i> </Button>
                    :null}
                    <Button variant="secondary" onClick={() => this.hideContactForm()}>
                        No
                    </Button>
                </Modal.Footer>
            </Modal>

        </main >

    }

}

export default checkAuthentication(ContactusManagement);



