import React, { Component } from 'react'
import Navbar from '../Component/Navbar'
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button } from "react-bootstrap"
import _ from 'lodash';
import { CSVLink, CSVDownload } from "react-csv";
import ReactTooltip from 'react-tooltip'; // For tool tip 

import { checkAuthentication } from '../Component/Authentication';

var path = cred.API_PATH;


export class AdminConference extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userId: '',
            userType: '',

            openSideMenu: false,
            searchString: '',
            searching: false,

            loading: false,
            searchLoading: false,

            adminCompanyId: '',
            adminCompanyName: '',
            adminCompanyType: '',

            companyLists: [],
            companyRecords: '',
            pageNo: 1,

        }
        this.perPage = 10;
    }

    componentDidMount() {
        document.body.classList.remove("sign-bg");
        document.body.classList.add("grey-bg");
        let that = this;

        let storage = localStorage.getItem("MIO_Local");
        storage = storage ? JSON.parse(storage) : null;
        if (storage && storage.data.userId) {
            this.setState({
                userId: storage.data.userId,
                userType: storage.data.userType,
                companyId: storage.data.companyId,
                adminCompanyId: storage.data.companyId,
                adminCompanyName: storage.data.companyName,
                adminCompanyType: storage.data.companyType
            }, () => {
                that.getCompanyListForAdmin();
                console.log('user management--', this.state.userType);
            });
            // console.log(this.parseJwt(storage.token));
            let tokenData = this.parseJwt(storage.token);

        }


    }

    // For getting company list -- 
    getCompanyListForAdmin = () => {
        this.setState({ loading: true });

        let dataToSend = {
            "page": this.state.pageNo,
            "perPage": this.perPage
        };
        // API --
        axios
            .post(path + 'user/get-companies', dataToSend)
            .then(serverResponse => {
                let res = serverResponse.data;
                if (!res.isError) {
                    let result = res.details && res.details[0] && res.details[0] && res.details[0].results;
                    let totalRows = res.details && res.details[0] && res.details[0] && res.details[0].noofpage;

                    this.setState({
                        companyLists: result,
                        companyRecords: totalRows,
                        loading: false
                    });
                } else {
                    this.setState({
                        loading: false
                    })
                }
            })
            .catch(error => {
                console.log(error);
            })

    }

    // For searching user - 
    searchCompany = (event) => {
        event.preventDefault();
        if (!this.state.searchString || (this.state.searchString && this.state.searchString.length < 2)) {
            alert("Please valid search");
        } else {

            this.setState({
                searching: true,
                searchLoading: true
            });

            let dataToSend = {
                companyType: "",
                companyName: this.state.searchString
            }
            // API ToDo - 
            axios
                .post(path + 'user/search-companies', dataToSend)
                .then(serverResponse => {
                    let res = serverResponse.data;
                    if (!res.isError) {
                        this.setState({
                            searchLoading: false,
                            companyLists: res.details
                        }, () => {
                            console.log("company Lists-----", this.state.companyLists);
                        });
                    } else {
                        this.setState({ searchLoading: false })
                        alert("Something is wrong...");
                    }
                })
                .catch(error => {
                    this.setState({ searchLoading: false })
                    console.log(error);
                })


        }

    }

    // For clearing search -
    clearSearch = (event) => {
        event.preventDefault();
        this.setState({
            searchString: '',
            searching: false,
            searchLoading: false
        }, () => {
            this.getCompanyListForAdmin();
        })
    }

    // For userList of a particular company - 
    joinConference = (index, name) => {

        if (name && name == "admin") {
            this.props.history && this.props.history.push({
                pathname: `/admincompanyconference/${index}`,
                state: {
                    companyName: this.state.adminCompanyName
                }
            });
        } else {
            let { companyLists } = this.state;
            let company = companyLists && companyLists[index];
            let { _id, companyName } = company;

            // linking to another url to get user's details of a company in list -- 
            // this.props.history && this.props.history.push(`/admincompanyconference/${_id}`);
            this.props.history && this.props.history.push({
                pathname: `/admincompanyconference/${_id}`,
                state: {
                    companyName: companyName
                }
            });
        }

    }

    openConferenceList = (index, name) => {
        // linking to another url to get conference list of that company -- 
        if (name && name == "admin") {
            this.props.history.push({
                pathname: '/adminconferencelist/' + `${index}`,
                state: { name: this.state.adminCompanyName, type: this.state.adminCompanyType }
            });
        } else {
            let selectedCompanyId = this.state.companyLists && this.state.companyLists[index] && this.state.companyLists[index]._id;
            let selectedCompanyname = this.state.companyLists && this.state.companyLists[index] && this.state.companyLists[index].companyName;
            let selectedCompanyType = this.state.companyLists && this.state.companyLists[index] && this.state.companyLists[index].companyType;

            // this.props.history && this.props.history.push(`/adminconferencelist/${1}`);

            this.props.history.push({
                pathname: '/adminconferencelist/' + `${selectedCompanyId}`,
                state: { name: selectedCompanyname, type: selectedCompanyType }
            });
        }
    }

    pageChangePrev = () => {

        this.setState({
            pageNo: (parseFloat(this.state.pageNo) - 1)
        }, () => {
            this.getCompanyListForAdmin()
        });

    }

    pageChangeNext = () => {

        this.setState({
            pageNo: (parseFloat(this.state.pageNo) + 1)
        }, () => {
            this.getCompanyListForAdmin()
        });

    }

    sideMenuToggle = () => {
        let { openSideMenu } = this.state;
        openSideMenu = !openSideMenu;
        this.setState({ openSideMenu });
    }

    mobMenuClick() {
        this.sideMenuToggle();
        console.log('mobMenuClick');
    }

    parseJwt = (token) => {
        if (!token) { return; }
        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace('-', '+').replace('_', '/');
        return JSON.parse(window.atob(base64));
    }


    render() {
        let { loading, searchLoading, companyLists, searching, pageNo, companyRecords } = this.state;

        return <main>
            <section className="user-mngnt-wrap">
                <div className="container-fluid">
                    <div className="row">

                        <Navbar routTo="/adminconference" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />
                        <div className="col-xl-10 col-lg-9 p-0 mob-full-conf">

                            <div className="mobile-menu-header">
                                {/* <a className="back-arw" onClick={() => console.log('<-BACK')}><i className="fas fa-arrow-left"></i></a> */}
                                <a href="JavaScript:Void(0);" className="menu-tgl" onClick={() => this.sideMenuToggle()}><img src="images/menu-tgl.png" alt="" /></a>
                            </div>
                            <div className="mng-full-list">

                                <div className="mng-full-srch">
                                    <div className="row">
                                        <div className="col-md-10">
                                            <div className="srch-wrap">
                                                <form>
                                                    <input value={this.state.searchString} onChange={(event) => this.setState({ searchString: event.target.value })} type="text" name="" className="form-control" placeholder="Search Company by name" />
                                                    {this.state.searching ?
                                                        <input type="button" onClick={(event) => this.clearSearch(event)}
                                                            // ref={ref => this.fooRef = ref}
                                                            data-tip='Clear'
                                                        // onFocus={() => { ReactTooltip.show(this.fooRef) }}
                                                        />
                                                        :
                                                        <input type="submit" onClick={(event) => this.searchCompany(event)}
                                                            // ref={ref => this.fooRef = ref}
                                                            data-tip='Search'
                                                        // onFocus={() => { ReactTooltip.show(this.fooRef) }}
                                                        />
                                                    }
                                                    <ReactTooltip
                                                        effect="float"
                                                        place="top"
                                                        data-border="true"
                                                    />
                                                </form>
                                            </div>
                                        </div>

                                        {searchLoading ?
                                            <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                                                <i className="fa fa-spinner fa-spin" ></i>
                                            </a>
                                            : null}

                                    </div>
                                </div>
                                {companyLists && companyLists.length > 0 ?
                                    <div className="mng-full-table">
                                        <div className="row">

                                            <div className="col-md-8">
                                                <div className="mng-full-table-hdr">
                                                    <div className="row">
                                                        <div className="col-md-4 border-rt">
                                                            <h6>Name/Company</h6>
                                                        </div>
                                                        <div className="col-md-4 border-rt">
                                                            <h6>Company Type</h6>
                                                        </div>
                                                        <div className="col-md-4">
                                                            <h6>No of Users</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="col-md-4">
                                                <div className="mng-full-table-hdr">
                                                    <div className="row">
                                                        <div className="col-md-6 border-rt">
                                                            <h6>Conference </h6>
                                                        </div>
                                                        <div className="col-md-6">
                                                            <h6>List</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div className="row">

                                            <div className="col-md-8">
                                                <div className="mng-full-table-row">
                                                    <div className="row">
                                                        <div className="col-md-4 border-rt">
                                                            <h6>Name/Company</h6>
                                                            <p className="textEllips">
                                                                <a href="JavaScript:Void(0);" onClick={() => this.joinConference(this.state.adminCompanyId, "admin")}>
                                                                    {this.state.adminCompanyName}
                                                                </a>
                                                            </p>
                                                        </div>
                                                        <div className="col-md-4 border-rt">
                                                            <h6>Company Type</h6>
                                                            <p className="textEllips">{this.state.adminCompanyType}</p>
                                                        </div>
                                                        <div className="col-md-4">
                                                            <h6>No of Users</h6>
                                                            <p className="textEllips">-</p>
                                                        </div>
                                                        <div className="mobile-ad-edt-btns">
                                                            <ul>
                                                                <li onClick={() => this.joinConference(this.state.adminCompanyId, "admin")}>
                                                                    <a href="JavaScript:Void(0);"
                                                                        data-tip='Join'
                                                                    >
                                                                        <i className="fas fa-eye"></i>

                                                                    </a>
                                                                    <ReactTooltip
                                                                        effect="float"
                                                                        place="top"
                                                                        data-border="true"
                                                                    />
                                                                </li>
                                                            </ul>
                                                            <ul>
                                                                <li onClick={() => this.openConferenceList(this.state.adminCompanyId, "admin")}>
                                                                    <a href="JavaScript:Void(0);"
                                                                        data-tip='Conference List'
                                                                    >
                                                                        <i className="fa fa-bars" aria-hidden="true"></i>

                                                                    </a>
                                                                    <ReactTooltip
                                                                        effect="float"
                                                                        place="top"
                                                                        data-border="true"
                                                                    />
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="col-md-4">
                                                <div className="mng-full-table-row add-edt text-center">
                                                    <div className="row">
                                                        <div className="col-md-6">
                                                            <a href="JavaScript:Void(0);"
                                                                data-tip='Join'
                                                                onClick={() => { this.joinConference(this.state.adminCompanyId, "admin") }}
                                                            >
                                                                <i className="fas fa-eye"></i>

                                                            </a>
                                                            <ReactTooltip
                                                                effect="float"
                                                                place="top"
                                                                data-border="true"
                                                            />
                                                        </div>
                                                        <div className="col-md-6">
                                                            <a href="JavaScript:Void(0);"
                                                                data-tip='Conference List'
                                                                onClick={() => { this.openConferenceList(this.state.adminCompanyId, "admin") }}
                                                            >
                                                                <i className="fas fa-eye"></i>

                                                            </a>
                                                            <ReactTooltip
                                                                effect="float"
                                                                place="top"
                                                                data-border="true"
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        {companyLists && companyLists.length > 0 ? companyLists.map((company, index) => {
                                            return <div className="row" key={index}>
                                                <div className="col-md-8">
                                                    <div className="mng-full-table-row">

                                                        <div className="row">
                                                            <div className="col-md-4 border-rt">
                                                                <h6>Name/Company</h6>
                                                                <p className="textEllips">
                                                                    <a href="JavaScript:Void(0);" onClick={() => this.joinConference(index)}>
                                                                        {company.companyName}
                                                                    </a>
                                                                </p>
                                                            </div>
                                                            <div className="col-md-4 border-rt">
                                                                <h6>Company Type</h6>
                                                                <p className="textEllips">{company.companyType}</p>
                                                            </div>
                                                            <div className="col-md-4">
                                                                <h6>No of Users</h6>
                                                                <p className="textEllips">2</p>
                                                            </div>

                                                            <div className="mobile-ad-edt-btns">
                                                                <ul>
                                                                    <li onClick={() => this.joinConference(index)}>
                                                                        <a href="JavaScript:Void(0);"
                                                                            data-tip='Join'
                                                                        >
                                                                            <i className="fas fa-eye"></i>

                                                                        </a>
                                                                        <ReactTooltip
                                                                            effect="float"
                                                                            place="top"
                                                                            data-border="true"
                                                                        />
                                                                    </li>
                                                                </ul>
                                                                <ul>
                                                                    <li onClick={() => this.openConferenceList(index)}>
                                                                        <a href="JavaScript:Void(0);"
                                                                            data-tip='Conference List'
                                                                        >
                                                                            <i className="fa fa-bars" aria-hidden="true"></i>

                                                                        </a>
                                                                        <ReactTooltip
                                                                            effect="float"
                                                                            place="top"
                                                                            data-border="true"
                                                                        />
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>

                                                <div className="col-md-4">
                                                    <div className="mng-full-table-row add-edt text-center">
                                                        <div className="row">
                                                            <div className="col-md-6">
                                                                <a href="JavaScript:Void(0);"
                                                                    data-tip='Join'
                                                                    onClick={() => { this.joinConference(index) }}
                                                                >
                                                                    <i className="fas fa-eye"></i>

                                                                </a>
                                                                <ReactTooltip
                                                                    effect="float"
                                                                    place="top"
                                                                    data-border="true"
                                                                />
                                                            </div>
                                                            <div className="col-md-6">
                                                                <a href="JavaScript:Void(0);"
                                                                    data-tip='Conference List'
                                                                    onClick={() => { this.openConferenceList(index) }}
                                                                >
                                                                    <i className="fas fa-eye"></i>

                                                                </a>
                                                                <ReactTooltip
                                                                    effect="float"
                                                                    place="top"
                                                                    data-border="true"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        })
                                            : null}

                                    </div>
                                    :
                                    <div className="mng-full-table">
                                        <div className="row">
                                            <div className="col-md-12">
                                                <div className="mng-full-table-hdr admn-usr-hdr">
                                                    <div className="row">
                                                        <div className="col-md-12">
                                                            <h6>No Company Found.</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                }
                                <br /><br />
                                {!searching ?
                                    <div className="form-group">
                                        {!loading && parseFloat(pageNo) > 1 ?
                                            <span style={{ color: "#007bff", cursor: "pointer" }}
                                                onClick={() => { this.pageChangePrev() }}
                                            >
                                                <b>{"<< Prev"}</b>
                                            </span>
                                            :
                                            null}
                                        &nbsp;&nbsp;|&nbsp;&nbsp;
                                        {!loading && (parseFloat(pageNo) < parseFloat(companyRecords)) ?
                                            <span style={{ color: "#007bff", cursor: "pointer" }}
                                                onClick={() => { this.pageChangeNext() }}
                                            >
                                                <b>{"Next >>"}</b>
                                            </span>
                                            :
                                            null}
                                        {loading ?
                                            <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                                                <i className="fa fa-spinner fa-spin" ></i>
                                            </a>
                                            :
                                            null}

                                    </div>
                                    :
                                    null}

                            </div>

                        </div>
                    </div>
                </div>

            </section>
        </main>

    }

}

export default checkAuthentication(AdminConference);