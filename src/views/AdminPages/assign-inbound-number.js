import React, { Component } from 'react'
import Navbar from '../Component/Navbar'
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button } from "react-bootstrap"
import _ from 'lodash';
import { CSVLink, CSVDownload } from "react-csv";
import ReactTooltip from 'react-tooltip'; // For tool tip 

import { checkAuthentication } from '../Component/Authentication';

var path = cred.API_PATH;


export class AssignNumber extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userId: '',
            userType: '',
            selectedCompanyName: '',
            selectedCompanyId: '',

            openSideMenu: false,
            searchCountry: '',
            searchProvider: '',
            searching: false,

            loading: false,
            loading_1: false,
            loading_2: false,

            pageNo: 1,
            pageNo_1: 1,
            usersList: [],
            numberListRecords: '',
            assignedNumbersRecords: '',

            assignedNumbers: [],
            assignedNumberIds: [],
            SIPProviderList: [],
            countryList: [],
            selectedArray: [],

            selectedNumber: {},

            showEditModal: false,
            countryCode: '',
            countryCodeError: '',
            phoneNumber: '',
            phoneNumberError: '',
            sipProvider: '',
            sipProviderError: '',
            country: '',
            countryError: '',
            costPerMinute: '',
            costPerMinuteError: '',
            isActive: false,

            showSuccessAlert: false,
            successAlertMessage: '',
            showErrorAlert: false,
            errorAlertMessage: '',

        }
        this.perPage = 5;

    }

    componentDidMount() {

        document.body.classList.remove("sign-bg");
        document.body.classList.add("grey-bg");

        let storage = localStorage.getItem("MIO_Local");
        storage = storage ? JSON.parse(storage) : null;

        let state = this.props.location.state;
        console.log("state ==", state);

        if (storage && storage.data.userId && state.companyName) {
            this.setState({
                userId: storage.data.userId,
                userType: storage.data.userType,
                companyId: storage.data.companyId,
                selectedCompanyName: state.companyName,
                selectedCompanyId: this.props.match.params.id
            }, () => {
                    this.loadData()
                    this.getCountrylist();
                    this.getSIPProvidersList();
                    console.log('user management--', this.state.userType);
            });
            let tokenData = this.parseJwt(storage.token);
        }

    }

    loadData= async()=>{
        let companyDetails = await this.getSelectedCompanyDetails();
        let NumberList = await this.getNumberListForAdmin();
    } 

    getSelectedCompanyDetails = () => {

        return new Promise((resolve, reject) => {
            let dataToSend = {
                "companyId": this.state.selectedCompanyId,
                "page": this.state.pageNo_1,
                "perPage": this.perPage
            }
            this.setState(prevState=>({loading_1: true}));
            // API - 
            axios
            .post(path+'user/get-inbound-numbers-by-companyid', dataToSend)
            .then(serverResponse=>{
                let res = serverResponse.data;
                if(!res.isError){
                   
                    // let assignedNumberIds = [];
                    // res.details && res.details[0].results && res.details[0].results.map(value => {
                    //     assignedNumberIds.push(value._id);
                    // })

                    this.setState({
                        loading_1: false,
                        assignedNumbers: res.details && res.details[0] && res.details[0].results,
                        assignedNumbersRecords: res.details && res.details[0] && res.details[0].noofpage,
                        // assignedNumberIds: assignedNumberIds
                    },()=>{
                        resolve();
                    });
                }else{
                    this.setState({
                        loading_1: false
                    },()=>{
                        reject();
                    });
                }
            })
            .catch(error=>{
                console.log(error);
            })

        })

    }

    // For getting number list -- 
    getNumberListForAdmin = (sipProvider, country) => {
        return new Promise((resolve, reject) => {
            let dataToSend;
            if (sipProvider || country) {
                dataToSend = {
                    companyId: this.state.selectedCompanyId,
                    pageno: this.state.pageNo,
                    perPage: this.perPage,
                    sipProvider: sipProvider,
                    country: country
                };

            } else {
                dataToSend = {
                    companyId: this.state.selectedCompanyId,
                    pageno: this.state.pageNo,
                    perPage: this.perPage,
                    sipProvider: '',
                    country: ''
                };
            }
            this.setState(prevState=>({ loading_2: true }));

            axios
                .post(path + 'services/get-inbound-numbers', dataToSend)
                .then(serverResponse => {
                    let res = serverResponse.data;
                    console.log("Number list == ", res);
                    if (!res.isError) {
                        let details = res.details;
                        let results = details && details[0].results;
                        let numberListRecords = details && details[0].total;

                        this.setState({
                            loading_2: false,
                            usersList: results && results,
                            numberListRecords: numberListRecords && numberListRecords
                        }, () => {
                            resolve();
                        });
                    } else {
                        this.setState({
                            loading_2: false
                        }, () => {
                            reject();
                        });
                        console.log('something goes wrong.');
                    }
                })
                .catch(error => {
                    console.log(error);
                    this.setState({
                        loading_2: false
                    })
                })

        })

    }

    // For getting country list for filter -- 
    getCountrylist = () => {
        let dataToSend = {
            "pageno": '',
            "perPage": '',
        };
        this.setState({loading: true});
        axios
            .post(path + 'services/getCountries', dataToSend)
            .then(serverResponse => {

                let res = serverResponse.data;
                if (!res.isError) {
                    // console.log("country list ==", res);
                    this.setState({
                        loading: false,
                        countryList: res.details
                    });
                }else{
                    this.setState({
                        loading: false
                    });
                }

            })
            .catch(error => {
                this.setState({
                    loading: false
                })
                console.log(error);
            })

    }

    // For getting SIP Provider lists --
    getSIPProvidersList = () => {
        let dataToSend = {
            "pageno": '',
            "perPage": '',
        };
        this.setState({loading: true});
        axios
            .post(path + 'services/get-sip-providers', dataToSend)
            .then(serverResponse => {

                let res = serverResponse.data;
                if (!res.isError) {

                    // console.log("country list ==", res);
                    this.setState({
                        loading: false,
                        SIPProviderList: res.details
                    });

                }else{
                    this.setState({
                        loading: false
                    });
                }

            })
            .catch(error => {
                this.setState({
                    loading: false
                })
                console.log(error);
            })

    }

    // For searching user - 
    searchNumber = (event) => {
        event.preventDefault();
        let { searchCountry, searchProvider } = this.state;
        this.setState({
            searching: true
        }, () => {
            this.getNumberListForAdmin(searchProvider, searchCountry);
        });

    }

    // For clearing search -
    clearSearch = (event) => {
        if (event) {
            event.preventDefault();
        }
        // let {searchCountry, searchProvider, searching} = this.state;
        this.setState({
            searchCountry: '',
            searchProvider: '',
            searching: false
        }, () => {
            this.getNumberListForAdmin()
        });
    }
 
    // For userList of a particular company - 
    editNumber = (index) => {

        let { usersList } = this.state;
        let selectedNumber = usersList[index];

        this.setState({
            selectedNumber: selectedNumber,
            isActive: selectedNumber.inboundNumbers.isActive,
            countryCode: selectedNumber.inboundNumbers.countryCode,
            phoneNumber: selectedNumber.inboundNumbers.phoneNumber,
            sipProvider: selectedNumber.inboundNumbers.sipProvider,
            numberType: selectedNumber.inboundNumbers.numberType,
            country: selectedNumber.inboundNumbers.country,
            costPerMinute: selectedNumber.inboundNumbers.costPerMinute,
            showEditModal: true
        });

    }

    UpdateNumber = () => {

        let { countryCode, phoneNumber, sipProvider, numberType, country,
            costPerMinute, isActive, loading, selectedNumber } = this.state;

        countryCode && !isNaN(countryCode) ? this.setState({ countryCodeError: '' }) : this.setState({ countryCodeError: '' })
        phoneNumber && !isNaN(phoneNumber) ? this.setState({ phoneNumberError: '' }) : this.setState({ phoneNumberError: '' })
        sipProvider ? this.setState({ sipProviderError: '' }) : this.setState({ sipProviderError: '' })
        country ? this.setState({ countryError: '' }) : this.setState({ countryError: '' })

        if (countryCode && !isNaN(countryCode) && phoneNumber && !isNaN(phoneNumber) && sipProvider && country) {
            this.setState({
                loading: true
            });
            let dataToSend = {
                "numberId": selectedNumber.inboundNumbers._id,
                // "compnayId": this.state.companyId,
                "countryCode": countryCode,
                "phoneNumber": phoneNumber,
                "phone": countryCode + phoneNumber,
                "sipProvider": sipProvider,
                "numberType": numberType,
                "country": country,
                "costPerMinute": costPerMinute,
                "isActive": isActive,
            };
            // API -- 
            axios
                .post(path + 'services/update-inbound-number', dataToSend)
                .then(serverResponse => {

                    let res = serverResponse.data;
                    if (!res.isError) {

                        alert("Successfully updated");
                        this.setState({
                            loading: false
                        }, () => {
                            this.hideEditModal();
                            this.getNumberListForAdmin();
                        });

                    } else {
                        alert("Something went wrong");
                        this.setState({
                            loading: false
                        }, () => {
                            this.hideEditModal();
                        });
                    }

                })
                .catch(error => {
                    console.log(error);
                })

        }

    }

    hideEditModal = () => {

        this.setState({
            loading: false,
            viewAssignModal: false            
        });

    }

    changeCheckStatus = (index) => {

        let { usersList } = this.state;

        let changedUserList = usersList[index];
        if (changedUserList.inboundNumbers.isAssigned) {
            changedUserList.inboundNumbers.isAssigned = !changedUserList.inboundNumbers.isAssigned;
        } else {
            changedUserList.inboundNumbers["isAssigned"] = true;
        }

        usersList[index] = changedUserList;
        this.setState({
            usersList: usersList
        });

    }

    assignNumbers = () => { 

        let { usersList } = this.state;
        let selectedArray = [];

        usersList.map((value, index) => {
            if (value.inboundNumbers.isAssigned) {
                delete value.inboundNumbers["isAssigned"];
                delete value.inboundNumbers["_id"];
                
                selectedArray.push(value.inboundNumbers);
            }
        });

        this.setState({
            selectedArray: selectedArray,
            viewAssignModal: true
        });
        
    }

    changeAssignForm= (event, key)=> {

        let {selectedArray} = this.state;

        let value = event.target.value;
        let name = event.target.name;
        if(name == "pricePerMinute"){
            selectedArray.map((data, index)=>{
                if(index == key){
                    data["pricePerMinute"] = value;
                    data["multiplier"] = !isNaN(parseFloat(data.costPerMinute)/parseFloat(value)) ? parseFloat(parseFloat(value)/parseFloat(data.costPerMinute)).toFixed('2'): 0.00;
                }
            })
            this.setState({selectedArray:selectedArray});

        }else if(name == "multiplier"){
            selectedArray.map((data, index)=>{
                if(index == key){
                    data["multiplier"] = value;
                    data["pricePerMinute"] = !isNaN(parseFloat(data.costPerMinute)*parseFloat(value))? parseFloat(parseFloat(data.costPerMinute)*parseFloat(value)).toFixed('2'): 0.00;
                }
            })
            this.setState({selectedArray:selectedArray});

        }else if(name == "customPricePerMinute"){
            selectedArray.map((data, index)=>{
                if(index == key){
                    data["customPricePerMinute"] = value;
                }
            })
            this.setState({selectedArray:selectedArray});

        }
        
    }

    UpdateAssignNumber= ()=>{

        let {selectedArray, selectedCompanyId} = this.state;

        this.setState({ loading: true });

        let dataToSend = {
            "companyId": selectedCompanyId,
            "numbers": selectedArray
        };
        // API --
        axios
            .post(path + 'user/assign-inbound-numbers', dataToSend)
            .then(serverResponse => {
                let res = serverResponse.data;
                if (!res.isError) {
                    // console.log("response of assign number ==", res);
                    this.setState({
                        loading: false,
                        selectedArray: [],
                        viewAssignModal: false,
                        showSuccessAlert: true,
                        successAlertMessage: 'Successfully assigned.',
                        searchCountry: '',
                        searchProvider: '',
                        searching: false
                    }, () => {
                        this.loadData();
                        // this.getSelectedCompanyDetails().then(() => {
                        //     this.getNumberListForAdmin();
                        // })
                    });
                } else {
                    this.setState({
                        loading: false,
                        selectedArray: [],
                        viewAssignModal: false,
                        showErrorAlert: true,
                        errorAlertMessage: 'Sorry! something is wrong.',
                    }, () => {
                        // this.clearSearch();
                        this.getSelectedCompanyDetails().then(() => {
                            this.getNumberListForAdmin();
                        })
                    });
                }
            })
            .catch(error => {
                console.log(error);
            }) 


    }

    changeUnassignStatus = (index) => {

        let { assignedNumbers } = this.state;

        let changedUserList = assignedNumbers[index];
        if (changedUserList.isChecked) {
            changedUserList.isChecked = !changedUserList.isChecked;
        } else {
            changedUserList["isChecked"] = true;
        }

        assignedNumbers[index] = changedUserList;
        this.setState({
            assignedNumbers: assignedNumbers
        });

    }

    unassignNumbers = () => {

        let { assignedNumbers, selectedCompanyId } = this.state;
        let unAssignArray = [];

        assignedNumbers.map((value, index) => {
            if (value.isChecked) {
                delete value["isChecked"];
                unAssignArray.push(value._id);
            }
        });
        this.setState({ 
            loading: true,
            assignedNumbers: assignedNumbers
        },()=>{
            console.log("unassign time assignedNumbers", this.state.assignedNumbers);
        });

        let dataToSend = {
            "companyId": selectedCompanyId,
            "numberIds": unAssignArray
        };

        axios
            .post(path + 'user/unassign-inbound-numbers', dataToSend)
            .then(serverResponse => {
                let res = serverResponse.data;
                if (!res.isError) {
                    this.setState({
                        loading: false,
                        showSuccessAlert: true,
                        successAlertMessage: 'Successfully un-assigned.',
                        searchCountry: '',
                        searchProvider: '',
                        searching: false
                    }, () => {
                        // this.clearSearch();
                        this.getSelectedCompanyDetails().then(() => {
                            this.getNumberListForAdmin();
                        });
                    });
                } else {
                    this.setState({
                        loading: false,
                        showErrorAlert: true,
                        errorAlertMessage: 'Sorry! something is wrong.'
                    }, () => {
                        // this.clearSearch();
                        this.getSelectedCompanyDetails().then(() => {
                            this.getNumberListForAdmin();
                        });
                    });
                }
            })
            .catch(error => {
                console.log(error);
            })

    }

    pageChangePrev = () => {

        this.setState({
            pageNo: (parseFloat(this.state.pageNo) - 1)
        }, () => {
            this.getNumberListForAdmin()
        });

    }

    pageChangeNext = () => {

        this.setState({
            pageNo: (parseFloat(this.state.pageNo) + 1)
        }, () => {
            this.getNumberListForAdmin()
        });

    }

    pageChangePrev_1= ()=> {

        this.setState({
            pageNo_1: (parseFloat(this.state.pageNo_1) - 1)
        }, () => {
            this.getSelectedCompanyDetails()
        });

    }

    pageChangeNext_1= ()=> {

        this.setState({
            pageNo_1: (parseFloat(this.state.pageNo_1) + 1)
        }, () => {
            this.getSelectedCompanyDetails()
        });

    }

    clearAlert = () => {
        this.setState({
            loading: false,
            showSuccessAlert: false,
            successAlertMessage: '',
            showErrorAlert: false,
            errorAlertMessage: '',
        });
    }

    sideMenuToggle = () => {
        let { openSideMenu } = this.state;
        openSideMenu = !openSideMenu;
        this.setState({ openSideMenu });
    }

    backMenueToggle = () => {
        this.props.history.goBack();
    }

    mobMenuClick() {
        this.sideMenuToggle();
        console.log('mobMenuClick');
    }

    parseJwt = (token) => {
        if (!token) { return; }
        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace('-', '+').replace('_', '/');
        return JSON.parse(window.atob(base64));
    }


    render() {

        let { loading, loading_1, loading_2, selectedNumber, assignedNumbers, assignedNumberIds, usersList, pageNo, pageNo_1, numberListRecords, selectedArray, 
            SIPProviderList, countryList, assignedNumbersRecords } = this.state;
        const perPage = this.perPage;

        return <main>
            <section className="user-mngnt-wrap">
                <div className="container-fluid">
                    <div className="row">

                        <Navbar routTo="/numberconfigue" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />
                        <div className="col-xl-10 col-lg-9 p-0 mob-full-conf">

                            <div className="mobile-menu-header">
                                <a href= "JavaScript:Void(0);" className="back-arw" onClick={() => this.backMenueToggle()}><i className="fas fa-arrow-left"></i></a>
                                {/* <a href="JavaScript:Void(0);" className="menu-tgl" onClick={() => this.sideMenuToggle()}><img src="images/menu-tgl.png" alt="" /></a> */}
                            </div>

                            <div className="mng-full-list">

                                <div className="mng-full-srch">
                                    <div className="row">
                                        <div className="col-margin col-md-4">
                                            <select className="form-control"
                                                name="searchType"
                                                value={this.state.searchCountry}
                                                onChange={(event) => { this.setState({ searchCountry: event.target.value }) }}
                                            >
                                                <option value="">Select Country</option>
                                                {countryList.map((value, index) => {
                                                    return <option value={value._id.country} key={index}>
                                                        {value._id.country}
                                                    </option>
                                                })}
                                            </select>
                                        </div>
                                        <div className="col-margin col-md-6">
                                            <select className="form-control"
                                                name="searchType"
                                                value={this.state.searchProvider}
                                                onChange={(event) => { this.setState({ searchProvider: event.target.value }) }}
                                            >
                                                <option value="">Select SIP Provider</option>
                                                {SIPProviderList.map((value, index) => {
                                                    return <option value={value._id.sipProvider} key={index}>
                                                        {value._id.sipProvider}
                                                    </option>
                                                })}
                                            </select>
                                        </div>
                                        <div className="col col-md-2">
                                            <div className="srch-wrap">
                                                <form>
                                                    {this.state.searching ?
                                                        <input type="button"
                                                            data-tip='Clear'
                                                            onClick={(event) => this.clearSearch(event)}
                                                        />
                                                        :
                                                        <input type="submit"
                                                            data-tip='Search'
                                                            onClick={(event) => this.searchNumber(event)}
                                                        />
                                                    }
                                                    <ReactTooltip
                                                        effect="float"
                                                        place="top"
                                                        data-border="true"
                                                    />
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                {assignedNumbers && assignedNumbers.length > 0 ?
                                    <div className="mng-full-table">
                                        <span>Already Assigned Inbound Numbers - </span>
                                        <div className="row">

                                            <div className="col-md-2">
                                                <div className="mng-full-table-hdr">
                                                    <div className="row">
                                                        <div className="col-md-6">
                                                            <h6>Select</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="col-md-10">
                                                <div className="mng-full-table-hdr">
                                                    <div className="row">
                                                        <div className="col-md-3 border-rt">
                                                            <h6>Phone Number</h6>
                                                        </div>
                                                        <div className="col-md-3 border-rt">
                                                            <h6>Country</h6>
                                                        </div>
                                                        <div className="col-md-3 border-rt">
                                                            <h6>SIP Provider</h6>
                                                        </div>
                                                        <div className="col-md-3">
                                                            <h6>Cost(Rs/min.)</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br/><br/>
                                            <button className="btn btn-primary" onClick={() => { this.unassignNumbers() }}>Unassign from {this.state.selectedCompanyName}</button>

                                        </div>

                                        {assignedNumbers.map((user, index) => {

                                            return <div className="row" key={index}>

                                                <div className="col-md-2">
                                                    <div className="mng-full-table-row add-edt text-center">
                                                        <div className="row">

                                                            <div className="col-md-6">
                                                                <input
                                                                    type="checkbox"
                                                                    checked ={user.isChecked ? true : false}
                                                                    onChange={() => { this.changeUnassignStatus(index) }}
                                                                />
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="col-md-10">
                                                    <div className="mng-full-table-row">
                                                        <div className="row">
                                                            <div className="col-md-3 border-rt">
                                                                <h6>Number</h6>
                                                                <p className="textEllips">
                                                                    <a href="JavaScript:Void(0);" onClick={() => this.editNumber(index)}>
                                                                        {user.countryCode+'-'+user.phoneNumber}
                                                                    </a>
                                                                </p>
                                                            </div>
                                                            <div className="col-md-3 border-rt">
                                                                <h6>Country</h6>
                                                                <p className="textEllips">{user.country}</p>
                                                            </div>
                                                            <div className="col-md-3 border-rt">
                                                                <h6>SIP Provider</h6>
                                                                <p className="textEllips">
                                                                    {user.sipProvider}
                                                                </p>
                                                            </div>
                                                            <div className="col-md-3">
                                                                <h6>Cost(/min)</h6>
                                                                <p className="textEllips">
                                                                    {parseFloat(user.costPerMinute).toFixed('2')}
                                                                </p>
                                                            </div>

                                                            <div className="mobile-ad-edt-btns">
                                                                <ul>

                                                                    {/* <li onClick={() => this.deleteNumber(index)}>
                                                                                <a href="JavaScript:Void(0);"
                                                                                    data-tip='View'
                                                                                >
                                                                                    <i className="far fa-trash-alt"></i>
                                                                                </a>
                                                                            </li> */}

                                                                    <li>
                                                                        <input
                                                                            type="checkbox"
                                                                            defaultChecked={false}
                                                                            onChange={() => { this.changeUnassignStatus(index) }}
                                                                        />
                                                                    </li>

                                                                </ul>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>

                                            </div>

                                        })}

                                    </div>
                                    :
                                    null}
                                    <br /><br />
                                    <div className="form-group">
                                        {!loading && !loading_1 && parseFloat(pageNo_1) > 1 ?
                                            <span style={{ color: "#007bff", cursor: "pointer" }}
                                                onClick={() => { this.pageChangePrev_1() }}
                                            >
                                                <b>{"<< Prev"}</b>
                                            </span>
                                            :
                                            null}
                                            &nbsp;&nbsp;|&nbsp;&nbsp;
                                            {!loading && !loading_1 && (parseFloat(pageNo_1) < parseFloat(assignedNumbersRecords)) ?
                                            <span style={{ color: "#007bff", cursor: "pointer" }}
                                                onClick={() => { this.pageChangeNext_1() }}
                                            >
                                                <b>{"Next >>"}</b>
                                            </span>
                                            :
                                            null}
                                        {loading || loading_1 ?
                                            <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                                                <i className="fa fa-spinner fa-spin" ></i>
                                            </a>
                                            :
                                            null}

                                    </div>


                                {this.state.usersList && this.state.usersList.length > 0 ?
                                    <div className="mng-full-table">
                                        <span>Available Inbound Numbers - </span>
                                        <div className="row">

                                            <div className="col-md-2">
                                                <div className="mng-full-table-hdr">
                                                    <div className="row">
                                                        <div className="col-md-6">
                                                            <h6>Select</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="col-md-10">
                                                <div className="mng-full-table-hdr">
                                                    <div className="row">
                                                        <div className="col-md-3 border-rt">
                                                            <h6>Phone Number</h6>
                                                        </div>
                                                        <div className="col-md-3 border-rt">
                                                            <h6>Country</h6>
                                                        </div>
                                                        <div className="col-md-3 border-rt">
                                                            <h6>SIP Provider</h6>
                                                        </div>
                                                        <div className="col-md-3">
                                                            <h6>Cost(Rs/min.)</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br/><br/>
                                            <button className="btn btn-primary" onClick={() => { this.assignNumbers() }}>Assign to {this.state.selectedCompanyName}</button>

                                        </div>

                                        {this.state.usersList.map((user, index) => {

                                            return user.inboundNumbers.isActive ?
                                                <div className="row" key={index}>

                                                    <div className="col-md-2">
                                                        <div className="mng-full-table-row add-edt text-center">
                                                            <div className="row">

                                                                <div className="col-md-6">
                                                                    <input
                                                                        type="checkbox"
                                                                        checked= {user.inboundNumbers.isAssigned? true : false}
                                                                        onChange={() => { this.changeCheckStatus(index) }}
                                                                    />
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="col-md-10">
                                                        <div className="mng-full-table-row">
                                                            <div className="row">
                                                                <div className="col-md-3 border-rt">
                                                                    <h6>Number</h6>
                                                                    <p className="textEllips">
                                                                        <a href="JavaScript:Void(0);" onClick={() => this.editNumber(index)}>
                                                                            {user.inboundNumbers.countryCode+'-'+user.inboundNumbers.phoneNumber}
                                                                        </a>
                                                                    </p>
                                                                </div>
                                                                <div className="col-md-3 border-rt">
                                                                    <h6>Country</h6>
                                                                    <p className="textEllips">{user.inboundNumbers.country}</p>
                                                                </div>
                                                                <div className="col-md-3 border-rt">
                                                                    <h6>SIP Provider</h6>
                                                                    <p className="textEllips">
                                                                        {user.inboundNumbers.sipProvider}
                                                                    </p>
                                                                </div>
                                                                <div className="col-md-3">
                                                                    <h6>Cost(Rs/min)</h6>
                                                                    <p className="textEllips">
                                                                        {parseFloat(user.inboundNumbers.costPerMinute).toFixed('2')}
                                                                    </p>
                                                                </div>

                                                                <div className="mobile-ad-edt-btns">
                                                                    <ul>

                                                                        <li>
                                                                            <input
                                                                                type="checkbox"
                                                                                defaultChecked={user.inboundNumbers.isAssigned}
                                                                                onChange={() => { this.changeCheckStatus(index) }}
                                                                            />
                                                                        </li>

                                                                    </ul>
                                                                </div>

                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>
                                                :
                                                null

                                        })}


                                    </div>
                                    :
                                    <div className="mng-full-table">
                                        <div className="row">
                                            <div className="col-md-12">
                                                <div className="mng-full-table-hdr admn-usr-hdr">
                                                    <div className="row">
                                                        <div className="col-md-12">
                                                            <h6>No Number Found.</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                }

                                <br /><br />
                                <div className="form-group">
                                    {!loading && !loading_2 && parseFloat(pageNo) > 1 ?
                                        <span style={{ color: "#007bff", cursor: "pointer" }}
                                            onClick={() => { this.pageChangePrev() }}
                                        >
                                            <b>{"<< Prev"}</b>
                                        </span>
                                        :
                                        null}
                                        &nbsp;&nbsp;|&nbsp;&nbsp;
                                        {!loading && !loading_2 && ((parseFloat(pageNo) * parseFloat(perPage)) < parseFloat(numberListRecords)) ?
                                        <span style={{ color: "#007bff", cursor: "pointer" }}
                                            onClick={() => { this.pageChangeNext() }}
                                        >
                                            <b>{"Next >>"}</b>
                                        </span>
                                        :
                                        null}
                                    {loading || loading_2 ?
                                        <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                                            <i className="fa fa-spinner fa-spin" ></i>
                                        </a>
                                        :
                                        null}

                                </div>

                            </div>

                        </div>
                    </div>
                </div>

            </section>

            <Modal size="lg" show={this.state.viewAssignModal} onHide={() => { this.hideEditModal() }}>
                <Modal.Header closeButton>
                    <Modal.Title>Assign Number</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    
                    {selectedArray && selectedArray.map((data,index)=>{
                        return<div className="row" key={index}>
                            <div className="col-md-2">
                                <div className="form-group">
                                    <label>Number:</label>
                                    <input
                                        type="text"
                                        name="phoneNumber"
                                        value={data.phone}
                                        className="form-control"
                                        placeholder="Enter Phone no."
                                        onChange={event => this.setState({ phoneNumber: event.target.value })}
                                        onFocus={() => this.setState({ phoneNumberError: "" })}
                                        readOnly={this.state.viewUser}
                                        readOnly = {true}
                                    />
                                    
                                </div>
                            </div>

                            <div className="col-md-2">
                                <div className="form-group">
                                    <label>Country:</label>
                                    <input
                                        type="tel"
                                        name="country"
                                        value={data.country}
                                        className="form-control"
                                        //placeholder="Enter Mobile Number"
                                        onChange={event => this.setState({ country: event.target.value })}
                                        onFocus={() => this.setState({ countryError: "" })}
                                        readOnly = {true}
                                    />
                                    
                                </div>
                            </div>

                            <div className="col-md-2">
                                <div className="form-group">
                                    <label>Cost(/min):</label>
                                    <input
                                        type="text"
                                        name="costPerMinute"
                                        value={parseFloat(data.costPerMinute).toFixed('2')}
                                        className="form-control"
                                        placeholder="Rs/min"
                                        onChange={event => this.setState({ costPerMinute: event.target.value })}
                                        onFocus={() => this.setState({ costPerMinuteError: "" })}
                                        readOnly = {true}
                                    />
                                    
                                </div>
                            </div>

                            <div className="col-md-2">
                                <div className="form-group">
                                    <label>Multiplier:</label>
                                    <input
                                        type="text"
                                        name="multiplier"
                                        value={(data.multiplier)? (data.multiplier): ''}
                                        className="form-control"
                                        placeholder="0.00"
                                        onChange={(event) => {this.changeAssignForm(event, index)} }
                                    />
                                    <span style={{ color: 'red' }}>{!data.multiplier ? '* Please give valid multiplier' : ''}</span>
                                </div>
                            </div>
                            
                            <div className="col-md-2">
                                <div className="form-group">
                                    <label>Price:</label>
                                    <input
                                        type="text"
                                        name="pricePerMinute"
                                        value={(data.pricePerMinute)? (data.pricePerMinute): ''}
                                        className="form-control"
                                        placeholder="Rs/min"
                                        onChange={(event) => {this.changeAssignForm(event, index)} }
                                    />
                                    <span style={{ color: 'red' }}>{!data.pricePerMinute ? '* Please give valid price' : ''}</span>
                                </div>
                            </div>

                            <div className="col-md-2">
                                <div className="form-group">
                                    <label>Custom price:</label>
                                    <input
                                        type="text"
                                        name="customPricePerMinute"
                                        value={(data.customPricePerMinute)? (data.customPricePerMinute) : ''}
                                        className="form-control"
                                        placeholder="Rs/min."
                                        onChange={(event) => {this.changeAssignForm(event, index)} }
                                    />
                                    
                                </div>
                            </div>
                        
                        </div>
                    }) }
                    
                </Modal.Body>
                <Modal.Footer>
                    {!loading ?
                        <Button variant="primary" onClick={() => this.UpdateAssignNumber()}>
                            Save
                        </Button>
                        :
                        null}
                    {loading ?
                        <Button variant="primary" >
                            <i className="fa fa-spinner fa-spin" ></i>
                        </Button>
                        :
                        null}
                </Modal.Footer>
            </Modal>

            <Modal show={this.state.showSuccessAlert} onHide={() => this.clearAlert()}>
                <Modal.Header closeButton>
                    <Modal.Title>OK</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h5 style={{ color: 'green' }}>{`${this.state.successAlertMessage}`}</h5>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={() => this.clearAlert()}>
                        Ok
            </Button>
                </Modal.Footer>
            </Modal>

            <Modal show={this.state.showErrorAlert} onHide={() => this.clearAlert()}>
                <Modal.Header closeButton>
                    <Modal.Title>OK</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h5 style={{ color: 'red' }}>{`${this.state.errorAlertMessage}`}</h5>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={() => this.clearAlert()}>
                        Ok
            </Button>
                </Modal.Footer>
            </Modal>

        </main>

    }

}

export default checkAuthentication(AssignNumber);