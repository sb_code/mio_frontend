import React, { Component } from 'react'
import Navbar from '../Component/Navbar'
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button } from "react-bootstrap"
import _ from 'lodash';
import { CSVLink, CSVDownload } from "react-csv";
import ReactTooltip from 'react-tooltip'; // For tool tip 

import { checkAuthentication } from '../Component/Authentication';

var path = cred.API_PATH;


export class AdminCountryCodeConfiguration extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userId: '',
            userType: '',

            openSideMenu: false,
            searchCountry: '',
            searchProvider: '',
            searching: false,

            loading: false,

            pageNo: 1,
            countryCodeRecords: [],

            selectedCode: {},

            showEditModal: false,
            countryCode: '',
            countryCodeError: '',
            phoneNumber: '',
            phoneNumberError: '',
            sipProvider: '',
            sipProviderError: '',
            country: '',
            countryError: '',
            costPerMinute: '',
            costPerMinuteError: '',
            isActive: false,

            showSuccessAlert: false,
            successAlertMessage: '',
            showErrorAlert: false,
            errorAlertMessage: '',


        }
        this.perPage = 5;
    }

    componentDidMount() {

        document.body.classList.remove("sign-bg");
        document.body.classList.add("grey-bg");

        let storage = localStorage.getItem("MIO_Local");
        storage = storage ? JSON.parse(storage) : null;
        if (storage && storage.data.userId) {
            this.setState({
                userId: storage.data.userId,
                userType: storage.data.userType,
                companyId: storage.data.companyId
            }, () => {
                this.getCountryCodelist();
                console.log('user management--', this.state.userType);
            });
            let tokenData = this.parseJwt(storage.token);
        }

    }

    // For getting country code list for filter -- 
    getCountryCodelist = () => {

        this.setState({
            loading: true
        });

        let dataToSend = {};
        // API --
        axios
            .post(path + 'services/get-country-codes', dataToSend)
            .then(serverResponse => {
                let res = serverResponse.data;
                console.log("Response is comming from here -- ", res);
                if (!res.isError) {

                    let countryCodeRecords = res.details;
                    this.setState({
                        loading: false,
                        countryCodeRecords: countryCodeRecords
                    });

                } else {
                    this.setState({
                        loading: false
                    })
                }
            })
            .catch(error => {
                console.log(error);
            })

    }

    // For searching user - 
    searchNumber = (event) => {
        event.preventDefault();
        let { country } = this.state;
        this.setState({
            searching: true
        }, () => {
            this.getCountryCodelist(country);
        });

    }

    // For clearing search -
    clearSearch = (event) => {
        event.preventDefault();
        // let {searchCountry, searchProvider, searching} = this.state;
        this.setState({
            searchCountry: '',
            searchProvider: '',
            searching: false
        }, () => {
            this.getCountryCodelist()
        });
    }

    // For changing in check box --
    changeActiveStatus = (e, index) => {

        // console.log("event value =", e.target.value);
        let { countryCodeRecords } = this.state;
        let selectedCode = countryCodeRecords[index];

        selectedCode.isActive = !selectedCode.isActive;
        countryCodeRecords[index] = selectedCode;

        this.setState({
            countryCodeRecords: countryCodeRecords
        });

    }

    updateList = (event) => {

        event.preventDefault();
        let { countryCodeRecords } = this.state;

        this.setState({ loading: true });
        let dataToSend = {
            "countryCodes": countryCodeRecords
        };
        // API --
        axios
            .post(path + 'services/update-country-codes', dataToSend)
            .then(serverResponse => {
                let res = serverResponse.data;
                if (!res.isError) {
                    this.setState({
                        loading: false,
                        showSuccessAlert: true,
                        successAlertMessage: 'Successfully updated'
                    }, () => {
                        this.getCountryCodelist()
                    });
                } else {
                    this.setState({
                        loading: false,
                        showErrorAlert: true,
                        errorAlertMessage: 'Sorry! Something is wrong'
                    })
                }
            })
            .catch(error => {
                console.log(error);
            })

    }

    pageChangePrev = () => {

        this.setState({
            pageNo: (parseFloat(this.state.pageNo) - 1)
        }, () => {
            this.getCountryCodelist()
        });

    }

    pageChangeNext = () => {

        this.setState({
            pageNo: (parseFloat(this.state.pageNo) + 1)
        }, () => {
            this.getCountryCodelist()
        });

    }

    clearAlert = () => {
        this.setState({
            loading: false,
            showSuccessAlert: false,
            successAlertMessage: '',
            showErrorAlert: false,
            errorAlertMessage: '',
        });
    }

    sideMenuToggle = () => {
        let { openSideMenu } = this.state;
        openSideMenu = !openSideMenu;
        this.setState({ openSideMenu });
    }

    backMenueToggle = () => {
        this.props.history.goBack();
    }

    mobMenuClick() {
        this.sideMenuToggle();
        console.log('mobMenuClick');
    }

    parseJwt = (token) => {
        if (!token) { return; }
        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace('-', '+').replace('_', '/');
        return JSON.parse(window.atob(base64));
    }

    render() {

        let { loading, selectedNumber, pageNo, countryCodeRecords } = this.state;
        const perPage = this.perPage;

        return <main>
            <section className="user-mngnt-wrap">
                <div className="container-fluid">
                    <div className="row">

                        <Navbar routTo="/numberconfigue" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />
                        <div className="col-xl-10 col-lg-9 p-0 mob-full-conf">

                            <div className="mobile-menu-header">
                                <a href="JavaScript:Void(0);" className="back-arw" onClick={() => this.backMenueToggle()}><i className="fas fa-arrow-left"></i></a>
                                {/* <a href="JavaScript:Void(0);" className="menu-tgl" onClick={() => this.sideMenuToggle()}><img src="images/menu-tgl.png" alt="" /></a> */}
                            </div>

                            <div className="mng-full-list">

                                <div className="mng-full-srch">
                                    <div className="row">

                                        <div className="col col-md-9">
                                            <div className="srch-wrap">
                                                <form>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        name="country"
                                                        value={this.state.country}
                                                        onChange={(e) => { this.setState({ country: e.target.value }) }}
                                                    />
                                                    {this.state.searching ?
                                                        <input type="button"
                                                            data-tip='Clear'
                                                            onClick={(event) => this.clearSearch(event)}
                                                        />
                                                        :
                                                        <input type="submit"
                                                            data-tip='Search'
                                                            onClick={(event) => this.searchNumber(event)}
                                                        />
                                                    }
                                                    <ReactTooltip
                                                        effect="float"
                                                        place="top"
                                                        data-border="true"
                                                    />
                                                </form>
                                            </div>
                                        </div>
                                        <div className="col-md-3">
                                            {!loading ?
                                                <a href="JavaScript:Void(0);" className="btn" onClick={(e) => this.updateList(e)}>Update</a>
                                                :
                                                null}
                                            {loading ?
                                                <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                                                    <i className="fa fa-spinner fa-spin" ></i>
                                                </a>
                                                :
                                                null}
                                        </div>
                                    </div>

                                </div>

                                {/* {this.state.countryCodeRecords && this.state.countryCodeRecords.length > 0 ? */}

                                    <div className="mng-full-table">
                                        
                                        <div className="row" >

                                            <div className="col-md-2">
                                                <div className="mng-full-table-hdr">
                                                    <div className="row">
                                                        <h6>Select</h6>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="col-md-10">
                                                <div className="mng-full-table-hdr">
                                                    <div className="row">
                                                        <div className="col-md-6 border-rt">
                                                            <h6>Country</h6>
                                                        </div>
                                                        <div className="col-md-6">
                                                            <h6>Code</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        {this.state.countryCodeRecords && this.state.countryCodeRecords.map((value, index) => {

                                            
                                            return <div className="row" key={index}>

                                                <div className="col-md-2">
                                                    <div className="mng-full-table-row add-edt text-center">
                                                        <div className="row">

                                                            <div className="col-md-6">
                                                                <input
                                                                    type="checkbox"
                                                                    checked={value.isActive ? true : false}
                                                                    onChange={(e) => { this.changeActiveStatus(e, index) }}
                                                                />
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="col-md-10">
                                                    <div className="mng-full-table-row">
                                                        <div className="row">

                                                            <div className="col-md-6 border-rt">
                                                                <h6>Country</h6>
                                                                <p className="textEllips">
                                                                    <a href="JavaScript:Void(0);" onClick={(e) => { this.changeActiveStatus(e, index) }}>
                                                                        {value.name}
                                                                    </a>
                                                                </p>
                                                            </div>

                                                            <div className="col-md-6">
                                                                <h6>Code</h6>
                                                                <p className="textEllips">
                                                                    {value.name}
                                                                </p>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        })}
                                    
                                    </div>

                                    {/* :
                                    null} */}

                            </div>

                        </div>
                    </div>
                </div>

            </section>


            <Modal show={this.state.showSuccessAlert} onHide={() => this.clearAlert()}>
                <Modal.Header closeButton>
                    <Modal.Title>OK</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h5 style={{ color: 'green' }}>{`${this.state.successAlertMessage}`}</h5>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={() => this.clearAlert()}>
                        Ok
            </Button>
                </Modal.Footer>
            </Modal>

            <Modal show={this.state.showErrorAlert} onHide={() => this.clearAlert()}>
                <Modal.Header closeButton>
                    <Modal.Title>OK</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h5 style={{ color: 'red' }}>{`${this.state.errorAlertMessage}`}</h5>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={() => this.clearAlert()}>
                        Ok
                    </Button>
                </Modal.Footer>
            </Modal>


        </main>

    }

}

export default checkAuthentication(AdminCountryCodeConfiguration);