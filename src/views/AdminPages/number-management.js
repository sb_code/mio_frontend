import React, { Component } from "react";
import Navbar from "../Component/Navbar";
import ProgressBar from 'react-bootstrap/ProgressBar';
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button, Row, Col } from "react-bootstrap"
import ToggleButton from 'react-bootstrap/ToggleButton'
import _ from 'lodash';
import { CSVLink, CSVDownload } from "react-csv";
import ReactTooltip from 'react-tooltip'; // For tool tip 

import { checkAuthentication } from '../Component/Authentication';

var path = cred.API_PATH;

export class AdminNumbermanagement extends Component {
  constructor(props) {
    super(props);
    this.state = {

      loading: false,
      searchLoading: false,
      pageNo: 1,
      companyRecords: '',

      userId: '',

      openSideMenu: false,
      searchString: '',
      searchType: '',
      searching: false,

      companyLists: [],
      numberList: [],

      showSuccessAlert: false,
      successAlertMessage: '',
      showErrorAlert: false,
      errorAlertMessage: '',

      numberModal: false,

    }
    this.perPage = 10;
  }

  componentDidMount() {
    document.body.classList.remove("sign-bg");
    document.body.classList.add("grey-bg");

    let storage = localStorage.getItem("MIO_Local");
    storage = storage ? JSON.parse(storage) : null;
    if (storage && storage.data.userId) {
      this.setState({
        userId: storage.data.userId,
        userType: storage.data.userType,
        companyId: storage.data.companyId,
        adminCompanyId: storage.data.companyId,
        adminCompanyName: storage.data.companyName,
        adminCompanyType: storage.data.companyType
      }, () => {
        this.getCompanyListForAdmin()
        console.log('user management--', this.state.userType);
      });

      // console.log(this.parseJwt(storage.token));
      let tokenData = this.parseJwt(storage.token);

    }

  }

  getCompanyListForAdmin = () => {
    this.setState({ loading: true });

    let dataToSend = {
      "page": this.state.pageNo,
      "perPage": this.perPage
    };

    axios
      .post(path + 'user/get-companies', dataToSend)
      .then(serverResponse => {
        let res = serverResponse.data;
        if (!res.isError) {
          this.setState({
            loading: false,
            companyLists: res.details && res.details[0].results,
            companyRecords: res.details && res.details[0].noofpage
          });
        } else {
          this.setState({ loading: false });
        }
      })
      .catch(error => {
        console.log(error);
      })
  }

  searchCompany = (event) => {
    event.preventDefault();

    if (this.state.searchType == '' && this.state.searchString == '') {
      alert('Invalid search');
    } else {

      this.setState({
        searchLoading: true,
        searching: true
      });

      let DataToSend = {
        companyType: this.state.searchType,
        companyName: this.state.searchString
      };
      // console.log("dataToSend - - ", DataToSend);
      // API -- 
      axios
        .post(path + 'user/search-companies', DataToSend)
        .then(serverResponse => {
          let res = serverResponse.data;
          if (!res.isError) {
            console.log("serverResponse for search -- ", res);
            if (res.details) {
              this.setState({
                searchLoading: false,
                companyLists: res.details
              });
            } else {
              this.setState({
                searchLoading: false
              })
            }
          } else {
            this.setState({
              searchLoading: false
            })
            alert('Sorry! something is wrong.');
          }
        })
        .catch(error => {
          this.setState({
            searchLoading: false
          })
          console.log(error);
        })

    }
  }

  clearSearch = (event) => {
    event.preventDefault();
    this.setState({
      searchLoading: false,
      searchString: '',
      searchType: '',
      searching: false,
    }, () => {
      this.getCompanyListForAdmin()
    });
  }

  userInBoundAssign = (index, name) => {

    if (name && name == 'admin') {
      this.props.history.push({
        pathname: '/assigninboundnumber/' + index,
        state: {
          "companyName": this.state.adminCompanyName
        }
      });
    } else {
      let { loading, companyLists } = this.state;
      let selectedCompany = companyLists[index];

      this.props.history.push({
        pathname: '/assigninboundnumber/' + selectedCompany._id,
        state: {
          "companyName": selectedCompany.companyName
        }
      });
    }

  }

  userOutBoundAssign = (index, name) => {

    if (name && name == 'admin') {
      this.props.history.push({
        pathname: '/assignoutboundnumber/' + index,
        state: {
          "companyName": this.state.adminCompanyName
        }
      });
    } else {
      let { loading, companyLists } = this.state;
      let selectedCompany = companyLists[index];

      this.props.history.push({
        pathname: '/assignoutboundnumber/' + selectedCompany._id,
        state: {
          "companyName": selectedCompany.companyName
        }
      });
    }

  }

  hideContactModal = () => {

    this.setState({
      numberList: [],
      numberModal: false
    });

  }

  clearState = () => {
    this.setState({
      loading: false,
      editUser: false,
      confirm: false,
      showSuccessAlert: false,
      showErrorAlert: false,
      successAlertMessage: '',
      errorAlertMessage: '',
      selectedCompany: {},
      selectedCompanyId: '',
      companyServices: [],
      companyName: '',
      noOfSeats: '',
      pricePerSeat: '',
      currency: '',
      searchString: '',
      searchType: '',
      searching: false,
      invoiceBillingInfo: '',
      voiceUrl: '',
      brandUrl: '',
      fileUploaded: '',
      vat: 0.0,
    })

  }

  onAlertHide() {
    this.setState({
      showSuccessAlert: false,
      showErrorAlert: false,
      successAlertMessage: '',
      errorAlertMessage: '',
    });
  }

  pageChangePrev = () => {

    this.setState({
      pageNo: (parseFloat(this.state.pageNo) - 1)
    }, () => {
      this.getCompanyListForAdmin()
    });

  }

  pageChangeNext = () => {

    this.setState({
      pageNo: (parseFloat(this.state.pageNo) + 1)
    }, () => {
      this.getCompanyListForAdmin()
    });

  }

  sideMenuToggle() {
    let { openSideMenu } = this.state;
    openSideMenu = !openSideMenu;
    this.setState({ openSideMenu });
  }

  mobMenuClick() {
    this.sideMenuToggle();
    console.log('mobMenuClick');
  }

  parseJwt = (token) => {
    if (!token) { return; }
    const base64Url = token.split('.')[1];
    const base64 = base64Url.replace('-', '+').replace('_', '/');
    return JSON.parse(window.atob(base64));
  }


  render() {

    let { users, loading, searchLoading, searching, searchType, companyLists, numberList, companyRecords, pageNo } = this.state;

    return (
      <main>

        <section className="user-mngnt-wrap">
          <div className="container-fluid">
            <div className="row">
              <Navbar routTo="/adminnumbermanagement" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />
              <div className="col-xl-10 col-lg-9 p-0">
                <div className="mobile-menu-header">

                  <a href="JavaScript:Void(0);" className="menu-tgl" onClick={() => this.sideMenuToggle()}>
                    <img src="/images/menu-tgl.png" alt="" />
                  </a>
                </div>
                <div className="mng-full-list">
                  <div className="mng-full-srch">
                    <div className="row">
                      <div className="col-margin col-md-4">
                        <select className="form-control"
                          name="searchType"
                          onChange={(event) => { this.setState({ searchType: event.target.value }) }}
                        >
                          <option value='' selected={searchType == '' ? true : false}>Select Company Type</option>
                          <option value={`${cred.COMPANY_TYPE_FREE_TYPE}`} selected={searchType == cred.COMPANY_TYPE_FREE_TYPE ? true : false}>
                            {cred.COMPANY_TYPE_FREE_TYPE}
                          </option>
                          <option value={`${cred.COMPANY_TYPE_STARTUP}`} selected={searchType == cred.COMPANY_TYPE_STARTUP ? true : false}>
                            {cred.COMPANY_TYPE_STARTUP}
                          </option>
                          <option value={`${cred.COMPANY_TYPE_ENTERPRISE}`} selected={searchType == cred.COMPANY_TYPE_ENTERPRISE ? true : false}>
                            {cred.COMPANY_TYPE_ENTERPRISE}
                          </option>
                        </select>
                      </div>
                      <div className="col col-md-8">
                        <div className="srch-wrap">
                          <form>
                            <input
                              type="text"
                              name="searchString"
                              value={this.state.searchString}
                              onChange={(event) => { this.setState({ searchString: event.target.value }) }}
                              className="form-control"
                              placeholder="Search by company name"
                            />
                            {this.state.searching ?
                              <input type="button" onClick={(event) => this.clearSearch(event)}
                                data-tip='Clear'
                              />
                              :
                              <input type="submit" onClick={(event) => this.searchCompany(event)}
                                data-tip='Search'
                              />
                            }
                            <ReactTooltip
                              effect="float"
                              place="top"
                              data-border="true"
                            />
                          </form>
                        </div>
                      </div>
                    </div>
                    {searchLoading ?
                      <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                        <i className="fa fa-spinner fa-spin" ></i>
                      </a>
                      : null}
                  </div>
                  {companyLists && companyLists.length > 0 ?
                    <div className="mng-full-table company-mangmnt">

                      <div className="row">

                        <div className="col-md-8">
                          <div className="mng-full-table-hdr">
                            <div className="row">
                              <div className="col-md-4 border-rt">
                                <h6>Company</h6>
                              </div>
                              <div className="col-md-4 border-rt">
                                <h6>Type</h6>
                              </div>
                              <div className="col-md-4">
                                <h6>Currency</h6>
                              </div>
                              {/* <div className="col-md-3">
                                <h6>Status</h6>
                              </div> */}
                            </div>
                          </div>
                        </div>

                        <div className="col-md-4">

                        </div>

                      </div>

                      <div className="row">
                        <div className="col-md-8">

                          <div className="mng-full-table-row">
                            <div className="row">
                              <div className="col-md-4 border-rt">
                                <h6>Company</h6>
                                <p className="textEllips">
                                  <a href="JavaScript:Void(0);">
                                    {this.state.adminCompanyName}
                                  </a>
                                </p>

                              </div>
                              <div className="col-md-4 border-rt">
                                <h6>Type</h6>
                                <p>{this.state.adminCompanyType}</p>
                              </div>
                              <div className="col-md-4">
                                <h6>Currency</h6>
                                <p>-</p>
                              </div>
                              {/* <div className="col-md-3 ">
                                  <h6>Status</h6>
                                  <p>{data.inboundNumbers && data.inboundNumbers.length > 0? "Assigned" : "Unassigned"}</p>
                                </div> */}

                              <div className="mobile-ad-edt-btns">

                              </div>

                            </div>
                          </div>
                        </div>

                        <div className="col-md-2">

                          <a href="JavaScript:Void(0);"
                            className="btn border w-100 h-auto-btn"
                            data-tip='Assign In Bound'
                            onClick={() => { this.userInBoundAssign(this.state.adminCompanyId, 'admin') }}
                          >
                            In-Bound
                            </a>

                          <ReactTooltip
                            effect="float"
                            place="top"
                            data-border="true"
                          />
                        </div>

                        <div className="col-md-2">

                          <a href="JavaScript:Void(0);"
                            className="btn w-100 h-auto-btn"
                            data-tip='Assign Out Bound'
                            onClick={() => { this.userOutBoundAssign(this.state.adminCompanyId, 'admin') }}
                          >
                            Out-Bound
                            </a>

                          <ReactTooltip
                            effect="float"
                            place="top"
                            data-border="true"
                          />
                        </div>

                      </div>

                      {companyLists && companyLists.length > 0 ? companyLists.map((data, index) => {

                        return <div className="row" key={index}>
                          <div className="col-md-8">

                            <div className="mng-full-table-row">
                              <div className="row">
                                <div className="col-md-4 border-rt">
                                  <h6>Company</h6>
                                  <p className="textEllips">
                                    <a href="JavaScript:Void(0);" >
                                      {data.companyName}
                                    </a>
                                  </p>

                                </div>
                                <div className="col-md-4 border-rt">
                                  <h6>Type</h6>
                                  <p>{data.companyType}</p>
                                </div>
                                <div className="col-md-4">
                                  <h6>Currency</h6>
                                  <p>{data.currency}</p>
                                </div>
                                {/* <div className="col-md-3 ">
                                  <h6>Status</h6>
                                  <p>{data.inboundNumbers && data.inboundNumbers.length > 0? "Assigned" : "Unassigned"}</p>
                                </div> */}

                                <div className="mobile-ad-edt-btns">

                                </div>

                              </div>
                            </div>
                          </div>

                          <div className="col-md-2">

                            <a href="JavaScript:Void(0);"
                              className="btn border w-100 h-auto-btn"
                              data-tip='Assign In Bound'
                              onClick={() => { this.userInBoundAssign(index) }}
                            >
                              In-Bound
                            </a>

                            <ReactTooltip
                              effect="float"
                              place="top"
                              data-border="true"
                            />
                          </div>

                          <div className="col-md-2">

                            <a href="JavaScript:Void(0);"
                              className="btn w-100 h-auto-btn"
                              data-tip='Assign Out Bound'
                              onClick={() => { this.userOutBoundAssign(index) }}
                            >
                              Out-Bound
                            </a>

                            <ReactTooltip
                              effect="float"
                              place="top"
                              data-border="true"
                            />
                          </div>

                        </div>

                      })
                        : null}

                    </div>
                    :
                    <div className="mng-full-table">
                      <div className="row">
                        <div className="col-md-12">
                          <div className="mng-full-table-hdr admn-usr-hdr">
                            <div className="row">
                              <div className="col-md-12">
                                <h6>No Company Found.</h6>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  }

                  <br /><br />
                  {!searching ?
                    <div className="form-group">
                      {!loading && parseFloat(pageNo) > 1 ?
                        <span style={{ color: "#007bff", cursor: "pointer" }}
                          onClick={() => { this.pageChangePrev() }}
                        >
                          <b>{"<< Prev"}</b>
                        </span>
                        :
                        null}
                        &nbsp;&nbsp;|&nbsp;&nbsp;
                        {!loading && (parseFloat(pageNo) < parseFloat(companyRecords)) ?
                        <span style={{ color: "#007bff", cursor: "pointer" }}
                          onClick={() => { this.pageChangeNext() }}
                        >
                          <b>{"Next >>"}</b>
                        </span>
                        :
                        null}
                      {loading ?
                        <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                          <i className="fa fa-spinner fa-spin" ></i>
                        </a>
                        :
                        null}

                    </div>
                    :
                    null}

                </div>

              </div>
            </div>
          </div>
        </section>


        <Modal show={this.state.showSuccessAlert} onHide={() => this.onAlertHide()}>
          <Modal.Header closeButton>
            <Modal.Title>OK</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h5 style={{ color: 'green' }}>{`${this.state.successAlertMessage}`}</h5>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="primary" onClick={() => this.onAlertHide()}>
              Ok
            </Button>
          </Modal.Footer>
        </Modal>

        <Modal show={this.state.showErrorAlert} onHide={() => this.onAlertHide()}>
          <Modal.Header closeButton>
            <Modal.Title>OK</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h5 style={{ color: 'red' }}>{`${this.state.errorAlertMessage}`}</h5>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="primary" onClick={() => this.onAlertHide()}>
              Ok
            </Button>
          </Modal.Footer>
        </Modal>

        {/* Address book modal --START */}
        <Modal size={"lg"} show={this.state.numberModal} onHide={() => this.hideContactModal()}>
          <Modal.Header closeButton>
            <Modal.Title>Manage Number Assignment</Modal.Title>
          </Modal.Header>
          <Modal.Body style={{ minHeight: '400px' }}>
            <div className="mng-full-table modal-adrs-book">
              <div className="row">
                <div className="col-md-12">
                  <div className="row pt-2 pb-2">
                    <div className="col-md-8">
                      <h4>Already assigned:</h4>
                    </div>
                  </div>
                  <div className="mng-full-table-hdr">
                    <div className="row">
                      <div className="col-md-2 border-rt">
                        <h6>Add/Remove</h6>
                      </div>
                      <div className="col-md-3 border-rt">
                        <h6>Contact Number</h6>
                      </div>
                      <div className="col-md-4 border-rt">
                        <h6>Provider</h6>
                      </div>
                      <div className="col-md-3">
                        <h6>Cost (/min)</h6>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
              {numberList && numberList.length > 0 && numberList.map((address, index) => {
                return <div className="row" key={index}>
                  <div className="col-md-12">
                    <div className="mng-full-table-row">
                      <div className="row">
                        <div className="col-md-2 border-rt">
                          <h6>Add</h6>
                          <input type="checkbox" value={address.isAdded} defaultChecked={address.isAdded} onChange={(event) => this.addOrRemoveToConference(address._id)} />
                        </div>
                        <div className="col-md-3 border-rt">
                          <h6>Contact Number</h6>
                          <p><a href="JavaScript:Void(0);">{address.name}</a></p>
                        </div>
                        <div className="col-md-4 border-rt">
                          <h6>Provider</h6>
                          <p>{address.userType}</p>
                        </div>
                        <div className="col-md-3">
                          <h6>Cost (/min)</h6>
                          <p>{address.phoneNumber}</p>
                        </div>

                      </div>
                    </div>

                  </div>
                </div>
              })
              }
            </div>

            <div className="mng-full-table modal-adrs-book">
              <div className="row">
                <div className="col-md-12">
                  <div className="row pt-2 pb-2">
                    <div className="col-md-8">
                      <h4>Not assigned yet:</h4>
                    </div>
                  </div>
                  <div className="mng-full-table-hdr">
                    <div className="row">
                      <div className="col-md-2 border-rt">
                        <h6>Add/Remove</h6>
                      </div>
                      <div className="col-md-3 border-rt">
                        <h6>Contact Number</h6>
                      </div>
                      <div className="col-md-4 border-rt">
                        <h6>Provider</h6>
                      </div>
                      <div className="col-md-3">
                        <h6>Cost (/min)</h6>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
              {numberList && numberList.length > 0 && numberList.map((address, index) => {
                return <div className="row" key={index}>
                  <div className="col-md-12">
                    <div className="mng-full-table-row">
                      <div className="row">
                        <div className="col-md-2 border-rt">
                          <h6>Add</h6>
                          <input type="checkbox" value={address.isAdded} defaultChecked={address.isAdded} onChange={(event) => this.addOrRemoveToConference(address._id)} />
                        </div>
                        <div className="col-md-3 border-rt">
                          <h6>Contact Number</h6>
                          <p><a href="JavaScript:Void(0);">{address.name}</a></p>
                        </div>
                        <div className="col-md-4 border-rt">
                          <h6>Provider</h6>
                          <p>{address.userType}</p>
                        </div>
                        <div className="col-md-3">
                          <h6>Cost (/min)</h6>
                          <p>{address.phoneNumber}</p>
                        </div>
                        {/* <div className="mobile-ad-edt-btns">
                            <ul>
                            <li><a href="#"><i className="fas fa-eye"></i></a></li>
                            <li><a href="JavaScript:Void(0);" data-toggle="modal" data-target="#delModal"><i className="far fa-trash-alt"></i></a></li>
                            </ul>
                        </div> */}
                      </div>
                    </div>

                  </div>
                </div>
              })
              }
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="primary" onClick={() => this.hideAddressBookModal()}> Save And Close </Button>
          </Modal.Footer>
        </Modal>



      </main>
    );
  }
}

export default checkAuthentication(AdminNumbermanagement);
