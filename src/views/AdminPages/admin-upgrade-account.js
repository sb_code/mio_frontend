import React, { Component } from 'react'
import Navbar from '../Component/Navbar'
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button, Badge } from "react-bootstrap"
import _ from 'lodash';
import { CSVLink, CSVDownload } from "react-csv";
import ReactTooltip from 'react-tooltip'; // For tool tip 

import { checkAuthentication } from '../Component/Authentication';

var path = cred.API_PATH;


export class AccountUpgrade extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userId: '',
            userType: '',

            openSideMenu: false,

            searchStatus: 'APPLIED',
            searching: false,

            loading: false,

            requestList: [],
            page: 1,
            listRecords: '',

            showSuccessAlert: false,
            successAlertMessage: '',
            showErrorAlert: false,
            errorAlertMessage: '',

        }
        this.perPage = 5;
    }

    componentDidMount() {
        document.body.classList.remove("sign-bg");
        document.body.classList.add("grey-bg");
        let that = this;

        let storage = localStorage.getItem("MIO_Local");
        storage = storage ? JSON.parse(storage) : null;
        if (storage && storage.data.userId) {
            this.setState({
                userId: storage.data.userId,
                userType: storage.data.userType,
                companyId: storage.data.companyId
            }, () => {
                that.getUpgradeRequestList();
                // console.log('user management--', this.state.userType);
            });

            let tokenData = this.parseJwt(storage.token);

        }


    }

    // For getting upgrade request list -- 
    getUpgradeRequestList = () => {
        let dataToSend = {
            "page": this.state.page,
            "perPage": this.perPage,
            "status": this.state.searchStatus
        };
        this.setState({loading: true});
        axios
            .post(path + 'user/upgrade-request-list',dataToSend)
            .then(serverResponse => {

                let res = serverResponse.data;
                if (!res.isError) {
                    // console.log("list requests==", res);
                    this.setState({
                        loading: false,
                        requestList: res.details && res.details[0] && res.details[0].results,
                        listsRecords: res.details && res.details[0] && res.details[0].noofpage
                    });
                }else{
                    this.setState({loading: false});
                }

            })
            .catch(error => {
                console.log(error);
            })

    }

    // For searching user - 
    searchRequest = (event) => {
        event.preventDefault();
        this.setState({
            page: 1,
            listsRecords: '',
            searching: true
        },()=>{
            this.getUpgradeRequestList()
        });
        
    }

    // For clearing search -
    clearSearch = (event) => {
        event.preventDefault();
        this.setState({
            page: 1,
            listsRecords: '',
            searchStatus: "APPLIED",
            searching: false
        },()=>{
            this.getUpgradeRequestList()
        });
    }

    // For userList of a particular company - 
    approveRequest = (index) => {

        let { requestList } = this.state;
        let request = requestList[index];
        let { changeToCompanyType, companyId } = request;

        // console.log("Selected req =", request);

        let dataToSend = {
            "companyId": request.companyId,
            "status": 'ACCEPTED',
            "requestId": request._id
        };
        this.setState({
            loading: true
        });
        axios
            .post(path + 'user/upgrade-request-action', dataToSend)
            .then(serverResponse => {
                let res = serverResponse.data;
                if (!res.isError) {
                    console.log("response after approval -- ", res);
                    this.setState({
                        showSuccessAlert: true,
                        successAlertMessage: 'Successfully approved to- ' + `${changeToCompanyType}`,
                        loading: false
                    });
                    this.updateBaseService(changeToCompanyType, companyId).then(() => {
                        this.getUpgradeRequestList();
                    });

                } else {
                    this.setState({
                        showErrorAlert: true,
                        errorAlertMessage: 'Sorry! Something is wrong..',
                        loading: false
                    });
                }
            })
            .catch(error => {
                console.log(error);
            })

    }


    rejectRequest = (index) => {

        let { requestList } = this.state;
        let request = requestList[index];
        let { companyId, _id } = request;
        // console.log("Selected req =", request);

        let dataToSend = {
            "companyId": companyId,
            "status": 'REJECTED',
            "requestId": _id
        };
        this.setState({
            loading: true
        })

        axios
            .post(path + 'user/upgrade-request-action', dataToSend)
            .then(serverResponse => {
                let res = serverResponse.data;
                if (!res.isError) {
                    // console.log("response after approval -- ", res);
                    this.setState({
                        showSuccessAlert: true,
                        successAlertMessage: 'Successfully rejected the request.',
                        loading: false
                    }, () => {
                        this.getUpgradeRequestList();
                    });

                } else {
                    this.setState({
                        showErrorAlert: true,
                        errorAlertMessage: 'Sorry! Something is wrong..',
                        loading: false
                    });
                }
            })
            .catch(error => {
                console.log(error);
            })

    }

    updateBaseService = (companyType, companyId) => {
        return new Promise((resolve, reject) => {

            let storage = localStorage.getItem("MIO_Local");
            storage = storage ? JSON.parse(storage) : null;

            if (storage.data) {
                let dataToSend = {
                    "companyId": companyId,
                    "companyType": companyType
                };
                // API - 
                axios
                    .post(path + 'services/add-base-services-to-company', dataToSend)
                    .then(serverResponse => {
                        let res = serverResponse.data;
                        if (!res.isError) {

                            console.log("updateBaseService---", res);
                            resolve();

                        } else {
                            reject();
                        }
                    })
                    .catch(error => {
                        console.log(error);
                    })

            }
        })
    }

    hideSuccessAlert = () => {
        this.setState({
            showSuccessAlert: false,
            successAlertMessage: '',
        });
    }

    hideErrorAlert = () => {
        this.setState({
            showErrorAlert: false,
            errorAlertMessage: ''
        });
    }

    pageChangePrev = () => {

        this.setState({
            page: (parseFloat(this.state.page) - 1)
        }, () => {
            this.getUpgradeRequestList()
        });

    }
    
    pageChangeNext = () => {

        this.setState({
            page: (parseFloat(this.state.page) + 1)
        }, () => {
            this.getUpgradeRequestList()
        });

    }

    sideMenuToggle = () => {
        let { openSideMenu } = this.state;
        openSideMenu = !openSideMenu;
        this.setState({ openSideMenu });
    }

    mobMenuClick() {
        this.sideMenuToggle();
        console.log('mobMenuClick');
    }

    parseJwt = (token) => {
        if (!token) { return; }
        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace('-', '+').replace('_', '/');
        return JSON.parse(window.atob(base64));
    }


    render() {
        let { requestList, searchStatus, loading, searching, userId, listsRecords, page } = this.state;
        const searchOption = ["APPLIED", "ACCEPTED", "REJECTED"];

        return <main>
            <section className="user-mngnt-wrap">
                <div className="container-fluid">
                    <div className="row">

                        <Navbar routTo="/adminaccountupgrade" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />
                        <div className="col-xl-10 col-lg-9 p-0 mob-full-conf">

                            <div className="mobile-menu-header">
                                {/* <a className="back-arw" onClick={() => console.log('<-BACK')}><i className="fas fa-arrow-left"></i></a> */}
                                <a href= "JavaScript:Void(0);" className="menu-tgl" onClick={() => this.sideMenuToggle()}><img src="images/menu-tgl.png" alt="" /></a>
                            </div>

                            <div className="mng-full-list">

                                <div className="mng-full-srch">
                                    <div className="row">
                                        <div className="col-md-10">
                                            <div className="srch-wrap">
                                                <select className="form-control"
                                                    name="searchStatus"
                                                    onChange={(event) => { this.setState({ searchStatus: event.target.value }) }}
                                                >
                                                    {searchOption && searchOption.map((data, index)=>{
                                                        return<option value= {data}
                                                            key= {index} selected= {searchStatus == data? true: false}
                                                        >
                                                            {data}
                                                        </option>
                                                    })}
                                                </select>
                                            </div>
                                        </div>

                                        <div className="col-md-2">
                                            <div className="srch-wrap">
                                                <form>
                                                        
                                                {this.state.searching ?
                                                    <input type="button" onClick={(event) => this.clearSearch(event)}
                                                        ref={ref => this.fooRef = ref}
                                                        data-tip='Clear'
                                                        onFocus={() => { ReactTooltip.show(this.fooRef) }}
                                                    />
                                                    :
                                                    <input type="submit" onClick={(event) => this.searchRequest(event)}
                                                        ref={ref => this.fooRef = ref}
                                                        data-tip='Search'
                                                        onFocus={() => { ReactTooltip.show(this.fooRef) }}
                                                    />
                                                }
                                                <ReactTooltip
                                                    effect="float"
                                                    place="top"
                                                    data-border="true"
                                                />
                                            </form>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div className="mng-full-table">
                                    <div className="row">

                                        <div className="col-md-8">
                                            <div className="mng-full-table-hdr">
                                                <div className="row">
                                                    <div className="col-md-3 border-rt">
                                                        <h6>Name/Company</h6>
                                                    </div>
                                                    <div className="col-md-3 border-rt">
                                                        <h6>Company Type</h6>
                                                    </div>
                                                    <div className="col-md-3 border-rt">
                                                        <h6>Upgrade to</h6>
                                                    </div>
                                                    <div className="col-md-3">
                                                        <h6>Status</h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-md-4">
                                            <div className="mng-full-table-hdr">
                                                <div className="row">
                                                    <div className="col-md-6 border-rt">
                                                        <h6>Approve </h6>
                                                    </div>
                                                    <div className="col-md-6">
                                                        <h6>Reject</h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    {requestList && requestList.length > 0 ? requestList.map((list, index) => {
                                        return <div className="row" key={index}>
                                            <div className="col-md-8">
                                                <div className="mng-full-table-row">

                                                    <div className="row">
                                                        <div className="col-md-3 border-rt">
                                                            <h6>Name/Company</h6>
                                                            <p className="textEllips">
                                                                {list.companyName}
                                                            
                                                            </p>
                                                        </div>
                                                        <div className="col-md-3 border-rt">
                                                            <h6>Company Type</h6>
                                                            <p className="textEllips">
                                                                {list.currentCompanyType}
                                                            </p>
                                                        </div>
                                                        <div className="col-md-3 border-rt">
                                                            <h6>Upgrade to</h6>
                                                            <p className="textEllips">{list.changeToCompanyType}</p>
                                                        </div>
                                                        <div className="col-md-3">
                                                            <h6>Status</h6>
                                                            {/* <p className="textEllips">{list.status}</p> */}
                                                            {list.status == "APPLIED" ?
                                                                <Badge variant="primary">{list.status}</Badge>
                                                                : list.status == "ACCEPTED" ?
                                                                    <Badge variant="success">{list.status}</Badge>
                                                                    : list.status == "REJECTED" ?
                                                                        <Badge variant="danger">{list.status}</Badge>
                                                                        : null}
                                                        </div>

                                                        <div className="mobile-ad-edt-btns">
                                                            {list.status == "APPLIED" ?
                                                                <ul>
                                                                    <li onClick={() => this.approveRequest(index)}>
                                                                        <a href="JavaScript:Void(0);"
                                                                            // ref={ref => this.fooRef = ref}
                                                                            data-tip='Approve'
                                                                            // onFocus={() => { ReactTooltip.show(this.fooRef) }}
                                                                        >
                                                                            <i className="fa fa-check" aria-hidden="true"></i>
                                                                            <ReactTooltip
                                                                                effect="float"
                                                                                place="top"
                                                                                data-border="true"
                                                                            />
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                                : null}

                                                            {list.status == "APPLIED" ?
                                                                <ul>
                                                                    <li onClick={() => this.rejectRequest(index)}>
                                                                        <a href="JavaScript:Void(0);"
                                                                            // ref={ref => this.fooRef = ref}
                                                                            data-tip='Reject'
                                                                            // onFocus={() => { ReactTooltip.show(this.fooRef) }}
                                                                        >
                                                                            <i className="fa fa-times" aria-hidden="true"></i>
                                                                            <ReactTooltip
                                                                                effect="float"
                                                                                place="top"
                                                                                data-border="true"
                                                                            />
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                                : null}

                                                        </div>
                                                    </div>

                                                </div>

                                            </div>

                                            <div className="col-md-4">
                                                <div className="mng-full-table-row add-edt text-center">
                                                    <div className="row">
                                                        <div className="col-md-6">

                                                            {list.status == "APPLIED" ?
                                                                    (!this.state.loading?
                                                                    < Button
                                                                        variant="success"
                                                                        style={{ backgroundColor: "#28a745" }}
                                                                        onClick={() => this.approveRequest(index)}
                                                                    >
                                                                        Approve
                                                                        
                                                                    </Button>
                                                                    :
                                                                    
                                                                        <span><i class="fas fa-spinner fa-spin" /></span>
                                                                    )
                                                                : 
                                                                < Button
                                                                    variant="success" style={{ backgroundColor: "#28a745" }}
                                                                    disabled={true}
                                                                >
                                                                    Approve

                                                                </Button>
                                                            }
                                                        </div>
                                                        <div className="col-md-6">

                                                            {list.status == "APPLIED" ?
                                                                (!this.state.loading?
                                                                    <Button variant="secondary" style={{ backgroundColor: "#6c757d" }}
                                                                        onClick={() => this.rejectRequest(index)}
                                                                        disabled={this.state.loading ? true : false}
                                                                    >
                                                                        Reject
                                                                        
                                                                    </Button>
                                                                    :
                                                                    
                                                                    <span><i class="fas fa-spinner fa-spin" /></span>
                                                                )
                                                                : 
                                                                <Button variant="secondary" style={{ backgroundColor: "#6c757d" }}
                                                                    disabled={true}
                                                                >
                                                                    Reject
                                                                </Button>
                                                            }

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    })
                                        : null}

                                </div>

                                <br/><br/>
                                <div className="form-group">
                                    {!loading && parseFloat(page) > 1 ?
                                        <span style={{ color: "#007bff", cursor: "pointer" }}
                                            onClick={() => { this.pageChangePrev() }}
                                        >
                                            <b>{"<< Prev"}</b>
                                        </span>
                                        :
                                        null}
                                        &nbsp;&nbsp;|&nbsp;&nbsp;
                                        {!loading && (parseFloat(page) < parseFloat(listsRecords)) ?
                                        <span style={{ color: "#007bff", cursor: "pointer" }}
                                            onClick={() => { this.pageChangeNext() }}
                                        >
                                            <b>{"Next >>"}</b>
                                        </span>
                                        :
                                        null}
                                    {loading ?
                                        <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                                            <i className="fa fa-spinner fa-spin" ></i>
                                        </a>
                                        :
                                        null}

                                    </div>
                                   

                            </div>

                        </div>
                    </div>
                </div>

            </section>

            <Modal show={this.state.showSuccessAlert} onHide={() => this.hideSuccessAlert()}>
                <Modal.Header closeButton>
                    <Modal.Title>OK</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h5 style={{ color: 'green' }}>{`${this.state.successAlertMessage}`}</h5>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={() => this.hideSuccessAlert()}>
                        Ok
                </Button>
                </Modal.Footer>
            </Modal>

            <Modal show={this.state.showErrorAlert} onHide={() => this.hideErrorAlert()}>
                <Modal.Header closeButton>
                    <Modal.Title>OK</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h5 style={{ color: 'red' }}>{`${this.state.errorAlertMessage}`}</h5>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={() => this.hideErrorAlert()}>
                        Ok
                    </Button>
                </Modal.Footer>
            </Modal>

        </main >

    }

}

export default checkAuthentication(AccountUpgrade);