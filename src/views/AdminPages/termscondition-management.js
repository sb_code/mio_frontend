import React, { Component } from 'react'
import Navbar from '../Component/Navbar'
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button } from "react-bootstrap"
import _ from 'lodash';
import { CSVLink, CSVDownload } from "react-csv";
import ReactTooltip from 'react-tooltip'; // For tool tip 
import RichTextEditor from 'react-rte';

import { checkAuthentication } from '../Component/Authentication';

var path = cred.API_PATH;


export class TermsconditionsManagement extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userId: '',
            userType: '',
            companyId: '',
            selectedPageName: '',

            openSideMenu: false,

            loading: false,

            editingField: '',

            showModal: false,
            term: RichTextEditor.createEmptyValue(),
            termDetails: '',
            dataToSaveInDb: '',

            showSuccessAlert: false,
            successAlertMessage: '',
            showErrorAlert: false,
            errorAlertMessage: ''

        }
    }

    componentDidMount() {
        document.body.classList.remove("sign-bg");
        document.body.classList.add("grey-bg");

        if (this.props.location.state && this.props.location.state.name) {

            let storage = localStorage.getItem("MIO_Local");
            storage = storage ? JSON.parse(storage) : null;
            let selectedPageName = this.props.location.state.name;

            if (storage && storage.data.userId) {
                this.setState({
                    userId: storage.data.userId,
                    userType: storage.data.userType,
                    companyId: storage.data.companyId,
                    selectedPageName: selectedPageName
                }, () => {
                    this.getAboutmeDetails();
                });

                // console.log(this.parseJwt(storage.token));
                let tokenData = this.parseJwt(storage.token);

            }
        } else {
            this.props.history.push("/admincontentmanagement");
        }


    }

    getAboutmeDetails = () => {
        this.setState({ loading: true });
        // API --- 
        axios
            .post(path + 'content-management/get-tac')
            .then(serverResponse => {
                let res = serverResponse.data;
                if (!res.isError) {
                    this.setState({
                        loading: false,
                        termDetails: res.details && res.details[0],
                    })
                } else {
                    this.setState({ loading: false });
                }
            })
            .catch(error => {
                this.setState({ loading: false });
                console.log(error);
            })

    }

    addNewTerm = () => {

        let { termDetails, term, dataToSaveInDb } = this.state;

        if (dataToSaveInDb && dataToSaveInDb != '') {

            let dataToSend = {
                "tac": [{
                    "termsAndCondition": dataToSaveInDb
                }]
            };
            this.setState({ loading: true });
            // API - -
            axios
                .post(path + 'content-management/insert-tac', dataToSend)
                .then(serverResponse => {
                    let res = serverResponse.data;
                    if (!res.isError) {
                        this.setState({
                            loading: false,
                            term: '',
                            showModal: false,
                            showSuccessAlert: true,
                            successAlertMessage: 'Successfully saved.'
                        }, () => {
                            this.getAboutmeDetails();
                        });
                    } else {
                        this.setState({
                            loading: false,
                            term: '',
                            showModal: false,
                            showErrorAlert: true,
                            errorAlertMessage: 'Sorry! Something is wrong.'
                        });
                    }
                })
                .catch(error => {
                    this.setState({ loading: false });
                    console.log(error);
                })

        }
    }

    editOption = () => {
        let { termDetails } = this.state;
        this.setState({
            editModal: true,
            term: RichTextEditor.createValueFromString(termDetails.termsAndCondition, 'html')
        });

    }

    setRTESectionState = (field, value) => {

        let { dataToSaveInDb } = this.state;
        dataToSaveInDb = value.toString('html');
        this.setState({ dataToSaveInDb, term: value });

    }

    editTerms = () => {
        let { termDetails, dataToSaveInDb } = this.state;

        if (dataToSaveInDb && dataToSaveInDb != '') {
            let dataToSend = {
                "tacId": termDetails._id,
                "termsAndCondition": dataToSaveInDb
            };
            this.setState({ loading: true });
            // API -- 
            axios
                .post(path + 'content-management/update-tac-by-id', dataToSend)
                .then(serverResponse => {
                    let res = serverResponse.data;
                    if (!res.isError) {

                        this.setState({
                            loading: false,
                            editingField: '',
                            showSuccessAlert: true,
                            successAlertMessage: 'Successfully saved.'
                        }, () => {
                            this.getAboutmeDetails();
                            this.clearState();
                        });
                    } else {
                        // this.getAboutmeDetails();
                        this.setState({
                            loading: false,
                            editingField: '',
                            showErrorAlert: true,
                            errorAlertMessage: 'Sorry! Something is wrong.'
                        }, () => {
                            this.getAboutmeDetails();
                            this.clearState();
                        });
                    }
                })
                .catch(error => {
                    this.setState({ loading: true });
                    console.log(error);
                })
        }

    }

    changeTerms = (event, index) => {

        let { terms } = this.state;
        let pair = terms[index];
        pair["termsAndCondition"] = event.target.value;
        terms[index] = pair;

        this.setState({
            terms: terms
        });

    }

    deleteTerms = (index) => {

        let { terms } = this.state;
        let pair = terms[index];
        let dataToSend = {
            "tacId": pair._id,
            "termsAndCondition": ''
        };
        // API for deleting -- 
        // axios
        //     .post(path + 'content-management/delete-tac', dataToSend)
        //     .then(serverResponse => {
        //         let res = serverResponse.data;
        //         if (!res.isError) {
        //             this.setState({
        //                 showSuccessAlert: true,
        //                 successAlertMessage: 'Successfully deleted.'
        //             }, () => {
        //                 this.getAboutmeDetails();
        //             })
        //         } else {
        //             this.setState({
        //                 showErrorAlert: true,
        //                 errorAlertMessage: 'Sorry! Something is wrong.'
        //             })
        //         }
        //     })
        //     .catch(error => {
        //         console.log(error);
        //     })

        // API -- 
        axios
            .post(path + 'content-management/update-tac-by-id', dataToSend)
            .then(serverResponse => {
                let res = serverResponse.data;
                if (!res.isError) {

                    this.setState({
                        editingField: '',
                        showSuccessAlert: true,
                        successAlertMessage: 'Successfully deleted.'
                    }, () => {
                        this.getAboutmeDetails();
                    });
                } else {
                    this.getAboutmeDetails();
                    this.setState({
                        editingField: '',
                        showErrorAlert: true,
                        errorAlertMessage: 'Sorry! Something is wrong.'
                    }, () => {
                        this.getAboutmeDetails();
                    });
                }
            })
            .catch(error => {
                console.log(error);
            })


    }

    clearState = () => {

        this.setState({
            term: RichTextEditor.createEmptyValue(),
            showModal: false,
            editModal: false,
            dataToSaveInDb: ''
        });

    }

    hideSuccessAlert() {
        this.setState({
            showSuccessAlert: false,
            successAlertMessage: '',
        })
    }

    hideErrorAlert() {
        this.setState({
            showErrorAlert: false,
            errorAlertMessage: ''
        })
    }

    sideMenuToggle = () => {
        let { openSideMenu } = this.state;
        openSideMenu = !openSideMenu;
        this.setState({ openSideMenu });
    }

    mobMenuClick() {
        this.sideMenuToggle();
        console.log('mobMenuClick');
    }

    backMenueToggle = () => {
        this.props.history.goBack();
    }

    parseJwt = (token) => {
        if (!token) { return; }
        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace('-', '+').replace('_', '/');
        return JSON.parse(window.atob(base64));
    }


    render() {

        let { loading, termDetails, editingField, term } = this.state;

        return <main>
            <section className="user-mngnt-wrap">
                <div className="container-fluid">
                    <div className="row">

                        <Navbar routTo="/admincontentmanagement" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />
                        <div className="col-xl-10 col-lg-10 p-0 mob-full-conf">

                            <div className="mobile-menu-header">
                                <a href="JavaScript:Void(0);" className="back-arw" onClick={() => this.backMenueToggle()}><i className="fas fa-arrow-left"></i></a>
                                {/* <a className="menu-tgl" onClick={() => this.sideMenuToggle()}><img src="images/menu-tgl.png" alt="" /></a> */}
                            </div>

                            <div className="custom-brdcrmb">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item"><a href="JavaScript:Void(0);">Content Management</a></li>
                                        <li className="breadcrumb-item active" aria-current="page">{this.state.selectedPageName}</li>
                                    </ol>
                                </nav>
                            </div>

                            <div className="mng-full-list about-mng-form" style={{ padding: "25px 30px" }}>
                                {!termDetails ?
                                    <div className="row">
                                        {!loading ?
                                            <div className="col-md-3">
                                                <div className="add-user-btn">
                                                    <a href="JavaScript:Void(0);"
                                                        className="btn"
                                                        onClick={() => this.setState({ showModal: true })}
                                                        ref={ref => this.fooRef = ref}
                                                        data-tip='Add new terms'
                                                        onFocus={() => { ReactTooltip.show(this.fooRef) }}
                                                    >
                                                        Add New Point
                                                </a>
                                                    <ReactTooltip
                                                        effect="float"
                                                        place="top"
                                                        data-border="true"
                                                    />
                                                </div>
                                            </div>
                                            : null}
                                        {loading ?
                                            <div className="col-md-3">
                                                <div className="add-user-btn">
                                                    <a href="JavaScript:Void(0);"
                                                        className="btn"
                                                    >
                                                        <i className="fa fa-spinner fa-spin" />
                                                    </a>
                                                </div>
                                            </div>
                                            : null}
                                    </div>
                                    :
                                    null}
                                {termDetails?

                                    <div className="row">
                                        <div className="col-lg-12 col-md-12">

                                            <div className="row">
                                                
                                                <div className="mobile-ad-edt-btns">
                                                    <ul>

                                                        <li onClick={() => { this.editOption(); }}>
                                                            <a href="JavaScript:Void(0);"
                                                                data-tip='Edit'
                                                            >
                                                                <i className="fas fa-edit"></i>
                                                            </a>
                                                            <ReactTooltip
                                                                effect="float"
                                                                place="top"
                                                                data-border="true"
                                                            />
                                                        </li>

                                                    </ul>
                                                </div>

                                                <div className="col-md-10">
                                                    <div className="form-group">

                                                        <RichTextEditor
                                                            value={RichTextEditor.createValueFromString(termDetails.termsAndCondition, 'html')}
                                                            readOnly={true}
                                                        />

                                                    </div>
                                                </div>

                                                <div className="col-md-2">
                                                    <div className="mng-full-table-row add-edt text-center">
                                                        <div className="row">

                                                            <div className="col-md-4">
                                                                <a href="JavaScript:Void(0);"
                                                                    ref={ref => this.fooRef = ref}
                                                                    data-tip='Click to edit'
                                                                    onFocus={() => { ReactTooltip.show(this.fooRef) }}
                                                                    onClick={() => { this.editOption(); ReactTooltip.hide(this.fooRef) }}
                                                                >
                                                                    <i className="fas fa-edit"></i>
                                                                </a>
                                                                <ReactTooltip
                                                                    effect="float"
                                                                    place="top"
                                                                    data-border="true"
                                                                />
                                                            </div>

                                                            {loading ?
                                                                <div className="col-md-4">
                                                                    <a href="JavaScript:Void(0);" >
                                                                        <i className="fa fa-spinner fa-spin" ></i>
                                                                    </a>
                                                                </div>
                                                                : null}

                                                        </div>

                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                    </div>
                                : null}

                            </div>
                        </div>

                    </div>
                </div>

            </section>

            <Modal show={this.state.showModal} size="lg" onHide={() => { this.clearState() }}>
                <Modal.Header closeButton>
                    <Modal.Title>Add new Term</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="row">

                        <div className="col-md-12">
                            <div className="form-group">
                                <label>New Point:</label>
                                {/* <textarea type="text" name="term" className="form-control" rows="200" cols="50"
                                    placeholder="Enter a term" style={{ height: "450px" }}
                                    value={term}
                                    onChange={(event) => this.setState({ term: event.target.value })}
                                    onFocus={() => { this.setState({ termError: '' }) }}
                                >
                                    {term}
                                </textarea>
                                <span style={{ color: 'red' }}>{this.state.termError ? `* ${this.state.termError}` : ''}</span> */}

                                <RichTextEditor
                                    value={term}
                                    onChange={(value) => this.setRTESectionState('value', value)}
                                />

                            </div>
                        </div>

                    </div>
                </Modal.Body>
                <Modal.Footer>
                    {!loading ?
                        <Button variant="primary" onClick={this.addNewTerm}> Save </Button>
                        : null}
                    {loading ?
                        <Button variant="primary"> <i className="fa fa-spinner fa-spin" ></i> </Button>
                        : null}
                </Modal.Footer>
            </Modal>

            <Modal show={this.state.editModal} size="lg" onHide={() => { this.clearState() }}>
                <Modal.Header closeButton>
                    <Modal.Title>Edit Term</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="row">

                        <div className="col-md-12">
                            <div className="form-group">
                                <label>New Point:</label>

                                <RichTextEditor
                                    value={term}
                                    onChange={(value) => this.setRTESectionState('value', value)}
                                />

                            </div>
                        </div>

                    </div>
                </Modal.Body>
                <Modal.Footer>
                    {!loading ?
                        <Button variant="primary" onClick={this.editTerms}> Update </Button>
                        : null}
                    {loading ?
                        <Button variant="primary"> <i className="fa fa-spinner fa-spin" ></i> </Button>
                        : null}
                </Modal.Footer>
            </Modal>

            <Modal show={this.state.showSuccessAlert} onHide={() => this.hideSuccessAlert()}>
                <Modal.Header closeButton>
                    <Modal.Title>OK</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h5 style={{ color: 'green' }}>{`${this.state.successAlertMessage}`}</h5>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={() => this.hideSuccessAlert()}>
                        Ok
            </Button>
                </Modal.Footer>
            </Modal>

            <Modal show={this.state.showErrorAlert} onHide={() => this.hideErrorAlert()}>
                <Modal.Header closeButton>
                    <Modal.Title>OK</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h5 style={{ color: 'red' }}>{`${this.state.errorAlertMessage}`}</h5>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={() => this.hideErrorAlert()}>
                        Ok
            </Button>
                </Modal.Footer>
            </Modal>

        </main >

    }

}

export default checkAuthentication(TermsconditionsManagement);



