import React, { Component } from 'react'
import Navbar from '../Component/Navbar'
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button } from "react-bootstrap"
import _ from 'lodash';
import { CSVLink, CSVDownload } from "react-csv";
import ReactTooltip from 'react-tooltip'; // For tool tip 

import { checkAuthentication } from '../Component/Authentication';

var path = cred.API_PATH;


export class AboutusManagement extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userId: '',
            userType: '',
            companyId: '',
            selectedPageName: '',

            openSideMenu: false,
            searchString: '',
            searching: false,

            showSuccessAlert: false,
            successAlertMessage: '',
            showErrorAlert: false,
            errorAlertMessage: '',

            loading: false,

            details: {},
            name: '',
            address: '',
            mobile: '',
            countryCode: '',
            details1: '',
            details2: '',
            image: '',

            willEdit: true,

        }
    }

    componentDidMount() {
        document.body.classList.remove("sign-bg");
        document.body.classList.add("grey-bg");

        let storage = localStorage.getItem("MIO_Local");
        storage = storage ? JSON.parse(storage) : null;
        let selectedPageName = this.props.location.state.name;

        if (this.props.location.state && this.props.location.state.name) {

            if (storage && storage.data.userId) {
                this.setState({
                    userId: storage.data.userId,
                    userType: storage.data.userType,
                    companyId: storage.data.companyId,
                    selectedPageName: selectedPageName
                }, () => {
                    this.getAboutmeDetails();
                });

                let tokenData = this.parseJwt(storage.token);

            }

        } else {

            this.props.history.push("/admincontentmanagement");

        }

    }


    getAboutmeDetails = () => {

        let dataToSend = {};
        // API --- 
        axios
            .post(path + 'content-management/get-mio-admin-detail', dataToSend)
            .then(serverResponse => {
                let res = serverResponse.data;
                if (!res.isError) {
                    let details = res.details;
                    this.setState({
                        details: details,
                        name: details.companyName,
                        address: details.address,
                        mobile: details.mobile,
                        countryCode: details.countryCode,
                        details1: details.detail1,
                        details2: details.detail2,
                        image: ''
                    })

                    // console.log("response--",res);
                }
            })
            .catch(error => {
                console.log(error);
            })

    }


    updateForm = (event) => {

        event.preventDefault();
        let dataToSend = {
            "companyId": this.state.companyId,
            "createdBy": this.state.userId,
            "companyName": this.state.name,
            "address": this.state.address,
            "mobile": this.state.mobile,
            "countryCode": this.state.countryCode,
            "detail1": this.state.details1,
            "detail2": this.state.details2
        };
        // API -- 
        axios
            .post(path + 'content-management/update-mio-admin-detail', dataToSend)
            .then(serverResponse => {
                let res = serverResponse.data;
                if (!res.isError) {

                    this.setState({
                        showSuccessAlert: true,
                        successAlertMessage: 'Successfully updated.',
                        willEdit: true
                    }, () => {
                        this.getAboutmeDetails();
                    })

                } else {
                    this.setState({
                        showErrorAlert: true,
                        errorAlertMessage: 'Sorry! something is wrong.',
                        willEdit: true
                    })

                }
            })


    }


    hideSuccessAlert() {
        this.setState({
            showSuccessAlert: false,
            successAlertMessage: '',
        })
    }

    hideErrorAlert() {
        this.setState({
            showErrorAlert: false,
            errorAlertMessage: '',
        })
    }


    sideMenuToggle = () => {
        let { openSideMenu } = this.state;
        openSideMenu = !openSideMenu;
        this.setState({ openSideMenu });
    }

    mobMenuClick() {
        this.sideMenuToggle();
        console.log('mobMenuClick');
    }

    backMenueToggle= ()=>{
        this.props.history.goBack();
    }

    parseJwt = (token) => {
        if (!token) { return; }
        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace('-', '+').replace('_', '/');
        return JSON.parse(window.atob(base64));
    }


    render() {

        let { details, name, address, mobile, countryCode, details1, details2, willEdit } = this.state;

        return <main>
            <section className="user-mngnt-wrap">
                <div className="container-fluid">
                    <div className="row">

                        <Navbar routTo="/admincontentmanagement" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />
                        <div className="col-xl-10 col-lg-9 p-0 mob-full-conf">

                            <div className="mobile-menu-header">
                                <a href = "JavaScript:Void(0)" className="back-arw" onClick={()=> this.backMenueToggle()}><i className="fas fa-arrow-left"></i></a>
                                {/* <a className="menu-tgl" onClick={() => this.sideMenuToggle()}><img src="images/menu-tgl.png" alt="" /></a> */}
                            </div>

                            <div className="custom-brdcrmb">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item"><a href="#">Content Management</a></li>
                                        <li className="breadcrumb-item active" aria-current="page">About Management</li>
                                    </ol>
                                </nav>
                            </div>

                            <div className="mng-full-list about-mng-form">
                                <div className="row">
                                    <div className="col-lg-9 col-md-8">
                                        <form>
                                            <div className="row">
                                                <div className="col-md-12">
                                                    <div className="form-group">
                                                        <label>Company Name:</label>
                                                        <input type="text" name="name" className="form-control" placeholder="Enter Company Name"
                                                            value={name} onChange={(event) => { this.setState({ name: event.target.value }) }}
                                                            readOnly={willEdit}
                                                        />
                                                    </div>
                                                </div>
                                                <div className="col-md-12">
                                                    <div className="form-group">
                                                        <label>Company Address:</label>
                                                        <input type="text" name="address" className="form-control" placeholder="Enter Company Email Address"
                                                            value={address} onChange={(event) => { this.setState({ address: event.target.value }) }}
                                                            readOnly={willEdit}
                                                        />
                                                    </div>
                                                </div>
                                                <div className="col-md-4">
                                                    <div className="form-group">
                                                        <label>Country Code:</label>
                                                        <input type="tel" name="mobile" className="form-control" placeholder="Enter Country Code"
                                                            value={countryCode} onChange={(event) => { this.setState({ countryCode: event.target.value }) }}
                                                            readOnly={willEdit}
                                                        />

                                                    </div>
                                                </div>
                                                <div className="col-md-8">
                                                    <div className="form-group">
                                                        <label>Company Mobile Number:</label>
                                                        <input type="tel" name="mobile" className="form-control" placeholder="Enter Company Mobile Number"
                                                            value={mobile} onChange={(event) => { this.setState({ mobile: event.target.value }) }}
                                                            readOnly={willEdit}
                                                        />

                                                    </div>
                                                </div>
                                                <div className="col-md-12">
                                                    <div className="form-group">
                                                        <label>Company Details1:</label>
                                                        <textarea type="text" name="details1" className="form-control" rows="4" cols="50"
                                                            placeholder="Enter Company Details1" style={{ height: "85px" }}
                                                            onChange={(event) => { this.setState({ details1: event.target.value }) }}
                                                            value={details1} readOnly={willEdit}
                                                        >
                                                            {details1}
                                                        </textarea>
                                                    </div>
                                                </div>
                                                <div className="col-md-12">
                                                    <div className="form-group">
                                                        <label>Company Details2:</label>
                                                        <textarea type="text" name="details2" className="form-control" rows="4" cols="50"
                                                            placeholder="Enter Company Details2" style={{ height: "85px" }}
                                                            onChange={(event) => { this.setState({ details2: event.target.value }) }}
                                                            value={details2} readOnly={willEdit}
                                                        >
                                                            {details2}
                                                        </textarea>
                                                    </div>
                                                </div>
                                                <div className="col-md-12">
                                                    <div className="form-group">
                                                        <button className="btn w-100"
                                                            data-tip='Update'
                                                            onClick={(event) => { this.updateForm(event) }}
                                                        >
                                                            <i className="fas fa-sync" />
                                                            Update
                                                        </button>
                                                        <ReactTooltip
                                                            effect="float"
                                                            place="top"
                                                            data-border="true"
                                                        />
                                                    </div>
                                                </div>

                                            </div>
                                        </form>
                                    </div>

                                    {willEdit ?
                                        <div class="col-lg-3 col-md-4">

                                            <a href="JavaScript:Void(0);"
                                                onClick={() => { this.setState({ willEdit: !this.state.willEdit }) }}
                                                className="btn pay-btn"
                                                // ref={ref => this.fooRef = ref}
                                                data-tip='Update'
                                            // onFocus={() => { ReactTooltip.show(this.fooRef) }}
                                            >
                                                <i className="far fa-edit" />
                                            </a>
                                            <ReactTooltip
                                                effect="float"
                                                place="top"
                                                data-border="true"
                                            />

                                        </div>
                                        : null}
                                    {/* <div class="col-lg-3 col-md-4">
                                        <div class="img-upload-rt">
                                            <div class="form-group">
                                                <label>Upload Image:</label>
                                                <div class="upload-box">
                                                    <img src= "https://cdn.shoplightspeed.com/shops/622945/files/10501368/800x1024x1/temby-australia-black-and-silver-curved-soprano-sa.jpg" />
                                                </div>
                                            </div>
                                            <button class="btn w-100"><i class="fas fa-upload"></i> Upload</button>
                                        </div>
                                    </div> */}

                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </section>

            <Modal show={this.state.showSuccessAlert} onHide={() => this.hideSuccessAlert()}>
                <Modal.Header closeButton>
                    <Modal.Title>OK</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h5 style={{ color: 'green' }}>{`${this.state.successAlertMessage}`}</h5>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={() => this.hideSuccessAlert()}>
                        Ok
            </Button>
                </Modal.Footer>
            </Modal>

            <Modal show={this.state.showErrorAlert} onHide={() => this.hideErrorAlert()}>
                <Modal.Header closeButton>
                    <Modal.Title>OK</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h5 style={{ color: 'red' }}>{`${this.state.errorAlertMessage}`}</h5>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={() => this.hideErrorAlert()}>
                        Ok
            </Button>
                </Modal.Footer>
            </Modal>

        </main >

    }

}

export default checkAuthentication(AboutusManagement);



