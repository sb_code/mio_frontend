import React, { Component } from 'react'
import Navbar from '../Component/Navbar'
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button } from "react-bootstrap"
import _ from 'lodash';
import { CSVLink, CSVDownload } from "react-csv";
import ReactTooltip from 'react-tooltip'; // For tool tip 

import { checkAuthentication } from '../Component/Authentication';

var path = cred.API_PATH;

export class AssignOutBoundNumber extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userId: '',
            userType: '',
            selectedCompanyName: '',
            selectedCompanyId: '',

            openSideMenu: false,
            searchCountry: '',
            searchProvider: '',
            searching: false,

            loading: false,
            loading_1: false,
            loading_2: false,

            pageNo1: 1,
            pageNo2: 1,
            numberList: [],
            numberListRecords: '',
            assignedNumbersRecords: '',

            assignedNumbers: [],
            assignedNumberIds: [],
            SIPProviderList: [],
            countryList: [],
            selectedArray: [],

            selectedNumber: {},

            showEditModal: false,
            countryCode: '',
            countryCodeError: '',
            region: '',
            phoneNumberError: '',
            sipProvider: '',
            sipProviderError: '',
            country: '',
            countryError: '',
            costPerMinute: '',
            costPerMinuteError: '',
            isActive: false,

            showSuccessAlert: false,
            successAlertMessage: '',
            showErrorAlert: false,
            errorAlertMessage: '',

        }
        this.perPage1 = 5;
        this.perPage2 = 10;

    }

    async componentDidMount() {

        try {

            document.body.classList.remove("sign-bg");
            document.body.classList.add("grey-bg");

            let storage = localStorage.getItem("MIO_Local");
            storage = storage ? JSON.parse(storage) : null;

            let state = this.props.location.state;
            console.log("state ==", state);

            if (storage && storage.data.userId && state.companyName) {
                this.setState({
                    userId: storage.data.userId,
                    userType: storage.data.userType,
                    companyId: storage.data.companyId,
                    selectedCompanyName: state.companyName,
                    selectedCompanyId: this.props.match.params.id
                }, async () => {
                    let assignedNumberStatus = await this.getSelectedCompanyDetails();
                    let outBoundNumberList = await this.getNumberListForAdmin();

                    
                });
                let tokenData = this.parseJwt(storage.token);
            }

        }
        catch (error) {
            console.log(error);
        }


    }

    getSelectedCompanyDetails = () => {

        return new Promise((resolve, reject) => {
            let dataToSend = {
                "companyId": this.state.selectedCompanyId,
                "page": this.state.pageNo1,
                "perPage": this.perPage1
            }
            this.setState({loading_1: true});
            // API - 
            axios
                .post(path + 'user/get-outbound-area-codes-by-companyid', dataToSend)
                .then(serverResponse => {

                    let res = serverResponse.data;
                    if (!res.isError) {
                        // console.log('getting response----', res.details);
                        let details = res.details;

                        let assignedNumberIds = [];
                        details && details[0] && details[0].results.map(value => {
                            assignedNumberIds.push(value._id);
                        })

                        this.setState({
                            loading_1: false,
                            assignedNumbers: details && details[0] && details[0].results,
                            assignedNumbersRecords: details && details[0] && details[0].noofpage,
                            assignedNumberIds: assignedNumberIds
                        }, () => {
                            // console.log("already assigned to this company ==", this.state.assignedNumbers);
                            resolve();
                        });
                    } else {
                        this.setState({
                            loading_1: false
                        },()=>{
                            reject();
                        });
                        
                    }
                })
                .catch(error => {
                    this.setState({
                        loading_1: false
                    })
                    console.log(error);
                })

        })

    }

    // For getting number list -- 
    getNumberListForAdmin = () => {
        return new Promise((resolve, reject) => {

            let dataToSend = {
                "companyId": this.state.selectedCompanyId,
                "page": this.state.pageNo2,
                "perPage": this.perPage2
            };

            this.setState({ loading_2: true });

            axios
                .post(path + 'services/fetch-sip-out-bound-price-factor', dataToSend)
                .then(serverResponse => {

                    let res = serverResponse.data;
                    if (!res.isError) {
                        let outBoundRecords = res.details && res.details.baseServiceDetails && res.details.baseServiceDetails[0] && res.details.baseServiceDetails[0].results;
                        this.setState({
                            numberList: outBoundRecords,
                            numberListRecords:res.details && res.details.baseServiceDetails && res.details.baseServiceDetails[0] && res.details.baseServiceDetails[0].noofpage,
                            loading_2: false
                        }, () => {
                            console.log("number list is = ", this.state.numberList);
                            resolve();
                        });
                    } else {
                        this.setState({
                            loading_2: false
                        }, () => {
                            reject();
                        });
                    }

                })
                .catch(error => {
                    this.setState({
                        loading_2: false
                    })
                    console.log(error);
                })

        })

    }

    // For getting country list for filter -- 
    getCountrylist = () => {
        let dataToSend = {
            "pageno": '',
            "perPage": '',
        };

        axios
            .post(path + 'services/getCountries', dataToSend)
            .then(serverResponse => {

                let res = serverResponse.data;
                if (!res.isError) {
                    // console.log("country list ==", res);
                    this.setState({
                        countryList: res.details
                    });
                }

            })
            .catch(error => {
                console.log(error);
            })

    }

    // For getting SIP Provider lists --
    getSIPProvidersList = () => {
        let dataToSend = {
            "pageno": '',
            "perPage": '',
        };

        axios
            .post(path + 'services/get-sip-providers', dataToSend)
            .then(serverResponse => {

                let res = serverResponse.data;
                if (!res.isError) {

                    // console.log("country list ==", res);
                    this.setState({
                        SIPProviderList: res.details
                    });

                }

            })
            .catch(error => {
                console.log(error);
            })

    }

    // For searching user - 
    searchNumber = (event) => {
        event.preventDefault();
        let { searchCountry, searchProvider } = this.state;
        this.setState({
            searching: true
        }, () => {
            this.getNumberListForAdmin(searchProvider, searchCountry);
        });

    }

    // For clearing search -
    clearSearch = (event) => {
        if (event) {
            event.preventDefault();
        }
        // let {searchCountry, searchProvider, searching} = this.state;
        this.setState({
            searchCountry: '',
            searchProvider: '',
            searching: false
        }, () => {
            this.getNumberListForAdmin()
        });
    }

    hideEditModal = () => {
        let {numberList} = this.state;
        numberList.map((value, index) => {
            if (value.outboundPriceFactor.multiplier || value.outboundPriceFactor.pricePerMinute) {
                // delete value["isAssigned"];
                delete value.outboundPriceFactor["multiplier"];
                delete value.outboundPriceFactor["pricePerMinute"];
            }
        });

        this.setState({
            loading: false,
            viewAssignModal: false,
            selectedArray: [],
            numberList: numberList
        });

    }

    changeCheckStatus = (index) => {

        let { numberList } = this.state;

        let changedUserList = numberList[index];
        if (changedUserList.outboundPriceFactor.isAssigned) {
            changedUserList.outboundPriceFactor.isAssigned = !changedUserList.outboundPriceFactor.isAssigned;
        } else {
            changedUserList.outboundPriceFactor["isAssigned"] = true;
        }

        numberList[index] = changedUserList;
        this.setState({
            numberList: numberList
        });

    }

    assignNumbers = () => {

        let { numberList } = this.state;
        let selectedArray = [];

        numberList.map((value, index) => {
            if (value.outboundPriceFactor.isAssigned) {
                delete value.outboundPriceFactor["isAssigned"];
                delete value.outboundPriceFactor["_id"];

                selectedArray.push(value.outboundPriceFactor);
            }
        });

        this.setState({
            selectedArray: selectedArray,
            viewAssignModal: true
        });

    }

    assignAll= ()=> {

        let dataToSend = {
            "companyId": this.state.selectedCompanyId
        };
        // API -- 
        this.setState({loading: true});
        axios
        .post(path+'user/assign-whole-outbound-area-code', dataToSend)
        .then(serverResponse=>{
            let res = serverResponse.data;
            if(!res.isError){
                this.setState({
                    loading: false,
                    showSuccessAlert: true,
                    successAlertMessage: 'Successfully assigned.'
                },()=>{
                    this.getSelectedCompanyDetails().then(() => {
                        this.getNumberListForAdmin();
                    })
                });
            }else{
                this.setState({
                    loading: false,
                    showErrorAlert: true,
                    errorAlertMessage: 'Sorry! something is wrong.',
                });
            }
        })
        .catch(error=>{
            console.log(error);
        })

    }

    changeAssignForm = (event, key) => {

        let { selectedArray } = this.state;

        let value = event.target.value;
        let name = event.target.name;
        if (name == "pricePerMinute") {
            selectedArray.map((data, index) => {
                if (index == key) {
                    data["pricePerMinute"] = value;
                    data["multiplier"] = !isNaN(parseFloat(data.costPerMinute) / parseFloat(value)) ? parseFloat(parseFloat(value) / parseFloat(data.costPerMinute)).toFixed('2') : 0.00;
                }
            })
            this.setState({ selectedArray: selectedArray });

        } else if (name == "multiplier") {
            selectedArray.map((data, index) => {
                if (index == key) {
                    data["multiplier"] = value;
                    data["pricePerMinute"] = !isNaN(parseFloat(data.costPerMinute) * parseFloat(value)) ? parseFloat(parseFloat(data.costPerMinute) * parseFloat(value)).toFixed('2') : 0.00;
                }
            })
            this.setState({ selectedArray: selectedArray });

        } else if (name == "customPricePerMinute") {
            selectedArray.map((data, index) => {
                if (index == key) {
                    data["customPricePerMinute"] = value;
                }
            })
            this.setState({ selectedArray: selectedArray });

        }

    }

    UpdateAssignNumber = () => {

        let { selectedArray, selectedCompanyId } = this.state;

        this.setState({ loading: true });

        let dataToSend = {
            "companyId": selectedCompanyId,
            "numbers": selectedArray
        };
        // API --
        axios
            .post(path + 'user/assign-outbound-area-codes', dataToSend)
            .then(serverResponse => {
                let res = serverResponse.data;
                if (!res.isError) {
                    // console.log("response of assign number ==", res);
                    this.setState({
                        loading: false,
                        selectedArray: [],
                        viewAssignModal: false,
                        showSuccessAlert: true,
                        successAlertMessage: 'Successfully assigned.'
                    }, () => {
                        this.clearSearch();
                        this.getSelectedCompanyDetails().then(() => {
                            this.getNumberListForAdmin();
                        })
                    });
                } else {
                    this.setState({
                        loading: false,
                        selectedArray: [],
                        viewAssignModal: false,
                        showErrorAlert: true,
                        errorAlertMessage: 'Sorry! something is wrong.',
                    }, () => {
                        this.clearSearch();
                        this.getSelectedCompanyDetails().then(() => {
                            this.getNumberListForAdmin();
                        })
                    });
                }
            })
            .catch(error => {
                console.log(error);
            })


    }

    changeUnassignStatus = (index) => {

        let { assignedNumbers } = this.state;

        let changedUserList = assignedNumbers[index];
        if (changedUserList.isChecked) {
            changedUserList.isChecked = !changedUserList.isChecked;
        } else {
            changedUserList["isChecked"] = true;
        }

        assignedNumbers[index] = changedUserList;
        this.setState({
            assignedNumbers: assignedNumbers
        });

    }

    unassignNumbers = () => {

        let { assignedNumbers, selectedCompanyId } = this.state;
        let unAssignArray = [];

        assignedNumbers.map((value, index) => {
            if (value.isChecked) {
                delete value["isChecked"];
                unAssignArray.push(value._id);
            }
        });

        if(!unAssignArray || unAssignArray.length == 0){
            this.setState({
                showErrorAlert: true,
                errorAlertMessage: 'No number selected.'
            });
        }else{
        
            this.setState({
                loading: true,
                assignedNumbers: assignedNumbers
            }, () => {
                console.log("unassign time assignedNumbers", this.state.assignedNumbers);
            });

            let dataToSend = {
                "companyId": selectedCompanyId,
                "numberIds": unAssignArray
            };

            axios
                .post(path + 'user/unassign-outbound-area-codes', dataToSend)
                .then(serverResponse => {
                    let res = serverResponse.data;
                    if (!res.isError) {
                        this.setState({
                            loading: false,
                            showSuccessAlert: true,
                            successAlertMessage: 'Successfully un-assigned.'
                        }, () => {
                            this.clearSearch();
                            this.getSelectedCompanyDetails().then(() => {
                                this.getNumberListForAdmin();
                            });
                        });
                    } else {
                        this.setState({
                            loading: false,
                            showErrorAlert: true,
                            errorAlertMessage: 'Sorry! something is wrong.'
                        }, () => {
                            this.clearSearch();
                            this.getSelectedCompanyDetails().then(() => {
                                this.getNumberListForAdmin();
                            });
                        });
                    }
                })
                .catch(error => {
                    console.log(error);
                })
        }
    }

    pageChangePrev = (no) => {
        if (no == 1) {
            this.setState({
                pageNo1: (parseFloat(this.state.pageNo1) - 1)
            }, () => {
                this.getSelectedCompanyDetails()
            });
        } else if (no == 2) {
            this.setState({
                pageNo2: (parseFloat(this.state.pageNo2) - 1)
            }, () => {
                this.getNumberListForAdmin()
            });
        }

    }

    pageChangeNext = (no) => {
        if (no == 1) {
            this.setState({
                pageNo1: (parseFloat(this.state.pageNo1) + 1)
            }, () => {
                this.getSelectedCompanyDetails();
            });
        } else if (no == 2) {
            this.setState({
                pageNo2: (parseFloat(this.state.pageNo2) + 1)
            }, () => {
                this.getNumberListForAdmin();
            });
        }

    }

    clearAlert = () => {
        this.setState({
            loading: false,
            showSuccessAlert: false,
            successAlertMessage: '',
            showErrorAlert: false,
            errorAlertMessage: '',
        });
    }

    sideMenuToggle = () => {
        let { openSideMenu } = this.state;
        openSideMenu = !openSideMenu;
        this.setState({ openSideMenu });
    }

    backMenueToggle = () => {
        this.props.history.goBack();
    }

    mobMenuClick() {
        this.sideMenuToggle();
        console.log('mobMenuClick');
    }

    parseJwt = (token) => {
        if (!token) { return; }
        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace('-', '+').replace('_', '/');
        return JSON.parse(window.atob(base64));
    }


    render() {

        let { loading, loading_1, loading_2, selectedNumber, assignedNumbers, assignedNumberIds, numberList, pageNo1, pageNo2, numberListRecords, selectedArray, assignedNumbersRecords } = this.state;
        // const perPage = this.perPage;

        return <main>
            <section className="user-mngnt-wrap">
                <div className="container-fluid">
                    <div className="row">

                        <Navbar routTo="/numberconfigue" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />
                        <div className="col-xl-10 col-lg-9 p-0 mob-full-conf">

                            <div className="mobile-menu-header">
                                <a href="JavaScript:Void(0);" className="back-arw" onClick={() => this.backMenueToggle()}><i className="fas fa-arrow-left"></i></a>
                                {/* <a href="JavaScript:Void(0);" className="menu-tgl" onClick={() => this.sideMenuToggle()}><img src="images/menu-tgl.png" alt="" /></a> */}
                            </div>

                            <div className="mng-full-list">

                                <div className="mng-full-srch">
                                    {/* <div className="row">
                                        <div className="col-margin col-md-4">
                                            <select className="form-control"
                                                name="searchType"
                                                value={this.state.searchCountry}
                                                onChange={(event) => { this.setState({ searchCountry: event.target.value }) }}
                                            >
                                                <option value="">Select Country</option>
                                                {countryList.map((value, index) => {
                                                    return <option value={value._id.country} key={index}>
                                                        {value._id.country}
                                                    </option>
                                                })}
                                            </select>
                                        </div>
                                        <div className="col-margin col-md-6">
                                            <select className="form-control"
                                                name="searchType"
                                                value={this.state.searchProvider}
                                                onChange={(event) => { this.setState({ searchProvider: event.target.value }) }}
                                            >
                                                <option value="">Select SIP Provider</option>
                                                {SIPProviderList.map((value, index) => {
                                                    return <option value={value._id.sipProvider} key={index}>
                                                        {value._id.sipProvider}
                                                    </option>
                                                })}
                                            </select>
                                        </div>
                                        <div className="col col-md-2">
                                            <div className="srch-wrap">
                                                <form>
                                                    {this.state.searching ?
                                                        <input type="button"
                                                            data-tip='Clear'
                                                            onClick={(event) => this.clearSearch(event)}
                                                        />
                                                        :
                                                        <input type="submit"
                                                            data-tip='Search'
                                                            onClick={(event) => this.searchNumber(event)}
                                                        />
                                                    }
                                                    <ReactTooltip
                                                        effect="float"
                                                        place="top"
                                                        data-border="true"
                                                    />
                                                </form>
                                            </div>
                                        </div>
                                    </div> */}

                                </div>

                                <div className="mng-full-table">
                                    <span>Already Assigned Outbound Numbers - </span>
                                    <div className="row">

                                        <div className="col-md-2">
                                            <div className="mng-full-table-hdr">
                                                <div className="row">
                                                    <div className="col-md-6">
                                                        <h6>Select</h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-md-10">
                                            <div className="mng-full-table-hdr">
                                                <div className="row">
                                                    <div className="col-md-4 border-rt">
                                                        <h6>Region</h6>
                                                    </div>
                                                    <div className="col-md-4 border-rt">
                                                        <h6>Code</h6>
                                                    </div>
                                                    <div className="col-md-4">
                                                        <h6>SIP Provider</h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br /><br />
                                        {!loading && assignedNumbers?
                                            <button className="btn btn-primary" onClick={() => { this.unassignNumbers() }}>Unassign form {this.state.selectedCompanyName}</button>
                                        :
                                            <button className="btn btn-primary" >Unassign from {this.state.selectedCompanyName}</button>
                                        }
                                        <br/>
                                    </div>

                                    {assignedNumbers && assignedNumbers.map((user, index) => {

                                        return <div className="row" key={index}>

                                            <div className="col-md-2">
                                                <div className="mng-full-table-row add-edt text-center">
                                                    <div className="row">

                                                        <div className="col-md-6">
                                                            <input
                                                                type="checkbox"
                                                                checked={user.isChecked ? true : false}
                                                                onChange={() => { this.changeUnassignStatus(index) }}
                                                            />
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                            <div className="col-md-10">
                                                <div className="mng-full-table-row">
                                                    <div className="row">
                                                        <div className="col-md-4 border-rt">
                                                            <h6>Region</h6>
                                                            <p className="textEllips">
                                                                <a href="JavaScript:Void(0);" >
                                                                    {user.region}
                                                                </a>
                                                            </p>
                                                        </div>
                                                        <div className="col-md-4 border-rt">
                                                            <h6>Code</h6>
                                                            <p className="textEllips">{user.dialCode}</p>
                                                        </div>
                                                        <div className="col-md-4">
                                                            <h6>SIP Provider</h6>
                                                            <p className="textEllips">
                                                                {user.provider}
                                                            </p>
                                                        </div>

                                                        <div className="mobile-ad-edt-btns">
                                                            <ul>

                                                                <li>
                                                                    <input
                                                                        type="checkbox"
                                                                        defaultChecked={false}
                                                                        onChange={() => { this.changeUnassignStatus(index) }}
                                                                    />
                                                                </li>

                                                            </ul>
                                                        </div>

                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                    })}

                                </div>

                                <br /><br />
                                <div className="form-group">
                                    {!loading_1 && !loading && parseFloat(pageNo1) > 1 ?
                                        <span style={{ color: "#007bff", cursor: "pointer" }}
                                            onClick={() => { this.pageChangePrev(1) }}
                                        >
                                            <b>{"<< Prev"}</b>
                                        </span>
                                        :
                                        null}
                                        &nbsp;&nbsp;|&nbsp;&nbsp;
                                        {!loading_1 && !loading && (parseFloat(pageNo1) < parseFloat(assignedNumbersRecords)) ?
                                        <span style={{ color: "#007bff", cursor: "pointer" }}
                                            onClick={() => { this.pageChangeNext(1) }}
                                        >
                                            <b>{"Next >>"}</b>
                                        </span>
                                        :
                                        null}
                                    {loading_1 || loading ?
                                        <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                                            <i className="fa fa-spinner fa-spin" ></i>
                                        </a>
                                        :
                                        null}

                                </div>

                                <br /><br /><br />
                                {numberList && numberList.length > 0 ?
                                    <div className="mng-full-table">
                                        <span>Available Outbound Numbers - </span>
                                        <div className="row">

                                            <div className="col-md-2">
                                                <div className="mng-full-table-hdr">
                                                    <div className="row">
                                                        <div className="col-md-6">
                                                            <h6>Select</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="col-md-10">
                                                <div className="mng-full-table-hdr">
                                                    <div className="row">
                                                        <div className="col-md-4 border-rt">
                                                            <h6>Region</h6>
                                                        </div>
                                                        <div className="col-md-4 border-rt">
                                                            <h6>Code</h6>
                                                        </div>
                                                        <div className="col-md-4">
                                                            <h6>SIP Provider</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br /><br />
                                            {!loading && numberList?
                                                <button className="btn btn-primary" onClick={() => { this.assignNumbers() }}>Assign to {this.state.selectedCompanyName}</button>
                                            :
                                                <button className="btn btn-primary" >Assign to {this.state.selectedCompanyName}</button>
                                            }
                                            {!loading?
                                                <button className="btn btn-primary" onClick={() => { this.assignAll() }}>Assign all to {this.state.selectedCompanyName}</button>
                                            :null}
                                            {/* <br/><br/> */}
                                        </div>

                                        {this.state.numberList.map((user, index) => {

                                            // return !assignedNumberIds.includes(user.outboundPriceFactor._id) ?
                                            return<div className="row" key={index}>

                                                    <div className="col-md-2">
                                                        <div className="mng-full-table-row add-edt text-center">
                                                            <div className="row">

                                                                <div className="col-md-6">
                                                                    <input
                                                                        type="checkbox"
                                                                        checked={user && user.outboundPriceFactor && user.outboundPriceFactor.isAssigned ? true : false}
                                                                        onChange={() => { this.changeCheckStatus(index) }}
                                                                    />
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="col-md-10">
                                                        <div className="mng-full-table-row">
                                                            <div className="row">
                                                                <div className="col-md-4 border-rt">
                                                                    <h6>Region</h6>
                                                                    <p className="textEllips">
                                                                        <a href="JavaScript:Void(0);">
                                                                            {user && user.outboundPriceFactor && user.outboundPriceFactor.region}
                                                                        </a>
                                                                    </p>
                                                                </div>
                                                                <div className="col-md-4 border-rt">
                                                                    <h6>Code</h6>
                                                                    <p className="textEllips">{user && user.outboundPriceFactor && user.outboundPriceFactor.dialCode}</p>
                                                                </div>
                                                                <div className="col-md-4">
                                                                    <h6>SIP Provider</h6>
                                                                    <p className="textEllips">
                                                                        {user && user.outboundPriceFactor && user.outboundPriceFactor.provider}
                                                                    </p>
                                                                </div>

                                                                <div className="mobile-ad-edt-btns">
                                                                    <ul>

                                                                        <li>
                                                                            <input
                                                                                type="checkbox"
                                                                                defaultChecked={user && user.outboundPriceFactor && user.outboundPriceFactor.isAssigned}
                                                                                onChange={() => { this.changeCheckStatus(index) }}
                                                                            />
                                                                        </li>

                                                                    </ul>
                                                                </div>

                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>

                                            // : null
                                        })
                                        }

                                    </div>
                                    :
                                    <div className="mng-full-table">
                                        <div className="row">
                                            <div className="col-md-12">
                                                <div className="mng-full-table-hdr admn-usr-hdr">
                                                    <div className="row">
                                                        <div className="col-md-12">
                                                            <h6>No Number Found.</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                }

                                <br /><br />
                                <div className="form-group">
                                    {!loading_2 && !loading && parseFloat(pageNo2) > 1 ?
                                        <span style={{ color: "#007bff", cursor: "pointer" }}
                                            onClick={() => { this.pageChangePrev(2) }}
                                        >
                                            <b>{"<< Prev"}</b>
                                        </span>
                                        :
                                        null}
                                        &nbsp;&nbsp;|&nbsp;&nbsp;
                                        {!loading_2 && !loading && (parseFloat(pageNo2) < parseFloat(numberListRecords)) ?
                                        <span style={{ color: "#007bff", cursor: "pointer" }}
                                            onClick={() => { this.pageChangeNext(2) }}
                                        >
                                            <b>{"Next >>"}</b>
                                        </span>
                                        :
                                        null}
                                    {loading_2 || loading ?
                                        <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                                            <i className="fa fa-spinner fa-spin" ></i>
                                        </a>
                                        :
                                        null}

                                </div>

                            </div>

                        </div>
                    </div>
                </div>

            </section>

            <Modal size="lg" show={this.state.viewAssignModal} onHide={() => { this.hideEditModal() }}>
                <Modal.Header closeButton>
                    <Modal.Title>Assign Number</Modal.Title>
                </Modal.Header>
                <Modal.Body>

                    {selectedArray && selectedArray.map((data, index) => {
                        return <div className="row" key={index}>

                            <div className="col-md-3">
                                <div className="form-group">
                                    <label>Region:</label>
                                    <input
                                        type="text"
                                        name="region"
                                        value={data.region}
                                        className="form-control"
                                        placeholder="Enter Phone no."
                                        onChange={event => this.setState({ region: event.target.value })}
                                        onFocus={() => this.setState({ phoneNumberError: "" })}
                                        readOnly={true}
                                    />

                                </div>
                            </div>

                            <div className="col-md-3">
                                <div className="form-group">
                                    <label>Code:</label>
                                    <input
                                        type="tel"
                                        name="code"
                                        value={data.dialCode}
                                        className="form-control"
                                        //placeholder="Enter Mobile Number"
                                        onChange={event => this.setState({ code: event.target.value })}
                                        onFocus={() => this.setState({ countryError: "" })}
                                        readOnly={true}
                                    />

                                </div>
                            </div>

                            <div className="col-md-2">
                                <div className="form-group">
                                    <label>Cost(/min):</label>
                                    <input
                                        type="text"
                                        name="costPerMinute"
                                        value={parseFloat(data.costPerMinute).toFixed('2')}
                                        className="form-control"
                                        placeholder="Cost per minute"
                                        onChange={event => this.setState({ costPerMinute: event.target.value })}
                                        onFocus={() => this.setState({ costPerMinuteError: "" })}
                                        readOnly={true}
                                    />

                                </div>
                            </div>

                            <div className="col-md-2">
                                <div className="form-group">
                                    <label>Multiplier:</label>
                                    <input
                                        type="text"
                                        name="multiplier"
                                        value={(data.multiplier) ? (data.multiplier) : ''}
                                        className="form-control"
                                        placeholder="0.00"
                                        onChange={(event) => { this.changeAssignForm(event, index) }}
                                    />
                                    <span style={{ color: 'red' }}>{!data.multiplier ? '* Please give valid multiplier' : ''}</span>
                                </div>
                            </div>

                            <div className="col-md-2">
                                <div className="form-group">
                                    <label>Price(/min):</label>
                                    <input
                                        type="text"
                                        name="pricePerMinute"
                                        value={(data.pricePerMinute) ? (data.pricePerMinute) : ''}
                                        className="form-control"
                                        placeholder="Rs/min"
                                        onChange={(event) => { this.changeAssignForm(event, index) }}
                                    />
                                    <span style={{ color: 'red' }}>{!data.pricePerMinute ? '* Please give valid price' : ''}</span>
                                </div>
                            </div>

                        </div>
                    })}

                </Modal.Body>
                <Modal.Footer>
                    {!loading ?
                        <Button variant="primary" onClick={() => this.UpdateAssignNumber()}>
                            Assign
                        </Button>
                        :
                        null}
                    {loading ?
                        <Button variant="primary" >
                            <i className="fa fa-spinner fa-spin" ></i>
                        </Button>
                        :
                        null}
                </Modal.Footer>
            </Modal>

            <Modal show={this.state.showSuccessAlert} onHide={() => this.clearAlert()}>
                <Modal.Header closeButton>
                    <Modal.Title>OK</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h5 style={{ color: 'green' }}>{`${this.state.successAlertMessage}`}</h5>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={() => this.clearAlert()}>
                        Ok
            </Button>
                </Modal.Footer>
            </Modal>

            <Modal show={this.state.showErrorAlert} onHide={() => this.clearAlert()}>
                <Modal.Header closeButton>
                    <Modal.Title>OK</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h5 style={{ color: 'red' }}>{`${this.state.errorAlertMessage}`}</h5>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={() => this.clearAlert()}>
                        Ok
            </Button>
                </Modal.Footer>
            </Modal>

        </main>

    }

}

export default checkAuthentication(AssignOutBoundNumber);