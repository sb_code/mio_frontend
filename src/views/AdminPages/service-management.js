import React, { Component } from "react";
import Navbar from "../Component/Navbar";
import ProgressBar from 'react-bootstrap/ProgressBar';
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button, Accordion, Card } from "react-bootstrap"
import { CSVReader } from 'react-papaparse';
import _ from 'lodash';
import { CSVLink, CSVDownload } from "react-csv";
import ReactTooltip from 'react-tooltip'; // For tool tip 

import { checkAuthentication } from '../Component/Authentication';

var path = cred.API_PATH;

export class ServiceManagement extends Component {
	constructor(props) {
		super(props);
		this.state = {
			userId: '',

			openSideMenu: false,
			searchString: '',
			searchType: '',
			searching: false,

			rowId: '',
			services: [],
			services1: [],

			loading: false,
			serviceLoading: false,
			inboundLoading: false,
			outboundLoading: false,
			countrycodeLoading: false,
			globalLoading: false,

			serviceName: '',
			costPerMin: 0.0,
			multiplier: 0.0,
			pricePerMin: 0.0,
			selectedService: '',
			serviceCreatedBy: '',

			serviceNameError: '',
			costPerMinError: '',
			pricePerMinError: '',
			multiplierError: '',

			companyType: '',
			noOfSeats: '0',
			pricePerSeat: '0.00',
			trialPeriod: '0',
			voiceUrl: '',
			brandVoiceUrl: '',
			currency: '',
			vat: '0.00',

			fileUploaded: '',
			invoiceBillingInfo: "",

			companyTypeError: '',
			trialPeriodError: '',
			pricePerSeatError: '',
			noOfSeatsError: '',
			currencyError: '',

			deleteItem: '',
			editService: false,
			editServiceConfig: false,
			addService: false,
			deleteService: false,
			deleteOthService: false,

			numberList: [],
			data: [],
			fileName: '',
			showImportAddress: false,
			importDataError: '',
			EmailError: [],

			numberListRecords: '',
			countryCodeRecords: '',
			outBoundRecords: '',

			codeData: [],
			fileCCName: '',
			showImportCode: false,
			importCCDataError: '',

			outBoundData: [],
			fileOBName: '',
			showImportOB: false,
			importOBDataError: '',

			showSuccessAlert: false,
			successAlertMessage: '',
			showErrorAlert: false,
			errorAlertMessage: '',

			globalFieldName: '',
			globalFieldValue: '',
			globalFieldEditType: '',
			showGlobalConfig: false,

			entryToneURL: '',
			exitToneURL: '',
			musicOnHoldURL: '',
			archiveStartURL: '',
			archiveEndURL: '',
			brandVoiceUrl: '',
			archiveLife: '',
			vatNumber: '',

		}
		this.fileInput = React.createRef();
		this.fileCCInput = React.createRef();
		this.fileOBInput = React.createRef();
	}

	componentDidMount() {
		document.body.classList.remove("sign-bg");
		document.body.classList.add("grey-bg");

		let storage = localStorage.getItem("MIO_Local");
		storage = storage ? JSON.parse(storage) : null;
		if (storage && storage.data.userId) {
			this.setState({
				userId: storage.data.userId,
				userType: storage.data.userType,
				companyId: storage.data.companyId
			}, () => {
				this.getServiceListForAdmin();
				this.getNumberListForAdmin();
				this.getCountryCodeListForAdmin();
				this.getOutBoundListForAdmin();
				this.getGlobalData();
				console.log('user management--', this.state.userType);
			});

			// console.log(this.parseJwt(storage.token));
			let tokenData = this.parseJwt(storage.token);

		}

	}

	getServiceListForAdmin = () => {
		let that = this;
		this.setState({
			serviceLoading: true
		});
		// API - 
		axios
			.post(path + 'services/get-base-services')
			.then(serverResponse => {

				let res = serverResponse.data;
				if (!res.isError) {
					let rowId = res.details && res.details[0] && res.details[0]._id;
					let services = res.details && res.details[0].services;
					let services1 = res.details && res.details[0].serviceConfig;
					that.setState({
						rowId: rowId,
						services: services,
						services1: services1,
						serviceLoading: false
					}, () => {
						console.log('Service list -- ', this.state.services);
					});
				} else {
					that.setState({
						serviceLoading: false
					})
				}
			})
			.catch(error => {
				console.log(error);
				that.setState({
					serviceLoading: false
				})
			})

	}

	getNumberListForAdmin = () => {

		let dataToSend = {
			pageno: '',
			perPage: '',
			sipProvider: '',
			country: ''
		};
		this.setState({ inboundLoading: true })
		// API --
		axios
			.post(path + 'services/get-inbound-numbers', dataToSend)
			.then(serverResponse => {
				let res = serverResponse.data;
				if (!res.isError) {
					console.log("Inbound Number Lists===", res);
					let details = res.details;
					let results = details && details[0].results;
					let numberListRecords = details && details[0].total;

					this.setState({
						inboundLoading: false,
						numberListRecords: numberListRecords
					});

				} else {
					this.setState({ inboundLoading: false })
				}
			})
			.catch(error => {
				this.setState({ inboundLoading: false })
				console.log(error);
			})

	}

	getCountryCodeListForAdmin = () => {

		let dataToSend = {
		};
		this.setState({
			countrycodeLoading: true
		})
		// API --
		axios
			.post(path + 'services/get-country-codes', dataToSend)
			.then(serverResponse => {
				let res = serverResponse.data;
				if (!res.isError) {

					let countryCodeRecords = res.details;
					this.setState({
						countrycodeLoading: false,
						countryCodeRecords: countryCodeRecords
					});

				} else {
					this.setState({ countrycodeLoading: false })
				}
			})
			.catch(error => {
				this.setState({ countrycodeLoading: false })
				console.log(error);
			})

	}

	getOutBoundListForAdmin = () => {

		let dataToSend = {
			"companyId": '',
			"page": 1,
			"perPage": 10
		};
		this.setState({ outboundLoading: true })
		// API --
		axios
			.post(path + 'services/fetch-sip-out-bound-price-factor', dataToSend)
			.then(serverResponse => {
				let res = serverResponse.data;
				if (!res.isError) {

					let outBoundRecords = res.details && res.details.baseServiceDetails && res.details.baseServiceDetails[0] && res.details.baseServiceDetails[0].total;
					this.setState({
						outboundLoading: false,
						outBoundRecords: outBoundRecords
					});

				} else {
					this.setState({ outboundLoading: false })
				}
			})
			.catch(error => {
				this.setState({ outboundLoading: false })
				console.log(error);
			})

	}

	addServiceModal = (event) => {
		this.setState({
			addService: true
		});
	}

	addServiceFirst() {
		let that = this;
		this.setState({
			loading: true
		});

		let prmiceArr = [];

		Object.keys(cred.SERVICES).map((key) => {
			let a = new Promise((resolve, reject) => {
				let dataToSend = {
					"serviceName": cred.SERVICES[key].key,
					"serviceKey": cred.SERVICES[key].key,
					"costPerMinute": 1,
					"pricePerMinute": 1,
					"multiplier": 1,
					"createdBy": this.state.userId
				};
				// API - 
				axios
					.post(path + 'services/add-services', dataToSend)
					.then(serverResponse => {
						let res = serverResponse.data;
						resolve();
						if (!res.isError) {
						} else {
						}
					})
					.catch(error => {
						resolve();
						console.log(error);
					})
			});
			prmiceArr.push(a);
		})

		Promise.all(prmiceArr).then(value => {
			that.setState({
				loading: false
			}, () => {
				that.clearState();
				that.getServiceListForAdmin();
			})
		});
	}

	addService = () => {

		let { serviceName, costPerMin, pricePerMin, multiplier, userId, selectedService, serviceCreatedBy } = this.state;
		let that = this;
		!serviceName ? this.setState({ serviceNameError: "Please enter service name." }) : this.setState({ serviceNameError: "" });
		!costPerMin || isNaN(costPerMin) ? this.setState({ costPerMinError: "Please enter valid cost per minute." }) : this.setState({ costPerMinError: "" });
		!pricePerMin || isNaN(pricePerMin) ? this.setState({ pricePerMinError: "Please enter valid price per minute." }) : this.setState({ pricePerMinError: "" });
		!multiplier || isNaN(multiplier) ? this.setState({ multiplierError: "Please enter valid price per minute." }) : this.setState({ multiplierError: "" });

		if (serviceName && costPerMin && pricePerMin && multiplier && !isNaN(costPerMin) && !isNaN(pricePerMin) && !isNaN(multiplier)) {

			this.setState({
				loading: true
			});
			let dataToSend = {
				"serviceName": serviceName,
				"serviceKey": serviceName,
				"costPerMinute": costPerMin,
				"pricePerMinute": pricePerMin,
				"multiplier": multiplier,
				"createdBy": userId
			};
			// API - 
			axios
				.post(path + 'services/add-services', dataToSend)
				.then(serverResponse => {
					let res = serverResponse.data;
					if (!res.isError) {
						// console.log('After add a service - ', res);
						that.setState({
							loading: false
						}, () => {
							that.clearState();
							that.getServiceListForAdmin();
						})

					} else {
						alert("Sorry! Try again.");
						that.setState({
							loading: false
						}, () => {
							that.clearState();
						});
					}
				})
				.catch(error => {
					console.log(error);
				})
		}

	}

	addOthServiceModal = () => {

		this.setState({
			addOthService: true
		});

	}

	addOthServiceFirst() {
		let that = this;
		this.setState({
			loading: true
		});
		let companyTypeArray = [cred.COMPANY_TYPE_FREE_TYPE, cred.COMPANY_TYPE_STARTUP, cred.COMPANY_TYPE_ENTERPRISE];
		let promiceArr = [];

		companyTypeArray.map((companyType) => {
			let a = new Promise((resolve, reject) => {
				let dataToSend = {
					"companyType": companyType,
					"trialPeriod": 0,
					"pricePerSeat": 1,
					"numberOfSeats": 5,
					"defaultCurrency": cred.CURRENCY.gbp,
					"defaultVat": 2,
					"invoiceBillingInfo": '',
					"customVoiceUrl": '',
					"brandVoiceUrl": ''
				};

				axios
					.post(path + 'services/add-services-config', dataToSend)
					.then(serverResponse => {
						let res = serverResponse.data;
						resolve();
						if (!res.isError) {
						} else {
						}
					})
					.catch(error => {
						resolve()
						console.log(error);
					})
			});
			promiceArr.push(a);
		});

		Promise.all(promiceArr).then(value => {
			that.setState({
				loading: false
			}, () => {
				that.clearState();
				that.getServiceListForAdmin();
			})
		});
	}

	addOthService = (event) => {

		let { companyType, trialPeriod, pricePerSeat, noOfSeats, currency, vat, invoiceBillingInfo } = this.state;

		!companyType ? this.setState({ companyTypeError: "Please select company type." }) : this.setState({ companyTypeError: "" });
		!trialPeriod || isNaN(trialPeriod) ? this.setState({ trialPeriodError: "Please enter valid trial Period." }) : this.setState({ trialPeriodError: "" });
		!pricePerSeat || isNaN(pricePerSeat) ? this.setState({ pricePerSeatError: "Please enter valid price per seat." }) : this.setState({ pricePerSeatError: "" });
		!noOfSeats || isNaN(noOfSeats) ? this.setState({ noOfSeatsError: "Please enter valid no of seats." }) : this.setState({ noOfSeatsError: "" });
		// !currency ? this.setState({ currencyError: "Please enter valid currency." }) : this.setState({ currencyError: "" });
		!vat ? this.setState({ vatError: "Please enter valid vat." }) : this.setState({ vatError: "" });

		if (companyType && trialPeriod && pricePerSeat && noOfSeats && vat && !isNaN(trialPeriod) && !isNaN(pricePerSeat) && !isNaN(noOfSeats)) {
			this.setState({
				loading: true
			});
			let dataToSend = {
				"companyType": this.state.companyType,
				"trialPeriod": this.state.trialPeriod,
				"pricePerSeat": this.state.pricePerSeat,
				"numberOfSeats": this.state.noOfSeats,
				"defaultCurrency": cred.CURRENCY.gbp,
				"defaultVat": this.state.vat,
				"invoiceBillingInfo": this.state.invoiceBillingInfo,
				"customVoiceUrl": this.state.voiceUrl,
				// "brandVoiceUrl": this.state.brandVoiceUrl
			};

			axios
				.post(path + 'services/add-services-config', dataToSend)
				.then(serverResponse => {
					let res = serverResponse.data;
					if (!res.isError) {
						// console.log("response from add-services-config--- ", res);

						this.clearState();
						this.getServiceListForAdmin();
					} else {
						alert("Sorry! Something is wrong.");
						this.clearState();
					}
				})
				.catch(error => {
					console.log(error);
				})

		}

	}

	serviceConfiguration = (index) => {
		let { services } = this.state;

		let service = services[index];

		let { _id, serviceName, costPerMinute, multiplier, pricePerMinute, createdBy } = service;

		this.setState({
			editService: true,
			serviceName: serviceName,
			costPerMin: costPerMinute,
			multiplier: multiplier,
			pricePerMin: pricePerMinute,
			selectedService: _id,
			serviceCreatedBy: createdBy,
		});

	}

	editUsers = () => {

		let { serviceName, costPerMin, pricePerMin, multiplier, userId, rowId, selectedService, serviceCreatedBy } = this.state;

		!serviceName ? this.setState({ serviceNameError: "Please enter service name." }) : this.setState({ serviceNameError: "" });
		!costPerMin || isNaN(costPerMin) ? this.setState({ costPerMinError: "Please enter valid cost per minute." }) : this.setState({ costPerMinError: "" });
		!pricePerMin || isNaN(pricePerMin) ? this.setState({ pricePerMinError: "Please enter valid price per minute." }) : this.setState({ pricePerMinError: "" });
		!multiplier || isNaN(multiplier) ? this.setState({ multiplierError: "Please enter valid price per minute." }) : this.setState({ multiplierError: "" });

		if (serviceName && costPerMin && pricePerMin && multiplier && !isNaN(costPerMin) && !isNaN(pricePerMin) && !isNaN(multiplier)) {
			this.setState({
				loading: true
			});
			let dataToSend = {
				"serviceId": selectedService,
				"rowId": rowId,
				"serviceName": serviceName,
				"costPerMinute": costPerMin,
				"multiplier": multiplier,
				"pricePerMinute": pricePerMin,
				"createdBy": serviceCreatedBy
			};
			// API ToDo
			axios
				.post(path + 'services/edit-base-services', dataToSend)
				.then(serverResponse => {
					let res = serverResponse.data;
					if (!res.isError) {
						this.clearState();
						this.getServiceListForAdmin();
					} else {

						alert("Sorry! Try again.");
						this.setState({
							loading: false
						}, () => {
							this.clearState();
						});
					}

				})

		}

	}

	deleteConfirm = (index) => {
		let { services } = this.state;
		let service = services[index];
		let { _id, serviceName } = service;

		this.setState({
			selectedService: _id,
			deleteItem: serviceName,
			deleteService: true
		});

	}

	hideDeleteConfirm = () => {
		this.setState({
			selectedService: '',
			deleteItem: '',
			deleteService: false,
			deleteOthService: false

		});
	}

	deleteService = () => {
		let dataToSend = {
			"serviceId": this.state.selectedService,
			"rowId": this.state.rowId
		};
		// API - 
		axios
			.post(path + 'services/delete-base-services', dataToSend)
			.then(serverResponse => {
				let res = serverResponse.data;
				if (!res.isError) {
					alert("Successfully Deleted.");
					this.hideDeleteConfirm();
					this.getServiceListForAdmin();
				} else {
					alert("Sorry! something is going wrong.");
				}
			})
			.catch(error => {
				console.log(error);
			})

	}

	serviceConfigEdit = (index) => {

		let { services1 } = this.state;
		let service1 = services1[index];
		let { _id, companyType, numberOfSeats, pricePerSeat, trialPeriod, defaultCurrency, defaultVat, customVoiceUrl, brandVoiceUrl, invoiceBillingInfo } = service1;

		this.setState({
			editServiceConfig: true,
			companyType: companyType,
			noOfSeats: numberOfSeats,
			pricePerSeat: pricePerSeat,
			trialPeriod: trialPeriod,
			selectedService: _id,
			currency: defaultCurrency,
			vat: defaultVat,
			invoiceBillingInfo,
			voiceUrl: customVoiceUrl,
			// brandVoiceUrl: brandVoiceUrl
		});

	}

	editOthService = (event) => {

		let { selectedService, companyType, trialPeriod, pricePerSeat, noOfSeats, currency, companyTypeError, trialPeriodError, pricePerSeatError, noOfSeatsError, currencyError, vat, invoiceBillingInfo } = this.state;

		!companyType ? this.setState({ companyTypeError: "Please select company type." }) : this.setState({ companyTypeError: "" });
		companyType == "FREE_TYPE" && !trialPeriod ? this.setState({ trialPeriodError: "Please enter valid trial Period." }) : this.setState({ trialPeriodError: "" });
		!pricePerSeat || isNaN(pricePerSeat) ? this.setState({ pricePerSeatError: "Please enter valid price per seat." }) : this.setState({ pricePerSeatError: "" });
		!noOfSeats || isNaN(noOfSeats) ? this.setState({ noOfSeatsError: "Please enter valid no of seats." }) : this.setState({ noOfSeatsError: "" });
		// !currency ? this.setState({ currencyError: "Please enter valid currency." }) : this.setState({ currencyError: "" });
		!vat ? this.setState({ vatError: "Please enter valid vat." }) : this.setState({ vatError: "" });


		// if (companyTypeError == "" && trialPeriodError == "" && pricePerSeatError == "" && noOfSeatsError == "" && currencyError == "") {
		if (companyType && pricePerSeat && !isNaN(pricePerSeat) && noOfSeats && !isNaN(noOfSeats) && vat) {
			this.setState({
				loading: true
			});
			let dataToSend = {
				"configId": selectedService,
				"rowId": this.state.rowId,
				"trialPeriod": trialPeriod,
				"numberOfSeats": noOfSeats,
				"defaultCurrency": cred.CURRENCY.gbp,
				"defaultVat": vat,
				"pricePerSeat": pricePerSeat,
				invoiceBillingInfo,
				"customVoiceUrl": this.state.voiceUrl,
				// "brandVoiceUrl": this.state.brandVoiceUrl
			};

			console.log("editing service configue == ", dataToSend);

			axios
				.post(path + 'services/edit-service-config', dataToSend)
				.then(serverResponse => {

					let res = serverResponse.data;
					if (!res.isError) {
						console.log("editing service configue---", res);
						this.clearState();
						this.getServiceListForAdmin();
					} else {
						alert("Sorry! Something is wrong.");
						this.clearState();
					}

				})
				.catch(error => {
					console.log(error);
				})

		}

	}

	deleteserviceConfigConfirm = (index) => {

		let { services1 } = this.state;
		let service1 = services1[index];
		let { _id, companyType } = service1;

		this.setState({
			selectedService: _id,
			deleteItem: companyType,
			deleteService: true,
			deleteOthService: true
		});

	}

	deleteOthService = () => {

		let dataToSend = {
			"configId": this.state.selectedService,
			"rowId": this.state.rowId
		};

		axios
			.post(path + 'services/delete-services-config', dataToSend)
			.then(serverResponse => {
				let res = serverResponse.data;
				if (!res.isError) {
					alert("Successfully Deleted.");
					this.hideDeleteConfirm();
					this.getServiceListForAdmin();
				} else {
					alert("Sorry! Something is wrong.");
					this.hideDeleteConfirm();
				}
			})
			.catch(error => {
				console.log(error);
			})

	}

	clearSearch = (event) => {
		event.preventDefault();
		this.setState({
			searching: false,
			searchString: '',
		}, () => { this.getServiceListForAdmin() })
	}

	changecostPerMin = (event) => {
		if (!isNaN(event.target.value)) {
			let pricePerMin = parseFloat(this.state.multiplier) * parseFloat(event.target.value);
			pricePerMin = isNaN(parseFloat(pricePerMin)) ? 0 : parseFloat(pricePerMin);
			this.setState({
				costPerMin: event.target.value,
				pricePerMin: parseFloat(pricePerMin).toFixed(2)
			});
		}
	}

	changeMultiplier = (event) => {
		if (!isNaN(event.target.value)) {
			let multiplier = event.target.value;
			let pricePerMin = parseFloat(this.state.costPerMin) * parseFloat(multiplier);
			this.setState({
				multiplier: multiplier,
				pricePerMin: isNaN(pricePerMin) ? 0 : parseFloat(pricePerMin).toFixed(2)
			});
		}
	}

	changePricePerMin = (event) => {
		if (!isNaN(event.target.value) && !isNaN(this.state.costPerMin) && this.state.costPerMin != 0) {
			let multiplier = parseFloat(event.target.value) / parseFloat(this.state.costPerMin);
			this.setState({
				pricePerMin: event.target.value,
				multiplier: isNaN(multiplier) ? 0 : parseFloat(multiplier).toFixed(2)
			});
		}
	}

	onFileChange = (e) => {

		console.log("uploading file name is===", e.target.name);
		let fileName = e.target.name;
		let file;
		if (fileName == 'welcome') {
			file = this.refs.welcome.files[0];
		} else if (fileName == 'brandVoiceUrl') {
			file = this.refs.brandVoiceUrl.files[0];
		} else if (fileName == 'entryToneURL') {
			file = this.refs.entryToneURL.files[0];
		} else if (fileName == 'exitToneURL') {
			file = this.refs.exitToneURL.files[0];
		} else if (fileName == 'musicOnHoldURL') {
			file = this.refs.musicOnHoldURL.files[0];
		} else if (fileName == 'archiveStartURL') {
			file = this.refs.archiveStartURL.files[0];
		} else if (fileName == 'archiveEndURL') {
			file = this.refs.archiveEndURL.files[0];
		}

		if (file) {
			// console.log("uploaded local file = ", file);
			// For checking uploaded file extension -- 
			let blnValid = false;
			let sizeValid = false;
			//For checking  file size ---
			let sizeInByte = file.size;
			let sizeInKB = parseFloat(parseFloat(sizeInByte) / 1024).toFixed('2');
			if (sizeInKB <= 2000) {
				sizeValid = true;
			} else {
				sizeValid = false;
			}

			let sFileName = file.name;
			var _validFileExtensions = [".mp3", ".wav"];
			for (var j = 0; j < _validFileExtensions.length; j++) {
				var sCurExtension = _validFileExtensions[j];
				if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
					blnValid = true;
					break;
				}
			}

			if (!blnValid) {
				this.setState({
					showErrorAlert: true,
					errorAlertMessage: "File type should be .mp3"
				})
			} else if (!sizeValid) {
				this.setState({
					showErrorAlert: true,
					errorAlertMessage: "File size shouldn't be more than 2mb."
				});

			} else {
				this.setState({
					loading: true
				});
				var reader = new FileReader();
				var url = reader.readAsDataURL(file);
				// console.log("uploading file details == ", url);
				// reader.onloadend = function (e) {
				//     this.setState({
				//         voiceUrl: [reader.result]
				//     })
				// }.bind(this);

				const data = new FormData();
				// data.append('file', this.refs.file.files[0]);
				data.append('file', file);
				// data.append('companyId', this.state.companyId);

				// console.log("uploading payload for file == "+ data);

				axios
					.post(path + "services/upload-custom-voice",
						data,
						{
							headers: { 'Content-Type': 'multipart/form-data' },
							onUploadProgress: (progressEvent) => {
								// console.log("raw upload loader ---------", progressEvent.loaded, progressEvent.total);

								let uploadPercentage = parseInt(Math.round((progressEvent.loaded / progressEvent.total) * 100));
								if (fileName == 'welcome') {
									this.setState({ fileUploaded1: uploadPercentage });
								} else if (fileName == 'brandVoiceUrl') {
									this.setState({ fileUploaded1: uploadPercentage });
								} else if (fileName == 'entryToneURL') {
									this.setState({ fileUploaded1: uploadPercentage });
								} else if (fileName == 'exitToneURL') {
									this.setState({ fileUploaded1: uploadPercentage });
								} else if (fileName == 'musicOnHoldURL') {
									this.setState({ fileUploaded1: uploadPercentage });
								} else if (fileName == 'archiveStartURL') {
									this.setState({ fileUploaded1: uploadPercentage });
								} else if (fileName == 'archiveEndURL') {
									this.setState({ fileUploaded1: uploadPercentage });
								}

							}

						}
					)
					.then(serverResponse => {

						let res = serverResponse.data;
						if (!res.isError) {
							// console.log("upload response ===", res);
							let voiceUrl = '';
							// let brandVoiceUrl = '';

							if (fileName == 'welcome') {
								voiceUrl = res.detail && res.detail.url;

								this.setState({
									voiceUrl: voiceUrl,
									showSuccessAlert: true,
									successAlertMessage: 'Successfully uploaded.',
									fileUploaded1: '',
									fileUploaded2: '',
									loading: false
								}, () => {
									// console.log("after file upload....", this.state.brandVoiceUrl);
								})

							} else if (fileName == 'entryToneURL' || fileName == 'exitToneURL' || fileName == 'musicOnHoldURL' || fileName == 'archiveStartURL' || fileName == 'archiveEndURL' || fileName == 'brandVoiceUrl') {
								let globalFieldValue = res.detail && res.detail.url;
								// console.log("response came after uploading -- url is = ", globalFieldValue);
								this.setState({
									globalFieldValue,
									showSuccessAlert: true,
									successAlertMessage: 'Successfully uploaded.',
									fileUploaded1: '',
									fileUploaded2: '',
									loading: false
								}, () => {
									// console.log("after file upload....", this.state.globalFieldValue);
								})
							}


						} else {
							this.setState({
								showErrorAlert: true,
								errorAlertMessage: 'Sorry! something wrong...',
								fileUploaded1: '',
								fileUploaded2: '',
								loading: false
							})
						}

					})
					.catch(error => {
						console.log(error);
					})

			}

		}


	}

	// For handling and uploading Number list in CSV / START--
	handleReadCSV = (data) => {
		console.log("csv data=", data);
		if (data && data.errors && data.errors.length < 1) {
			let fileName = this.fileInput.current.files[0].name;
			let allContacts = [];
			let rawData = data.data;
			console.log('data==', data);
			rawData && Object.keys(rawData).map(index => {
				if (index > 0 && rawData[index].length == 6) {
					let rowArray = rawData[index];
					let newObject = {};
					rowArray && Object.keys(rowArray).map(i => {
						// if (rowArray[i]) {
						newObject[rawData[0][i]] = rowArray[i];
						// }
					});
					allContacts.push(newObject)
				}
			});
			console.log('allContacts==', allContacts);
			this.setState({ data: allContacts, fileName, showImportAddress: true });
		} else {
			this.setState({ importDataError: 'Please import a valid CSV.', showImportAddress: false });
		}

	}

	handleOnError = (err, file, inputElem, reason) => {

		console.log('err==', err, reason);
		this.setState({ importDataError: reason });

	}

	handleImportOffer = () => {

		this.setState({ importDataError: null });
		this.fileInput.current.click();

	}

	importDataChange = (index, value, name) => {
		let { data } = this.state;

		data[index][name] = value;
		// console.log("Price is=", data[index][name]);
		this.setState({ data });

	}

	hideImportAddress = () => {
		this.setState({
			loading: false,
			showImportAddress: false,
			data: [],
			fileName: ''
		});
	}

	saveContacts = () => {

		let { data, userId } = this.state;
		let that = this;
		let isValid = true;
		if (data && data.length > 0) {
			let details = [];
			data.map((contact, index) => {
				if (contact.countryCode && contact.countryCode.slice(0, 1) == '+') {
					contact.countryCode = contact.countryCode.slice(1);
				}
				// console.log("yes.. data exist---");
				if (contact.countryCode && !isNaN(contact.countryCode) && contact.phoneNumber && !isNaN(contact.phoneNumber) && contact.costPerMinute && !isNaN(contact.costPerMinute)) {
					// console.log("condition fullfilled");

					let detail = {};
					detail['countryCode'] = contact.countryCode;
					detail['phoneNumber'] = contact.phoneNumber;
					detail['phone'] = contact.countryCode + contact.phoneNumber;
					detail['sipProvider'] = contact.sipProvider;
					detail['country'] = contact.country;
					detail['numberType'] = contact.numberType;
					detail['costPerMinute'] = contact.costPerMinute;

					details.push(detail);
				} else {
					isValid = false;
				}
			})

			if (isValid) {
				this.setState({
					loading: true
				});
				let dataToSend = {
					"inboundNumbers": details
				};
				console.log("imported datas ==", dataToSend);
				axios
					.post(path + "services/insert-inbound-numbers", dataToSend, {
						headers: { "Content-Type": "application/json" }
					})
					.then(serverResponse => {
						console.log("Response from here : ", serverResponse.data);
						const res = serverResponse.data;
						if (!res.isError) {
							// alert('Successfully save');
							this.setState({
								loading: false,
								showSuccessAlert: true,
								successAlertMessage: 'Successfully uploaded.',

							}, () => {
								this.hideImportAddress();
								this.getNumberListForAdmin();
							});

						} else {
							this.setState({
								loading: false,
								showErrorAlert: true,
								errorAlertMessage: 'Sorry! somethingis wrong.',
							}, () => {
								this.hideImportAddress();
							});
							// alert('Server error data not save.');
						}
					}).catch(error => {
						console.log(error);
					});

			} else {
				// alert('Data not valid');
			}
		} else {
			this.setState({
				showErrorAlert: true,
				errorAlertMessage: 'Sorry! Data not found.',
			}, () => {
				this.hideImportAddress();
			});
		}

	}

	numberConfigEdit = () => {

		this.props.history.push({
			pathname: '/numberconfigue'
		});

	}
	// For handling and uploading Number list in CSV / END--


	// For handling and uploading country code list CSV / START --
	handleCCReadCSV = (data) => {
		console.log("1111// data =", data);

		// if (data && data.errors && data.errors.length < 1) {
		if (data && data.data && data.data.length > 0) {
			console.log("2222");
			let fileCCName = this.fileCCInput.current.files[0].name;
			let allContacts = [];
			let rawData = data.data;
			console.log('data==', data);
			rawData && Object.keys(rawData).map(index => {
				if (index > 0 && rawData[index].length == 2) {
					let rowArray = rawData[index];
					let newObject = {};
					rowArray && Object.keys(rowArray).map(i => {
						// if (rowArray[i]) {
						newObject[rawData[0][i]] = rowArray[i];
						// }
					});
					allContacts.push(newObject)
				}
			});
			console.log('allContacts==', allContacts);
			this.setState({ codeData: allContacts, fileCCName, showImportCode: true });
		} else {
			this.setState({ importCCDataError: 'Please import a valid CSV.', showImportCode: false });
		}

	}

	handleCCOnError = (err, file, inputElem, reason) => {

		console.log('err==', err, reason);
		this.setState({ importCCDataError: reason });

	}

	handleCCImportOffer = () => {

		this.setState({ importCCDataError: null });
		this.fileCCInput.current.click();

	}

	importCCDataChange = (index, value, name) => {

		let { codeData } = this.state;

		codeData[index][name] = value;
		this.setState({ codeData });

	}

	saveCountryCode = () => {

		let { codeData, userId } = this.state;
		// let that = this;
		let isValid = true;
		let details = [];
		if (codeData && codeData.length > 0) {

			codeData.map((contact, index) => {
				if (contact.countryCode && contact.countryCode.slice(0, 1) == '+') {
					contact.countryCode = contact.countryCode.slice(1);
				}
				// console.log("yes.. data exist---");
				if (contact.countryCode && !isNaN(contact.countryCode) && contact.countryName) {
					// console.log("condition fullfilled");

					let detail = {};
					detail['name'] = contact.countryName;
					detail['code'] = contact.countryCode;
					detail['flag'] = "";
					detail['isActive'] = false;

					details.push(detail);
				} else {
					isValid = false;
				}
			})
			console.log("Uploading details == ", details);

			if (isValid) {
				this.setState({ loading: true });
				let dataToSend = {
					"countryCodes": details
				};
				// API --
				axios
					.post(path + 'services/set-country-codes', dataToSend)
					.then(serverResponse => {

						let res = serverResponse.data;
						if (!res.isError) {
							console.log(res);
							this.setState({
								loading: false,
								showSuccessAlert: true,
								successAlertMessage: 'Successfully submitted'
							}, () => {
								this.getCountryCodeListForAdmin();
								this.hideImportCode();
							});
						} else {
							this.setState({
								loading: false,
								showErrorAlert: true,
								errorAlertMessage: 'Sorry! Something is wrong.',
							}, () => {
								this.hideImportCode()
							});
						}

					})
					.catch(error => {
						console.log(error);
					})

			}

		}

	}

	hideImportCode = () => {
		this.setState({
			loading: false,
			showImportCode: false,
			codeData: [],
			fileCCName: '',
			importCCDataError: ''
		});
	}

	countryCodeConfig = () => {

		this.props.history.push({
			pathname: '/countrycodeconfigue'
		});

	}
	// For handling and uploading country code list CSV / END --


	// For handling and uploading Out Bound list CSV / START --
	handleOBReadCSV = (data) => {

		// if (data && data.errors && data.errors.length < 1) {
		if (data && data.data && data.data.length > 0) {
			// console.log("2222");
			let fileOBName = this.fileOBInput.current.files[0].name;
			let allContacts = [];
			let rawData = data.data;
			console.log('data==', data);
			rawData && Object.keys(rawData).map(index => {
				if (index > 0 && rawData[index].length == 6) {
					let rowArray = rawData[index];
					let newObject = {};
					rowArray && Object.keys(rowArray).map(i => {
						// if (rowArray[i]) {
						newObject[rawData[0][i]] = rowArray[i];
						// }
					});
					allContacts.push(newObject)
				}
			});
			console.log('allContacts==', allContacts);
			this.setState({ outBoundData: allContacts, fileOBName, showImportOB: true });
		} else {
			this.setState({ importOBDataError: 'Please import a valid CSV.', showImportOB: false });
		}

	}

	handleOBOnError = (err, file, inputElem, reason) => {

		console.log('err==', err, reason);
		this.setState({ importOBDataError: reason });

	}

	handleOBImportOffer = () => {

		this.setState({ importOBDataError: null });
		this.fileOBInput.current.click();

	}

	importOBDataChange = (index, value, name) => {

		let { outBoundData } = this.state;

		outBoundData[index][name] = value;

		if (name == "costPerMinute") {

			outBoundData[index][name] = value;
			if (value && !isNaN(value)) {
				let newValue_1 = parseFloat(parseFloat(outBoundData[index]["multiplier"]) * parseFloat(outBoundData[index][name])).toFixed('2');
				outBoundData[index]["pricePerMinute"] = newValue_1 && !isNaN(newValue_1) ? newValue_1 : '';

				let newValue_2 = parseFloat(parseFloat(outBoundData[index]["pricePerMinute"]) / parseFloat(outBoundData[index][name])).toFixed('2');
				outBoundData[index]["multiplier"] = newValue_2 && !isNaN(newValue_2) ? newValue_2 : '';
			} else {
				outBoundData[index]["pricePerMinute"] = '';
				outBoundData[index]["multiplier"] = '';
			}

		}

		if (name == "multiplier") {

			outBoundData[index][name] = value;
			if (value && !isNaN(value)) {
				let newValue = parseFloat(parseFloat(outBoundData[index]["costPerMinute"]) * parseFloat(outBoundData[index][name])).toFixed('2');
				outBoundData[index]["pricePerMinute"] = newValue && !isNaN(newValue) ? newValue : '';
			} else {
				outBoundData[index]["pricePerMinute"] = '';
			}
		}

		if (name == "pricePerMinute") {

			outBoundData[index][name] = value;
			if (value && !isNaN(value)) {
				let newValue = parseFloat(parseFloat(outBoundData[index][name]) / parseFloat(outBoundData[index]["costPerMinute"])).toFixed('2');
				outBoundData[index]["multiplier"] = newValue && !isNaN(newValue) ? newValue : '';
			} else {
				outBoundData[index]["multiplier"] = '';
			}
		}

		this.setState({ outBoundData });

	}

	saveOutBound = () => {

		let { outBoundData, userId } = this.state;
		// let that = this;
		let isValid = true;
		let details = [];
		if (outBoundData && outBoundData.length > 0) {

			outBoundData.map((contact, index) => {
				if (contact.dialCode && contact.dialCode.slice(0, 1) == '+') {
					contact.dialCode = contact.dialCode.slice(1);
				}

				if (contact.dialCode && !isNaN(contact.dialCode) && contact.region && contact.costPerMinute && contact.multiplier && contact.pricePerMinute && contact.provider
					&& !isNaN(contact.costPerMinute) && !isNaN(contact.multiplier) && !isNaN(contact.pricePerMinute)) {

					let detail = {};
					detail['region'] = contact.region;
					detail['dialCode'] = contact.dialCode;
					detail['provider'] = contact.provider;
					detail['costPerMinute'] = contact.costPerMinute;
					detail['pricePerMinute'] = contact.pricePerMinute;
					detail['multiplier'] = contact.multiplier;

					details.push(detail);
				} else {
					isValid = false;
				}
			})
			console.log("Uploading details == ", details);

			if (isValid) {
				this.setState({ loading: true });
				let dataToSend = {
					"outboundPriceFactor": details
				};
				// API --
				axios
					.post(path + 'services/insert-sip-out-bound-price-factor', dataToSend)
					.then(serverResponse => {

						let res = serverResponse.data;
						if (!res.isError) {
							console.log(res);
							this.setState({
								loading: false,
								showSuccessAlert: true,
								successAlertMessage: 'Successfully submitted'
							}, () => {
								this.getOutBoundListForAdmin();
								this.hideImportOB();
							});
						} else {
							this.setState({
								loading: false,
								showErrorAlert: true,
								errorAlertMessage: 'Sorry! Something is wrong.',
							}, () => {
								this.hideImportOB()
							});
						}

					})
					.catch(error => {
						console.log(error);
					})

			}

		}

	}

	hideImportOB = () => {
		this.setState({
			loading: false,
			showImportOB: false,
			outBoundData: '',
			fileOBName: '',
			importOBDataError: ''
		});
	}

	outBoundConfig = () => {

		this.props.history.push({
			pathname: '/outboundconfigue'
		});

	}

	// For handling and uploading Out Bound list CSV / END --
	getGlobalData() {
		this.setState({ globalLoading: true })
		axios
			.post(path + 'services/get-global-data', {})
			.then(serverResponse => {
				let res = serverResponse.data;
				if (!res.isError) {
					// console.log("Inbound Number Lists===", res);
					let details = res.details;
					this.setState({
						globalLoading: false,
						entryToneURL: details.entryToneURL,
						exitToneURL: details.exitToneURL,
						musicOnHoldURL: details.musicOnHoldURL,
						archiveStartURL: details.archiveStartURL,
						archiveEndURL: details.archiveEndURL,
						brandVoiceUrl: details.brandVoiceUrl,
						archiveLife: details.archiveLife,
						vatNumber: details.vatNumber,
					});
				} else {
					this.setState({
						globalLoading: false
					})
				}
			})
			.catch(error => {
				this.setState({
					globalLoading: false
				})
				console.log(error);
			})
	}

	setGlobalData() {
		let that = this;
		let dataToSend = {};
		if (this.state.globalFieldValue) {
			this.setState({
				loading: true
			});
			dataToSend[this.state.globalFieldName] = this.state.globalFieldValue;

			// API - 
			axios
				.post(path + 'services/set-global-data', dataToSend)
				.then(serverResponse => {
					let res = serverResponse.data;
					if (!res.isError) {
						// console.log('After add a service - ', res);
						that.setState({
							loading: false
						}, () => {
							that.clearState();
							that.getGlobalData();
						})
					} else {
						alert("Sorry! Try again.");
						that.setState({
							loading: false
						}, () => {
							that.clearState();
						});
					}
				})
				.catch(error => {
					console.log(error);
				})
		}
	}

	showGlobalConfigModal(globalFieldName, globalFieldEditType) {
		this.setState({ globalFieldName, globalFieldEditType, showGlobalConfig: true, globalFieldValue: this.state[globalFieldName] });
	}

	clearState = () => {
		this.setState({
			loading: false,

			serviceName: '',
			costPerMin: '',
			multiplier: '',
			pricePerMin: '',
			selectedService: '',
			serviceCreatedBy: '',

			editService: false,
			addService: false,
			addOthService: false,

			companyType: '',
			noOfSeats: '0',
			pricePerSeat: '0.00',
			trialPeriod: '0',
			currency: '',
			defaultVat: '',
			vat: '',
			invoiceBillingInfo: '',
			voiceUrl: '',
			brandVoiceUrl: '',

			editServiceConfig: false,

			serviceNameError: '',
			costPerMinError: '',
			multiplierError: '',
			pricePerMinError: '',

			showSuccessAlert: false,
			successAlertMessage: '',
			showErrorAlert: false,
			errorAlertMessage: '',

			showImportAddress: false,

			globalFieldName: '',
			globalFieldValue: '',
			globalFieldEditType: '',
			showGlobalConfig: false,

		});
	}

	hideUploadAlert() {
		this.setState({
			showErrorAlert: false,
			errorAlertMessage: '',
			showSuccessAlert: false,
			successAlertMessage: ''
		})
	}

	sideMenuToggle() {
		let { openSideMenu } = this.state;
		openSideMenu = !openSideMenu;
		this.setState({ openSideMenu });
	}

	mobMenuClick() {
		this.sideMenuToggle();
		console.log('mobMenuClick');
	}

	parseJwt = (token) => {
		if (!token) { return; }
		const base64Url = token.split('.')[1];
		const base64 = base64Url.replace('-', '+').replace('_', '/');
		return JSON.parse(window.atob(base64));
	}


	render() {
		let { loading, globalLoading, serviceLoading, inboundLoading, outboundLoading, countrycodeLoading,
			serviceName, costPerMin, multiplier, pricePerMin, services, services1, companyType, noOfSeats,
			pricePerSeat, trialPeriod, currency, vat, voiceUrl, brandVoiceUrl, invoiceBillingInfo, data, codeData, outBoundData,
			importDataError, importCCDataError, importOBDataError, entryToneURL,
			exitToneURL,
			musicOnHoldURL,
			archiveStartURL,
			archiveEndURL,
			archiveLife,
			vatNumber,
		} = this.state;
		let companyTypeArray = [cred.COMPANY_TYPE_FREE_TYPE, cred.COMPANY_TYPE_STARTUP, cred.COMPANY_TYPE_ENTERPRISE];

		return (
			<main>

				<section className="user-mngnt-wrap">
					<div className="container-fluid">
						<div className="row">
							<Navbar routTo="/servicemanagement" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />

							<div className="col-xl-10 col-lg-9 p-0">

								<div className="mobile-menu-header">
									<a href="JavaScript:Void(0);" className="menu-tgl" onClick={() => this.sideMenuToggle()}>
										<img src="images/menu-tgl.png" alt="" />
									</a>
								</div>

								<Accordion>

									<Card>
										<Card.Header style={{ backgroundColor: 'white' }}>
											{!serviceLoading ? <Accordion.Toggle as={Button} variant="link" eventKey="0">
												Basic Services <i className="fa fa-angle-double-down" style={{ fontSize: "16px", color: 'white' }}></i>
											</Accordion.Toggle>
												: null}
											{serviceLoading ? <Accordion.Toggle as={Button} variant="link" eventKey="0">
												Basic Services <i className="fa fa-spinner fa-spin" style={{ fontSize: "16px", color: 'white' }}></i>
											</Accordion.Toggle>
												: null}
										</Card.Header>
										<Accordion.Collapse eventKey="0">
											<Card.Body style={{ backgroundColor: 'whitesmoke' }}>
												<div className="mng-full-list">
													{services && services.length > 0 && services.length < 4 ?
														<div className="row" style={{ padding: '5px', margin: '10px 0px' }}>
															<div className="col-md-9 border-rt">
																<p style={{ fontSize: '15px' }}><i>You have to add default <b>Cost Per Menute,</b> <b>Multiplayer</b> and <b>Price Per Minute</b> for services <b>(Web call, Inbound, Outbound and Archive)</b></i></p>
															</div>
															<div className="col-md-3">
																<a
																	href="JavaScript:Void(0);"
																	className="btn border"
																	data-tip='Add Service'
																	onClick={(event) => { this.addServiceModal(event) }}
																	style={{ width: '100%' }}
																>
																	Add Services
																</a>
															</div>
														</div>

														: null
													}

													{services && services.length > 0 ?
														<div className="mng-full-table">
															<div className="row">
																<div className="col-md-10">
																	<div className="mng-full-table-hdr">
																		<div className="row">
																			<div className="col-md-3 border-rt">
																				<h6>Service Name</h6>
																			</div>
																			<div className="col-md-3 border-rt">
																				<h6>Cost Per Minute</h6>
																			</div>
																			<div className="col-md-3 border-rt">
																				<h6>Price Per Minute</h6>
																			</div>
																			<div className="col-md-3">
																				<h6>Multiplier</h6>
																			</div>
																		</div>
																	</div>
																</div>

																<div className="col-md-2">
																	<div className="mng-full-table-hdr">
																		<div className="row">
																			<div className="col-md-12">
																				<h6>Edit</h6>
																			</div>
																		</div>
																	</div>
																</div>
															</div>

															{services && services.length > 0 && services.map((value, index) => {

																return <div className="row" key={index}>
																	<div className="col-md-10">

																		<div className="mng-full-table-row">
																			<div className="row">
																				<div className="col-md-3 border-rt">
																					<h6>Service Name</h6>
																					<p>
																						<a href="JavaScript:Void(0);" onClick={() => { this.serviceConfiguration(index) }}>
																							{value.serviceName}
																						</a>
																					</p>
																				</div>
																				<div className="col-md-3 border-rt">
																					<h6>Cost Per Minute</h6>
																					<p>{value.costPerMinute}</p>
																				</div>
																				<div className="col-md-3 border-rt">
																					<h6>Price Per Minute</h6>
																					<p>{value.pricePerMinute}</p>
																				</div>
																				<div className="col-md-3">
																					<h6>Multiplier</h6>
																					<p>{value.multiplier}</p>
																				</div>
																				<div className="mobile-ad-edt-btns">
																					<ul>
																						<li>
																							<a href="JavaScript:Void(0);"
																								onClick={() => { this.serviceConfiguration(index) }}
																							>
																								<i className="fas fa-cog"></i>
																							</a>
																						</li>
																						{/* <li>
																							<a href="JavaScript:Void(0);"
																								onClick={() => { this.deleteConfirm(index) }}
																							>
																								<i className="fas fa-trash-alt"></i>
																							</a>
																						</li> */}

																					</ul>
																				</div>
																			</div>
																		</div>

																	</div>

																	<div className="col-md-2">
																		<div className="mng-full-table-row add-edt text-center">
																			<div className="row">
																				<div className="col-md-12">
																					<a href="JavaScript:Void(0);"
																						// ref={ref => this.fooRef = ref}
																						data-tip='Configuration'
																						onClick={() => { this.serviceConfiguration(index); }}
																					// onFocus={() => { ReactTooltip.show(this.fooRef) }}
																					>
																						<i className="fas fa-cog"></i>
																					</a>
																					<ReactTooltip
																						globalEventOff="click"
																						effect="float"
																						place="top"
																						data-border="true"
																					/>
																				</div>

																				{/* <div className="col-md-6">

																					<a href="JavaScript:Void(0);"
																						// ref={ref => this.fooRef = ref}
																						data-tip='Delete'
																						onClick={() => { this.deleteConfirm(index); }}
																					// onFocus={() => { ReactTooltip.show(this.fooRef) }}
																					>
																						<i className="fas fa-trash-alt"></i>
																					</a>
																					<ReactTooltip
																						globalEventOff="click"
																						effect="float"
																						place="top"
																						data-border="true"
																					/>
																				</div> */}
																			</div>
																		</div>
																	</div>
																</div>
															})}
														</div>
														:
														this.state.serviceLoading ?
															<i><h4>Loading...</h4></i>
															: <div className="row" style={{ padding: '5px', margin: '10px 0px' }}>
																<div className="col-md-9 border-rt">
																	<p style={{ fontSize: '15px' }}><i>You have to add default <b>Cost Per Menute,</b> <b>Multiplayer</b> and <b>Price Per Minute</b> for services <b>(Web call, Inbound, Outbound and Archive)</b></i></p>
																</div>
																<div className="col-md-3">
																	<a
																		href="JavaScript:Void(0);"
																		className="btn border"
																		data-tip='Add Service'
																		onClick={() => { this.addServiceFirst() }}
																		style={{ width: '100%' }}
																	>
																		Add Services
                                  									</a>
																</div>
															</div>

													}
												</div>
											</Card.Body>
										</Accordion.Collapse>
									</Card>

									<Card>
										<Card.Header style={{ backgroundColor: 'white' }}>
											{!serviceLoading ? <Accordion.Toggle as={Button} variant="link" eventKey="1">
												Company Wise Settings <i className="fa fa-angle-double-down" style={{ fontSize: "16px", color: 'white' }}></i>
											</Accordion.Toggle>
												: null}
											{serviceLoading ? <Accordion.Toggle as={Button} variant="link" eventKey="1">
												Company Wise Settings <i className="fa fa-spinner fa-spin" style={{ fontSize: "16px", color: 'white' }}></i>
											</Accordion.Toggle>
												: null}
										</Card.Header>
										<Accordion.Collapse eventKey="1">
											<Card.Body style={{ backgroundColor: 'whitesmoke' }}>
												<div className="mng-full-list">

													{services1 && services1.length > 0 && services1.length < 3 ?
														<div className="row" style={{ padding: '5px', margin: '10px 0px' }}>
															<div className="col-md-9 border-rt">
																<p style={{ fontSize: '15px' }}><i>You have to add default configuration for different company type <b>(FREE, STARTUP and ENTERPRISE)</b></i></p>
															</div>
															<div className="col-md-3">
																<a
																	href="JavaScript:Void(0);"
																	className="btn border"
																	data-tip='Add Service'
																	onClick={(event) => { this.addOthServiceModal(event) }}
																	style={{ width: '100%' }}
																>
																	Add Services
																</a>
															</div>
														</div>

														: null
													}
													{services1 && services1.length > 0 ?
														<div className="mng-full-table">
															<div className="row">
																<div className="col-md-10">
																	<div className="mng-full-table-hdr">
																		<div className="row">
																			<div className="col-md-3 border-rt">
																				<h6>Company Type</h6>
																			</div>
																			<div className="col-md-3 border-rt">
																				<h6>No of Seat</h6>
																			</div>
																			<div className="col-md-3 border-rt">
																				<h6>Price Per Seat</h6>
																			</div>
																			<div className="col-md-3">
																				<h6>Currency</h6>
																			</div>
																		</div>
																	</div>
																</div>

																<div className="col-md-2">
																	<div className="mng-full-table-hdr">
																		<div className="row">
																			<div className="col-md-12 ">
																				<h6>Edit</h6>
																			</div>
																			{/* <div className="col-md-6">
																				<h6>Delete</h6>
																			</div> */}
																		</div>
																	</div>
																</div>
															</div>

															{services1 && services1.length > 0 && services1.map((value, index) => {

																return <div className="row" key={index}>
																	<div className="col-md-10">

																		<div className="mng-full-table-row">
																			<div className="row">
																				<div className="col-md-3 border-rt">
																					<h6>Company Type</h6>
																					<p>
																						<a href="JavaScript:Void(0);" onClick={() => { this.serviceConfigEdit(index) }}>
																							{value.companyType}
																						</a>
																					</p>
																				</div>
																				<div className="col-md-3 border-rt">
																					<h6>No of Seat</h6>
																					<p>{value.numberOfSeats}</p>
																				</div>
																				<div className="col-md-3 border-rt">
																					<h6>Price Per Seat</h6>
																					<p>{value.pricePerSeat}</p>
																				</div>
																				<div className="col-md-3">
																					<h6>Currency</h6>
																					<p>{value.defaultCurrency}</p>
																				</div>
																				<div className="mobile-ad-edt-btns">
																					<ul>
																						<li>
																							<a href="JavaScript:Void(0);"
																								onClick={() => { this.serviceConfigEdit(index) }}
																							>
																								<i className="fas fa-cog"></i>
																							</a>
																						</li>
																						{/* <li>
																							<a href="JavaScript:Void(0);"
																								onClick={() => { this.deleteserviceConfigConfirm(index) }}
																							>
																								<i className="fas fa-trash-alt"></i>
																							</a>
																						</li> */}

																					</ul>
																				</div>
																			</div>
																		</div>

																	</div>

																	<div className="col-md-2">
																		<div className="mng-full-table-row add-edt text-center">
																			<div className="row">
																				<div className="col-md-12">
																					<a href="JavaScript:Void(0);"
																						// ref={ref => this.fooRef = ref}
																						data-tip='Configuration'
																						onClick={() => { this.serviceConfigEdit(index); }}
																					// onFocus={() => { ReactTooltip.show(this.fooRef) }}
																					>
																						<i className="fas fa-cog"></i>
																					</a>
																					<ReactTooltip
																						globalEventOff="click"
																						effect="float"
																						place="top"
																						data-border="true"
																					/>
																				</div>
																				{/* <div className="col-md-6">
																					<a href="JavaScript:Void(0);"
																						// ref={ref => this.fooRef = ref}
																						data-tip='Delete'
																						onClick={() => { this.deleteserviceConfigConfirm(index); }}
																					// onFocus={() => { ReactTooltip.show(this.fooRef) }}
																					>
																						<i className="fas fa-trash-alt"></i>
																					</a>
																					<ReactTooltip
																						globalEventOff="click"
																						effect="float"
																						place="top"
																						data-border="true"
																					/>
																				</div> */}
																			</div>
																		</div>
																	</div>
																</div>
															})}
														</div>
														:
														this.state.serviceLoading ?
															<i><h4>Loading...</h4></i>
															:
															<div className="row" style={{ padding: '5px', margin: '10px 0px' }}>
																<div className="col-md-9 border-rt">
																	<p style={{ fontSize: '15px' }}><i>You have to add default configuration for different company type <b>(FREE, STARTUP and ENTERPRISE)</b></i></p>
																</div>
																<div className="col-md-3">
																	<a
																		href="JavaScript:Void(0);"
																		className="btn border"
																		data-tip='Add Service'
																		onClick={(event) => { this.addOthServiceFirst() }}
																		style={{ width: '100%' }}
																	>
																		Add Services
																</a>
																</div>
															</div>

													}

												</div>
											</Card.Body>
										</Accordion.Collapse>
									</Card>

									<Card>
										<Card.Header style={{ backgroundColor: 'white' }}>
											{!inboundLoading ? <Accordion.Toggle as={Button} variant="link" eventKey="2">
												Inbound Numbers <i className="fa fa-angle-double-down" style={{ fontSize: "16px", color: 'white' }}></i>
											</Accordion.Toggle>
												: null}
											{inboundLoading ? <Accordion.Toggle as={Button} variant="link" eventKey="2">
												Inbound Numbers <i className="fa fa-spinner fa-spin" style={{ fontSize: "16px", color: 'white' }}></i>
											</Accordion.Toggle>
												: null}
										</Card.Header>
										<Accordion.Collapse eventKey="2">
											<Card.Body style={{ backgroundColor: 'whitesmoke' }}>
												<div className="mng-full-list">
													<div className="mng-full-srch">
														<div className="row">

															<div className="col-md-6">
																<CSVReader
																	onFileLoaded={this.handleReadCSV}
																	inputRef={this.fileInput}
																	style={{ display: 'none' }}
																	onError={this.handleOnError}
																/>
																<button className="btn btn-primary" style={{ width: '100%' }} onClick={this.handleImportOffer}>IMPORT INBOUND NUMBER LIST </button>
																{importDataError && <span className="error-text">{importDataError}</span>}
															</div>

															<div className="col-md-6" style={{ textAlign: 'center' }}>
																{/* <a className="btn btn-primary" style={{ margin: 5, width: '100%' }} href="https://mioappimages.s3.us-east-2.amazonaws.com/Inbound_Numbers.csv">Download Sample</a> */}
																<a className="btn btn-primary" style={{ margin: 5, width: '100%' }} target="blank" href="/asset/Inbound_Numbers.csv">Download Sample</a>
															</div>

														</div>
													</div>

													<div className="mng-full-table">
														<div className="row">
															<div className="col-md-10">
																<div className="mng-full-table-hdr">
																	<div className="row">
																		<div className="col-md-6 border-rt">
																			<h6>Status</h6>
																		</div>
																		<div className="col-md-6">
																			<h6>Records</h6>
																		</div>
																	</div>
																</div>
															</div>

															<div className="col-md-2">
																<div className="mng-full-table-hdr">
																	<div className="row">
																		<div className="col-md-12">
																			<h6>Edit</h6>
																		</div>
																	</div>
																</div>
															</div>
														</div>

														<div className="row">

															<div className="col-md-10">
																<div className="mng-full-table-row">
																	<div className="row">
																		<div className="col-md-6 border-rt">
																			<h6>Status</h6>
																			<p>{this.state.numberListRecords ? "Uploaded" : "Not uploaded"}</p>
																		</div>
																		<div className="col-md-6">
																			<h6>Records</h6>
																			<p>{this.state.numberListRecords}</p>
																		</div>
																		<div className="mobile-ad-edt-btns">
																			<ul>
																				<li>
																					<a href="JavaScript:Void(0);"
																						onClick={() => { this.numberConfigEdit() }}
																					>
																						<i className="fas fa-cog"></i>
																					</a>
																				</li>
																			</ul>
																		</div>
																	</div>
																</div>

															</div>

															<div className="col-md-2">
																<div className="mng-full-table-row add-edt text-center">
																	<div className="row">
																		<div className="col-md-12">
																			<a href="JavaScript:Void(0);"
																				data-tip='Configuration'
																				onClick={() => { this.numberConfigEdit(); }}
																			>
																				<i className="fas fa-cog"></i>
																			</a>
																			<ReactTooltip
																				globalEventOff="click"
																				effect="float"
																				place="top"
																				data-border="true"
																			/>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</Card.Body>
										</Accordion.Collapse>
									</Card>

									<Card>
										<Card.Header style={{ backgroundColor: 'white' }}>
											{!outboundLoading ? <Accordion.Toggle as={Button} variant="link" eventKey="3">
												Outbound Numbers <i className="fa fa-angle-double-down" style={{ fontSize: "16px", color: 'white' }}></i>
											</Accordion.Toggle>
												: null}
											{outboundLoading ? <Accordion.Toggle as={Button} variant="link" eventKey="3">
												Outbound Numbers <i className="fa fa-spinner fa-spin" style={{ fontSize: "16px", color: 'white' }}></i>
											</Accordion.Toggle>
												: null}
										</Card.Header>
										<Accordion.Collapse eventKey="3">
											<Card.Body style={{ backgroundColor: 'whitesmoke' }}>
												<div className="mng-full-list">
													<div className="mng-full-srch">
														<div className="row">

															<div className="col-md-6">
																<CSVReader
																	onFileLoaded={this.handleOBReadCSV}
																	inputRef={this.fileOBInput}
																	style={{ display: 'none' }}
																	onError={this.handleOBOnError}
																/>
																<button className="btn btn-primary" style={{ width: '100%' }} onClick={this.handleOBImportOffer}>IMPORT OUTBOUND LIST </button>
																{importOBDataError && <span className="error-text">{importOBDataError}</span>}
															</div>

															<div className="col-md-6" style={{ textAlign: 'center' }}>
																{/* <a className="btn btn-primary" style={{ margin: 5, width: '100%' }} href="https://mioappimages.s3.us-east-2.amazonaws.com/outBoundCSV.csv">Download Sample</a> */}
																<a className="btn btn-primary" style={{ margin: 5, width: '100%' }} target="blank" href="/asset/outBoundCSV.csv">Download Sample</a>
															</div>

														</div>
													</div>

													<div className="mng-full-table">

														<div className="row">
															<div className="col-md-10">
																<div className="mng-full-table-hdr">
																	<div className="row">
																		<div className="col-md-6 border-rt">
																			<h6>Status</h6>
																		</div>
																		<div className="col-md-6">
																			<h6>Records</h6>
																		</div>
																	</div>
																</div>
															</div>

															<div className="col-md-2">
																<div className="mng-full-table-hdr">
																	<div className="row">
																		<div className="col-md-12">
																			<h6>Edit</h6>
																		</div>
																	</div>
																</div>
															</div>
														</div>

														<div className="row">

															<div className="col-md-10">
																<div className="mng-full-table-row">
																	<div className="row">
																		<div className="col-md-6 border-rt">
																			<h6>Status</h6>
																			<p>{this.state.outBoundRecords ? "Uploaded" : "Not uploaded"}</p>
																		</div>
																		<div className="col-md-6">
																			<h6>Records</h6>
																			<p>{this.state.outBoundRecords}</p>
																		</div>
																		<div className="mobile-ad-edt-btns">
																			<ul>
																				<li>
																					<a href="JavaScript:Void(0);"
																						onClick={() => { this.outBoundConfig() }}
																					>
																						<i className="fas fa-cog"></i>
																					</a>
																				</li>
																			</ul>
																		</div>
																	</div>
																</div>

															</div>

															<div className="col-md-2">
																<div className="mng-full-table-row add-edt text-center">
																	<div className="row">
																		<div className="col-md-12">
																			<a href="JavaScript:Void(0);"
																				data-tip='Configuration'
																				onClick={() => { this.outBoundConfig(); }}
																			>
																				<i className="fas fa-cog"></i>
																			</a>
																			<ReactTooltip
																				globalEventOff="click"
																				effect="float"
																				place="top"
																				data-border="true"
																			/>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</Card.Body>
										</Accordion.Collapse>
									</Card>

									<Card>
										<Card.Header style={{ backgroundColor: 'white' }}>
											{!countrycodeLoading ? <Accordion.Toggle as={Button} variant="link" eventKey="4">
												Country Codes <i className="fa fa-angle-double-down" style={{ fontSize: "16px", color: 'white' }}></i>
											</Accordion.Toggle>
												: null}
											{countrycodeLoading ? <Accordion.Toggle as={Button} variant="link" eventKey="4">
												Country Codes <i className="fa fa-spinner fa-spin" style={{ fontSize: "16px", color: 'white' }}></i>
											</Accordion.Toggle>
												: null}
										</Card.Header>
										<Accordion.Collapse eventKey="4">
											<Card.Body style={{ backgroundColor: 'whitesmoke' }}>
												<div className="mng-full-list">
													<div className="mng-full-srch">
														<div className="row">

															<div className="col-md-6">
																<CSVReader
																	onFileLoaded={this.handleCCReadCSV}
																	inputRef={this.fileCCInput}
																	style={{ display: 'none' }}
																	onError={this.handleCCOnError}
																/>
																<button className="btn btn-primary" style={{ width: '100%' }} onClick={this.handleCCImportOffer}>IMPORT COUNTRY CODE LIST </button>
																{importCCDataError && <span className="error-text">{importCCDataError}</span>}
															</div>

															<div className="col-md-6" style={{ textAlign: 'center' }}>
																<a className="btn btn-primary" style={{ margin: 5, width: '100%' }} href="https://mioappimages.s3.us-east-2.amazonaws.com/CountryCode.csv">Download Sample</a>
															</div>

														</div>
													</div>

													<div className="mng-full-table">

														<div className="row">
															<div className="col-md-10">
																<div className="mng-full-table-hdr">
																	<div className="row">
																		<div className="col-md-6 border-rt">
																			<h6>Status</h6>
																		</div>
																		<div className="col-md-6">
																			<h6>Records</h6>
																		</div>
																	</div>
																</div>
															</div>

															<div className="col-md-2">
																<div className="mng-full-table-hdr">
																	<div className="row">
																		<div className="col-md-12">
																			<h6>Edit</h6>
																		</div>
																	</div>
																</div>
															</div>
														</div>

														<div className="row">

															<div className="col-md-10">
																<div className="mng-full-table-row">
																	<div className="row">
																		<div className="col-md-6 border-rt">
																			<h6>Status</h6>
																			<p>{this.state.countryCodeRecords ? "Uploaded" : "Not uploaded"}</p>
																		</div>
																		<div className="col-md-6">
																			<h6>Records</h6>
																			<p>{this.state.countryCodeRecords && this.state.countryCodeRecords.length}</p>
																		</div>
																		<div className="mobile-ad-edt-btns">
																			<ul>
																				<li>
																					<a href="JavaScript:Void(0);"
																						onClick={() => { this.countryCodeConfig() }}
																					>
																						<i className="fas fa-cog"></i>
																					</a>
																				</li>
																			</ul>
																		</div>
																	</div>
																</div>

															</div>

															<div className="col-md-2">
																<div className="mng-full-table-row add-edt text-center">
																	<div className="row">
																		<div className="col-md-12">
																			<a href="JavaScript:Void(0);"
																				data-tip='Configuration'
																				onClick={() => { this.countryCodeConfig(); }}
																			>
																				<i className="fas fa-cog"></i>
																			</a>
																			<ReactTooltip
																				globalEventOff="click"
																				effect="float"
																				place="top"
																				data-border="true"
																			/>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</Card.Body>
										</Accordion.Collapse>
									</Card>

									<Card>
										<Card.Header style={{ backgroundColor: 'white' }}>
											{!globalLoading ? <Accordion.Toggle as={Button} variant="link" eventKey="5">
												Global Settings <i className="fa fa-angle-double-down" style={{ fontSize: "16px", color: 'white' }} ></i>
											</Accordion.Toggle>
												: null}
											{globalLoading ? <Accordion.Toggle as={Button} variant="link" eventKey="5">
												Global Settings <i className="fa fa-spinner fa-spin" style={{ fontSize: "16px", color: 'white' }}></i>
											</Accordion.Toggle>
												: null}
										</Card.Header>
										<Accordion.Collapse eventKey="5">
											<Card.Body style={{ backgroundColor: 'whitesmoke' }}>
												<div className="mng-full-list">
													<div className="mng-full-table">

														<div className="row">
															<div className="col-md-10">
																<div className="mng-full-table-hdr">
																	<div className="row">
																		<div className="col-md-4 border-rt">
																			<h6>Field</h6>
																		</div>
																		<div className="col-md-4 border-rt">
																			<h6>Purpose</h6>
																		</div>
																		<div className="col-md-4">
																			<h6>Value</h6>
																		</div>
																	</div>
																</div>
															</div>

															<div className="col-md-2">
																<div className="mng-full-table-hdr">
																	<div className="row">
																		<div className="col-md-12">
																			<h6>Update</h6>
																		</div>
																	</div>
																</div>
															</div>
														</div>

														<div className="row">
															<div className="col-md-10">
																<div className="mng-full-table-row">
																	<div className="row">
																		<div className="col-md-4 border-rt">
																			<h6>Field</h6>
																			<p>Entry Tone</p>
																		</div>
																		<div className="col-md-4 border-rt">
																			<h6>Purpose</h6>
																			<p>This will play when new participant enter into conference room</p>
																		</div>
																		<div className="col-md-4">
																			<h6>Value</h6>
																			{entryToneURL && <audio controls style={{ width: '100%', border: 'solid #0092DD 1px', borderRadius: '30px' }}>
																				<source src={entryToneURL} type="audio/mpeg" />
																				{entryToneURL}
																			</audio>
																			}
																		</div>
																		<div className="mobile-ad-edt-btns">
																			<ul>
																				<li>
																					<a href="JavaScript:Void(0);"
																						onClick={() => this.showGlobalConfigModal('entryToneURL', 'file')}
																					>
																						<i className="fas fa-cog"></i>
																					</a>
																				</li>
																			</ul>
																		</div>
																	</div>
																</div>

															</div>
															<div className="col-md-2">
																<div className="mng-full-table-row add-edt text-center">
																	<div className="row">
																		<div className="col-md-12">
																			<a href="JavaScript:Void(0);"
																				data-tip='Update'
																				onClick={() => this.showGlobalConfigModal('entryToneURL', 'file')}
																			>
																				<i className="fas fa-cog"></i>
																			</a>
																			<ReactTooltip
																				globalEventOff="click"
																				effect="float"
																				place="top"
																				data-border="true"
																			/>
																		</div>
																	</div>
																</div>
															</div>
														</div>

														<div className="row">
															<div className="col-md-10">
																<div className="mng-full-table-row">
																	<div className="row">
																		<div className="col-md-4 border-rt">
																			<h6>Field</h6>
																			<p>Exit Tone</p>
																		</div>
																		<div className="col-md-4 border-rt">
																			<h6>Purpose</h6>
																			<p>This will play when participant exit from conference room</p>
																		</div>
																		<div className="col-md-4">
																			<h6>Value</h6>
																			{exitToneURL && <audio controls style={{ width: '100%', border: 'solid #0092DD 1px', borderRadius: '30px' }}>
																				<source src={exitToneURL} type="audio/mpeg" />
																				{exitToneURL}
																			</audio>
																			}
																		</div>
																		<div className="mobile-ad-edt-btns">
																			<ul>
																				<li>
																					<a href="JavaScript:Void(0);"
																						onClick={() => this.showGlobalConfigModal('exitToneURL', 'file')}
																					>
																						<i className="fas fa-cog"></i>
																					</a>
																				</li>
																			</ul>
																		</div>
																	</div>
																</div>

															</div>
															<div className="col-md-2">
																<div className="mng-full-table-row add-edt text-center">
																	<div className="row">
																		<div className="col-md-12">
																			<a href="JavaScript:Void(0);"
																				data-tip='Update'
																				onClick={() => this.showGlobalConfigModal('exitToneURL', 'file')}
																			>
																				<i className="fas fa-cog"></i>
																			</a>
																			<ReactTooltip
																				globalEventOff="click"
																				effect="float"
																				place="top"
																				data-border="true"
																			/>
																		</div>
																	</div>
																</div>
															</div>
														</div>

														<div className="row">
															<div className="col-md-10">
																<div className="mng-full-table-row">
																	<div className="row">
																		<div className="col-md-4 border-rt">
																			<h6>Field</h6>
																			<p>Music on hold</p>
																		</div>
																		<div className="col-md-4 border-rt">
																			<h6>Purpose</h6>
																			<p>This will play when host will not join conference but participants joined</p>
																		</div>
																		<div className="col-md-4">
																			<h6>Value</h6>
																			{musicOnHoldURL && <audio controls style={{ width: '100%', border: 'solid #0092DD 1px', borderRadius: '30px' }}>
																				<source src={musicOnHoldURL} type="audio/mpeg" />
																				{musicOnHoldURL}
																			</audio>
																			}
																		</div>
																		<div className="mobile-ad-edt-btns">
																			<ul>
																				<li>
																					<a href="JavaScript:Void(0);"
																						onClick={() => this.showGlobalConfigModal('musicOnHoldURL', 'file')}
																					>
																						<i className="fas fa-cog"></i>
																					</a>
																				</li>
																			</ul>
																		</div>
																	</div>
																</div>

															</div>
															<div className="col-md-2">
																<div className="mng-full-table-row add-edt text-center">
																	<div className="row">
																		<div className="col-md-12">
																			<a href="JavaScript:Void(0);"
																				data-tip='Update'
																				onClick={() => this.showGlobalConfigModal('musicOnHoldURL', 'file')}
																			>
																				<i className="fas fa-cog"></i>
																			</a>
																			<ReactTooltip
																				globalEventOff="click"
																				effect="float"
																				place="top"
																				data-border="true"
																			/>
																		</div>
																	</div>
																</div>
															</div>
														</div>

														<div className="row">
															<div className="col-md-10">
																<div className="mng-full-table-row">
																	<div className="row">
																		<div className="col-md-4 border-rt">
																			<h6>Field</h6>
																			<p>Recording start</p>
																		</div>
																		<div className="col-md-4 border-rt">
																			<h6>Purpose</h6>
																			<p>This will play when start recording conference</p>
																		</div>
																		<div className="col-md-4">
																			<h6>Value</h6>
																			{archiveStartURL && <audio controls style={{ width: '100%', border: 'solid #0092DD 1px', borderRadius: '30px' }}>
																				<source src={archiveStartURL} type="audio/mpeg" />
																				{archiveStartURL}
																			</audio>
																			}
																		</div>
																		<div className="mobile-ad-edt-btns">
																			<ul>
																				<li>
																					<a href="JavaScript:Void(0);"
																						onClick={() => this.showGlobalConfigModal('archiveStartURL', 'file')}
																					>
																						<i className="fas fa-cog"></i>
																					</a>
																				</li>
																			</ul>
																		</div>
																	</div>
																</div>

															</div>
															<div className="col-md-2">
																<div className="mng-full-table-row add-edt text-center">
																	<div className="row">
																		<div className="col-md-12">
																			<a href="JavaScript:Void(0);"
																				data-tip='Update'
																				onClick={() => this.showGlobalConfigModal('archiveStartURL', 'file')}
																			>
																				<i className="fas fa-cog"></i>
																			</a>
																			<ReactTooltip
																				globalEventOff="click"
																				effect="float"
																				place="top"
																				data-border="true"
																			/>
																		</div>
																	</div>
																</div>
															</div>
														</div>

														<div className="row">
															<div className="col-md-10">
																<div className="mng-full-table-row">
																	<div className="row">
																		<div className="col-md-4 border-rt">
																			<h6>Field</h6>
																			<p>Recording Stop</p>
																		</div>
																		<div className="col-md-4 border-rt">
																			<h6>Purpose</h6>
																			<p>This will play when stop recording conference</p>
																		</div>
																		<div className="col-md-4">
																			<h6>Value</h6>
																			{archiveEndURL && <audio controls style={{ width: '100%', border: 'solid #0092DD 1px', borderRadius: '30px' }}>
																				<source src={archiveEndURL} type="audio/mpeg" />
																				{archiveEndURL}
																			</audio>
																			}
																		</div>
																		<div className="mobile-ad-edt-btns">
																			<ul>
																				<li>
																					<a href="JavaScript:Void(0);"
																						onClick={() => this.showGlobalConfigModal('archiveEndURL', 'file')}
																					>
																						<i className="fas fa-cog"></i>
																					</a>
																				</li>
																			</ul>
																		</div>
																	</div>
																</div>

															</div>
															<div className="col-md-2">
																<div className="mng-full-table-row add-edt text-center">
																	<div className="row">
																		<div className="col-md-12">
																			<a href="JavaScript:Void(0);"
																				data-tip='Update'
																				onClick={() => this.showGlobalConfigModal('archiveEndURL', 'file')}
																			>
																				<i className="fas fa-cog"></i>
																			</a>
																			<ReactTooltip
																				globalEventOff="click"
																				effect="float"
																				place="top"
																				data-border="true"
																			/>
																		</div>
																	</div>
																</div>
															</div>
														</div>

														<div className="row">
															<div className="col-md-10">
																<div className="mng-full-table-row">
																	<div className="row">
																		<div className="col-md-4 border-rt">
																			<h6>Field</h6>
																			<p>Branding Voice</p>
																		</div>
																		<div className="col-md-4 border-rt">
																			<h6>Purpose</h6>
																			<p>This will play as branding tune</p>
																		</div>
																		<div className="col-md-4">
																			<h6>Value</h6>
																			{brandVoiceUrl && <audio controls style={{ width: '100%', border: 'solid #0092DD 1px', borderRadius: '30px' }}>
																				<source src={brandVoiceUrl} type="audio/mpeg" />
																				{brandVoiceUrl}
																			</audio>
																			}
																		</div>
																		<div className="mobile-ad-edt-btns">
																			<ul>
																				<li>
																					<a href="JavaScript:Void(0);"
																						onClick={() => this.showGlobalConfigModal('brandVoiceUrl', 'file')}
																					>
																						<i className="fas fa-cog"></i>
																					</a>
																				</li>
																			</ul>
																		</div>
																	</div>
																</div>

															</div>
															<div className="col-md-2">
																<div className="mng-full-table-row add-edt text-center">
																	<div className="row">
																		<div className="col-md-12">
																			<a href="JavaScript:Void(0);"
																				data-tip='Update'
																				onClick={() => this.showGlobalConfigModal('brandVoiceUrl', 'file')}
																			>
																				<i className="fas fa-cog"></i>
																			</a>
																			<ReactTooltip
																				globalEventOff="click"
																				effect="float"
																				place="top"
																				data-border="true"
																			/>
																		</div>
																	</div>
																</div>
															</div>
														</div>



														<div className="row">
															<div className="col-md-10">
																<div className="mng-full-table-row">
																	<div className="row">
																		<div className="col-md-4 border-rt">
																			<h6>Field</h6>
																			<p>Recording Life</p>
																		</div>
																		<div className="col-md-4 border-rt">
																			<h6>Purpose</h6>
																			<p>Duration of life span for recording.</p>
																		</div>
																		<div className="col-md-4">
																			<h6>Value</h6>
																			{archiveLife && <p> {archiveLife}</p>
																			}
																		</div>
																		<div className="mobile-ad-edt-btns">
																			<ul>
																				<li>
																					<a href="JavaScript:Void(0);"
																						onClick={() => this.showGlobalConfigModal('archiveLife', 'text')}
																					>
																						<i className="fas fa-cog"></i>
																					</a>
																				</li>
																			</ul>
																		</div>
																	</div>
																</div>

															</div>
															<div className="col-md-2">
																<div className="mng-full-table-row add-edt text-center">
																	<div className="row">
																		<div className="col-md-12">
																			<a href="JavaScript:Void(0);"
																				data-tip='Update'
																				onClick={() => this.showGlobalConfigModal('archiveLife', 'text')}
																			>
																				<i className="fas fa-cog"></i>
																			</a>
																			<ReactTooltip
																				globalEventOff="click"
																				effect="float"
																				place="top"
																				data-border="true"
																			/>
																		</div>
																	</div>
																</div>
															</div>
														</div>

														<div className="row">
															<div className="col-md-10">
																<div className="mng-full-table-row">
																	<div className="row">
																		<div className="col-md-4 border-rt">
																			<h6>Field</h6>
																			<p>VAT Number</p>
																		</div>
																		<div className="col-md-4 border-rt">
																			<h6>Purpose</h6>
																			<p>VAT Number reflect in invoice.</p>
																		</div>
																		<div className="col-md-4">
																			<h6>Value</h6>
																			{vatNumber && <p> {vatNumber}</p>
																			}
																		</div>
																		<div className="mobile-ad-edt-btns">
																			<ul>
																				<li>
																					<a href="JavaScript:Void(0);"
																						onClick={() => this.showGlobalConfigModal('vatNumber', 'text')}
																					>
																						<i className="fas fa-cog"></i>
																					</a>
																				</li>
																			</ul>
																		</div>
																	</div>
																</div>

															</div>
															<div className="col-md-2">
																<div className="mng-full-table-row add-edt text-center">
																	<div className="row">
																		<div className="col-md-12">
																			<a href="JavaScript:Void(0);"
																				data-tip='Update'
																				onClick={() => this.showGlobalConfigModal('vatNumber', 'text')}
																			>
																				<i className="fas fa-cog"></i>
																			</a>
																			<ReactTooltip
																				globalEventOff="click"
																				effect="float"
																				place="top"
																				data-border="true"
																			/>
																		</div>
																	</div>
																</div>
															</div>
														</div>

													</div>
												</div>
											</Card.Body>
										</Accordion.Collapse>
									</Card>

								</Accordion>

							</div>
						</div>
					</div>

				</section>

				<Modal show={this.state.editService} onHide={() => { this.clearState() }}>
					<Modal.Header closeButton>
						<Modal.Title> Edit Service</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<div className="row">

							<div className="col-md-12">
								<div className="form-group">
									<label>Service Name:</label>
									<input
										type="text"
										name="serviceName"
										value={serviceName}
										className="form-control"
										placeholder="Enter Service Name"
										onChange={event => this.setState({ serviceName: event.target.value })}
										onFocus={() => this.setState({ serviceNameError: "" })}
										readOnly={true}
									/>
									<span style={{ color: 'red' }}>{this.state.serviceNameError ? `* ${this.state.serviceNameError}` : ''}</span>
								</div>
							</div>

							<div className="col-md-6">
								<div className="form-group">
									<label>Cost Per Minute:</label>
									<input
										type="text"
										name="costPerMin"
										value={costPerMin}
										className="form-control"
										placeholder="Enter Cost Per Minute"
										// onChange={event => this.setState({ costPerMin: event.target.value })}
										onChange={(event) => { this.changecostPerMin(event) }}
										onFocus={() => this.setState({ costPerMinError: "" })}
									/>
									<span style={{ color: 'red' }}>{this.state.costPerMinError ? `* ${this.state.costPerMinError}` : ''}</span>
								</div>
							</div>

							<div className="col-md-6">
								<div className="form-group">
									<label>Multiplier:</label>
									<input
										type="text"
										name="multiplier"
										value={multiplier}
										className="form-control"
										placeholder="Enter multiplier"
										// onChange={event => this.setState({ multiplier: event.target.value })}
										onChange={(event) => { this.changeMultiplier(event) }}
										onFocus={() => this.setState({ multiplierError: "" })}
									/>
									<span style={{ color: 'red' }}>{this.state.multiplierError ? `* ${this.state.multiplierError}` : ''}</span>
								</div>
							</div>

							<div className="col-md-6">
								<div className="form-group">
									<label>Price Per Minute:</label>
									<input
										type="text"
										name="pricePerMin"
										value={pricePerMin}
										className="form-control"
										placeholder="Enter Price Per Minute"
										onChange={event => this.setState({ pricePerMin: event.target.value })}
										onChange={(event) => { this.changePricePerMin(event) }}
										onFocus={() => this.setState({ pricePerMinError: "" })}
									/>
									<span style={{ color: 'red' }}>{this.state.pricePerMinError ? `* ${this.state.pricePerMinError}` : ''}</span>
								</div>
							</div>

						</div>
					</Modal.Body>
					<Modal.Footer>
						{!loading ?
							<Button variant="primary" onClick={() => this.editUsers()}>
								Save
                </Button>
							:
							null}
						{loading ?
							<Button variant="primary">
								<i className="fa fa-spinner fa-spin" ></i>
							</Button>
							:
							null}
					</Modal.Footer>
				</Modal>

				<Modal size="lg" show={this.state.addService} onHide={() => { this.clearState() }}>
					<Modal.Header closeButton>
						<Modal.Title> Add Service</Modal.Title>
					</Modal.Header>
					<Modal.Body>

						<div className="row">

							<div className="col-md-6">
								<div className="form-group">
									<label>Service Name:</label>
									<select
										name="serviceName"
										value={serviceName}
										className="form-control"
										onChange={event => this.setState({ serviceName: event.target.value })}
										onFocus={() => this.setState({ serviceNameError: "" })}
									>
										<option value="">Select Service</option>
										{Object.keys(cred.SERVICES).map((key) => {
											return _.filter(services, { serviceKey: cred.SERVICES[key].key }).length < 1 ?
												<option value={cred.SERVICES[key].key} >{cred.SERVICES[key].value}</option>
												: null
										})}
									</select>
									<span style={{ color: 'red' }}>{this.state.serviceNameError ? `* ${this.state.serviceNameError}` : ''}</span>
								</div>
							</div>

							<div className="col-md-6">
								<div className="form-group">
									<label>Cost Per Minute:</label>
									<input
										type="text"
										name="costPerMin"
										value={costPerMin}
										className="form-control"
										placeholder="Enter Cost Per Minute"
										// onChange={event => this.setState({ costPerMin: event.target.value })}
										onChange={(event) => { this.changecostPerMin(event) }}
										onFocus={() => this.setState({ costPerMinError: "" })}
									/>
									<span style={{ color: 'red' }}>{this.state.costPerMinError ? `* ${this.state.costPerMinError}` : ''}</span>
								</div>
							</div>

							<div className="col-md-6">
								<div className="form-group">
									<label>Multiplier:</label>
									<input
										type="text"
										name="multiplier"
										value={multiplier}
										className="form-control"
										placeholder="Enter multiplier"
										// onChange={event => this.setState({ multiplier: event.target.value })}
										onChange={(event) => { this.changeMultiplier(event) }}
										onFocus={() => this.setState({ multiplierError: "" })}
									/>
									<span style={{ color: 'red' }}>{this.state.multiplierError ? `* ${this.state.multiplierError}` : ''}</span>
								</div>
							</div>

							<div className="col-md-6">
								<div className="form-group">
									<label>Price Per Minute:</label>
									<input
										type="text"
										name="pricePerMin"
										value={pricePerMin}
										className="form-control"
										placeholder="Enter Price Per Minute"
										// onChange={event => this.setState({ pricePerMin: event.target.value })}
										onChange={(event) => { this.changePricePerMin(event) }}
										onFocus={() => this.setState({ pricePerMinError: "" })}
									/>
									<span style={{ color: 'red' }}>{this.state.pricePerMinError ? `* ${this.state.pricePerMinError}` : ''}</span>
								</div>
							</div>

						</div>
					</Modal.Body>
					<Modal.Footer>
						{!loading ?
							<Button variant="primary" onClick={(event) => this.addService(event)}>
								Save
                </Button>
							:
							null}
						{loading ?
							<Button variant="primary">
								<i className="fa fa-spinner fa-spin" ></i>
							</Button>
							:
							null}
					</Modal.Footer>
				</Modal>

				<Modal show={this.state.addOthService} onHide={() => { this.clearState() }}>
					<Modal.Header closeButton>
						<Modal.Title> Add Service Configuration</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<div className="row">

							<div className="col-md-6">
								<div className="form-group">
									<label>Company Type:</label>

									<select
										name="companyType"
										value={companyType}
										className="form-control"
										onChange={event => this.setState({ companyType: event.target.value })}
										onFocus={() => this.setState({ companyTypeError: "" })}
									>
										<option value="">Select Company Type</option>
										{companyTypeArray.map(type => {
											return _.filter(services1, { companyType: type }).length < 1 ?
												<option value={type} >{type}</option>
												: null
										})}

									</select>
									<span style={{ color: 'red' }}>{this.state.companyTypeError ? `* ${this.state.companyTypeError}` : ''}</span>
								</div>
							</div>

							<div className="col-md-6">
								<div className="form-group">
									<label>No of Seats:</label>
									<input
										type="text"
										name="noOfSeats"
										value={noOfSeats}
										className="form-control"
										placeholder="Enter no of seats"
										onChange={event => this.setState({ noOfSeats: event.target.value })}
										// onChange={(event) => { this.changecostPerMin(event) }}
										onFocus={() => this.setState({ noOfSeatsError: "" })}
									/>
									<span style={{ color: 'red' }}>{this.state.noOfSeatsError ? `* ${this.state.noOfSeatsError}` : ''}</span>
								</div>
							</div>

							<div className="col-md-6">
								<div className="form-group">
									<label>Price Per Seat:</label>
									<input
										type="text"
										name="pricePerSeat"
										value={pricePerSeat}
										className="form-control"
										placeholder="Enter Price"
										onChange={event => this.setState({ pricePerSeat: event.target.value })}
										// onChange={(event) => { this.changeMultiplier(event) }}
										onFocus={() => this.setState({ pricePerSeatError: "" })}
									/>
									<span style={{ color: 'red' }}>{this.state.pricePerSeatError ? `* ${this.state.pricePerSeatError}` : ''}</span>
								</div>
							</div>
							{companyType && companyType == `${cred.COMPANY_TYPE_FREE_TYPE}` ?
								<div className="col-md-6">
									<div className="form-group">
										<label>Trial Period(days):</label>
										<input
											type="text"
											name="trialPeriod"
											value={trialPeriod}
											className="form-control"
											placeholder="Enter Price"
											onChange={event => this.setState({ trialPeriod: event.target.value })}
											// onChange={(event) => { this.changeMultiplier(event) }}
											onFocus={() => this.setState({ trialPeriodError: "" })}
										/>
										<span style={{ color: 'red' }}>{this.state.trialPeriodError ? `* ${this.state.trialPeriodError}` : ''}</span>
									</div>
								</div>
								: null}

							<div className="col-md-6">
								<div className="form-group">
									<label>Currency:</label>
									<select
										name="currency"
										value={currency}
										className="form-control"
										onChange={event => this.setState({ currency: event.target.value })}
										onFocus={() => this.setState({ currencyError: "" })}
									>
										{/* <option value="">Select Currency</option> */}
										{/* <option value={cred.CURRENCY.usd}>{cred.CURRENCY.usd}</option> */}
										<option value={cred.CURRENCY.gbp}>{cred.CURRENCY.gbp}</option>
										{/* <option value={cred.CURRENCY.naira}>{cred.CURRENCY.naira}</option> */}
										{/* <option value={cred.CURRENCY.euro}>{cred.CURRENCY.euro}</option> */}
									</select>
									<span style={{ color: 'red' }}>{this.state.currencyError ? `* ${this.state.currencyError}` : ''}</span>
								</div>
							</div>

							<div className="col-md-6">
								<div className="form-group">
									<label>VAT:(%)</label>
									<input
										type="text"
										name="vat"
										value={vat}
										className="form-control"
										placeholder="Enter VAT"
										onChange={event => this.setState({ vat: event.target.value })}
										onFocus={() => this.setState({ vatError: "" })}
									/>
									<span style={{ color: 'red' }}>{this.state.vatError ? `* ${this.state.vatError}` : ''}</span>
								</div>
							</div>

							<div className="col-md-12">
								<div className="form-group">
									<label>Welcome Tune: {voiceUrl}</label>
								</div>
							</div>

							<div className="col-md-12">
								<div className="form-group">
									<label>Upload welcome tune(MP3/WAV)</label>
									<input
										type="file" name="welcome"
										ref="welcome"
										className="form-control"
										onChange={(e) => { this.onFileChange(e) }} multiple={false}
									// style= {{background: "#0083C6", color: "#fff"}}
									/>
									{this.state.fileUploaded1 ?
										<ProgressBar striped variant="success" now={this.state.fileUploaded1} label={`${this.state.fileUploaded1}%`} />
										:
										null}
								</div>
							</div>

							{/* <div className="col-md-12">
								<div className="form-group">
									<label>Branding Voice: {brandVoiceUrl}</label>
								</div>
							</div>

							<div className="col-md-12">
								<div className="form-group">
									<label>Upload branding tune(MP3/WAV)</label>
									<input
										type="file" name="branding"
										ref="branding"
										className="form-control"
										onChange={(e) => { this.onFileChange(e) }} multiple={false}
									// style= {{background: "#0083C6", color: "#fff"}}
									/>
									{this.state.fileUploaded2 ?
										<ProgressBar striped variant="success" now={this.state.fileUploaded2} label={`${this.state.fileUploaded2}%`} />
										:
										null}
								</div>
							</div> */}

							<div className="col-md-12">
								<div className="form-group">
									<label>Invoice Billing Info</label>
									<textarea
										type="text"
										rows="5"
										name="invoiceBillingInfo"
										value={invoiceBillingInfo}
										className="form-control"
										placeholder="Enter Invoice Billing Info"
										onChange={event => this.setState({ invoiceBillingInfo: event.target.value })}
										style={{ height: '150px' }}
									// onFocus={() => this.setState({ vatError: "" })}
									/>
								</div>
							</div>
						</div>
					</Modal.Body>
					<Modal.Footer>

						<div className="row">
							<div className="col-md-6">
								<div className="form-group" style={{ marginRight: "25px" }}>
									{loading ?
										<Button variant="primary"> <i className="fa fa-spinner fa-spin" ></i> </Button>
										:
										null}
									{!loading ?
										<Button variant="primary" onClick={(event) => this.addOthService(event)}> Save </Button>
										:
										null}
								</div>
							</div>
							<div className="col-md-6">

							</div>
						</div>

					</Modal.Footer>
				</Modal>

				<Modal show={this.state.editServiceConfig} onHide={() => { this.clearState() }}>
					<Modal.Header closeButton>
						<Modal.Title> Edit Service Configuration</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<div className="row">

							<div className="col-md-6">
								<div className="form-group">
									<label>Company Type:</label>

									<input
										name="companyType"
										value={companyType}
										className="form-control"
										// onChange={event => this.setState({ companyType: event.target.value })}
										// onFocus={() => this.setState({ companyTypeError: "" })}
										readOnly={true}
									/>

									<span style={{ color: 'red' }}>{this.state.companyTypeError ? `* ${this.state.companyTypeError}` : ''}</span>
								</div>
							</div>

							<div className="col-md-6">
								<div className="form-group">
									<label>No of Seats:</label>
									<input
										type="text"
										name="noOfSeats"
										value={noOfSeats}
										className="form-control"
										placeholder="Enter no of seats"
										onChange={event => this.setState({ noOfSeats: event.target.value })}
										// onChange={(event) => { this.changecostPerMin(event) }}
										onFocus={() => this.setState({ noOfSeatsError: "" })}
									/>
									<span style={{ color: 'red' }}>{this.state.noOfSeatsError ? `* ${this.state.noOfSeatsError}` : ''}</span>
								</div>
							</div>

							<div className="col-md-6">
								<div className="form-group">
									<label>Price Per Seat:</label>
									<input
										type="text"
										name="pricePerSeat"
										value={pricePerSeat}
										className="form-control"
										placeholder="Enter Price"
										onChange={event => this.setState({ pricePerSeat: event.target.value })}
										// onChange={(event) => { this.changeMultiplier(event) }}
										onFocus={() => this.setState({ pricePerSeatError: "" })}
									/>
									<span style={{ color: 'red' }}>{this.state.pricePerSeatError ? `* ${this.state.pricePerSeatError}` : ''}</span>
								</div>
							</div>
							{companyType && companyType == `${cred.COMPANY_TYPE_FREE_TYPE}` ?
								<div className="col-md-6">
									<div className="form-group">
										<label>Trial Period(days):</label>
										<input
											type="text"
											name="trialPeriod"
											value={trialPeriod}
											className="form-control"
											placeholder="Enter Price"
											onChange={event => this.setState({ trialPeriod: event.target.value })}
											// onChange={(event) => { this.changeMultiplier(event) }}
											onFocus={() => this.setState({ trialPeriodError: "" })}
										/>
										<span style={{ color: 'red' }}>{this.state.trialPeriodError ? `* ${this.state.trialPeriodError}` : ''}</span>
									</div>
								</div>
								: null}

							<div className="col-md-6">
								<div className="form-group">
									<label>Currency:</label>
									<select
										name="currency"
										value={currency}
										className="form-control"
										onChange={event => this.setState({ currency: event.target.value })}
										onFocus={() => this.setState({ currencyError: "" })}
									>
										{/* <option value="">Select Currency</option> */}
										{/* <option value={cred.CURRENCY.usd}>{cred.CURRENCY.usd}</option> */}
										<option value={cred.CURRENCY.gbp}>{cred.CURRENCY.gbp}</option>
										{/* <option value={cred.CURRENCY.naira}>{cred.CURRENCY.naira}</option> */}
										{/* <option value={cred.CURRENCY.euro}>{cred.CURRENCY.euro}</option> */}
									</select>
									<span style={{ color: 'red' }}>{this.state.currencyError ? `* ${this.state.currencyError}` : ''}</span>
								</div>
							</div>

							<div className="col-md-6">
								<div className="form-group">
									<label>VAT:(%)</label>
									<input
										type="text"
										name="vat"
										value={vat}
										className="form-control"
										placeholder="Enter VAT"
										onChange={event => this.setState({ vat: event.target.value })}
										onFocus={() => this.setState({ vatError: "" })}
									/>
									<span style={{ color: 'red' }}>{this.state.vatError ? `* ${this.state.vatError}` : ''}</span>
								</div>
							</div>

							<div className="col-md-12">
								<div className="form-group">
									<label>Welcome Tune: </label><br></br>
									{voiceUrl && <audio controls>
										<source src={voiceUrl} type="audio/mpeg" />
										{voiceUrl}
									</audio>
									}
								</div>
							</div>

							<div className="col-md-12">
								<div className="form-group">
									<label>Upload welcome tune(MP3/WAV)</label>
									<input
										type="file" name="welcome"
										ref="welcome"
										className="form-control"
										onChange={(e) => { this.onFileChange(e) }} multiple={false}
									// style= {{background: "#0083C6", color: "#fff"}}
									/>
									{this.state.fileUploaded1 ?
										<ProgressBar striped variant="success" now={this.state.fileUploaded1} label={`${this.state.fileUploaded1}%`} />
										:
										null}
								</div>
							</div>

							{/* <div className="col-md-12">
								<div className="form-group">
									<label>Branding Voice:</label><br></br>
									{brandVoiceUrl && <audio controls>
										<source src={brandVoiceUrl} type="audio/mpeg" />
										{brandVoiceUrl}
									</audio>
									}
								</div>
							</div>

							<div className="col-md-12">
								<div className="form-group">
									<label>Upload branding tune(MP3/WAV)</label>
									<input
										type="file" name="branding"
										ref="branding"
										className="form-control"
										onChange={(e) => { this.onFileChange(e) }} multiple={false}
									// style= {{background: "#0083C6", color: "#fff"}}
									/>
									{this.state.fileUploaded2 ?
										<ProgressBar striped variant="success" now={this.state.fileUploaded2} label={`${this.state.fileUploaded2}%`} />
										:
										null}
								</div>
							</div> */}

							<div className="col-md-12">
								<div className="form-group">
									<label>Invoice Billing Info</label>
									<textarea
										type="text"
										rows="5"
										name="vat"
										value={invoiceBillingInfo}
										className="form-control"
										placeholder="Enter Invoice Billing Info"
										onChange={event => this.setState({ invoiceBillingInfo: event.target.value })}
										style={{ height: '150px' }}
									// onFocus={() => this.setState({ vatError: "" })}
									/>
									{/* <span style={{ color: 'red' }}>{this.state.vatError ? `* ${this.state.vatError}` : ''}</span> */}
								</div>
							</div>

						</div>
					</Modal.Body>
					<Modal.Footer>

						<div className="row">
							<div className="col-md-6">
								<div className="form-group" style={{ marginRight: "25px" }}>
									{loading ?
										<Button variant="primary"> <i className="fa fa-spinner fa-spin" ></i> </Button>
										:
										null}
									{!loading ?
										<Button variant="primary" onClick={(event) => this.editOthService(event)}> Save </Button>
										:
										null}
								</div>
							</div>
							<div className="col-md-6">

							</div>
						</div>
					</Modal.Footer>
				</Modal>

				<Modal show={this.state.deleteService} onHide={() => this.hideDeleteConfirm()}>
					<Modal.Header closeButton>
						<Modal.Title>Delete Confirmation</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<h3>Are you sure, delete {this.state.deleteItem} ?</h3>
					</Modal.Body>
					<Modal.Footer>
						{this.state.deleteOthService ?
							<Button variant="secondary" onClick={() => this.deleteOthService()}>
								Yes
                </Button>
							:
							<Button variant="secondary" onClick={() => this.deleteService()}>
								Yes
                </Button>
						}
						<Button variant="primary" onClick={() => this.hideDeleteConfirm()}>
							No
            </Button>
					</Modal.Footer>
				</Modal>

				<Modal show={this.state.showSuccessAlert} onHide={() => this.hideUploadAlert()}>
					<Modal.Header closeButton>
						<Modal.Title>OK</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<h5 style={{ color: 'green' }}>{`${this.state.successAlertMessage}`}</h5>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="primary" onClick={() => this.hideUploadAlert()}>
							Ok
            </Button>
					</Modal.Footer>
				</Modal>

				<Modal show={this.state.showErrorAlert} onHide={() => this.hideUploadAlert()}>
					<Modal.Header closeButton>
						<Modal.Title>OK</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<h5 style={{ color: 'red' }}>{`${this.state.errorAlertMessage}`}</h5>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="primary" onClick={() => this.hideUploadAlert()}>
							Ok
                    </Button>
					</Modal.Footer>
				</Modal>

				<Modal size="lg" show={this.state.showImportAddress} onHide={() => this.hideImportAddress()}>
					<Modal.Header closeButton>
						<Modal.Title>Verify Number List</Modal.Title>
					</Modal.Header>
					<Modal.Body>

						{this.state.data && this.state.data.length > 0 ?
							this.state.data.map((contact, index) => {
								return <div className='row' key={index}>
									{/* {console.log(index)} */}
									{/* countryCodeRecords */}
									<div className='col-md-2'>
										<div className="form-group">
											<label>Country Code</label>
											<select className="form-control"
												name="countryCode"
												value={contact['countryCode']}
												onChange={e => this.importDataChange(index, e.target.value, 'countryCode')}
											>
												<option value="">Select country</option>
												{this.state.countryCodeRecords && this.state.countryCodeRecords.map((list, index) => {
													return <option value={list.code}
														selected={contact['countryCode'] == list.code ? true : false}
													>
														{list.code + ' ' + list.name}
													</option>
												})}
											</select>
											{!contact['countryCode'] || isNaN(contact['countryCode']) ? <span style={{ color: 'red' }}>*Enter valid country code</span> : null}
										</div>
									</div>

									<div className='col-md-2'>
										<div className="form-group">
											<label>Phone Number</label>
											<input
												type='text'
												className="form-control"
												value={contact['phoneNumber']}
												onChange={e => this.importDataChange(index, e.target.value, 'phoneNumber')}
											/>
											{!contact['phoneNumber'] || isNaN(contact['phoneNumber']) || contact['phoneNumber'].length != 10 ? <span style={{ color: 'red' }}>*Enter a valid phone no.</span> : null}
										</div>
									</div>

									<div className='col-md-2'>
										<div className="form-group">
											<label>SIP Provider</label>
											<input
												type='text'
												className="form-control"
												value={contact['sipProvider']}
												onChange={e => this.importDataChange(index, e.target.value, 'sipProvider')}
											/>
											{!contact['sipProvider'] ? <span style={{ color: 'red' }}>*Enter a SIP Provider no.</span> : null}
										</div>
									</div>

									<div className='col-md-2'>
										<div className="form-group">
											<label>Country</label>
											<input
												type='text'
												className="form-control"
												value={contact['country']}
												onChange={e => this.importDataChange(index, e.target.value, 'country')}
											/>
											{!contact['country'] ? <span style={{ color: 'red' }}>*Enter a country</span> : null}
										</div>
									</div>

									<div className='col-md-2'>
										<div className="form-group">
											<label>Number Type</label>
											<input
												type='text'
												className="form-control"
												value={contact['numberType']}
												onChange={e => this.importDataChange(index, e.target.value, 'numberType')}
											/>
											{!contact['numberType'] ? <span style={{ color: 'red' }}>*Enter a number type</span> : null}
										</div>
									</div>

									<div className='col-md-2'>
										<div className="form-group">
											<label>Cost</label>
											<input
												type='text'
												className="form-control"
												value={contact['costPerMinute']}
												onChange={e => this.importDataChange(index, e.target.value, 'costPerMinute')}
											/>
											{!contact['costPerMinute'] || isNaN(contact['costPerMinute']) ? <span style={{ color: 'red' }}>*Enter a cost</span> : null}
										</div>
									</div>

								</div>
							})
							:
							<div className='rew'>
								<div className='col-md-12' style={{ textAlign: 'center' }}>Data not selected</div>
							</div>
						}
					</Modal.Body>
					<Modal.Footer>
						{!loading ?
							<Button variant="primary" onClick={() => this.saveContacts()}>
								Save numbers
                            </Button>
							:
							null}
						{loading ?
							<Button variant="primary">
								<i className="fa fa-spinner fa-spin" ></i>
							</Button>
							:
							null}

					</Modal.Footer>
				</Modal>

				<Modal show={this.state.showImportCode} onHide={() => this.hideImportCode()}>
					<Modal.Header closeButton>
						<Modal.Title>Verify Country Code List</Modal.Title>
					</Modal.Header>
					<Modal.Body>

						{this.state.codeData && this.state.codeData.length > 0 ?
							this.state.codeData.map((contact, index) => {
								return <div className='row' key={index}>
									{/* {console.log(index)} */}

									<div className='col-md-6'>
										<div className="form-group">
											<label>Country Code</label>
											<input
												type='text'
												className="form-control"
												name="countryName"
												value={codeData[index]['countryName']}
												onChange={e => this.importCCDataChange(index, e.target.value, 'countryName')}
											/>

											{!codeData[index]['countryName'] ? <span style={{ color: 'red' }}>*Enter valid country name</span> : null}
										</div>
									</div>

									<div className='col-md-6'>
										<div className="form-group">
											<label>Country Code</label>
											<input
												type='text'
												className="form-control"
												name="countryCode"
												value={codeData[index]['countryCode']}
												onChange={e => this.importCCDataChange(index, e.target.value, 'countryCode')}
											/>
											{!codeData[index]['countryCode'] || isNaN(codeData[index]['countryCode']) ? <span style={{ color: 'red' }}>*Enter a valid phone no.</span> : null}
										</div>
									</div>

								</div>
							})
							:
							<div className='rew'>
								<div className='col-md-12' style={{ textAlign: 'center' }}>Data not selected</div>
							</div>
						}
					</Modal.Body>
					<Modal.Footer>
						{!loading ?
							<Button variant="primary" onClick={() => this.saveCountryCode()}>
								Save country code
                            </Button>
							:
							null}
						{loading ?
							<Button variant="primary">
								<i className="fa fa-spinner fa-spin" ></i>
							</Button>
							:
							null}

					</Modal.Footer>
				</Modal>

				<Modal size="lg" show={this.state.showImportOB} onHide={() => this.hideImportOB()}>
					<Modal.Header closeButton>
						<Modal.Title>Verify Out Bound List</Modal.Title>
					</Modal.Header>
					<Modal.Body>

						{this.state.outBoundData && this.state.outBoundData.length > 0 ?
							this.state.outBoundData.map((contact, index) => {
								return <div className='row' key={index}>

									<div className='col-md-2'>
										<div className="form-group">
											<label>Region</label>
											<input
												type='text'
												className="form-control"
												name="region"
												value={outBoundData[index]['region']}
												onChange={e => this.importOBDataChange(index, e.target.value, 'region')}
											/>

											{!outBoundData[index]['region'] ? <span style={{ color: 'red' }}>*Enter valid region</span> : null}
										</div>
									</div>

									<div className='col-md-2'>
										<div className="form-group">
											<label>Code</label>
											<input
												type='text'
												className="form-control"
												name="dialCode"
												value={outBoundData[index]['dialCode']}
												onChange={e => this.importOBDataChange(index, e.target.value, 'dialCode')}
											/>
											{!outBoundData[index]['dialCode'] || isNaN(outBoundData[index]['dialCode']) ? <span style={{ color: 'red' }}>*Enter a valid code</span> : null}
										</div>
									</div>

									<div className='col-md-2'>
										<div className="form-group">
											<label>Provider</label>
											<input
												type='text'
												className="form-control"
												name="provider"
												value={outBoundData[index]['provider']}
												onChange={e => this.importOBDataChange(index, e.target.value, 'provider')}
											/>
											{!outBoundData[index]['provider'] ? <span style={{ color: 'red' }}>*Enter a provider</span> : null}
										</div>
									</div>

									<div className='col-md-2'>
										<div className="form-group">
											<label>Cost/Min</label>
											<input
												type='text'
												className="form-control"
												name="costPerMinute"
												value={outBoundData[index]['costPerMinute']}
												onChange={e => this.importOBDataChange(index, e.target.value, 'costPerMinute')}
											/>
											{!outBoundData[index]['costPerMinute'] || isNaN(outBoundData[index]['costPerMinute']) ? <span style={{ color: 'red' }}>*Enter a valid code</span> : null}
										</div>
									</div>

									<div className='col-md-2'>
										<div className="form-group">
											<label>Multiplier</label>
											<input
												type='text'
												className="form-control"
												name="multiplier"
												value={outBoundData[index]['multiplier']}
												onChange={e => this.importOBDataChange(index, e.target.value, 'multiplier')}
											/>
											{!outBoundData[index]['multiplier'] || isNaN(outBoundData[index]['multiplier']) ? <span style={{ color: 'red' }}>*Enter a valid multiplayer</span> : null}
										</div>
									</div>

									<div className='col-md-2'>
										<div className="form-group">
											<label>Price/Min</label>
											<input
												type='text'
												className="form-control"
												name="pricePerMinute"
												value={outBoundData[index]['pricePerMinute']}
												onChange={e => this.importOBDataChange(index, e.target.value, 'pricePerMinute')}
											/>
											{!outBoundData[index]['pricePerMinute'] || isNaN(outBoundData[index]['pricePerMinute']) ? <span style={{ color: 'red' }}>*Enter a valid code</span> : null}
										</div>
									</div>

								</div>
							})
							:
							<div className='rew'>
								<div className='col-md-12' style={{ textAlign: 'center' }}>Data not selected</div>
							</div>
						}
					</Modal.Body>
					<Modal.Footer>
						{!loading ?
							<Button variant="primary" onClick={() => this.saveOutBound()}>
								Save List
                            </Button>
							:
							null}
						{loading ?
							<Button variant="primary">
								<i className="fa fa-spinner fa-spin" ></i>
							</Button>
							:
							null}

					</Modal.Footer>
				</Modal>

				<Modal show={this.state.showGlobalConfig} onHide={() => { this.clearState() }}>
					<Modal.Header closeButton>
						<Modal.Title> Global Configuration</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<div className="row">

							{this.state.globalFieldEditType && this.state.globalFieldEditType == `text` ?
								<div className="col-md-12">
									<div className="form-group">
										<label>{this.state.globalFieldName}:</label>
										<input
											type="text"
											name="pricePerSeat"
											value={this.state.globalFieldValue}
											className="form-control"
											placeholder={this.state.globalFieldName}
											onChange={event => this.setState({ globalFieldValue: event.target.value })}
										// onFocus={() => this.setState({ pricePerSeatError: "" })}
										/>
										{/* <span style={{ color: 'red' }}>{this.state.pricePerSeatError ? `* ${this.state.pricePerSeatError}` : ''}</span> */}
									</div>
								</div>
								: null}

							{this.state.globalFieldEditType && this.state.globalFieldEditType == `file` ?
								<>
									<div className="col-md-12">
										<div className="form-group">
											<label>{this.state.globalFieldName}:</label><br></br>
											{this.state.globalFieldValue && <audio controls>
												<source src={this.state.globalFieldValue} type="audio/mpeg" />
												{this.state.globalFieldValue}
											</audio>
											}
										</div>
									</div>

									<div className="col-md-12">
										<div className="form-group">
											<label>Upload {this.state.globalFieldName}(MP3/WAV)</label>
											<input
												type="file"
												name={this.state.globalFieldName}
												ref={this.state.globalFieldName}
												className="form-control"
												onChange={(e) => { this.onFileChange(e) }} multiple={false}
											// style= {{background: "#0083C6", color: "#fff"}}
											/>
											{this.state.fileUploaded1 ?
												<ProgressBar striped variant="success" now={this.state.fileUploaded1} label={`${this.state.fileUploaded1}%`} />
												:
												null}
										</div>
									</div>
								</>
								: null}

						</div>
					</Modal.Body>
					<Modal.Footer>

						<div className="row">
							<div className="col-md-6">
								<div className="form-group" style={{ marginRight: "25px" }}>
									{loading ?
										<Button variant="primary"> <i className="fa fa-spinner fa-spin" ></i> </Button>
										:
										null}
									{!loading ?
										<Button variant="primary" onClick={(event) => this.setGlobalData(event)}> Save </Button>
										:
										null}
								</div>
							</div>
							<div className="col-md-6">

							</div>
						</div>

					</Modal.Footer>
				</Modal>

			</main >
		);
	}

}

export default checkAuthentication(ServiceManagement);