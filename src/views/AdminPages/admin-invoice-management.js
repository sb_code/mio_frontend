import React, { Component } from 'react'
import Navbar from '../Component/Navbar'
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button, Alert } from "react-bootstrap"
import Table from 'react-bootstrap/Table'
import _ from 'lodash';
import { CSVLink, CSVDownload } from "react-csv";
import ReactTooltip from 'react-tooltip'; // For tool tip 
import { reportCSS } from '../../util/reportCSS';
import { getInvoiceReportHtml } from '../../util/getInvoiceReportHtml';

import { checkAuthentication } from '../Component/Authentication';

var path = cred.API_PATH;


const minOffset = 0;
const maxOffset = 60;

export class AdminInvoiceManagement extends Component {
	constructor(props) {
		super(props)
		this.state = {
			userId: '',

			openSideMenu: false,
			searchString: '',
			searching: false,

			loading: false,
			selectedCompanyId: '',
			selectedCompanyName: '',
			selectedCompanyType: '',
			selectedCompanyCurrency: '',

			invoices: [],

			showInvoiceModal: false,
			viewUser: false,
			editInvoice: false,

			selectedInvoiceId: '',
			selectedInvoice: {},
			invoiceServices: [],
			isPublished: false,
			isEditError: false,

			editInvoice: false,
			newServiceModal: false,
			newServiceName: '',
			newPricePerMinute: '',
			newTotalDuration: '',
			newTotalAmount: '',
			newServiceNameError: '',
			newPricePerMinuteError: '',
			newTotalDurationError: '',
			newTotalAmountError: '',

			viewManualPayment: false,

			selectedUsageDetails: {},
			viewUsageModal: false,

			companyType: '',
			companyTypeError: '',
			isActive: false,

			transactionCurrency: '',
			amount: '',
			referenceCode: '',
			description: '',

			// invoiceCompanyId: '',

			month: '',
			monthError: '',
			thisYear: '',
			selectedYear: '',
			selectedYearError: '',

			showSuccessAlert: false,
			successAlertMessage: '',
			showErrorAlert: false,
			errorAlertMessage: ''

		}
		this.monthNames = ["January", "February", "March", "April", "May", "June",
			"July", "August", "September", "October", "November", "December"
		];
	}

	componentDidMount() {
		document.body.classList.remove("sign-bg");
		document.body.classList.add("grey-bg");

		if (this.props.location.state && this.props.location.state.name && this.props.location.state.type) {

			let selectedCompanyId = this.props.match.params.id;
			let selectedCompanyName = this.props.location.state.name;
			let selectedCompanyType = this.props.location.state.type;
			let selectedCompanyCurrency = this.props.location.state.currency;

			// console.log("selectedCompanyId -- ", selectedCompanyId);

			let storage = localStorage.getItem("MIO_Local");
			storage = storage ? JSON.parse(storage) : null;
			if (storage && storage.data.userId) {
				this.setState({
					userId: storage.data.userId,
					companyType: storage.data.companyType,
					companyId: storage.data.companyId,
					selectedCompanyId: selectedCompanyId,
					selectedCompanyName: selectedCompanyName,
					selectedCompanyType: selectedCompanyType,
					selectedCompanyCurrency
				}, () => { this.getInvoiceList() });

				// console.log(this.parseJwt(storage.token));
				let tokenData = this.parseJwt(storage.token);

			}
		} else {
			this.props.history.push("/adminpaymentmanagement");
		}

	}

	getInvoiceList = () => {

		this.setState(prevState => ({ loading: true }));
		let year = new Date().getFullYear();
		let dataToSend = {
			"companyId": this.state.selectedCompanyId,
			"year": year
		};
		console.log("dataToSend--", dataToSend);
		// API to get invoices -- 
		axios
			.post(path + 'price-management/fetch-invoice-companyId', dataToSend)
			.then(serverResponse => {

				let res = serverResponse.data;
				if (!res.isError) {

					let result = res.details;
					this.setState({
						invoices: result,
						loading: false
					}, () => {
						console.log("user-management---", this.state.invoices);
					});
				} else {
					this.setState(prevState => ({ loading: false }));
				}

			})
			.catch(error => {
				this.setState(prevState => ({ loading: false }));
				console.log(error);
			})

	}

	generateInvoice = () => {

		this.setState(prevState => ({ loading: true }));
		let { loading, month, selectedYear, companyType, selectedCompanyId } = this.state;

		!month ? this.setState({ monthError: "Please enter a month." }) : this.setState({ monthError: "" });
		!selectedYear ? this.setState({ selectedYearError: "Please select a year." }) : this.setState({ selectedYearError: "" });

		if (month && selectedYear) {

			let dataToSend = {
				"companyId": selectedCompanyId,
				"year": Number(selectedYear),
				"month": Number(month)
			};

			// For saving new user -- 
			axios
				.post(path + 'price-management/create-invoice', dataToSend)
				.then(serverResponse => {
					let res = serverResponse.data;
					if (!res.isError && res.statuscode == 200) {
						// console.log(res);
						this.setState({
							loading: false,
							showInvoiceModal: false,
							showSuccessAlert: true,
							successAlertMessage: "Successfully saved"
						}, () => {
							this.getInvoiceList();
							this.clearState();
						});

					} else {

						this.clearState();
						// let message = '';
						// message = res.message;
						this.setState({
							showErrorAlert: true,
							errorAlertMessage: `Sorry! invoice can't be created. ${res.message}`
						});
					}
				})
				.catch(error => {
					console.log(error);
					this.setState(prevState => ({ loading: false }));
				})

		}

	}

	hideSuccessAlert() {
		this.setState({
			showSuccessAlert: false,
			successAlertMessage: '',
		})
	}

	hideErrorAlert() {
		this.setState({
			showErrorAlert: false,
			errorAlertMessage: '',
		})
	}

	getTotalAmount = (index) => {
		let { invoices } = this.state;
		let invoice = invoices[index];
		let total = 0;
		invoice && invoice.services.map(value => {
			total = parseFloat(parseFloat(total) + parseFloat(value.totalPrice)).toFixed(2);
		})
		return total;
	}

	openAddressBook = (index) => {
		let { invoices } = this.state;
		let user = invoices[index];
		let { userId, fname, lname } = user;

		// Linking to addressBook Page - 
		// this.props.history && this.props.history.push(`/adminaddressbook/${1}/${2}`);
		this.props.history.push({
			pathname: '/adminaddressbook/' + `${this.state.selectedCompanyId}` + '/' + `${userId}`,
			state: {
				name: this.state.selectedCompanyName,
				type: this.state.selectedCompanyType,
				userId: userId,
				userName: fname + ' ' + lname
			}
		});

	}

	openEditInvoice = (index) => {

		let { invoices } = this.state;
		let invoice = invoices[index];
		let { _id, services } = invoice;

		// Getting data for edit modal -- 
		this.setState({
			// invoiceCompanyId: invoice.companyId,
			selectedInvoiceId: _id,
			selectedInvoice: invoice,
			invoiceServices: services,
			isPublished: invoice.isPublished,
			editInvoice: true
		});

	}

	newServiceSetState(fieldName, fieldVal) {
		let reg = /^-?\d+\.?\d*$/;
		switch (fieldName) {
			case 'newServiceName':
				this.setState({ newServiceName: fieldVal });
				break;
			case 'newPricePerMinute':
				fieldVal = fieldVal ? fieldVal : 0;
				if (!reg.test(fieldVal)) {
					this.setState({ newPricePerMinuteError: 'Invalid entry.' })
				} else {
					this.setState({ newPricePerMinute: fieldVal });
				}
				break;
			case 'newTotalDuration':
				fieldVal = fieldVal ? fieldVal : 0;
				if (!reg.test(fieldVal)) {
					this.setState({ newTotalDurationError: 'Invalid entry.' })
				} else {
					this.setState({ newTotalDuration: fieldVal });
				}
				break;
			case 'newTotalAmount':
				fieldVal = fieldVal ? fieldVal : 0;
				if (!reg.test(fieldVal)) {
					this.setState({ newTotalAmountError: 'Invalid entry.' })
				} else {
					this.setState({ newTotalAmount: fieldVal });
				}
				break;
			default:
				break;
		}
	}

	addNewService = () => {

		let { selectedInvoiceId, selectedInvoice, invoiceServices, newServiceName, newPricePerMinute, newTotalDuration, newTotalAmount } = this.state;

		!newServiceName || newServiceName == '' ? this.setState({ newServiceNameError: "Use valid service name" }) : this.setState({ newServiceNameError: "" });
		!newPricePerMinute || isNaN(newPricePerMinute) ? this.setState({ newPricePerMinuteError: "Use valid service name" }) : this.setState({ newPricePerMinuteError: "" });
		!newTotalDuration || isNaN(newTotalDuration) ? this.setState({ newTotalDurationError: "Use valid service name" }) : this.setState({ newTotalDurationError: "" });
		!newTotalAmount || isNaN(newTotalAmount) ? this.setState({ newTotalAmountError: "Use valid service name" }) : this.setState({ newTotalAmountError: "" });

		if (newServiceName && newServiceName != '' && newPricePerMinute && !isNaN(newPricePerMinute) && newTotalDuration && !isNaN(newTotalDuration) && newTotalAmount && !isNaN(newTotalAmount)) {

			let dataToSend = {
				"serviceKey": "CUSTOM",
				"serviceName": newServiceName,
				"pricePerMinute": parseFloat(newPricePerMinute),
				"totalDuration": parseFloat(newTotalDuration),
				"totalPrice": parseFloat(newTotalAmount),
				"totalCost": 0
			};

			let newService = invoiceServices.concat(dataToSend);
			console.log("invoiceServices new ==", invoiceServices.concat(dataToSend));
			this.setState({
				invoiceServices: newService
			}, () => {
				this.clearAddService()
			});

		}

	}

	clearAddService = () => {

		this.setState({
			newServiceName: '',
			newPricePerMinute: '',
			newTotalDuration: '',
			newTotalAmount: '',
			newServiceModal: false
		});

	}

	changeEditInInvoice = (event, key) => {

		let fieldName = event.target.name;
		let fieldVal = event.target.value ? event.target.value : 0;
		let reg = /^-?\d+\.?\d*$/;
		let { invoiceServices } = this.state;
		let changedService = invoiceServices[key];
		let isEditError = false;

		if (!fieldVal) {
			changedService[`${fieldName}`] = 0;
			invoiceServices[key] = changedService;
		} else if (!reg.test(fieldVal)) {
			isEditError = true;
		} else {
			changedService[`${fieldName}`] = fieldVal;
			invoiceServices[key] = changedService;
		}
		this.setState({ invoiceServices, isEditError });
	}

	editInvoice = () => {

		let { isEditError, isPublished, invoiceServices, selectedInvoiceId, selectedInvoice } = this.state;

		if (selectedInvoice && selectedInvoiceId) {

			this.setState({ loading: true })

			if (!isPublished) {
				let dataToSend = {
					'companyId': this.state.selectedCompanyId,
					'invoiceId': selectedInvoiceId,
					'services': invoiceServices,
					'isPublished': isPublished
				};
				this.generlEdit(dataToSend);
			} else {
				let dataToSend_1 = {
					'companyId': this.state.selectedCompanyId,
					'invoiceId': selectedInvoiceId,
					'services': invoiceServices,
					'isPublished': false
				};
				let dataToSend_2 = {
					'companyId': this.state.selectedCompanyId,
					'invoiceId': selectedInvoiceId,
					'services': invoiceServices,
					'isPublished': isPublished
				};
				this.editBeforePublish(dataToSend_1, dataToSend_2);
			}

		}

	}

	generlEdit = (dataToSend) => {
		// API For updating invoice details -- 
		axios
			.post(path + 'price-management/edit-invoice-By-Id', dataToSend)
			.then(serverResponse => {
				let res = serverResponse.data;
				if (!res.isError) {

					this.setState({
						loading: false,
						showSuccessAlert: true,
						successAlertMessage: "Successfully updated."
					}, () => {
						this.clearState();
						this.getInvoiceList();
					});
				} else {
					this.clearState();
					this.setState({
						loading: false,
						showErrorAlert: true,
						errorAlertMessage: res.message
					});
					// alert(res.message);
				}
			})
			.catch(error => {
				this.setState({ loading: false });
				console.log(error);
			})

	}

	editBeforePublish = (dataToSend_1, dataToSend_2) => {

		axios
			.post(path + 'price-management/edit-invoice-By-Id', dataToSend_1)
			.then(serverResponse => {
				let res = serverResponse.data;
				if (!res.isError) {
					this.publishInvoice(dataToSend_2);
				} else {
					this.clearState();
					this.setState({
						loading: false,
						showErrorAlert: true,
						errorAlertMessage: res.message
					});
					// alert(res.message);
				}
			})
			.catch(error => {
				this.setState({ loading: false });
				console.log(error);
			})

	}

	publishInvoice = async (dataToSend) => {

		let { selectedInvoice } = this.state;

		let invoiceId = selectedInvoice._id;
		let invoiceCompanyId = selectedInvoice.companyId;
		let invoiceMonth = selectedInvoice.month;
		let invoiceYear = selectedInvoice.year;

		let usageDetails = {};
		let getUsageDetails = await this.fetchUsages(invoiceCompanyId, invoiceYear, invoiceMonth);
		if (getUsageDetails && !getUsageDetails.isError) {
			usageDetails = getUsageDetails.response;
		}
		let invoice = await this.getInvoiceeDetails(invoiceId);
		console.log("Final invoice ===== ", invoice);
		console.log("Final usageDetails ===== ", usageDetails);
		if (invoice) {
			// API ToDo - 
			const htmlMarkup =
				`<html>
                    <head>
                        <style>
                            ${reportCSS}
                        </style>
                    </head>
                    <body>
                        ${getInvoiceReportHtml(invoice, usageDetails)}
                    </body>
                </html>
                `;
			let fileName = "invioce_" + `${invoiceCompanyId}` + "_" + `${this.monthNames[invoiceMonth - 1]}` + "_" + `${invoiceYear}` + ".pdf";
			axios
				.post(path + 'payment/invoice-pdf-generate', { html: htmlMarkup, fileName: fileName })
				.then(serverResponse => {
					console.log(serverResponse);
					if (serverResponse.data && !serverResponse.data.isError) {

						// window.location.href = serverResponse.data.details;
						if (!serverResponse.data.details || serverResponse.data.details == '') {
							// alert("PDF can't be generated.");
							this.setState({
								loading: false,
								showErrorAlert: true,
								errorAlertMessage: "PDF can't be generated."
							}, () => {
								this.clearState();
							});
						} else {
							dataToSend.invoiceUrl = serverResponse.data.details;
							this.generlEdit(dataToSend);
						}

					} else {
						this.setState({
							loading: false,
							showErrorAlert: true,
							errorAlertMessage: "PDF can't be generated."
						}, () => {
							this.clearState();
						});
					}
				})
				.catch(error => {
					console.log(error);
					// reject();
				})

		} else {
			alert('Something goes wrong.');
		}


	}

	// For getting usage of all services for an invoice month --
	fetchUsages = (invoiceCompanyId, invoiceYear, invoiceMonth) => {

		return new Promise((resolve, reject) => {

			let dataToSend = {
				"companyId": invoiceCompanyId,
				"month": Number(invoiceMonth),
				"year": Number(invoiceYear)
			};
			// API --
			axios
				.post(path + 'price-management/get-monthly-usage-by-companyId', dataToSend)
				.then(serverResponse => {

					let res = serverResponse.data;
					if (!res.isError) {
						// console.log("all usage details == ", res);

						let usageDetails = res.details;
						resolve({
							"isError": false,
							"response": usageDetails
						});

					} else {
						resolve({
							"isError": true
						});
					}

				})
				.catch(error => {
					console.log(error);
					resolve({
						"isError": true
					});
				})

		})

	}

	// For getting calculations fo summary table --
	// calculationForSummary = (usageDetails) => {

	//     return new Promise((resolve, reject) => {

	//         let archiveList = usageDetails.archiveList;
	//         let webCallList = usageDetails.webCallList;
	//         let sipCallList = usageDetails.sipCallList;

	//         let archiveListArr = [];
	//         let webCallListArr = [];
	//         let sipCallListArr = [];

	//         let archiveCount = 0;
	//         let webCallCount = 0;
	//         let sipCallCount = 0;

	//         // For calculation of archive list --
	//         if (archiveList && archiveList.length > 0) {
	//             let sessionArr = [];
	//             for (let i = 0; i < archiveList.length; i++) {
	//                 if (sessionArr.includes(archiveList[i].sessionId)) {
	//                     // let commonSession = _.filter(archiveListArr, {"sessionId": archiveList[i].sessionId})[0];
	//                     archiveListArr.map((data, index) => {
	//                         if (data.sessionId == archiveList[i].sessionId) {
	//                             data.duration = parseFloat(data.duration) + parseFloat(archiveList[i].archiveDetail.duration);
	//                             data.cost = parseFloat(data.cost) + parseFloat(archiveList[i].archiveDetail.cost);
	//                             data.count = parseInt(data.count) + 1;
	//                         }
	//                     });
	//                 } else {
	//                     sessionArr.push(archiveList[i].sessionId);
	//                     archiveListArr.push({
	//                         "sessionId": archiveList[i].sessionId,
	//                         "duration": parseFloat(archiveList[i].archiveDetail.duration),
	//                         "cost": parseFloat(archiveList[i].archiveDetail.cost),
	//                         "count": 1
	//                     });

	//                 }

	//             }

	//         }


	//         // For calculating web call list -- 
	//         if (webCallList && webCallList.length > 0) {
	//             let sessionArr = [];
	//             for (let i = 0; i < webCallList.length; i++) {
	//                 if (sessionArr.includes(webCallList[i].sessionId)) {
	//                     // let commonSession = _.filter(archiveListArr, {"sessionId": archiveList[i].sessionId})[0];
	//                     webCallListArr.map((data, index) => {
	//                         if (data.sessionId == webCallList[i].sessionId) {
	//                             data.duration = parseFloat(data.duration) + parseFloat(webCallList[i].webCallDetail.duration);
	//                             data.cost = parseFloat(data.cost) + parseFloat(webCallList[i].webCallDetail.cost);
	//                             data.count = parseInt(data.count) + 1;
	//                         }
	//                     });
	//                 } else {
	//                     sessionArr.push(webCallList[i].sessionId);
	//                     webCallListArr.push({
	//                         "sessionId": webCallList[i].sessionId,
	//                         "duration": parseFloat(webCallList[i].webCallDetail.duration),
	//                         "cost": parseFloat(webCallList[i].webCallDetail.cost),
	//                         "count": 1
	//                     });

	//                 }


	//             }

	//         }


	//         // For calculating  sip call list -- 
	//         if (sipCallList && sipCallList.length > 0) {
	//             let sessionArr = [];
	//             for (let i = 0; i < sipCallList.length; i++) {
	//                 if (sessionArr.includes(sipCallList[i].sessionId)) {
	//                     // let commonSession = _.filter(archiveListArr, {"sessionId": archiveList[i].sessionId})[0];
	//                     sipCallListArr.map((data, index) => {
	//                         if (data.sessionId == sipCallList[i].sessionId) {
	//                             data.duration = parseFloat(data.duration) + parseFloat(sipCallList[i].sipCallDetail.duration);
	//                             data.Cost = parseFloat(data.Cost) + parseFloat(sipCallList[i].sipCallDetail.Cost);
	//                             data.count = parseInt(data.count) + 1;
	//                         }
	//                     });
	//                 } else {
	//                     sessionArr.push(sipCallList[i].sessionId);
	//                     sipCallListArr.push({
	//                         "sessionId": sipCallList[i].sessionId,
	//                         "duration": parseFloat(sipCallList[i].sipCallDetail.duration),
	//                         "Cost": parseFloat(sipCallList[i].sipCallDetail.Cost),
	//                         "count": 1
	//                     });

	//                 }


	//             }

	//         }

	//         resolve({
	//             "archiveListArr": archiveListArr,
	//             "webCallListArr": webCallListArr,
	//             "sipCallListArr": sipCallListArr
	//         });


	//     })

	// }

	// Getting company Details -- 
	getCompanyDetails = (companyId) => {
		return new Promise((resolve, reject) => {

			let dataToSend = {
				"companyId": companyId
			};
			axios
				.post(path + 'user/get-company-by-id', dataToSend)
				.then(serverResponse => {

					let res = serverResponse.data;
					if (!res.isError) {
						console.log("Company Details ==", res);

						let address = res.details && res.details.invoiceBillingAddress;
						let gateway = res.details && res.details.paymentGateway;
						let currency = res.details && res.details.currency;
						let invoiceBillingInfo = res.details && res.details.invoiceBillingInfo;
						let vat = res.details && res.details.vat;

						let returnData = {};
						returnData.address = address;
						returnData.gateway = gateway;
						returnData.currency = currency;
						returnData.invoiceBillingInfo = invoiceBillingInfo;
						returnData.vat = vat;

						console.log("returnData 1 ==", returnData);
						resolve(returnData);
					} else {
						reject();
					}

				})
				.catch(error => {
					console.log(error);
				})

		})
	}

	getMioAdminDetails = (result) => {

		return new Promise((resolve, reject) => {

			let dataToSend = {}

			axios
				.post(path + 'content-management/get-mio-admin-detail', dataToSend)
				.then(serverResponse => {

					let res = serverResponse.data;
					if (!res.isError) {
						let logoUrl = res.details && res.details.fileUrl;
						result.logo = logoUrl;
						resolve(result);
					} else {
						reject();
					}

				})
				.catch(error => {
					console.log(error);
					reject();
				})

		})

	}

	// getting invoice details -- 
	getInvoiceeDetails = (invoiceId) => {
		return new Promise((resolve, reject) => {
			let dataToSend = { "invoiceId": invoiceId };
			let invoice = {};
			axios
				.post(path + 'price-management/fetch-invoice-By-Id', dataToSend)
				.then(serverResponse => {
					let res = serverResponse.data;
					if (!res.isError) {
						let invoiceDetails = res.details[0];
						let services = invoiceDetails.services;

						// Total amount calculation - 
						let total = 0;
						services && services.map(value => {
							total = value.totalPrice && (parseFloat(value.totalPrice) + parseFloat(total)).toFixed(2);
						})
						let issueDate = invoiceDetails && invoiceDetails.date;
						issueDate = this.formatDate(issueDate);
						let dueDate = invoiceDetails && invoiceDetails.dueOn;
						dueDate = this.formatDate(dueDate);
						let vatAmount = 0;
						let vatAddedTotal = 0;
						if (invoiceDetails.vat && invoiceDetails.vat > 0) {
							vatAmount = total * invoiceDetails.vat / 100
							vatAddedTotal = total + vatAmount
						}
						invoice = invoiceDetails;
						invoice.issueDate = issueDate;
						invoice.dueDate = dueDate;
						// invoice.totalAmount = parseFloat(total).toFixed(2);
						// invoice.vatAmount = parseFloat(vatAmount).toFixed(2);
						// invoice.vatAddedTotal = parseFloat(vatAddedTotal).toFixed(2);
						console.log("invoice 2 -- ", invoice);
						resolve(invoice);

					} else {
						reject();
					}

				})

		})
	}

	// For date formatting -- 
	formatDate = (string) => {
		let options = { year: 'numeric', month: 'long', day: 'numeric' };
		return string ? new Date(string).toLocaleDateString([], options) : '';
	}

	openViewInvoice = (index) => {

		let { invoices } = this.state;
		let invoice = invoices[index];
		let { _id, services } = invoice;

		// Getting data for edit modal -- 
		this.setState({
			selectedInvoice: invoice,
			invoiceServices: services,
			isPublished: invoice.isPublished,
			viewInvoice: true
		});

	}

	openUsedServiceDetails = (index) => {

		let { invoices } = this.state;
		let invoice = invoices[index];
		let { _id, services, month, year } = invoice;

		let dataToSend = {
			"companyId": this.state.selectedCompanyId,
			"month": Number(month),
			"year": Number(year)
		};
		// console.log("DataToSend  == ", dataToSend);
		axios
			.post(path + 'price-management/get-monthly-usage-by-companyId', dataToSend)
			.then(serverResponse => {
				let res = serverResponse.data;
				if (!res.isError) {

					console.log("usage details2 -- >", res);
					let usageDetails = res.details;
					this.setState({
						month: month,
						selectedYear: year,
						viewUsageModal: true,
						selectedUsageDetails: usageDetails
					}, () => {
						// console.log("selectedmonth == ",month);
						// console.log("selectedyear === ",year);
					});

				}
			})
			.catch(error => {
				console.log(error);
			})

	}

	closeUsageModal = () => {

		this.setState({
			month: '',
			selectedYear: '',
			viewUsageModal: false,
			selectedUsageDetails: {}
		})

	}

	openCreateInvoice = (event) => {

		let thisYear = (new Date()).getFullYear();

		this.setState({
			thisYear: thisYear,
			selectedYear: thisYear,
			showInvoiceModal: true
		})

	}

	openManualPayment = (_id) => {

		this.setState({
			viewManualPayment: true,
			referenceCode: _id
		});

	}

	clearManualPayment = () => {

		this.setState({
			viewManualPayment: false,
			transactionCurrency: '',
			amount: '',
			referenceCode: '',
			description: ''
		});

	}

	payManually = () => {

		let { selectedInvoice, referenceCode, description } = this.state;

		!referenceCode ? this.setState({ referenceCodeError: "Use a code" }) : this.setState({ referenceCodeError: "" });

		if (selectedInvoice.currency && selectedInvoice.vatAddedTotal) {

			let dataToSend = {
				"description": description,
				"currency": selectedInvoice.currency,
				"amount": selectedInvoice.vatAddedTotal,
				"userId": this.state.userId,
				"companyId": selectedInvoice.companyId,
				"referenceCode": referenceCode
			};
			axios
				.post(path + 'payment/manual-payment', dataToSend)
				.then(serverResponse => {
					let res = serverResponse.data;
					if (!res.isError) {
						console.log("payment result = ", res);
						// this.clearManualPayment();
						this.clearState();
						this.getInvoiceList();
					}else{
						alert(res.message)
					}
				})
				.catch(error => {
					console.log(error);
				})

		}

	}

	viewInvoicePage = () => {

		let { selectedInvoice, selectedCompanyName } = this.state;

		this.props.history.push({
			pathname: '/viewinvoice/' + `${selectedInvoice.companyId}` + '/' + `${selectedInvoice._id}`,
			state: {
				date: selectedInvoice.date,
				company: selectedCompanyName,
				month: selectedInvoice.month,
				year: selectedInvoice.year
			}
		});

	}

	// For clearing state -- 
	clearState = () => {
		this.setState({
			loading: false,
			showInvoiceModal: false,
			viewUser: false,
			month: '',
			selectedYear: '',

			searching: false,
			searchString: '',

			editInvoice: false,
			newServiceModal: false,
			selectedInvoiceId: '',
			selectedInvoice: {},
			invoiceServices: [],
			isPublished: false,

			newServiceName: '',
			newPricePerMinute: '',
			newTotalDuration: '',
			newTotalAmount: '',

			viewInvoice: false,

			viewManualPayment: false,
			transactionCurrency: '',
			amount: '',
			referenceCode: '',
			description: ''

		});
	}

	sideMenuToggle() {
		let { openSideMenu } = this.state;
		openSideMenu = !openSideMenu;
		this.setState({ openSideMenu });
	}

	mobMenuClick() {
		this.sideMenuToggle();
		console.log('mobMenuClick');
	}

	backMenueToggle = () => {
		this.props.history.goBack();
	}

	parseJwt = (token) => {
		if (!token) { return; }
		const base64Url = token.split('.')[1];
		const base64 = base64Url.replace('-', '+').replace('_', '/');
		return JSON.parse(window.atob(base64));
	}


	render() {

		let { invoices, loading, isActive, month, thisYear, selectedYear, selectedInvoiceId, selectedInvoice, invoiceServices,
			newServiceName, newPricePerMinute, newTotalDuration, newTotalAmount,
			transactionCurrency, amount, referenceCode, description, selectedUsageDetails } = this.state;

		let headers = [];
		headers = [
			{ label: "Name", key: 'name' },
			{ label: "Country Code", key: 'countryCode' },
			{ label: "Phone", key: 'phoneNumber' },
			{ label: "Email", key: 'emailAddress' },
			{ label: "User Type", key: 'userType' },
		];

		var options = [];
		for (let i = minOffset; i <= maxOffset; i++) {
			let year = thisYear - i;
			options.push(<option value={year} key={i}>{year}</option>);
		}

		const monthNames = ["January", "February", "March", "April", "May", "June",
			"July", "August", "September", "October", "November", "December"
		];

		return <main>

			<section className="user-mngnt-wrap">
				<div className="container-fluid">
					<div className="row">
						<Navbar routTo="/adminpaymentmanagement" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />

						<div className="col-xl-10 col-lg-9 p-0 mob-full-conf">

							<div className="mobile-menu-header">
								<a href="JavaScript:Void(0);" className="back-arw" onClick={() => this.backMenueToggle()}><i className="fas fa-arrow-left"></i></a>
								{/* <a className="menu-tgl" onClick={() => this.sideMenuToggle()}><img src="images/menu-tgl.png" alt="" /></a> */}
							</div>

							<div className="paymnt-mng-hdr" style={{ margin: "30px" }}>
								<div className="blnc">
									<div className="logobox"><img src="/images/logo-sign.png" alt="" /></div>
								</div>
								<div className="rchrg-btn">
									<p className="text-right" style={{ margin: "0" }}>
										Company Name: {this.state.selectedCompanyName}
										<br />Company Type: {this.state.selectedCompanyType}
									</p>
								</div>
							</div>

							<div className="mng-full-list" style={{ padding: "0px 50px" }}>

								<div className="mng-full-srch">
									<div className="row">

										<div className="col-md-3">
											<div className="add-user-btn">
												<a href="JavaScript:Void(0);"
													className="btn"
													onClick={(event) => this.openCreateInvoice(event)}
												>
													Create an Invoice
                                                </a>
												<ReactTooltip
													effect="float"
													place="top"
													data-border="true"
												/>
											</div>
										</div>
										{loading ?
											<div className="col-md-3">
												<a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
													<i className="fa fa-spinner fa-spin" ></i>
												</a>
											</div>
											: null}
									</div>
								</div>

								<div className="mng-full-table">
									<div className="row">

										<div className="col-md-8">
											<div className="mng-full-table-hdr">
												<div className="row">
													<div className="col-md-4 border-rt">
														<h6>Month</h6>
													</div>
													<div className="col-md-4 border-rt">
														<h6>Amount ({this.state.selectedCompanyCurrency})</h6>
													</div>

													<div className="col-md-4">
														<h6>Payment Status</h6>
													</div>
												</div>
											</div>
										</div>

										<div className="col-md-4">
											<div className="mng-full-table-hdr">
												<div className="row">

													<div className="col-md-6 border-rt">
														<h6>View/Edit</h6>
													</div>
													<div className="col-md-6">
														<h6>Usage</h6>
													</div>

												</div>
											</div>
										</div>

									</div>

									{invoices && invoices.length > 0 && invoices.map((data, index) => {

										return <div className="row" key={index}>

											<div className="col-md-8">
												<div className="mng-full-table-row">
													<div className="row">
														<div className="col-md-4 border-rt">
															<h6>Month</h6>
															{!data.isPublished ?
																<p className="textEllips">
																	<a href="JavaScript:Void(0);" onClick={() => { this.openEditInvoice(index) }}>
																		{monthNames[data.month - 1]} {data.year}
																	</a>
																</p>
																:
																null}
															{data.isPublished ?
																<p className="textEllips">
																	<a href="JavaScript:Void(0);" onClick={() => { this.openViewInvoice(index) }}>
																		{monthNames[data.month - 1]} {data.year}
																	</a>
																</p>
																:
																null}
														</div>
														<div className="col-md-4 border-rt">
															<h6>Amount ({this.state.selectedCompanyCurrency})</h6>
															<p className="textEllips">{data.vatAddedTotal ? (data.vatAddedTotal).toFixed(2) : 0.0}</p>
														</div>

														<div className="col-md-4">
															<h6>Payment Status</h6>
															<p className="textEllips">{data.isPaid ? "Paid" : "Due"}</p>
														</div>

														<div className="mobile-ad-edt-btns">
															{!data.isPublished ?
																<ul>
																	<li onClick={() => this.openEditInvoice(index)}>
																		<a href="JavaScript:Void(0);"
																			data-tip='Edit'
																		>
																			<i className="far fa-edit"></i>
																		</a>
																		<ReactTooltip
																			effect="float"
																			place="top"
																			data-border="true"
																		/>
																	</li>
																</ul>
																:
																<ul>
																	<li onClick={() => this.openViewInvoice(index)}>
																		<a href="JavaScript:Void(0);"
																			data-tip='View'
																		>
																			<i className="fas fa-eye"></i>
																		</a>
																		<ReactTooltip
																			effect="float"
																			place="top"
																			data-border="true"
																		/>
																	</li>
																</ul>
															}

															<ul>
																<li onClick={() => this.openUsedServiceDetails(index)}>
																	<a href="JavaScript:Void(0);"
																		data-tip='View'
																	>
																		<i className="fas fa-eye"></i>
																	</a>
																	<ReactTooltip
																		effect="float"
																		place="top"
																		data-border="true"
																	/>
																</li>
															</ul>

														</div>

													</div>
												</div>

											</div>

											<div className="col-md-4">
												<div className="mng-full-table-row add-edt text-center">
													<div className="row">
														{!data.isPublished ?
															<div className="col-md-6">
																<a href="JavaScript:Void(0);"
																	// ref={ref => this.fooRef2 = ref}
																	data-tip='Edit'
																	onClick={() => { this.openEditInvoice(index); }}
																// onFocus={() => { ReactTooltip.show(this.fooRef2) }}
																>
																	<i className="far fa-edit"></i>
																</a>
																<ReactTooltip
																	effect="float"
																	place="top"
																	data-border="true"
																/>
															</div>
															:
															null}
														{data.isPublished ?
															<div className="col-md-6">
																<a href="JavaScript:Void(0);"
																	// ref={ref => this.fooRef3 = ref}
																	data-tip='View'
																	onClick={() => { this.openViewInvoice(index); }}
																// onFocus={() => { ReactTooltip.show(this.fooRef3) }}
																>
																	<i className="fas fa-eye"></i>
																</a>
																<ReactTooltip
																	effect="float"
																	place="top"
																	data-border="true"
																/>
															</div>
															:
															null}

														<div className="col-md-6">
															<a href="JavaScript:Void(0);"
																// ref={ref => this.fooRef3 = ref}
																data-tip='View'
																onClick={() => { this.openUsedServiceDetails(index); }}
															// onFocus={() => { ReactTooltip.show(this.fooRef3) }}
															>
																<i className="fas fa-eye"></i>
															</a>
															<ReactTooltip
																effect="float"
																place="top"
																data-border="true"
															/>
														</div>

													</div>
												</div>
											</div>

										</div>

									})
									}

								</div>

							</div>

						</div>

					</div>
				</div>

			</section>

			<Modal show={this.state.showInvoiceModal} onHide={() => { this.clearState() }}>
				<Modal.Header closeButton>
					<Modal.Title>Generate new invoice</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<div className="row">

						<div className="col-md-6">
							<div className="form-group">
								<label>For the month of:</label>

								<select
									name="month"
									value={month}
									className="form-control"
									onChange={event => { this.setState({ month: event.target.value }) }}
								>
									<option value="">Select a month</option>
									<option value="1">January</option>
									<option value="2">February</option>
									<option value="3">March</option>
									<option value="4">April</option>
									<option value="5">May</option>
									<option value="6">June</option>
									<option value="7">July</option>
									<option value="8">August</option>
									<option value="9">September</option>
									<option value="10">October</option>
									<option value="11">November</option>
									<option value="12">December</option>

								</select>

								<span style={{ color: 'red' }}>{this.state.monthError ? `* ${this.state.monthError}` : ''}</span>
							</div>
						</div>

						<div className="col-md-6">
							<div className="form-group">
								<label>For the year of:</label>

								<select
									name="month"
									className="form-control"
									value={selectedYear}
									onChange={event => { this.setState({ selectedYear: event.target.value }) }}
								>
									{options}
								</select>

								<span style={{ color: 'red' }}>{this.state.selectedYearError ? `* ${this.state.selectedYearError}` : ''}</span>
							</div>
						</div>

					</div>
				</Modal.Body>
				<Modal.Footer>
					{!loading ?
						<Button variant="primary" onClick={this.generateInvoice}> Save </Button>
						: null}
					{loading ?
						<Button variant="primary"> <i className="fa fa-spinner fa-spin" ></i> </Button>
						: null}

				</Modal.Footer>
			</Modal>

			<Modal show={this.state.editInvoice} onHide={() => { this.clearState() }}>
				<Modal.Header closeButton>
					<Modal.Title>Edit Invoice</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					{this.state.isEditError ?
						<div className="row">
							<div className="col-md-12">
								<div class="alert alert-danger" role="alert" style={{ margin: '10px' }}>
									<strong>Invalid input!</strong>
								</div>
							</div>
						</div>
						:
						null
					}
					<div className="row">

						<div className="col-md-12">
							<div className="form-group">

								<span>Invoice for the month of : {monthNames[selectedInvoice.month - 1]}, {selectedInvoice.year}</span>

							</div>
						</div>
						{invoiceServices.map((service, key) => {
							return <React.Fragment key={key}>
								<div className="col-md-12">
									<div className="form-group">

										<span>Service Name : {service.serviceName}</span>

									</div>
								</div>

								<div className="col-md-4">
									<div className="form-group">
										<label>Pricec per minute:</label>
										<input
											type="text"
											name="pricePerMinute"
											value={service.pricePerMinute}
											className="form-control"
											placeholder="0.00"
											onChange={(event) => { this.changeEditInInvoice(event, key) }}
										// onFocus={() => this.setState({ emailError: "" })}
										/>
										<span style={{ color: 'red' }}>{service.pricePerMinuteError ? `* ${service.pricePerMinuteError}` : ''}</span>
									</div>
								</div>

								<div className="col-md-4">
									<div className="form-group">
										<label>Total Duration:</label>
										<input
											type="text"
											name="totalDuration"
											value={service.totalDuration ? parseFloat(service.totalDuration).toFixed(2) : '0.0'}
											className="form-control"
											// onChange={(event) => {this.changeEditInInvoice(event, key)}}
											// onFocus={() => this.setState({ countryCodeError: "" })}
											readOnly={true}
										/>
										{/* <span style={{ color: 'red' }}>{this.state.countryCodeError ? `* ${this.state.countryCodeError}` : ''}</span> */}
									</div>
								</div>

								<div className="col-md-4">
									<div className="form-group">
										<label>Total Amount:</label>
										<input
											type="text"
											name="totalPrice"
											value={service.totalPrice}
											className="form-control"
											placeholder="0.00"
											onChange={(event) => { this.changeEditInInvoice(event, key) }}
										// onFocus={() => this.setState({ mobileError: "" })}
										/>
										<span style={{ color: 'red' }}>{service.totalAmountError ? `* ${service.totalAmountError}` : ''}</span>
									</div>
								</div>
							</React.Fragment>

						})}

						<div className="col-md-6">
							<div className="form-group">

								<Button variant="primary" style={{ backgroundColor: '#0062cc' }}
									onClick={() => { this.setState({ newServiceModal: true }) }}
								>
									Add Service
                                </Button>


							</div>
						</div>

						<div className="col-md-6">
							<div className="form-group">
								{!this.state.isPublished ?
									<Button variant="success" style={{ backgroundColor: '#28a745' }}
										onClick={() => { this.setState({ isPublished: true }) }}
									>
										Publish
                                    </Button>
									:
									<Button variant="secondary" style={{ backgroundColor: '#6c757d' }}
										onClick={() => { this.setState({ isPublished: false }) }}
									>
										Undo
                                    </Button>
								}
							</div>
						</div>

					</div>
				</Modal.Body>
				<Modal.Footer>
					{!loading ?
						<Button variant="primary" onClick={() => this.editInvoice()}>
							Save
                        </Button>
						: null}
					{loading ?
						<Button variant="primary"> <i className="fa fa-spinner fa-spin" ></i> </Button>
						: null}
				</Modal.Footer>
			</Modal>

			<Modal show={this.state.newServiceModal} onHide={() => { this.clearAddService() }}>
				<Modal.Header closeButton>
					<Modal.Title>Add New Service</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<div className="row">

						<div className="col-md-12">
							<div className="form-group">
								<label>Name of Service:</label>
								<input
									type="text"
									name="newServiceName"
									value={newServiceName}
									className="form-control"
									placeholder="Enter service name"
									onChange={(event) => { this.newServiceSetState('newServiceName', event.target.value) }}
									onFocus={() => this.setState({ newServiceNameError: "" })}
								/>
								<span style={{ color: 'red' }}>{this.state.newServiceNameError ? `* ${this.state.newServiceNameError}` : ''}</span>
							</div>
						</div>

						<div className="col-md-4">
							<div className="form-group">
								<label>Price per minute:</label>
								<input
									type="text"
									name="newPricePerMinute"
									value={newPricePerMinute}
									className="form-control"
									placeholder="0.00"
									onChange={(event) => { this.newServiceSetState('newPricePerMinute', event.target.value) }}
									onFocus={() => this.setState({ newPricePerMinuteError: "" })}
								/>
								<span style={{ color: 'red' }}>{this.state.newPricePerMinuteError ? `* ${this.state.newPricePerMinuteError}` : ''}</span>
							</div>
						</div>

						<div className="col-md-4">
							<div className="form-group">
								<label>Total Duration:</label>
								<input
									type="text"
									name="newTotalDuration"
									value={newTotalDuration}
									className="form-control"
									placeholder="0.00"
									onChange={(event) => { this.newServiceSetState('newTotalDuration', event.target.value) }}
									onFocus={() => this.setState({ newTotalDurationError: "" })}
								/>
								<span style={{ color: 'red' }}>{this.state.newTotalDurationError ? `* ${this.state.newTotalDurationError}` : ''}</span>
							</div>
						</div>

						<div className="col-md-4">
							<div className="form-group">
								<label>Total Amount:</label>
								<input
									type="text"
									name="newTotalAmount"
									value={newTotalAmount}
									className="form-control"
									placeholder="0.00"
									onChange={(event) => { this.newServiceSetState('newTotalAmount', event.target.value) }}
									onFocus={() => this.setState({ newTotalAmountError: "" })}
								/>
								<span style={{ color: 'red' }}>{this.state.newTotalAmountError ? `* ${this.state.newTotalAmountError}` : ''}</span>
							</div>
						</div>

					</div>
				</Modal.Body>
				<Modal.Footer>
					{!loading ?
						<Button variant="primary" onClick={() => this.addNewService()}>
							Save
                        </Button>
						: null}
					{loading ?
						<Button variant="primary"> <i className="fa fa-spinner fa-spin" ></i> </Button>
						: null}
				</Modal.Footer>
			</Modal>

			<Modal show={this.state.viewInvoice} onHide={() => { this.clearState() }}>
				<Modal.Header closeButton>
					<Modal.Title>View Invoice</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<div className="row">

						<div className="col-md-12">
							<div className="form-group">

								<span>Invoice for the month of : {monthNames[selectedInvoice.month - 1]}, {selectedInvoice.year}</span>

							</div>
						</div>
						{invoiceServices.map((service, key) => {
							return <React.Fragment key={key}>
								<div className="col-md-12">
									<div className="form-group">

										<span>Service Name : {service.serviceName}</span>

									</div>
								</div>

								<div className="col-md-4">
									<div className="form-group">
										<label>Pricec per minute:</label>
										<input
											type="text"
											name="pricePerMinute"
											value={service.pricePerMinute}
											className="form-control"
											placeholder="0.00"

											readOnly={true}
										/>

									</div>
								</div>

								<div className="col-md-4">
									<div className="form-group">
										<label>Total Duration:</label>
										<input
											type="text"
											name="totalDuration"
											value={service.totalDuration ? parseFloat(service.totalDuration).toFixed(2) : 0}
											className="form-control"

											readOnly={true}
										/>

									</div>
								</div>

								<div className="col-md-4">
									<div className="form-group">
										<label>Total Amount:</label>
										<input
											type="text"
											name="totalAmount"
											value={service.totalPrice ? parseFloat(service.totalPrice).toFixed(2) : 0}
											className="form-control"
											placeholder="0.00"

											readOnly={true}
										/>

									</div>
								</div>
							</React.Fragment>

						})}

						{selectedInvoice.isPublished ?
							<div className="col-md-6">
								<div className="form-group">

									<span>Status: Published</span>

								</div>
							</div>
							: null}

						{!selectedInvoice.isPaid ?
							<div className="col-md-6">
								<div className="form-group">

									<Button variant="success"
										onClick={() => { this.openManualPayment(selectedInvoice._id) }}
										style={{ backgroundColor: '#28a745' }} >
										Manual Payment
                                    </Button>

								</div>
							</div>
							: null}

					</div>
				</Modal.Body>
				<Modal.Footer>
					{selectedInvoice.isPublished ?
						<Button variant="primary" onClick={() => this.viewInvoicePage()}>
							View Invoice
                        </Button>
						: null}
					<Button variant="primary" onClick={() => this.clearState()}>
						Cancel
                    </Button>

				</Modal.Footer>
			</Modal>

			<Modal show={this.state.viewManualPayment} onHide={() => { this.clearManualPayment() }}>
				<Modal.Header closeButton>
					<Modal.Title>Manual Payment</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<div className="row">

						{/* <div className="col-md-6">
							<div className="form-group">
								<label>Currency:</label>
								<select
									name="transactionCurrency"
									value={transactionCurrency}
									className="form-control"
									onChange={(event) => { this.setState({ transactionCurrency: event.target.value }) }}
									onFocus={() => { this.setState({ transactionCurrencyError: '' }) }}
								>
									<option value="">Select Currency</option>
									<option value={cred.CURRENCY.usd}>{cred.CURRENCY.usd}</option>
									<option value={cred.CURRENCY.gbp}>{cred.CURRENCY.gbp}</option>
									<option value={cred.CURRENCY.naira}>{cred.CURRENCY.naira}</option>
									<option value={cred.CURRENCY.euro}>{cred.CURRENCY.euro}</option>
								</select>
								<span style={{ color: 'red' }}>{this.state.transactionCurrencyError ? `* ${this.state.transactionCurrencyError}` : ''}</span>
							</div>
						</div> */}

						<div className="col-md-6">
							<div className="form-group">
								<label>Amount:</label>
								<input
									type="text"
									name="amount"
									value={selectedInvoice.vatAddedTotal ? (selectedInvoice.vatAddedTotal).toFixed(2) : 0.0}
									className="form-control"
									placeholder="0.00"
									readOnly={true}
								/>
							</div>
						</div>

						<div className="col-md-12">
							<div className="form-group">
								<label>Reference Code:</label>
								<input
									type="text"
									name="referenceCode"
									value={referenceCode}
									className="form-control"
									placeholder="Enter transaction code"
									onChange={(event) => { this.setState({ referenceCode: event.target.value }) }}
									readOnly={true}
								/>

							</div>
						</div>

						<div className="col-md-12">
							<div className="form-group">
								<label>Description:</label>
								<textarea
									rows="4" cols="50" style={{ height: "85px" }}
									name="description"
									value={description}
									className="form-control"
									placeholder="Enter Description"
									onChange={(event) => { this.setState({ description: event.target.value }) }}
									onFocus={() => { this.setState({ descriptionError: '' }) }}
								>
									{description}
								</textarea>
								{/* <span style={{ color: 'red' }}>{this.state.descriptionError ? `* ${this.state.descriptionError}` : ''}</span> */}
							</div>
						</div>

					</div>
				</Modal.Body>
				<Modal.Footer>
					<Button variant="primary" onClick={() => this.payManually()}>
						Save
                    </Button>
				</Modal.Footer>
			</Modal>

			<Modal show={this.state.viewUsageModal} size="lg" onHide={() => { this.closeUsageModal() }}>
				<Modal.Header closeButton>
					<Modal.Title>Usage Details</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<div className="row">

						<div className="col-md-12">
							<div className="form-group">
								<span>Invoice of : {monthNames[month - 1]}, {selectedYear}</span>
							</div>
						</div>

						<div className="col-md-12">
							<div className="form-group">
								<span>Archive List : </span>
							</div>
						</div>

						<div className="col-md-12">
							<Table striped bordered hover size="sm">
								<thead>
									<tr>
										<th>Archive Name</th>
										<th style={{ textAlign: "right" }}>Duration</th>
										<th style={{ textAlign: "right" }}>Cost</th>
									</tr>
								</thead>
								<tbody>
									{selectedUsageDetails && selectedUsageDetails.descriptionOfArchives && selectedUsageDetails.descriptionOfArchives.ArchiveList && selectedUsageDetails.descriptionOfArchives.ArchiveList.map((value, index) => {
										return <tr key={index}>
											<td>{value.name}</td>
											<td style={{ textAlign: "right" }}>{value.duration}</td>
											<td style={{ textAlign: "right" }}>{value.price}</td>
										</tr>
									})}
								</tbody>
								<tfoot>
									<tr>
										<td>Total Archive: {selectedUsageDetails.descriptionOfArchives && selectedUsageDetails.descriptionOfArchives.totalArchive}</td>
										<td style={{ textAlign: "right" }}>Total Duration: {selectedUsageDetails.descriptionOfArchives && selectedUsageDetails.descriptionOfArchives.totalDuration && parseFloat(selectedUsageDetails.descriptionOfArchives.totalDuration).toFixed(2)}</td>
										<td style={{ textAlign: "right" }}>Total Cost: {selectedUsageDetails.descriptionOfArchives && selectedUsageDetails.descriptionOfArchives.totalCost && parseFloat(selectedUsageDetails.descriptionOfArchives.totalCost).toFixed(2)}</td>
									</tr>
								</tfoot>

							</Table>
						</div>

						<div className="col-md-12">
							<div className="form-group">
								<span>Web Call : </span>
							</div>
						</div>

						<div className="col-md-12">
							<Table striped bordered hover size="sm">
								<thead>
									<tr>
										<th>Web Call Name</th>
										<th style={{ textAlign: "right" }}>Duration</th>
										<th style={{ textAlign: "right" }}>Cost</th>
									</tr>
								</thead>
								<tbody>
									{selectedUsageDetails && selectedUsageDetails.descriptionOfWebCall && selectedUsageDetails.descriptionOfWebCall.webCallList && selectedUsageDetails.descriptionOfWebCall.webCallList.map((value, index) => {
										return <tr key={index}>
											<td>{value.conferenceTitle}</td>
											<td style={{ textAlign: "right" }}>{value && value.duration && parseFloat(value.duration).toFixed(2)}</td>
											<td style={{ textAlign: "right" }}>{value && value.totalCost && parseFloat(value.totalCost).toFixed(2)}</td>
										</tr>
									})}
								</tbody>
								<tfoot>
									<tr>
										<td>Total Web Call: {selectedUsageDetails.descriptionOfWebCall && selectedUsageDetails.descriptionOfWebCall.totalWebCall}</td>
										<td style={{ textAlign: "right" }}>Total Duration: {selectedUsageDetails && selectedUsageDetails.descriptionOfWebCall && selectedUsageDetails.descriptionOfWebCall.totalDuration && parseFloat(selectedUsageDetails.descriptionOfWebCall.totalDuration).toFixed(2)}</td>
										<td style={{ textAlign: "right" }}>Total Cost: {selectedUsageDetails && selectedUsageDetails.descriptionOfWebCall && selectedUsageDetails.descriptionOfWebCall.totalCost && parseFloat(selectedUsageDetails.descriptionOfWebCall.totalCost).toFixed(2)}</td>
									</tr>
								</tfoot>

							</Table>
						</div>

						<div className="col-md-12">
							<div className="form-group">
								<span>Dial Out : </span>
							</div>
						</div>

						<div className="col-md-12">
							<Table striped bordered hover size="sm">
								<thead>
									<tr>
										<th>Name</th>
										<th style={{ textAlign: "right" }}>Duration</th>
										<th style={{ textAlign: "right" }}>Cost</th>
									</tr>
								</thead>
								<tbody>
									{selectedUsageDetails && selectedUsageDetails.descriptionOfSipCall && selectedUsageDetails.descriptionOfSipCall.sipCallList && selectedUsageDetails.descriptionOfSipCall.sipCallList.map((value, index) => {
										return <tr key={index}>
											<td>{value.conferenceTitle}</td>
											<td style={{ textAlign: "right" }}>{value && value.duration && parseFloat(value.duration).toFixed(2)}</td>
											<td style={{ textAlign: "right" }}>{value && value.totalCost && parseFloat(value.totalCost).toFixed(2)}</td>
										</tr>
									})}
								</tbody>
								<tfoot>
									<tr>
										<td>Total Dial Out: {selectedUsageDetails.descriptionOfSipCall && selectedUsageDetails.descriptionOfSipCall.totalSipCall}</td>
										<td style={{ textAlign: "right" }}>Total Duration: {selectedUsageDetails && selectedUsageDetails.descriptionOfSipCall && selectedUsageDetails.descriptionOfSipCall.totalDuration && parseFloat(selectedUsageDetails.descriptionOfSipCall.totalDuration).toFixed(2)}</td>
										<td style={{ textAlign: "right" }}>Total Cost: {selectedUsageDetails && selectedUsageDetails.descriptionOfSipCall && selectedUsageDetails.descriptionOfSipCall.totalCost && parseFloat(selectedUsageDetails.descriptionOfSipCall.totalCost).toFixed(2)}</td>
									</tr>
								</tfoot>

							</Table>

						</div>

						<div className="col-md-12">
							<div className="form-group">
								<span>Dial In : </span>
							</div>
						</div>

						<div className="col-md-12">
							<Table striped bordered hover size="sm">
								<thead>
									<tr>
										<th>Name</th>
										<th style={{ textAlign: "right" }}>Duration</th>
										<th style={{ textAlign: "right" }}>Cost</th>
									</tr>
								</thead>
								<tbody>
									{selectedUsageDetails && selectedUsageDetails.descriptionOfSipInbound && selectedUsageDetails.descriptionOfSipInbound.sipInboundList && selectedUsageDetails.descriptionOfSipInbound.sipInboundList.map((value, index) => {
										return <tr key={index}>
											<td>{value.conferenceTitle}</td>
											<td style={{ textAlign: "right" }}>{value && value.duration && parseFloat(value.duration).toFixed(2)}</td>
											<td style={{ textAlign: "right" }}>{value && value.totalCost && parseFloat(value.totalCost).toFixed(2)}</td>
										</tr>
									})}
								</tbody>
								<tfoot>
									<tr>
										<td>Total Dial In: {selectedUsageDetails.descriptionOfSipInbound && selectedUsageDetails.descriptionOfSipInbound.totalSipInbound}</td>
										<td style={{ textAlign: "right" }}>Total Duration: {selectedUsageDetails && selectedUsageDetails.descriptionOfSipInbound && selectedUsageDetails.descriptionOfSipInbound.totalDuration && parseFloat(selectedUsageDetails.descriptionOfSipInbound.totalDuration).toFixed(2)}</td>
										<td style={{ textAlign: "right" }}>Total Cost: {selectedUsageDetails && selectedUsageDetails.descriptionOfSipInbound && selectedUsageDetails.descriptionOfSipInbound.totalCost && parseFloat(selectedUsageDetails.descriptionOfSipInbound.totalCost).toFixed(2)}</td>
									</tr>
								</tfoot>

							</Table>

						</div>

					</div>
				</Modal.Body>
				<Modal.Footer>
					<Button variant="primary" onClick={() => this.closeUsageModal()}>
						Cancel
                    </Button>
				</Modal.Footer>
			</Modal>

			<Modal show={this.state.showSuccessAlert} onHide={() => this.hideSuccessAlert()}>
				<Modal.Header closeButton>
					<Modal.Title>OK</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<h5 style={{ color: 'green' }}>{`${this.state.successAlertMessage}`}</h5>
				</Modal.Body>
				<Modal.Footer>
					<Button variant="primary" onClick={() => this.hideSuccessAlert()}>
						Ok
            </Button>
				</Modal.Footer>
			</Modal>

			<Modal show={this.state.showErrorAlert} onHide={() => this.hideErrorAlert()}>
				<Modal.Header closeButton>
					<Modal.Title>OK</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<h5 style={{ color: 'red' }}>{`${this.state.errorAlertMessage}`}</h5>
				</Modal.Body>
				<Modal.Footer>
					<Button variant="primary" onClick={() => this.hideErrorAlert()}>
						Ok
                    </Button>
				</Modal.Footer>
			</Modal>

		</main>
	}

}

export default checkAuthentication(AdminInvoiceManagement);