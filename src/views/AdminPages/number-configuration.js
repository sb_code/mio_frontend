import React, { Component } from 'react'
import Navbar from '../Component/Navbar'
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button } from "react-bootstrap"
import _ from 'lodash';
import { CSVLink, CSVDownload } from "react-csv";
import ReactTooltip from 'react-tooltip'; // For tool tip 

import { checkAuthentication } from '../Component/Authentication';

var path = cred.API_PATH;


export class AdminUserManagement extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userId: '',
            userType: '',

            openSideMenu: false,
            searchCountry: '',
            searchProvider: '',
            searching: false,

            loading: false,

            pageNo: 1,
            usersList: [],
            numberListRecords: '',

            SIPProviderList: [],
            countryList: [],

            selectedNumber: {},
            countryCodeList: '',

            showEditModal: false,
            countryCode: '',
            countryCodeError: '',
            phoneNumber: '',
            phoneNumberError: '',
            sipProvider: '',
            sipProviderError: '',
            country: '',
            countryError: '',
            costPerMinute: '',
            costPerMinuteError: '',
            isActive: false,

            showSuccessAlert: false,
            successAlertMessage: '',
            showErrorAlert: false,
            errorAlertMessage: '',


        }
        this.perPage = 5;
    }

    componentDidMount() {

        document.body.classList.remove("sign-bg");
        document.body.classList.add("grey-bg");

        let storage = localStorage.getItem("MIO_Local");
        storage = storage ? JSON.parse(storage) : null;
        if (storage && storage.data.userId) {
            this.setState({
                userId: storage.data.userId,
                userType: storage.data.userType,
                companyId: storage.data.companyId
            }, () => {
                this.getNumberListForAdmin();
                this.getCountrylist();
                this.getSIPProvidersList();
                this.getCountryCodeList();
                // console.log('user management--', this.state.userType);
            });
            let tokenData = this.parseJwt(storage.token);
        }

    }

    // For getting number list -- 
    getNumberListForAdmin = (sipProvider, country) => {
        let dataToSend;
        if (sipProvider || country) {
            dataToSend = {
                pageno: this.state.pageNo,
                perPage: this.perPage,
                sipProvider: sipProvider,
                country: country
            };

        } else {
            dataToSend = {
                pageno: this.state.pageNo,
                perPage: this.perPage,
                sipProvider: '',
                country: ''
            };
        }
        this.setState({
            loading: true
        });

        axios
            .post(path + 'services/get-inbound-numbers', dataToSend)
            .then(serverResponse => {
                let res = serverResponse.data;
                console.log("Number list == ", res);
                if (!res.isError) {
                    let details = res.details;
                    let results = details && details[0].results;
                    let numberListRecords = details && details[0].total;

                    this.setState({
                        loading: false,
                        usersList: results,
                        numberListRecords: numberListRecords
                    });
                } else {
                    this.setState({ loading: false });
                    console.log('something goes wrong.');
                }
            })
            .catch(error => {
                console.log(error);
            })

    }

    // For getting country list for filter -- 
    getCountrylist = () => {
        let dataToSend = {
            "pageno": '',
            "perPage": '',
        };

        axios
            .post(path + 'services/getCountries', dataToSend)
            .then(serverResponse => {

                let res = serverResponse.data;
                if (!res.isError) {
                    console.log("country list ==", res);
                    this.setState({
                        countryList: res.details
                    });
                }

            })
            .catch(error => {
                console.log(error);
            })

    }

    // For getting SIP Provider lists --
    getSIPProvidersList = () => {
        let dataToSend = {
            "pageno": '',
            "perPage": '',
        };

        axios
            .post(path + 'services/get-sip-providers', dataToSend)
            .then(serverResponse => {

                let res = serverResponse.data;
                if (!res.isError) {

                    console.log("country list ==", res);
                    this.setState({
                        SIPProviderList: res.details
                    });

                }

            })
            .catch(error => {
                console.log(error);
            })

    }

    getCountryCodeList = () => {

        axios
            .post(path + 'services/get-country-codes')
            .then(serverResponse => {

                let res = serverResponse.data;
                if (!res.isError) {
                    this.setState({
                        countryCodeList: res.details
                    });
                }

            })
            .catch(error => {
                console.log(error);
            })

    }

    // For searching user - 
    searchNumber = (event) => {
        event.preventDefault();
        let { searchCountry, searchProvider } = this.state;
        this.setState({
            searching: true
        }, () => {
            this.getNumberListForAdmin(searchProvider, searchCountry);
        });

    }

    // For clearing search -
    clearSearch = (event) => {
        event.preventDefault();
        // let {searchCountry, searchProvider, searching} = this.state;
        this.setState({
            searchCountry: '',
            searchProvider: '',
            searching: false
        }, () => {
            this.getNumberListForAdmin()
        });
    }

    // For userList of a particular company - 
    editNumber = (index) => {

        let { usersList } = this.state;
        let selectedNumber = usersList[index];

        this.setState({
            selectedNumber: selectedNumber,
            isActive: selectedNumber.inboundNumbers.isActive,
            countryCode: selectedNumber.inboundNumbers.countryCode,
            phoneNumber: selectedNumber.inboundNumbers.phoneNumber,
            sipProvider: selectedNumber.inboundNumbers.sipProvider,
            numberType: selectedNumber.inboundNumbers.numberType,
            country: selectedNumber.inboundNumbers.country,
            costPerMinute: selectedNumber.inboundNumbers.costPerMinute,
            showEditModal: true
        });

    }

    UpdateNumber = () => {

        let { countryCode, phoneNumber, sipProvider, numberType, country,
            costPerMinute, isActive, loading, selectedNumber } = this.state;

        countryCode && !isNaN(countryCode) ? this.setState({ countryCodeError: '' }) : this.setState({ countryCodeError: '' })
        phoneNumber && !isNaN(phoneNumber) ? this.setState({ phoneNumberError: '' }) : this.setState({ phoneNumberError: '' })
        sipProvider ? this.setState({ sipProviderError: '' }) : this.setState({ sipProviderError: '' })
        country ? this.setState({ countryError: '' }) : this.setState({ countryError: '' })

        if (countryCode && !isNaN(countryCode) && phoneNumber && !isNaN(phoneNumber) && sipProvider && country) {
            this.setState({
                loading: true
            });
            let dataToSend = {
                "numberId": selectedNumber.inboundNumbers._id,
                // "compnayId": this.state.companyId,
                "countryCode": countryCode,
                "phoneNumber": phoneNumber,
                "phone": countryCode + phoneNumber,
                "sipProvider": sipProvider,
                "numberType": numberType,
                "country": country,
                "costPerMinute": costPerMinute,
                "isActive": isActive,
            };
            // API -- 
            axios
                .post(path + 'services/update-inbound-number', dataToSend)
                .then(serverResponse => {

                    let res = serverResponse.data;
                    if (!res.isError) {

                        this.setState({
                            loading: false,
                            showSuccessAlert: true,
                            successAlertMessage: 'Sccessfully updated.',
                        }, () => {
                            this.hideEditModal();
                            this.getNumberListForAdmin();
                        });

                    } else {
                        this.setState({
                            loading: false,
                            showErrorAlert: true,
                            errorAlertMessage: 'Sorry! Something is wrong.',
                        }, () => {
                            this.hideEditModal();
                        });
                    }

                })
                .catch(error => {
                    console.log(error);
                })

        }

    }

    hideEditModal = () => {

        this.setState({
            showEditModal: false,
            countryCode: '',
            countryCodeError: '',
            phoneNumber: '',
            phoneNumberError: '',
            sipProvider: '',
            sipProviderError: '',
            country: '',
            countryError: '',
            costPerMinute: '',
            costPerMinuteError: '',
            isActive: false,
        });

    }

    deleteNumber = (index) => {

        let { usersList } = this.state;
        let selectedNumber = usersList[index];

        let dataToSend = {
            "numberId": selectedNumber.inboundNumbers._id
        };
        // API -- 
        axios
            .post(path + 'services/delete-inbound-number', dataToSend)
            .then(serverResponse => {

                let res = serverResponse.data;
                if (!res.isError) {

                    this.setState({
                        loading: false,
                        showSuccessAlert: true,
                        successAlertMessage: 'Succefully deleted.',
                    }, () => {
                        this.getNumberListForAdmin();
                    });

                } else {
                    this.setState({
                        loading: false,
                        showErrorAlert: true,
                        errorAlertMessage: 'Sorry! Something is wrong.',
                    });
                }

            })
            .catch(error => {
                console.log(error);
            })

    }

    pageChangePrev = () => {

        this.setState({
            pageNo: (parseFloat(this.state.pageNo) - 1)
        }, () => {
            this.getNumberListForAdmin()
        });

    }

    pageChangeNext = () => {

        this.setState({
            pageNo: (parseFloat(this.state.pageNo) + 1)
        }, () => {
            this.getNumberListForAdmin()
        });

    }

    clearAlert = () => {
        this.setState({
            loading: false,
            showSuccessAlert: false,
            successAlertMessage: '',
            showErrorAlert: false,
            errorAlertMessage: '',
        });
    }

    sideMenuToggle = () => {
        let { openSideMenu } = this.state;
        openSideMenu = !openSideMenu;
        this.setState({ openSideMenu });
    }

    backMenueToggle = () => {
        this.props.history.goBack();
    }

    mobMenuClick() {
        this.sideMenuToggle();
        console.log('mobMenuClick');
    }

    parseJwt = (token) => {
        if (!token) { return; }
        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace('-', '+').replace('_', '/');
        return JSON.parse(window.atob(base64));
    }


    render() {

        let { loading, selectedNumber, usersList, pageNo, numberListRecords, SIPProviderList, countryList, countryCodeList } = this.state;
        const perPage = this.perPage;

        return <main>
            <section className="user-mngnt-wrap">
                <div className="container-fluid">
                    <div className="row">

                        <Navbar routTo="/numberconfigue" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />
                        <div className="col-xl-10 col-lg-9 p-0 mob-full-conf">

                            <div className="mobile-menu-header">
                                <a href="JavaScript:Void(0);" className="back-arw" onClick={() => this.backMenueToggle()}><i className="fas fa-arrow-left"></i></a>
                                {/* <a href="JavaScript:Void(0);" className="menu-tgl" onClick={() => this.sideMenuToggle()}><img src="images/menu-tgl.png" alt="" /></a> */}
                            </div>

                            <div className="mng-full-list">

                                <div className="mng-full-srch">
                                    <div className="row">
                                        <div className="col-margin col-md-4">
                                            <select className="form-control"
                                                name="searchType"
                                                value={this.state.searchCountry}
                                                onChange={(event) => { this.setState({ searchCountry: event.target.value }) }}
                                            >
                                                <option value="">Select Country</option>
                                                {countryList.map((value, index) => {
                                                    return <option value={value._id.country} key={index}>
                                                        {value._id.country}
                                                    </option>
                                                })}
                                            </select>
                                        </div>
                                        <div className="col-margin col-md-6">
                                            <select className="form-control"
                                                name="searchType"
                                                value={this.state.searchProvider}
                                                onChange={(event) => { this.setState({ searchProvider: event.target.value }) }}
                                            >
                                                <option value="">Select SIP Provider</option>
                                                {SIPProviderList.map((value, index) => {
                                                    return <option value={value._id.sipProvider} key={index}>
                                                        {value._id.sipProvider}
                                                    </option>
                                                })}
                                            </select>
                                        </div>
                                        <div className="col col-md-2">
                                            <div className="srch-wrap">
                                                <form>
                                                    {this.state.searching ?
                                                        <input type="button"
                                                            data-tip='Clear'
                                                            onClick={(event) => this.clearSearch(event)}
                                                        />
                                                        :
                                                        <input type="submit"
                                                            data-tip='Search'
                                                            onClick={(event) => this.searchNumber(event)}
                                                        />
                                                    }
                                                    <ReactTooltip
                                                        effect="float"
                                                        place="top"
                                                        data-border="true"
                                                    />
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                {this.state.usersList && this.state.usersList.length > 0 ?
                                    <div className="mng-full-table">
                                        <div className="row">

                                            <div className="col-md-8">
                                                <div className="mng-full-table-hdr">
                                                    <div className="row">
                                                        <div className="col-md-3 border-rt">
                                                            <h6>Phone Number</h6>
                                                        </div>
                                                        <div className="col-md-3 border-rt">
                                                            <h6>Country</h6>
                                                        </div>
                                                        <div className="col-md-3 border-rt">
                                                            <h6>SIP Provider</h6>
                                                        </div>
                                                        <div className="col-md-3">
                                                            <h6>Cost(/min)</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="col-md-4">
                                                <div className="mng-full-table-hdr">
                                                    <div className="row">
                                                        <div className="col-md-6">
                                                            <h6>Edit</h6>
                                                        </div>
                                                        <div className="col-md-6">
                                                            <h6>Delete</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        {this.state.usersList && this.state.usersList.length > 0 && this.state.usersList.map((user, index) => {
                                            return <div className="row" key={index}>

                                                <div className="col-md-8">
                                                    <div className="mng-full-table-row">
                                                        <div className="row">
                                                            <div className="col-md-3 border-rt">
                                                                <h6>Number</h6>
                                                                <p className="textEllips">
                                                                    <a href="JavaScript:Void(0);" onClick={() => this.editNumber(index)}>
                                                                        {user.inboundNumbers.countryCode + '-' + user.inboundNumbers.phoneNumber}
                                                                    </a>
                                                                </p>
                                                            </div>
                                                            <div className="col-md-3 border-rt">
                                                                <h6>Country</h6>
                                                                <p className="textEllips">{user.inboundNumbers.country}</p>
                                                            </div>
                                                            <div className="col-md-3 border-rt">
                                                                <h6>SIP Provider</h6>
                                                                <p className="textEllips">
                                                                    {user.inboundNumbers.sipProvider}
                                                                </p>
                                                            </div>
                                                            <div className="col-md-3">
                                                                <h6>Cost(/min)</h6>
                                                                <p className="textEllips">
                                                                    {parseFloat(user.inboundNumbers.costPerMinute).toFixed('2')}
                                                                </p>
                                                            </div>

                                                            <div className="mobile-ad-edt-btns">
                                                                <ul>

                                                                    <li onClick={() => this.editNumber(index)}>
                                                                        <a href="JavaScript:Void(0);"
                                                                            // ref={ref => this.fooRef = ref}
                                                                            data-tip='View'
                                                                        // onFocus={() => { ReactTooltip.show(this.fooRef) }}
                                                                        >
                                                                            <i className="far fa-edit"></i>
                                                                        </a>
                                                                        <ReactTooltip
                                                                            effect="float"
                                                                            place="top"
                                                                            data-border="true"
                                                                        />
                                                                    </li>
                                                                    <li onClick={() => this.deleteNumber(index)}>
                                                                        <a href="JavaScript:Void(0);"
                                                                            // ref={ref => this.fooRef = ref}
                                                                            data-tip='View'
                                                                        // onFocus={() => { ReactTooltip.show(this.fooRef) }}
                                                                        >
                                                                            <i className="far fa-trash-alt"></i>
                                                                        </a>
                                                                        <ReactTooltip
                                                                            effect="float"
                                                                            place="top"
                                                                            data-border="true"
                                                                        />
                                                                    </li>

                                                                </ul>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>

                                                <div className="col-md-4">
                                                    <div className="mng-full-table-row add-edt text-center">
                                                        <div className="row">

                                                            <div className="col-md-6">
                                                                <a href="JavaScript:Void(0);"
                                                                    data-tip='View'
                                                                    onClick={() => { this.editNumber(index) }}
                                                                >
                                                                    <i className="far fa-edit"></i>

                                                                </a>
                                                                <ReactTooltip
                                                                    effect="float"
                                                                    place="top"
                                                                    data-border="true"
                                                                />
                                                            </div>
                                                            <div className="col-md-6">
                                                                <a href="JavaScript:Void(0);"
                                                                    data-tip='View'
                                                                    onClick={() => { this.deleteNumber(index) }}
                                                                >
                                                                    <i className="far fa-trash-alt"></i>

                                                                </a>
                                                                <ReactTooltip
                                                                    effect="float"
                                                                    place="top"
                                                                    data-border="true"
                                                                />
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        })}

                                    </div>
                                    :
                                    <div className="mng-full-table">
                                        <div className="row">
                                            <div className="col-md-12">
                                                <div className="mng-full-table-hdr admn-usr-hdr">
                                                    <div className="row">
                                                        <div className="col-md-12">
                                                            <h6>No Company Found.</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                }
                                <br /><br />
                                <div className="form-group">
                                    {parseFloat(pageNo) > 1 ?
                                        <span style={{ color: "#007bff", cursor: "pointer" }}
                                            onClick={() => { this.pageChangePrev() }}
                                        >
                                            <b>{"<< Prev"}</b>
                                        </span>
                                        :
                                        null}
                                    &nbsp;&nbsp;|&nbsp;&nbsp;
                                    {((parseFloat(pageNo) * parseFloat(perPage)) < parseFloat(numberListRecords)) ?
                                        <span style={{ color: "#007bff", cursor: "pointer" }}
                                            onClick={() => { this.pageChangeNext() }}
                                        >
                                            <b>{"Next >>"}</b>
                                        </span>
                                        :
                                        null}
                                    {loading ?
                                        <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                                            <i className="fa fa-spinner fa-spin" ></i>
                                        </a>
                                        :
                                        null}

                                </div>

                            </div>

                        </div>
                    </div>
                </div>

            </section>

            <Modal show={this.state.showEditModal} onHide={() => { this.hideEditModal() }}>
                <Modal.Header closeButton>
                    <Modal.Title>Edit Number</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="row">

                        <div className="col-md-4">
                            <div className="form-group">
                                <label>Country Code:</label>
                                <select
                                    className="form-control"
                                    value={this.state.countryCode}
                                    onChange={(e) => { this.setState({ countryCode: e.target.value }) }}
                                    onFocus={() => this.setState({ countryCodeError: "" })}
                                >
                                    <option value="">Select country code</option>
                                    {countryCodeList && countryCodeList.map((data, index) => {
                                        return <option value={data.code}>{data.code + ' ' + data.name}</option>
                                    })}
                                </select>
                                <span style={{ color: 'red' }}>{this.state.countryCodeError ? `* ${this.state.countryCodeError}` : ''}</span>
                            </div>
                        </div>

                        <div className="col-md-8">
                            <div className="form-group">
                                <label>Phone Number:</label>
                                <input
                                    type="text"
                                    name="phoneNumber"
                                    value={this.state.phoneNumber}
                                    className="form-control"
                                    placeholder="Enter Phone no."
                                    onChange={event => this.setState({ phoneNumber: event.target.value })}
                                    onFocus={() => this.setState({ phoneNumberError: "" })}
                                    readOnly={this.state.viewUser}
                                />
                                <span style={{ color: 'red' }}>{this.state.phoneNumberError ? `* ${this.state.phoneNumberError}` : ''}</span>
                            </div>
                        </div>

                        <div className="col-md-12">
                            <div className="form-group">
                                <label>SIP Provider:</label>
                                <input
                                    type="text"
                                    name="sipProvider"
                                    value={this.state.sipProvider}
                                    className="form-control"
                                    placeholder="Enter SIP Provider"
                                    onChange={event => this.setState({ sipProvider: event.target.value })}
                                    onFocus={() => this.setState({ sipProviderError: "" })}
                                />
                                <span style={{ color: 'red' }}>{this.state.sipProviderError ? `* ${this.state.sipProviderError}` : ''}</span>
                            </div>
                        </div>

                        <div className="col-md-6">
                            <div className="form-group">
                                <label>Country:</label>
                                <input
                                    type="tel"
                                    name="country"
                                    value={this.state.country}
                                    className="form-control"
                                    //placeholder="Enter Mobile Number"
                                    onChange={event => this.setState({ country: event.target.value })}
                                    onFocus={() => this.setState({ countryError: "" })}
                                />
                                <span style={{ color: 'red' }}>{this.state.countryError ? `* ${this.state.countryError}` : ''}</span>
                            </div>
                        </div>

                        <div className="col-md-6">
                            <div className="form-group">
                                <label>Cost Per Minute:</label>
                                <input
                                    type="text"
                                    name="costPerMinute"
                                    value={this.state.costPerMinute}
                                    className="form-control"
                                    placeholder="Enter cost per min."
                                    onChange={(event) => { this.setState({ costPerMinute: event.target.value }) }}
                                    onFocus={() => this.setState({ costPerMinuteError: "" })}
                                />
                                <span style={{ color: 'red' }}>{this.state.costPerMinuteError ? `* ${this.state.costPerMinuteError}` : ''}</span>
                            </div>
                        </div>

                        <div className="col-md-6">
                            <div className="form-group">
                                {!this.state.isActive ?
                                    <Button variant="success" style={{ backgroundColor: '#28a745' }}
                                        onClick={() => { this.setState({ isActive: true }) }}
                                    >
                                        Active
                                    </Button>
                                    :
                                    <Button variant="secondary" style={{ backgroundColor: '#6c757d' }}
                                        onClick={() => { this.setState({ isActive: false }) }}
                                    >
                                        In Active
                                    </Button>
                                }
                            </div>
                        </div>

                    </div>
                </Modal.Body>
                <Modal.Footer>
                    {!loading ?
                        <Button variant="primary" onClick={() => this.UpdateNumber()}>
                            Save
                        </Button>
                        :
                        null}
                    {loading ?
                        <Button variant="primary" >
                            <i className="fa fa-spinner fa-spin" ></i>
                        </Button>
                        :
                        null}
                </Modal.Footer>
            </Modal>

            <Modal show={this.state.showSuccessAlert} onHide={() => this.clearAlert()}>
                <Modal.Header closeButton>
                    <Modal.Title>OK</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h5 style={{ color: 'green' }}>{`${this.state.successAlertMessage}`}</h5>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={() => this.clearAlert()}>
                        Ok
            </Button>
                </Modal.Footer>
            </Modal>

            <Modal show={this.state.showErrorAlert} onHide={() => this.clearAlert()}>
                <Modal.Header closeButton>
                    <Modal.Title>OK</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h5 style={{ color: 'red' }}>{`${this.state.errorAlertMessage}`}</h5>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={() => this.clearAlert()}>
                        Ok
                    </Button>
                </Modal.Footer>
            </Modal>


        </main>

    }

}

export default checkAuthentication(AdminUserManagement);