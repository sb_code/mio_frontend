import React, { Component } from 'react'
import Navbar from '../Component/Navbar'
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button, Accordion, Card } from "react-bootstrap"
import _ from 'lodash';
import { CSVLink, CSVDownload } from "react-csv";
import ReactTooltip from 'react-tooltip'; // For tool tip 
var path = cred.API_PATH;


class PackageManagement extends Component {
	constructor(props) {
		super(props)
		this.state = {
			userId: '',
			userType: '',

			openSideMenu: false,
			searchString: '',
			searching: false,

			loading: false,

			freePackage: '',
			startupPackage: '',
			enterprisePackage: '',
			freePackageId: '',
			startupPackageId: '',
			enterprisePackageId: '',
			selectedId: '',

			selectedType: '',
			selectedpageNo: '',
			policyArray: [],

			addPackageModel: false,
			policy: '',
			policyError: '',

			showSuccessAlert: false,
			successAlertMessage: '',
			showErrorAlert: false,
			errorAlertMessage: ''

		}
	}

	componentDidMount() {
		document.body.classList.remove("sign-bg");
		document.body.classList.add("grey-bg");

		let storage = localStorage.getItem("MIO_Local");
		storage = storage ? JSON.parse(storage) : null;

		// Page name -- 
		// let pages = [{ name: "FREE TYPE", key: "FREE_TYPE" }, { name: "STARTUP", key: "STARTUP" }, { name: "ENTERPRISE", key: "ENTERPRISE" }];

		if (storage && storage.data.userId) {
			this.setState({
				userId: storage.data.userId,
				userType: storage.data.userType,
				companyId: storage.data.companyId
			}, () => {
				this.getPackageDetails();
			});

			// console.log(this.parseJwt(storage.token));
			let tokenData = this.parseJwt(storage.token);

		}


	}

	// For userList of a particular company - 
	getPackageDetails = () => {
		this.setState({ loading: true });
		axios
			.post(path + 'content-management/get-pricing-plans')
			.then(serverResponse => {

				let res = serverResponse.data;
				if (!res.isError) {
					// console.log("all pricing package details ===", res);

					let freePackage = res.plans && res.plans[0] && res.plans[0].contents;
					let startupPackage = res.plans && res.plans[1] && res.plans[1].contents;
					let enterprisePackage = res.plans && res.plans[2] && res.plans[2].contents;

					let freePackageId = res.plans && res.plans[0] && res.plans[0]._id;
					let startupPackageId = res.plans && res.plans[1] && res.plans[1]._id;
					let enterprisePackageId = res.plans && res.plans[2] && res.plans[2]._id;

					this.setState({
						loading: false,
						freePackage,
						startupPackage,
						enterprisePackage,
						freePackageId,
						startupPackageId,
						enterprisePackageId
					});

				} else {
					this.setState({
						loading: false
					})
				}

			})
			.catch(error => {
				console.log(error);
			})

	}

	addPackage = (event, name) => {

		event.preventDefault();

		if (name == "free") {
			this.setState({
				addPackageModel: true,
				selectedType: cred.COMPANY_TYPE_FREE_TYPE,
				selectedId: this.state.freePackageId,
				policyArray: this.state.freePackage
			})
		} else if (name == "start") {
			this.setState({
				addPackageModel: true,
				selectedType: cred.COMPANY_TYPE_STARTUP,
				selectedId: this.state.startupPackageId,
				policyArray: this.state.startupPackage
			})
		} else if (name == "enterprise") {
			this.setState({
				addPackageModel: true,
				selectedType: cred.COMPANY_TYPE_ENTERPRISE,
				selectedId: this.state.enterprisePackageId,
				policyArray: this.state.enterprisePackage
			})
		}

	}

	savePolicy = () => {

		if (this.state.policy == '') {
			this.setState({ policyError: "Please give a policy plan." })
		} else {

			let contents = this.state.policyArray && this.state.policyArray.length > 0 ? [...this.state.policyArray, this.state.policy] : [this.state.policy]
			let dataToSend = {
				planId: '',
				type: this.state.selectedType,
				contents: contents
			};
			this.setState({ loading: true });
			axios
				.post(path + 'content-management/set-pricing-plan', dataToSend)
				.then(serverResponse => {

					let res = serverResponse.data;
					if (!res.isError) {
						this.setState({
							showSuccessAlert: true,
							successAlertMessage: 'Successfully added',
							loading: false,
							addPackageModel: false
						}, () => {
							this.getPackageDetails();
							this.clearState();
						})
					} else {
						this.setState({
							loading: false,
							showErrorAlert: true,
							errorAlertMessage: 'Sorry! something is wrong',
							addPackageModel: false
						}, () => {
							this.clearState();
						})
					}

				})
				.catch(error => {
					console.log(error);
				})

		}

	}

	deletePackage = (event, name, key) => {
		// console.log("In delete package == ", key, name);
		event.preventDefault();
		// if (key) {
		let dataToSend = {};

		if (name == "free") {

			let contents = [];
			this.state.freePackage && this.state.freePackage.map((value, index) => {
				if (index != key) {
					contents.push(value);
				}
			})

			dataToSend = {
				planId: this.state.freePackageId,
				type: cred.COMPANY_TYPE_FREE_TYPE,
				contents: contents
			}

		} else if (name == "start") {

			let contents = [];
			this.state.startupPackage && this.state.startupPackage.map((value, index) => {
				if (index != key) {
					contents.push(value);
				}
			})

			dataToSend = {
				planId: this.state.startupPackageId,
				type: cred.COMPANY_TYPE_STARTUP,
				contents: contents
			}
		} else if (name == "enterprise") {

			let contents = [];
			this.state.enterprisePackage && this.state.enterprisePackage.map((value, index) => {
				if (index != key) {
					contents.push(value);
				}
			})

			dataToSend = {
				planId: this.state.enterprisePackageId,
				type: cred.COMPANY_TYPE_STARTUP,
				contents: contents
			}
		}

		this.setState({ loading: true })

		axios
			.post(path + 'content-management/set-pricing-plan', dataToSend)
			.then(serverResponse => {
				let res = serverResponse.data;
				if (!res.isError) {
					this.setState({
						showSuccessAlert: true,
						successAlertMessage: 'Successfully deleted',
						loading: false
					}, () => {
						this.getPackageDetails();
					})
				} else {
					this.setState({
						showErrorAlert: true,
						errorAlertMessage: 'Sorry! Try again later.',
						loading: false
					}, () => {
						this.getPackageDetails();
					})
				}
			})
			.catch(error => {
				console.log(error);
			})

		// }


	}

	changePolicy = (event, name, key) => {

		let value = event.target.value;
		let { freePackage, startupPackage, enterprisePackage } = this.state;
		if (name == "free") {
			freePackage[key] = value;
			this.setState({
				freePackage: freePackage
			});
		} else if (name == "start") {
			startupPackage[key] = value;
			this.setState({
				startupPackage: startupPackage
			});
		} else if (name == "enterprise") {
			enterprisePackage[key] = value;
			this.setState({
				enterprisePackage: enterprisePackage
			});
		}

	}

	updatePackage = (event, name, key) => {
		let dataToSend;
		let { freePackage, startupPackage, enterprisePackage } = this.state;
		if (name == "free") {
			dataToSend = {
				planId: this.state.freePackageId,
				type: cred.COMPANY_TYPE_FREE_TYPE,
				contents: freePackage
			};
		} else if (name == "start") {
			dataToSend = {
				planId: this.state.startupPackageId,
				type: cred.COMPANY_TYPE_STARTUP,
				contents: startupPackage
			};
		} else if (name == "enterprise") {
			dataToSend = {
				planId: this.state.enterprisePackageId,
				type: cred.COMPANY_TYPE_ENTERPRISE,
				contents: enterprisePackage
			};
		}

		axios
			.post(path + 'content-management/set-pricing-plan', dataToSend)
			.then(serverResponse => {

				let res = serverResponse.data;
				if (!res.isError) {
					this.setState({
						showSuccessAlert: true,
						successAlertMessage: 'Successfully updated',
						loading: false,
						addPackageModel: false
					}, () => {
						this.getPackageDetails();
						this.clearState();
					})
				} else {
					this.setState({
						loading: false,
						showErrorAlert: true,
						errorAlertMessage: 'Sorry! something is wrong',
						addPackageModel: false
					}, () => {
						this.clearState();
					})
				}

			})
			.catch(error => {
				console.log(error);
			})


	}

	clearState = () => {

		this.setState({
			addPackageModel: false,
			selectedType: '',
			selectedId: '',
			policy: '',
			policyArray: [],
			loading: false
		})

	}

	hideSuccessAlert = () => {

		this.setState({
			showSuccessAlert: false,
			successAlertMessage: '',
		})

	}

	hideErrorAlert = () => {

		this.setState({
			showErrorAlert: false,
			errorAlertMessage: ''
		})

	}

	backMenueToggle = () => {
		this.props.history.goBack();
	}

	sideMenuToggle = () => {
		let { openSideMenu } = this.state;
		openSideMenu = !openSideMenu;
		this.setState({ openSideMenu });
	}

	mobMenuClick() {
		this.sideMenuToggle();
		console.log('mobMenuClick');
	}

	parseJwt = (token) => {
		if (!token) { return; }
		const base64Url = token.split('.')[1];
		const base64 = base64Url.replace('-', '+').replace('_', '/');
		return JSON.parse(window.atob(base64));
	}


	render() {

		let { loading, policy, freePackage, startupPackage, enterprisePackage, } = this.state;
		return <main>
			<section className="user-mngnt-wrap">
				<div className="container-fluid">
					<div className="row">

						<Navbar routTo="/admincontentmanagement" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />
						<div className="col-xl-10 col-lg-9 p-0 mob-full-conf">

							<div className="mobile-menu-header">
								<a href="JavaScript:Void(0)" className="back-arw" onClick={() => this.backMenueToggle()}><i className="fas fa-arrow-left"></i></a>
								{/* <a className="menu-tgl" onClick={() => this.sideMenuToggle()}><img src="images/menu-tgl.png" alt="" /></a> */}
							</div>

							<Accordion>

								<Card>
									<Card.Header style={{ backgroundColor: 'white' }}>
										{!loading ? <Accordion.Toggle as={Button} variant="link" eventKey="0">
											FREE TYPE <i className="fa fa-angle-double-down" style={{ fontSize: "16px", color: 'white' }}></i>
										</Accordion.Toggle>
											: null}
										{loading ? <Accordion.Toggle as={Button} variant="link" eventKey="0">
											FREE TYPE <i className="fa fa-spinner fa-spin" style={{ fontSize: "16px", color: 'white' }}></i>
										</Accordion.Toggle>
											: null}
									</Card.Header>
									<Accordion.Collapse eventKey="0">
										<Card.Body style={{ backgroundColor: 'whitesmoke' }}>
											<div className="mng-full-list">
												<div className="row" style={{ padding: '5px', margin: '10px 0px' }}>

													<div className="col-md-3">
														<a
															href="JavaScript:Void(0);"
															className="btn border"
															data-tip='Add Package'
															onClick={(event) => { this.addPackage(event, "free") }}
															style={{ width: '100%' }}
														>
															Add package policy
															</a>
													</div>

												</div>

												<div className="row">
													<div className="col-lg-12 col-md-12">
														{freePackage && freePackage.length > 0 ?

															(freePackage.map((value, index) => {
																return <div className="row" key={index}>
																	
																	<div className="col-md-1">
																		<div className="form-group">
																			<span><a href="JavaScript:Void(0);" style={{ color: "#0092DD" }}><b>{index + 1}: </b></a></span>

																		</div>
																	</div>

																	<div className="mobile-ad-edt-btns">
																		<ul>

																			<li onClick={(event) => { this.deletePackage(event, "free", index) }}>
																				<a href="JavaScript:Void(0);"
																					data-tip='Delete'
																				>
																					<i className="fas fa-trash"></i>
																				</a>
																				<ReactTooltip
																					effect="float"
																					place="top"
																					data-border="true"
																				/>
																			</li>

																			<li onClick={(event) => { this.updatePackage(event, "free", index) }}>
																				<a href="JavaScript:Void(0);"
																					data-tip='Update'
																				>
																					<i className="fas fa-save"></i>
																				</a>
																				<ReactTooltip
																					effect="float"
																					place="top"
																					data-border="true"
																				/>
																			</li>

																		</ul>
																	</div>

																	<div className="col-md-8">
																		<div className="form-group">

																			<textarea type="text" name="package" className="form-control" rows="200" cols="50"
																				placeholder="Enter package and conditions....." style={{ height: "75px" }}
																				onChange={(event) => this.changePolicy(event, "free", index)}
																				readOnly={false}
																				value={value}
																			>

																			</textarea>
																		</div>
																	</div>

																	<div className="col-md-3">
																		<div className="mng-full-table-row add-edt text-center">
																			<div className="row">
																				{!loading ?
																					<div className="col-md-4">
																						<a href="JavaScript:Void(0);"
																							ref={ref => this.fooRef = ref}
																							data-tip='Click to delete'
																							onFocus={() => { ReactTooltip.show(this.fooRef) }}
																							onClick={(event) => { this.deletePackage(event, "free", index); ReactTooltip.hide(this.fooRef) }}
																						>
																							<i className="fas fa-trash"></i>
																						</a>
																						<ReactTooltip
																							effect="float"
																							place="top"
																							data-border="true"
																						/>
																					</div>
																					: null}

																				{!loading ?
																					<div className="col-md-4">
																						<a href="JavaScript:Void(0);"
																							ref={ref => this.fooRef = ref}
																							data-tip='Click to update'
																							onFocus={() => { ReactTooltip.show(this.fooRef) }}
																							onClick={(event) => { this.updatePackage(event, "free", index); ReactTooltip.hide(this.fooRef) }}
																						>
																							<i className="fas fa-save"></i>
																						</a>
																						<ReactTooltip
																							effect="float"
																							place="top"
																							data-border="true"
																						/>
																					</div>
																					: null}

																				{loading ?
																					<div className="col-md-4">
																						<a href="JavaScript:Void(0);" >
																							<i className="fa fa-spinner fa-spin" ></i>
																						</a>
																					</div>
																					: null}

																			</div>

																		</div>
																	</div>
																	
																</div>
															}))

															:
															this.state.loading ?
																<i><h4>Loading...</h4></i>
																: null
														}
													</div>
												</div>
											</div>
										</Card.Body>
									</Accordion.Collapse>
								</Card>

								<Card>
									<Card.Header style={{ backgroundColor: 'white' }}>
										{!loading ? <Accordion.Toggle as={Button} variant="link" eventKey="1">
											START TYPE <i className="fa fa-angle-double-down" style={{ fontSize: "16px", color: 'white' }}></i>
										</Accordion.Toggle>
											: null}
										{loading ? <Accordion.Toggle as={Button} variant="link" eventKey="1">
											START TYPE <i className="fa fa-spinner fa-spin" style={{ fontSize: "16px", color: 'white' }}></i>
										</Accordion.Toggle>
											: null}
									</Card.Header>
									<Accordion.Collapse eventKey="1">
										<Card.Body style={{ backgroundColor: 'whitesmoke' }}>
											<div className="mng-full-list">
												<div className="row" style={{ padding: '5px', margin: '10px 0px' }}>

													<div className="col-md-3">
														<a
															href="JavaScript:Void(0);"
															className="btn border"
															data-tip='Add Package'
															onClick={(event) => { this.addPackage(event, "start") }}
															style={{ width: '100%' }}
														>
															Add package policy
															</a>
													</div>

												</div>
												<div className="row">
													<div className="col-lg-12 col-md-12">
														{startupPackage && startupPackage.length > 0 ?

															(startupPackage.map((value, index) => {
																return <div className="row" key={index}>
																	<div className="col-md-1">
																		<div className="form-group">
																			<span><a href="JavaScript:Void(0);" style={{ color: "#0092DD" }}><b>{index + 1}: </b></a></span>

																		</div>
																	</div>

																	<div className="mobile-ad-edt-btns">
																		<ul>

																			<li onClick={(event) => { this.deletePackage(event, "start", index) }}>
																				<a href="JavaScript:Void(0);"
																					data-tip='Delete'
																				>
																					<i className="fas fa-trash"></i>
																				</a>
																				<ReactTooltip
																					effect="float"
																					place="top"
																					data-border="true"
																				/>
																			</li>

																			<li onClick={(event) => { this.updatePackage(event, "start", index) }}>
																				<a href="JavaScript:Void(0);"
																					data-tip='Update'
																				>
																					<i className="fas fa-save"></i>
																				</a>
																				<ReactTooltip
																					effect="float"
																					place="top"
																					data-border="true"
																				/>
																			</li>

																		</ul>
																	</div>

																	<div className="col-md-8">
																		<div className="form-group">

																			<textarea type="text" name="package" className="form-control" rows="200" cols="50"
																				placeholder="Enter package and conditions....." style={{ height: "75px" }}
																				onChange={(event) => this.changePolicy(event, "start", index)}
																				readOnly={false}
																				value={value}
																			>

																			</textarea>
																		</div>
																	</div>

																	<div className="col-md-3">
																		<div className="mng-full-table-row add-edt text-center">
																			<div className="row">
																				{!loading ?
																					<div className="col-md-4">
																						<a href="JavaScript:Void(0);"
																							ref={ref => this.fooRef = ref}
																							data-tip='Click to delete'
																							onFocus={() => { ReactTooltip.show(this.fooRef) }}
																							onClick={(event) => { this.deletePackage(event, "start", index); ReactTooltip.hide(this.fooRef) }}
																						>
																							<i className="fas fa-trash"></i>
																						</a>
																						<ReactTooltip
																							effect="float"
																							place="top"
																							data-border="true"
																						/>
																					</div>
																					: null}

																				{!loading ?
																					<div className="col-md-4">
																						<a href="JavaScript:Void(0);"
																							ref={ref => this.fooRef = ref}
																							data-tip='Click to update'
																							onFocus={() => { ReactTooltip.show(this.fooRef) }}
																							onClick={(event) => { this.updatePackage(event, "start", index); ReactTooltip.hide(this.fooRef) }}
																						>
																							<i className="fas fa-save"></i>
																						</a>
																						<ReactTooltip
																							effect="float"
																							place="top"
																							data-border="true"
																						/>
																					</div>
																					: null}

																				{loading ?
																					<div className="col-md-4">
																						<a href="JavaScript:Void(0);" >
																							<i className="fa fa-spinner fa-spin" ></i>
																						</a>
																					</div>
																					: null}

																			</div>

																		</div>
																	</div>

																</div>
															}))

															:
															this.state.loading ?
																<i><h4>Loading...</h4></i>
																: null
														}
													</div>
												</div>
											</div>
										</Card.Body>
									</Accordion.Collapse>
								</Card>

								<Card>
									<Card.Header style={{ backgroundColor: 'white' }}>
										{!loading ? <Accordion.Toggle as={Button} variant="link" eventKey="2">
											ENTERPRISE <i className="fa fa-angle-double-down" style={{ fontSize: "16px", color: 'white' }}></i>
										</Accordion.Toggle>
											: null}
										{loading ? <Accordion.Toggle as={Button} variant="link" eventKey="2">
											ENTERPRISE <i className="fa fa-spinner fa-spin" style={{ fontSize: "16px", color: 'white' }}></i>
										</Accordion.Toggle>
											: null}
									</Card.Header>
									<Accordion.Collapse eventKey="2">
										<Card.Body style={{ backgroundColor: 'whitesmoke' }}>
											<div className="mng-full-list">
												<div className="row" style={{ padding: '5px', margin: '10px 0px' }}>

													<div className="col-md-3">
														<a
															href="JavaScript:Void(0);"
															className="btn border"
															data-tip='Add Package'
															onClick={(event) => { this.addPackage(event, "enterprise") }}
															style={{ width: '100%' }}
														>
															Add package policy
															</a>
													</div>

												</div>

												<div className="row">
													<div className="col-lg-12 col-md-12">
														{enterprisePackage && enterprisePackage.length > 0 ?

															(enterprisePackage.map((value, index) => {
																return <div className="row" key={index}>
																	<div className="col-md-1">
																		<div className="form-group">
																			<span><a href="JavaScript:Void(0);" style={{ color: "#0092DD" }}><b>{index + 1}: </b></a></span>

																		</div>
																	</div>

																	<div className="mobile-ad-edt-btns">
																		<ul>

																			<li onClick={(event) => { this.deletePackage(event, "enterprise", index) }}>
																				<a href="JavaScript:Void(0);"
																					data-tip='Delete'
																				>
																					<i className="fas fa-trash"></i>
																				</a>
																				<ReactTooltip
																					effect="float"
																					place="top"
																					data-border="true"
																				/>
																			</li>

																			<li onClick={(event) => { this.updatePackage(event, "enterprise", index) }}>
																				<a href="JavaScript:Void(0);"
																					data-tip='Update'
																				>
																					<i className="fas fa-save"></i>
																				</a>
																				<ReactTooltip
																					effect="float"
																					place="top"
																					data-border="true"
																				/>
																			</li>

																		</ul>
																	</div>

																	<div className="col-md-8">
																		<div className="form-group">

																			<textarea type="text" name="package" className="form-control" rows="200" cols="50"
																				placeholder="Enter package and conditions....." style={{ height: "75px" }}
																				onChange={(event) => this.changePolicy(event, "enterprise", index)}
																				readOnly={false}
																				value={value}
																			>

																			</textarea>
																		</div>
																	</div>

																	<div className="col-md-3">
																		<div className="mng-full-table-row add-edt text-center">
																			<div className="row">
																				{!loading ?
																					<div className="col-md-4">
																						<a href="JavaScript:Void(0);"
																							ref={ref => this.fooRef = ref}
																							data-tip='Click to delete'
																							onFocus={() => { ReactTooltip.show(this.fooRef) }}
																							onClick={(event) => { this.deletePackage(event, "enterprise", index); ReactTooltip.hide(this.fooRef) }}
																						>
																							<i className="fas fa-trash"></i>
																						</a>
																						<ReactTooltip
																							effect="float"
																							place="top"
																							data-border="true"
																						/>
																					</div>
																					: null}

																				{!loading ?
																					<div className="col-md-4">
																						<a href="JavaScript:Void(0);"
																							ref={ref => this.fooRef = ref}
																							data-tip='Click to update'
																							onFocus={() => { ReactTooltip.show(this.fooRef) }}
																							onClick={(event) => { this.updatePackage(event, "enterprise", index); ReactTooltip.hide(this.fooRef) }}
																						>
																							<i className="fas fa-save"></i>
																						</a>
																						<ReactTooltip
																							effect="float"
																							place="top"
																							data-border="true"
																						/>
																					</div>
																					: null}

																				{loading ?
																					<div className="col-md-4">
																						<a href="JavaScript:Void(0);" >
																							<i className="fa fa-spinner fa-spin" ></i>
																						</a>
																					</div>
																					: null}

																			</div>

																		</div>
																	</div>

																</div>
															}))

															:
															this.state.loading ?
																<i><h4>Loading...</h4></i>
																: null
														}
													</div>
												</div>
											</div>
										</Card.Body>
									</Accordion.Collapse>
								</Card>

							</Accordion>

						</div>

					</div>
				</div>

			</section>

			<Modal show={this.state.addPackageModel} size="lg" onHide={() => { this.clearState() }}>
				<Modal.Header closeButton>
					<Modal.Title>Add new Policy</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<div className="row">

						<div className="col-md-12">
							<div className="form-group">
								<label>New Point:</label>
								<textarea type="text" name="policy" className="form-control" rows="200" cols="50"
									placeholder="Enter a policy" style={{ height: "150px" }}
									value={policy}
									onChange={(event) => this.setState({ policy: event.target.value })}
									onFocus={() => { this.setState({ policyError: '' }) }}
								>
									{policy}
								</textarea>
								<span style={{ color: 'red' }}>{this.state.policyError ? `* ${this.state.policyError}` : ''}</span>
							</div>
						</div>

					</div>
				</Modal.Body>
				<Modal.Footer>
					{!loading ?
						<Button variant="primary" onClick={this.savePolicy}> Save </Button>
						: null}
					{loading ?
						<Button variant="primary"> <i className="fa fa-spinner fa-spin" ></i> </Button>
						: null}

				</Modal.Footer>
			</Modal>

			<Modal show={this.state.showSuccessAlert} onHide={() => this.hideSuccessAlert()}>
				<Modal.Header closeButton>
					<Modal.Title>OK</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<h5 style={{ color: 'green' }}>{`${this.state.successAlertMessage}`}</h5>
				</Modal.Body>
				<Modal.Footer>
					<Button variant="primary" onClick={() => this.hideSuccessAlert()}>
						Ok
            </Button>
				</Modal.Footer>
			</Modal>

			<Modal show={this.state.showErrorAlert} onHide={() => this.hideErrorAlert()}>
				<Modal.Header closeButton>
					<Modal.Title>OK</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<h5 style={{ color: 'red' }}>{`${this.state.errorAlertMessage}`}</h5>
				</Modal.Body>
				<Modal.Footer>
					<Button variant="primary" onClick={() => this.hideErrorAlert()}>
						Ok
            </Button>
				</Modal.Footer>
			</Modal>


		</main >

	}

}

export default PackageManagement;



