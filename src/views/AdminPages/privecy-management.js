import React, { Component } from 'react'
import Navbar from '../Component/Navbar'
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button } from "react-bootstrap"
import _ from 'lodash';
import { CSVLink, CSVDownload } from "react-csv";
import ReactTooltip from 'react-tooltip'; // For tool tip 
import RichTextEditor from 'react-rte';

import { checkAuthentication } from '../Component/Authentication';

var path = cred.API_PATH;


export class PrivecypolicyManagement extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userId: '',
            userType: '',
            companyId: '',
            selectedPageName: '',

            openSideMenu: false,

            loading: false,

            editingField: '',

            showModal: false,
            policy: RichTextEditor.createEmptyValue(),
            dataToSaveInDb: '',
            policyDetails: '',

            showSuccessAlert: false,
            successAlertMessage: '',
            showErrorAlert: false,
            errorAlertMessage: ''

        }
    }

    componentDidMount() {
        document.body.classList.remove("sign-bg");
        document.body.classList.add("grey-bg");

        if (this.props.location.state && this.props.location.state.name) {

            let storage = localStorage.getItem("MIO_Local");
            storage = storage ? JSON.parse(storage) : null;
            let selectedPageName = this.props.location.state.name;

            if (storage && storage.data.userId) {
                this.setState({
                    userId: storage.data.userId,
                    userType: storage.data.userType,
                    companyId: storage.data.companyId,
                    selectedPageName: selectedPageName
                }, () => {
                    this.getPolicyDetails();
                    console.log("selectedPageName====", selectedPageName);
                });

                // console.log(this.parseJwt(storage.token));
                let tokenData = this.parseJwt(storage.token);

            }
        } else {
            this.props.history.push("/admincontentmanagement");
        }

    }

    getPolicyDetails = () => {
        this.setState({ loading: true });
        // API --- 
        axios
            .post(path + 'content-management/get-privacy-policy')
            .then(serverResponse => {
                let res = serverResponse.data;
                if (!res.isError) {
                    this.setState({
                        policyDetails: res.details && res.details[0],
                        loading: false
                    })
                } else {
                    this.setState({ loading: false });
                }
            })
            .catch(error => {
                this.setState({ loading: false });
                console.log(error);
            })

    }

    setRTESectionState(field, value) {
        let { dataToSaveInDb } = this.state;
        dataToSaveInDb = value.toString('html');
        this.setState({ dataToSaveInDb, policy: value });
    }

    addNewPolicy = () => {

        let { policy, dataToSaveInDb } = this.state;
        // policy || policy != '' ? this.setState({ policyError: '' }) : this.setState({ policyError: 'Enter a valid policy.' });

        if (dataToSaveInDb && dataToSaveInDb != '') {
            let dataToSend = {
                "policy": dataToSaveInDb
            };
            this.setState({ loading: true });
            // API - -
            axios
                .post(path + 'content-management/insert-privacy-policy', dataToSend)
                .then(serverResponse => {
                    let res = serverResponse.data;
                    if (!res.isError) {
                        this.setState({
                            loading: false,
                            policy: '',
                            showModal: false,
                            showSuccessAlert: true,
                            successAlertMessage: 'Successfully saved.'
                        }, () => {
                            this.getPolicyDetails();
                        });
                    } else {
                        this.setState({
                            loading: false,
                            policy: '',
                            showModal: false,
                            showErrorAlert: true,
                            errorAlertMessage: 'Sorry! Something is wrong.'
                        });
                    }
                })
                .catch(error => {
                    this.setState({ loading: false });
                    console.log(error);
                })

        }
    }

    editOption = () => {
        let { policyDetails } = this.state;
        this.setState({
            editModal: true,
            policy: RichTextEditor.createValueFromString(policyDetails.policy, 'html')
        })
    }

    editPolicy = () => {
        let { policyDetails, dataToSaveInDb } = this.state;

        if (dataToSaveInDb && dataToSaveInDb != '') {

            let dataToSend = {
                "policyId": policyDetails._id,
                "policy": dataToSaveInDb
            };
            this.setState({ loading: true });
            // API -- 
            axios
                .post(path + 'content-management/update-privacy-policy-by-id', dataToSend)
                .then(serverResponse => {
                    let res = serverResponse.data;
                    if (!res.isError) {
                        this.setState({
                            loading: false,
                            editingField: '',
                            showSuccessAlert: true,
                            successAlertMessage: 'Successfully saved.'
                        }, () => {
                            this.getPolicyDetails();
                            this.clearState();
                        });
                    } else {
                        this.setState({
                            loading: false,
                            editingField: '',
                            showErrorAlert: true,
                            errorAlertMessage: 'Sorry! Something is wrong.'
                        }, () => {
                            this.getPolicyDetails();
                            this.clearState();
                        });
                    }
                })
                .catch(error => {
                    this.setState({ loading: false });
                    console.log(error);
                })
        }

    }

    changePolicy = (event, index) => {

        let { policies } = this.state;
        let pair = policies[index];
        pair["policy"] = event.target.value;
        policies[index] = pair;

        this.setState({
            policies: policies
        });

    }

    deletePolicy = () => {

        let { policyDetails } = this.state;

        let dataToSend = {
            "policyId": policyDetails._id,
            "policy": ''
        };
        this.setState({ loading: true });
        // API -- 
        axios
            .post(path + 'content-management/update-privacy-policy-by-id', dataToSend)
            .then(serverResponse => {
                let res = serverResponse.data;
                if (!res.isError) {
                    this.setState({
                        loading: false,
                        editingField: '',
                        showSuccessAlert: true,
                        successAlertMessage: 'Successfully deleted.'
                    }, () => {
                        this.getPolicyDetails();
                    });
                } else {
                    this.setState({
                        loading: false,
                        editingField: '',
                        showErrorAlert: true,
                        errorAlertMessage: 'Sorry! Something is wrong.'
                    }, () => {
                        this.getPolicyDetails();
                    });
                }
            })
            .catch(error => {
                console.log(error);
            })


    }

    clearState = () => {

        this.setState({
            loading: false,
            dataToSaveInDb: '',
            policy: RichTextEditor.createEmptyValue(),
            showModal: false,
            editModal: false
        });

    }

    hideSuccessAlert() {
        this.setState({
            showSuccessAlert: false,
            successAlertMessage: '',
        })
    }

    hideErrorAlert() {
        this.setState({
            showErrorAlert: false,
            errorAlertMessage: ''
        })
    }

    sideMenuToggle = () => {
        let { openSideMenu } = this.state;
        openSideMenu = !openSideMenu;
        this.setState({ openSideMenu });
    }

    mobMenuClick() {
        this.sideMenuToggle();
        console.log('mobMenuClick');
    }

    backMenueToggle = () => {
        this.props.history.goBack();
    }

    parseJwt = (token) => {
        if (!token) { return; }
        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace('-', '+').replace('_', '/');
        return JSON.parse(window.atob(base64));
    }


    render() {

        let { loading, editingField, policy, policyDetails } = this.state;

        return <main>
            <section className="user-mngnt-wrap">
                <div className="container-fluid">
                    <div className="row">

                        <Navbar routTo="/admincontentmanagement" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />
                        <div className="col-xl-10 col-lg-10 p-0 mob-full-conf">

                            <div className="mobile-menu-header">
                                <a href="JavaScript:Void(0);" className="back-arw" onClick={() => this.backMenueToggle()}><i className="fas fa-arrow-left"></i></a>
                                {/* <a className="menu-tgl" onClick={() => this.sideMenuToggle()}><img src="images/menu-tgl.png" alt="" /></a> */}
                            </div>

                            <div className="custom-brdcrmb">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item"><a href="JavaScript:Void(0);">Content Management</a></li>
                                        <li className="breadcrumb-item active" aria-current="page">{this.state.selectedPageName}</li>
                                    </ol>
                                </nav>
                            </div>

                            <div className="mng-full-list about-mng-form" style={{ padding: "25px 30px" }}>
                                {!policyDetails ?
                                    <div className="row">
                                        <div className="col-md-3">
                                            <div className="add-user-btn">
                                                {!loading ?
                                                    <a href="JavaScript:Void(0);"
                                                        className="btn"
                                                        ref={ref => this.fooRef = ref}
                                                        data-tip='Add new Policy'
                                                        onFocus={() => { ReactTooltip.show(this.fooRef) }}
                                                        onClick={() => { this.setState({ showModal: true }); ReactTooltip.hide(this.fooRef) }}
                                                    >
                                                        Add New Policy
                                                    </a>
                                                    :
                                                    null}
                                                {loading ?
                                                    <a href="JavaScript:Void(0);"
                                                        className="btn"
                                                    >
                                                        <i className="fa fa-spinner fa-spin" />
                                                    </a>
                                                    :
                                                    null}
                                                <ReactTooltip
                                                    effect="float"
                                                    place="top"
                                                    data-border="true"
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    :
                                    null}

                                <div className="row">
                                    <div className="col-lg-12 col-md-12">

                                        <div className="row">

                                            <div className="mobile-ad-edt-btns">

                                                {/* <ul>
                                                        {editingField == `${index}` ?
                                                            <li onClick={() => { this.editPolicy(index) }}>
                                                                <a href="JavaScript:Void(0);"
                                                                    data-tip='Save'
                                                                >
                                                                    <i className="fas fa-save"></i>
                                                                </a>
                                                                <ReactTooltip
                                                                    effect="float"
                                                                    place="top"
                                                                    data-border="true"
                                                                />
                                                            </li>
                                                            :
                                                            null}
                                                        {editingField != `${index}` ?
                                                            <li onClick={() => { this.setState({ editingField: `${index}` }) }}>
                                                                <a href="JavaScript:Void(0);"
                                                                    // ref={ref => this.fooRef = ref}
                                                                    data-tip='Edit'
                                                                // onFocus={() => { ReactTooltip.show(this.fooRef) }}
                                                                >
                                                                    <i className="fas fa-edit"></i>
                                                                </a>
                                                                <ReactTooltip
                                                                    effect="float"
                                                                    place="top"
                                                                    data-border="true"
                                                                />
                                                            </li>
                                                            :
                                                            null}
                                                    </ul>
                                                 */}
                                            </div>

                                            {policyDetails && policyDetails.policy ?
                                                <div className="col-md-10">
                                                    <div className="form-group">

                                                        <RichTextEditor
                                                            value={RichTextEditor.createValueFromString(policyDetails.policy, 'html')}
                                                            readOnly={true}
                                                        />

                                                    </div>

                                                </div>
                                                : null}

                                            <div className="col-md-2">
                                                <div className="mng-full-table-row add-edt text-center">
                                                    <div className="row">
                                                        {!loading ?
                                                            <div className="col-md-6">
                                                                <a href="JavaScript:Void(0);"
                                                                    ref={ref => this.fooRef = ref}
                                                                    data-tip='Click to edit'
                                                                    onFocus={() => { ReactTooltip.show(this.fooRef) }}
                                                                    onClick={() => { this.editOption(); ReactTooltip.hide(this.fooRef) }}
                                                                >
                                                                    <i className="fas fa-edit"></i>
                                                                </a>
                                                                <ReactTooltip
                                                                    effect="float"
                                                                    place="top"
                                                                    data-border="true"
                                                                />
                                                            </div>
                                                            : null}
                                                        {!loading ?
                                                            <div className="col-md-6">
                                                                <a href="JavaScript:Void(0);"
                                                                    ref={ref => this.fooRef = ref}
                                                                    data-tip='Click to delete'
                                                                    onFocus={() => { ReactTooltip.show(this.fooRef) }}
                                                                    onClick={() => { this.deletePolicy(); ReactTooltip.hide(this.fooRef) }}
                                                                >
                                                                    <i className="fas fa-trash"></i>
                                                                </a>
                                                                <ReactTooltip
                                                                    effect="float"
                                                                    place="top"
                                                                    data-border="true"
                                                                />
                                                            </div>
                                                            : null}


                                                        {loading ?
                                                            <div className="col-md-6">
                                                                <a href="JavaScript:Void(0);" >
                                                                    <i className="fa fa-spinner fa-spin" ></i>
                                                                </a>
                                                            </div>
                                                            : null}

                                                    </div>
                                                </div>

                                            </div>

                                        </div>


                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </section>

            <Modal show={this.state.showModal} size="lg" onHide={() => { this.clearState() }}>
                <Modal.Header closeButton>
                    <Modal.Title>Add new Policy</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="row">

                        {/* <div className="col-md-12">
                            <div className="form-group">
                                <label>Policy:</label>
                                <textarea type="text" name="policy" className="form-control" rows="200" cols="50"
                                    placeholder="Enter a policy" style={{ height: "450px" }}
                                    value={policy}
                                    onChange={(event) => this.setState({ policy: event.target.value })}
                                    onFocus={() => { this.setState({ policyError: '' }) }}
                                >
                                    {policy}
                                </textarea>
                                <span style={{ color: 'red' }}>{this.state.policyError ? `* ${this.state.policyError}` : ''}</span>
                            </div>
                        </div> */}

                        <RichTextEditor
                            value={policy}
                            onChange={(value) => this.setRTESectionState('value', value)}
                        />

                    </div>
                </Modal.Body>
                <Modal.Footer>
                    {!loading ?
                        <Button variant="primary" onClick={this.addNewPolicy}> Save </Button>
                        :
                        null}
                    {loading ?
                        <Button variant="primary"> <i className="fa fa-spinner fa-spin" ></i> </Button>
                        :
                        null}

                </Modal.Footer>
            </Modal>

            <Modal show={this.state.editModal} size="lg" onHide={() => { this.clearState() }}>
                <Modal.Header closeButton>
                    <Modal.Title>Update Policy</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="row">

                        <RichTextEditor
                            value={policy}
                            onChange={(value) => this.setRTESectionState('value', value)}
                        // readOnly={true}
                        />

                    </div>
                </Modal.Body>
                <Modal.Footer>
                    {!loading ?
                        <Button variant="primary" onClick={this.editPolicy}> Update </Button>
                        :
                        null}
                    {loading ?
                        <Button variant="primary"> <i className="fa fa-spinner fa-spin" ></i> </Button>
                        :
                        null}

                </Modal.Footer>
            </Modal>

            <Modal show={this.state.showSuccessAlert} onHide={() => this.hideSuccessAlert()}>
                <Modal.Header closeButton>
                    <Modal.Title>OK</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h5 style={{ color: 'green' }}>{`${this.state.successAlertMessage}`}</h5>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={() => this.hideSuccessAlert()}>
                        Ok
            </Button>
                </Modal.Footer>
            </Modal>

            <Modal show={this.state.showErrorAlert} onHide={() => this.hideErrorAlert()}>
                <Modal.Header closeButton>
                    <Modal.Title>OK</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h5 style={{ color: 'red' }}>{`${this.state.errorAlertMessage}`}</h5>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={() => this.hideErrorAlert()}>
                        Ok
            </Button>
                </Modal.Footer>
            </Modal>

        </main >

    }

}

export default checkAuthentication(PrivecypolicyManagement);



