import React, { Component } from "react";
import Navbar from "../Component/Navbar";
import ReactTooltip from 'react-tooltip'; // For tool tip
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button } from "react-bootstrap"
import _ from 'lodash';
import { CSVLink, CSVDownload } from "react-csv";

import { checkAuthentication } from '../Component/Authentication';
// import { ReactTelInput } from 'react-telephone-input';

var path = cred.API_PATH;
var ReactTelInput = require('react-telephone-input');




export class OperatorManagement extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,

      userId: '',
      openSideMenu: false,

      searching: false,
      searchString: '',

      showUser: false,

      loading: false,
      searchLoading: false,

      countryCodeList: [],
      users: [],
      userRecords: '',
      pageNo: 1,

      selectedUserId: '',

      showUser: false,
      viewUser: false,
      editUser: false,
      showDeleteModal: false,
      deleteItem: '',

      fname: '',
      fnameError: '',
      lname: '',
      lnameError: '',
      companyId: '',
      companyIdError: '',
      userType: '',
      userTypeError: '',
      email: '',
      emailError: '',
      mobile: '',
      mobileError: '',
      countryCode: '',
      companyType: '',
      companyTypeError: '',
      service: '',
      serviceError: '',
      pricePerMin: '',
      pricePerMinError: '',

      showSuccessAlert: false,
      successAlertMessage: '',
      showErrorAlert: false,
      errorAlertMessage: ''

    }
    this.perPage = 5;

  }

  componentDidMount() {
    document.body.classList.remove("sign-bg");
    document.body.classList.add("grey-bg");

    let storage = localStorage.getItem("MIO_Local");
    storage = storage ? JSON.parse(storage) : null;
    if (storage && storage.data.userId) {
      this.setState({
        userId: storage.data.userId,
        companyType: storage.data.companyType,
        companyId: storage.data.companyId
      }, () => {
        this.getUserList();
        this.getCountryCodeList();
      });

    }

  }

  getUserList = () => {
    this.setState(prevState => ({ loading: true }));

    let dataToSend = {
      "companyId": this.state.companyId,
      "createdBy": this.state.userId,
      "userType": cred.USER_TYPE_ADMIN_OPERATOR,
      "page": this.state.pageNo,
      "perPage": this.perPage
    };

    // API to get users -- 
    axios
      .post(path + 'user-management/get-user-usertype-basis', dataToSend)
      .then(serverResponse => {
        let res = serverResponse.data;
        if (!res.isError) {
          console.log("response from get-user-usertype-basis -- ", res);
          let result = res.details && res.details.response && res.details.response[0] && res.details.response[0].results;
          let totalRows = res.details && res.details.response && res.details.response[0].noofpage;
          this.setState({
            users: result,
            userRecords: totalRows,
            loading: false
          });

        } else {
          this.setState(prevState => ({ loading: false }));
        }
      })
      .catch(error => {
        console.log(error);
      })

  }

  getCountryCodeList = () => {

    axios
      .post(path + 'services/get-country-codes')
      .then(serverResponse => {

        let res = serverResponse.data;
        if (!res.isError) {
          this.setState({
            countryCodeList: res.details
          });
        }

      })
      .catch(error => {
        console.log(error);
      })

  }

  addNewUser = () => {

    let { loading, fname, lname, companyId, email, mobile, countryCode, companyType, service, pricePerMin } = this.state;

    !fname ? this.setState({ fnameError: "Please enter a first name." }) : this.setState({ fnameError: "" });
    !lname ? this.setState({ lnameError: "Please enter a last name." }) : this.setState({ lnameError: "" });
    (/.+@.+\.[A-Za-z]+$/.test(`${email}`)) ? this.setState({ emailError: "" }) : this.setState({ emailError: "Please enter an email address." });

    (!mobile || isNaN(mobile)) ? this.setState({ mobileError: "Please enter a valid phone number." }) : this.setState({ mobileError: "" });
    (!countryCode || isNaN(countryCode)) ? this.setState({ countryCodeError: "Please enter a valid country code." }) : this.setState({ countryCodeError: "" });

    !service ? this.setState({ serviceError: "Please select a service." }) : this.setState({ serviceError: "" });
    !pricePerMin || isNaN(pricePerMin) ? this.setState({ pricePerMinError: "Please enter a valid price per min." }) : this.setState({ pricePerMinError: "" });

    if (fname && lname && (/.+@.+\.[A-Za-z]+$/.test(`${email}`)) && !isNaN(mobile) && mobile != '' && !isNaN(countryCode) && countryCode != '' && companyId && service && pricePerMin && !isNaN(pricePerMin)) {
      this.setState(prevState => ({ loading: true }));

      if (countryCode.slice(0, 1) == '+') {
        countryCode = countryCode.slice(1);
      }

      let dataToSend = {
        'fname': fname,
        'lname': lname,
        'email': email,
        'countryCode': countryCode,
        'mobile': mobile,
        'service': service,
        'pricePerMinute': pricePerMin,
        'availablity': 'Available',
        'isAssigned': false,
        'companyId': companyId,
        'userType': cred.USER_TYPE_ADMIN_OPERATOR,
        'companyType': companyType,
        'createdBy': this.state.userId
      };

      console.log(dataToSend);

      //For saving new user -- 
      axios
        .post(path + 'user-management/add-new-user', dataToSend)
        .then(serverResponse => {
          let res = serverResponse.data;
          if (!res.isError && res.statuscode == 200) {
            // console.log(res);
            this.setState({
              loading: false,
              showUser: false,
              showSuccessAlert: true,
              successAlertMessage: "Successfully added. The Password is : " + `${res.details && res.details.password}`
            });
            this.getUserList();
            this.clearState();
            // alert('Password is: ' + res.details.password);
          } else {

            this.clearState();
            this.setState({
              loading: false,
              showUser: false,
              showErrorAlert: true,
              errorAlertMessage: res.message
            });
          }
        })
        .catch(error => {
          console.log(error);
          this.setState(prevState => ({ loading: false }));
        })

    } else {
      this.setState(prevState => ({ loading: false }));
    }
  }

  searchUsers = (event) => {
    event.preventDefault();
    this.setState(prevState => ({ searching: true, searchLoading: true }));

    let dataToSend = {
      "userName": this.state.searchString,
      "companyId": this.state.companyId,
      "createdBy": this.state.userId,
      "userType": cred.USER_TYPE_ADMIN_OPERATOR
    };

    // console.log("dataToSend--", dataToSend);
    // API for searching -- 
    axios
      .post(path + 'user-management/search-user-name-basis', dataToSend)
      .then(serverResponse => {
        let res = serverResponse.data;
        if (!res.isError) {
          console.log(res);
          if (res.response) {
            this.setState(prevState => ({
              searchLoading: false,
              searching: true,
              users: res.response
            }));
          }else{
            this.setState({searchLoading: false})
          }
        } else {
          alert('Something is wrong..');
          this.setState(prevState => ({ searching: false, searchLoading: false }));
        }
      })
      .catch(error => {
        console.log(error);
      })

  }

  clearSearch(event) {
    event.preventDefault();
    // let { userId } = this.state;
    this.setState(prevState => ({
      searchString: '',
      searching: false,
      searchLoading: false
    }));
    this.getUserList();
  }

  userAssign = (index) => {

    let { loading, users } = this.state;
    let user = users[index];
    console.log(user);
    let { fname, lname, isAssigned, service, companyId, userType, email, mobile, countryCode, companyType, userId } = user;
    alert("Assigning user-- " + fname);

    // this.setState({ selectedId: index, fname, lname, isAssigned, service, companyId: companyId, userType, email, mobile, countryCode, companyType });

  }

  userUnassign = (index) => {

    let { loading, users } = this.state;
    let user = users[index];
    console.log(user);
    let { fname, lname, isAssigned, service, companyId, userType, email, mobile, countryCode, companyType, userId } = user;
    alert("Unassigning user-- " + fname);

    // this.setState({ selectedId: index, fname, lname, isAssigned, service, companyId: companyId, userType, email, mobile, countryCode, companyType });

  }

  viewOperator = (index) => {
    let { users } = this.state;

    let user = users[index];

  }

  editOperator = (index) => {
    let { users } = this.state;
    let user = users[index];

    this.setState({
      selectedUserId: index,
      fname: user.fname,
      lname: user.lname,
      userType: user.userType,
      email: user.email,
      mobile: user.mobile,
      countryCode: user.countryCode,
      service: user.service,
      pricePerMin: user.pricePerMinute,
      isActive: user.isActive,

      editUser: true,
    });

  }

  editOperatorDetails = () => {

    let { loading, users, fname, lname, companyId, email, mobile, countryCode, companyType, service, pricePerMin, isActive, selectedUserId } = this.state;

    !fname ? this.setState({ fnameError: "Please enter a first name." }) : this.setState({ fnameError: "" });
    !lname ? this.setState({ lnameError: "Please enter a last name." }) : this.setState({ lnameError: "" });
    (/.+@.+\.[A-Za-z]+$/.test(`${email}`)) ? this.setState({ emailError: "" }) : this.setState({ emailError: "Please enter an email address." });
    (!isNaN(mobile)) ? this.setState({ mobileError: "" }) : this.setState({ mobileError: "Please enter a phone number." });
    (!isNaN(countryCode)) ? this.setState({ countryCodeError: "" }) : this.setState({ countryCodeError: "Please enter a country code." });
    !service ? this.setState({ serviceError: "Please select a service." }) : this.setState({ serviceError: "" });
    !pricePerMin || isNaN(pricePerMin) ? this.setState({ pricePerMinError: "Please enter a valid price per min." }) : this.setState({ pricePerMinError: "" });

    if (fname && lname && (/.+@.+\.[A-Za-z]+$/.test(`${email}`)) && !isNaN(mobile) && !isNaN(countryCode) && companyId && service && pricePerMin && !isNaN(pricePerMin)) {
      this.setState(prevState => ({ loading: true }));

      if (countryCode.slice(0, 1) == '+') {
        countryCode = countryCode.slice(1);
      }

      let user = users[selectedUserId];
      let { userId } = user;

      let dataToSend = {
        'userId': userId,
        'fname': fname,
        'lname': lname,
        'email': email,
        'countryCode': countryCode,
        'mobile': mobile,
        'service': service,
        'pricePerMinute': pricePerMin,
        'userType': cred.USER_TYPE_ADMIN_OPERATOR,

        'companyId': companyId,
        'companyType': companyType,
        'isActive': isActive

      };

      axios
        .post(path + 'user-management/edit-user', dataToSend)
        .then(serverResponse => {
          let res = serverResponse.data;

          if (!res.isError) {
            this.getUserList();
            this.clearState();
          }

        })
        .catch(error => {
          console.log(error);
        })

    }
  }

  deleteOperatorConfirm = (index) => {
    let { users } = this.state;
    let user = users[index];

    this.setState({
      selectedUserId: index,
      deleteItem: user.fname,
      showDeleteModal: true
    });

  }

  deleteOperator = () => {

    let { users, selectedUserId } = this.state;
    let user = users[selectedUserId];

    let dataToSend = {
      "userId": user.userId
    };

    axios
      .post(path + 'user-management/delete-user-id-basis', dataToSend)
      .then(serverResponse => {
        let res = serverResponse.data;

        if (!res.isError) {
          this.setState({
            showSuccessAlert: true,
            successAlertMessage: "Successfully deleted"
          });
          this.getUserList();
          this.hideDeleteModal();
        } else {
          this.setState({
            showErrorAlert: true,
            errorAlertMessage: "Sorry! Try again..."
          });
          this.hideDeleteModal();
        }

      })
      .catch(error => {
        console.log(error);
      })

  }

  viewOperatorStat = (index) => {

    let { users } = this.state;
    let user = users[index];

    let { _id, service, isActive, fname, lname } = user;

    this.props.history.push({
      pathname: '/adminoperatorstat/' + `${_id}`,
      state: {
        service: service,
        name: fname + ' ' + lname,
        isActive: isActive
      }
    });

  }


  hideDeleteModal = () => {
    this.setState({
      selectedUserId: '',
      deleteItem: '',
      showDeleteModal: false
    })
  }

  hideSuccessAlert() {
    this.setState({
      showSuccessAlert: false,
      successAlertMessage: '',
    })
  }

  hideErrorAlert() {
    this.setState({
      showErrorAlert: false,
      errorAlertMessage: '',
    })
  }

  clearState = () => {
    this.setState({
      loading: false,
      showUser: false,
      showUser: false,
      viewUser: false,
      editUser: false,
      deleteUser: false,
      deleteItem: '',
      selectedUserId: '',
      fname: '',
      lname: '',
      // companyId: '',
      userType: '',
      email: '',
      mobile: '',
      countryCode: '',
      // companyType: '',
      service: '',
      pricePerMin: '',

      isActive: false,

      emailError: '',
      fnameError: '',
      lnameError: '',
      countryCodeError: '',
      mobileError: '',
      serviceError: '',
      pricePerMinError: ''

    });
  }

  pageChangePrev = () => {

    this.setState({
      pageNo: (parseFloat(this.state.pageNo) - 1)
    }, () => {
      this.getUserList()
    });

  }

  pageChangeNext = () => {

    this.setState({
      pageNo: (parseFloat(this.state.pageNo) + 1)
    }, () => {
      this.getUserList()
    });

  }

  sideMenuToggle() {
    let { openSideMenu } = this.state;
    openSideMenu = !openSideMenu;
    this.setState({ openSideMenu });
  }

  mobMenuClick = () => {
    console.log('mobMenuClick');
  }


  render() {

    let { users, loading, searchLoading, searching, fname, lname, email, mobile, countryCode, pageNo, userRecords, service, pricePerMin, isActive, countryCodeList } = this.state;
    let headers = [];
    headers = [
      { label: "Name", key: 'name' },
      { label: "Country Code", key: 'countryCode' },
      { label: "Phone", key: 'phoneNumber' },
      { label: "Email", key: 'emailAddress' },
      { label: "User Type", key: 'userType' },
    ];

    return (
      <main>

        <section className="user-mngnt-wrap">
          <div className="container-fluid">
            <div className="row">
              <Navbar routTo="/operatormanagement" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />
              <div className="col-xl-10 col-lg-9 p-0">
                <div className="mobile-menu-header">
                  {/* <a className="back-arw" onClick={() => console.log('<-BACK')}><i className="fas fa-arrow-left"></i></a> */}
                  <a href="JavaScript:Void(0);" className="menu-tgl" onClick={() => this.sideMenuToggle()}><img src="images/menu-tgl.png" alt="" /></a>
                </div>

                <div className="mng-full-list orga-mng">
                  <div className="mng-full-srch">
                    <div className="row">
                      <div className="col-md-9">
                        <div className="srch-wrap">
                          <form>
                            <input
                              type="text"
                              name=""
                              className="form-control"
                              placeholder="Search User"
                              value={this.state.searchString} onChange={(event) => this.setState({ searchString: event.target.value })}
                            />
                            {
                              this.state.searching ?
                                <input type="button" onClick={(event) => { this.clearSearch(event); }}
                                  // ref={ref => this.fooRef = ref}
                                  data-tip='Clear'
                                // onFocus={() => { ReactTooltip.show(this.fooRef) }}
                                />
                                :
                                <input type="submit" onClick={(event) => { this.searchUsers(event); }}
                                  // ref={ref => this.fooRef = ref}
                                  data-tip='Search'
                                // onFocus={() => { ReactTooltip.show(this.fooRef) }}
                                />
                            }
                            <ReactTooltip
                              effect="float"
                              place="top"
                              data-border="true"
                            />
                          </form>
                        </div>
                      </div>
                      <div className="col-md-3">
                        <div className="add-user-btn">
                          <a href="JavaScript:Void(0);" className="btn"
                            onClick={() => { this.setState({ showUser: true }) }}
                            // ref={ref => this.fooRef = ref}
                            data-tip='Add New Operator'
                          // onFocus={() => { ReactTooltip.show(this.fooRef) }}
                          >
                            Add Operator
                          </a>
                          <ReactTooltip
                            effect="float"
                            place="top"
                            data-border="true"
                          />
                        </div>
                      </div>
                    </div>
                    {searchLoading?
                      <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                        <i className="fa fa-spinner fa-spin" ></i>
                      </a>
                    :null}
                  </div>

                  {users && users.length > 0 ?
                    <div className="mng-full-table">
                      <div className="row">
                        <div className="col-md-8">
                          <div className="mng-full-table-hdr">
                            <div className="row">
                              <div className="col-md-4 border-rt">
                                <h6>Name</h6>
                              </div>
                              <div className="col-md-4 border-rt">
                                <h6>Service</h6>
                              </div>
                              <div className="col-md-4">
                                <h6>Availability</h6>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div className="col-md-4">
                          <div className="mng-full-table-hdr">
                            <div className="row">
                              <div className="col-md-4 border-rt">
                                <h6>Utilization</h6>
                              </div>
                              <div className="col-md-4 border-rt">
                                <h6>Edit</h6>
                              </div>
                              <div className="col-md-4">
                                <h6>Delete</h6>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      {users && users.length > 0 && users.map((data, index) => {

                        return (data.userId != this.state.userId) ?

                          <div className="row" key={index}>
                            <div className="col-md-8">
                              <div className="mng-full-table-row">
                                <div className="row">
                                  <div className="col-md-4 border-rt">
                                    <h6>Name</h6>
                                    <p>
                                      <a href="JavaScript:Void(0);" onClick={() => this.viewOperatorStat(index)}>
                                        {data.fname + ' ' + data.lname}
                                      </a>
                                    </p>
                                  </div>
                                  <div className="col-md-4 border-rt">
                                    <h6>Service</h6>
                                    <p>{data.service}</p>
                                  </div>
                                  <div className="col-md-4">
                                    <h6>Availability</h6>
                                    <p>{data.availablity}</p>
                                  </div>

                                  <div className="mobile-ad-edt-btns">
                                    <ul>
                                      <li onClick={() => this.viewOperatorStat(index)}>
                                        <a href="JavaScript:Void(0);"
                                          // ref={ref => this.fooRef = ref}
                                          data-tip='View Stats'
                                        // onFocus={() => { ReactTooltip.show(this.fooRef) }}
                                        >
                                          <i className="fas fa-eye"></i>
                                        </a>
                                        <ReactTooltip
                                          effect="float"
                                          place="top"
                                          data-border="true"
                                        />
                                      </li>

                                      <li onClick={() => { data.availablity == 'Available' ? this.editOperator(index) : console.log() }}>
                                        <a href="JavaScript:Void(0);"
                                          // ref={ref => this.fooRef = ref}
                                          data-tip='Edit'
                                          // onFocus={() => { ReactTooltip.show(this.fooRef) }}
                                          style={data.availablity == 'Available' ? {} : { background: "grey" }}
                                        >
                                          <i className="far fa-edit"></i>

                                        </a>
                                        <ReactTooltip
                                          effect="float"
                                          place="top"
                                          data-border="true"
                                        />
                                      </li>

                                      <li onClick={() => { data.availablity == 'Available' ? this.deleteOperatorConfirm(index) : console.log() }}>
                                        <a href="JavaScript:Void(0);"
                                          data-toggle="modal"
                                          data-target="#delModal"
                                          // ref={ref => this.fooRef = ref}
                                          data-tip='Delete'
                                          // onFocus={() => { ReactTooltip.show(this.fooRef) }}
                                          style={data.availablity == 'Available' ? {} : { background: "grey" }}
                                        >
                                          <i className="far fa-trash-alt"></i>
                                        </a>
                                        <ReactTooltip
                                          effect="float"
                                          place="top"
                                          data-border="true"
                                        />
                                      </li>

                                    </ul>
                                  </div>

                                </div>
                              </div>
                            </div>

                            <div className="col-md-4">
                              <div className="mng-full-table-row add-edt text-center">
                                <div className="row">

                                  <div className="col-md-4">
                                    <a href="JavaScript:Void(0);"
                                      onClick={() => { this.viewOperatorStat(index); }}
                                      // ref={ref => this.fooRef1 = ref}
                                      data-tip='View Stat'
                                    // onFocus={() => { ReactTooltip.show(this.fooRef1) }}
                                    >
                                      <i className="fas fa-eye"></i>
                                    </a>
                                    <ReactTooltip
                                      effect="float"
                                      place="top"
                                      data-border="true"
                                    />
                                  </div>
                                  <div className="col-md-4">
                                    <a href="JavaScript:Void(0);"
                                      onClick={() => { data.availablity == 'Available' ? this.editOperator(index) : console.log() }}
                                      // ref={ref => this.fooRef2 = ref}
                                      data-tip='Edit/View'
                                      // onFocus={() => { ReactTooltip.show(this.fooRef2) }}
                                      style={data.availablity == 'Available' ? {} : { background: "grey" }}
                                    >
                                      <i className="far fa-edit"></i>
                                    </a>
                                    <ReactTooltip
                                      effect="float"
                                      place="top"
                                      data-border="true"
                                    />
                                  </div>
                                  <div className="col-md-4">
                                    <a href="JavaScript:Void(0);"
                                      onClick={() => { data.availablity == 'Available' ? this.deleteOperatorConfirm(index) : console.log() }}
                                      // ref={ref => this.fooRef3 = ref}
                                      data-tip='Delete'
                                      // onFocus={() => { ReactTooltip.show(this.fooRef3) }}
                                      style={data.availablity == 'Available' ? {} : { background: "grey" }}
                                    >
                                      <i className="far fa-trash-alt"></i>
                                    </a>
                                    <ReactTooltip
                                      effect="float"
                                      place="top"
                                      data-border="true"
                                    />
                                  </div>

                                </div>
                              </div>

                            </div>

                          </div>

                          : null

                      })}

                    </div>
                    :
                    <div className="mng-full-table">
                      <div className="row">
                        <div className="col-md-9">
                          <div className="mng-full-table-hdr admn-usr-hdr">
                            <div className="row">
                              <div className="col-md-12">
                                <h6>No Operator Found.</h6>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  }
                  <br /><br />
                  {!searching ?
                    <div className="form-group">
                      {!loading && parseFloat(pageNo) > 1 ?
                        <span style={{ color: "#007bff", cursor: "pointer" }}
                          onClick={() => { this.pageChangePrev() }}
                        >
                          <b>{"<< Prev"}</b>
                        </span>
                        :
                        null}
                        &nbsp;&nbsp;|&nbsp;&nbsp;
                        {!loading && (parseFloat(pageNo) < parseFloat(userRecords)) ?
                        <span style={{ color: "#007bff", cursor: "pointer" }}
                          onClick={() => { this.pageChangeNext() }}
                        >
                          <b>{"Next >>"}</b>
                        </span>
                        :
                        null}
                      {loading ?
                        <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                          <i className="fa fa-spinner fa-spin" ></i>
                        </a>
                        :
                        null}

                    </div>
                    :
                    null}

                </div>

              </div>
            </div>
          </div>
        </section>

        <Modal show={this.state.showUser} onHide={() => { this.clearState() }}>
          <Modal.Header closeButton>
            <Modal.Title>Add new Operator</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="row">

              <div className="col-md-6">
                <div className="form-group">
                  <label>First Name:</label>
                  <input
                    type="text"
                    name="fname"
                    value={fname}
                    className="form-control"
                    placeholder="Enter First Name"
                    onChange={event => this.setState({ fname: event.target.value })}
                    onFocus={() => this.setState({ fnameError: "" })}
                  />
                  <span style={{ color: 'red' }}>{this.state.fnameError ? `* ${this.state.fnameError}` : ''}</span>
                </div>
              </div>

              <div className="col-md-6">
                <div className="form-group">
                  <label>Last Name:</label>
                  <input
                    type="text"
                    name="lname"
                    value={lname}
                    className="form-control"
                    placeholder="Enter Last Name"
                    onChange={event => this.setState({ lname: event.target.value })}
                    onFocus={() => this.setState({ lnameError: "" })}
                  />
                  <span style={{ color: 'red' }}>{this.state.lnameError ? `* ${this.state.lnameError}` : ''}</span>
                </div>
              </div>

              <div className="col-md-12">
                <div className="form-group">
                  <label>Email Address:</label>
                  <input
                    type="email"
                    name="email"
                    value={email}
                    className="form-control"
                    placeholder="Enter Email Address"
                    onChange={event => this.setState({ email: event.target.value })}
                    onFocus={() => this.setState({ emailError: "" })}
                  />
                  <span style={{ color: 'red' }}>{this.state.emailError ? `* ${this.state.emailError}` : ''}</span>
                </div>
              </div>

              <div className="col-md-5">
                <div className="form-group">
                  <label>Country Code:</label>
                  <select
                    className="form-control"
                    name="countryCode"
                    value={countryCode}
                    onChange={event => this.setState({ countryCode: event.target.value })}
                    onFocus={() => this.setState({ countryCodeError: "" })}
                  >
                    <option value="">Select country</option>
                    {countryCodeList && countryCodeList.map((data, index) => {
                      return <option value={data.code}>{data.name + ' ' + data.code}</option>
                    })}
                  </select>
                  <span style={{ color: 'red' }}>{this.state.countryCodeError ? `* ${this.state.countryCodeError}` : ''}</span>
                </div>
              </div>

              {/* <div className="col-md-4">
                <div className="form-group">
                  <label>Country Code:</label>
                  <ReactTelInput
                    defaultCountry='us'
                    value={countryCode}
                    onChange={event => this.setState({ countryCode: event.target.value })}
                    onlyCountries={[
                      { name: "United States", iso2: "us", dialCode: "1", priority: 0, format: "+. (...) ...-...." },
                      { name: "Canada", iso2: "ca", dialCode: "1", priority: 1, format: "+. (...) ...-....", hasAreaCodes: true },
                      { name: "Mexico (México)", iso2: "mx", dialCode: "52", priority: 0, format: "+..-..-..-...." },
                      { name: "Brazil (Brasil)", iso2: "br", dialCode: "55", priority: 0, format: "+..-..-....-...." },
                    ]}
                  />
                </div>
              </div> */}

              <div className="col-md-7">
                <div className="form-group">
                  <label>Mobile Number:</label>
                  <input
                    type="tel"
                    name="mobile"
                    value={mobile}
                    className="form-control"
                    placeholder="Enter Mobile Number"
                    onChange={event => this.setState({ mobile: event.target.value })}
                    onFocus={() => this.setState({ mobileError: "" })}
                  />
                  <span style={{ color: 'red' }}>{this.state.mobileError ? `* ${this.state.mobileError}` : ''}</span>
                </div>
              </div>

              <div className="col-md-6">
                <div className="form-group">
                  <label>Service:</label>
                  <select className="form-control" name="service" value={service}
                    onChange={event => this.setState({ service: event.target.value })}
                    onClick={() => this.setState({ serviceError: "" })}
                  >
                    <option value="">Select Service</option>
                    {Object.keys(cred.OPERATOR_SERVICES).map(service => {
                      return <option value={service}>{cred.OPERATOR_SERVICES[service]}</option>
                    })}

                  </select>

                  <span style={{ color: 'red' }}>{this.state.serviceError ? `* ${this.state.serviceError}` : ''}</span>
                </div>
              </div>

              <div className="col-md-6">
                <div className="form-group">
                  <label>Price Per Minute:</label>
                  <input
                    type="tel"
                    name="pricePerMin"
                    value={pricePerMin}
                    className="form-control"
                    placeholder="Enter Price Per Minute"
                    onChange={event => this.setState({ pricePerMin: event.target.value })}
                    onFocus={() => this.setState({ pricePerMinError: "" })}
                  />

                  <span style={{ color: 'red' }}>{this.state.pricePerMinError ? `* ${this.state.pricePerMinError}` : ''}</span>
                </div>
              </div>

            </div>
          </Modal.Body>
          <Modal.Footer>

            <div className="row">
              <div className="col-md-6">
                <div className="form-group" style={{ marginRight: "25px" }}>
                  {loading ?
                    <Button variant="primary"> <i className="fa fa-spinner fa-spin" ></i> </Button>
                    :
                    null}
                  {!loading ?
                    <Button variant="primary" onClick={this.addNewUser}> Save </Button>
                    :
                    null}
                </div>
              </div>
              <div className="col-md-6">

              </div>
            </div>

          </Modal.Footer>
        </Modal>

        <Modal show={this.state.editUser} onHide={() => { this.clearState() }}>
          <Modal.Header closeButton>
            <Modal.Title>Edit Operator</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="row">

              <div className="col-md-6">
                <div className="form-group">
                  <label>First Name:</label>
                  <input
                    type="text"
                    name="fname"
                    value={fname}
                    className="form-control"
                    placeholder="Enter First Name"
                    onChange={event => this.setState({ fname: event.target.value })}
                    onFocus={() => this.setState({ fnameError: "" })}
                  />
                  <span style={{ color: 'red' }}>{this.state.fnameError ? `* ${this.state.fnameError}` : ''}</span>
                </div>
              </div>

              <div className="col-md-6">
                <div className="form-group">
                  <label>Last Name:</label>
                  <input
                    type="text"
                    name="lname"
                    value={lname}
                    className="form-control"
                    placeholder="Enter Last Name"
                    onChange={event => this.setState({ lname: event.target.value })}
                    onFocus={() => this.setState({ lnameError: "" })}
                  />
                  <span style={{ color: 'red' }}>{this.state.lnameError ? `* ${this.state.lnameError}` : ''}</span>
                </div>
              </div>

              <div className="col-md-12">
                <div className="form-group">
                  <label>Email Address:</label>
                  <input
                    type="email"
                    name="email"
                    value={email}
                    className="form-control"
                    placeholder="Enter Email Address"
                    onChange={event => this.setState({ email: event.target.value })}
                    onFocus={() => this.setState({ emailError: "" })}
                  />
                  <span style={{ color: 'red' }}>{this.state.emailError ? `* ${this.state.emailError}` : ''}</span>
                </div>
              </div>

              <div className="col-md-5">
                <div className="form-group">
                  <label>Country Code:</label>
                  <select
                    className="form-control"
                    name="countryCode"
                    value={countryCode}
                    onChange={event => this.setState({ countryCode: event.target.value })}
                    onFocus={() => this.setState({ countryCodeError: "" })}
                  >
                    <option value="">Select country</option>
                    {countryCodeList && countryCodeList.map((data, index) => {
                      return <option value={data.code}>{data.name + ' ' + data.code}</option>
                    })}
                  </select>
                  <span style={{ color: 'red' }}>{this.state.countryCodeError ? `* ${this.state.countryCodeError}` : ''}</span>
                </div>
              </div>

              <div className="col-md-7">
                <div className="form-group">
                  <label>Mobile Number:</label>
                  <input
                    type="tel"
                    name="mobile"
                    value={mobile}
                    className="form-control"
                    placeholder="Enter Mobile Number"
                    onChange={event => this.setState({ mobile: event.target.value })}
                    onFocus={() => this.setState({ mobileError: "" })}
                  />
                  <span style={{ color: 'red' }}>{this.state.mobileError ? `* ${this.state.mobileError}` : ''}</span>
                </div>
              </div>

              <div className="col-md-6">
                <div className="form-group">
                  <label>Service:</label>
                  <select className="form-control" name="service" value={service}
                    onChange={event => this.setState({ service: event.target.value })}
                    onClick={() => this.setState({ serviceError: "" })}
                  >
                    <option value="">Select Service</option>
                    {Object.keys(cred.OPERATOR_SERVICES).map(service => {
                      return <option value={service}>{cred.OPERATOR_SERVICES[service]}</option>
                    })}
                  </select>

                  <span style={{ color: 'red' }}>{this.state.serviceError ? `* ${this.state.serviceError}` : ''}</span>
                </div>
              </div>

              <div className="col-md-6">
                <div className="form-group">
                  <label>Price Per Minute:</label>
                  <input
                    type="tel"
                    name="pricePerMin"
                    value={pricePerMin}
                    className="form-control"
                    placeholder="Enter Price Per Minute"
                    onChange={event => this.setState({ pricePerMin: event.target.value })}
                    onFocus={() => this.setState({ pricePerMinError: "" })}
                  />

                  <span style={{ color: 'red' }}>{this.state.pricePerMinError ? `* ${this.state.pricePerMinError}` : ''}</span>
                </div>
              </div>

              <div className="col-md-8">
                <div className="form-group">
                  {isActive ?
                    <Button variant="success" style={{ backgroundColor: '#28a745' }}
                      onClick={() => { this.setState({ isActive: false }) }}
                    >
                      Active
                    </Button>
                    :
                    <Button variant="secondary" style={{ backgroundColor: '#6c757d' }}
                      onClick={() => { this.setState({ isActive: true }) }}
                    >
                      In Active
                    </Button>
                  }
                </div>
              </div>

            </div>
          </Modal.Body>
          <Modal.Footer>

            <div className="row">
              <div className="col-md-6">
                <div className="form-group" style={{ marginRight: "25px" }}>
                  {loading ?
                    <Button variant="primary"> <i className="fa fa-spinner fa-spin" ></i> </Button>
                    :
                    null}
                  {!loading ?
                    <Button variant="primary" onClick={this.editOperatorDetails}> Save </Button>
                    :
                    null}
                </div>
              </div>
              <div className="col-md-6">

              </div>
            </div>


          </Modal.Footer>
        </Modal>

        <Modal show={this.state.showDeleteModal} onHide={() => this.hideDeleteModal()}>
          <Modal.Header closeButton>
            <Modal.Title>Delete Operator confirm</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h3>Are you sure to delete {this.state.deleteItem}</h3>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={() => this.deleteOperator()}>
              Yes
          </Button>
            <Button variant="primary" onClick={() => this.hideDeleteModal()}>
              No
          </Button>
          </Modal.Footer>
        </Modal>

        <Modal show={this.state.showSuccessAlert} onHide={() => this.hideSuccessAlert()}>
          <Modal.Header closeButton>
            <Modal.Title>OK</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h5 style={{ color: 'green' }}>{`${this.state.successAlertMessage}`}</h5>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="primary" onClick={() => this.hideSuccessAlert()}>
              Ok
          </Button>
          </Modal.Footer>
        </Modal>

        <Modal show={this.state.showErrorAlert} onHide={() => this.hideErrorAlert()}>
          <Modal.Header closeButton>
            <Modal.Title>OK</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h5 style={{ color: 'red' }}>{`${this.state.errorAlertMessage}`}</h5>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="primary" onClick={() => this.hideErrorAlert()}>
              Ok
          </Button>
          </Modal.Footer>
        </Modal>

      </main>
    );
  }
}

export default checkAuthentication(OperatorManagement);
