import React, { Component } from "react";
import Navbar from "../Component/Navbar";
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button } from "react-bootstrap"
import _ from 'lodash';
import { CSVLink, CSVDownload } from "react-csv";
import ReactTooltip from 'react-tooltip'; // For tool tip 

import { checkAuthentication } from '../Component/Authentication';
import ChartsPage from '../Component/barChart';

var path = cred.API_PATH;

const minOffset = 0;
const maxOffset = 60;

export class OperatorManagementStat extends Component {

    constructor(props) {
        super(props);
        this.state = {

            userId: '',
            userType: '',
            companyId: '',
            selectedOperatorId: '',
            selectedOperatorService: '',
            selectedOperatorName: '',
            selectedOperatorStatus: '',

            durationDetails: [],
            graphData: [],

            loading: false,

            openSideMenu: false,

            searchString: (new Date()).getFullYear(),
            searchType: '',
            searching: false,

            thisYear: (new Date()).getFullYear(),
            
            // For bar charts--
            labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],


        }
    }

    componentDidMount() {
        document.body.classList.remove("sign-bg");
        document.body.classList.add("grey-bg");

        if (this.props.location.state && this.props.location.state.service && this.props.location.state.name && this.props.location.state.isActive) {

            let storage = localStorage.getItem("MIO_Local");
            storage = storage ? JSON.parse(storage) : null;

            let selectedOperatorId = this.props.match.params.id;
            let selectedOperatorService = this.props.location.state.service;
            let selectedOperatorName = this.props.location.state.name;
            let selectedOperatorStatus = this.props.location.state.isActive;

            if (storage && storage.data.userId) {
                this.setState({
                    userId: storage.data.userId,
                    userType: storage.data.userType,
                    companyId: storage.data.companyId,
                    selectedOperatorId: selectedOperatorId,
                    selectedOperatorService: selectedOperatorService,
                    selectedOperatorName: selectedOperatorName,
                    selectedOperatorStatus: selectedOperatorStatus
                }, () => {
                    this.getOperatorUsage(this.state.searchString);
                });

                // let tokenData = this.parseJwt(storage.token);

            }
        } else {
            this.props.history.push("/operatormanagement");
        }

    }


    getOperatorUsage = (year) => {

        let dataToSend = {
            "operatorId": this.state.selectedOperatorId,
            // "operatorId": "5e182c12240ac91830510960",
            "service": this.state.selectedOperatorService,
            "year": year,
            "companyId": ""
        };

        axios
            .post(path + 'conference/get-users-webcall-duration', dataToSend)
            .then(serverResponse => {

                let res = serverResponse.data;
                if (!res.isError) {
                    console.log("Operator usage details---", res);

                    let graphData = [];

                    if (res.details) {

                        let durationDetails = res.details && res.details.webCallDurationDetail;
                        durationDetails && durationDetails.map((value, index) => {
                            let hours = parseFloat(parseFloat(value.totalHours)).toFixed('2');
                            graphData.push(hours);
                        });

                        this.setState({
                            // durationDetails: durationDetails,
                            graphData: graphData,
                        }, () => {
                            console.log("graphData===", graphData);
                        });
                    } else {
                        this.setState({
                            graphData: graphData
                        }, () => {
                            console.log("graphData===", graphData);
                        });
                    }

                }

            })
            .catch(error => {
                console.log(error);
            })

    }


    searchByYear = (event) => {

        event.preventDefault();
        this.setState({
            searching: true
        },()=>{
            this.getOperatorUsage(this.state.searchString)
        });
        

    }

    sideMenuToggle() {
        let { openSideMenu } = this.state;
        openSideMenu = !openSideMenu;
        this.setState({ openSideMenu });
    }

    mobMenuClick() {
        this.sideMenuToggle();
        console.log('mobMenuClick');
    }

    backMenueToggle= ()=>{
        this.props.history.goBack();
    }

    parseJwt = (token) => {
        if (!token) { return; }
        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace('-', '+').replace('_', '/');
        return JSON.parse(window.atob(base64));
    }


    render() {
        let { durationDetails, searchString, searchType, searching, thisYear } = this.state;
        var options = [];
        for (let i = minOffset; i <= maxOffset; i++) {
            let year = thisYear - i;
            options.push(<option value={year} key={i}>{year}</option>);
        }
        return (

            <main>

                <section className="user-mngnt-wrap">
                    <div className="container-fluid">
                        <div className="row">
                            <Navbar routTo="/operatormanagement" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />

                            <div className="col-xl-10 col-lg-9 p-0 mob-full-conf">

                                <div className="mobile-menu-header">
                                    <a href="JavaScript:Void(0);" className="back-arw" onClick={() => this.backMenueToggle()}><i className="fas fa-arrow-left"></i></a>
                                    {/* <a href="#" className="menu-tgl" onClick={() => this.sideMenuToggle()}>
                                        <img src="images/menu-tgl.png" alt="" />
                                    </a> */}
                                </div>

                                <div className="paymnt-mng-hdr" style={{ margin: "30px" }}>
                                    <div className="blnc">
                                        <div className="logobox"><img src="/images/logo-sign.png" alt="" /></div>
                                    </div>
                                    <div className="rchrg-btn">
                                        <p className="text-right" style={{ margin: "0" }}>
                                            Operator Name: {this.state.selectedOperatorName}
                                            <br />Service: {this.state.selectedOperatorService}
                                        </p>
                                    </div>
                                </div>
                                {/* style={{ padding: "0px 50px" }} */}
                                <div className="statistic-wrap">
                                    <div className="statistic-srch">
                                        <form>
                                            <div className="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>Year</label>
                                                        <select class="form-control"
                                                            name="searchString"
                                                            vlaue={searchString}
                                                            onChange={(event) => { this.setState({ searchString: event.target.value }) }}
                                                        >
                                                            <option value="">Select Year</option>
                                                            {options}
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-md-1">
                                                    <div class="form-group">
                                                        <input type="submit"
                                                            onClick={(event) => { this.searchByYear(event) }}
                                                        />
                                                    </div>
                                                </div>
                                                    
                                            </div>
                                        </form>
                                    </div>

                                    <div class="statistic-graph">
                                        {/* <img src="images/chart.png" alt="" /> */}
                                        {this.state.graphData.length > 0 ?
                                            <ChartsPage
                                                labels={this.state.labels}
                                                label="month"
                                                data={this.state.graphData}
                                            />
                                            :
                                            <div>
                                                No Data Found
                                        </div>}

                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>

                </section>

            </main>

        );

    }

}

export default checkAuthentication(OperatorManagementStat);