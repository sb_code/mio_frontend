import React, { Component } from "react";
import Navbar from "../Component/Navbar";
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button } from "react-bootstrap"
import _ from 'lodash';
import { CSVLink, CSVDownload } from "react-csv";
import ReactTooltip from 'react-tooltip'; // For tool tip 

import { checkAuthentication } from '../Component/Authentication';

var path = cred.API_PATH;

export class AdminConferenceList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userId: '',
            selectedCompanyId: '',
            selectedCompanyName: '',
            selectedCompanyType: '',

            openSideMenu: false,
            searchString: '',
            searchType: '',
            searching: false,

            conferenceList: [],
            page: 1,
            listRecords: '',

            loading: false,
            searchLoading: false,

            serviceName: '',
            costPerMin: '',
            multiplier: '',
            pricePerMin: '',

            editUser: false,

        }
        this.perPage = 5;
    }

    componentDidMount() {
        document.body.classList.remove("sign-bg");
        document.body.classList.add("grey-bg");

        if (this.props.location.state && this.props.location.state.name && this.props.location.state.type) {

            let storage = localStorage.getItem("MIO_Local");
            storage = storage ? JSON.parse(storage) : null;
            let selectedCompanyId = this.props.match.params.id;
            let selectedCompanyName = this.props.location.state.name;
            let selectedCompanyType = this.props.location.state.type;

            if (storage && storage.data.userId) {
                this.setState({
                    userId: storage.data.userId,
                    userType: storage.data.userType,
                    companyId: storage.data.companyId,
                    selectedCompanyId: selectedCompanyId,
                    selectedCompanyName: selectedCompanyName,
                    selectedCompanyType: selectedCompanyType
                }, () => {
                    this.getConferenceListForAdmin()
                    // console.log('selected conference --', this.state.selectedCompanyId);
                });

                // console.log(this.parseJwt(storage.token));
                let tokenData = this.parseJwt(storage.token);

            }
        } else {
            this.props.history.push("/adminconference");
        }

    }

    getConferenceListForAdmin = () => {
        this.setState({loading: true});
        let dataToSend = {
            "companyId": this.state.selectedCompanyId,
            "page": this.state.page,
        	"perPage": this.perPage

        };

        axios
            .post(path + 'conference/list-conference-company', dataToSend)
            .then(serverResponse => {
                let res = serverResponse.data;
                if (!res.isError) {
                    // console.log(" response for get conference list--", res);
                    let conferenceList = res.details && res.details[0] && res.details[0].results;
                    this.setState({
                        loading: false,
                        listRecords: res.details && res.details[0] && res.details[0].noofpage,
                        conferenceList: conferenceList
                    });
                }else{
                    this.setState({loading: false});
                }
            })
            .catch(error => {
                console.log(error);
            })

    }


    searchConference = (event) => {
        event.preventDefault();
        this.setState({
            searching: true,
            searchLoading: true
        });

        let dataToSend = {
            "conferenceName": this.state.searchString,
            "companyId": this.state.selectedCompanyId
        };
        // API ToDo - 
        axios
            .post(path + 'conference/search-conference', dataToSend)
            .then(serverResponse => {
                let res = serverResponse.data;
                if (!res.isError) {
                    let conferenceList = res.details;
                    this.setState({
                        searchLoading: false,
                        conferenceList: conferenceList
                    });
                } else {
                    alert("Something is wrong---");
                    this.setState({
                        searching: false,
                        searchLoading: false
                    }, () => {
                        this.getConferenceListForAdmin();
                    })
                }
            })
            .catch(error => {
                this.setState({
                    searching: false,
                    searchLoading: false
                })
                console.log(error);
            })

    }

    clearSearch = (event) => {
        event.preventDefault()

        this.setState({
            searching: false,
            searchString: '',
            searching: false,
            searchLoading: false
        }, () => {
            this.getConferenceListForAdmin();
        });
    }

    viewArchives = (index) => {
        // console.log("View archives ---- ");
        let { conferenceList, selectedCompanyName, selectedCompanyId, selectedCompanyType } = this.state;

        let { _id, conferenceTitle } = conferenceList && conferenceList[index];

        this.props.history.push({
            pathname: '/archives/' + `${selectedCompanyId}` + '/' + `${_id}`,
            state: {
                name: selectedCompanyName,
                type: selectedCompanyType,
                conference: conferenceTitle
            }
        });

    }

    editUsers = () => {
        let dataToSend = {
            "serviceName": this.state.serviceName,
            "costPerMin": this.state.costPerMin,
            "multiplier": this.state.multiplier,
            "pricePerMin": this.state.pricePerMin
        };
        // API ToDo
        console.log("dataToSave -- ", dataToSend);

    }

    clearState = (event) => {
        this.setState({
            loading: false,
            searching: false,
            serviceName: '',
            costPerMin: '',
            multiplier: '',
            pricePerMin: '',

            editUser: false,
        });
    }

    pageChangePrev = () => {

        this.setState({
            page: (parseFloat(this.state.page) - 1)
        }, () => {
            this.getConferenceListForAdmin()
        });

    }

    pageChangeNext = () => {

        this.setState({
            page: (parseFloat(this.state.page) + 1)
        }, () => {
            this.getConferenceListForAdmin()
        });

    }

    sideMenuToggle() {
        let { openSideMenu } = this.state;
        openSideMenu = !openSideMenu;
        this.setState({ openSideMenu });
    }

    mobMenuClick() {
        this.sideMenuToggle();
        console.log('mobMenuClick');
    }

    backMenueToggle= ()=>{
        this.props.history.goBack();
    }

    parseJwt = (token) => {
        if (!token) { return; }
        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace('-', '+').replace('_', '/');
        return JSON.parse(window.atob(base64));
    }


    render() {
        let {loading, searchLoading, searching, listRecords, page, searchString, serviceName, costPerMin, multiplier, pricePerMin } = this.state;
        return (
            <main>

                <section className="user-mngnt-wrap">
                    <div className="container-fluid">
                        <div className="row">
                            <Navbar routTo="/adminconference" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />

                            <div className="col-xl-10 col-lg-9 p-0">

                                <div className="mobile-menu-header">
                                    <a href="JavaScript:Void(0);" className="back-arw" onClick={()=> this.backMenueToggle()}><i className="fas fa-arrow-left"></i></a>
                                    
                                </div>

                                <div className="paymnt-mng-hdr" style={{ margin: "30px" }}>
                                    <div className="blnc">
                                        <div className="logobox"><img src="/images/logo-sign.png" alt="" /></div>
                                    </div>
                                    <div className="rchrg-btn">
                                        <p className="text-right" style={{ margin: "0" }}>
                                            Company Name: {this.state.selectedCompanyName}
                                            <br />Company Type: {this.state.selectedCompanyType}
                                        </p>
                                    </div>
                                </div>

                                <div className="mng-full-list" style={{ padding: "0px 50px" }}>
                                    <div className="mng-full-srch">
                                        <div className="row">
                                            <div className="col-md-10">
                                                <div className="srch-wrap">
                                                    <form>
                                                        <input type="text" name="searchString" value={searchString}
                                                            className="form-control" placeholder="Search conference by name"
                                                            onChange={(event) => { this.setState({ searchString: event.target.value }) }}
                                                        />
                                                        {this.state.searching ?
                                                            <input type="button" onClick={(event) => this.clearSearch(event)}
                                                                // ref={ref => this.fooRef = ref}
                                                                data-tip='Clear'
                                                            // onFocus={() => { ReactTooltip.show(this.fooRef) }}
                                                            />
                                                            :
                                                            <input type="submit" onClick={(event) => this.searchConference(event)}
                                                                // ref={ref => this.fooRef = ref}
                                                                data-tip='Search'
                                                            // onFocus={() => { ReactTooltip.show(this.fooRef) }}
                                                            />
                                                        }
                                                        <ReactTooltip
                                                            effect="float"
                                                            place="top"
                                                            data-border="true"
                                                        />
                                                    </form>
                                                </div>
                                            </div>
                                            {searchLoading?
                                                <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                                                    <i className="fa fa-spinner fa-spin" ></i>
                                                </a>
                                            :null}
                                        </div>
                                    </div>


                                    <div className="mng-full-table">
                                        <div className="row">

                                            <div className="col-md-10">
                                                <div className="mng-full-table-hdr">
                                                    <div className="row">
                                                        <div className="col-md-4 border-rt">
                                                            <h6>Conference Name</h6>
                                                        </div>
                                                        <div className="col-md-4 border-rt">
                                                            <h6>Recording Preference</h6>
                                                        </div>
                                                        <div className="col-md-4">
                                                            <h6>No of Participants</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="col-md-2">
                                                <div className="mng-full-table-hdr">
                                                    <div className="row">
                                                        <div className="col-md-12">
                                                            <h6>Archives</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        {this.state.conferenceList && this.state.conferenceList.map((user, index) => {
                                            return <div className="row" key={index}>

                                                <div className="col-md-10">
                                                    <div className="mng-full-table-row">
                                                        <div className="row">
                                                            <div className="col-md-4 border-rt">
                                                                <h6>Conference Name</h6>
                                                                <p className="textEllips">
                                                                    <a href="JavaScript:Void(0);" onClick={() => this.viewArchives(index)}>
                                                                        {user.conferenceTitle}
                                                                    </a>
                                                                </p>
                                                            </div>
                                                            <div className="col-md-4 border-rt">
                                                                <h6>Recording Preference</h6>
                                                                <p className="textEllips">{user.recordingPreference}</p>
                                                            </div>
                                                            <div className="col-md-4">

                                                                <h6>No of Participants</h6>
                                                                <p className="textEllips">
                                                                    {user.participants && user.participants.length}
                                                                </p>

                                                            </div>

                                                            <div className="mobile-ad-edt-btns">
                                                                <ul>
                                                                    <li onClick={() => this.viewArchives(index)}>
                                                                        <a href="JavaScript:Void(0);"
                                                                            // ref={ref => this.fooRef = ref}
                                                                            data-tip='View'
                                                                        // onFocus={() => { ReactTooltip.show(this.fooRef) }}
                                                                        >
                                                                            <i className="fas fa-play"></i>

                                                                        </a>
                                                                        <ReactTooltip
                                                                            effect="float"
                                                                            place="top"
                                                                            data-border="true"
                                                                        />
                                                                    </li>

                                                                </ul>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>

                                                <div className="col-md-2">
                                                    <div className="mng-full-table-row add-edt text-center">
                                                        <div className="row">
                                                            <div className="col-md-12">
                                                                <a href="JavaScript:Void(0);"
                                                                    // ref={ref => this.fooRef1 = ref}
                                                                    data-tip='View'
                                                                    onClick={() => { this.viewArchives(index) }}
                                                                // onFocus={() => { ReactTooltip.show(this.fooRef1) }}
                                                                >
                                                                    <i className="fas fa-play"></i>

                                                                </a>
                                                                <ReactTooltip
                                                                    effect="float"
                                                                    place="top"
                                                                    data-border="true"
                                                                />
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        })}

                                    </div>

                                    <br /><br />
                                {!searching ?
                                    <div className="form-group">
                                        {!loading && parseFloat(page) > 1 ?
                                            <span style={{ color: "#007bff", cursor: "pointer" }}
                                                onClick={() => { this.pageChangePrev() }}
                                            >
                                                <b>{"<< Prev"}</b>
                                            </span>
                                            :
                                            null}
                                            &nbsp;&nbsp;|&nbsp;&nbsp;
                                            {!loading && (parseFloat(page) < parseFloat(listRecords)) ?
                                            <span style={{ color: "#007bff", cursor: "pointer" }}
                                                onClick={() => { this.pageChangeNext() }}
                                            >
                                                <b>{"Next >>"}</b>
                                            </span>
                                            :
                                            null}
                                        {loading ?
                                            <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                                                <i className="fa fa-spinner fa-spin" ></i>
                                            </a>
                                            :
                                            null}

                                    </div>
                                    :
                                    null}

                                </div>

                            </div>

                        </div>
                    </div>
                </section>

                <Modal show={this.state.editUser} onHide={() => { this.clearState() }}>
                    <Modal.Header closeButton>
                        <Modal.Title> Edit Service</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="row">

                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>Service Name:</label>
                                    <input
                                        type="text"
                                        name="serviceName"
                                        value={serviceName}
                                        className="form-control"
                                        placeholder="Enter Company Name"
                                        onChange={event => this.setState({ serviceName: event.target.value })}
                                        onFocus={() => this.setState({ serviceNameError: "" })}
                                    />
                                    <span style={{ color: 'red' }}>{this.state.serviceNameError ? `* ${this.state.serviceNameError}` : ''}</span>
                                </div>
                            </div>

                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>Cost Per Minute:</label>
                                    <input
                                        type="text"
                                        name="costPerMin"
                                        value={costPerMin}
                                        className="form-control"
                                        placeholder="Enter Cost Per Minute"
                                        onChange={event => this.setState({ costPerMin: event.target.value })}
                                        onFocus={() => this.setState({ costPerMinError: "" })}
                                    />
                                    <span style={{ color: 'red' }}>{this.state.costPerMinError ? `* ${this.state.costPerMinError}` : ''}</span>
                                </div>
                            </div>

                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>Multiplier:</label>
                                    <input
                                        type="text"
                                        name="multiplier"
                                        value={multiplier}
                                        className="form-control"
                                        placeholder="Enter multiplier"
                                        onChange={event => this.setState({ multiplier: event.target.value })}
                                        onFocus={() => this.setState({ multiplierError: "" })}
                                    />
                                    <span style={{ color: 'red' }}>{this.state.multiplierError ? `* ${this.state.multiplierError}` : ''}</span>
                                </div>
                            </div>

                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>Price Per Minute:</label>
                                    <input
                                        type="text"
                                        name="pricePerMin"
                                        value={pricePerMin}
                                        className="form-control"
                                        placeholder="Enter Price Per Minute"
                                        onChange={event => this.setState({ pricePerMin: event.target.value })}
                                        onFocus={() => this.setState({ pricePerMinError: "" })}
                                    />
                                    <span style={{ color: 'red' }}>{this.state.pricePerMinError ? `* ${this.state.pricePerMinError}` : ''}</span>
                                </div>
                            </div>

                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="primary" onClick={() => this.editUsers()}>
                            Save
                        </Button>
                    </Modal.Footer>
                </Modal>

            </main>
        );
    }

}

export default checkAuthentication(AdminConferenceList);