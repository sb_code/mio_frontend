/* Let CRA handle linting for sample app */
import React, { Component } from 'react';
import Spinner from 'react-spinner';
import classNames from 'classnames';

import Navbar from '../Component/Navbar';
import { Modal, Button, Badge, Toast } from "react-bootstrap";
import ProgressBar from 'react-bootstrap/ProgressBar';
import axios from "axios";
import cred from "../../cred.json";

import AccCore from 'opentok-accelerator-core';
import _ from 'lodash';

import { checkAuthentication } from '../Component/Authentication';

import ReactTooltip from 'react-tooltip' // For tool tip 


// import {FileUploader, FileManager} from 'reactjs-file-uploader';

// import 'opentok-solutions-css';

// import logo from './logo-sign.png';
// import config from './config.json';
// import './App.css';

var path = cred.API_PATH;

let otCore;
const otCoreOptions = {
	credentials: {
		apiKey: "46425072",
		sessionId: "2_MX40NjQyNTA3Mn5-MTU3NDc2NjQ3MTExOX53Ly9YajRiNUs4MmovcmFWRElsUmdyUDJ-fg",
		token: "T1==cGFydG5lcl9pZD00NjQyNTA3MiZzaWc9NWM1ZDlmNmViY2YzMTljMzdkYmFjZWUzMDU0YmZjNTU0ZTUyOTNjNDpzZXNzaW9uX2lkPTJfTVg0ME5qUXlOVEEzTW41LU1UVTNORGMyTmpRM01URXhPWDUzTHk5WWFqUmlOVXM0TW1vdmNtRldSRWxzVW1keVVESi1mZyZjcmVhdGVfdGltZT0xNTc0ODUwNjQzJm5vbmNlPTAuMjEyODI2OTQ0Mzg3NTc4NTQmcm9sZT1wdWJsaXNoZXImZXhwaXJlX3RpbWU9MTU3NDkzNzA0MyZpbml0aWFsX2xheW91dF9jbGFzc19saXN0PQ==",
	},
	// A container can either be a query selector or an HTML Element
	streamContainers(pubSub, type, data, stream) {
		return {
			publisher: {
				camera: '#cameraPublisherContainer',
				screen: '#screenPublisherContainer',
			},
			subscriber: {
				camera: '#cameraSubscriberContainer',
				screen: '#screenSubscriberContainer',
			},
		}[pubSub][type];
	},
	controlsContainer: '#controls',
	packages: ['textChat', 'screenSharing'],
	// packages: ['textChat', ', 'annotation'],
	communication: {
		callProperties: null, // Using default
	},
	textChat: {
		name: ['David', 'Paul', 'Emma', 'George', 'Amanda'][Math.random() * 5 | 0], // eslint-disable-line no-bitwise
		waitingMessage: '',
		container: '#chat',
		alwaysOpen: true,
	},
	screenSharing: {
		extensionID: 'plocfffmbcclpdifaikiikgplfnepkpo',
		// annotation: true,
		externalWindow: false,
		dev: true,
		screenProperties: {
			insertMode: 'append',
			width: '100%',
			height: '100%',
			showControls: true,
			style: {
				buttonDisplayMode: 'on',
			},
			videoSource: 'window',
			fitMode: 'contain' // Using default
		},
	},
	// annotation: {
	//   absoluteParent: {
	//     publisher: '.App-video-container',
	//     subscriber: '.App-video-container'
	//   }
	// },
};

/**
 * Build classes for container elements based on state
 * @param {Object} state
 */
const containerClasses = (state) => {
	const { active, meta, localAudioEnabled, localVideoEnabled, isPresenter, isChatOpen } = state;
	const sharingScreen = meta ? !!meta.publisher.screen : false;
	const viewingSharedScreen = meta ? meta.subscriber.screen : false;
	const activeCameraSubscribers = meta ? meta.subscriber.camera : 0;
	const activeCameraSubscribersGt2 = activeCameraSubscribers > 2;
	const activeCameraSubscribersOdd = activeCameraSubscribers % 2;
	const screenshareActive = viewingSharedScreen || sharingScreen;

	return {
		controlClass: classNames('App-control-container', { hidden: !active }),
		localAudioClass: classNames('ots-video-control circle audio', { hidden: !active, muted: !localAudioEnabled }),
		localVideoClass: classNames('ots-video-control circle video', { hidden: !active, muted: !localVideoEnabled }),
		localCallClass: classNames('ots-video-control circle end-call', { hidden: !active }),
		cameraPublisherClass: classNames('video-container', { hidden: !active, small: !!activeCameraSubscribers || screenshareActive, left: screenshareActive }),
		// screenPublisherClass: classNames('video-container', { hidden: !active || !sharingScreen }),
		screenPublisherClass: classNames('video-container', { hidden: !active || !sharingScreen || isPresenter || viewingSharedScreen }),
		cameraSubscriberClass: classNames('video-container', { hidden: !active || !activeCameraSubscribers },
			{ 'active-gt2': activeCameraSubscribersGt2 && !screenshareActive },
			{ 'active-odd': activeCameraSubscribersOdd && !screenshareActive },
			{ small: screenshareActive }
		),
		screenSubscriberClass: classNames('video-container', { hidden: !viewingSharedScreen || !active }),
		appChatOuterWrap: classNames('app-chat-outer-wrap', { displaynone: !active || !isChatOpen }),
	};
};

const connectingMask = () =>
	<div className="App-mask">
		{/* <Spinner /> */}
		<div className="message with-spinner">Select a conference</div>
	</div>;

const startCallMask = start =>
	<div className="App-mask">
		<button className="message button clickable" onClick={start}>Click to Join Conference / Start Call </button>
	</div>;

const loadingMask = () =>
	<div className="App-mask">
		{/* <Spinner /> */}
		<div className="message with-spinner">Loading...</div>
	</div>;

export class AdminCompanyConference extends Component {

	constructor(props) {
		super(props);
		this.state = {
			showParticipantsModal: false,
			openSideMenu: false,
			openSubMenuMob: true,
			showCreateConference: false,
			maxNoOfParticipants: '',
			conferenceTitle: '',
			userId: '',
			hasAccess: false,
			conferenceTitleError: '',
			maxNoOfParticipantsError: '',

			selectedCompanyId: '',
			selectedCompanyName: '',

			conferenceList: [],
			selectedConference: {},

			sessionId: '',
			token: '',

			starting: false,
			selecting: false,
			connected: false,
			active: false,
			publishers: null,
			subscribers: null,
			meta: null,
			localAudioEnabled: true,
			localVideoEnabled: true,

			showAddressBook: false,
			contactList: [],

			inboundRadio: false,
			invitationCode: '',
			callInitiator: '',
			timeOutHrs: '',
			timeOutMins: '',
			durationHrs: '',
			durationMins: '',
			recordingPreference: '',
			// recordingCheck: true,
			fileShare: true,
			inboundCall: false,
			screenWithoutPermission: true,
			screenWithPermission: false,
			conferenceMode: '',
			currentScreen: {},

			conferenceMember: '',
			selectedConferenceId: '',
			inboundConnectionId: '', // For nexmo call 
			showMember: false,
			sipCallModal: false,
			showDeleteModal: false,
			conferenceToDelete: '',
			isPresenter: false, // For screen sharing 

			speakers: [],

			sipNo: '',
			sipCountryCode: '',
			// sipStatusCount: {}, // For getting sip call status 

			files: [],
			imageURL: '',
			fileUploaded: '',
			// uploading: false,

			actionObserver: {},

			currentStream: [],
			myConnectionDetails: {},

			// For show /hide toast --
			showToast: false,
			toastTitle: '',
			toastMessage: '',

			isFullScreen: false,
			isChatOpen: true,

			// manual chat 
			chatMessage: '',


		};
		this.startCall = this.startCall.bind(this);
		this.endCall = this.endCall.bind(this);
		this.toggleLocalAudio = this.toggleLocalAudio.bind(this);
		this.toggleLocalVideo = this.toggleLocalVideo.bind(this);
		this.getSIPStatus = null;
	}

	componentDidMount() {
		document.body.classList.remove("sign-bg");
		document.body.classList.add("grey-bg");

		let selectedCompanyId = this.props.match.params.id;
		let selectedCompanyName = this.props.location.state && this.props.location.state.companyName;

		// Fetch user from local storage
		let that = this;
		let storage = localStorage.getItem("MIO_Local");
		storage = storage ? JSON.parse(storage) : null;
		if (storage && storage.data.userId) {
			let hasAccess = false;
			if (storage.data.userType == cred.USER_TYPE_ADMIN || storage.data.userType == cred.USER_TYPE_COMPANY_ADMIN) {
				hasAccess = true;
			} else {
				hasAccess = false;
			}
			this.setState({
				userId: storage.data.userId,
				userType: storage.data.userType,
				selectedCompanyId: selectedCompanyId,
				selectedCompanyName: selectedCompanyName,
				hasAccess: hasAccess
			}, () => {
				that.fetchAddresses();
				that.getConferences();
				that.getCompanyDetailsByID(selectedCompanyId);
			});
		} else {
			this.props.history.push("/");
		}
	}

	componentWillUnmount() {
		let that = this;
		clearInterval(that.getSIPStatus);

	}


	videoFullScreenModeOn() {
		this.setState({ isFullScreen: true, isChatOpen: false });
		let element = document.getElementById('fullconf');
		element && element.classList.remove("col-xl-8");
		element && element.classList.add("col-xl-12");
	}


	videoFullScreenModeOff() {
		this.setState({ isFullScreen: false, isChatOpen: true });
		let element = document.getElementById('fullconf');
		element && element.classList.remove("col-xl-12");
		element && element.classList.add("col-xl-8");
	}


	getCompanyDetailsByID = (selectedCompanyId) => {

		let DataToSend = {
			companyId: selectedCompanyId
		};
		// API ToDo - 




	}


	getConferenceById = (id, contactNumber, countryCode) => {
		// return new Promise((resolve, reject) => {

		let dataToSend = {
			"conferenceId": id
		};

		axios
			.post(path + 'conference/fetch-conference-by-id', dataToSend)
			.then(serverResponse => {
				let res = serverResponse.data;
				if (!res.isError) {
					// console.log('New API result---- get conference by id--------- ', res);
					let participants = res.details && res.details[0].participants;
					let sessionId = res.details && res.details.sessionId;
					console.log('New API result---- get conference by id--------- / participants', participants);

					if (contactNumber && countryCode) {
						let callingParticipant = _.filter(participants, { 'contactNumber': contactNumber, 'countryCode': countryCode })[0];
						// console.log("SIP Call status --", callingParticipant)
						let state = callingParticipant && callingParticipant.state;
						let reason_code = callingParticipant && callingParticipant.reason_code;
						let reason_message = callingParticipant && callingParticipant.reason_message;
						let event = callingParticipant && callingParticipant.event;
						// console.log("SIP Call status -- ",reason_message, event );
						let sipData = {
							'contactNumber': contactNumber
						};

						console.log('fetch-conference-by-id --- / in setInterval -- ', sipData);
					}

					this.setState({
						conferenceMember: participants
					})

				} else {
					console.log('New API result---- get conference by id---------// error', res);
					// reject();
				}
			})
			.catch(error => {
				console.log(error);
				// reject();
			})

		// });
	}

	// getting total conference list -- 
	getConferences = () => {
		// return new Promise((resolve, reject) => {
		// 	let { userId } = this.state;
		// 	let that = this;
		// 	let dataToSend = {
		// 		"userId": userId,
		// 	};
		// 	console.log('dataToSend ', { dataToSend });

		// 	axios
		// 		.post(path + "conference/list-conference", dataToSend, {
		// 			headers: { "Content-Type": "application/json" }
		// 		})
		// 		.then(serverResponse => {
		// 			console.log("Response from getConferences : ", serverResponse.data);
		// 			const res = serverResponse.data;
		// 			if (!res.isError) {
		// 				let conferenceList = res.details;
		// 				console.log(res.details);

		// 				if (this.state.selectedConferenceId) {
		// 					let selectedConference = _.filter(conferenceList, { '_id': this.state.selectedConferenceId })[0];
		// 					this.setState({ conferenceMember: selectedConference.participants });
		// 					console.log('in get conference --- // selected conference members', selectedConference.participants);
		// 				}

		// 				that.setState({ conferenceList }, () => resolve());

		// 			} else {
		// 				reject();
		// 			}
		// 		}).catch(error => {
		// 			console.log(error);
		// 			reject();
		// 		});
		// });



		// For getting all conference lists of a company by companyId --
		return new Promise((resolve, reject) => {

			let dataToSend = {
				"companyId": this.state.selectedCompanyId
			};

			axios
				.post(path + 'conference/list-conference-company', dataToSend)
				.then(serverResponse => {

					let res = serverResponse.data;
					if (!res.isError) {
						// console.log("All conference lists of this company is ==", res);
						let conferenceList = res.details;

						if (this.state.selectedConferenceId) {
							let selectedConference = _.filter(conferenceList, { '_id': this.state.selectedConferenceId })[0];
							this.setState({ conferenceMember: selectedConference.participants });
							console.log('in get conference --- // selected conference members', selectedConference.participants);
						}

						this.setState({
							conferenceList: conferenceList
						}, () => {
							resolve();
						});

					} else {
						reject();
					}

				})
				.catch(error => {
					console.log(error);
					reject();
				})

		})



	}

	/**
	 * create a new conference
	 */
	createConference() {

		let that = this;
		let { userId, conferenceTitle, maxNoOfParticipants, conferenceTitleError, maxNoOfParticipantsError, contactList, callInitiator, conferenceMode, timeOutHrs, timeOutMins, durationHrs, durationMins, recordingPreference, recordingCheck, fileShare, inboundCall, screenWithoutPermission } = this.state;
		!(conferenceTitle) ? this.setState({ conferenceTitleError: "Please enter a title." }) : this.setState({ conferenceTitleError: "" });
		!(callInitiator) ? this.setState({ callInitiatorError: "Please select an initiator." }) : this.setState({ callInitiatorError: "" });
		!(conferenceMode) ? this.setState({ conferenceModeError: "Please select an mode." }) : this.setState({ conferenceModeError: "" });
		!(timeOutHrs) ? this.setState({ timeOutHrsError: "Please select timeout." }) : this.setState({ timeOutHrsError: "" });
		!(timeOutMins) ? this.setState({ timeOutMinsError: "Please select timeout." }) : this.setState({ timeOutMinsError: "" });
		!(durationHrs) ? this.setState({ durationHrsError: "Please select duration." }) : this.setState({ durationHrsError: "" });
		!(durationMins) ? this.setState({ durationMinsError: "Please select duration." }) : this.setState({ durationMinsError: "" });
		!(recordingPreference) ? this.setState({ recordingPreferenceError: "Please select recording preference." }) : this.setState({ recordingPreferenceError: "" });
		!(maxNoOfParticipants) ? this.setState({ maxNoOfParticipantsError: "Please select max limit of participants." }) : this.setState({ maxNoOfParticipantsError: "" });

		// maxNoOfParticipants && !isNaN(maxNoOfParticipants) && Number(maxNoOfParticipants) > 0 ? this.setState({ maxNoOfParticipantsError: "" }) : this.setState({ maxNoOfParticipantsError: "Enter valide number of paticipants" });

		if (conferenceTitle && maxNoOfParticipants && !conferenceTitleError && !maxNoOfParticipantsError) {
			let participants = [];
			participants.push(userId);
			contactList.map(contact => {
				if (contact.isAdded) {
					participants.push(contact.contactUserId);
				}
			})

			let duration = {
				hours: !isNaN(Number(durationHrs)) ? Number(durationHrs) : 1,
				minutes: !isNaN(Number(durationMins)) ? Number(durationMins) : 0,
				seconds: 0
			};

			let timeOut = {
				hours: !isNaN(Number(timeOutHrs)) ? Number(timeOutHrs) : 1,
				minutes: !isNaN(Number(timeOutMins)) ? Number(timeOutMins) : 0,
				seconds: 0
			};

			let dataToSend = {
				"conferenceTitle": conferenceTitle,
				"callInitiateBy": callInitiator,
				"hostUserId": userId,
				"broadcastId": '',
				"timeOut": timeOut,
				"duration": duration,
				"callType": conferenceMode,
				"recordingPreference": recordingPreference,
				"fileShare": fileShare,
				"acceptInboundCall": inboundCall,
				"allowParticipantToShareScreen": screenWithoutPermission,
				"maxNoOfParticipants": !isNaN(Number(maxNoOfParticipants)) ? Number(maxNoOfParticipants) : 1,
				"participants": participants
			};

			console.log('dataToSend === ', dataToSend);

			axios
				.post(path + "conference/create-conference", dataToSend, {
					headers: { "Content-Type": "application/json" }
				})
				.then(serverResponse => {
					console.log("Response from here : ", serverResponse.data);
					const res = serverResponse.data;
					if (!res.isError) {
						console.log(res.details.results);
						// Fatch all conference to refresh the list.
						that.getConferences();
						that.hideCreateConferenceModal();
					} else {
						// TODO: Handle create conference error
						that.hideCreateConferenceModal();
					}
				}).catch(error => {
					that.hideCreateConferenceModal();
					// TODO: Handle create conference error
				});

		}

	}


	openCreateConferenceModal() {
		this.setState({
			showCreateConference: true,
			conferenceTitleError: '',
			maxNoOfParticipantsError: '',
			conferenceTitle: '',
		})
	}


	hideCreateConferenceModal() {
		this.setState({
			showCreateConference: false,
			// conferenceTitleError: '',
			// maxNoOfParticipantsError: '',
			conferenceTitle: '',
			maxNoOfParticipants: '',
			callInitiator: '',
			conferenceMode: '',
			timeOutHrs: '',
			timeOutMins: '',
			durationHrs: '',
			durationMins: '',
			recordingPreference: ''

		});
	}

	/**
	 * select aparticular conference from list.
	 */
	selectConference(conferenceId) {
		return new Promise((resolve, reject) => {
			if (!this.state.selecting) {
				let that = this;
				that.setState({ selecting: true });
				// for mobile
				that.subMenuMobToggle();
				// for mobile --end
				that.switchConference();

				let { conferenceList, userId } = that.state;
				let selectedConference = _.filter(conferenceList, { '_id': conferenceId })[0];
				// let userName = _.filter(selectedConference.participants, { 'userId': userId })[0].name;

				let userName = '';
				that.setState({ selectedConference: selectedConference }, () => {
					console.log("SELECTED CONFERENCE---", this.state.selectedConference);
					console.log(this.state.selectedConference.hostUserId);
					console.log(this.state.userId);
					userName = this._getUserNameById(userId);
				});

				that.setState({ selectedConferenceId: conferenceId });
				console.log('selectedConference-> participants ============== ', selectedConference.participants);

				that.setState({ conferenceMember: selectedConference.participants });

				// get sessionId, token, invitationLink from server by conferenceId.
				let sessionId = "";
				let token = "";
				let invitationLink = "";

				sessionId = selectedConference.sessionId
				let dataToSend = {
					"conferenceId": selectedConference._id,
					"userId": userId,
					"sessionId": sessionId,
				};
				axios
					.post(path + "conference/join-conference", dataToSend, {
						headers: { "Content-Type": "application/json" }
					})
					.then(serverResponse => {
						console.log('here -----------3');

						const res = serverResponse.data;
						if (!res.isError) {
							token = res.details.token;

							let credentials = {
								"apiKey": cred.OPENTOK.apiKey,
								"sessionId": sessionId,
								"token": token
							}
							otCoreOptions.credentials = credentials;
							otCoreOptions.textChat.name = `${userName}`;
							otCore = new AccCore(otCoreOptions);
							// console.log('here -----------4');

							otCore.connect().then((response) => {
								this.setState({ connected: true, selecting: false });
								console.log('here -----------5', response);

								let session = otCore.getSession();

								// let myConnection = session.connection;
								// let allConnections = session.connections;
								let streams = session.streams;
								console.log("here -----------5/streams", streams);
								streams.map(stream => {
									console.log("here -----------5/stream", stream);
								})

								const events = [
									'subscribeToCamera',
									'unsubscribeFromCamera',
									'subscribeToScreen',
									'unsubscribeFromScreen',
									'startScreenShare',
									'endScreenShare',
									'streamPropertyChanged',
								];

								events.forEach(event => otCore.on(event, ({ publishers, subscribers, meta }) => {
									if (publishers && subscribers && meta) {
										this.setState({ publishers, subscribers, meta });
									} else {
										let otCoreState = otCore.state();
										this.setState({ publishers: otCoreState.publishers, subscribers: otCoreState.subscribers, meta: otCoreState.meta });
									}
								}));

								// otCore.on('streamPropertyChanged', (event) => {
								//   var subscribers = otCore.getSubscribersForStream(event.stream);
								//   console.log('event ======= ', subscribers.length, event);

								//   // Getting userId changing their states (i.e: hasAudio, hasVideo etc)
								//   let changedUserId = event.stream.connection.data.slice(7);
								//   let action = event.changedProperty;
								//   let actionValue = event.newValue;
								//   let { actionObserver } = this.state;
								//   let thisUser = actionObserver[changedUserId];

								//   if (!thisUser) {
								//     thisUser = { 'hasAudio': false, 'hasVideo': false };
								//   }
								//   if (action === 'hasAudio') {
								//     thisUser['hasAudio'] = actionValue;
								//   }
								//   else if (action === 'hasVideo') {
								//     thisUser['hasVideo'] = actionValue;
								//   }
								//   actionObserver[changedUserId] = thisUser;
								//   this.setState({
								//     actionObserver
								//   }, () => {
								//     console.log(this.state.actionObserver);
								//   });


								//   for (var i = 0; i < subscribers.length; i++) {
								//     console.log('================', subscribers[i]);
								//   }

								// });

								resolve();
							});

						} else {
							// TODO: Handel connect to a conference error
							that.setState({ selecting: false });
							reject();
						}
					}).catch(error => {
						// TODO: Handel connect to a conference error
						that.setState({ selecting: false });
						reject();
					});
			} else {
				reject();
			}
		});
	}


	switchConference = () => {
		// this.endCall();
		if (otCore) {
			otCore.disconnect();
			this.state.active && this.endCall();

			let element = document.getElementById('startScreenSharing');
			element && element.parentNode.removeChild(element);

			let element2 = document.getElementById('chat');
			element2 && element2.childNodes[0] && element2.removeChild(element2.childNodes[0]);

			let element3 = document.getElementById('dialog-form-chrome');
			element3 && element3.remove();

			let element4 = document.getElementById('dialog-form-ff');
			element4 && element4.remove();
		}

		this.setState({
			selectedConference: '',
			selectedConferenceId: '',
			conferenceMember: '',
			connected: false,
			active: false,
			speakers: [],
			isPresenter: false,
			currentScreen: {},
			currentStream: []
		}, () => { this.getConferences(); });

	}


	joinConferenceByCode(e) {
		e.preventDefault();
		let that = this;
		let { invitationCode, userId, userType } = this.state;

		let dataToSend = {
			"userType": userType,
			"userId": userId,
			"invitationCode": invitationCode
		};

		axios
			.post(path + "conference/join-conference-with-code", dataToSend, {
				headers: { "Content-Type": "application/json" }
			})
			.then(serverResponse => {
				console.log("Response from here : ", serverResponse.data);
				const res = serverResponse.data;
				if (!res.isError) {
					// get user id / participants id
					console.log(res);
					if (res.statuscode == 204) {
						alert('Sorry! Invitation code is wrong...');
						that.setState({ invitationCode: '' });
						that.getConferences(); // for updating active and inactive conference's lists // may not be use here.
					} else {
						that.getConferences().then(() => {
							let conference = res.details;
							that.selectConference(conference[0]._id).then(data => {
								console.log('in selectConference..............');
								// that.startCall();
								this.startCallModified();
							});
						});
						that.setState({ invitationCode: '' });

					}

				} else {
					alert('Sorry! Invitation code is wrong...');
					that.setState({ invitationCode: '' });
				}

			}).catch(error => {
				console.log(error);
			});

	}

	///// Address book --START /////
	fetchAddresses() {
		console.log('fetchAddresses============');

		let that = this;
		let { userId } = this.state;
		let dataToSend = {
			"userId": userId,
		};
		axios
			.post(path + "user/get_contact_list", dataToSend, {
				headers: { "Content-Type": "application/json" }
			})
			.then(serverResponse => {
				console.log("Response from here : ", serverResponse.data);
				const res = serverResponse.data;
				if (!res.isError) {
					let contactList = res.details.results;
					console.log(res.details.results);
					that.setState({ contactList });
				} else {

				}
			}).catch(error => {
				console.log(error);
			});
	}


	openAddressBookModal() {
		this.setState({ showAddressBook: true })
	}

	
	hideAddressBookModal() {
		this.setState({ showAddressBook: false })
	}

	// For addding members from address book -- 
	addOrRemoveToConference(id) {
		let { contactList } = this.state;
		contactList.map(contact => {
			if (contact._id === id) {
				contact['isAdded'] = !contact.isAdded;
			}
			return false;
		});
		// console.log(contactList);
	}

	// In group SIP call hide and show checkBox as per call status -- 
	sipCallInGroup(id) {

		let { conferenceMember } = this.state;
		conferenceMember.map(contact => {
			if (contact._id === id) {
				contact['isCalled'] = !contact.isCalled;
			}
			return false;

		});
		// console.log(conferenceMember);
	}

	// For SIP call in group from address book -- 
	groupSipCallFromAddressBook() {

		let group = [];
		let dataToSend = {};

		this.state.conferenceMember.map(member => {
			if (member.isCalled) {
				group.push(
					{
						"countryCode": `${member.countryCode}`,
						"toMobile": `${member.contactNumber}`
					}
				)
				// member['isInCall'] = true;

			}
		})
		console.log(this.state.conferenceMember);

		dataToSend = {
			"sessionId": this.state.selectedConference.sessionId,
			"fromMobile": "9874433192",
			"mioUsers": group
		};
		console.log(dataToSend);
		this.setState({ showMember: false });

		// API -- 
		axios
			.post(path + "sip-dial/sip-dial-out-multiple", dataToSend)
			.then(serverResponse => {

				// hiding modal --
				// this.setState({ showMember: false });
				this.hideSIPCallModal();

				let res = serverResponse.data.details;

				console.log("Multiple SIP call response----", res.length);

				var groupStringNotCall = '';
				var groupStringCalling = '';

				res.map((data, index) => {

					console.log(data.message);
					if (!data.isError) {
						if (data.statuscode != 200) {

							console.log(`${data.toUser}` + "  Can't be reached.");
							// resetting selected members --
							this.state.conferenceMember.map(member => {
								if (('' + member.countryCode + member.contactNumber) == data.toUser) {
									member.isCalled = !member.isCalled;
									member.isInCall = false;

								}
							})

							groupStringNotCall += data.toUser + ', ';

						} else if (data.statuscode == 200) {

							console.log(`${data.toUser}` + "  is connecting...");
							// resetting selected members --
							this.state.conferenceMember.map(member => {
								if (('' + member.countryCode + member.contactNumber) == data.toUser) {
									member.isCalled = !member.isCalled;
									member.isInCall = true;
								}
							})

							groupStringCalling += data.toUser + ', ';

						}

					}

				})

				let message = '';
				if (groupStringNotCall != '' && groupStringCalling != '') {
					message = "* " + groupStringNotCall.slice(0, -1) + " Can't be reached." + " and " + "* " + groupStringCalling.slice(0, -1) + " Joining soon.";
				} else if (groupStringNotCall != '' && groupStringCalling == '') {
					message = "* " + groupStringNotCall.slice(0, -1) + " Can't be reached.";
				} else if (groupStringNotCall == '' && groupStringCalling != '') {
					message = "* " + groupStringCalling.slice(0, -1) + " Joining soon.";
				}

				this.displayToast("Dial Out in Group", message);

			})
			.catch(error => {
				console.log(error);

			})

	}


	// For inbound call start functionality --
	openInboundCall(e) {
		let that = this;
		this.setState({ inboundRadio: !this.state.inboundRadio },
			() => { console.log(this.state.inboundRadio); });

		// For starting Nexmo Call// API -- 
		let dataToSend = {
			sessionId: that.state.selectedConference.sessionId
		};
		// API -- 
		axios
			.post(path + "conference/dialout-for-dialin", dataToSend)
			.then(serverResponse => {

				let resNexmo = serverResponse.data.details;
				console.log(resNexmo);

				that.setState({
					inboundConnectionId: resNexmo.connectionId
				});

				// console.log("Nexmo call starting // response-----",resNexmo);
			})
			.catch(error => {
				console.log(error);
			})
		//For starting NEXMO call // End -- 

	}


	// For inbound call end functionality --
	closeInboundCall(e) {
		let that = this;
		this.setState({ inboundRadio: !this.state.inboundRadio },
			() => { console.log(this.state.inboundRadio); });

		// For ending Nexmo Call// API -- 
		if (that.state.userId == that.state.selectedConference.hostUserId) {
			// only host user can stop the nexmo call while ending the call;

			// console.log("Nexmo connectionId // call end time----", that.state.inboundConnectionId);
			let dataToSendNexmo = {
				sessionId: that.state.selectedConference.sessionId,
				connectionId: that.state.inboundConnectionId
			};

			axios
				.post(path + "conference/dialout-hang-up", dataToSendNexmo)
				.then(serverResponse => {

					let resNexmo = serverResponse.data;
					console.log(resNexmo);

					that.setState({
						inboundConnectionId: '' // resetting the nexmo connection id in state
					});
					// console.log("Nexmo response // end call --- ",resNexmo);

				})
				.catch(error => {
					console.log(error);
				})

		}
		// For Nexmo call end // end -- 

	}


	startCall() {
		if (!this.state.active) {
			let that = this;
			// API for start call --
			this.setState({ starting: true });
			let dataToSend = {
				userId: this.state.userId,
				conferenceId: this.state.selectedConferenceId
			};
			// console.log('startCall ==================== ', dataToSend);

			axios
				.post(path + "conference/start-conference", dataToSend)
				.then(serverResponse => {
					console.log(serverResponse.data);
					let res = serverResponse.data;
					let status = serverResponse.data.details;

					// Checking whether host user has initiated the call or not --
					if (res.statuscode == 200) {
						console.log("Start conference call /  response ---- ", res);
						if (status.isStarted) {
							// Starting call -- 
							otCore.startCall()
								.then(({ publishers, subscribers, meta }) => {
									// console.log("subscribers....11......",publishers, subscribers,meta);
									this.setState({ publishers, subscribers, meta, starting: false, active: true });
									// this.getPublisherForStream();

									that.getSessionDetails(); // For getting session data
									that.registerEventListener(); // Extra added
									that.getConferences();
									// Calling function to get SIP status if any in the call -- 
									that.getSIPCallStatus(this.state.selectedConferenceId);

								})
								.catch(error => {
									console.log(error);
									if (error.code == 1500) {
										// Showing the reason of not startig call -- // error message
										alert(error.message.split('.')[0]);
									} else if (error.code == 1010) {
										alert(error.message);
									}
								});

						}
						else {
							that.setState({ starting: false });
							alert("Sorry! You can't join untill host starts the call...");
						}

					} else {
						that.setState({ starting: false });
						alert("Sorry! You can't join untill host starts the call...");
					}

				})
				.catch(error => {
					that.setState({ starting: false });
					alert('Some thing goes wrong please try again!');
					console.log(error);

				})
		}
	}


	startCallModified = () => {
		let that = this;
		if (!that.state.active) {

			// API for start call --
			that.setState({ starting: true });

			otCore.startCall()
				.then(({ publishers, subscribers, meta }) => {
					// console.log("subscribers....11......",publishers, subscribers,meta);
					this.setState({ publishers, subscribers, meta, starting: false, active: true });
					// this.getPublisherForStream();

					this.getSessionDetails().then(() => {

						let dataToSend = {
							userId: this.state.userId,
							conferenceId: this.state.selectedConferenceId,
							connections: this.state.myConnectionDetails
						};

						axios
							.post(path + "conference/start-conference", dataToSend)
							.then(serverResponse => {
								console.log(serverResponse.data);
								let res = serverResponse.data;
								let status = serverResponse.data.details;

								// Checking whether host user has initiated the call or not --
								if (res.statuscode == 200) {
									console.log("Start conference call /  response ---- ", res);
									if (status.isStarted) {

										// that.getSessionDetails(); // For getting session data
										that.registerEventListener(); // Extra added
										that.getConferences();
										// Calling function to get SIP status if any in the call -- 
										that.getSIPCallStatus(that.state.selectedConferenceId);

									}
									else {
										that.setState({ starting: false });
										that.endCall();
										alert("Sorry! You can't join untill host starts the call...");
									}

								} else {
									that.setState({ starting: false });
									that.endCall();
									alert("Sorry! You can't join untill host starts the call...");
								}

							})
							.catch(error => {
								that.setState({ starting: false });
								alert('Some thing goes wrong please try again!');
								console.log(error);

							})

					})


				})
				.catch(error => {
					console.log(error);
					if (error.code == 1500) {
						// Showing the reason of not startig call -- // error message
						alert(error.message.split('.')[0]);
					} else if (error.code == 1010) {
						alert(error.message);
					}

				});

		}

	}

	// For getting session data --
	getSessionDetails = () => {
		return new Promise((resolve, reject) => {
			console.log('Inside getSessionDetails function ------------ ');

			let session = otCore.getSession();

			let myConnection = session.connection;
			let allConnections = session.connections;
			let streams = session.streams;

			console.log("subscribers..........", session.toString());
			console.log("my connection..........", myConnection);

			allConnections.map(thisconnection => {
				console.log("all connection..........", thisconnection);
			});

			// Setting toast notifications start-
			console.log("all confefrence members of selected one----", this.state.conferenceMember);
			let newMemberId = myConnection.data.split(':')[1];

			let newMemberNameData = _.filter(this.state.conferenceMember, { 'userId': newMemberId })[0];
			let newMemberName = newMemberNameData && newMemberNameData.name;
			console.log("New member is joining having name of ----- ", newMemberName);

			let message = "";
			if (newMemberId == this.state.selectedConference.hostUserId) {
				message = "Host " + newMemberName + " of this conference is joining.";
			} else {
				message = newMemberName + " is joining.";
			}

			let activeArray = _.filter(this.state.currentStream, { 'userId': newMemberId });
			let isActive = activeArray && activeArray.length > 0;
			// checking .. whather the member is already active or not ..  
			if (newMemberName && !isActive) {
				this.displayToast(this.state.selectedConference.conferenceTitle, message);
			}
			// Setting toast notifications end--


			let streamArray = [];
			let myConnectionDetails = {};
			// console.log(streamArray);
			streams.map(stream => {
				console.log("all streams======", stream);

				let streamData = stream.connection.data.split(':');
				let streamElement = {};

				if (streamData && streamData[0] == 'userId') {
					streamElement = {
						streamId: stream.streamId,
						userId: streamData[1],
						phnNo: '',
						hasAudio: stream.hasAudio,
						hasVideo: stream.hasVideo
					};
					if (streamData[1] == this.state.userId) {
						myConnectionDetails = {
							streamId: stream.streamId,
							connectionId: stream.connection.connectionId
						}
					}

				} else {
					streamElement = {
						streamId: stream.streamId,
						userId: '',
						phnNo: streamData[1],
						hasAudio: stream.hasAudio,
						hasVideo: stream.hasVideo
					};
				}


				streamArray = streamArray.concat(streamElement);
				// console.log("streamArray-----", streamArray);

				if (stream.hasAudio) {
					this.findSpeakers(stream);
				}

				// For screen sharing -- 
				if (stream.hasVideo && stream.videoType === "screen") {
					if (Object.keys(this.state.currentScreen).length === 0) { // if screen sharing is going on other can't share their one
						// showing tost notification for screen sharing and control -- 
						this.screenSharingStarting(stream);
					}

				}

			})

			this.setState({
				currentStream: streamArray,
				myConnectionDetails: myConnectionDetails
			}, () => {
				console.log("Current stream array----", this.state.currentStream);
				console.log("Current connection details Obj----", this.state.myConnectionDetails);
				resolve();
			});

		});

	}


	registerEventListener = () => {
		let that = this;
		let session = otCore.getSession();

		// 1.>> "streamCreated" / Start --->
		session.on("streamCreated", function (event) {
			console.log('event/streamCreated/stream =======', event.stream);
			console.log('getSubscribersForStream1', otCore.getSubscribersForStream(event.stream));
			console.log('getPublisherForStream', otCore.getPublisherForStream(event.stream));

			// calling for getting sip call status start --- 

			let creatingMember = event.stream.connection.data.split(':');
			console.log("registerEventListener / new stream member --------", creatingMember[1]);

			that.getConferences().then(() => {

				// that.getSessionDetails().then(() => {
				if (creatingMember[0] == "phoneNumber") {
					// To DO
					console.log(creatingMember[1]);
				} else {
					// TODO:
					let { currentStream } = that.state;
					let activeArray = _.filter(currentStream, { 'userId': creatingMember[1] });
					let isActive = activeArray && activeArray.length > 0;
					let userName = that._getUserNameById(creatingMember[1]);
					let message = userName + ' Is joining.';
					// !isActive && that.displayToast(creatingMember[1], "Join")
					if (!isActive) {
						that.displayToast(that.state.selectedConference.conferenceTitle, message);
					}

				}

				// });
			})

			that.getSessionDetails();
			that.findSpeakers(event.stream);
		})
		// "streamCreated" / End --> 

		// 2.>> "streamDestroyed" / Start --->
		session.on("streamDestroyed", function (event) {

			console.log('event/streamDestroyed/stream =======', event.stream);
			console.log("Stream " + event.stream.name + " ended. " + event.reason);

			// resetting "speaking" in status of the member getting disconnected from call
			let destroyingMember = event.stream.connection.data.split(':')[1];
			// console.log(destroyingMember);

			let speaker = [];
			speaker[destroyingMember] = false;
			let newSpeaker = [];
			newSpeaker = that.state.speakers.concat(speaker);

			that.setState({
				speakers: newSpeaker
			}, () => { that.getSessionDetails(); })


			if (event.stream.hasVideo && event.stream.videoType === "screen") {
				if (Object.keys(that.state.currentScreen).length != 0) { // if screen sharing is going on other can't share their one
					// showing tost notification for screen sharing and control -- 

					that.screenSharingEnding();

				}
			}

		});
		// "streamDestroyed" / End ---> 

		// 3.>> "signal:mute" / Start --->
		session.on("signal:mute", function (event) {
			if (event.from.connectionId !== session.connection.id) {
				// let data = event.data;
				that.toggleLocalAudio();

			}
		});
		// "signal:mute" / End --->

		//4.>> ScreenSharing events/START --
		otCore.on('startScreenSharing', (publisher) => {

			//that.getSessionDetails();
			// For screen sharing start  // for mirror efffect-- 
			let connectionData = publisher.stream.connection.data;
			let sharedUser = connectionData.split(':')[1];
			if (sharedUser === that.state.userId) {
				that.setState({ isPresenter: true });
			} else {
				that.setState({ isPresenter: false });
			}

			console.log('startScreenSharing', publisher);
			let element = document.getElementById('startScreenSharing');
			element && element.classList.add("active");
			// document.body.classList.remove("sign-bg");

		});

		// For screen sharing END -- 
		otCore.on('endScreenSharing', (publisher) => {
			that.setState({ isPresenter: false });
			console.log('endScreenSharing', publisher);

			// Showing toast notification for end screen share and control -- 
			that.screenSharingEnding();

			let element = document.getElementById('startScreenSharing');
			element && element.classList.remove("active");

		});

		// For manual chat 
		// session.on("signal", function(event) {
		//   console.log("Signal sent from connection--- " , event);
		//   // Process the event.data property, if there is any data.
		// });

		// For ending call while host is leaving conference --
		session.on("connectionDestroyed", function (event) {
			// connectionCount--;
			// displayConnectionCount();

			console.log('Is host leaving the conference? -- ', event);
			let leavingUserId = event.connection && event.connection.data && event.connection.data.split(':')[1];
			// alert(leavingUserId+' - is leaving the room');

			if (that.state.selectedConference.hostUserId == leavingUserId) {

				let userName = that._getUserNameById(leavingUserId);
				let message = 'Host - ' + userName + ' Is Leaving...';

				that.displayToast(that.state.selectedConference.conferenceTitle, message);

				that.endCall();

			}


		});


	}


	unregisterEventListener = () => {
		console.log('unregisterEventListener------');
		let session = otCore ? otCore.getSession() : null;
		session && session.off("streamCreated", () => {
			console.log('unregister streamCreated.');
		});
		session && session.off("streamDestroyed", () => {
			console.log('unregister streamDestroyed.');
		});
		otCore && otCore.off("startScreenSharing", () => {
			console.log('unregister startScreenSharing.');
		});
		otCore && otCore.off("endScreenSharing", () => {
			console.log('unregister endScreenSharing.');
		});
	}


	screenSharingStarting = (stream) => {

		console.log('Inside screenSharing Start ============ ');
		let screenUserId = stream.connection.data.split(':')[1];
		let screenUserData = _.filter(this.state.conferenceMember, { 'userId': screenUserId })[0];
		let screenUser = screenUserData && screenUserData.name;

		let screenSharingUsers = {
			screenUserId: screenUserId
		};

		this.setState({ currentScreen: screenSharingUsers });

		if (this.state.userId != screenSharingUsers.screenUserId) {

			let element = document.getElementById('startScreenSharing');
			element && element.classList.add("hidden");

		}

		let message = screenUser + ' is sharing screen';

		// Setting notification -- 
		this.displayToast(this.state.selectedConference.conferenceTitle, message);
		// this.setState({
		//   showToast: true,
		//   toastTitle: this.state.selectedConference.conferenceTitle,
		//   toastMessage: message
		// });

	}


	screenSharingEnding = () => {

		console.log('Inside screenSharing End ============ ');
		this.setState({ currentScreen: {} });
		let message = 'Screen Sharing is being stopped.'
		// Setting notification --
		this.setState({
			showToast: true,
			toastTitle: this.state.selectedConference.conferenceTitle,
			toastMessage: message
		});

		if (this.state.userId != this.state.currentScreen.screenUserId) {
			let element = document.getElementById('startScreenSharing');
			element && element.classList.remove("hidden");
		}

	}


	speakerDetection = function (subscriber, userData, startTalking, stopTalking) {
		let that = this;
		var activity = null;
		subscriber.on('audioLevelUpdated', function (event) {
			var now = Date.now();
			if (event.audioLevel > 0.05) {
				if (!activity) {
					activity = { timestamp: now, talking: false };
				} else if (activity.talking) {
					activity.timestamp = now;
				} else if (now - activity.timestamp > 1000) {
					// detected audio activity for more than 1s
					// for the first time.
					activity.talking = true;
					if (typeof (startTalking) === 'function') {
						if (userData && userData.split(':').length === 2) {
							let speakers = that.state.speakers ? that.state.speakers : {};
							// let newSpeakers= [];
							speakers[userData.split(':')[1]] = true;

							that.setState({ speakers }, () => { console.log("Speaking...", that.state.speakers); });
						}
						startTalking();
					}
				}
			} else if (activity && now - activity.timestamp > 3000) {
				// detected low audio activity for more than 3s
				if (activity.talking) {
					if (typeof (stopTalking) === 'function') {
						if (userData && userData.split(':').length === 2) {
							let speakers = that.state.speakers ? that.state.speakers : {};
							// let newSpeakers= [];
							speakers[userData.split(':')[1]] = false;

							that.setState({ speakers }, () => { console.log("Speaking...", that.state.speakers); });
						}
						stopTalking();
					}
				}
				activity = null;
			}
		});

	};


	findSpeakers = (stream) => {

		let allSubscribers = otCore.getSubscribersForStream(stream);
		if (allSubscribers && allSubscribers.length > 0) {
			allSubscribers.map(subscriber => {
				this.speakerDetection(subscriber, stream.connection.data, function () {
					console.log('started talking');
				}, function () {
					console.log('stopped talking');
				});

				console.log("getting Subscribers-------", subscriber);

			});

		}
	}


	endCall() {
		let that = this;

		// that.getConnectionIdFromSession().then(() => {

		that.videoFullScreenModeOff(); // on call end full screen will compress
		that.unregisterEventListener(); // Extra added
		otCore.disconnect();
		otCore.endCall();

		that.setState({
			active: false,
			connected: false
		}, () => { });

		let webUserConnectionIds = that.getConnectionIdFromSession();
		// console.log("web user's connection Ids -- ",webUserConnectionIds);
		// API for end call --
		let dataToSend = {
			userId: this.state.userId,
			conferenceId: this.state.selectedConferenceId,
			connectionId: webUserConnectionIds,
			sessionId: this.state.selectedConference.sessionId
		}

		console.log("DataToSend for end-conference -- ", dataToSend);

		axios
			.post(path + "conference/end-conference", dataToSend)
			.then(serverResponse => {
				that.setState({
					selectedConference: '',
					selectedConferenceId: '',
					conferenceMember: '',
					connected: false,
					active: false,
					speakers: [],
					isPresenter: false,
					currentScreen: {},
					currentStream: []
				});
				// that.getConferences();
				clearInterval(that.getSIPStatus); // stopping setInterval 
				console.log("after ending conference call ---> ", serverResponse.data);
				window.location.reload();
			})
			.catch(error => {
				console.log(error);
			})

		// })

	}


	getConnectionIdFromSession = () => {
		let session = otCore.getSession();
		// let myConnection = session.connection;
		let allConnections = session.connections;

		console.log("all connection..........", allConnections);

		let connectionArray = [];

		allConnections.map(thisconnection => {

			console.log("all connection..........", thisconnection);
			let hasData = thisconnection.data && thisconnection.data.split(':')[0];
			if (hasData == 'userId' && thisconnection.connectionId) {
				console.log("thisconnection.connectionId----", thisconnection.connectionId);
				let connectionId = thisconnection.connectionId;
				connectionArray = [...connectionArray, connectionId];

			}

		})

		return connectionArray;

	}


	getPublisherForStream() {
		console.log("In getPublisherForStream..............");

	}


	toggleLocalAudio() {
		otCore.toggleLocalAudio(!this.state.localAudioEnabled);
		this.setState({ localAudioEnabled: !this.state.localAudioEnabled });
	}


	toggleLocalVideo() {
		otCore.toggleLocalVideo(!this.state.localVideoEnabled);
		this.setState({ localVideoEnabled: !this.state.localVideoEnabled });
	}

	// For adding member inside a conference room / start -->
	openAddMemberModal(conferenceId) {
		console.log("add member in conference room----", conferenceId);
		// this.setState({selectedConferenceId: conferenceId, showMember: true});
		this.setState({ showMember: true });
	}


	hideAddMemberModal() {
		this.setState({ showMember: false });
	}


	// For group SIP call modal control --
	openSIPCallModal(conferenceId) {
		this.setState({ sipCallModal: true });
	}


	hideSIPCallModal() {
		this.setState({ sipCallModal: false });
	}


	// For adding member inside a conference room -->
	addMemberFromAddressBook() {

		let members = [];
		this.state.contactList.map(contact => {
			console.log("contact is added----", contact);
			if (contact.isAdded) {
				members.push({
					"userId": contact.contactUserId,
					"userType": contact.userType
				});
				// members["userType"].push(contact.userType);
			}
		})

		let dataToSend = {
			"conferenceId": this.state.selectedConferenceId,
			"participants": members
		};

		let that = this;
		// API
		axios
			.post(path + "conference/add-participant", dataToSend,
				{ headers: { "Content-Type": "application/json" } })
			.then(serverResponse => {
				// console.log("Response of Add member----", serverResponse);
				const res = serverResponse.data;
				if (!res.isError) {
					// console.log(res);
					that.getConferences().then(() => {
						that.selectConference(this.state.selectedConferenceId);
					});
				} else {

				}

			})
			.catch(error => {
				console.log(error);
			})

		this.setState({ showMember: false });

	}

	// Removing member from conference room // start-->
	endSIPCall(countryCode, contactNumber) {
		let that = this;
		// To end the SIP Call of this contactNumber
		console.log("SIP");
		let dataToSend = {
			conferenceId: this.state.selectedConferenceId,
			countryCode: `${countryCode}`,
			toMobile: `${contactNumber}`
		}

		// API 
		axios
			.post(path + "conference/sip-dial-end", dataToSend,
				{ header: { "Content-Type": "application/json" } })
			.then(serverResponse => {
				const res = serverResponse.data;
				if (!res.isError) {
					// For notification -- 
					this.displayToast("Dial Out", `${countryCode}${contactNumber} is leaving the room.`);

					console.log("Ending SIP Call ------------------ ", res);
					// that.registerEventListener();

					// For updating conference member after ending call -
					this.getConferenceById(this.state.selectedConferenceId);

					// Hiding from group Sip Call List --
					this.state.conferenceMember.map(member => {
						if (('' + member.countryCode + member.contactNumber) == `${countryCode}${contactNumber}`) {
							// member.isCalled = !member.isCalled;
							member.isInCall = false;
						}
					})

				}
			})
			.catch(error => {
				console.log(error);
			})
	}
	// Removing member from conference room // end-->

	// For Deleting conference -- 
	openDeleteModal = (selectedConferenceId) => {
		console.log(selectedConferenceId);

		this.setState({
			showDeleteModal: true,
			conferenceToDelete: selectedConferenceId
		});
	}


	hideDeleteModal = () => {
		this.setState({ showDeleteModal: false });
	}


	deleteConference = () => {

		let dataToSend = {
			conferenceId: this.state.conferenceToDelete
		};

		// API -- 
		axios
			.post(path + "conference/delete-conference", dataToSend)
			.then(serverResponse => {

				console.log(serverResponse.data);
				let res = serverResponse.data;
				if (!res.isError) {
					this.setState({
						showDeleteModal: false,
						conferenceToDelete: ''
					});

					this.switchConference();
					this.getConferences();
				}

			})
			.catch(error => {
				console.log(error);

			})

	}


	getSIPCallStatus = (selectedConferenceId, contactNumber, countryCode) => {

		this.getSIPStatus = setInterval(() => {

			// console.log('In getSIPCallStatus----------');

			let isSIPCall = false;
			this.state.currentStream.map((value, index) => {
				if (value.phnNo != '') {
					isSIPCall = true;
				}
			})

			if (isSIPCall) {

				let dataToSend = {
					"conferenceId": selectedConferenceId
				};

				axios
					.post(path + 'conference/fetch-conference-by-id', dataToSend)
					.then(serverResponse => {
						let res = serverResponse.data;
						if (!res.isError) {
							// console.log('New API result---- get conference by id--------- ', res);
							let participants = res.details && res.details[0].participants;
							let sessionId = res.details && res.details.sessionId;
							console.log('New API result---- get conference by id--------- / participants', participants);

							if (contactNumber && countryCode) {
								let callingParticipant = contactNumber && countryCode && _.filter(participants, { 'contactNumber': contactNumber, 'countryCode': countryCode })[0];
								// console.log("SIP Call status --", callingParticipant)
								let state = callingParticipant && callingParticipant.state;
								let reason_code = callingParticipant && callingParticipant.reason_code;
								let reason_message = callingParticipant && callingParticipant.reason_message;
								let event = callingParticipant && callingParticipant.event;
								// console.log("SIP Call status -- ",reason_message, event );
							}

							this.setState({
								conferenceMember: participants
							}, () => {
								console.log('New API result---- get conference====', this.state.conferenceMember);
							})

						} else {
							console.log('New API result---- get conference by id---------// error', res);
							// reject();
						}
					})
					.catch(error => {
						console.log(error);
						// reject();
					})

			}

			//}
		}, 2000);

	}


	dialSipCall = () => {
		let that = this;
		// console.log(this.state.sipNo);
		console.log(this.state.selectedConference.sessionId);
		let phoneNo = `${this.state.sipNo}`;
		let countryCode = `${this.state.sipCountryCode}`;
		if (countryCode.slice(0, 1) == '+') {
			countryCode = countryCode.slice(1);
		}

		let dataToSend = {
			"sessionId": this.state.selectedConference.sessionId,
			// "sessionId": "2_MX40NjQyNTA3Mn5-MTU3NDc2NjQ3MTExOX53Ly9YajRiNUs4MmovcmFWRElsUmdyUDJ-fg",
			"toMobile": `${phoneNo}`,
			"countryCode": `${countryCode}`,
			"fromMobile": "9874433192"
		}
		console.log(dataToSend.toMobile);

		// APi-- 
		axios
			.post(path + "conference/sip-dial-out", dataToSend,
				{ headers: { "Content-Type": "application/json" } })

			.then(serverResponse => {
				const res = serverResponse.data;
				// console.log('res=========================res', res);

				if (!res.isError) {
					// this.getConferences();
					that.getConferences().then(() => {
						// that.getConferenceById(that.state.userId).then(()=>{
						let newParticipants = res.details[0];
						console.log("SIP Dial out response // newParticipants -- ", newParticipants);
						// Updating the conferenceMembers in state after sip call getting connected 
						that.setState({
							conferenceMember: newParticipants.participants
						});

						// For finding SIP call status ..
						// that.getSIPCallStatus(that.state.selectedConferenceId, phoneNo, countryCode);
						// For finding SIP call status ..

						// For notification --
						that.displayToast("Dial Out", `+${countryCode}${phoneNo} Is joining soon.`);
						// console.log("SIP Dial out response -- ", res);
						that.setState({ sipNo: '', sipCountryCode: '' });

					});
				}
			})
			.catch(error => {
				console.log(error);

			})


	}

	// For SIP Call from conference -- 
	sipCallFromConference(countryCode, contactNumber) {
		let that = this;
		let toMobile = `${countryCode}${contactNumber}`
		let dataToSend = {
			"sessionId": this.state.selectedConference.sessionId,
			// "sessionId": "2_MX40NjQyNTA3Mn5-MTU3NDc2NjQ3MTExOX53Ly9YajRiNUs4MmovcmFWRElsUmdyUDJ-fg",
			"toMobile": `${contactNumber}`,
			"countryCode": `${countryCode}`,
			"fromMobile": "9874433192"
		}

		// APi-- 
		axios
			.post(path + "conference/sip-dial-out", dataToSend,
				{ headers: { "Content-Type": "application/json" } })

			.then(serverResponse => {
				const res = serverResponse.data;
				if (!res.isError) {
					// for notification of joining --
					that.displayToast("Dial Out", `${toMobile} Is joining soon.`);

					console.log("SIP Dial out response -- ", res);
					let newParticipants = res.details[0];
					console.log("SIP Dial out response // newParticipants -- ", newParticipants);
					// Updating the conferenceMembers in state after sip call getting connected 
					that.setState({
						conferenceMember: newParticipants.participants
					});


					// For finding SIP call status ...
					// that.getSIPCallStatus(that.state.selectedConferenceId, contactNumber, countryCode);
					// For finding SIP call status ..

					// that.registerEventListener();

					// Hiding from group Sip Call List --
					this.state.conferenceMember.map(member => {
						if (('' + member.countryCode + member.contactNumber) == toMobile) {
							// member.isCalled = !member.isCalled;
							member.isInCall = true;
						}
					})

				}
			})
			.catch(error => {
				console.log(error);

			})

	}


	// For removing SIP user ---
	removeSIPUser = (contactNumber, _id) => {

		let dataToSend = {
			'conferenceId': this.state.selectedConference._id,
			'participantId': _id
		};

		axios
			.post(path + "conference/remove-participant", dataToSend)
			.then(serverResponse => {
				let res = serverResponse.data;
				if (!res.isError) {

					this.getConferences().then(() => {

						console.log("after delete/after updation========");

					})

				}

			})
			.catch(error => {
				console.log(error);
			})

	}

	//#region for Audio Handler
	/*******************************/
	// For mute and unmute a member --
	muteMember(name, userId) {
		console.log("Mute....");

	}


	unmuteMember(name, userId) {
		console.log("unMute....");

	}
	//#endregion

	//#region for Chat Handler
	/*********************************/
	onFileChange = (e) => {
		var file = this.refs.file.files[0];

		// For checking uploaded file extension -- 
		let blnValid = false;
		let sFileName = file.name;
		var _validFileExtensions = [".jpg", ".jpeg", ".png"];
		for (var j = 0; j < _validFileExtensions.length; j++) {
			var sCurExtension = _validFileExtensions[j];
			if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
				blnValid = true;
				break;
			}
		}
		console.log(blnValid);

		if (blnValid) { // If the extension matches will be desplayed as it is -- 

			var reader = new FileReader();
			var url = reader.readAsDataURL(file);
			reader.onloadend = function (e) {
				this.setState({
					imageURL: [reader.result]
				})
			}.bind(this);

		} else {
			// If the uploaded file is not of type mentioned above, will be displayed another -- 
			this.setState({
				imageURL: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQYQzzO-D0kZN8PYN2Bt-xUToYTxfK7wZGdSQ5s8nqRuG9xK_kO&s"
			});

		}

	}

	// For file uploading -- 
	handleUploadImage = (ev) => {
		ev.preventDefault();
		// this.setState({uploading: true});
		const photo = new FormData();
		photo.append('file', this.refs.file.files[0]);
		// photo.append('filename', this.uploadInput.files[0].name);

		axios
			.post(path + "conference/upload-participant-file",
				photo,
				{
					headers: { 'Content-Type': 'multipart/form-data' },
					onUploadProgress: (progressEvent) => {
						console.log("raw upload loader ---------", progressEvent.loaded, progressEvent.total);

						let uploadPercentage = parseInt(Math.round((progressEvent.loaded / progressEvent.total) * 100));
						this.setState({ fileUploaded: uploadPercentage }, () => { console.log("File uploaded ....................", this.state.fileUploaded); });

					}
				})
			.then((response) => {

				let res = response.data;
				if (!res.isError) {
					let imgUrl = res.detail.url;
					let inputF = document.getElementById("messageBox");
					inputF.value = imgUrl;

					this.setState({ imageURL: '' });
					console.log(imgUrl);

					console.log("File uploaded in response.....................", this.state.fileUploaded);

				}

			})
			.catch(error => {
				console.log(error);

			})

	}

	//#endregion

	//#region for mobile version: Menu handler
	/******************************************/
	sideMenuToggle() {
		let { openSideMenu } = this.state;
		openSideMenu = !openSideMenu;
		this.setState({ openSideMenu });
	}

	mobMenuClick() {
		console.log('mobMenuClick');

		this.sideMenuToggle();
		this.subMenuMobToggle();
	}

	subMenuMobToggle() {
		let { openSubMenuMob } = this.state;
		openSubMenuMob = !openSubMenuMob;
		this.setState({ openSubMenuMob });
	}
	//#endregion

	//#region for mobile version: participant list
	/***********************************************/
	participantsModalOpen() {
		this.setState({ showParticipantsModal: true });
	}

	hideParticipantsModal() {
		this.setState({ showParticipantsModal: false });
	}

	//#endregion

	//#region Toast Handler
	/************************************/
	/**
	 * @param {string} toastTitle
	 * @param {string} toastMessage
	 */
	displayToast = (toastTitle, toastMessage) => {
		this.setState({ showToast: true, toastTitle, toastMessage, });
	}


	hideToast = () => {
		this.setState({
			showToast: false,
			toastTitle: "",
			toastMessage: ""
		});
	}

	/**********************************/
	// For Util functions -- 
	_getUserNameById = (userId) => {
		let userName = '';
		if (userId) {
			if (this.state.selectedConference && this.state.selectedConference.participants) {
				try {
					let userNameData = this.state.selectedConference.participants && _.filter(this.state.selectedConference.participants, { 'userId': userId })[0];
					userName = userNameData && userNameData.name;
				} catch (err) {

				}
			}
		}

		return userName;

	}


	_getStreamUserDetails = (data) => {

		let userDetails = {
			name: '',
			value: ''
		};

		if (data && data.length > 0) {
			let dataPart = data.split(':');

			userDetails = {
				name: dataPart[0],
				value: dataPart[1]
			};
		}
		return userDetails;

	}

	/****************************/


	toggleRemortAudio = (userId) => {

		let session = otCore.getSession();
		// let myConnection = session.connection;
		let allConnections = session.connections;
		allConnections.map(thisconnection => {
			let data = this._getStreamUserDetails(thisconnection.data);
			if (data && data.value == userId) {
				// this.signalSpecificClient(thisconnection, true, 'mute').then(() => {
				//   this.getSessionDetails();

				// });

				this.getSessionDetails().then(() => {
					this.signalSpecificClient(thisconnection, true, 'mute');
				});

			}

		});

	}

	// Sending a signal to a specific client in a session

	/**
	 * connection, data, type
	 */
	signalSpecificClient = (connection, data, type = 'textMessage') => {
		return otCore.signal(type, data, connection);
	}


	chatDisplayToggle = () => {
		// let { isChatOpen, isFullScreen } = this.state;
		// this.setState({isChatOpen:!isChatOpen});

		// best-practices
		this.setState(prevState => ({ isChatOpen: !prevState.isChatOpen }))
	}


	//#region Render
	/**************************************/
	render() {
		const { connected, starting, active, contactList, conferenceList, selectedConference, conferenceMember, selectedConferenceId, isFullScreen, isChatOpen } = this.state;
		const {
			localAudioClass,
			localVideoClass,
			localCallClass,
			controlClass,
			cameraPublisherClass,
			screenPublisherClass,
			cameraSubscriberClass,
			screenSubscriberClass,
			appChatOuterWrap,
		} = containerClasses(this.state);


		return <main>

			<section className="user-mngnt-wrap">
				<div className="container-fluid">
					<div className="row">
						{!isFullScreen ? <Navbar routTo="/adminconference" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} /> : null}
						{!isFullScreen ? <div className="col-xl-2 col-lg-3 p-0 main-menu-lt-col-2">
							<div className="main-menu-lt">
								<div className="inv-code-top">
									<h5>{this.state.selectedCompanyName}</h5>
									{/* Company Name */}
								</div>
								<div className="inv-code-top">
									<div className="inv-code-form">
										{this.state.userType != 'GUEST' ?
											<button className="btn"
												// ref={ref => this.fooRef = ref}
												data-tip='Create Conference'
												onClick={() => this.openCreateConferenceModal()}
											// onFocus={() => { ReactTooltip.show(this.fooRef) }}  
											>
												<ReactTooltip
													effect="float"
													place="top"
													data-border="true"

												/>
												Create Conference
                      						</button>
											: null}

									</div>
								</div>
								<h5>Ongoing Conferences</h5>

								<div className="lt-menu">
									<ul>
										{
											conferenceList && conferenceList.length > 0 ? conferenceList.map((conference, index) => {

												return conference.isActive ?
													<li key={index} className={selectedConference && selectedConference._id === conference._id ? "current-menu" : ""} onClick={() => this.selectConference(conference._id)}>
														<a>{conference.conferenceTitle}</a>

													</li>
													: null
											})

												: null
										}
									</ul>
								</div>

								<h5>Other Conferences</h5>

								<div className="lt-menu">
									<ul>
										{
											conferenceList && conferenceList.length > 0 ? conferenceList.map((conference, index) => {
												// return !conference.isActive && conference.hostUserId == this.state.userId ?
												return !conference.isActive ?
													<li key={index} className={selectedConference && selectedConference._id === conference._id ? "current-menu" : ""} onClick={() => this.selectConference(conference._id)}>
														<a>{conference.conferenceTitle}</a>
													</li>
												: null
											})
												:
												null
										}
									</ul>
								</div>

							</div>
						</div>
							: null}


						<div id="fullconf" className={`col-lg-6 p-0 mob-full-conf ${isFullScreen && isChatOpen ? 'full-src-opn-chat col-xl-12' : isFullScreen && !isChatOpen ? 'col-xl-12' : 'col-xl-8'}`}>

							<div className="mobile-menu-header">
								{/* <a href="JavaScript:Void(0);" className="back-arw" onClick={() => console.log('<-BACK')}><i className="fas fa-arrow-left"></i></a> */}
								<a href="JavaScript:Void(0);" className="menu-tgl" onClick={() => this.sideMenuToggle()}><img src="images/menu-tgl.png" alt="" /></a>


								<div className="main-menu-lt conf-list-mobile" style={this.state.openSubMenuMob ? { display: 'block' } : { display: 'none' }}>
									<a href="JavaScript:Void(0);" className="back-arw pb-2 pl-3 d-block" onClick={() => this.subMenuMobToggle()}><i className="fas fa-arrow-left"></i></a>
									<div className="inv-code-top">
										<h5>JBG GLOBAL</h5>
										{/* Company Name */}
									</div>
									<div className="inv-code-top">
										<div className="inv-code-form">

											<button className="btn"
												// ref={ref => this.fooRef = ref}
												data-tip='Create Conference'
												onClick={() => this.openCreateConferenceModal()}
											// onFocus={() => { ReactTooltip.show(this.fooRef) }}  
											>
												<ReactTooltip
													effect="float"
													place="top"
													data-border="true"
												/>
												Create Conference
                        					</button>

										</div>
									</div>
									<h5>Ongoing Conferences</h5>

									<div className="lt-menu">
										<ul>
											{
												conferenceList && conferenceList.length > 0 ? conferenceList.map((conference, index) => {
													return conference.isActive && conference.hostUserId == this.state.userId ?
														<li key={index} className={selectedConference && selectedConference._id === conference._id ? "current-menu" : ""} onClick={() => this.selectConference(conference._id)}>
															<a>{conference.conferenceTitle}</a>
														</li>
														: null
												})
													:
													null
											}
										</ul>
									</div>

									<h5>Others Conferences</h5>

									<div className="lt-menu">
										<ul>
											{
												conferenceList && conferenceList.length > 0 ? conferenceList.map((conference, index) => {
													return !conference.isActive && conference.hostUserId == this.state.userId ?
														<li key={index} className={selectedConference && selectedConference._id === conference._id ? "current-menu" : ""} onClick={() => this.selectConference(conference._id)}>
															<a>{conference.conferenceTitle}</a>
														</li>
														: null
												})
													:
													null
											}
										</ul>
									</div>

								</div>

							</div>

							<div className="confrnc-outer">
								{selectedConferenceId && this.state.userId == this.state.selectedConference.hostUserId ? // testing whather any conference is selected or not
									<div className="row">
										<div className="col-md-12">
											<div className="inv-code">
												<span>Invitation code: </span><b>{selectedConference && selectedConference.invitationCode}</b>
											</div>
										</div>
										{/* <div className="col-md-4">
                      <div className="inv-code">
                        <span>Phone :</span> <b>+447451271121</b><span> &nbsp;&nbsp;&nbsp; OTP :</span><b>{selectedConference && selectedConference.dtmf}</b>
                      </div>
                    </div>
                    <div className="col-md-4">
                      <div className="inv-code">
                        <span>Inbound Call :</span> <span>&nbsp;&nbsp;&nbsp;</span>
                        {!this.state.inboundRadio ?
                          <Badge variant="success" style={{ cursor: "pointer" }} onClick={(e) => { this.openInboundCall(e) }}>Open</Badge>
                          : <Badge variant="danger" style={{ cursor: "pointer" }} onClick={(e) => { this.closeInboundCall(e) }}>Close</Badge>}
                      </div>
                    </div> */}
									</div>
									: null}
								<div className="row">
									<div className="col-md-4">

									</div>
								</div>
								<div className="confrnc-box">

									<div className="App-main">
										<div className="App-video-container">
											{!connected && connectingMask()}
											{connected && !active && !starting && startCallMask(this.startCallModified)}
											{starting && loadingMask()}
											<div id="cameraPublisherContainer" className={cameraPublisherClass} />
											<div id="screenPublisherContainer" className={screenPublisherClass} />
											<div id="cameraSubscriberContainer" className={cameraSubscriberClass} />
											<div id="screenSubscriberContainer" className={screenSubscriberClass} />
										</div>
										<div id="controls" className={controlClass}>
											<div className={localAudioClass} onClick={this.toggleLocalAudio} />
											<div className={localVideoClass} onClick={this.toggleLocalVideo} />
											<div className={localCallClass} onClick={this.endCall} />
											{isFullScreen ?
												(<div className="ots-video-control circle full-scr-close" onClick={() => this.videoFullScreenModeOff()}> <span><i className="fas fa-compress"></i> </span></div>)
												:
												(<div className="ots-video-control circle full-scr-open" onClick={() => this.videoFullScreenModeOn()}><span> <i className="fas fa-expand"></i></span> </div>)
											}

											{isChatOpen ?
												(<div className="ots-video-control circle chat-close" onClick={() => this.chatDisplayToggle()}> <span><i className="fas fa-comment"></i> </span></div>)
												:
												(<div className="ots-video-control circle chat-open" onClick={() => this.chatDisplayToggle()}><span> <i className="far fa-comment"></i></span> </div>)
											}

										</div>

										<div className={appChatOuterWrap}>
											<a href="JavaScript:Void(0);" className="back-arw d-lg-none" onClick={() => this.chatDisplayToggle()}><i className="fas fa-arrow-left"></i></a>
											<div id="chat" className="App-chat-container" />

											{/* File Upload start */}
											<form>
												<div className="chat-attach">
													<i className="far fa-images"></i>
													{/* <input ref={(ref) => { this.uploadInput = ref ;}} type="file" /> */}
													<input ref="file" name="file" type="file"
														onChange={this.onFileChange} multiple={true}
													// ref={ref => this.fooRef = ref}
													// data-tip='Upload File'
													// onFocus={() => { ReactTooltip.show(this.fooRef) }}
													/>
													{/* <ReactTooltip
                            effect="float"
                            place="top"
                            data-border="true"
                          /> */}
												</div>
												{this.state.imageURL ?
													<div className="chat-upload">
														<button onClick={this.handleUploadImage}><i className="fas fa-upload"></i></button>
														{/* {this.state.uploading? <i className="fa fa-spinner fa-spin spinner"></i> : <button onClick={this.handleUploadImage}><i className="fas fa-upload"></i></button>} */}
														<div className="imgbox" onClick={this.handleUploadImage}><img src={this.state.imageURL ? this.state.imageURL : "https://www.goldensherpa.com/assets/img/default-banner.jpg"} alt="img" /></div>
														<ProgressBar striped variant="success" now={this.state.fileUploaded} label={`${this.state.fileUploaded}%`} />
													</div>
													: null}
											</form>
											{/* File Upload end */}

										</div>
									</div>

									{selectedConferenceId !== '' && this.state.userType != 'GUEST' ?

										<div className="add-user-btn">
											{this.state.active && this.state.userId == this.state.selectedConference.hostUserId ?
												<a className="btn"
													// ref={ref => this.fooRef = ref}
													data-tip="Phone dial out"
													onClick={() => this.openSIPCallModal(selectedConferenceId)}
												// onFocus={() => { ReactTooltip.show(this.fooRef) }} 
												>
													<i className="fas fa-phone"></i>
													<ReactTooltip
														effect="float"
														place="top"
														data-border="true"
													/>
												</a>
												: null}
											{!this.state.active && this.state.userId == this.state.selectedConference.hostUserId ?
												<a className="btn"
													// ref={ref => this.fooRef = ref}
													data-tip="Add Member"
													onClick={() => this.openAddMemberModal(selectedConferenceId)}
												// onFocus={() => { ReactTooltip.show(this.fooRef) }}  
												>
													<i className="fas fa-user-plus"></i>
													<ReactTooltip
														effect="float"
														place="top"
														data-border="true"
													/>
												</a>
												: null}
											{!this.state.active && this.state.userId == this.state.selectedConference.hostUserId ?
												<a className="btn red"
													// ref={ref => this.fooRef = ref}
													data-tip="Delete Conference"
													// onFocus={() => { ReactTooltip.show(this.fooRef) }}
													onClick={() => this.openDeleteModal(selectedConferenceId)}
												>
													<i className="fas fa-trash-alt"></i>
													<ReactTooltip
														effect="float"
														place="top"
														data-border="true" />
												</a>
												: null}
										</div>

										: null}

									{
										active && conferenceMember.length > 0 ?

											<div className="all-users-list" >

												{conferenceMember.map((member, index) => {
													var memberId = member.userId;
													var contactNumber = '' + member.countryCode + member.contactNumber; // from db

													// whether he belong from sip, check from db.
													let isSipMember = member.userType === 'DIALEDUSER';

													// from OT active stream.
													let currentStream = this.state.currentStream;

													// whethre this SIP member is in ongoing stream.
													let sipCallArray = isSipMember ? _.filter(currentStream, { 'phnNo': contactNumber }) : [];
													let isInSipStream = sipCallArray && sipCallArray.length > 0;

													let activeArray = _.filter(currentStream, { 'userId': memberId });
													let isActive = activeArray && activeArray.length > 0;

													let muteStatus = (isActive && activeArray[0].hasAudio);

													return (memberId !== this.state.userId && this.state.userId == this.state.selectedConference.hostUserId) ?
														<ul key={index}>
															<li>

																<div className="thumb-main">
																	<div className="userimg">

																		{memberId && isActive ? <span className="active-call-cirlce"></span> : <span></span>}
																		{memberId && this.state.speakers && this.state.speakers[memberId] ? <span className="speaking"><i className="fas fa-microphone"></i></span> : <span><i></i></span>}
																		{memberId && Object.keys(this.state.currentScreen).length > 0 && memberId == this.state.currentScreen.screenUserId ?
																			<span className="ic-share"><i className="fas fa-desktop"></i></span>
																			: null}
																		{/* For SIP User */}
																		{(!memberId && isInSipStream && member.state == 'ACTIVE' && member.event == "callUpdated") ? <span className="active-call-cirlce" ></span>
																			: (!memberId && isInSipStream && member.state != 'NA' && member.state != 'ACTIVE' && member.event == "callCreated" || member.event == "callUpdated") ? <span className="active-call-cirlce2" ></span>
																				: (!memberId && !isInSipStream) ? <span></span>
																					: <span></span>
																		}

																		{(!memberId && this.state.speakers && this.state.speakers[contactNumber]) ? <span className="speaking"><i className="fas fa-microphone"></i></span> : <span><i></i></span>}
																		{/* For SIP User */}

																		{/* <i className="fas fa-microphone-slash"></i> */}

																		{member && member.name ? member.name.slice(0, 2) : <span><i className="far fa-user"></i></span>}

																		{/* {this.state.actionObserver[memberId] ?
                                      !this.state.actionObserver[memberId].hasAudio ?
                                        <span className="vol-up-hvr" ><button className="fas fa-microphone" ><i></i></button></span>
                                        : <span className="vol-off-hvr" ><button className="fas fa-microphone-slash"> <i></i></button></span>
                                      : null} */}

																	</div>

																	<div className="clients-name"><span>{member.name ? member.name : "Participant"}</span></div>

																	{this.state.userId == this.state.selectedConference.hostUserId ?
																		<div className="users-info">

																			{memberId ?
																				<ul>
																					{isActive ? <li><span style={{ color: "green" }}>Online</span></li> : null}
																					<li><span>{this.state.speakers && this.state.speakers[memberId] ? "Speaking" : null}</span></li>
																					<li><span>{member.contactNumber ? contactNumber : member.userType}</span></li>

																					{/* {isActive && muteStatus ?
                                            <li className="vol-off-hvr" onClick={() => this.toggleRemortAudio(memberId)}><button className="fas fa-microphone"><span><i></i></span></button></li>
                                            : <li className="vol-up-hvr" onClick={() => this.toggleRemortAudio(memberId)}><button className="fas fa-microphone-slash"><span><i></i></span></button></li>
                                          } */}

																					{isActive ?
																						<li>
																							<span style={{ color: "green", cursor: "pointer" }} onClick={() => { this.toggleRemortAudio(memberId) }}>{muteStatus ? "Mute" : "Unmute"}</span>
																						</li>
																						: null}

																				</ul>
																				: <ul>

																					{isInSipStream ? <li><span style={{ color: "green" }}>Online</span></li> : null}
																					<li><span>{this.state.speakers && this.state.speakers[contactNumber] ? "Speaking" : null}</span></li>
																					<li><span>{member.contactNumber ? contactNumber : member.userType}</span></li>
																					<li><span>{member.state && member.state != 'NA' && member.state != 'ACTIVE' && member.event == "callUpdated" ?
																						member.state
																						: member.state && member.state != 'NA' && member.state != 'ACTIVE' && member.event == "callCreated" ?
																							"Connecting.."
																							: member.state && member.state != 'NA' && member.state != 'ACTIVE' && member.event == "callDestroyed" ?
																								null
																								: null}
																					</span>
																					</li>

																					<li>
																						{isInSipStream ?
																							<button onClick={() => this.endSIPCall(member.countryCode, member.contactNumber)}>End Call</button>
																							:
																							<button onClick={() => this.sipCallFromConference(member.countryCode, member.contactNumber)}>Start Call</button>
																						}
																					</li>
																					<li>
																						{!isInSipStream ?
																							<button onClick={() => this.removeSIPUser(contactNumber, member._id)}>Remove</button>
																							: null}
																					</li>

																				</ul>
																			}

																		</div>
																		: null}

																</div>

															</li>

														</ul>
														// For the users are not host -- // only the online users will be visible
														: (memberId !== this.state.userId && this.state.userId != this.state.selectedConference.hostUserId && isActive || isInSipStream) ?
															// <ul> will be repleated....
															<ul key={index}>

																<li>

																	<div className="thumb-main">
																		<div className="userimg">

																			{memberId && isActive ? <span className="active-call-cirlce"></span> : <span></span>}
																			{memberId && this.state.speakers && this.state.speakers[memberId] ? <span className="speaking"><i className="fas fa-microphone"></i></span> : <span><i></i></span>}
																			{memberId && Object.keys(this.state.currentScreen).length > 0 && memberId == this.state.currentScreen.screenUserId ?
																				<span className="ic-share"><i className="fas fa-desktop"></i></span>
																				: null}
																			{/* For SIP User */}
																			{/* {!memberId && isInSipStream ? <span className="active-call-cirlce"></span> : <span></span>} */}

																			{(!memberId && isInSipStream && member.state == 'ACTIVE' && member.event == "callUpdated") ? <span className="active-call-cirlce" ></span>
																				: (!memberId && isInSipStream && member.state != 'NA' && member.state != 'ACTIVE' && member.event == "callCreated" || member.event == "callUpdated") ? <span className="active-call-cirlce2" ></span>
																					: (!memberId && !isInSipStream) ? <span></span>
																						: <span></span>
																			}
																			{!memberId && this.state.speakers && this.state.speakers[contactNumber] ? <span className="speaking"><i className="fas fa-microphone"></i></span> : <span><i></i></span>}
																			{/* For SIP User */}

																			{/* <i className="fas fa-microphone-slash"></i> */}

																			{member && member.name ?
																				member.name.slice(0, 2)
																				: <span><i className="far fa-user"></i></span>}

																			{/* {this.state.actionObserver[memberId] ?
                                        !this.state.actionObserver[memberId].hasAudio ?
                                          <span className="vol-up-hvr" ><button className="fas fa-microphone" ><i></i></button></span>
                                          : <span className="vol-off-hvr" ><button className="fas fa-microphone-slash"> <i></i></button></span>
                                        : null} */}
																		</div>

																		<div className="clients-name"><span>{member.name ? member.name : "Participant"}{/*<small>Presenter</small>*/}</span></div>

																	</div>

																</li>

															</ul>
															: null

												})
												}

											</div>
											: null
									}

									{/* hide untill confefrence start */}
									{this.state.selectedConferenceId && this.state.active ?
										<span onClick={() => this.participantsModalOpen()} className="mob-user-list-ic"><i className="fas fa-ellipsis-h"></i></span>
										: null}

								</div>

							</div>

						</div>
					</div>
				</div>

				{/* For manual chatbox by signalling */}
				{/* <div>
          <textarea name= "message" value= {this.state.chatMessage} 
            onChange= {e=> this.setState({chatMessage : e.target.value})} 
          />
          <button onClick= {e=> this.sendManualChat(e)}>
            send
          </button>
        </div> */}

			</section>

			{/* For Mobile version--  Participants list*/}
			<Modal size={"lg"} show={this.state.showParticipantsModal} onHide={() => this.hideParticipantsModal()}>
				<Modal.Header closeButton>
					<Modal.Title>All Participants</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<div className="mob-partcpt-list">
						{
							active && conferenceMember.length > 0 && <div className="all-users-list" >
								{conferenceMember.map((member, index) => {
									var memberId = member.userId;
									var contactNumber = '' + member.countryCode + member.contactNumber; // from db

									// whether he belong from sip, check from db.
									let isSipMember = member.userType === 'DIALEDUSER';

									// from OT active stream.
									let currentStream = this.state.currentStream;

									// whethre this SIP member is in ongoing stream.
									let sipCallArray = isSipMember ? _.filter(currentStream, { 'phnNo': contactNumber }) : [];
									let isInSipStream = sipCallArray && sipCallArray.length > 0;

									let activeArray = _.filter(currentStream, { 'userId': memberId });
									let isActive = activeArray && activeArray.length > 0;

									let muteStatus = (isActive && activeArray[0].hasAudio);

									// console.log("isSipMember,isInSipStream ==== ", isSipMember, isInSipStream);
									return memberId !== this.state.userId ? <ul key={index}>
										<li>

											<div className="thumb-main">
												<div className="userimg">

													{memberId && isActive ? <span className="active-call-cirlce"></span> : <span></span>}
													{memberId && this.state.speakers && this.state.speakers[memberId] ? <span className="speaking"><i className="fas fa-microphone"></i></span> : <span><i></i></span>}
													{memberId && Object.keys(this.state.currentScreen).length > 0 && memberId == this.state.currentScreen.screenUserId ?
														<span className="ic-share"><i className="fas fa-desktop"></i></span>
														: <span><i></i></span>}
													{/* For SIP User */}
													{!memberId && isInSipStream ? <span className="active-call-cirlce"></span> : <span></span>}
													{!memberId && this.state.speakers && this.state.speakers[contactNumber] ? <span className="speaking"><i className="fas fa-microphone"></i></span> : <span><i></i></span>}
													{/* For SIP User */}

													{/* <i className="fas fa-microphone-slash"></i> */}

													{member.name ?
														member.name.slice(0, 2)
														: <i className="far fa-user"></i>}

													{/* {this.state.actionObserver[memberId] ?
                            !this.state.actionObserver[memberId].hasAudio ?
                              <span className="vol-up-hvr" ><button className="fas fa-microphone" ><i></i></button></span>
                              : <span className="vol-off-hvr" ><button className="fas fa-microphone-slash"> <i></i></button></span>
                            : null} */}
												</div>

												<div className="clients-name"><span>{member.name ? member.name : "Participant"}{/*<small>Presenter</small>*/}</span></div>

												{this.state.userId == this.state.selectedConference.hostUserId ?
													<div className="users-info">

														{memberId ?
															<ul>
																{isActive ? <li><span style={{ color: "green" }}>Online</span></li> : null}
																<li><span>{this.state.speakers && this.state.speakers[memberId] ? "Speaking" : null}</span></li>
																<li><span>{member.contactNumber ? contactNumber : member.userType}</span></li>

																{/* {
                                      !(this.state.actionObserver[memberId] && this.state.actionObserver[memberId].hasAudio === true) ?
                                        <li className="vol-up-hvr" onClick={() => this.muteMember(member.name, member.userId)}><button className="fas fa-microphone-slash" ><i></i></button></li>
                                        : <li className="vol-off-hvr" onClick={() => this.unmuteMember(member.name, member.userId)}><button className="fas fa-microphone"><i></i> </button></li>
                                    } */}

																{isActive ?
																	<li>
																		<span style={{ color: "green", cursor: "pointer" }} onClick={() => { this.toggleRemortAudio(memberId) }}>{muteStatus ? "Mute" : "Unmute"}</span>
																	</li>
																	: null}

															</ul>
															: <ul>

																{isInSipStream ? <li><span style={{ color: "green" }}>Online</span></li> : null}
																<li><span>{this.state.speakers && this.state.speakers[contactNumber] ? "Speaking" : null}</span></li>
																<li><span>{member.contactNumber ? contactNumber : member.userType}</span></li>
																<li><span>{member.state && member.state != 'NA' && member.state != 'ACTIVE' && member.event == "callUpdated" ?
																	member.state
																	: member.state && member.state != 'NA' && member.state != 'ACTIVE' && member.event == "callCreated" ?
																		"Connecting.."
																		: member.state && member.state != 'NA' && member.state != 'ACTIVE' && member.event == "callDestroyed" ?
																			null
																			: null}
																</span>
																</li>
																<li>
																	{isInSipStream ?
																		<button onClick={() => this.endSIPCall(member.countryCode, member.contactNumber)}>End Call</button>
																		:
																		<button onClick={() => this.sipCallFromConference(member.countryCode, member.contactNumber)}>Start Call</button>
																	}
																</li>

															</ul>
														}

													</div>
													: null}

											</div>


										</li>
									</ul>
										: null

								})
								}

							</div>
						}
					</div>
				</Modal.Body>
				<Modal.Footer>
					<Button variant="primary" onClick={() => this.hideParticipantsModal()}>
						Close
          </Button>
				</Modal.Footer>
			</Modal>

			<Modal size="xl" show={this.state.showCreateConference} onHide={() => this.hideCreateConferenceModal()}>
				<Modal.Header closeButton>
					<Modal.Title>Create New Conference</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<div className="row">

						<div className="col-md-12">
							<div className="form-group">
								<label>Title:</label>
								<input
									type="text"
									name="usrFname"
									value={this.state.conferenceTitle}
									className="form-control"
									placeholder="Title"
									onChange={event => this.setState({ conferenceTitle: event.target.value })}
								/>
								<span style={{ color: "red" }}>{this.state.conferenceTitleError ? `* ${this.state.conferenceTitleError}` : ''}</span>
							</div>
						</div>

						<div className="col-md-6">
							<div className="form-group">
								<label>Call initiate by:</label>
								<select className="form-control" value={this.state.callInitiator} onChange={(event) => this.setState({ callInitiator: event.target.value })} >
									<option value="">Select call initiator</option>
									<option value="OPEN">Open to all</option>
									<option value="OWNER">Creator of the conference</option>
									<option value="OPERATOR">Operator</option>
								</select>
								<span style={{ color: "red" }}>{this.state.callInitiatorError ? `* ${this.state.callInitiatorError}` : ''}</span>
							</div>
						</div>

						<div className="col-md-6">
							<div className="form-group">
								<label>Mode:</label>
								<select className="form-control" value={this.state.conferenceMode} onChange={(event) => this.setState({ conferenceMode: event.target.value })} >
									<option value="">Select conference mode</option>
									<option value="CONFERENCE">Normal</option>
									<option value="LECTURE">Lecture</option>
								</select>
								<span style={{ color: "red" }}>{this.state.conferenceModeError ? `* ${this.state.conferenceModeError}` : ''}</span>
							</div>
						</div>

						<div className="col-md-6">
							<div className="form-group">
								<label>Time out(hours):</label>
								<select className="form-control" value={this.state.timeOutHrs} onChange={(event) => this.setState({ timeOutHrs: event.target.value })} >
									<option value="1">Select hours</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
									<option value="10">10</option>
									<option value="11">11</option>
									<option value="12">12</option>
									<option value="-1">No time out</option>
								</select>
								<span style={{ color: "red" }}>{this.state.timeOutHrsError ? `* ${this.state.timeOutHrsError}` : ''}</span>
							</div>
						</div>

						<div className="col-md-6">
							<div className="form-group">
								<label>Time out(Minutes):</label>
								<select className="form-control" value={this.state.timeOutMins} onChange={(event) => this.setState({ timeOutMins: event.target.value })} >
									<option value="0">Select mins</option>
									<option value="00">00</option>
									<option value="10">10</option>
									<option value="20">20</option>
									<option value="30">30</option>
									<option value="40">40</option>
									<option value="50">50</option>
									<option value="-1">No time out</option>
								</select>
								<span style={{ color: "red" }}>{this.state.timeOutMinsError ? `* ${this.state.timeOutMinsError}` : ''}</span>
							</div>
						</div>

						<div className="col-md-6">
							<div className="form-group">
								<label>Call duration(Hours):</label>
								<select className="form-control" value={this.state.durationHrs} onChange={(event) => this.setState({ durationHrs: event.target.value })} >
									<option value="1">Select hours</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
									<option value="10">10</option>
									<option value="11">11</option>
									<option value="12">12</option>
									<option value="-1">Unlimited</option>
								</select>
								<span style={{ color: "red" }}>{this.state.durationHrsError ? `* ${this.state.durationHrsError}` : ''}</span>
							</div>
						</div>

						<div className="col-md-6">
							<div className="form-group">
								<label>Call duration(Mins):</label>
								<select className="form-control" value={this.state.durationMins} onChange={(event) => this.setState({ durationMins: event.target.value })} >
									<option value="0">Select mins</option>
									<option value="00">00</option>
									<option value="10">10</option>
									<option value="20">20</option>
									<option value="30">30</option>
									<option value="40">40</option>
									<option value="50">50</option>
									<option value="-1">Unlimited</option>
								</select>
								<span style={{ color: "red" }}>{this.state.durationMinsError ? `* ${this.state.durationMinsError}` : ''}</span>
							</div>
						</div>

						<div className="col-md-6">
							<div className="form-group">
								<label>Recording preference:</label>
								<select className="form-control" value={this.state.recordingPreference} onChange={(event) => this.setState({ recordingPreference: event.target.value })} >
									<option value="">Select an option</option>
									<option value="AUTOMATICALLY_INITIATED">Automatically initiated</option>
									<option value="AVAILABLE_TO_ORGANISER">Available to organiser</option>
									<option value="NOT_ALLOWED">Not Allowed</option>
								</select>
								<span style={{ color: "red" }}>{this.state.recordingPreferenceError ? `* ${this.state.recordingPreferenceError}` : ''}</span>
							</div>
						</div>

						<div className="col-md-6">
							<div className="form-group">
								<label>No of participants:</label>
								<select className="form-control" value={this.state.maxNoOfParticipants} onChange={(event) => this.setState({ maxNoOfParticipants: event.target.value })} >
									<option value="1">Select max. limit</option>
									<option value="50">50</option>
									<option value="100">100</option>
									<option value="150">150</option>
									<option value="200">200</option>
								</select>
								<span style={{ color: "red" }}>{this.state.maxNoOfParticipantsError ? `* ${this.state.maxNoOfParticipantsError}` : ''}</span>
							</div>
						</div>

						<div className="col-md-3">
							<div className="form-group">
								<label>File Share:</label>
							</div>
						</div>
						<div className="col-md-3">
							<div className="form-group">
								<input
									type="checkbox"
									checked={this.state.fileShare}
									onChange={(e) => this.setState({ fileShare: !this.state.fileShare })}
								/>
							</div>
						</div>

						<div className="col-md-3">
							<div className="form-group">
								<label>Accept Inbound Call:</label>
							</div>
						</div>
						<div className="col-md-3">
							<div className="form-group">
								<input
									type="checkbox"
									checked={this.state.inboundCall}
									onChange={(e) => this.setState({ inboundCall: !this.state.inboundCall })}
								/>
							</div>
						</div>

						<div className="col-md-12">
							<div className="form-group">
								<label>Allow participants to share screen:</label>
							</div>
						</div>
						<div className="col-md-3">
							<div className="form-group">
								<label>Without organizer's Permission:</label>
							</div>
						</div>
						<div className="col-md-3">
							<div className="form-group">
								<input
									type="checkbox"
									checked={this.state.screenWithoutPermission}
									onChange={(e) => this.setState({ screenWithoutPermission: !this.state.screenWithoutPermission })}
								/>
							</div>
						</div>
						<div className="col-md-3">
							<div className="form-group">
								<label>With organizer's Permission:</label>
							</div>
						</div>
						<div className="col-md-3">
							<div className="form-group">
								<input
									type="checkbox"
									checked={!this.state.screenWithoutPermission}
									onChange={(e) => this.setState({ screenWithoutPermission: !this.state.screenWithoutPermission })}
								/>
							</div>
						</div>

						<div className="col-md-12">
							<div className="form-group" style={{ padding: "5px" }}>
								{contactList && contactList.length > 0 && contactList.map((address, index) => {
									// return address.isAdded ? <tr style= {{padding: "5px"}}><td style={{color: "#0083C6", padding: "5px"}} key={index}>{address.name}</td></tr> : null
									return address.isAdded ?
										<Badge pill variant="success" style={{ fontSize: "12px", margin: "3px" }} key={index}>{address.name}</Badge>
										: null
								})
								}
							</div>
						</div>

						<table style={{ marginLeft: '20px' }}>
							<tbody>
								<tr style={{ marginTop: '10px' }}>
									<td>
										<Button variant="primary" onClick={() => this.openAddressBookModal()}>
											Add/Remove Participant
                    </Button>
										{/* <h6 variant="primary" onClick={() => this.openAddressBookModal()}> Add/Remove Participant </h6> */}
									</td>
								</tr>
							</tbody>
						</table>

					</div>
				</Modal.Body>
				<Modal.Footer>
					{/* <Button variant="secondary" onClick={() => this.hideAddAddress()}>
            Close
          </Button> */}
					<Button variant="primary" onClick={() => this.createConference()}>
						Save
          </Button>
				</Modal.Footer>
			</Modal>

			{/* Address book modal --START */}
			<Modal size={"lg"} show={this.state.showAddressBook} onHide={() => this.hideAddressBookModal()}>
				<Modal.Header closeButton>
					<Modal.Title>Address book</Modal.Title>
				</Modal.Header>
				<Modal.Body style={{ minHeight: '400px' }}>
					<div className="mng-full-table modal-adrs-book">
						<div className="row">
							<div className="col-md-12">
								<div className="mng-full-table-hdr">
									<div className="row">
										<div className="col-md-2 border-rt">
											<h6>Add/Remove</h6>
										</div>
										<div className="col-md-4 border-rt">
											<h6>Contact Name</h6>
										</div>
										<div className="col-md-3 border-rt">
											<h6>User Type</h6>
										</div>
										<div className="col-md-3">
											<h6>Phone Number</h6>
										</div>
									</div>
								</div>
							</div>

						</div>
						{contactList && contactList.length > 0 && contactList.map((address, index) => {
							return <div className="row" key={index}>
								<div className="col-md-12">
									<div className="mng-full-table-row">
										<div className="row">
											<div className="col-md-2 border-rt">
												<h6>Add</h6>
												<input type="checkbox" value={address.isAdded} defaultChecked={address.isAdded} onChange={(event) => this.addOrRemoveToConference(address._id)} />
											</div>
											<div className="col-md-4 border-rt">
												<h6>Contact Name</h6>
												<p><a href="#">{address.name}</a></p>
											</div>
											<div className="col-md-3 border-rt">
												<h6>User Type</h6>
												<p>{address.userType}</p>
											</div>
											<div className="col-md-3">
												<h6>Phone Number</h6>
												<p>{address.phoneNumber}</p>
											</div>
											{/* <div className="mobile-ad-edt-btns">
                        <ul>
                          <li><a href="#"><i className="fas fa-eye"></i></a></li>
                          <li><a href="JavaScript:Void(0);" data-toggle="modal" data-target="#delModal"><i className="far fa-trash-alt"></i></a></li>
                        </ul>
                      </div> */}
										</div>
									</div>

								</div>
							</div>
						})
						}
					</div>
				</Modal.Body>
				<Modal.Footer>
					<Button variant="primary" onClick={() => this.hideAddressBookModal()}> Save And Close </Button>
				</Modal.Footer>
			</Modal>
			{/* Address book modal --END */}

			{/*Add Member modal --START */}
			<Modal size={"lg"} show={this.state.showMember} onHide={() => this.hideAddMemberModal()}>
				<Modal.Header closeButton>
					<Modal.Title>Address book</Modal.Title>
				</Modal.Header>
				<Modal.Body style={{ minHeight: '400px' }}>
					<div className="mng-full-table modal-adrs-book">
						<div className="row">
							<div className="col-md-12">

								{/* For adding member Start */}
								<div className="row pt-2 pb-2">
									<div className="col-md-8">
										<h4>Add new member to conference</h4>
									</div>

									<div className="col-md-4">
										<button className="btn w-100" onClick={() => this.addMemberFromAddressBook()}>Save and close</button>
									</div>
								</div>
								{/* For adding member End */}

								<div className="mng-full-table-hdr">
									<div className="row">
										<div className="col-md-2 border-rt">
											<h6>Add/Remove</h6>
										</div>
										<div className="col-md-4 border-rt">
											<h6>Contact Name</h6>
										</div>
										<div className="col-md-3 border-rt">
											<h6>User Type</h6>
										</div>
										<div className="col-md-3">
											<h6>Phone Number</h6>
										</div>
									</div>
								</div>

							</div>

						</div>
						{contactList && contactList.length > 0 && contactList.map((address, index) => {
							return <div className="row" key={index}>
								<div className="col-md-12">

									<div className="mng-full-table-row">
										<div className="row">
											<div className="col-md-2 border-rt">
												<h6>Add/Remove</h6>
												<input type="checkbox" value={address.isAdded} defaultChecked={address.isAdded} onChange={(event) => this.addOrRemoveToConference(address._id)} />
											</div>

											<div className="col-md-4 border-rt">
												<h6>Contact Name</h6>
												<p><a href="#">{address.name}</a></p>
											</div>
											<div className="col-md-3 border-rt">
												<h6>User Type</h6>
												<p>{address.userType}</p>
											</div>
											<div className="col-md-3">
												<h6>Phone Number</h6>
												<p>{address.phoneNumber}</p>
											</div>

										</div>
									</div>

								</div>
							</div>
						})
						}

					</div>

				</Modal.Body>
				{/* <Modal.Footer>
          <Button variant="primary" onClick={() => this.addMemberFromAddressBook()}> Save And Close </Button>
        </Modal.Footer> */}
			</Modal>
			{/* Add member modal --END */}

			{/* SIP Group Call Modal start */}
			<Modal size={"lg"} show={this.state.sipCallModal} onHide={() => this.hideSIPCallModal()}>
				<Modal.Header closeButton>
					<Modal.Title>Call Book</Modal.Title>
				</Modal.Header>
				<Modal.Body style={{ minHeight: '400px' }}>

					<div className="mng-full-table modal-adrs-book">

						<div className="row">

							<div className="col-md-12">

								{/* For SIP call Start */}
								{selectedConferenceId !== '' ?
									<div className="row srch-row">
										<div className="col-md-3">
											<input type="text" value={this.state.sipCountryCode} className="form-control" placeholder="Country code" name="sipNo" onChange={(e) => this.setState({ sipCountryCode: e.target.value })} />
										</div>
										<div className="col-md-6">
											<input type="text" value={this.state.sipNo} className="form-control" placeholder="Phone No." name="sipNo" onChange={(e) => this.setState({ sipNo: e.target.value })} />
										</div>
										<div className="col-md-3">
											<button className="btn w-100" onClick={this.dialSipCall}><i className="fas fa-phone"></i></button>
										</div>
									</div>
									: null}
								{/* For SIP call End */}

							</div>

						</div>

					</div>

					<div className="mng-full-table modal-adrs-book">
						<div className="row">
							<div className="col-md-12">

								<div className="row pt-2 pb-2">
									<div className="col-md-8">
										<h4>Call members of conference</h4>
									</div>

									<div className="col-md-4">
										<button className="btn w-100" onClick={() => this.groupSipCallFromAddressBook()}>Call</button>
									</div>
								</div>
								{/* For adding member End */}

								<div className="mng-full-table-hdr">
									<div className="row">
										<div className="col-md-2 border-rt">
											<h6>Select</h6>
										</div>
										<div className="col-md-4 border-rt">
											<h6>Contact Name</h6>
										</div>
										<div className="col-md-3 border-rt">
											<h6>User Type</h6>
										</div>
										<div className="col-md-3">
											<h6>Phone Number</h6>
										</div>
									</div>
								</div>

							</div>

						</div>
						{conferenceMember.length > 0 && conferenceMember.map((member, index) => {

							return <div className="row" key={index}>
								{member.contactNumber != '' ?
									<div className="col-md-12">

										<div className="mng-full-table-row">

											<div className="row">
												<div className="col-md-2 border-rt">
													<h6>Select</h6>
													{/* {!member.isInCall ? */}
													<input type="checkbox" value={member.isCalled} defaultChecked={member.isCalled} onChange={(event) => this.sipCallInGroup(member._id)} />
													{/* : null} */}
												</div>

												<div className="col-md-4 border-rt">
													<h6>Contact Name</h6>
													<p><a href="#">{member.name}</a></p>
												</div>
												<div className="col-md-3 border-rt">
													<h6>User Type</h6>
													<p>{member.userType}</p>
												</div>
												<div className="col-md-3">
													<h6>Phone Number</h6>
													<p>{'' + member.countryCode + member.contactNumber}</p>
												</div>

											</div>

										</div>

									</div>
									: null}
							</div>

						})

						}


					</div>

				</Modal.Body>
				{/* <Modal.Footer>
          <Button variant="primary" onClick={() => this.addMemberFromAddressBook()}> Save And Close </Button>
        </Modal.Footer> */}
			</Modal>
			{/* SIP Group Call Modal start */}

			{/*Delete conference modal --START */}
			<Modal size={"lg"} show={this.state.showDeleteModal} onHide={() => this.hideDeleteModal()}>
				<Modal.Header closeButton>
					<Modal.Title>Delete Conference</Modal.Title>
				</Modal.Header>
				<Modal.Body style={{ minHeight: '50px' }}>
					<h4>Are you sure to delete conference?</h4>
				</Modal.Body>

				<Modal.Footer>
					<Button variant="primary" onClick={() => this.deleteConference()}> Ok </Button>
					<Button variant="primary" onClick={() => this.hideDeleteModal()}> Cancel </Button>
				</Modal.Footer>
			</Modal>
			{/* Delete conference modal --END */}

			{/* Toast Notification Start -- */}
			<Toast
				show={this.state.showToast}
				onClose={() => { this.hideToast() }}
				delay={3500}
				className="toast-class"
				autohide
			>
				<Toast.Header>
					<strong className="mr-auto">{this.state.toastTitle}</strong>
					{/* <small>11 mins ago</small> */}
				</Toast.Header>
				<Toast.Body>{this.state.toastMessage}</Toast.Body>
			</Toast>
			{/* Toast Notification End -- */}

		</main>
	}
	//#endregion
}

export default checkAuthentication(AdminCompanyConference);
