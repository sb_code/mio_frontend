import React, { Component } from 'react'
import Navbar from '../Component/Navbar'
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button } from "react-bootstrap"
import _ from 'lodash';
import { CSVLink, CSVDownload } from "react-csv";
import ReactTooltip from 'react-tooltip'; // For tool tip 

import { checkAuthentication } from '../Component/Authentication';

var path = cred.API_PATH;


export class AdminOutBoundConfiguration extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userId: '',
            userType: '',

            openSideMenu: false,
            searchString: '',
            searching: false,

            loading: false,

            pageNo: 1,
            numberList: [],
            numberListRecords: '',

            selectedNumber: {},

            selectedId: '',
            region: '',
            dialCode: '',
            costPerMinute: '',
            pricePerMinute: '',
            multiplier: '',

            showEditModal: false,

            showSuccessAlert: false,
            successAlertMessage: '',
            showErrorAlert: false,
            errorAlertMessage: '',

        }
        this.perPage = 5;

    }

    componentDidMount() {

        document.body.classList.remove("sign-bg");
        document.body.classList.add("grey-bg");

        let storage = localStorage.getItem("MIO_Local");
        storage = storage ? JSON.parse(storage) : null;

        if (storage && storage.data.userId ) {
            this.setState({
                userId: storage.data.userId,
                userType: storage.data.userType,
                companyId: storage.data.companyId
            }, () => {
                this.getOutBoundList();
                console.log('user management--', this.state.userType);
            });
            let tokenData = this.parseJwt(storage.token);
        }

    }
    
    // For getting country list for filter -- 
    getOutBoundList = () => {
        this.setState({loading: true});

        let dataToSend = {
            "companyId": '',
            "page": this.state.pageNo,
            "perPage": this.perPage,
        };

        axios
            .post(path + 'services/fetch-sip-out-bound-price-factor', dataToSend)
            .then(serverResponse => {

                let res = serverResponse.data;
                if (!res.isError) {
                    let outBoundRecords = res.details && res.details.baseServiceDetails && res.details.baseServiceDetails[0] && res.details.baseServiceDetails[0].results;
                    this.setState({
                        numberList: outBoundRecords,
                        numberListRecords: res.details && res.details.baseServiceDetails && res.details.baseServiceDetails[0] && res.details.baseServiceDetails[0].noofpage,
                        loading: false
                    },()=>{
                        console.log("nunmber list is = ", this.state.numberList);
                    });
                }else{
                    this.setState({loading: false});
                }

            })
            .catch(error => {
                console.log(error);
            })

    }

    // For searching user - 
    searchNumber = (event) => {
        event.preventDefault();
        let { searchCountry, searchProvider } = this.state;
        this.setState({
            searching: true
        }, () => {
            this.getNumberListForAdmin(searchProvider, searchCountry);
        });

    }

    // For clearing search -
    clearSearch = (event) => {
        if (event) {
            event.preventDefault();
        }
        // let {searchCountry, searchProvider, searching} = this.state;
        this.setState({
            searchCountry: '',
            searchProvider: '',
            searching: false
        }, () => {
            this.getNumberListForAdmin()
        });
    }

    // For userList of a particular company - 
    editNumber = (index) => {

        let { numberList } = this.state;
        let selectedNumber = numberList[index];

        this.setState({
            selectedId: selectedNumber.outboundPriceFactor._id,
            region: selectedNumber.outboundPriceFactor.region,
            dialCode: selectedNumber.outboundPriceFactor.dialCode,
            costPerMinute: selectedNumber.outboundPriceFactor.costPerMinute && selectedNumber.outboundPriceFactor.costPerMinute,
            pricePerMinute: selectedNumber.outboundPriceFactor.pricePerMinute && selectedNumber.outboundPriceFactor.pricePerMinute,
            multiplier: selectedNumber.outboundPriceFactor.multiplier && selectedNumber.outboundPriceFactor.multiplier,
            showEditModal: true
        });

    }

    changeCalculation= (name, value)=> {
        let {costPerMinute, multiplier, pricePerMinute} = this.state;

        if(name == "costPerMinute"){
            if(value && !isNaN(value)){
                costPerMinute = value;
                let newValue_1 = parseFloat(parseFloat(multiplier)*parseFloat(costPerMinute)).toFixed('2');
                pricePerMinute = newValue_1 && !isNaN(newValue_1)? newValue_1 : '';
                let newValue_2 = parseFloat(parseFloat(pricePerMinute)/parseFloat(costPerMinute)).toFixed('2');
                multiplier = newValue_2 && !isNaN(newValue_2)? newValue_2: '';

                this.setState({
                    costPerMinute, 
                    multiplier,
                    pricePerMinute
                })
            }else{
                this.setState({
                    costPerMinute: value, 
                    multiplier: '',
                    pricePerMinute: ''
                })
            }
        }else if(name == "multiplier"){

            if(value && !isNaN(value)){
                multiplier = value;
                let newValue = parseFloat(parseFloat(multiplier)*parseFloat(costPerMinute)).toFixed('2');
                pricePerMinute = newValue && !isNaN(newValue)? newValue: '';
                this.setState({
                    costPerMinute, 
                    multiplier,
                    pricePerMinute
                })
            }else{
                this.setState({
                    costPerMinute: costPerMinute, 
                    multiplier: value,
                    pricePerMinute: ''
                })
            }

        }else if(name == "pricePerMinute"){
            if(value && !isNaN(value)){
                pricePerMinute = value;
                let newValue = parseFloat(parseFloat(pricePerMinute)/parseFloat(costPerMinute)).toFixed('2');
                multiplier = newValue && !isNaN(newValue)? newValue: '';
                this.setState({
                    costPerMinute, 
                    multiplier,
                    pricePerMinute
                })
            }else{
                this.setState({
                    costPerMinute: costPerMinute, 
                    multiplier: '',
                    pricePerMinute: value
                })
            }
        }

    }

    UpdateNumber = () => {

        let { selectedId, region, dialCode, costPerMinute, pricePerMinute, multiplier } = this.state;

        if (region && !isNaN(dialCode) && dialCode && !isNaN(costPerMinute) && costPerMinute && !isNaN(multiplier) && multiplier && !isNaN(pricePerMinute) && pricePerMinute ) {
            this.setState({
                loading: true
            });
            let dataToSend = {
                "outboundPriceFactorId": selectedId,
                "region": region,
                "dialCode": dialCode,
                "costPerMinute": costPerMinute,
                "pricePerMinute": pricePerMinute,
                "multiplier": multiplier
            };
            // API -- 
            axios
                .post(path + 'services/update-sip-out-bound-price-factor', dataToSend)
                .then(serverResponse => {

                    let res = serverResponse.data;
                    if (!res.isError) {

                        this.setState({
                            loading: false,
                            showSuccessAlert: true,
                            successAlertMessage: 'Successfully Updated'
                        }, () => {
                            this.hideEditModal();
                            this.getOutBoundList();
                        });

                    } else {
                        this.setState({
                            loading: false,
                            showErrorAlert: true,
                            errorAlertMessage: 'Sorry! Something is wrong',
                        }, () => {
                            this.hideEditModal();
                        });
                    }

                })
                .catch(error => {
                    console.log(error);
                })

        }

    }

    hideEditModal = () => {

        this.setState({
            loading: false,
            selectedId: '',
            province: '',
            dialCode: '',
            rate: '',
            price: '',
            costPerMinute: '', 
            multiplayer: '',
            pricePerMinute: '',
            showEditModal: false            
        });

    }

    deleteNumber= (index)=> {

        let {numberList} = this.state;
        let selectedNumber = numberList[index];
        this.setState({loading: true});

        let dataToSend= {
            "outboundPriceFactorId": selectedNumber.outboundPriceFactor && selectedNumber.outboundPriceFactor._id
        };  
        // API -- 
        axios
        .post(path+'services/delete-sip-out-bound-price-factor', dataToSend)
        .then(serverResponse=>{

            let res = serverResponse.data;
            if(!res.isError){

                this.setState({
                    loading: false,
                    showSuccessAlert: true,
                    successAlertMessage: 'Successfully deleted'
                },()=>{
                    this.getOutBoundList();
                });

            }else{
                this.setState({
                    loading: false,
                    showErrorAlert: true,
                    errorAlertMessage: 'Sorry! Something is wrong.'
                })
            }

        })
        .catch(error=>{
            console.log(error);
        })


    }

    pageChangePrev = () => {

        this.setState({
            pageNo: (parseFloat(this.state.pageNo) - 1)
        }, () => {
            this.getOutBoundList()
        });

    }

    pageChangeNext = () => {

        this.setState({
            pageNo: (parseFloat(this.state.pageNo) + 1)
        }, () => {
            this.getOutBoundList()
        });

    }

    clearAlert = () => {
        this.setState({
            loading: false,
            showSuccessAlert: false,
            successAlertMessage: '',
            showErrorAlert: false,
            errorAlertMessage: '',
        });
    }

    sideMenuToggle = () => {
        let { openSideMenu } = this.state;
        openSideMenu = !openSideMenu;
        this.setState({ openSideMenu });
    }

    backMenueToggle = () => {
        this.props.history.goBack();
    }

    mobMenuClick() {
        this.sideMenuToggle();
        console.log('mobMenuClick');
    }

    parseJwt = (token) => {
        if (!token) { return; }
        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace('-', '+').replace('_', '/');
        return JSON.parse(window.atob(base64));
    }


    render() {

        let { loading, numberList, selectedNumber, assignedNumbers,  pageNo, numberListRecords, selectedArray, selectedId,
        region,
        dialCode,
        costPerMinute,
        pricePerMinute,
        multiplier } = this.state;
        const perPage = this.perPage;

        return <main>
            <section className="user-mngnt-wrap">
                <div className="container-fluid">
                    <div className="row">

                        <Navbar routTo="/numberconfigue" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />
                        <div className="col-xl-10 col-lg-9 p-0 mob-full-conf">

                            <div className="mobile-menu-header">
                                <a href= "JavaScript:Void(0);" className="back-arw" onClick={() => this.backMenueToggle()}><i className="fas fa-arrow-left"></i></a>
                                {/* <a href="JavaScript:Void(0);" className="menu-tgl" onClick={() => this.sideMenuToggle()}><img src="images/menu-tgl.png" alt="" /></a> */}
                            </div>

                            <div className="mng-full-list">

                                <div className="mng-full-srch">
                                    <div className="row">
                                        
                                        {/* <div className="col col-md-12">
                                            <div className="srch-wrap">
                                                <form>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        name="searchString"
                                                        value={this.state.searchString}
                                                        onChange={(e) => { this.setState({ searchString: e.target.value }) }}
                                                    />
                                                    {this.state.searching ?
                                                        <input type="button"
                                                            data-tip='Clear'
                                                            onClick={(event) => this.clearSearch(event)}
                                                        />
                                                        :
                                                        <input type="submit"
                                                            data-tip='Search'
                                                            onClick={(event) => this.searchNumber(event)}
                                                        />
                                                    }
                                                    <ReactTooltip
                                                        effect="float"
                                                        place="top"
                                                        data-border="true"
                                                    />
                                                </form>
                                            </div>
                                        </div> */}
                                    
                                    </div>

                                </div>

                                    <div className="mng-full-table">
                                        <div className="row">
                                            
                                            <div className="col-md-8">
                                                <div className="mng-full-table-hdr">
                                                    <div className="row">
                                                        <div className="col-md-3 border-rt">
                                                            <h6>Region</h6>
                                                        </div>
                                                        <div className="col-md-3 border-rt">
                                                            <h6>Provider</h6>
                                                        </div>
                                                        <div className="col-md-3 border-rt">
                                                            <h6>Code</h6>
                                                        </div>
                                                        <div className="col-md-3">
                                                            <h6>Cost/Minute</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="col-md-4">
                                                <div className="mng-full-table-hdr">
                                                    <div className="row">
                                                        <div className="col-md-6 border-rt">
                                                            <h6>Edit</h6>
                                                        </div>
                                                        <div className="col-md-6">
                                                            <h6>Delete</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>

                                        {numberList && numberList.map((user, index) => {

                                            return <div className="row" key={index}>

                                                <div className="col-md-8">
                                                    <div className="mng-full-table-row">
                                                        <div className="row">
                                                            <div className="col-md-3 border-rt">
                                                                <h6>Region</h6>
                                                                <p className="textEllips">
                                                                    <a href="JavaScript:Void(0);" onClick={() => this.editNumber(index)}>
                                                                        {user.outboundPriceFactor.region}
                                                                    </a>
                                                                </p>
                                                            </div>
                                                            <div className="col-md-3 border-rt">
                                                                <h6>Region</h6>
                                                                <p className="textEllips">
                                                                    {user.outboundPriceFactor.provider}
                                                                </p>
                                                            </div>
                                                            <div className="col-md-3 border-rt">
                                                                <h6>Code</h6>
                                                                <p className="textEllips">{user.outboundPriceFactor.dialCode}</p>
                                                            </div>
                                                            <div className="col-md-3">
                                                                <h6>Cost/Minute</h6>
                                                                <p className="textEllips">
                                                                    {user.outboundPriceFactor.costPerMinute && parseFloat(user.outboundPriceFactor.costPerMinute).toFixed('2')}
                                                                </p>
                                                            </div>
                                                            
                                                            <div className="mobile-ad-edt-btns">
                                                                <ul>

                                                                    <li onClick={() => this.editNumber(index)}>
                                                                        <a href="JavaScript:Void(0);"
                                                                            data-tip='View'
                                                                        >
                                                                            <i className="far fa-edit"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li onClick={() => this.deleteNumber(index)}>
                                                                        <a href="JavaScript:Void(0);"
                                                                            data-tip='delete'
                                                                        >
                                                                            <i className="far fa-trash-alt"></i>
                                                                        </a>
                                                                    </li>

                                                                </ul>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>

                                                <div className="col-md-4">
                                                    <div className="mng-full-table-row add-edt text-center">
                                                        <div className="row">

                                                            <div className="col-md-6">
                                                                <a href="JavaScript:Void(0);"
                                                                    data-tip='Edit'
                                                                    onClick={() => { this.editNumber(index); }}
                                                                >
                                                                    <i className="far fa-edit"></i>
                                                                    <ReactTooltip
                                                                        effect="float"
                                                                        place="top"
                                                                        data-border="true"
                                                                    />
                                                                </a>
                                                            </div>
                                                            <div className="col-md-6">
                                                                <a href="JavaScript:Void(0);"
                                                                    data-tip='Edit'
                                                                    onClick={() => { this.deleteNumber(index); }}
                                                                >
                                                                    <i className="far fa-trash-alt"></i>
                                                                    <ReactTooltip
                                                                        effect="float"
                                                                        place="top"
                                                                        data-border="true"
                                                                    />
                                                                </a>
                                                            </div>
                                                        
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                            </div>

                                        })}
                                        
                                    </div>

                                <br /><br />
                                <div className="form-group">
                                    {!loading && parseFloat(pageNo) > 1 ?
                                        <span style={{ color: "#007bff", cursor: "pointer" }}
                                            onClick={() => { this.pageChangePrev() }}
                                        >
                                            <b>{"<< Prev"}</b>
                                        </span>
                                        :
                                        null}
                                        &nbsp;&nbsp;|&nbsp;&nbsp;
                                        {!loading && (parseFloat(pageNo) < parseFloat(numberListRecords)) ?
                                        <span style={{ color: "#007bff", cursor: "pointer" }}
                                            onClick={() => { this.pageChangeNext() }}
                                        >
                                            <b>{"Next >>"}</b>
                                        </span>
                                        :
                                        null}
                                    {loading ?
                                        <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                                            <i className="fa fa-spinner fa-spin" ></i>
                                        </a>
                                        :
                                        null}

                                </div>

                            </div>

                        </div>
                    </div>
                </div>

            </section>

            <Modal size="lg" show={this.state.showEditModal} onHide={() => { this.hideEditModal() }}>
                <Modal.Header closeButton>
                    <Modal.Title>Edit Out Bound Detals</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    
                        <div className="row">

                            <div className="col-md-3">
                                <div className="form-group">
                                    <label>Region:</label>
                                    <input
                                        type="text"
                                        name="region"
                                        value={region}
                                        className="form-control"
                                        placeholder="Enter region"
                                        onChange={event => this.setState({ region: event.target.value })}
                                        onFocus={() => this.setState({ provinceError: "" })}
                                    />
                                    
                                </div>
                            </div>

                            <div className="col-md-3">
                                <div className="form-group">
                                    <label>Code:</label>
                                    <input
                                        type="tel"
                                        name="dialCode"
                                        value={dialCode}
                                        className="form-control"
                                        onChange={event => this.setState({ dialCode: event.target.value })}
                                        onFocus={() => this.setState({ dialCodeError: "" })}
                                    />
                                    <span style={{ color: 'red' }}>{!dialCode ? '* Please give valid price' : ''}</span>
                                </div>
                            </div>

                            <div className="col-md-2">
                                <div className="form-group">
                                    <label>Cost(/min):</label>
                                    <input
                                        type="text"
                                        name="costPerMinute"
                                        value={costPerMinute? costPerMinute: ''}
                                        className="form-control"
                                        placeholder="0.00"
                                        // onChange={event => this.setState({ costPerMinute: event.target.value })}
                                        onChange={(event) => this.changeCalculation("costPerMinute", event.target.value)}
                                        onFocus={() => this.setState({ rateError: "" })}
                                    />
                                    <span style={{ color: 'red' }}>{!costPerMinute ? '* Please give valid cost' : ''}</span>                                    
                                </div>
                            </div>

                            <div className="col-md-2">
                                <div className="form-group">
                                    <label>Multiplier:</label>
                                    <input
                                        type="text"
                                        name="multiplier"
                                        value={multiplier? multiplier: ''}
                                        className="form-control"
                                        placeholder="0.00"
                                        onChange={(event) => this.changeCalculation("multiplier", event.target.value)}
                                        onFocus={() => this.setState({ rateError: "" })}
                                    />
                                    <span style={{ color: 'red' }}>{!multiplier ? '* Please give valid value' : ''}</span>                                    
                                </div>
                            </div>

                            <div className="col-md-2">
                                <div className="form-group">
                                    <label>Price(/min):</label>
                                    <input
                                        type="text"
                                        name="pricePerMinute"
                                        value={pricePerMinute? pricePerMinute: ''}
                                        className="form-control"
                                        placeholder="0.00"
                                        onChange={(event) => this.changeCalculation("pricePerMinute", event.target.value)}
                                        onFocus={() => this.setState({ rateError: "" })}
                                    />
                                    <span style={{ color: 'red' }}>{!pricePerMinute ? '* Please give valid price' : ''}</span>                                    
                                </div>
                            </div>

                        </div>
                </Modal.Body>
                <Modal.Footer>
                    {!loading ?
                        <Button variant="primary" onClick={() => this.UpdateNumber()}>
                            Save
                        </Button>
                        :
                        null}
                    {loading ?
                        <Button variant="primary" >
                            <i className="fa fa-spinner fa-spin" ></i>
                        </Button>
                        :
                        null}
                </Modal.Footer>
            </Modal>

            <Modal show={this.state.showSuccessAlert} onHide={() => this.clearAlert()}>
                <Modal.Header closeButton>
                    <Modal.Title>OK</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h5 style={{ color: 'green' }}>{`${this.state.successAlertMessage}`}</h5>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={() => this.clearAlert()}>
                        Ok
            </Button>
                </Modal.Footer>
            </Modal>

            <Modal show={this.state.showErrorAlert} onHide={() => this.clearAlert()}>
                <Modal.Header closeButton>
                    <Modal.Title>OK</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h5 style={{ color: 'red' }}>{`${this.state.errorAlertMessage}`}</h5>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={() => this.clearAlert()}>
                        Ok
            </Button>
                </Modal.Footer>
            </Modal>

        </main>

    }

}

export default checkAuthentication(AdminOutBoundConfiguration);