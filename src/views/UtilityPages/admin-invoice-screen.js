import React, { Component } from 'react'
import Navbar from '../Component/Navbar'
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button } from "react-bootstrap"
import _ from 'lodash';

import { checkAuthentication } from '../Component/Authentication';

var path = cred.API_PATH;


export class AdminInvoice extends Component {
	constructor(props) {
		super(props)
		this.state = {
			userId: '',
			companyType: '',
			companyId: '',
			companyName: '',
			companyMailId: '',

			openSideMenu: false,

			loading: false,

			selectedInvoiceId: '',
			invoiceMonth: '',
			invoiceYear: '',
			invoiceCompanyId: '',
			invoiceCompany: '',
			billingAddress: '',

			invoiceDetails: {},
			invoiceServices: [],
			issueDate: '',
			paymentGateway: '',
			defaultCurrency: '',
			totalDuration: 0,

			showSuccessAlert: false,
			successAlertMessage: '',
			showErrorAlert: false,
			errorAlertMessage: ''

		}

	}

	componentDidMount() {
		document.body.classList.remove("sign-bg");
		document.body.classList.add("grey-bg");

		if (this.props.location.state && this.props.location.state.month && this.props.location.state.year) {

			let invoiceId = this.props.match.params.invoiceid;
			let invoiceCompanyId = this.props.match.params.companyid;
			let invoiceMonth = this.props.location.state && this.props.location.state.month;
			let invoiceYear = this.props.location.state && this.props.location.state.year;
			let invoiceCompany = this.props.location.state && this.props.location.state.company;

			let storage = localStorage.getItem("MIO_Local");
			storage = storage ? JSON.parse(storage) : null;

			if (storage && storage.data.userId) {
				this.setState({
					userId: storage.data.userId,
					companyType: storage.data.companyType,
					companyId: storage.data.companyId,
					invoiceCompanyId: invoiceCompanyId,
					invoiceCompany: invoiceCompany,
					companyName: storage.data.companyName,
					selectedInvoiceId: invoiceId,
					invoiceMonth: invoiceMonth,
					invoiceYear: invoiceYear
				}, () => {

					this.viewCompanyDetails();
					this.viewInvoiceDetails(); // getting selected invoice details 
				});

			}
		} else {
			this.props.history.push("/paymentmanagement");
		}

	}

	viewCompanyDetails = () => {

		let dataToSend = {
			"companyId": this.state.invoiceCompanyId
		};
		console.log("dataToSend==", dataToSend);
		// API - -
		axios
			.post(path + 'user/get-company-by-id', dataToSend)
			.then(serverResponse => {
				let res = serverResponse.data;
				if (!res.isError) {
					// let wallet = res.details && res.details.walletBalence;
					let gateway = res.details && res.details.paymentGateway;
					let currency = res.details && res.details.currency;
					// let companyName = res.details && res.details.companyName;
					let companyMailId = res.details && res.details.mailId;
					let billingAddress = res.details && res.details.invoiceBillingAddress;

					this.setState({
						paymentGateway: gateway,
						defaultCurrency: currency,
						companyMailId: companyMailId,
						billingAddress: billingAddress
					}, () => {
						console.log("currency and payment ==", this.state.defaultCurrency);
					})

				}
			})
			.catch(error => {
				console.log(error);
			})

	}

	// getting payment details of user
	viewInvoiceDetails = () => {

		let dataToSend = {
			"invoiceId": this.state.selectedInvoiceId,
		};
		// API / ToDo
		axios
			.post(path + 'price-management/fetch-invoice-By-Id', dataToSend)
			.then(serverResponse => {

				let res = serverResponse.data;
				if (!res.isError) {

					let invoiceDetails = res.details[0];
					let services = invoiceDetails.services;

					let totalDuration = 0;
					services && services.map(value => {
						totalDuration = value.totalDuration ? (parseFloat(value.totalDuration) + parseFloat(totalDuration)).toFixed(2) : parseFloat(totalDuration).toFixed(2);
					})
					let issueDate = invoiceDetails && invoiceDetails.date;

					this.setState({
						invoiceDetails: invoiceDetails,
						invoiceServices: services,
						totalDuration: totalDuration,
						issueDate: this.formatDate(issueDate),
						loading: false
					});
				}

			})
			.catch(error => {
				console.log(error);
			})

	};

	// Date Format -- 
	formatDate = (string) => {

		// new Intl.DateTimeFormat('en-US', {year: 'numeric', month: '2-digit',day: '2-digit', hour: '2-digit', minute: '2-digit', second: '2-digit'}).format(value.date)

		let options = { year: 'numeric', month: 'long', day: 'numeric' };
		return new Date(string).toLocaleDateString([], options);

	}

	// For payment of any month from list -- 
	sendInvoiceByMail = () => {
		// console.log("sendInvoiceByMail");
		let { invoiceDetails, companyMailId, invoiceCompany } = this.state;

		let dataToSend = {
			invoiceUrl: invoiceDetails.invoiceUrl,
			email: companyMailId,
			companyId: invoiceDetails.companyId,
			companyName: invoiceCompany,
			month: invoiceDetails.month,
			year: invoiceDetails.year,
			logoURL: ''
		};
		this.setState({ loading: true });
		// API ToDo -- 
		axios
			.post(path + 'payment/send-invoice', dataToSend)
			.then(serverResponse => {
				let res = serverResponse.data;
				if (!res.isError) {
					this.setState({
						loading: false,
						showSuccessAlert: true,
						successAlertMessage: 'Successfully sent',
					});
					// alert("Successfully sent.");
				} else {
					this.setState({
						loading: false,
						showErrorAlert: true,
						errorAlertMessage: "Sorry! Can't be send",
					})
				}
			})
			.catch(error => {
				console.log(error);
			})


	}

	hideAlert = () => {
		this.setState({
			showSuccessAlert: false,
			successAlertMessage: '',
			showErrorAlert: false,
			errorAlertMessage: '',
			loading: false
		})
	}

	sideMenuToggle() {
		let { openSideMenu } = this.state;
		openSideMenu = !openSideMenu;
		this.setState({ openSideMenu });
	}

	mobMenuClick() {
		this.sideMenuToggle();
		console.log('mobMenuClick');
	}


	render() {
		const monthNames = ["January", "February", "March", "April", "May", "June",
			"July", "August", "September", "October", "November", "December"
		];
		let { loading, invoiceDetails, issueDate, invoiceServices, totalDuration } = this.state;
		return <main>

			<section className="user-mngnt-wrap">
				<div className="container-fluid">
					<div className="row">
						<Navbar routTo="/paymentmanagement" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />
						<div className="col-xl-10 col-lg-9 p-0">
							<div className="mobile-menu-header">
								<a href="JavaScript:Void(0);" className="back-arw"><i className="fas fa-arrow-left"></i></a>
								<a href="JavaScript:Void(0);" className="menu-tgl"><img src="images/menu-tgl.png" alt="" /></a>
							</div>
							<div className="payment-mng-wrap">
								<div className="invoice-main-area">
									<div className="inv-top-info">
										<div className="row">
											<div className="col-md-4">
												<h6>Bill To</h6>
												<p>{invoiceDetails.companyName}
													<br />{invoiceDetails.address}<br />
												</p>
											</div>
											<div className="col-md-4">
												<h6>Invoice Number</h6>
												<p>{invoiceDetails.invoiceNumber}</p>
												<h6>Date Of Issue</h6>
												<p>{issueDate}</p>
											</div>
											<div className="col-md-4">
												<h6>Total <strong>{invoiceDetails.vatAddedTotal ? parseFloat(invoiceDetails.vatAddedTotal).toFixed(2) : 0.0}</strong></h6>
												<h6>Currency {invoiceDetails.currency}</h6>
											</div>
										</div>
									</div>

									<div className="inv-table-info">
										<table className="table table-striped">
											<tbody>
												<tr>
													<th>Service Name</th>
													<th>Duration(min.)</th>
													<th className="text-right">Amount</th>
												</tr>

												{invoiceServices && invoiceServices.length > 0 && invoiceServices.map((value, index) => {

													return <tr key={index}>
														<td>{value.serviceName}</td>
														<td>{value.totalDuration}</td>
														<td className="text-right">{value.totalPrice}</td>
													</tr>

												})}

											</tbody>
										</table>
									</div>

									<div className="inv-table-info">
										<table className="table">
											<tbody>
												<tr>
													<td>&nbsp;</td>
													<td className="text-right"><b>{totalDuration} (Total minutes)</b></td>
													<td className="text-right"><b>Subtotal: {invoiceDetails.totalAmount}</b></td>
												</tr>
												<tr>
													<td>&nbsp;</td>
													<td className="text-right"><b></b></td>
													<td className="text-right"><b>VAT ({invoiceDetails.vat} %): {invoiceDetails.vatAmount}</b></td>
												</tr>
												<tr>
													<td>&nbsp;</td>
													<td className="text-right"><h5></h5></td>
													<td className="text-right"><h5>Total: {invoiceDetails.vatAddedTotal ? parseFloat(invoiceDetails.vatAddedTotal).toFixed(2) : 0.0}</h5></td>
												</tr>
											</tbody>
										</table>
									</div>

									<div className="inv-option">
										{!loading && invoiceDetails.invoiceUrl ?
											<a href={invoiceDetails.invoiceUrl} target="blank" className="btn" style={{ margin: '10px' }}><i className="fas fa-download" /> Download</a>
											:
											null
										}
										<a href="JavaScript:Void(0);" className="btn" style={{ margin: '10px' }} onClick={this.sendInvoiceByMail}> <i className="fas fa-envelope" /> Email </a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<Modal show={this.state.showSuccessAlert} onHide={() => this.hideAlert()}>
				<Modal.Header closeButton>
					<Modal.Title>OK</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<h5 style={{ color: 'green' }}>{`${this.state.successAlertMessage}`}</h5>
				</Modal.Body>
				<Modal.Footer>
					<Button variant="primary" onClick={() => this.hideAlert()}>
						Ok
            </Button>
				</Modal.Footer>
			</Modal>

			<Modal show={this.state.showErrorAlert} onHide={() => this.hideAlert()}>
				<Modal.Header closeButton>
					<Modal.Title>OK</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<h5 style={{ color: 'red' }}>{`${this.state.errorAlertMessage}`}</h5>
				</Modal.Body>
				<Modal.Footer>
					<Button variant="primary" onClick={() => this.hideAlert()}>
						Ok
                    </Button>
				</Modal.Footer>
			</Modal>


		</main >

	}

}

export default checkAuthentication(AdminInvoice);