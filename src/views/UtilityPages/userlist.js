import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import cred from "../../../src/cred.json";
var path = cred.API_PATH + "admin/";

class Userlist extends Component {
  constructor(props) {
    super(props);

    this.state = {
      userList: [],
      searchTerm: ""
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
  }

  componentDidMount() {
    document.body.classList.remove('sign-bg')
    document.body.classList.add('grey-bg')
    this.getUserList();
  }

  handleChange(event) {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });


  }

  handleSearch(event) {
    event.preventDefault()
    this.searchUserList();
  }

  searchUserList() {
    // console.log(searchTerm)
    axios
      .post(
        path + "find_user",
        {
          search_term: this.state.searchTerm
        },
        {
          headers: { "Content-Type": "application/json" }
        }
      )
      .then(serverResponse => {
        console.log("Response from server : ", serverResponse);
        const res = serverResponse.data;
        if (!res.isError) {
          this.setState({
            userList: res.details
          });
        } else {
          alert("Some error occured");
        }
      });
  }

  getUserList() {
    axios
      .post(
        path + "get_user_list",
        {},
        {
          headers: { "Content-Type": "application/json" }
        }
      )
      .then(serverResponse => {
        console.log("Response from server : ", serverResponse);
        const res = serverResponse.data;
        if (!res.isError) {
          this.setState({
            userList: res.details
          });
        } else {
          alert("Some error occured");
        }
      });
  }

  viewEditUser(userId){
    console.log("uuu",userId)
    this.props.history.push(`/adduser/${userId}`);
  }

  removeUser(uId){
    axios
      .post(
        path + "remove_user",
        {user_id : uId},
        {
          headers: { "Content-Type": "application/json" }
        }
      )
      .then(serverResponse => {
        console.log("Response from server : ", serverResponse);
        const res = serverResponse.data;
        if (!res.isError) {
          this.getUserList();
        } else {
          alert("Some error occured");
        }
      });
  }

  render() {
    return (
     <React.Fragment>
       {/* --content sction-- */}
        <main>
          <section className="user-mngnt-wrap">
            <div className="container-fluid">
              <div className="row">
                <div className="col-xl-2 col-lg-3 p-0 main-menu-lt-col">
                  <div className="main-menu-lt">
                    <div className="logobox">
                      <img src="images/logo-sign.png" alt="" />
                    </div>
                    <div className="user-top-area">
                      <div className="userimg">
                        <img src="images/user-img.png" alt="" />
                      </div>
                      <h5>
                        Mr. John Doe<small>Company Name</small>
                      </h5>
                    </div>

                    <div className="lt-menu">
                      <ul>
                        <li className="current-menu">
                          <a href="#">User Management</a>
                        </li>
                        <li>
                          <a href="#">Unique participation Code/ID</a>
                        </li>
                        <li>
                          <a href="#">Prescreening by Operator</a>
                        </li>
                        <li>
                          <a href="#">Operator management</a>
                        </li>
                        <li>
                          <a href="#">Service Management</a>
                        </li>
                        <li>
                          <a href="#">About Management</a>
                        </li>
                        <li>
                          <a href="#">Upgrade account</a>
                        </li>
                        <li>
                          <a href="#">Payment Management</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div className="col-xl-10 col-lg-9 p-0">
                  <div className="mobile-menu-header">
                    <a href="#" className="back-arw">
                      <i className="fas fa-arrow-left"></i>
                    </a>
                    <a href="#" className="menu-tgl">
                      <img src="images/menu-tgl.png" alt="" />
                    </a>
                  </div>

                  <div className="mng-full-list">
                    <div className="mng-full-srch">
                      <div className="row">
                        <div className="col-md-8">
                          <div className="srch-wrap">
                            <form>
                              <input
                                type="text"
                                name="searchTerm"
                                className="form-control"
                                placeholder="Search User"
                                onChange={this.handleChange}
                              />
                              <input
                                type="submit"
                                onClick={this.handleSearch}
                              />
                            </form>
                          </div>
                        </div>
                        <div className="col-md-4">
                        <Link to="/adduser/new">
                          <div className="add-user-btn">
                            <a href="#" className="btn">
                              Add New User
                            </a>
                          </div></Link>
                        </div>
                      </div>
                    </div>

                    <div className="mng-full-table">
                      <div className="row">
                        <div className="col-md-8">
                          <div className="mng-full-table-hdr">
                            <div className="row">
                            <div className="col-md-3 border-rt">
                                <h6>Company</h6>
                              </div>
                              <div className="col-md-3 border-rt">
                                <h6>First Name</h6>
                              </div>
                              <div className="col-md-3 border-rt">
                                <h6>Last Name</h6>
                              </div>
                              <div className="col-md-3">
                                <h6>Email</h6>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div className="col-md-4">
                          <div className="mng-full-table-hdr">
                            <div className="row">
                              <div className="col-md-4 border-rt">
                                <h6>View</h6>
                              </div>
                              <div className="col-md-4 border-rt">
                                <h6>Edit</h6>
                              </div>
                              <div className="col-md-4">
                                <h6>Delete</h6>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      {this.state.userList.map(eachUser => (
                        <div className="row" key={eachUser.userId}>
                          <div className="col-md-8">
                            <div className="mng-full-table-row">
                              <div className="row">
                                <div className="col-md-3 border-rt">
                                  <h6>Company</h6>
                                  <p>{eachUser.company}</p>
                                </div>
                                <div className="col-md-3 border-rt">
                                  <h6>First Name</h6>
                                  <p>{eachUser.fname}</p>
                                </div>
                                <div className="col-md-3 border-rt">
                                  <h6>Last Name</h6>
                                  <p>{eachUser.lname}</p>
                                </div>
                                <div className="col-md-3">
                                  <h6>Email</h6>
                                  <p>{eachUser.email}</p>
                                </div>
                                <div className="mobile-ad-edt-btns">
                                  <ul>
                                    <li onClick = {()=> this.viewEditUser(eachUser.userId)}>
                                      <a href="#" >
                                        <i 
                                        className="fas fa-eye">
                                        </i>
                                      </a>
                                    </li>
                                    <li onClick = {()=> this.viewEditUser(eachUser.userId)}>
                                      <a href="#">
                                        <i className="far fa-edit"></i>
                                      </a>
                                    </li>
                                    <li >
                                      <a
                                        href="JavaScript:Void(0);"
                                        onClick = {()=> this.removeUser(eachUser.userId)}
                                        // data-toggle="modal"
                                        // data-target="#delModal"
                                      >
                                        <i className="far fa-trash-alt"></i>
                                      </a>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div className="col-md-4">
                            <div className="mng-full-table-row add-edt text-center">
                              <div className="row">
                                <div className="col-md-4" onClick = {()=> this.viewEditUser(eachUser.userId)}>
                                  <a href="#">
                                    <i className="fas fa-eye"></i>
                                  </a>
                                </div>
                                <div className="col-md-4" onClick = {()=> this.viewEditUser(eachUser.userId)}>
                                  <a href="#">
                                    <i className="far fa-edit"></i>
                                  </a>
                                </div>
                                <div className="col-md-4" onClick = {()=> this.removeUser(eachUser.userId)}>
                                  <a
                                    href="JavaScript:Void(0);"
                                    // data-toggle="modal"
                                    // data-target="#delModal"
                                  >
                                    <i className="far fa-trash-alt"></i>
                                  </a>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      ))}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </main>
        {/* --content sction-- */}

        {/* -- The Modal -- */}

        <div className="modal fade" id="delModal">
          <div className="modal-dialog modal-md modal-dialog-centered">
            <div className="modal-content">
              {/* -- Modal body -- */}
              <div className="modal-body">
                <div className="user-del-optn">
                  <h4>Delete User</h4>
                  <p>Do you want to delete this user?</p>
                  <div className="dlt-optn-ftr">
                    <a href="#" className="btn">
                      Yes
                    </a>
                    <a href="#" className="btn dark-btn">
                      No
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/*-- The Modal --*/}
      </React.Fragment>
    );
  }
}

export default Userlist;
