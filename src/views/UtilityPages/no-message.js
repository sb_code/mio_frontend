import React, { Component } from "react";
import Navbar from "../Component/Navbar";

class NoMessage extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    document.body.classList.remove("sign-bg");
    document.body.classList.add("grey-bg");
  }

  render() {
    return (
      <main>
        <section className="user-mngnt-wrap">
          <div className="container-fluid">
            <div className="row">
              <Navbar />
              <div className="col-xl-2 col-lg-3 p-0 main-menu-lt-col-2">
                <div className="main-menu-lt">
                  <div className="inv-code-top">
                    <h5>Invitation Code</h5>
                    <div className="inv-code-form">
                      <form>
                        <input
                          type="text"
                          name=""
                          className="form-control"
                          placeholder="Paste Link"
                        />
                        <button className="btn">Invite to Conference</button>
                      </form>
                    </div>
                  </div>
                  <h5>Online Users</h5>
                  <div className="sdbar-srch">
                    <form>
                      <input
                        type="text"
                        placeholder="Search"
                        className="form-control"
                      />
                      <input type="submit" />
                    </form>
                  </div>
                  <div className="lt-menu">
                    <ul>
                      <li className="current-menu">
                        <a href="#">
                          <span className="user-img">
                            <img src="/images/conf-big-user.png" />
                          </span>
                          User1
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <span className="user-img">
                            <img src="/images/conf-big-user.png" />
                          </span>
                          User2
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <span className="user-img">
                            <img src="/images/conf-big-user.png" />
                          </span>
                          User3
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <span className="user-img">
                            <img src="/images/conf-big-user.png" />
                          </span>
                          User4
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <span className="user-img">
                            <img src="/images/conf-big-user.png" />
                          </span>
                          User5
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="col-xl-8 col-lg-6 p-0">
                <div className="mobile-menu-header">
                  <a href="#" className="back-arw">
                    <i className="fas fa-arrow-left"></i>
                  </a>
                  <a href="#" className="menu-tgl">
                    <img src="/images/menu-tgl.png" alt="" />
                  </a>
                </div>

                <div className="confrnc-outer">
                  <div className="wait-list">
                    <img src="/images/no-msg-graphic.png" alt="" />
                    <h2>
                      No user is online now.
                      <small>Invite users to start chatting</small>
                    </h2>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
    );
  }
}

export default NoMessage;
