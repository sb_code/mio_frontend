import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import cred from "../../../src/cred.json";
import { Snackbar } from "@material-ui/core";
import MySnackbarContentWrapper from "../Component/MaterialSnack";
import Navbar from "../Component/Navbar.js";
var path = cred.API_PATH + "admin/";

class Userlist extends Component {
  constructor(props) {
    super(props);

    this.state = {
      usrFname: "",
      usrLname: "",
      usrEmail: "",
      usrMobile: "",
      usrId: "",
      upc: "",
      company: "",
      mode: "ADD",

      open: false,
      notiMessage: "",
      notiType: "info"
    };

    this.handleChange = this.handleChange.bind(this);
    //   this.handleSubmit = this.handleSubmit.bind(this);
  }

  openModal = (type, msg) => {
    this.setState({
      open: true,
      notiMessage: msg,
      notiType: type
    });
  };

  handleModalClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({
      open: false
    });
    // setOpen(false);
  };

  componentDidMount() {
    document.body.classList.remove("sign-bg");
    document.body.classList.add("grey-bg");
    if (this.props.match.params.id != "new") {
      this.getUserDetails(this.props.match.params.id);
      this.setState({ mode: "EDIT" });
    } else {
      this.setState({ mode: "ADD" });
    }
  }

  handleChange(event) {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  }

  getUserDetails(uid) {
    axios
      .post(
        path + "get_user_details",
        { user_id: uid },
        {
          headers: { "Content-Type": "application/json" }
        }
      )
      .then(serverResponse => {
        console.log("Response from server : ", serverResponse);
        const res = serverResponse.data;
        if (!res.isError) {
          let usrDetails = res.details;
          
          this.setState({
            usrFname: usrDetails.fname,
            usrLname: usrDetails.lname,
            usrEmail: usrDetails.email,
            usrMobile: usrDetails.mobile,
            usrId: usrDetails.userId,
            upc: usrDetails.upc,
            company: usrDetails.company
          });
        } else {
          alert("Some error occured");
        }
      });
  }

  addEditUser() {
    let that = this;
    axios
      .post(
        path + "add_edit_user",
        {
          userid: that.state.usrId,
          fname: that.state.usrFname,
          lname: that.state.usrLname,
          email: that.state.usrEmail,
          mobile: that.state.usrMobile,
          upc: that.state.upc,
          company: that.state.company,
          mode: that.state.mode
        },
        {
          headers: { "Content-Type": "application/json" }
        }
      )
      .then(serverResponse => {
        console.log("Response from server : ", serverResponse);
        const res = serverResponse.data;
        if (!res.isError) {
          if (that.state.mode == "ADD") {
            that.openModal("success", "Profile added successfully");
            setTimeout(function() {
              that.props.history.push(`/userlist`);
            }, 3000);
          } else {
            that.openModal("success", "Profile updated successfully");
            setTimeout(function() {
              that.props.history.push(`/userlist`);
            }, 3000);
          }
        } else {
          this.openModal("error", "Some error occured");
        }
      });
  }

  goBack() {
    this.props.history.push(`/userlist`);
  }

  render() {
    return (
      <main>
        <section className="user-mngnt-wrap">
          <div className="container-fluid">
            <div className="row">
              {/* col-xl-2 col-lg-3 */}
              <Navbar />
              <div className="col-xl-10 col-lg-9 p-0">
                <div className="mobile-menu-header">
                  <a href="#" className="back-arw">
                    <i className="fas fa-arrow-left"></i>
                  </a>
                  <a href="#" className="menu-tgl">
                    <img src="images/menu-tgl.png" alt="" />
                  </a>
                </div>

                <div className="custom-brdcrmb">
                  <nav aria-label="breadcrumb">
                    <ol className="breadcrumb">
                      <li className="breadcrumb-item">
                        <Link to="/userlist">User Management</Link>
                      </li>
                      <li
                        className="breadcrumb-item active"
                        aria-current="page"
                      >
                        Edit User Details
                      </li>
                    </ol>
                  </nav>
                </div>

                <div className="payment-mng-wrap usr-mng-form">
                  <form>
                    <div className="row">
                      <div className="col-md-6">
                        <div className="form-group">
                          <label>First Name:</label>
                          <input
                            type="text"
                            name="usrFname"
                            value={this.state.usrFname}
                            className="form-control"
                            placeholder="Enter First Name"
                            onChange={event => this.handleChange(event)}
                          />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-group">
                          <label>Last Name:</label>
                          <input
                            type="text"
                            name="usrLname"
                            value={this.state.usrLname}
                            className="form-control"
                            placeholder="Enter Last Name"
                            onChange={event => this.handleChange(event)}
                          />
                        </div>
                      </div>
                      <div className="col-md-12">
                        <div className="form-group">
                          <label>Email Address:</label>
                          <input
                            type="email"
                            name="usrEmail"
                            value={this.state.usrEmail}
                            className="form-control"
                            placeholder="Enter Email Address"
                            onChange={event => this.handleChange(event)}
                          />
                        </div>
                      </div>
                      <div className="col-md-12">
                        <div className="form-group">
                          <label>Mobile Number:</label>
                          <input
                            type="tel"
                            name="usrMobile"
                            value={this.state.usrMobile}
                            className="form-control"
                            placeholder="Enter Mobile Number"
                            onChange={event => this.handleChange(event)}
                          />
                        </div>
                      </div>
                      <div className="col-md-12">
                        <div className="form-group">
                          <label>Company Name:</label>
                          <input
                            type="text"
                            name="company"
                            value={this.state.company}
                            className="form-control"
                            placeholder="Enter company name"
                            onChange={event => this.handleChange(event)}
                          />
                        </div>
                      </div>
                      <div className="col-md-12">
                        <div className="form-group">
                          <label>Unique participation code:</label>
                          <input
                            type="text"
                            name="upc"
                            value={this.state.upc}
                            className="form-control"
                            placeholder="Enter participation code"
                            onChange={event => this.handleChange(event)}
                          />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-group">
                          <button
                            type="button"
                            name=""
                            className="btn cancl-btn"
                            onClick={() => this.goBack()}
                          >
                            <i className="fas fa-ban"></i> Cancel
                          </button>
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-group">
                          <button
                            type="button"
                            name=""
                            className="btn"
                            onClick={() => this.addEditUser()}
                          >
                            <i className="far fa-save"></i> Save
                          </button>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </section>
        <Snackbar
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "center"
          }}
          open={this.state.open}
          autoHideDuration={4000}
          onClose={() => this.handleModalClose()}
        >
          <MySnackbarContentWrapper
            onClose={() => this.handleModalClose()}
            variant={this.state.notiType}
            message={this.state.notiMessage}
          />
        </Snackbar>
      </main>
    );
  }
}

export default Userlist;
