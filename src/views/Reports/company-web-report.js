import React, { Component } from "react";
import Navbar from "../Component/Navbar";
import ProgressBar from 'react-bootstrap/ProgressBar';
import Moment from 'react-moment';
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button, Row, Col } from "react-bootstrap"
import ToggleButton from 'react-bootstrap/ToggleButton'
import _ from 'lodash';
import { CSVLink, CSVDownload } from "react-csv";
import ReactTooltip from 'react-tooltip'; // For tool tip 

import { checkAuthentication } from '../Component/Authentication';

var path = cred.API_PATH;
const monthList = [{ "key": 1, "name": 'January' }, { "key": 2, "name": 'February' }, { "key": 3, "name": 'March' }, { "key": 4, "name": 'April' }, { "key": 5, "name": 'May' },
{ "key": 6, "name": 'June' }, { "key": 7, "name": 'July' }, { "key": 8, "name": 'August' }, { "key": 9, "name": 'September' }, { "key": 10, "name": 'October' },
{ "key": 11, "name": 'November' }, { "key": 12, "name": 'December' }];

export class CompanyWebReport extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,

            userId: '',
            companyId: '',

            openSideMenu: false,
            searchString: '',
            searchYear: (new Date()).getFullYear(),
            searchMonth: (new Date()).getMonth() + 1,
            searching: false,

            companyLists: [],
            duration: 0,
            cost: 0,
            price: 0,

            editUser: false,
            confirm: false,

            companyName: '',
            selectedCompanyType: '',

            thisYear: (new Date()).getFullYear(),
            minOffset: 0,
            maxOffset: 60,
            thisMonth: (new Date()).getMonth() + 1,

            fromDt: '',
            toDT: '',

            perPage: 10,
            pageNo: 1,
            totalRows: '',
            totalRecords: '',

        }

    }

    componentDidMount() {
        document.body.classList.remove("sign-bg");
        document.body.classList.add("grey-bg");

        let storage = localStorage.getItem("MIO_Local");
        storage = storage ? JSON.parse(storage) : null;
        if (storage && storage.data.userId) {
            this.setState({
                userId: storage.data.userId,
                userType: storage.data.userType,
                companyId: storage.data.companyId,
                companyType: storage.data.companyType
            }, () => {

                // this.getTransactionListForAdmin();
            });

            // console.log(this.parseJwt(storage.token));
            let tokenData = this.parseJwt(storage.token);

        }

    }

    searchCompany = (event) => {
        if (event) {
            event.preventDefault();
        }
        this.fetchDetails("+");
    }

    fetchDetails = (status) => {

        this.setState({
            searching: true,
            loading: true
        });

        let { perPage, pageNo, companyId, fromDt, toDT } = this.state;

        let dataToSend = {
            "fromDate": fromDt,
            "toDate": toDT,
            "companyId": companyId,
            "page": pageNo,
            "perPage": perPage
        };

        axios
            .post(path + 'price-management/fetch-webcall-related-details-for-date-range', dataToSend)
            .then(serverResponse => {
                let res = serverResponse.data;
                if (!res.isError) {

                    let webCallList = res.details && res.details.webCallCostDetails && res.details.webCallCostDetails.webCallList;
                    let { duration, cost, price } = this.state;
                    if (status && status == "+") {
                        duration = parseFloat(duration) + parseFloat(res.details && res.details.webCallCostDetails && res.details.webCallCostDetails.totalWebCalDuration);
                        cost = parseFloat(cost) + parseFloat(res.details && res.details.webCallCostDetails && res.details.webCallCostDetails.totalWebCalCost);
                        price = parseFloat(price) + parseFloat(res.details && res.details.webCallCostDetails && res.details.webCallCostDetails.totalWebCalPrice);

                    } else if(status && status == "-") {
                        duration = parseFloat(duration) - parseFloat(res.details && res.details.webCallCostDetails && res.details.webCallCostDetails.totalWebCalDuration);
                        cost = parseFloat(cost) - parseFloat(res.details && res.details.webCallCostDetails && res.details.webCallCostDetails.totalWebCalCost);
                        price = parseFloat(price) - parseFloat(res.details && res.details.webCallCostDetails && res.details.webCallCostDetails.totalWebCalPrice);

                    }

                    this.setState({
                        transactionLists: webCallList,
                        totalRows: res.details && res.details.noofpage,
                        totalRecords: res.details && res.details.total,
                        loading: false,
                        duration: duration,
                        cost: cost, 
                        price: price 
                    });
                } else {
                    this.setState({
                        loading: false
                    });
                }
            })
            .catch(error => {
                console.log(error);
            })

    }

    clearSearch = (event) => {
        event.preventDefault();
        this.setState({
            searchString: '',
            fromDt: '',
            toDT: '',
            pageNo: 1,
            transactionLists: [],
            totalRecords: '',
            totalRows: '',
            searching: false,
            loading: false,
            duration: 0,
            cost: 0,
            price: 0,
        }, () => {
            this.totalDuration = 0;
            this.totalCost = 0;
            this.totalPrice = 0;
        });
    }

    clearState = () => {
        this.setState({
            loading: false,

        })

    }

    getCompanyName = (companyId) => {

        let { companyLists } = this.state;
        let companyNameStatus = _.filter(companyLists, { '_id': companyId })[0];
        return companyNameStatus && companyNameStatus.companyName;
        // console.log("getting company name status ===", companyNameStatus);

    }

    pageChangePrev = () => {

        this.setState({
            pageNo: (parseFloat(this.state.pageNo) - 1)
        }, () => {
            this.fetchDetails("-")
        });

    }

    pageChangeNext = () => {

        this.setState({
            pageNo: (parseFloat(this.state.pageNo) + 1)
        }, () => {
            this.fetchDetails("+")
        });

    }

    backMenueToggle = () => {
        this.props.history.goBack();
    }

    sideMenuToggle() {
        let { openSideMenu } = this.state;
        openSideMenu = !openSideMenu;
        this.setState({ openSideMenu });
    }

    mobMenuClick() {
        this.sideMenuToggle();
        console.log('mobMenuClick');
    }

    parseJwt = (token) => {
        if (!token) { return; }
        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace('-', '+').replace('_', '/');
        return JSON.parse(window.atob(base64));
    }


    render() {

        let { loading, companyType, transactionLists, companyLists, minOffset, maxOffset, thisYear, thisMonth, fromDt, toDT, pageNo, perPage, totalRows } = this.state;
        var options = [];
        for (let i = minOffset; i <= maxOffset; i++) {
            let year = thisYear - i;
            options.push(<option value={year} key={i}>{year}</option>);
        }
        // console.log("This year in state ===", thisYear);
        return (
            <main>

                <section className="user-mngnt-wrap">
                    <div className="container-fluid">
                        <div className="row">
                            <Navbar routTo="/companyreport" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />
                            <div className="col-xl-10 col-lg-9 p-0">
                                <div className="mobile-menu-header">
                                    <a href="JavaScript:Void(0);" className="back-arw" onClick={() => this.backMenueToggle()}><i className="fas fa-arrow-left"></i></a>

                                </div>
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="inv-code">
                                            <span style={{ color: '#007bff' }}>Web call Report</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="mng-full-list">
                                    <div className="mng-full-srch">
                                        <div className="row">

                                            <div className="col-md-6">
                                                <div class="form-group">
                                                    <label>From Date</label>
                                                    <input
                                                        type="date"
                                                        className="form-control"
                                                        name="fromDt" value={fromDt}
                                                        onChange={(event) => { this.setState({ fromDt: event.target.value }) }}
                                                    />
                                                </div>
                                            </div>

                                            <div className="col col-md-6">
                                                <label>To Date</label>
                                                <div className="srch-wrap">
                                                    <form>
                                                        <div className="row">
                                                            <div className="col-md-10">
                                                                <input
                                                                    type="date"
                                                                    className="form-control"
                                                                    name="fromDt" value={toDT}
                                                                    onChange={(event) => { this.setState({ toDT: event.target.value }) }}
                                                                />
                                                            </div>
                                                            <div className="col-md-2">
                                                                {this.state.searching ?
                                                                    <input type="button" onClick={(event) => this.clearSearch(event)}
                                                                        data-tip='Clear'
                                                                    />
                                                                    :
                                                                    <input type="submit" onClick={(event) => this.searchCompany(event)}
                                                                        data-tip='Search'
                                                                    />
                                                                }
                                                            </div>
                                                        </div>

                                                        <ReactTooltip
                                                            effect="float"
                                                            place="top"
                                                            data-border="true"
                                                        />
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    {transactionLists && transactionLists.length > 0 && companyType == cred.COMPANY_TYPE_STARTUP ?
                                        <div className="mng-full-table company-mangmnt">

                                            <div className="row">

                                                <div className="col-md-12">
                                                    <div className="mng-full-table-hdr">
                                                        <div className="row">
                                                            <div className="col-md-3 border-rt">
                                                                <h6>Conference</h6>
                                                            </div>
                                                            <div className="col-md-3 border-rt">
                                                                <h6>Date</h6>
                                                            </div>
                                                            <div className="col-md-3 border-rt">
                                                                <h6>Duration</h6>
                                                            </div>
                                                            <div className="col-md-3">
                                                                <h6>price</h6>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            {transactionLists && transactionLists.length > 0 ? transactionLists.map((data, index) => {

                                                return <div className="row" key={index}>
                                                    <div className="col-md-12">

                                                        <div className="mng-full-table-row">
                                                            <div className="row">
                                                                <div className="col-md-3 border-rt">
                                                                    <h6>Conference</h6>
                                                                    <p className="textEllips">
                                                                        {data.conferenceTitle}
                                                                    </p>
                                                                </div>
                                                                <div className="col-md-3 border-rt">
                                                                    <h6>Date</h6>
                                                                    <p>
                                                                        <Moment format="DD/MM/YYYY">
                                                                            {data.date}
                                                                        </Moment>
                                                                    </p>
                                                                </div>
                                                                <div className="col-md-3 border-rt">
                                                                    <h6>Duration</h6>
                                                                    <p>{parseFloat(data.duration).toFixed('2') + 'Mins'}</p>
                                                                </div>
                                                                <div className="col-md-3">
                                                                    <h6>Price</h6>
                                                                    <p>{parseFloat(data.totalPrice).toFixed('2') + ' ' + data.currency && data.currency}</p>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                            })
                                                : null}

                                            {!loading && parseFloat(this.state.totalRecords) <= parseFloat(parseFloat(pageNo) * parseFloat(perPage)) ?
                                                <div className="row">
                                                    <div className="col-md-12">

                                                        <div className="mng-full-table-row">
                                                            <div className="row">
                                                                <div className="col-md-3 border-rt">
                                                                    <h6>Conference</h6>
                                                                    <p className="textEllips">Total</p>
                                                                </div>
                                                                <div className="col-md-3 border-rt">
                                                                    <h6>Date</h6>
                                                                    <p>-</p>
                                                                </div>
                                                                <div className="col-md-3 border-rt">
                                                                    <h6>Duration</h6>
                                                                    <p>{this.state.duration && parseFloat(this.state.duration).toFixed('2') + 'Mins'}</p>
                                                                </div>
                                                                <div className="col-md-3">
                                                                    <h6>Price</h6>
                                                                    <p>{this.state.price && parseFloat(this.state.price).toFixed('2')}</p>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                : null}

                                        </div>
                                        : transactionLists && transactionLists.length > 0 && (companyType == cred.COMPANY_TYPE_ENTERPRISE || companyType == cred.COMPANY_TYPE_FREE_TYPE) ?
                                            <div className="mng-full-table company-mangmnt">

                                                <div className="row">

                                                    <div className="col-md-12">
                                                        <div className="mng-full-table-hdr">
                                                            <div className="row">
                                                                <div className="col-md-4 border-rt">
                                                                    <h6>Conference</h6>
                                                                </div>
                                                                <div className="col-md-4 border-rt">
                                                                    <h6>Date</h6>
                                                                </div>
                                                                <div className="col-md-4">
                                                                    <h6>Duration</h6>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                {transactionLists && transactionLists.length > 0 ? transactionLists.map((data, index) => {

                                                    return <div className="row" key={index}>
                                                        <div className="col-md-12">

                                                            <div className="mng-full-table-row">
                                                                <div className="row">
                                                                    <div className="col-md-4 border-rt">
                                                                        <h6>Conference</h6>
                                                                        <p className="textEllips">
                                                                            {data.conferenceTitle && data.conferenceTitle}
                                                                        </p>
                                                                    </div>
                                                                    <div className="col-md-4 border-rt">
                                                                        <h6>Date</h6>
                                                                        <p>
                                                                            <Moment format="DD/MM/YYYY">
                                                                                {data.date}
                                                                            </Moment>
                                                                        </p>
                                                                    </div>
                                                                    <div className="col-md-4">
                                                                        <h6>Duration</h6>
                                                                        <p>{data.duration && parseFloat(data.duration).toFixed('2') + 'Mins'}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>

                                                })
                                                    : null}

                                                {!loading && parseFloat(this.state.totalRecords) <= parseFloat(parseFloat(pageNo) * parseFloat(perPage)) ?
                                                    <div className="row">
                                                        <div className="col-md-12">

                                                            <div className="mng-full-table-row">
                                                                <div className="row">
                                                                    <div className="col-md-4 border-rt">
                                                                        <h6>Conference</h6>
                                                                        <p className="textEllips">Total</p>
                                                                    </div>
                                                                    <div className="col-md-4 border-rt">
                                                                        <h6>Date</h6>
                                                                        <p>-</p>
                                                                    </div>
                                                                    <div className="col-md-4">
                                                                        <h6>Duration</h6>
                                                                        <p>{this.state.duration && parseFloat(this.state.duration).toFixed('2') + 'Mins'}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    : null}

                                            </div>
                                            :
                                            <div className="mng-full-table">
                                                <div className="row">
                                                    <div className="col-md-12">
                                                        <div className="mng-full-table-hdr admn-usr-hdr">
                                                            <div className="row">
                                                                <div className="col-md-12">
                                                                    <h6>No Details Found.</h6>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    }

                                    <br /><br />
                                    <div className="form-group">
                                        {!loading && parseFloat(pageNo) > 1 ?
                                            <span style={{ color: "#007bff", cursor: "pointer" }}
                                                onClick={() => { this.pageChangePrev() }}
                                            >
                                                <b>{"<< Prev"}</b>
                                            </span>
                                            :
                                            null}
                                        &nbsp;&nbsp;|&nbsp;&nbsp;
                                        {!loading && (parseFloat(pageNo) < parseFloat(totalRows)) ?
                                            <span style={{ color: "#007bff", cursor: "pointer" }}
                                                onClick={() => { this.pageChangeNext() }}
                                            >
                                                <b>{"Next >>"}</b>
                                            </span>
                                            :
                                            null}
                                        {loading ?
                                            <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                                                <i className="fa fa-spinner fa-spin" ></i>
                                            </a>
                                            :
                                            null}

                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </section>

            </main>
        );
    }
}

export default checkAuthentication(CompanyWebReport);
