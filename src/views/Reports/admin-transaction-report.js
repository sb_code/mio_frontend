import React, { Component } from "react";
import Navbar from "../Component/Navbar";
import ProgressBar from 'react-bootstrap/ProgressBar';
import Moment from 'react-moment';
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button, Row, Col } from "react-bootstrap"
import ToggleButton from 'react-bootstrap/ToggleButton'
import _ from 'lodash';
import { CSVLink, CSVDownload } from "react-csv";
import ReactTooltip from 'react-tooltip'; // For tool tip 

import { checkAuthentication } from '../Component/Authentication';

var path = cred.API_PATH;
const monthList = [{ "key": 1, "name": 'January' }, { "key": 2, "name": 'February' }, { "key": 3, "name": 'March' }, { "key": 4, "name": 'April' }, { "key": 5, "name": 'May' },
{ "key": 6, "name": 'June' }, { "key": 7, "name": 'July' }, { "key": 8, "name": 'August' }, { "key": 9, "name": 'September' }, { "key": 10, "name": 'October' },
{ "key": 11, "name": 'November' }, { "key": 12, "name": 'December' }];

export class AdminTransactionReport extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,

            userId: '',
            selectedCompanyId: '',
            selectedCompanyName: '',

            openSideMenu: false,
            searchString: '',
            searchYear: (new Date()).getFullYear(),
            searchMonth: (new Date()).getMonth() + 1,
            searching: false,

            companyLists: [],

            editUser: false,
            confirm: false,

            companyName: '',
            selectedCompanyType: '',

            thisYear: (new Date()).getFullYear(),
            minOffset: 0,
            maxOffset: 60,
            thisMonth: (new Date()).getMonth() + 1,

            fromDt: '',
            toDT: '',

            perPage: 10,
            pageNo: 1,
            totalRows: '',

        }
    }

    componentDidMount() {
        document.body.classList.remove("sign-bg");
        document.body.classList.add("grey-bg");

        let storage = localStorage.getItem("MIO_Local");
        storage = storage ? JSON.parse(storage) : null;

        let selectedCompanyId = this.props.match.params.id;
        let selectedCompanyName = this.props.location.state.companyName;

        if (storage && storage.data.userId) {
            this.setState({
                userId: storage.data.userId,
                userType: storage.data.userType,
                companyId: storage.data.companyId,
                selectedCompanyId: selectedCompanyId,
                selectedCompanyName: selectedCompanyName
            }, () => {

                // this.loadData();
                // this.getCompanyListForAdmin();

            });

            // console.log(this.parseJwt(storage.token));
            let tokenData = this.parseJwt(storage.token);

        }

    }


    loadData = async () => {

        let get_companydetails = await this.getCompanyListForAdmin();
        let get_transactionDetails = await this.getTransactionListForAdmin();

    }

    getCompanyListForAdmin = () => {
        return new Promise((resolve, reject) => {
            axios
                .post(path + 'user/get-companies')
                .then(serverResponse => {
                    let res = serverResponse.data;
                    if (!res.isError) {
                        this.setState({
                            companyLists: res.details
                        }, () => {
                            resolve();
                        });
                    } else {
                        reject();
                    }
                })
                .catch(error => {
                    console.log(error);
                })
        })

    }

    // getTransactionListForAdmin = () => {

    //     return new Promise((resolve, reject) => {
    //         let { searchYear, searchMonth, searchString } = this.state;

    //         let dataToSend = {
    //             "year": searchYear,
    //             "month": searchMonth,
    //             "companyId": searchString,
    //             "page": 1,
    //             "perPage": 10
    //         };

    //         axios
    //             .post(path + 'payment/get-transaction-details', dataToSend)
    //             .then(serverResponse => {
    //                 let res = serverResponse.data;
    //                 if (res.isError) {
    //                     let details = res.details && res.details[0];
    //                     console.log("transaction details ==", details);

    //                     this.setState({
    //                         transactionLists: details.results
    //                     }, () => {
    //                         // console.log("transaction details after setState =", this.state.transactionLists);
    //                         resolve();
    //                     });
    //                 } else {
    //                     reject();
    //                 }
    //             })
    //             .catch(error => {
    //                 console.log(error);
    //             })
    //     })

    // }

    searchCompany = (event) => {
        if (event) {
            event.preventDefault();
        }

        this.setState({
            searching: true,
            loading: true
        });

        let { searchYear, searchMonth, searchString, selectedCompanyId, pageNo, perPage } = this.state;

        let dataToSend = {
            "year": searchYear,
            "month": searchMonth,
            "companyId": selectedCompanyId,
            "page": pageNo,
            "perPage": perPage
        };

        axios
            .post(path + 'payment/get-transaction-details', dataToSend)
            .then(serverResponse => {
                let res = serverResponse.data;
                if (res.isError) {
                    let details = res.details && res.details[0];
                    // console.log("transaction details ==", details);
                    this.setState({
                        transactionLists: details.results,
                        totalRows: details.total,
                        loading: false
                    });
                } else {
                    this.setState({
                        loading: false
                    });
                }
            })
            .catch(error => {
                console.log(error);
            })


    }

    clearSearch = (event) => {
        event.preventDefault();
        this.setState({
            searchString: '',
            searchMonth: this.state.thisMonth,
            searchYear: this.state.thisYear,
            searching: false,
            pageNo: 1
        }, () => {
            this.getCompanyListForAdmin();
            // this.getTransactionListForAdmin();
        });
    }

    clearState = () => {
        this.setState({
            loading: false,

        })

    }

    getCompanyName = (companyId) => {

        let { companyLists } = this.state;
        let companyNameStatus = _.filter(companyLists, { '_id': companyId })[0];
        return companyNameStatus && companyNameStatus.companyName;
        // console.log("getting company name status ===", companyNameStatus);

    }

    pageChangePrev = () => {

        this.setState({
            pageNo: (parseFloat(this.state.pageNo) - 1)
        }, () => {
            this.searchCompany()
        });

    }

    pageChangeNext = () => {

        this.setState({
            pageNo: (parseFloat(this.state.pageNo) + 1)
        }, () => {
            this.searchCompany()
        });

    }

    backMenueToggle = () => {
        this.props.history.goBack();
    }

    sideMenuToggle() {
        let { openSideMenu } = this.state;
        openSideMenu = !openSideMenu;
        this.setState({ openSideMenu });
    }

    mobMenuClick() {
        this.sideMenuToggle();
        console.log('mobMenuClick');
    }

    parseJwt = (token) => {
        if (!token) { return; }
        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace('-', '+').replace('_', '/');
        return JSON.parse(window.atob(base64));
    }


    render() {

        let { loading, transactionLists, companyLists, minOffset, maxOffset, thisYear, thisMonth, searchMonth, searchYear, pageNo, perPage, totalRows } = this.state;
        var options = [];
        for (let i = minOffset; i <= maxOffset; i++) {
            let year = thisYear - i;
            options.push(<option value={year} key={i}>{year}</option>);
        }
        // console.log("This year in state ===", thisYear);
        return (
            <main>

                <section className="user-mngnt-wrap">
                    <div className="container-fluid">
                        <div className="row">
                            <Navbar routTo="/adminreport" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />
                            <div className="col-xl-10 col-lg-9 p-0">
                                <div className="mobile-menu-header">
                                    <a href="JavaScript:Void(0);" className="back-arw" onClick={() => this.backMenueToggle()}><i className="fas fa-arrow-left"></i></a>

                                </div>
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="inv-code">
                                            <span style={{ color: '#007bff' }}>Transaction Report For {this.state.selectedCompanyName}</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="mng-full-list">
                                    <div className="mng-full-srch">
                                        <div className="row">
                                            <div className="col-md-6">
                                                <label>Month</label>
                                                <select className="form-control"
                                                    name="searchType"
                                                    onChange={(event) => { this.setState({ searchMonth: event.target.value }) }}
                                                >
                                                    <option value="">Select Month</option>
                                                    {monthList && monthList.map((data, index) => {
                                                        return <option value={data.key}
                                                            selected={data.key == searchMonth ? true : false}
                                                        >
                                                            {data.name}
                                                        </option>
                                                    })}
                                                </select>
                                            </div>
                                            
                                            <div className="col col-md-6">
                                                <label>Year</label>
                                                <div className="srch-wrap">
                                                    <form>

                                                        <select class="form-control"
                                                            name="searchString"
                                                            vlaue={thisYear}
                                                            onChange={(event) => { this.setState({ searchYear: event.target.value }) }}
                                                        >
                                                            {options}
                                                        </select>
                                                        
                                                        {this.state.searching ?
                                                            <input type="button" onClick={(event) => this.clearSearch(event)}
                                                                data-tip='Clear'
                                                            />
                                                            :
                                                            <input type="submit" onClick={(event) => this.searchCompany(event)}
                                                                data-tip='Search'
                                                            />
                                                        }

                                                        <ReactTooltip
                                                            effect="float"
                                                            place="top"
                                                            data-border="true"
                                                        />
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {transactionLists && transactionLists.length > 0 ?
                                        <div className="mng-full-table company-mangmnt">

                                            <div className="row">

                                                <div className="col-md-12">
                                                    <div className="mng-full-table-hdr">
                                                        <div className="row">
                                                            <div className="col-md-3 border-rt">
                                                                <h6>Company</h6>
                                                            </div>
                                                            <div className="col-md-3 border-rt">
                                                                <h6>Date</h6>
                                                            </div>
                                                            <div className="col-md-3 border-rt">
                                                                <h6>Amount</h6>
                                                            </div>
                                                            <div className="col-md-3">
                                                                <h6>Currency</h6>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            {transactionLists && transactionLists.length > 0 ? transactionLists.map((data, index) => {

                                                return <div className="row" key={index}>
                                                    <div className="col-md-12">

                                                        <div className="mng-full-table-row">
                                                            <div className="row">
                                                                <div className="col-md-3 border-rt">
                                                                    <h6>Company</h6>
                                                                    <p className="textEllips">
                                                                        <a href="JavaScript:Void(0);" >
                                                                            {this.getCompanyName(data.companyId)}
                                                                        </a>
                                                                    </p>
                                                                </div>
                                                                <div className="col-md-3 border-rt">
                                                                    <h6>Date</h6>
                                                                    <p>
                                                                        <Moment format="DD/MM/YYYY">
                                                                            {data.createdAt}
                                                                        </Moment>
                                                                    </p>
                                                                </div>
                                                                <div className="col-md-3 border-rt">
                                                                    <h6>Amount</h6>
                                                                    <p>{parseFloat(data.amount) / 100}</p>
                                                                </div>
                                                                <div className="col-md-3">
                                                                    <h6>Currency</h6>
                                                                    <p>{data.currency}</p>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                            })
                                                : null}

                                        </div>
                                        :
                                        <div className="mng-full-table">
                                            <div className="row">
                                                <div className="col-md-12">
                                                    <div className="mng-full-table-hdr admn-usr-hdr">
                                                        <div className="row">
                                                            <div className="col-md-12">
                                                                <h6>No Details Found.</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    }

                                    <br /><br />
                                    <div className="form-group">
                                        {!loading && parseFloat(pageNo) > 1 ?
                                            <span style={{ color: "#007bff", cursor: "pointer" }}
                                                onClick={() => { this.pageChangePrev() }}
                                            >
                                                <b>{"<< Prev"}</b>
                                            </span>
                                            :
                                            null}
                                        &nbsp;&nbsp;|&nbsp;&nbsp;
                                        {!loading && ((parseFloat(pageNo) * parseFloat(perPage)) < parseFloat(totalRows)) ?
                                            <span style={{ color: "#007bff", cursor: "pointer" }}
                                                onClick={() => { this.pageChangeNext() }}
                                            >
                                                <b>{"Next >>"}</b>
                                            </span>
                                            :
                                            null}
                                        {loading ?
                                            <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                                                <i className="fa fa-spinner fa-spin" ></i>
                                            </a>
                                            :
                                            null}

                                    </div>


                                </div>

                            </div>
                        </div>
                    </div>
                </section>

            </main>
        );
    }
}

export default checkAuthentication(AdminTransactionReport);
