import React, { Component } from "react";
import Navbar from "../Component/Navbar";
import ProgressBar from 'react-bootstrap/ProgressBar';
import Moment from 'react-moment';
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button, Row, Col } from "react-bootstrap"
import ToggleButton from 'react-bootstrap/ToggleButton'
import _ from 'lodash';
import { CSVLink, CSVDownload } from "react-csv";
import ReactTooltip from 'react-tooltip'; // For tool tip 

import { checkAuthentication } from '../Component/Authentication';

var path = cred.API_PATH;
const monthList = [{ "key": 1, "name": 'January' }, { "key": 2, "name": 'February' }, { "key": 3, "name": 'March' }, { "key": 4, "name": 'April' }, { "key": 5, "name": 'May' },
{ "key": 6, "name": 'June' }, { "key": 7, "name": 'July' }, { "key": 8, "name": 'August' }, { "key": 9, "name": 'September' }, { "key": 10, "name": 'October' },
{ "key": 11, "name": 'November' }, { "key": 12, "name": 'December' }];

export class AdminMonthlyReport extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,

            userId: '',
            selectedCompanyId: '',
            selectedCompanyName: '',

            openSideMenu: false,
            searchString: '',
            searchYear: (new Date()).getFullYear(),
            searchMonth: (new Date()).getMonth() + 1,
            searching: false,

            transactionLists: '',
            archive: '',
            sipInBoundCall: '',
            sipcall: '',
            webCall: '',
            totalDuration: '',
            totalCost: '',
            totalPrice: '',

            editUser: false,
            confirm: false,

            companyName: '',
            selectedCompanyType: '',

            thisYear: (new Date()).getFullYear(),
            minOffset: 0,
            maxOffset: 60,
            thisMonth: (new Date()).getMonth() + 1,

            fromDt: '',
            toDT: '',

            perPage: 10,
            pageNo: 1,
            totalRows: '',

        }
    }

    componentDidMount() {
        document.body.classList.remove("sign-bg");
        document.body.classList.add("grey-bg");

        let storage = localStorage.getItem("MIO_Local");
        storage = storage ? JSON.parse(storage) : null;

        let selectedCompanyId = this.props.match.params.id;
        let selectedCompanyName = this.props.location.state.companyName;

        if (storage && storage.data.userId) {
            this.setState({
                userId: storage.data.userId,
                userType: storage.data.userType,
                companyId: storage.data.companyId,
                selectedCompanyId: selectedCompanyId,
                selectedCompanyName: selectedCompanyName
            }, () => {
                // this.loadData();
                // this.getCompanyListForAdmin();
            });

            // console.log(this.parseJwt(storage.token));
            let tokenData = this.parseJwt(storage.token);

        }

    }


    searchCompany = (event) => {
        if (event) {
            event.preventDefault();
        }

        this.setState({
            searching: true,
            loading: true
        });

        let { searchYear, searchMonth, selectedCompanyId, pageNo, perPage } = this.state;

        let dataToSend = {
            "year": searchYear,
            "month": searchMonth,
            // "companyId": "5e5fc83922feea7a2f232c77"
            "companyId": selectedCompanyId
        };

        axios
            .post(path + 'price-management/fetch-overall-cost-for-particular-company', dataToSend)
            .then(serverResponse => {
                let res = serverResponse.data;
                if (!res.isError) {
                    let details = res.details;

                    let archive = details && details.archive;
                    let sipInBoundCall = details && details.sipInBoundCall;
                    let sipcall = details && details.sipcall;
                    let webCall = details && details.webCall;

                    let { totalDuration, totalCost, totalPrice } = {};
                    if (archive && sipInBoundCall && sipcall && webCall) {

                        totalDuration = parseFloat(archive.archiveOverAllDuration) + parseFloat(sipInBoundCall.sipInBoundOverAllDuration) +
                            parseFloat(sipcall.sipCallOverAllDuration) + parseFloat(webCall.webCallOverAllDuration);

                        totalCost = parseFloat(archive.archiveOverAllCost) + parseFloat(sipInBoundCall.sipInBoundOverAllCost) +
                            parseFloat(sipcall.sipCallOverAllCost) + parseFloat(webCall.webCallOverAllCost);

                        totalPrice = parseFloat(archive.archiveOverAllPrice) + parseFloat(sipInBoundCall.sipInBoundOverAllPrice) +
                            parseFloat(sipcall.sipCallOverAllPrice) + parseFloat(webCall.webCallOverAllPrice);

                    }

                    this.setState({
                        transactionLists: details,
                        archive: archive,
                        sipInBoundCall: sipInBoundCall,
                        sipcall: sipcall,
                        webCall: webCall,
                        loading: false,
                        totalDuration: totalDuration,
                        totalCost: totalCost,
                        totalPrice: totalPrice
                    });
                } else {
                    this.setState({
                        loading: false
                    });
                }
            })
            .catch(error => {
                console.log(error);
            })


    }

    clearSearch = (event) => {
        event.preventDefault();
        this.setState({
            searchMonth: this.state.thisMonth,
            searchYear: this.state.thisYear,
            searching: false,
            transactionLists: '',
            archive: '',
            sipInBoundCall: '',
            sipcall: '',
            webCall: '',
            loading: false
        });
    }

    clearState = () => {
        this.setState({
            loading: false,

        })

    }

    backMenueToggle = () => {
        this.props.history.goBack();
    }

    sideMenuToggle() {
        let { openSideMenu } = this.state;
        openSideMenu = !openSideMenu;
        this.setState({ openSideMenu });
    }

    mobMenuClick() {
        this.sideMenuToggle();
        console.log('mobMenuClick');
    }

    parseJwt = (token) => {
        if (!token) { return; }
        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace('-', '+').replace('_', '/');
        return JSON.parse(window.atob(base64));
    }


    render() {

        let { loading, transactionLists, companyLists, minOffset, maxOffset, thisYear, thisMonth, searchYear, searchMonth,
            archive,
            sipInBoundCall,
            sipcall,
            webCall } = this.state;
        var options = [];
        for (let i = minOffset; i <= maxOffset; i++) {
            let year = thisYear - i;
            options.push(<option value={year} selected={year == this.state.searchYear ? true : false} key={i}>{year}</option>);
        }
        // console.log("This year in state ===", thisYear);
        return (
            <main>

                <section className="user-mngnt-wrap">
                    <div className="container-fluid">
                        <div className="row">
                            <Navbar routTo="/adminreport" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />
                            <div className="col-xl-10 col-lg-9 p-0">
                                <div className="mobile-menu-header">
                                    <a href="JavaScript:Void(0);" className="back-arw" onClick={() => this.backMenueToggle()}><i className="fas fa-arrow-left"></i></a>

                                </div>
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="inv-code">
                                            <span style={{ color: '#007bff' }}>Monthly Report For {this.state.selectedCompanyName}</span>
                                        </div>
                                    </div>
                                </div>

                                <div className="mng-full-list">
                                    <div className="mng-full-srch">
                                        <div className="row">
                                            <div className="col-md-6">
                                                <label>Month</label>
                                                <select className="form-control"
                                                    name="searchType"
                                                    onChange={(event) => { this.setState({ searchMonth: event.target.value }) }}
                                                >
                                                    <option value="">Select Month</option>
                                                    {monthList && monthList.map((data, index) => {
                                                        return <option value={data.key}
                                                            selected={data.key == searchMonth ? true : false}
                                                        >
                                                            {data.name}
                                                        </option>
                                                    })}
                                                </select>
                                            </div>

                                            <div className="col col-md-6">
                                                <label>Year</label>
                                                <div className="srch-wrap">
                                                    <form>

                                                        <select class="form-control"
                                                            name="searchString"
                                                            vlaue={thisYear}
                                                            onChange={(event) => { this.setState({ searchYear: event.target.value }) }}
                                                        >
                                                            {options}
                                                        </select>

                                                        {this.state.searching ?
                                                            <input type="button" onClick={(event) => this.clearSearch(event)}
                                                                data-tip='Clear'
                                                            />
                                                            :
                                                            <input type="submit" onClick={(event) => this.searchCompany(event)}
                                                                data-tip='Search'
                                                            />
                                                        }

                                                        <ReactTooltip
                                                            effect="float"
                                                            place="top"
                                                            data-border="true"
                                                        />
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {transactionLists ?
                                        <div className="mng-full-table company-mangmnt">

                                            <div className="row">

                                                <div className="col-md-12">
                                                    <div className="mng-full-table-hdr">
                                                        <div className="row">
                                                            <div className="col-md-3 border-rt">
                                                                <h6>Name</h6>
                                                            </div>
                                                            <div className="col-md-3 border-rt">
                                                                <h6>No of Calls</h6>
                                                            </div>
                                                            <div className="col-md-2 border-rt">
                                                                <h6>Duration</h6>
                                                            </div>
                                                            <div className="col-md-2 border-rt">
                                                                <h6>Cost</h6>
                                                            </div>
                                                            <div className="col-md-2">
                                                                <h6>Price</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            {sipInBoundCall ?
                                                <div className="row">
                                                    <div className="col-md-12">

                                                        <div className="mng-full-table-row">
                                                            <div className="row">
                                                                <div className="col-md-3 border-rt">
                                                                    <h6>Name</h6>
                                                                    <p className="textEllips">
                                                                        <a href="JavaScript:Void(0);" >
                                                                            SIP Inbound Call
                                                                        </a>
                                                                    </p>
                                                                </div>
                                                                <div className="col-md-3 border-rt">
                                                                    <h6>No of Calls</h6>
                                                                    <p>
                                                                        {sipInBoundCall.totalSipInBound}
                                                                    </p>
                                                                </div>
                                                                <div className="col-md-2 border-rt">
                                                                    <h6>Duration</h6>
                                                                    <p>{parseFloat(sipInBoundCall.sipInBoundOverAllDuration).toFixed('2')}</p>
                                                                </div>
                                                                <div className="col-md-2 border-rt">
                                                                    <h6>Cost</h6>
                                                                    <p>{parseFloat(sipInBoundCall.sipInBoundOverAllCost).toFixed('2')}</p>
                                                                </div>
                                                                <div className="col-md-2">
                                                                    <h6>Price</h6>
                                                                    <p>{parseFloat(sipInBoundCall.sipInBoundOverAllPrice).toFixed('2')}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                :
                                                null}

                                            {sipcall ?
                                                <div className="row">
                                                    <div className="col-md-12">

                                                        <div className="mng-full-table-row">
                                                            <div className="row">
                                                                <div className="col-md-3 border-rt">
                                                                    <h6>Name</h6>
                                                                    <p className="textEllips">
                                                                        <a href="JavaScript:Void(0);" >
                                                                            SIP Outbound Call
                                                                    </a>
                                                                    </p>
                                                                </div>
                                                                <div className="col-md-3 border-rt">
                                                                    <h6>No of Calls</h6>
                                                                    <p>
                                                                        {sipcall.totalSipCall}
                                                                    </p>
                                                                </div>
                                                                <div className="col-md-2 border-rt">
                                                                    <h6>Duration</h6>
                                                                    <p>{parseFloat(sipcall.sipCallOverAllDuration).toFixed('2')}</p>
                                                                </div>
                                                                <div className="col-md-2 border-rt">
                                                                    <h6>Cost</h6>
                                                                    <p>{parseFloat(sipcall.sipCallOverAllCost).toFixed('2')}</p>
                                                                </div>
                                                                <div className="col-md-2">
                                                                    <h6>Price</h6>
                                                                    <p>{parseFloat(sipcall.sipCallOverAllPrice).toFixed('2')}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                :
                                                null}

                                            {webCall ?
                                                <div className="row">
                                                    <div className="col-md-12">

                                                        <div className="mng-full-table-row">
                                                            <div className="row">
                                                                <div className="col-md-3 border-rt">
                                                                    <h6>Name</h6>
                                                                    <p className="textEllips">
                                                                        <a href="JavaScript:Void(0);" >
                                                                            Web Calls
                                                                        </a>
                                                                    </p>
                                                                </div>
                                                                <div className="col-md-3 border-rt">
                                                                    <h6>No of Calls</h6>
                                                                    <p>
                                                                        {webCall.totalWebCall}
                                                                    </p>
                                                                </div>
                                                                <div className="col-md-2 border-rt">
                                                                    <h6>Duration</h6>
                                                                    <p>{parseFloat(webCall.webCallOverAllDuration).toFixed('2')}</p>
                                                                </div>
                                                                <div className="col-md-2 border-rt">
                                                                    <h6>Cost</h6>
                                                                    <p>{parseFloat(webCall.webCallOverAllCost).toFixed('2')}</p>
                                                                </div>
                                                                <div className="col-md-2">
                                                                    <h6>Price</h6>
                                                                    <p>{parseFloat(webCall.webCallOverAllPrice).toFixed('2')}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                :
                                                null}

                                            {archive ?
                                                <div className="row">
                                                    <div className="col-md-12">

                                                        <div className="mng-full-table-row">
                                                            <div className="row">
                                                                <div className="col-md-3 border-rt">
                                                                    <h6>Name</h6>
                                                                    <p className="textEllips">
                                                                        <a href="JavaScript:Void(0);" >
                                                                            Archives
                                                                    </a>
                                                                    </p>
                                                                </div>
                                                                <div className="col-md-3 border-rt">
                                                                    <h6>No of Calls</h6>
                                                                    <p>
                                                                        {archive.totalArchive}
                                                                    </p>
                                                                </div>
                                                                <div className="col-md-2 border-rt">
                                                                    <h6>Duration</h6>
                                                                    <p>{parseFloat(archive.archiveOverAllDuration).toFixed('2')}</p>
                                                                </div>
                                                                <div className="col-md-2 border-rt">
                                                                    <h6>Cost</h6>
                                                                    <p>{parseFloat(archive.archiveOverAllCost).toFixed('2')}</p>
                                                                </div>
                                                                <div className="col-md-2">
                                                                    <h6>Price</h6>
                                                                    <p>{parseFloat(archive.archiveOverAllPrice).toFixed('2')}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                :
                                                null}

                                            <div className="row">
                                                <div className="col-md-12">

                                                    <div className="mng-full-table-row">
                                                        <div className="row">
                                                            <div className="col-md-3 border-rt">
                                                                <h6>Name</h6>
                                                                <p className="textEllips">
                                                                    <a href="JavaScript:Void(0);" >
                                                                        Total
                                                                    </a>
                                                                </p>
                                                            </div>
                                                            <div className="col-md-3 border-rt">
                                                                <h6>No of Calls</h6>
                                                                <p>-</p>
                                                            </div>
                                                            <div className="col-md-2 border-rt">
                                                                <h6>Duration</h6>
                                                                <p>{this.state.totalDuration && parseFloat(this.state.totalDuration).toFixed('2')}</p>
                                                            </div>
                                                            <div className="col-md-2 border-rt">
                                                                <h6>Cost</h6>
                                                                <p>{this.state.totalCost && parseFloat(this.state.totalCost).toFixed('2')}</p>
                                                            </div>
                                                            <div className="col-md-2">
                                                                <h6>Price</h6>
                                                                <p>{this.state.totalPrice && parseFloat(this.state.totalPrice).toFixed('2')}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                        :
                                        <div className="mng-full-table">
                                            <div className="row">
                                                <div className="col-md-12">
                                                    <div className="mng-full-table-hdr admn-usr-hdr">
                                                        <div className="row">
                                                            <div className="col-md-12">
                                                                <h6>No Details Found.</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    }

                                    {loading ?
                                        <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                                            <i className="fa fa-spinner fa-spin" ></i>
                                        </a>
                                        :
                                        null}

                                </div>

                            </div>
                        </div>
                    </div>
                </section>

            </main>
        );
    }
}

export default checkAuthentication(AdminMonthlyReport);
