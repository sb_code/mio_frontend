import React, { Component } from "react";
import Navbar from "../Component/Navbar";
import ProgressBar from 'react-bootstrap/ProgressBar';
import Moment from 'react-moment';
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button, Row, Col } from "react-bootstrap"
import ToggleButton from 'react-bootstrap/ToggleButton'
import _ from 'lodash';
import { CSVLink, CSVDownload } from "react-csv";
import ReactTooltip from 'react-tooltip'; // For tool tip 

import { checkAuthentication } from '../Component/Authentication';

var path = cred.API_PATH;

export class AdminConferenceReportList extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: false,

			userId: '',
			selectedCompanyId: '',
			selectedCompanyName: '',

			openSideMenu: false,
			searchString: '',
			searching: false,

			companyLists: [],

			editUser: false,
			confirm: false,

			companyName: '',
			selectedCompanyType: '',

			fromDt: '',
			toDT: '',

			page: 1,
			totalRows: '',

		}
		this.perPage = 10;
	}

	componentDidMount() {

		document.body.classList.remove("sign-bg");
		document.body.classList.add("grey-bg");

		let storage = localStorage.getItem("MIO_Local");
		storage = storage ? JSON.parse(storage) : null;

		console.log("this.props.location.state ===", this.props.location.state);

		let selectedCompanyId = this.props.match.params.id;
		let selectedCompanyName = this.props.location.state.companyName;

		if (storage && storage.data.userId) {
			this.setState({
				userId: storage.data.userId,
				userType: storage.data.userType,
				companyId: storage.data.companyId,
				selectedCompanyId: selectedCompanyId,
				selectedCompanyName: selectedCompanyName
			}, () => {

				// this.loadData();
				this.searchCompany();

			});

			// console.log(this.parseJwt(storage.token));
			let tokenData = this.parseJwt(storage.token);

		}

	}

	// componentWillMount() {
	// 	this.searchCompany();
	// }

	searchCompany = () => {

		this.setState({
			searching: true,
			loading: true
		});

		let { perPage, page, searchString, selectedCompanyId, fromDt, toDT } = this.state;

		let dataToSend = {
			"companyId": selectedCompanyId,
			"page": this.state.page,
			"perPage": this.perPage
		};

		axios
			.post(path + 'conference/list-conference-company', dataToSend)
			.then(serverResponse => {
				let res = serverResponse.data;
				// console.log("response for inbound details ==", res);
				if (!res.isError) {
					this.setState({
						transactionLists: res.details && res.details[0] && res.details[0].results,
						totalRows: res.details && res.details[0] && res.details[0].noofpage,
						loading: false
					});
				} else {
					this.setState({
						loading: false
					});
				}
			})
			.catch(error => {
				console.log(error);
			})

	}

	clearSearch = (event) => {
		event.preventDefault();
		this.setState({
			searchString: '',
			fromDt: '',
			toDT: '',
			pageNo: 1,
			transactionLists: [],
			totalRows: '',
			searching: false,
			loading: false
		}, () => {
			this.getCompanyListForAdmin();
			// this.getTransactionListForAdmin();
		});
	}

	clearState = () => {
		this.setState({
			loading: false,
		})

	}

	viewReport = (index) => {

		let { loading, transactionLists, companyLists, searchString, selectedCompanyId, selectedCompanyName } = this.state;
		let selectedConference = transactionLists[index];

		if (!loading) {

			this.props.history.push({
				pathname: "/adminreport/" + `${selectedCompanyId}` + "/conference/" + `${selectedConference._id}`,
				state: {
					"id": selectedConference._id,
					"conference": selectedConference.conferenceTitle,
					"company": selectedCompanyName
				}
			});

		}

	}

	pageChangePrev = () => {

        this.setState({
            page: (parseFloat(this.state.page) - 1)
        }, () => {
            this.searchCompany()
        });

    }

    pageChangeNext = () => {

        this.setState({
            page: (parseFloat(this.state.page) + 1)
        }, () => {
            this.searchCompany()
        });

    }

	backMenueToggle = () => {
		this.props.history.goBack();
	}

	sideMenuToggle() {
		let { openSideMenu } = this.state;
		openSideMenu = !openSideMenu;
		this.setState({ openSideMenu });
	}

	mobMenuClick() {
		this.sideMenuToggle();
		console.log('mobMenuClick');
	}

	parseJwt = (token) => {
		if (!token) { return; }
		const base64Url = token.split('.')[1];
		const base64 = base64Url.replace('-', '+').replace('_', '/');
		return JSON.parse(window.atob(base64));
	}


	render() {

		let { loading, searching, transactionLists, totalRows, page } = this.state;

		return (
			<main>

				<section className="user-mngnt-wrap">
					<div className="container-fluid">
						<div className="row">
							<Navbar routTo="/adminreport" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />
							<div className="col-xl-10 col-lg-9 p-0">
								<div className="mobile-menu-header">
									<a href="JavaScript:Void(0);" className="back-arw" onClick={() => this.backMenueToggle()}><i className="fas fa-arrow-left"></i></a>

								</div>
								<div className="row">
									<div className="col-md-12">
										<div className="inv-code">
											<span style={{ color: '#007bff' }}>Conference List For {this.state.selectedCompanyName}</span>
										</div>
									</div>
								</div>
								<div className="mng-full-list">
									<div className="mng-full-srch">

										{/* <div className="row">

											<div className="col col-md-12">
												<label>Company</label>
												<div className="srch-wrap">
													<form>

														<select className="form-control"
															name="searchString" value={this.state.searchString}
															onChange={(event) => { this.setState({ searchString: event.target.value }) }}
														// readOnly={searching ? true : false}
														>
															<option>Select Company</option>
															{companyLists && companyLists.map((value, index) => {
																return <option value={value._id} key={index}>
																	{value.companyName}
																</option>
															})}
														</select>

														{this.state.searching && !loading ?
															<input type="button" onClick={(event) => this.clearSearch(event)}
																data-tip='Clear'
															/>
															: !this.state.searching && !loading ?
																<input type="submit" onClick={(event) => this.searchCompany(event)}
																	data-tip='Search'
																/>
																: (!this.state.searching || this.state.searching) && loading ?
																	<a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
																		<i className="fa fa-spinner fa-spin" ></i>
																	</a>
																	: null

														}

														<ReactTooltip
															effect="float"
															place="top"
															data-border="true"
														/>
													</form>
												</div>
											</div>

										</div> */}

									</div>

									{transactionLists && transactionLists.length > 0 ?
										<div className="mng-full-table company-mangmnt">

											<div className="row">

												<div className="col-md-10">
													<div className="mng-full-table-hdr">
														<div className="row">
															<div className="col-md-4 border-rt">
																<h6>Name</h6>
															</div>
															<div className="col-md-4 border-rt">
																<h6>Created On</h6>
															</div>
															<div className="col-md-4">
																<h6>Participants</h6>
															</div>

														</div>
													</div>
												</div>

												<div className="col-md-2">
													<div className="mng-full-table-hdr">
														<div className="row">
															<div className="col-md-12">
																<h6>View</h6>
															</div>
														</div>
													</div>
												</div>

											</div>

											{transactionLists && transactionLists.length > 0 ? transactionLists.map((data, index) => {

												return <div className="row" key={index}>

													<div className="col-md-10">
														<div className="mng-full-table-row">
															<div className="row">
																<div className="col-md-4 border-rt">
																	<h6>Name</h6>
																	<p className="textEllips">
																		{data.conferenceTitle}
																	</p>
																</div>
																<div className="col-md-4 border-rt">
																	<h6>Created On</h6>
																	<p className="textEllips">
																		<Moment format="DD/MM/YYYY">
																			{data.createdAt}
																		</Moment>
																	</p>
																</div>
																<div className="col-md-4">
																	<h6>Participants</h6>
																	<p>{data.participants && data.participants.length}</p>
																</div>

																<div className="mobile-ad-edt-btns">
																	<ul>
																		<li onClick={() => this.viewReport(index)}>
																			<a href="JavaScript:Void(0);"
																				data-tip='View'
																			>
																				<i className="fas fa-eye"></i>
																			</a>
																			<ReactTooltip
																				effect="float"
																				place="top"
																				data-border="true"
																			/>
																		</li>

																	</ul>
																</div>

															</div>
														</div>
													</div>

													<div className="col-md-2">
														<div className="mng-full-table-row add-edt text-center">
															<div className="row">
																<div className="col-md-12">
																	<a href="JavaScript:Void(0);"
																		data-tip='View'
																		onClick={() => { this.viewReport(index) }}
																	>
																		<i className="fas fa-eye"></i>

																	</a>
																	<ReactTooltip
																		effect="float"
																		place="top"
																		data-border="true"
																	/>
																</div>

															</div>
														</div>
													</div>

												</div>
											})
												: null}

										</div>
										:
										<div className="mng-full-table">
											<div className="row">
												<div className="col-md-12">
													<div className="mng-full-table-hdr admn-usr-hdr">
														<div className="row">
															<div className="col-md-12">
																<h6>No Details Found.</h6>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									}

									<br/><br/>
									<div className="form-group">
										{!loading && parseFloat(page) > 1 ?
											<span style={{ color: "#007bff", cursor: "pointer" }}
												onClick={() => { this.pageChangePrev() }}
											>
												<b>{"<< Prev"}</b>
											</span>
											:
											null}
                                            &nbsp;&nbsp;|&nbsp;&nbsp;
                                            {!loading && (parseFloat(page) < parseFloat(totalRows)) ?
											<span style={{ color: "#007bff", cursor: "pointer" }}
												onClick={() => { this.pageChangeNext() }}
											>
												<b>{"Next >>"}</b>
											</span>
											:
											null}
										{loading ?
											<a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
												<i className="fa fa-spinner fa-spin" ></i>
											</a>
											:
											null}

									</div>


								</div>

							</div>
						</div>
					</div>
				</section>

			</main>
		);
	}
}

export default checkAuthentication(AdminConferenceReportList);
