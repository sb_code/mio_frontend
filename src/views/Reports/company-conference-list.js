import React, { Component } from "react";
import Navbar from "../Component/Navbar";
import ProgressBar from 'react-bootstrap/ProgressBar';
import Moment from 'react-moment';
import moment from 'moment';
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button, Row, Col } from "react-bootstrap"
import ToggleButton from 'react-bootstrap/ToggleButton'
import _ from 'lodash';
import { CSVLink, CSVDownload } from "react-csv";
import ReactTooltip from 'react-tooltip'; // For tool tip 

import { checkAuthentication } from '../Component/Authentication';

var path = cred.API_PATH;

export class CompanyConferenceReportList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,

            userId: '',

            openSideMenu: false,
            searchString: '',
            searching: false,

            conferenceList: [],
            conferenceDurations: [],

            conferenceId: '',

            details: '',
            totalCost: '',

            fromDt: '',
            toDT: '',

            page: 1,
            totalRows: '',

        }
        this.perPage = 10;
    }

    componentDidMount() {
        document.body.classList.remove("sign-bg");
        document.body.classList.add("grey-bg");

        let storage = localStorage.getItem("MIO_Local");
        storage = storage ? JSON.parse(storage) : null;

        if (storage && storage.data.userId) {
            this.setState({
                userId: storage.data.userId,
                userType: storage.data.userType,
                companyId: storage.data.companyId,
                companyName: storage.data.companyName
            }, () => {
                this.getconferenceList();
            });

            let tokenData = this.parseJwt(storage.token);

        }

    }

    getconferenceList = () => {

        this.setState({ loading: true });
        let dataToSend = {
            "companyId": this.state.companyId,
            "page": this.state.page,
            "perPage": this.perPage
        };
        // API -
        axios
            .post(path + 'conference/list-conference-company', dataToSend)
            .then(serverResponse => {
                let res = serverResponse.data;
                // console.log("response for inbound details ==", res);
                if (!res.isError) {
                    this.setState({
                        conferenceList: res.details && res.details[0] && res.details[0].results,
                        totalRows: res.details && res.details[0] && res.details[0].noofpage,
                        loading: false
                    });
                } else {
                    this.setState({
                        loading: false
                    });
                }
            })
            .catch(error => {
                console.log(error);
            })

    }

    getDurationListByConference = (event) => {

        let { conferenceList, loading } = this.state;
        let selectedConferenceId = event && event.target.value;
        this.setState({
            conferenceId: selectedConferenceId
        },()=>{
            this.getConferenceDurationForAdmin();
        });

    }

    getConferenceDurationForAdmin =() => {

        this.setState({ loading: true });
        let dataToSend = {
            "conferenceId": this.state.conferenceId,
            "page": '',
            "perPage": ''
        };
        // API -- 
        axios
            .post(path + 'conference/fetch-conference-duration-for-conference', dataToSend)
            .then(serverResponse => {

                let res = serverResponse.data;
                if (!res.isError) {
                    let data = res.details && res.details.conferenceDurations;
                    this.setState({
                        loading: false,
                        conferenceDurations: data && data[0] && data[0].results
                    });
                } else {
                    this.setState({
                        loading: false
                    });
                }

            })
            .catch(error => {
                console.log(error);
            })


    }

    // searchDurationDetails = (event) => {
    //     if (event) {
    //         event.preventDefault();
    //     }

    //     this.setState({
    //         loading: true,
    //         searching: true
    //     });
    //     let dataToSend = {
    //         // "conferenceId": "5ea276f80c2d0369ee1584d2"
    //         "conferenceDurationId": this.state.searchString
    //     };
    //     // API --
    //     axios
    //         .post(path + 'price-management/fetch-cost-for-duration-of-conference', dataToSend)
    //         .then(serverResponse => {
    //             let res = serverResponse.data;
    //             // console.log("conference duration and report for admin  =  ", res);
    //             if (!res.isError) {
    //                 this.setState({
    //                     loading: false,
    //                     details: res.details,
    //                     totalCost: res.details && res.details.lastDurationAllCallsCost
    //                 });
    //             } else {
    //                 this.setState({
    //                     loading: false
    //                 });

    //             }
    //         })
    //         .catch(error => {
    //             console.log(error);
    //         })


    // }

    clearSearch = (event) => {
        event.preventDefault();
        this.setState({
            companyId: '',
            searchString: '',
            fromDt: '',
            toDT: '',
            pageNo: 1,
            details: '',
            totalRows: '',
            searching: false,
            loading: false
        });
    }

    clearState = () => {
        this.setState({
            loading: false,

        })

    }

    viewReport= (index)=> {

        let { loading, conferenceList, companyId, companyName } = this.state;
		let selectedConference = conferenceList[index];

		if (!loading) {

			this.props.history.push({
				pathname: "/companyreport/conference/" +`${selectedConference._id}`,
				state: {
					"id": selectedConference._id,
					"conference": selectedConference.conferenceTitle,
					"company": companyName
				}
			});

		}

    }

    pageChangePrev = () => {

        this.setState({
            page: (parseFloat(this.state.page) - 1)
        }, () => {
            this.getconferenceList()
        });

    }

    pageChangeNext = () => {

        this.setState({
            page: (parseFloat(this.state.page) + 1)
        }, () => {
            this.getconferenceList()
        });

    }

    backMenueToggle = () => {
        this.props.history.goBack();
    }

    sideMenuToggle() {
        let { openSideMenu } = this.state;
        openSideMenu = !openSideMenu;
        this.setState({ openSideMenu });
    }

    mobMenuClick() {
        this.sideMenuToggle();
        console.log('mobMenuClick');
    }

    parseJwt = (token) => {
        if (!token) { return; }
        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace('-', '+').replace('_', '/');
        return JSON.parse(window.atob(base64));
    }


    render() {

        let { loading, conferenceList, page, perPage, totalRows } = this.state;


        return (
            <main>

                <section className="user-mngnt-wrap">
                    <div className="container-fluid">
                        <div className="row">
                            <Navbar routTo="/companyreport" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />
                            <div className="col-xl-10 col-lg-9 p-0">
                                <div className="mobile-menu-header">
                                    <a href="JavaScript:Void(0);" className="back-arw" onClick={() => this.backMenueToggle()}><i className="fas fa-arrow-left"></i></a>

                                </div>

                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="inv-code">
                                            <span style={{ color: '#007bff' }}>Conference List</span>
                                        </div>
                                    </div>
                                </div>

                                <div className="mng-full-list">
                                    <div className="mng-full-srch">
                                        <div className="row">

                                            {/* <div className="col-md-6">
                                                <label>Conference</label>
                                                <select className="form-control"
                                                    name="conferenceId" value={this.state.conferenceId}
                                                    onChange={(event) => { this.getDurationListByConference(event) }}
                                                >
                                                    <option value= "">Select Conference</option>
                                                    {conferenceList && conferenceList.map((value, index) => {
                                                        return <option value={value._id} key={index}>
                                                            {value.conferenceTitle}
                                                        </option>
                                                    })}
                                                </select>
                                            </div>

                                            
                                            <div className="col col-md-6">
                                                <label>Duration</label>
                                                <div className="srch-wrap">
                                                    <form>

                                                        <select className="form-control"
                                                            name="searchString" value={this.state.searchString}
                                                            onChange={(event) => { this.setState({ searchString: event.target.value }) }}
                                                        >
                                                            <option>Select Duration</option>
                                                            {conferenceDurations && conferenceDurations.map((value, index) => {
                                                                return <option value={value._id} key={index}>
                                                                    {moment(new Date(value.createdAt)).format("YYYY-MM-DD") + '  ' + moment(new Date(value.createdAt)).format("HH.mm")}
                                                                </option>
                                                            })}
                                                        </select>

                                                        {this.state.searching && !loading ?
                                                            <input type="button" onClick={(event) => this.clearSearch(event)}
                                                                data-tip='Clear'
                                                            />
                                                            : !this.state.searching && !loading ?
                                                                <input type="submit" onClick={(event) => this.searchDurationDetails(event)}
                                                                    data-tip='Search'
                                                                />
                                                                : loading ?
                                                                    <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                                                                        <i className="fa fa-spinner fa-spin" ></i>
                                                                    </a>
                                                                    : null}

                                                        <ReactTooltip
                                                            effect="float"
                                                            place="top"
                                                            data-border="true"
                                                        />
                                                    </form>
                                                </div>
                                            </div> */}

                                        </div>
                                    </div>

                                    {conferenceList ?
										<div className="mng-full-table company-mangmnt">

											<div className="row">

												<div className="col-md-10">
													<div className="mng-full-table-hdr">
														<div className="row">
															<div className="col-md-4 border-rt">
																<h6>Name</h6>
															</div>
															<div className="col-md-4 border-rt">
																<h6>Created On</h6>
															</div>
															<div className="col-md-4">
																<h6>Participants</h6>
															</div>

														</div>
													</div>
												</div>

												<div className="col-md-2">
													<div className="mng-full-table-hdr">
														<div className="row">
															<div className="col-md-12">
																<h6>View</h6>
															</div>
														</div>
													</div>
												</div>

											</div>

											{conferenceList ? conferenceList.map((data, index) => {

												return <div className="row" key={index}>

													<div className="col-md-10">
														<div className="mng-full-table-row">
															<div className="row">
																<div className="col-md-4 border-rt">
																	<h6>Name</h6>
																	<p className="textEllips">
																		{data.conferenceTitle}
																	</p>
																</div>
																<div className="col-md-4 border-rt">
																	<h6>Created On</h6>
																	<p className="textEllips">
																		<Moment format="DD/MM/YYYY">
																			{data.createdAt}
																		</Moment>
																	</p>
																</div>
																<div className="col-md-4">
																	<h6>Participants</h6>
																	<p>{data.participants && data.participants.length}</p>
																</div>

																<div className="mobile-ad-edt-btns">
																	<ul>
																		<li onClick={() => this.viewReport(index)}>
																			<a href="JavaScript:Void(0);"
																				data-tip='View'
																			>
																				<i className="fas fa-eye"></i>
																			</a>
																			<ReactTooltip
																				effect="float"
																				place="top"
																				data-border="true"
																			/>
																		</li>

																	</ul>
																</div>

															</div>
														</div>
													</div>

													<div className="col-md-2">
														<div className="mng-full-table-row add-edt text-center">
															<div className="row">
																<div className="col-md-12">
																	<a href="JavaScript:Void(0);"
																		data-tip='View'
																		onClick={() => { this.viewReport(index) }}
																	>
																		<i className="fas fa-eye"></i>

																	</a>
																	<ReactTooltip
																		effect="float"
																		place="top"
																		data-border="true"
																	/>
																</div>

															</div>
														</div>
													</div>

												</div>
											})
												: null}

										</div>
										:
										<div className="mng-full-table">
											<div className="row">
												<div className="col-md-12">
													<div className="mng-full-table-hdr admn-usr-hdr">
														<div className="row">
															<div className="col-md-12">
																<h6>No Details Found.</h6>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									}

                                    <br /><br />
                                    <div className="form-group">
                                        {!loading && parseFloat(page) > 1 ?
                                            <span style={{ color: "#007bff", cursor: "pointer" }}
                                                onClick={() => { this.pageChangePrev() }}
                                            >
                                                <b>{"<< Prev"}</b>
                                            </span>
                                            :
                                            null}
                                        &nbsp;&nbsp;|&nbsp;&nbsp;
                                        {!loading && parseFloat(page) < parseFloat(totalRows) ?
                                            <span style={{ color: "#007bff", cursor: "pointer" }}
                                                onClick={() => { this.pageChangeNext() }}
                                            >
                                                <b>{"Next >>"}</b>
                                            </span>
                                            :
                                            null}
                                        {loading ?
                                            <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                                                <i className="fa fa-spinner fa-spin" ></i>
                                            </a>
                                            :
                                            null}

                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </section>

            </main>
        );
    }
}

export default checkAuthentication(CompanyConferenceReportList);
