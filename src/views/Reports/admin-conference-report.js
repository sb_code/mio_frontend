import React, { Component } from "react";
import Navbar from "../Component/Navbar";
import ProgressBar from 'react-bootstrap/ProgressBar';
// import Moment from 'react-moment';
import moment from 'moment';
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button, Row, Col } from "react-bootstrap"
import ToggleButton from 'react-bootstrap/ToggleButton'
import _ from 'lodash';
import { CSVLink, CSVDownload } from "react-csv";
import ReactTooltip from 'react-tooltip'; // For tool tip 

import { checkAuthentication } from '../Component/Authentication';

var path = cred.API_PATH;

export class AdminConferenceReport extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: false,

			userId: '',

			selectedCompanyId: '',
			selectedConferenceId: '',
			selectedCompanyName: '',
			selectedConferenceName: '',

			openSideMenu: false,
			searchString: '',
			durationString: '',
			searching: false,

			inboundCalls: '',
			outBoundCalls: '',
			webCalls: '',

			editUser: false,
			confirm: false,

			details: '',
			webCallDetails: '',
			duration: 0,
			cost: 0,
			price: 0,

			fromDt: '',
			toDT: '',

			perPage: 10,
			pageNo: 1,
			totalRows: '',

			modalDetails: {},
			showDetails: false,
		}
		this.totalDuration = 0;
		this.totalCost = 0;
		this.totalPrice = 0;
	}

	componentDidMount() {
		document.body.classList.remove("sign-bg");
		document.body.classList.add("grey-bg");

		let storage = localStorage.getItem("MIO_Local");
		storage = storage ? JSON.parse(storage) : null;

		let selectedCompanyId = this.props.match.params.id;
		let selectedConferenceId = this.props.match.params.conId;
		let selectedCompanyName = this.props.location.state.company;
		let selectedConferenceName = this.props.location.state.conference;

		if (storage && storage.data.userId) {
			this.setState({
				userId: storage.data.userId,
				userType: storage.data.userType,
				companyId: storage.data.companyId,
				selectedCompanyId: selectedCompanyId,
				selectedConferenceId: selectedConferenceId,
				selectedCompanyName: selectedCompanyName,
				selectedConferenceName: selectedConferenceName
			}, () => {
				// this.getConferenceReportForAdmin();
				this.getConferenceDurationForAdmin();
			});

			let tokenData = this.parseJwt(storage.token);

		}

	}

	getConferenceDurationForAdmin = () => {

		return new Promise((resolve, reject) => {
			this.setState({ loading: true });
			let dataToSend = {
				"conferenceId": this.state.selectedConferenceId,
				"page": '',
				"perPage": ''
			};
			// API -- 
			axios
				.post(path + 'conference/fetch-conference-duration-for-conference', dataToSend)
				.then(serverResponse => {

					let res = serverResponse.data;
					if (!res.isError) {
						let data = res.details && res.details.conferenceDurations;
						this.setState({
							loading: false,
							conferenceDurations: data && data[0] && data[0].results
						}, () => {
							resolve()
						});
					} else {
						this.setState({
							loading: false
						}, () => {
							reject()
						});
					}

				})
				.catch(error => {
					console.log(error);
				})

		})

	}


	searchDurationDetails = (event) => {
		if (event) {
			event.preventDefault();
		}

		this.setState({
			loading: true,
			searching: true
		});
		let dataToSend = {
			"conferenceDurationId": this.state.searchString
		};
		// API --
		axios
			.post(path + 'price-management/fetch-cost-for-duration-of-conference', dataToSend)
			.then(serverResponse => {
				let res = serverResponse.data;
				if (!res.isError) {
					// console.log(res.details);
					let details = res.details
					let webCallList = details.webCallDetails && details.webCallDetails.webCallList && details.webCallDetails.webCallList.length > 0 && details.webCallDetails.webCallList;

					let { duration, cost, price } = this.state;

					// webCallList && webCallList.map((data, index) => {

					// 	duration = parseFloat(duration) + parseFloat(data.webCallDetails.duration);

					// 	cost = parseFloat(cost) + parseFloat(data.webCallDetails.totalCost);

					// 	price = parseFloat(price) + parseFloat(data.webCallDetails.totalPrice);

					// });

					this.setState({
						loading: false,
						details: webCallList,
						webCallDetails: details.webCallDetails,
						duration: res.details && res.details.webCallDetails && res.details.webCallDetails.totalWebCallDuration,
						cost: res.details && res.details.webCallDetails && res.details.webCallDetails.totalWebCalCost,
						price: res.details && res.details.webCallDetails && res.details.webCallDetails.totalWebCalPrice,
					}, () => {
						this.getUserwiseDetails(this.state.searchString);
					});
				} else {
					this.setState({
						loading: false
					});
				}
			})
			.catch(error => {
				console.log(error);
			})


	}

	getUserwiseDetails = (durationId) => {

		let dataToSend = {
			"conferenceDurationId": durationId
		};
		// API -- 
		axios
			.post(path + 'conference/get-meeting-details', dataToSend)
			.then(serverResponse => {
				let res = serverResponse.data;
				this.setState({
					inboundCalls: res.details && res.details.inboundCalls,
					outBoundCalls: res.details && res.details.uotboundCalls,
					webCalls: res.details && res.details.webCalls
				})
			})
			.catch(error => {
				console.log(error);
			})

	}

	clearSearch = (event) => {
		event.preventDefault();
		this.setState({
			searchString: '',
			durationString: '',
			webCallDetails: '',
			details: '',
			searching: false,
			loading: false,
			duration: 0,
			cost: 0,
			price: 0,
		});
	}

	clearState = () => {
		this.setState({
			loading: false,

		})

	}

	pageChangePrev = () => {

		this.setState({
			pageNo: (parseFloat(this.state.pageNo) - 1)
		}, () => {
			this.searchDurationDetails()
		});

	}

	pageChangeNext = () => {

		this.setState({
			pageNo: (parseFloat(this.state.pageNo) + 1)
		}, () => {
			this.searchDurationDetails()
		});

	}

	backMenueToggle = () => {
		this.props.history.goBack();
	}

	sideMenuToggle() {
		let { openSideMenu } = this.state;
		openSideMenu = !openSideMenu;
		this.setState({ openSideMenu });
	}

	mobMenuClick() {
		this.sideMenuToggle();
		console.log('mobMenuClick');
	}

	parseJwt = (token) => {
		if (!token) { return; }
		const base64Url = token.split('.')[1];
		const base64 = base64Url.replace('-', '+').replace('_', '/');
		return JSON.parse(window.atob(base64));
	}

	showDetailsModal(modalDetails) {

		let dataToSend = { userId: modalDetails.userId };
		// API -- 
		axios
			.post(path + 'user/get_user_by_id', dataToSend)
			.then(serverResponse => {

				let res = serverResponse.data;
				if (!res.isError && res.details) {
					let data = res.details;
					console.log(data);
					modalDetails.userName = data.fname ? `${data.fname} ${data.lname}` : '';
				} else {

				}
				this.setState({
					modalDetails,
					showDetails: true,
				});

			})
			.catch(error => {
				console.log(error);
			})
	}

	hideDetailsModal() {
		this.setState({
			modalDetails: {},
			showDetails: false,
		});
	}

	render() {

		let { loading, conferenceDurations, details, webCallDetails, inboundCalls, outBoundCalls, webCalls, } = this.state;

		return (
			<main>

				<section className="user-mngnt-wrap">
					<div className="container-fluid">
						<div className="row">
							<Navbar routTo="/adminreport" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />
							<div className="col-xl-10 col-lg-9 p-0">
								<div className="mobile-menu-header">
									<a href="JavaScript:Void(0);" className="back-arw" onClick={() => this.backMenueToggle()}><i className="fas fa-arrow-left"></i></a>

								</div>

								<div className="paymnt-mng-hdr" style={{ margin: "30px" }}>
									<div className="blnc">
										<div className="logobox"><img src="/images/logo-sign.png" alt="" /></div>
									</div>
									<div className="rchrg-btn">
										<p className="text-right" style={{ margin: "0" }}>
											<b style={{ color: "#0092DD" }}>Conference Report</b>
											<br />Conference Name: {this.state.selectedConferenceName}
											<br />Company Name: {this.state.selectedCompanyName}<br />
										</p>
									</div>
								</div>

								<div className="mng-full-list" style={{ padding: "0px 50px" }}>
									<div className="mng-full-srch">
										<div className="row">

											<div className="col col-md-12">
												<label>Duration</label>
												<div className="srch-wrap">
													<form>

														<select className="form-control"
															name="searchString" value={this.state.searchString}
															onChange={(event) => { this.setState({ searchString: event.target.value, }) }}
														// readOnly={searching ? true : false}
														>
															<option>Select Duration</option>
															{conferenceDurations && conferenceDurations.length > 0 && conferenceDurations.reverse().map((value, index) => {
																return <option value={value._id} key={index}>
																	{moment(new Date(value.createdAt)).format("YYYY MMM DD") + ' T: ' + moment(new Date(value.createdAt)).format("HH:mm a")}
																</option>
															})}
														</select>

														{this.state.searching && !loading ?
															<input type="button" onClick={(event) => this.clearSearch(event)}
																data-tip='Clear'
															/>
															: !this.state.searching && !loading ?
																<input type="submit" onClick={(event) => this.searchDurationDetails(event)}
																	data-tip='Search'
																/>
																: (!this.state.searching || this.state.searching) && loading ?
																	<a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
																		<i className="fa fa-spinner fa-spin" ></i>
																	</a>
																	: null}

														<ReactTooltip
															effect="float"
															place="top"
															data-border="true"
														/>
													</form>
												</div>
											</div>

										</div>
									</div>

									{webCallDetails ?
										<div className="mng-full-table company-mangmnt">

											<div className="row">

												<div className="col-md-12">
													<div className="mng-full-table-hdr">
														<div className="row">

															<div className="col-md-3 border-rt">
																<h6>User</h6>
															</div>
															<div className="col-md-3 border-rt">
																<h6>Medium</h6>
															</div>
															<div className="col-md-2 border-rt">
																<h6>Duration</h6>
															</div>
															<div className="col-md-2 border-rt">
																<h6>Cost</h6>
															</div>
															<div className="col-md-2">
																<h6>Price</h6>
															</div>
														</div>
													</div>
												</div>

											</div>

											{details && details.map((respdata, index) => {
												return <div className="row">
													<div className="col-md-12">
														<div className="mng-full-table-row">
															<div className="row">
																<div className="col-md-3 border-rt">
																	<h6>User</h6>
																	<p style={{ cursor: 'pointer' }} onClick={() => this.showDetailsModal({
																		medium: 'WEB',
																		userId: respdata.webCallDetails.userId,
																		start: respdata.webCallDetails.createdAt,
																		end: respdata.webCallDetails.updatedAt,
																		duration: respdata.webCallDetails.duration,
																		cost: respdata.webCallDetails.totalCost,
																		price: respdata.webCallDetails.totalPrice,
																	})} className="textEllips">{respdata.webCallDetails && respdata.webCallDetails.userId}</p>
																</div>
																<div className="col-md-3 border-rt">
																	<h6>Medium</h6>
																	<p>WEB</p>
																</div>
																<div className="col-md-2 border-rt">
																	<h6>Duration</h6>
																	<p>{respdata.webCallDetails && respdata.webCallDetails.duration && parseFloat(respdata.webCallDetails.duration).toFixed(2)}</p>
																</div>
																<div className="col-md-2 border-rt">
																	<h6>Cost</h6>
																	<p>{respdata.webCallDetails && respdata.webCallDetails.totalCost && parseFloat(respdata.webCallDetails.totalCost).toFixed(2)}</p>
																</div>
																<div className="col-md-2">
																	<h6>Price</h6>
																	<p>{respdata.webCallDetails && respdata.webCallDetails.totalPrice && parseFloat(respdata.webCallDetails.totalPrice).toFixed(2)}</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											})}

											<div className="row">
												<div className="col-md-12">
													<div className="mng-full-table-row">
														<div className="row">
															<div className="col-md-3 border-rt">
																<h6>User</h6>
																<p>Total</p>
															</div>
															<div className="col-md-3 border-rt">
																<h6>Medium</h6>
																<p>WEB</p>
															</div>
															<div className="col-md-2 border-rt">
																<h6>Duration</h6>
																<p>{this.state.duration && parseFloat(this.state.duration).toFixed('2') + 'Mins'}</p>
															</div>
															<div className="col-md-2 border-rt">
																<h6>Cost</h6>
																<p>{this.state.cost && parseFloat(this.state.cost).toFixed('2')}</p>
															</div>
															<div className="col-md-2">
																<h6>Price</h6>
																<p>{this.state.price && parseFloat(this.state.price).toFixed('2')}</p>
															</div>
														</div>
													</div>
												</div>
											</div>

										</div>
										:
										null}
									<br/><br/>
									{inboundCalls && inboundCalls.length > 0 ?
									<span>Inbound Details: </span>
									:null}
									{inboundCalls && inboundCalls.length > 0 ?
										<div className="mng-full-table company-mangmnt">

											<div className="row">

												<div className="col-md-12">
													<div className="mng-full-table-hdr">
														<div className="row">

															<div className="col-md-3 border-rt">
																<h6>From No.</h6>
															</div>
															<div className="col-md-3 border-rt">
																<h6>Register Name</h6>
															</div>
															<div className="col-md-2 border-rt">
																<h6>Register Email</h6>
															</div>
															<div className="col-md-2 border-rt">
																<h6>Duration</h6>
															</div>
															<div className="col-md-2">
																<h6>Cost</h6>
															</div>
														</div>
													</div>
												</div>

											</div>

											{inboundCalls && inboundCalls.map((data, index) => {

												return <div className="row" key={index}>

													<div className="col-md-12">
														<div className="mng-full-table-row">
															<div className="row">

																<div className="col-md-3 border-rt">
																	<p>{data.fromMobile}</p>
																</div>
																<div className="col-md-3 border-rt">
																	<p>{data.registerName}</p>
																</div>
																<div className="col-md-2 border-rt">
																	<p>{data.registerEmail}</p>
																</div>
																<div className="col-md-2 border-rt">
																	<p>{parseFloat(data.duration).toFixed('2')}</p>
																</div>
																<div className="col-md-2">
																	<p>{parseFloat(data.totalCost).toFixed('2')}</p>
																</div>
															</div>
														</div>
													</div>

												</div>

											})}

										</div>
										: null}

									<br/><br/>
									{outBoundCalls && outBoundCalls.length > 0 ?
										<span>Outbound Details: </span>
									:null}
									{outBoundCalls && outBoundCalls.length > 0 ?
										<div className="mng-full-table company-mangmnt">

											<div className="row">

												<div className="col-md-12">
													<div className="mng-full-table-hdr">
														<div className="row">

															<div className="col-md-4 border-rt">
																<h6>To No.</h6>
															</div>
															<div className="col-md-4 border-rt">
																<h6>Duration</h6>
															</div>
															<div className="col-md-4">
																<h6>Cost</h6>
															</div>
														</div>
													</div>
												</div>

											</div>

											{outBoundCalls && outBoundCalls.map((data, index) => {

												return <div className="row" key={index}>

													<div className="col-md-12">
														<div className="mng-full-table-row">
															<div className="row">

																<div className="col-md-4 border-rt">
																	<p>{data.toMobile}</p>
																</div>
																<div className="col-md-4 border-rt">
																	<p>{parseFloat(data.duration).toFixed('2')}</p>
																</div>
																<div className="col-md-4">
																	<p>{parseFloat(data.totalCost).toFixed('2')}</p>
																</div>
															</div>
														</div>
													</div>

												</div>

											})}

										</div>
										: null}

									<br/><br/>
									{webCalls && webCalls.length > 0 ?
										<span>Web Call Details: </span>
									:null}
									{webCalls && webCalls.length > 0 ?
										<div className="mng-full-table company-mangmnt">

											<div className="row">

												<div className="col-md-12">
													<div className="mng-full-table-hdr">
														<div className="row">

															<div className="col-md-3 border-rt">
																<h6>User</h6>
															</div>
															<div className="col-md-2 border-rt">
																<h6>User Type</h6>
															</div>
															<div className="col-md-3 border-rt">
																<h6>Email</h6>
															</div>
															<div className="col-md-2 border-rt">
																<h6>Duration</h6>
															</div>
															<div className="col-md-2 ">
																<h6>Cost</h6>
															</div>
														</div>
													</div>
												</div>

											</div>

											{webCalls && webCalls.map((data, index) => {

												return <div className="row" key={index}>

													<div className="col-md-12">
														<div className="mng-full-table-row">
															<div className="row">

																<div className="col-md-3 border-rt">
																	<p>{data.userName? data.userName: '-'}</p>
																</div>
																<div className="col-md-2 border-rt">
																	<p>{data.userType? data.userType: '-'}</p>
																</div>
																<div className="col-md-3 border-rt">
																	<p>{data.userEmail? data.userEmail: '-'}</p>
																</div>
																<div className="col-md-2 border-rt">
																	<p>{parseFloat(data.duration).toFixed('2')}</p>
																</div>
																<div className="col-md-2">
																	<p>{parseFloat(data.totalCost).toFixed('2')}</p>
																</div>
															</div>
														</div>
													</div>

												</div>

											})}

										</div>
										: null}

								</div>

							</div>

						</div>

					</div>

				</section >

				<Modal show={this.state.showDetails} onHide={() => this.hideDetailsModal()}>
					<Modal.Header closeButton>
						<Modal.Title>Details</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<div className="row">

							<div className="col-md-12">
								<div className="form-group">
									<label>Conference Title:</label>
									<input
										type="text"
										name="name"
										value={this.state.selectedConferenceName}
										disabled
										className="form-control"
										placeholder="Enter Name"
									/>
								</div>
							</div>

							{/* <div className="col-md-12">
								<div className="form-group">
									<label>Conference Date:</label>
									<input
										type="text"
										name="name"
										value={this.state.durationString}
										disabled
										className="form-control"
										placeholder="Enter Name"
									/>
								</div>
							</div> */}

							<div className="col-md-12">
								<div className="form-group">
									<label>User Name:</label>
									<input
										type="text"
										name="name"
										value={this.state.modalDetails.userName}
										disabled
										className="form-control"
										placeholder="Enter Name"
									/>
								</div>
							</div>

							<div className="col-md-6">
								<div className="form-group">
									<label>Medium:</label>
									<input
										type="text"
										name="name"
										value={this.state.modalDetails.medium}
										disabled
										className="form-control"
										placeholder="Enter Name"
									/>
								</div>
							</div>

							<div className="col-md-6">
								<div className="form-group">
									<label>Start:</label>
									<input
										type="text"
										name="name"
										value={moment(new Date(this.state.modalDetails.start)).format("YYYY MMM DD") + ' T: ' + moment(new Date(this.state.modalDetails.start)).format("HH:mm a")}
										disabled
										className="form-control"
										placeholder="Enter Name"
									/>
								</div>
							</div>
							<div className="col-md-6">
								<div className="form-group">
									<label>End:</label>
									<input
										type="text"
										name="name"
										value={moment(new Date(this.state.modalDetails.end)).format("YYYY MMM DD") + ' T: ' + moment(new Date(this.state.modalDetails.end)).format("HH:mm a")}
										disabled
										className="form-control"
										placeholder="Enter Name"
									/>
								</div>
							</div>

							<div className="col-md-6">
								<div className="form-group">
									<label>Duration:</label>
									<input
										type="text"
										name="name"
										value={this.state.modalDetails.duration}
										disabled
										className="form-control"
										placeholder="Enter Name"
									/>
								</div>
							</div>

							<div className="col-md-6">
								<div className="form-group">
									<label>Cost:</label>
									<input
										type="text"
										name="name"
										value={this.state.modalDetails.cost}
										disabled
										className="form-control"
										placeholder="Enter Name"
									/>
								</div>
							</div>
							<div className="col-md-6">
								<div className="form-group">
									<label>Price:</label>
									<input
										type="text"
										name="name"
										value={this.state.modalDetails.price}
										disabled
										className="form-control"
										placeholder="Enter Name"
									/>
								</div>
							</div>

						</div>

					</Modal.Body>
					<Modal.Footer>
						<Button variant="primary" onClick={() => this.hideDetailsModal()}>Close</Button>
					</Modal.Footer>
				</Modal>
			
			</main >
		);
	}
}

export default checkAuthentication(AdminConferenceReport);
