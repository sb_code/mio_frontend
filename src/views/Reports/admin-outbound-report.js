import React, { Component } from "react";
import Navbar from "../Component/Navbar";
import ProgressBar from 'react-bootstrap/ProgressBar';
import Moment from 'react-moment';
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button, Row, Col } from "react-bootstrap"
import ToggleButton from 'react-bootstrap/ToggleButton'
import _ from 'lodash';
import { CSVLink, CSVDownload } from "react-csv";
import ReactTooltip from 'react-tooltip'; // For tool tip 

import { checkAuthentication } from '../Component/Authentication';

var path = cred.API_PATH;
const monthList = [{ "key": 1, "name": 'January' }, { "key": 2, "name": 'February' }, { "key": 3, "name": 'March' }, { "key": 4, "name": 'April' }, { "key": 5, "name": 'May' },
{ "key": 6, "name": 'June' }, { "key": 7, "name": 'July' }, { "key": 8, "name": 'August' }, { "key": 9, "name": 'September' }, { "key": 10, "name": 'October' },
{ "key": 11, "name": 'November' }, { "key": 12, "name": 'December' }];

export class AdminOutboundReport extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,

            userId: '',
            selectedCompanyId: '',
            selectedCompanyName: '',

            openSideMenu: false,
            searchString: '',
            searchYear: (new Date()).getFullYear(),
            searchMonth: (new Date()).getMonth() + 1,
            searching: false,

            companyLists: [],
            duration: 0,
            cost: 0,
            price: 0,

            editUser: false,
            confirm: false,

            companyName: '',
            selectedCompanyType: '',

            thisYear: (new Date()).getFullYear(),
            minOffset: 0,
            maxOffset: 60,
            thisMonth: (new Date()).getMonth() + 1,

            fromDt: '',
            toDT: '',

            perPage: 10,
            pageNo: 1,
            totalRows: '',
            totalRecords: '',

        }
        this.totalDuration = 0;
        this.totalCost = 0;
        this.totalPrice = 0;
    }

    componentDidMount() {
        document.body.classList.remove("sign-bg");
        document.body.classList.add("grey-bg");

        let storage = localStorage.getItem("MIO_Local");
        storage = storage ? JSON.parse(storage) : null;

        let selectedCompanyId = this.props.match.params.id;
        let selectedCompanyName = this.props.location.state.companyName;

        if (storage && storage.data.userId) {
            this.setState({
                userId: storage.data.userId,
                userType: storage.data.userType,
                companyId: storage.data.companyId,
                selectedCompanyId: selectedCompanyId,
                selectedCompanyName: selectedCompanyName
            }, () => {

                // this.loadData();
                // this.getCompanyListForAdmin();

            });

            // console.log(this.parseJwt(storage.token));
            let tokenData = this.parseJwt(storage.token);

        }
    }


    loadData = async () => {

        let get_companydetails = await this.getCompanyListForAdmin();
        let get_transactionDetails = await this.getTransactionListForAdmin();

    }

    getCompanyListForAdmin = () => {
        return new Promise((resolve, reject) => {
            axios
                .post(path + 'user/get-companies')
                .then(serverResponse => {
                    let res = serverResponse.data;
                    if (!res.isError) {
                        this.setState({
                            companyLists: res.details
                        }, () => {
                            resolve();
                        });
                    } else {
                        reject();
                    }
                })
                .catch(error => {
                    console.log(error);
                })
        })

    }

    // getTransactionListForAdmin = () => {

    //     return new Promise((resolve, reject) => {
    //         let { searchYear, searchMonth, searchString } = this.state;

    //         let dataToSend = {
    //             "year": searchYear,
    //             "month": searchMonth,
    //             "companyId": searchString,
    //             "page": 1,
    //             "perPage": 10
    //         };

    //         axios
    //             .post(path + 'payment/get-transaction-details', dataToSend)
    //             .then(serverResponse => {
    //                 let res = serverResponse.data;
    //                 if (res.isError) {
    //                     let details = res.details && res.details[0];
    //                     console.log("transaction details ==", details);

    //                     this.setState({
    //                         transactionLists: details.results
    //                     }, () => {
    //                         // console.log("transaction details after setState =", this.state.transactionLists);
    //                         resolve();
    //                     });
    //                 } else {
    //                     reject();
    //                 }
    //             })
    //             .catch(error => {
    //                 console.log(error);
    //             })
    //     })

    // }

    searchCompany = (event) => {
        if (event) {
            event.preventDefault();
        }
        this.fetchDetails("+");
    }

    fetchDetails = (status) => {

        this.setState({
            searching: true,
            loading: true
        });

        let { perPage, pageNo, searchString, selectedCompanyId, fromDt, toDT } = this.state;

        let dataToSend = {
            "fromDate": fromDt,
            "toDate": toDT,
            "companyId": selectedCompanyId,
            "page": pageNo,
            "perPage": perPage
        };

        axios
            .post(path + 'price-management/fetch-sipcall-related-details-for-date-range', dataToSend)
            .then(serverResponse => {
                let res = serverResponse.data;
                if (!res.isError) {

                    let sipCallList = res.details && res.details.sipCallList;
                    let { duration, cost, price } = this.state;
                    
                    if (status && status == "+") {
                        duration = parseFloat(duration)+parseFloat(res.details && res.details.totalSipCalDuration);
                        cost = parseFloat(cost)+parseFloat(res.details && res.details.totalSipCalCost);
                        price = parseFloat(price)+parseFloat(res.details && res.details.totalSipCalPrice);
                    } else if (status && status == "-") {
                        duration = parseFloat(duration)-parseFloat(res.details && res.details.totalSipCalDuration);
                        cost = parseFloat(cost)-parseFloat(res.details && res.details.totalSipCalCost);
                        price = parseFloat(price)-parseFloat(res.details && res.details.totalSipCalPrice);
                    }

                    this.setState({
                        transactionLists: sipCallList,
                        totalRows: res.details && res.details.noofpage,
                        totalRecords: res.details && res.details.total,
                        loading: false,
                        duration: duration,
                        cost: cost,
                        price: price
                    });
                } else {
                    this.setState({
                        loading: false
                    });
                }
            })
            .catch(error => {
                console.log(error);
            })

    }

    clearSearch = (event) => {
        event.preventDefault();
        this.setState({
            searchString: '',
            fromDt: '',
            toDT: '',
            pageNo: 1,
            duration: 0,
            cost: 0,
            price: 0,
            transactionLists: [],
            totalRows: '',
            totalRecords: '',
            searching: false,
            loading: false,
        }, () => {
            this.totalDuration = 0;
            this.totalCost = 0;
            this.totalPrice = 0;
            this.getCompanyListForAdmin();
            // this.getTransactionListForAdmin();
        });
    }

    pageChangePrev = () => {

        this.setState({
            pageNo: (parseFloat(this.state.pageNo) - 1)
        }, () => {
            this.fetchDetails("-")
        });

    }

    pageChangeNext = () => {

        this.setState({
            pageNo: (parseFloat(this.state.pageNo) + 1)
        }, () => {
            this.fetchDetails("+")
        });

    }

    clearState = () => {
        this.setState({
            loading: false,

        })

    }

    getCompanyName = (companyId) => {

        let { companyLists } = this.state;
        let companyNameStatus = _.filter(companyLists, { '_id': companyId })[0];
        return companyNameStatus && companyNameStatus.companyName;
        // console.log("getting company name status ===", companyNameStatus);

    }

    backMenueToggle = () => {
        this.props.history.goBack();
    }

    sideMenuToggle() {
        let { openSideMenu } = this.state;
        openSideMenu = !openSideMenu;
        this.setState({ openSideMenu });
    }

    mobMenuClick() {
        this.sideMenuToggle();
        console.log('mobMenuClick');
    }

    parseJwt = (token) => {
        if (!token) { return; }
        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace('-', '+').replace('_', '/');
        return JSON.parse(window.atob(base64));
    }


    render() {

        let { loading, transactionLists, companyLists, minOffset, maxOffset, thisYear, fromDt, toDT, pageNo, perPage, totalRows } = this.state;
        var options = [];
        for (let i = minOffset; i <= maxOffset; i++) {
            let year = thisYear - i;
            options.push(<option value={year} key={i}>{year}</option>);
        }
        // console.log("This year in state ===", thisYear);
        return (
            <main>

                <section className="user-mngnt-wrap">
                    <div className="container-fluid">
                        <div className="row">
                            <Navbar routTo="/adminreport" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />
                            <div className="col-xl-10 col-lg-9 p-0">
                                <div className="mobile-menu-header">
                                    <a href="JavaScript:Void(0);" className="back-arw" onClick={() => this.backMenueToggle()}><i className="fas fa-arrow-left"></i></a>

                                </div>
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="inv-code">
                                            <span style={{ color: '#007bff' }}>Outbound Report For {this.state.selectedCompanyName}</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="mng-full-list">
                                    <div className="mng-full-srch">

                                        <div className="row">
                                            <div className="col-md-5">
                                                <label>From Date</label>
                                                <input
                                                    type="date"
                                                    className="form-control"
                                                    name="fromDt" value={fromDt}
                                                    onChange={(event) => { this.setState({ fromDt: event.target.value }) }}
                                                />
                                            </div>

                                            <div className="col col-md-7">
                                                <label>To Date</label>
                                                <div className="srch-wrap">
                                                    <form>
                                                        <div className="row">
                                                            <div className="col col-md-9">
                                                                <input
                                                                    type="date"
                                                                    className="form-control"
                                                                    name="fromDt" value={toDT}
                                                                    onChange={(event) => { this.setState({ toDT: event.target.value }) }}
                                                                />
                                                            </div>
                                                            <div className="col col-md-3">
                                                                {this.state.searching ?
                                                                    <input type="button" onClick={(event) => this.clearSearch(event)}
                                                                        data-tip='Clear'
                                                                    />
                                                                    :
                                                                    <input type="submit" onClick={(event) => this.searchCompany(event)}
                                                                        data-tip='Search'
                                                                    />
                                                                }

                                                                <ReactTooltip
                                                                    effect="float"
                                                                    place="top"
                                                                    data-border="true"
                                                                />
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    {transactionLists ?
                                        <div className="mng-full-table company-mangmnt">

                                            <div className="row">

                                                <div className="col-md-12">
                                                    <div className="mng-full-table-hdr">
                                                        <div className="row">
                                                            <div className="col-md-2 border-rt">
                                                                <h6>Conference Title</h6>
                                                            </div>
                                                            <div className="col-md-2 border-rt">
                                                                <h6>Date/Time</h6>
                                                            </div>
                                                            <div className="col-md-2 border-rt">
                                                                <h6>To No.</h6>
                                                            </div>
                                                            <div className="col-md-2 border-rt">
                                                                <h6>Duration</h6>
                                                            </div>
                                                            <div className="col-md-2 border-rt">
                                                                <h6>Cost</h6>
                                                            </div>
                                                            <div className="col-md-2">
                                                                <h6>price</h6>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            {transactionLists ? transactionLists.map((data, index) => {

                                                return <div className="row" key={index}>
                                                    <div className="col-md-12">

                                                        <div className="mng-full-table-row">
                                                            <div className="row">
                                                                <div className="col-md-2 border-rt">
                                                                    <h6>Conference Title</h6>
                                                                    <p>
                                                                        {data.conferenceTitle && data.conferenceTitle}
                                                                    </p>
                                                                </div>
                                                                <div className="col-md-2 border-rt">
                                                                    <h6>Date/Time</h6>
                                                                    <p className="textEllips">
                                                                        {data.date ?
                                                                            <Moment format="DD MMM YYYY">
                                                                                {data.date}
                                                                            </Moment> : null} <br />
                                                                        {data.date ? <Moment format="HH:mm:ss">{data.date}</Moment> : null} {' to '}
                                                                        {data.updatedAt ? <Moment format="HH:mm:ss">{data.updatedAt}</Moment> : null}
                                                                    </p>
                                                                </div>
                                                                <div className="col-md-2 border-rt">
                                                                    <h6>To No.</h6>
                                                                    <p>
                                                                        {data.countryCode + '-' + data.toMobile}
                                                                    </p>
                                                                </div>
                                                                <div className="col-md-2 border-rt">
                                                                    <h6>Duration</h6>
                                                                    <p>{data.duration && parseFloat(data.duration).toFixed('2') + 'Mins'}</p>
                                                                </div>
                                                                <div className="col-md-2 border-rt">
                                                                    <h6>Cost</h6>
                                                                    <p>{data.totalCost && (parseFloat(data.totalCost).toFixed('2') + ' ')}</p>
                                                                </div>
                                                                <div className="col-md-2">
                                                                    <h6>Price</h6>
                                                                    <p>{data.totalPrice && (parseFloat(data.totalPrice).toFixed('2') + ' ')}</p>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                            })
                                                : null}

                                            {!loading && parseFloat(this.state.totalRecords) <= parseFloat(parseFloat(pageNo) * parseFloat(perPage)) ?
                                                <div className="row">
                                                    <div className="col-md-12">

                                                        <div className="mng-full-table-row">
                                                            <div className="row">
                                                                <div className="col-md-2 border-rt">
                                                                    <h6>Conference Title</h6>
                                                                    <p>
                                                                        Total
                                                                    </p>
                                                                </div>
                                                                <div className="col-md-2 border-rt">
                                                                    <h6>Date/Time</h6>
                                                                    <p className="textEllips">
                                                                        -
                                                                    </p>
                                                                </div>
                                                                <div className="col-md-2 border-rt">
                                                                    <h6>To No.</h6>
                                                                    <p>-</p>
                                                                </div>
                                                                <div className="col-md-2 border-rt">
                                                                    <h6>Duration</h6>
                                                                    <p>{this.state.duration && parseFloat(this.state.duration).toFixed('2') + 'Mins'}</p>
                                                                </div>
                                                                <div className="col-md-2 border-rt">
                                                                    <h6>Cost</h6>
                                                                    <p>{this.state.cost && parseFloat(this.state.cost).toFixed('2')}</p>
                                                                </div>
                                                                <div className="col-md-2">
                                                                    <h6>Price</h6>
                                                                    <p>{this.state.price && parseFloat(this.state.price).toFixed('2')}</p>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                : null}

                                        </div>
                                        :
                                        <div className="mng-full-table">
                                            <div className="row">
                                                <div className="col-md-12">
                                                    <div className="mng-full-table-hdr admn-usr-hdr">
                                                        <div className="row">
                                                            <div className="col-md-12">
                                                                <h6>No Details Found.</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    }

                                    <br /><br />
                                    <div className="form-group">
                                        {!loading && parseFloat(pageNo) > 1 ?
                                            <span style={{ color: "#007bff", cursor: "pointer" }}
                                                onClick={() => { this.pageChangePrev() }}
                                            >
                                                <b>{"<< Prev"}</b>
                                            </span>
                                            :
                                            null}
                                        &nbsp;&nbsp;|&nbsp;&nbsp;
                                        {!loading && (parseFloat(pageNo) < parseFloat(totalRows)) ?
                                            <span style={{ color: "#007bff", cursor: "pointer" }}
                                                onClick={() => { this.pageChangeNext() }}
                                            >
                                                <b>{"Next >>"}</b>
                                            </span>
                                            :
                                            null}
                                        {loading ?
                                            <a href="JavaScript:Void(0);" style={{ color: "#007bff" }} >
                                                <i className="fa fa-spinner fa-spin" ></i>
                                            </a>
                                            :
                                            null}

                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </section>

            </main>
        );
    }
}

export default checkAuthentication(AdminOutboundReport);
