import React, { Component } from 'react'
import Navbar from '../Component/Navbar'
import axios from "axios";
import cred from "../../cred.json";
import { Modal, Button } from "react-bootstrap"
import _ from 'lodash';
import { CSVLink, CSVDownload } from "react-csv";
import ReactTooltip from 'react-tooltip'; // For tool tip 

import { checkAuthentication } from '../Component/Authentication';

var path = cred.API_PATH;


export class AdminReport extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userId: '',
            userType: '',

            selectedCompanyId: '',
            selectedCompanyName: '',
            selectedCompanyType: '',

            openSideMenu: false,
            searchString: '',
            searching: false,

            loading: false,

            usersList: [],

        };
        this.reportList = [
            {"name": "Inbound Report", "key": "inbound"},
            {"name": "Outbound Report", "key": "outbound"},
            {"name": "Web Report", "key": "web"},
            {"name": "Archive Report", "key": "archive"},
            {"name": "Transaction Report", "key": "transaction"},
            {"name": "Conference Report", "key": "conference"},
            {"name": "Monthly Report", "key": "monthly"}
        ];
    }

    componentDidMount() {
        document.body.classList.remove("sign-bg");
        document.body.classList.add("grey-bg");

        if (this.props.location.state && this.props.location.state.name && this.props.location.state.type) {

            let storage = localStorage.getItem("MIO_Local");
            storage = storage ? JSON.parse(storage) : null;

            let selectedCompanyId = this.props.match.params.id;
            let selectedCompanyName = this.props.location.state.name;
            let selectedCompanyType = this.props.location.state.type;

            if (storage && storage.data.userId) {
                this.setState({
                    userId: storage.data.userId,
                    userType: storage.data.userType,
                    companyId: storage.data.companyId,
                    selectedCompanyId: selectedCompanyId,
                    selectedCompanyName: selectedCompanyName,
                    selectedCompanyType: selectedCompanyType
                });

                // console.log(this.parseJwt(storage.token));
                let tokenData = this.parseJwt(storage.token);

            }

        }else{
            this.props.history.push("/adminreport");
        }
            
    }

    openReport= (index)=> {

        let selectedReport = this.reportList[index];
        let selectedKey = selectedReport.key;

        if(selectedKey == 'inbound'){
            this.props.history.push({
                pathname: '/adminreport/'+`${this.state.selectedCompanyId}`+'/inbound',
                state: { companyName: this.state.selectedCompanyName }
            });
        }else if(selectedKey == 'outbound'){
            this.props.history.push({
                pathname: '/adminreport/'+`${this.state.selectedCompanyId}`+'/outbound',
                state: { companyName: this.state.selectedCompanyName }
            });
        }else if(selectedKey == 'web'){
            this.props.history.push({
                pathname: '/adminreport/'+`${this.state.selectedCompanyId}`+'/web',
                state: { companyName: this.state.selectedCompanyName }
            });
        }else if(selectedKey == 'archive'){
            this.props.history.push({
                pathname: '/adminreport/'+`${this.state.selectedCompanyId}`+'/archive',
                state: { companyName: this.state.selectedCompanyName }
            });
        }else if(selectedKey == 'transaction'){
            this.props.history.push({
                pathname: '/adminreport/'+`${this.state.selectedCompanyId}`+'/transaction',
                state: { companyName: this.state.selectedCompanyName }
            });
        }else if(selectedKey == 'conference'){
            this.props.history.push({
                pathname: '/adminreport/'+`${this.state.selectedCompanyId}`+'/conferencelist',
                state: { companyName: this.state.selectedCompanyName }
            });
        }else if(selectedKey == 'monthly'){
            this.props.history.push({
                pathname: '/adminreport/'+`${this.state.selectedCompanyId}`+'/monthly',
                state: { companyName: this.state.selectedCompanyName }
            });
        }

    }

    sideMenuToggle = () => {
        let { openSideMenu } = this.state;
        openSideMenu = !openSideMenu;
        this.setState({ openSideMenu });
    }

    backMenueToggle = () => {
        this.props.history.goBack();
    }

    mobMenuClick() {
        this.sideMenuToggle();
        console.log('mobMenuClick');
    }

    parseJwt = (token) => {
        if (!token) { return; }
        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace('-', '+').replace('_', '/');
        return JSON.parse(window.atob(base64));
    }


    render() {

        return <main>
            <section className="user-mngnt-wrap">
                <div className="container-fluid">
                    <div className="row">

                        <Navbar routTo="/adminreport" open={this.state.openSideMenu} mobMenuClick={() => this.mobMenuClick()} closeMenu={() => this.sideMenuToggle()} />
                        <div className="col-xl-10 col-lg-9 p-0 mob-full-conf">

                            <div className="mobile-menu-header">
                                <a href="JavaScript:Void(0);" className="back-arw" onClick={() => this.backMenueToggle()}><i className="fas fa-arrow-left"></i></a>
                                {/* <a href= "JavaScript:Void(0);" className="menu-tgl" onClick={() => this.sideMenuToggle()}><img src="images/menu-tgl.png" alt="" /></a> */}
                            </div>

                            {/* <div className="payment-mng-wrap"> */}
                            <div className="paymnt-mng-hdr" style={{ margin: "30px" }}>
                                <div className="blnc">
                                    <div className="logobox"><img src="/images/logo-sign.png" alt="" /></div>
                                </div>
                                <div className="rchrg-btn">
                                    <p className="text-right" style={{ margin: "0" }}>
                                        Company Name: {this.state.selectedCompanyName}
                                        <br />Company Type: {this.state.selectedCompanyType}
                                    </p>
                                </div>
                            </div>
                            {/* </div> */}

                            <div className="mng-full-list" style={{ padding: "0px 50px" }}>

                                <div className="mng-full-table">
                                    
                                    <div className="row">

                                        <div className="col-md-10">
                                            <div className="mng-full-table-hdr">
                                                <div className="row">
                                                    
                                                    <div className="col-md-12">
                                                        <h6>Name</h6>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-md-2">
                                            <div className="mng-full-table-hdr">
                                                <div className="row">
                                                    <div className="col-md-12">
                                                        <h6>View</h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    {this.reportList && this.reportList.map((value, index)=>{
                                        return<div className="row" key= {index}>

                                            <div className="col-md-10">
                                                <div className="mng-full-table-row">
                                                    <div className="row">
                                                        <div className="col-md-12">
                                                            <h6>Name</h6>
                                                            <p className="textEllips">
                                                                {value.name}
                                                            </p>
                                                        </div>
                                                        
                                                        <div className="mobile-ad-edt-btns">
                                                            <ul>
                                                                <li onClick={() => this.openReport(index)}>
                                                                    <a href="JavaScript:Void(0);"
                                                                        data-tip='View'
                                                                    >
                                                                        <i className="fas fa-eye"></i>
                                                                    </a>
                                                                    <ReactTooltip
                                                                        effect="float"
                                                                        place="top"
                                                                        data-border="true"
                                                                    />
                                                                </li>

                                                            </ul>
                                                        </div>

                                                    </div>
                                                </div>

                                            </div>

                                            <div className="col-md-2">
                                                <div className="mng-full-table-row add-edt text-center">
                                                    <div className="row">
                                                        <div className="col-md-12">
                                                            <a href="JavaScript:Void(0);"
                                                                data-tip='View'
                                                                onClick={() => { this.openReport(index) }}
                                                            >
                                                                <i className="fas fa-eye"></i>

                                                            </a>
                                                            <ReactTooltip
                                                                effect="float"
                                                                place="top"
                                                                data-border="true"
                                                            />
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    })}

                                </div>
                                    
                            </div>

                        </div>
                    </div>
                </div>

            </section>
        </main>

    }

}

export default checkAuthentication(AdminReport);