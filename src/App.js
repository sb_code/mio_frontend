import React, { Component } from 'react';
import { HashRouter, BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
// import { renderRoutes } from 'react-router-config';
import logo from './logo.svg';
import './App.css';
import AddPayment from './views/Component/Payment/AddPayment';
import ArchiveList from './views/Conference/archive-list';
import DurationDetails from './views/CommonPages/duration-details';
import Thankyou from './views/CommonPages/thankyou';

const loading = () => (
  <div className="animated fadeIn pt-3 text-center">Loading...</div>
);

// Containers
const DefaultLayout = React.lazy(() => import("./containers/DefaultLayout"));

/**
 *  Pages breakdown according to USER Role
 * 'ADMIN','ADMIN_OPERATOR','COMPANY_OPERATOR','ENTERPRISE','STARTER','PARTICIPANT'
 * 
 */


// Basic Auth Pages and content base page

const Register = React.lazy(() => import("./views/Pages/Register"));
const Login = React.lazy(() => import("./views/Pages/Login"));
const ForgotPass = React.lazy(() => import("./views/Pages/ForgotPass"));
const ResetPass = React.lazy(() => import("./views/Pages/ResetPass"));
const AboutUs = React.lazy(() => import("./views/Pages/AboutUs"));
const ContactUs = React.lazy(() => import("./views/Pages/ContactUs"));
const FAQ = React.lazy(() => import("./views/Pages/FAQ"));
const TermsConditions = React.lazy(() => import("./views/Pages/TermsConditions"));
const PrivacyPolicy = React.lazy(() => import("./views/Pages/PrivacyPolicy"));


const UserProfile = React.lazy(() => import("./views/CommonPages/user-profile"));
const CompanyProfile = React.lazy(() => import("./views/CommonPages/company-profile"));

// Other pages

const Userlist = React.lazy(() => import("./views/UtilityPages/userlist"));
const Adduser = React.lazy(() => import("./views/UtilityPages/adduser"));
const PaymentManagement = React.lazy(() => import("./views/CompanyPages/payment-management"));
const UsedServices = React.lazy(() => import("./views/CommonPages/used-services"));

const AdminPaymentManagement = React.lazy(() => import("./views/AdminPages/admin-payment-management"));
const AdminInvoiceManagement = React.lazy(() => import("./views/AdminPages/admin-invoice-management"));

const Packages = React.lazy(() => import("./views/CompanyPages/packages-screen"));
const Invoice = React.lazy(() => import("./views/UtilityPages/invoice-screen"));
const AdminInvoice = React.lazy(() => import("./views/UtilityPages/admin-invoice-screen"));
const OpmAddressBook = React.lazy(() => import("./views/CompanyPages/opm-addressbook")); // OPM => Operator management
const CompanyOperator = React.lazy(() => import("./views/CompanyPages/opm-company-operator"));

const OperatorManagement = React.lazy(() => import("./views/AdminPages/admin-operator-management"));

// const AdminOperator = React.lazy(() => import("./views/UtilityPages/opm-admin-operator"));
const OperatorManagementStat = React.lazy(() => import("./views/AdminPages/admin-operator-stat"));
const CompanyOperatorStat = React.lazy(() => import("./views/CompanyPages/company-operator-stat"));

const CompanyServices = React.lazy(() => import("./views/CompanyPages/company-service-management"));

const ContentManagement = React.lazy(() => import("./views/AdminPages/admin-content-management"));
const AboutusManagement = React.lazy(() => import("./views/AdminPages/about-management"));
const ContactusManagement = React.lazy(() => import("./views/AdminPages/contactus-management"));
const FaqManagement = React.lazy(() => import("./views/AdminPages/faq-management"));
const TermsconditionsManagement = React.lazy(() => import("./views/AdminPages/termscondition-management"));
const PrivecypolicyManagement = React.lazy(() => import("./views/AdminPages/privecy-management"));

const InboundDetails = React.lazy(() => import("./views/CommonPages/inbound-details"));

// const CallNotAvailable = React.lazy(() => import("./views/UtilityPages/call-not-available"));
// const CallRequests = React.lazy(() => import("./views/UtilityPages/call-requests"));
const ConferenceCall = React.lazy(() => import("./views/Conference/conference-call"));

const CompanyArchives = React.lazy(() => import("./views/CompanyPages/company-archives"));
const ConferenceArchives = React.lazy(() => import("./views/CompanyPages/conference-archives"));

// const ConferenceManagement = React.lazy(() => import("./views/UtilityPages/conference-management"));
// const NoMessage = React.lazy(() => import("./views/UtilityPages/no-message"));
const AdminAddressBook = React.lazy(() => import("./views/AdminPages/admin-company-address-book"));
const AdminUserManagement = React.lazy(() => import("./views/AdminPages/admin-user-management"));
const AdminCompanyUser = React.lazy(() => import("./views/AdminPages/admin-company-user"));
const AdminConference = React.lazy(() => import("./views/AdminPages/admin-conference"));
const AdminCompanyConference = React.lazy(() => import("./views/AdminPages/admin-company-conference"));
const ServiceManagement = React.lazy(() => import("./views/AdminPages/service-management"));
const AdminConferenceList = React.lazy(() => import("./views/AdminPages/admin-conferencelist"));


const AdminContentManagement = React.lazy(() => import("./views/AdminPages/admin-content-management"));
const AdminCompanyManagement = React.lazy(() => import("./views/AdminPages/admin-company-management"));
// const AdminPricingModel = React.lazy(() => import("./views/UtilityPages/admin-pricing-model"));
const UserManagement = React.lazy(() => import("./views/CompanyPages/user-management"));
const AdminOperatorPool = React.lazy(() => import("./views/CompanyPages/admin-operator-pool"));
const UpgradeAccount = React.lazy(() => import("./views/CompanyPages/upgrade-account")); 
const AccountUpgrade = React.lazy(() => import("./views/AdminPages/admin-upgrade-account"));
const PackageManagement = React.lazy(() => import("./views/AdminPages/package-management"));
const NewRegistered = React.lazy(() => import("./views/AdminPages/admin-new-registered"));

const AdminNumbermanagement = React.lazy(() => import("./views/AdminPages/number-management"));
const AdminNumberConfiguration = React.lazy(() => import("./views/AdminPages/number-configuration"));
const Numbermanagement = React.lazy(() => import("./views/CompanyPages/number-management"));
const AssignInBoundNumber = React.lazy(() => import("./views/AdminPages/assign-inbound-number"));
const AssignOutBoundNumber = React.lazy(() => import("./views/AdminPages/assign-outbound-number"));

const AdminCountryCodeConfiguration = React.lazy(() => import("./views/AdminPages/country-code-config"));
const AdminOutBoundConfiguration = React.lazy(() => import("./views/AdminPages/outbound-config"));

const AdminReportList = React.lazy(() => import("./views/Reports/admin-report-list"));
const AdminReport = React.lazy(() => import("./views/Reports/admin-report"));
const AdminInboundReport = React.lazy(() => import("./views/Reports/admin-inbound-report")); 
const AdminOutboundReport = React.lazy(() => import("./views/Reports/admin-outbound-report"));
const AdminArchiveReport = React.lazy(() => import("./views/Reports/admin-archive-report"));
const AdminWebReport = React.lazy(() => import("./views/Reports/admin-web-report"));
const AdminTransactionReport = React.lazy(() => import("./views/Reports/admin-transaction-report"));
const AdminConferenceReportList = React.lazy(() => import("./views/Reports/admin-conference-list"));
const AdminConferenceReport = React.lazy(() => import("./views/Reports/admin-conference-report")); 
const AdminMonthlyReport = React.lazy(() => import("./views/Reports/admin-monthly-list"));
const GlobalReport = React.lazy(() => import("./views/Reports/global-report"));


const CompanyReport = React.lazy(() => import("./views/Reports/company-report"));
const CompanyInboundReport = React.lazy(() => import("./views/Reports/company-inbound-report"));
const CompanyOutboundReport = React.lazy(() => import("./views/Reports/company-outbound-report"));
const CompanyArchiveReport = React.lazy(() => import("./views/Reports/company-archive-report"));
const CompanyWebReport = React.lazy(() => import("./views/Reports/company-web-report"));
const CompanyTransactionReport = React.lazy(() => import("./views/Reports/company-transaction-report"));
const CompanyConferenceReportList = React.lazy(() => import("./views/Reports/company-conference-list")); 
const CompanyConferenceReport = React.lazy(() => import("./views/Reports/company-conference-report"));
const CompanyMonthlyReport = React.lazy(() => import("./views/Reports/company-monthly-report"));


const ParticipantRegistration = React.lazy(() => import("./views/Component/participant-registration"));


class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isAuthUser: false
    };
  }

  componentDidMount() {
    // if (localStorage.getItem("mio_user_details")) {
    //   console.log("inside if2");
    //   this.setState(() => ({ isAuthUser: true }));
    // } else {
    //   console.log("inside else2");
    //   this.setState(() => ({ isAuthUser: false }));
    // }
  }

  render() {
    // const {isAuthUser} =this.state;
    return (
      <BrowserRouter>
        <React.Suspense fallback={loading()}>
          <Switch>
            <Route exact path="/" name="Login Page" render={props => <Login {...props} />} />
            <Route exact path="/login" name="Login Page" render={props => <Login {...props} />} />
            <Route exact path="/register" name="Register Page" render={props => <Register {...props} />} />
            <Route exact path="/forgotpass" name="Forgot Password" render={props => <ForgotPass {...props} />} />
            <Route exact path="/resetpass" name="Reset Password" render={props => <ResetPass {...props} />} />
            <Route exact path="/aboutus" name="About Us" render={props => <AboutUs {...props} />} />
            <Route exact path="/contactus" name="Contact Us" render={props => <ContactUs {...props} />} />
            <Route exact path="/faqs" name="FAQs" render={props => <FAQ {...props} />} />
            <Route exact path="/termsconditions" name="Terms and Conditions" render={props => <TermsConditions {...props} />} />
            <Route exact path="/privacypolicy" name="Privecy Policy" render={props => <PrivacyPolicy {...props} />} />


            <Route exact path="/userprofileupdate" name="Privecy Policy" render={props => <UserProfile {...props} />} />
            <Route exact path="/companyprofileupdate" name="Privecy Policy" render={props => <CompanyProfile {...props} />} />


            {/* <Route path="/" name="Home" render={props => <DefaultLayout {...props}/>} /> */}

            {/* <Route path="/userlist" name="User List" render={props => <Userlist {...props} />} />
            <Route path="/adduser/:id" name="Add User" render={props => <Adduser {...props} />} /> */}

            <Route path="/paymentmanagement" name="Payment Management" render={props => <PaymentManagement {...props} />} />
            <Route path="/usedservices" name="Payment Management" render={props => <UsedServices {...props} />} />

            <Route path="/adminpaymentmanagement" name="Payment Management" render={props => <AdminPaymentManagement {...props} />} />
            <Route path="/admininvoicemanagement/:id" name="Payment Management" render={props => <AdminInvoiceManagement {...props} />} />

            <Route path="/packages" name="Packages" render={props => <Packages {...props} />} />
            <Route path="/add-payment" name="Add Payment" render={props => <AddPayment {...props} />} />
            <Route path="/invoice/:id" name="Invoice" render={props => <Invoice {...props} />} />
            <Route path="/viewinvoice/:companyid/:invoiceid" name="Invoice" render={props => <AdminInvoice {...props} />} />
            <Route path="/opmaddressbook" name="OPM Address Book" render={props => <OpmAddressBook {...props} />} />
            <Route path="/companyoperator" name="Company Operator" render={props => <CompanyOperator {...props} />} />

            <Route path="/operatormanagement" name="Company Operator" render={props => <OperatorManagement {...props} />} />
            <Route path="/companyservicemanagement" name="Company Operator" render={props => <CompanyServices {...props} />} />

            <Route path="/companyarchives" name="Company Operator" render={props => <CompanyArchives {...props} />} />
            <Route path="/conferencearchivelist/:id" name="Company Operator" render={props => <ConferenceArchives {...props} />} />


            {/* <Route path="/adminoperator" name="Admin Operator" render={props => <AdminOperator {...props} />} /> */}
            <Route path="/adminoperatorstat/:id" name="Operator Management Statistics" render={props => <OperatorManagementStat {...props} />} />
            <Route path="/companyoperatorstat/:id" name="Operator Management Statistics" render={props => <CompanyOperatorStat {...props} />} />

            <Route path="/conferencecall" name="Conference Call" render={props => <ConferenceCall {...props} />} />
            <Route path="/thankyou" name="Thank You" render={props => <Thankyou {...props} />} />
            <Route path="/useses/:id" name="Useses" render={props => <DurationDetails {...props} />} />
            <Route path="/archives/:companyId/:conferenceId" name="Archive List" render={props => <ArchiveList {...props} />} />

            {/* <Route path="/conferencemanagement" name="Conference Management" render={props => <ConferenceManagement {...props} />} /> */}
            {/* <Route path="/nomessage" name="No Message" render={props => <NoMessage {...props} />} /> */}
            <Route path="/adminaddressbook/:company/:user" name="Admin Address Book" render={props => <AdminAddressBook {...props} />} />
            <Route path="/adminusermanagement" name="Admin User Management" render={props => <AdminUserManagement {...props} />} />
            <Route path="/admincompanyuser/:id" name="Admin User Management" render={props => <AdminCompanyUser {...props} />} />
            <Route path="/adminconference" name="Admin User Management" render={props => <AdminConference {...props} />} />
            {/* <Route path="/admincompanyconference/:id" name="Admin User Management" render={props => <AdminCompanyConference {...props} />} /> */}
            <Route path="/admincompanyconference/:id" name="Admin User Management" render={props => <ConferenceCall {...props} />} />
            <Route path="/servicemanagement" name="Service Management" render={props => <ServiceManagement {...props} />} />
            <Route path="/adminconferencelist/:id" name="Service Management" render={props => <AdminConferenceList {...props} />} />


            <Route exact path="/admincontentmanagement" name="Admin Content Management" render={props => <ContentManagement {...props} />} />
            <Route exact path="/admincontentmanagement/aboutus" name="Admin Content Management" render={props => <AboutusManagement {...props} />} />
            <Route exact path="/admincontentmanagement/contactus" name="Admin Content Management" render={props => <ContactusManagement {...props} />} />
            <Route exact path="/admincontentmanagement/faq" name="Admin Content Management" render={props => <FaqManagement {...props} />} />
            <Route exact path="/admincontentmanagement/termsconditions" name="Admin Content Management" render={props => <TermsconditionsManagement {...props} />} />
            <Route exact path="/admincontentmanagement/privacypolicy" name="Admin Content Management" render={props => <PrivecypolicyManagement {...props} />} />
            <Route exact path="/admincontentmanagement/package" name="Admin Content Management" render={props => <PackageManagement {...props} />} />

            <Route exact path="/join-information/:id" name="Inbound Details" render={props => <InboundDetails {...props} />} />


            {/* <Route path="/admincurrencymanagement" name="Admin Currency Management" render={props => <AdminCurrencyManagement {...props} />} /> */}
            <Route path="/admincompanymanagement" name="Admin Company Management" render={props => <AdminCompanyManagement {...props} />} />
            {/* <Route path="/adminpricingmodel" name="Admin Pricing Model" render={props => <AdminPricingModel {...props} />} /> */}
            <Route path="/usermanagement" name="Admin Pricing Model" render={props => <UserManagement {...props} />} />
            <Route path="/adminoperatorpool" name="Admin Pricing Model" render={props => <AdminOperatorPool {...props} />} />
            <Route path="/upgrade-account" name="Upgrade Account" render={props => <UpgradeAccount {...props} />} />
            <Route path="/adminaccountupgrade" name="Upgrade Account" render={props => <AccountUpgrade {...props} />} />
            <Route path="/newregistered" name="New Registered" render={props => <NewRegistered {...props} />} />

            <Route path="/numbermanagement" name="Upgrade Account" render={props => <Numbermanagement {...props} />} />
            <Route path="/numberconfigue" name="Upgrade Account" render={props => <AdminNumberConfiguration {...props} />} />
            <Route path="/adminnumbermanagement" name="Upgrade Account" render={props => <AdminNumbermanagement {...props} />} />
            <Route path="/assigninboundnumber/:id" name="Upgrade Account" render={props => <AssignInBoundNumber {...props} />} />
            <Route path="/assignoutboundnumber/:id" name="Upgrade Account" render={props => <AssignOutBoundNumber {...props} />} />

            <Route path="/countrycodeconfigue" name="Upgrade Account" render={props => <AdminCountryCodeConfiguration {...props} />} />
            <Route path="/outboundconfigue" name="Upgrade Account" render={props => <AdminOutBoundConfiguration {...props} />} />

            <Route exact path="/adminreport" name="Report" render={props => <AdminReportList {...props} />} />
            <Route exact path="/adminreport/:id" name="Report" render={props => <AdminReport {...props} />} />
            <Route exact path="/adminreport/:id/inbound" name="Report" render={props => <AdminInboundReport {...props} />} />
            <Route exact path="/adminreport/:id/outbound" name="Report" render={props => <AdminOutboundReport {...props} />} />
            <Route exact path="/adminreport/:id/archive" name="Report" render={props => <AdminArchiveReport {...props} />} />
            <Route exact path="/adminreport/:id/web" name="Report" render={props => <AdminWebReport {...props} />} />
            <Route exact path="/adminreport/:id/transaction" name="Report" render={props => <AdminTransactionReport {...props} />} />
            <Route exact path="/adminreport/:id/conferencelist" name="Report" render={props => <AdminConferenceReportList {...props} />} />
            <Route exact path="/adminreport/:id/conference/:conId" name="Report" render={props => <AdminConferenceReport {...props} />} />
            <Route exact path="/adminreport/:id/monthly" name="Report" render={props => <AdminMonthlyReport {...props} />} />
            <Route exact path="/monthly-summary" name="Report" render={props => <GlobalReport {...props} />} />

            <Route exact path="/companyreport" name="Report" render={props => <CompanyReport {...props} />} />
            <Route exact path="/companyreport/inbound" name="Report" render={props => <CompanyInboundReport {...props} />} />
            <Route exact path="/companyreport/outbound" name="Report" render={props => <CompanyOutboundReport {...props} />} />
            <Route exact path="/companyreport/archive" name="Report" render={props => <CompanyArchiveReport {...props} />} />
            <Route exact path="/companyreport/web" name="Report" render={props => <CompanyWebReport {...props} />} />
            <Route exact path="/companyreport/transaction" name="Report" render={props => <CompanyTransactionReport {...props} />} />
            <Route exact path="/companyreport/conferencelist" name="Report" render={props => <CompanyConferenceReportList {...props} />} />
            <Route exact path="/companyreport/conference/:id" name="Report" render={props => <CompanyConferenceReport {...props} />} />
            <Route exact path="/companyreport/monthly" name="Report" render={props => <CompanyMonthlyReport {...props} />} />


            <Route path="/participant-registration/:id" name="Participant-Registration" render={props => <ParticipantRegistration {...props} />} />



          </Switch>
        </React.Suspense>
      </BrowserRouter>
    );
  }
}

export default App;



//--------------------------------------------- Old boilerplate code -------------------------------- //


// import React from 'react';
// import logo from './logo.svg';
// import './App.css';

// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

// export default App;
