import React, { Component } from 'react';
import Moment from 'react-moment';
import ReactDomServer from 'react-dom/server';

export function getInvoiceReportHtml(invoice, usageDetails) {

  let services = invoice.services;
  let totalDuration = 0;
  services && services.map(value => {
    totalDuration = value.totalDuration ? (parseFloat(value.totalDuration) + parseFloat(totalDuration)).toFixed(2) : parseFloat(totalDuration).toFixed(2);
  })

  let html = ReactDomServer.renderToStaticMarkup(
    <div id='port'>
      <div id="pageHeader"></div>

      <div className="elemClass page">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff">
          <tbody>

            <tr>
              <td>
                <table width="100%" border="0">
                  <tbody>
                    <tr>
                      <td width="5%">&nbsp;</td>
                      <td width="45%">
                        {/* <img src="images/menu-tgl.png" alt="" width="250" className="img-logo" />  */}
                        {/* <img src="https://test-subendu-bucket.s3.us-east-2.amazonaws.com/logo-big.png" alt="" width="250" className="img-logo" /> */}
                        <img src={invoice.logo} alt="" width="250" className="img-logo" />
                      </td>
                      <td width="45%">
                        <h1 className="title-logo">Invoice</h1>
                      </td>
                      <td width="5%">&nbsp;</td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>

            <tr>
              <td>
                <table width="100%">
                  <tbody>
                    <tr>
                      <td width="5%">&nbsp;</td>
                      <td width="45%">
                        <p className="title-heading">To: <br />{invoice.companyName}</p>
                        {/* <p className="title-sub-heading">{invoice.companyName} <br />{invoice.address} <br /></p> */}
                        <p className="title-sub-heading">{invoice.address} <br /></p>

                      </td>
                      <td width="45%">
                        <table width="100%" class="table table-bordered">
                          <tbody>
                            <tr>
                              <td width="50%" className="cell-text-right">
                                <strong>Invoice Date:</strong>
                              </td>
                              <td width="50%">
                                {invoice.issueDate}
                              </td>
                            </tr>
                            <tr>
                              <td width="50%" className="cell-text-right">
                                <strong>Invoice Due Date:</strong>
                              </td>
                              <td width="50%">
                                {invoice.dueDate}
                              </td>
                            </tr>
                            <tr>
                              <td width="50%" className="cell-text-right">
                                <strong>Invoice Number:</strong>
                              </td>
                              <td width="50%">
                                {invoice.invoiceNumber}
                              </td>
                            </tr>
                            <tr>
                              <td width="50%" className="cell-text-right">
                                <strong>Customer Number:</strong>
                              </td>
                              <td width="50%">
                                {invoice.companyId}
                              </td>
                            </tr>
                          </tbody>
                        </table>

                        <table width="100%" class="table table-bordered">
                          <tbody>
                            <tr>
                              <td width="50%" className="cell-text-right">
                                <strong>Our VAT Number:</strong>
                              </td>
                              <td width="50%">
                                {/* GB972324124# */}
                                {invoice.vatNumber}
                              </td>
                            </tr>
                          </tbody>
                        </table>

                      </td>
                      <td width="5%">&nbsp;</td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>

            <tr>
              <td>
                <table width="100%" border="0">
                  <tbody>
                    <tr>
                      <td width="5%">&nbsp;</td>
                      <td width="90%">
                        <h2 className="summary-table-heading">Total Usage</h2>
                        <table width="75%" class="table table-bordered" className="summary-table">
                          <tbody>
                            <tr>
                              <th>
                                <strong>Total ex VAT</strong>
                              </th>
                              <th>
                                <strong>Vat</strong>
                              </th>
                              <th>
                                <strong>Total</strong>
                              </th>
                              <th>
                                <strong>Currency</strong>
                              </th>
                            </tr>
                            <tr>
                              <td className="cell-text-right">
                                {invoice.totalAmount ? parseFloat(invoice.totalAmount).toFixed(2) : 0.0}
                              </td>
                              <td className="cell-text-right">
                                {invoice.vatAmount ? parseFloat(invoice.vatAmount).toFixed(2) : 0.0}
                              </td>
                              <td className="cell-text-right">
                                {invoice.vatAddedTotal ? parseFloat(invoice.vatAddedTotal).toFixed(2) : 0.0}
                              </td>
                              <td className="cell-text-right">
                                <strong>{invoice.currency}</strong>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                      <td width="5%">&nbsp;</td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>

            <tr>
              <td>
                <table width="100%" border="0">
                  <tbody>
                    <tr>
                      <td width="5%">&nbsp;</td>
                      <td width="90%">
                        <h2 className="summary-table-heading">Summary</h2>
                        <table width="100%" class="table table-bordered" className="summary-table">
                          <tbody>
                            <tr>
                              <th>
                                <strong>Service</strong>
                              </th>
                              <th>
                                <strong>Description</strong>
                              </th>
                              <th className="cell-text-right">
                                <strong>Minutes</strong>
                              </th>
                              <th className="cell-text-right">
                                <strong>Charge</strong>
                              </th>
                            </tr>
                            {invoice.services && invoice.services.length > 0 && invoice.services.map((value, index) => {
                              return <tr>
                                <td>
                                  {value.serviceKey}
                                </td>
                                <td>
                                  {value.serviceName}
                                </td>
                                <td className="cell-text-right">
                                  {parseFloat(value.totalDuration).toFixed(2)}
                                </td>
                                <td className="cell-text-right">
                                  <span className="details-table-float">{invoice.currency}</span>
                                  <strong>{parseFloat(value.totalPrice).toFixed(2)}</strong>
                                </td>
                              </tr>
                            })}
                            <tr>
                              <td colspan="2">
                                <strong>Please see following sheet(s) for detail</strong>
                              </td>
                              <td className="cell-text-right">
                                <span className="details-table-float">Total Mins.</span>
                                <strong>{totalDuration}</strong>
                              </td>
                              <td className="cell-text-right">
                                <span className="details-table-float">{invoice.currency}</span>
                                <strong>{parseFloat(invoice.totalAmount).toFixed(2)}</strong>
                              </td>
                            </tr>
                            <tr>
                              <td colspan="3" className="cell-text-right">
                                <strong>VAT @ {parseFloat(invoice.vat).toFixed(2)}%</strong>
                              </td>
                              <td className="cell-text-right">
                                <span className="details-table-float">{invoice.currency}</span>
                                <strong>{parseFloat(invoice.vatAmount).toFixed(2)}</strong>
                              </td>
                            </tr>
                            <tr>
                              <td colspan="3" className="cell-text-right">
                                <strong>Total Payable Including VAT</strong>
                              </td>
                              <td className="summary-vat-tax">
                                <span className="details-table-float">{invoice.currency}</span>
                                <strong>{parseFloat(invoice.vatAddedTotal).toFixed(2)}</strong>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                      <td width="5%">&nbsp;</td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>

            <tr>
              <td>
                <p className="middle-paragraph">
                  {invoice.invoiceBillingInfo}
                </p>
              </td>
            </tr>

            <tr>
              <td style={{ height: "30" }}>&nbsp;</td>
            </tr>

          </tbody>
        </table>
      </div>

      <div className="elemClass page">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff">
          <tbody>
            <tr>
              <td>
                <table width="100%" border="0">
                  <tbody>
                    <tr>
                      <td width="5%">&nbsp;</td>
                      <td width="90%">
                        <h2 className="summary-table-heading">Detail</h2>
                        <table width="100%" class="table table-bordered" className="cell-text-left">
                          <tbody>

                            <tr>
                              <th>
                                <strong>Conference-conferenceTitle</strong>
                              </th>
                              <th>
                                <strong>Service</strong>
                              </th>
                              <th className="cell-text-center">
                                <strong>Date-date</strong>
                              </th>
                              <th className="cell-text-center">
                                <strong>Time-date</strong>
                              </th>
                              <th className="cell-text-right">
                                <strong>Minutes-duration</strong>
                              </th>
                              <th className="cell-text-right">
                                <strong>Charge-totalPrice</strong>
                              </th>
                            </tr>

                            {usageDetails && usageDetails.descriptionOfArchives && usageDetails.descriptionOfArchives.ArchiveList && usageDetails.descriptionOfArchives.ArchiveList.map((archive, index) => {

                              return <tr key={index}>
                                <td>
                                  {archive.conferenceTitle}
                                </td>
                                <td>
                                  Archives
                                </td>
                                <td className="cell-text-center">
                                  <Moment format="YYYY/MM/DD">
                                    {archive.date}
                                  </Moment>
                                </td>
                                <td className="cell-text-center">
                                  <Moment format="hh:mm:ss a">
                                    {archive.createdAt}
                                  </Moment>
                                  -
                                  <Moment format="hh:mm:ss a">
                                    {archive.updatedAt}
                                  </Moment>
                                  {/* 18:30:15 - 18:39:30 */}
                                </td>
                                <td className="cell-text-right">
                                  {archive.duration && parseFloat(archive.duration).toFixed(2)}
                                </td>
                                <td className="cell-text-right">
                                  <span className="details-table-float">{invoice.currency}</span>
                                  <span className="details-charges-float">{archive.totalPrice && parseFloat(archive.totalPrice).toFixed(2)}</span>
                                </td>
                              </tr>

                            })}

                            {usageDetails && usageDetails.descriptionOfWebCall && usageDetails.descriptionOfWebCall.webCallList && usageDetails.descriptionOfWebCall.webCallList.map((webCall, index) => {

                              return <tr key={index}>
                                <td>
                                  {webCall.conferenceTitle}
                                </td>
                                <td>
                                  Web Calls
                                </td>
                                <td className="cell-text-center">
                                  <Moment format="YYYY/MM/DD">
                                    {webCall.date}
                                  </Moment>
                                </td>
                                <td className="cell-text-center">
                                  <Moment format="hh:mm:ss a">
                                    {webCall.createdAt}
                                  </Moment>
                                  -
                                  <Moment format="hh:mm:ss a">
                                    {webCall.updatedAt}
                                  </Moment>
                                  {/* 18:30:15 - 18:39:30 */}
                                </td>
                                <td className="cell-text-right">
                                  {webCall.duration && parseFloat(webCall.duration).toFixed(2)}
                                </td>
                                <td className="cell-text-right">
                                  <span className="details-table-float">{invoice.currency}</span>
                                  <span className="details-charges-float">{webCall.totalPrice && parseFloat(webCall.totalPrice).toFixed(2)}</span>
                                </td>
                              </tr>

                            })}

                            {usageDetails && usageDetails.descriptionOfSipCall && usageDetails.descriptionOfSipCall.sipCallList && usageDetails.descriptionOfSipCall.sipCallList.map((sipCall, index) => {

                              return <tr key={index}>
                                <td>
                                  {sipCall.conferenceTitle}
                                </td>
                                <td>
                                  Phone Calls
                                </td>
                                <td className="cell-text-center">
                                  <Moment format="YYYY/MM/DD">
                                    {sipCall.date}
                                  </Moment>
                                </td>
                                <td className="cell-text-center">
                                  <Moment format="hh:mm:ss a">
                                    {sipCall.date}
                                  </Moment>
                                  -
                                  <Moment format="hh:mm:ss a">
                                    {sipCall.updatedAt}
                                  </Moment>
                                  {/* 18:30:15 - 18:39:30 */}
                                </td>
                                <td className="cell-text-right">
                                  {sipCall.duration && parseFloat(sipCall.duration).toFixed(2)}
                                </td>
                                <td className="cell-text-right">
                                  <span className="details-table-float">{invoice.currency}</span>
                                  <span className="details-charges-float">{sipCall.totalPrice && parseFloat(sipCall.totalPrice).toFixed(2)}</span>
                                </td>
                              </tr>
                            })}

                            {usageDetails && usageDetails.descriptionOfSipInbound && usageDetails.descriptionOfSipInbound.sipInboundList && usageDetails.descriptionOfSipInbound.sipInboundList.map((inboundSipCall, index) => {

                              return <tr key={index}>
                                <td>
                                  {inboundSipCall.conferenceTitle}
                                </td>
                                <td>
                                  Received Calls
                                </td>
                                <td className="cell-text-center">
                                  <Moment format="YYYY/MM/DD">
                                    {inboundSipCall.date}
                                  </Moment>
                                </td>
                                <td className="cell-text-center">
                                  <Moment format="hh:mm:ss a">
                                    {inboundSipCall.createdAt}
                                  </Moment>
                                   -
                                  <Moment format="hh:mm:ss a">
                                    {inboundSipCall.updatedAt}
                                  </Moment>
                                  {/* 18:30:15 - 18:39:30 */}
                                </td>
                                <td className="cell-text-right">
                                  {inboundSipCall.duration && parseFloat(inboundSipCall.duration).toFixed(2)}
                                </td>
                                <td className="cell-text-right">
                                  <span className="details-table-float">{invoice.currency}</span>
                                  <span className="details-charges-float">{inboundSipCall.totalPrice && parseFloat(inboundSipCall.totalPrice).toFixed(2)}</span>
                                </td>
                              </tr>
                            })}

                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>

            <tr>
              <td style={{ height: "30" }}>&nbsp;</td>
            </tr>

          </tbody>
        </table>
      </div>

      <div id="pageFooter"></div>

    </div>
  )

  return html;
}