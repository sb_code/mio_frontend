
// @charset "utf-8";
/*..............embedded fonts..............*/

// @import url('https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900&display=swap');


export const reportCSS = ` 

body *{font-family: Arial, sans-serif !important; zoom: 96%; }
    
table { border-collapse: collapse; border-spacing: 0; }
table { width: 100%; max-width: 100%; margin: 0 0 15px;}
.table-bordered { border: 1px solid #ddd; }
.table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th { border: 1px solid #ddd; }
table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th { padding: 5px; line-height: 1.42857143; vertical-align: middle; border-top: 1px solid #ddd; font-size: 14px}


.cell-text-right { text-align: right;}
.cell-text-center { text-align: center;}
.cell-text-left { text-align: left;}

.img-logo {border: 0; display: inline-block; margin: 15px 0;}
.title-logo {color: #99ccff; margin: 0; text-align: right; font-weight: normal; font-size: 35px;}
.title-heading {margin: 0 0 10px; color: #000; font-size: 15px;}
.title-sub-heading {margin: 0 0 5px; color: #000; font-size: 15px;}

.summary-table-heading { color: #99ccff; margin: 0 0 10px; text-align: left; font-weight: normal; font-size: 20px; }
.summary-table { text-align: right; max-width: 75%; }
.summary-vat-tax { text-align: right; background: #99ccff; }
.details-charges-float { float: right; }

.details-table-float { float: left; }

.middle-paragraph { text-align: center; font-weight: normal; font-size: 12px; color: #050d1a; margin:0 0 10px 0; }
.page { page-break-after: always; } .page:last-of-type { page-break-after: auto }

`